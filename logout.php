<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
include 'common.php';

$loggedout = $user->user_data['is_online'] - 320; // 5 min 20 sec

$query = "UPDATE " . $DBPrefix . "users SET hide_online = :yes, is_online = :timer WHERE id = :id";
$params = array();
$params[] = array(':yes', 'y', 'str');
$params[] = array(':timer', $loggedout, 'int');
$params[] = array(':id', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']), 'int');
$db->query($query, $params);

$query = "DELETE from " . $DBPrefix . "online WHERE SESSION = :id";
$params = array();
$params[] = array(':id', 'uId-' . $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']), 'str');
$db->query($query, $params);

if(isset($_SESSION['SELL_action']) || isset($_SESSION['SELL_auction_id'])) 
{
	include $include_path . 'functions_sell.php';
	unsetsessions();
}

if (isset($_COOKIE[$system->SETTINGS['cookie_name'] . '-RM_ID']))
{
	$query = "DELETE FROM " . $DBPrefix . "rememberme WHERE hashkey = :hash";
	$params = array();
	$params[] = array(':hash', $_COOKIE[$system->SETTINGS['cookie_name'] . '-RM_ID'], 'str');
	$db->query($query, $params);
	setcookie($system->SETTINGS['cookie_name'] . '-RM_ID', '', $system->ctime - 3600);
}
if (isset($_SESSION['csrftoken'])) unset($_SESSION['csrftoken']);
if (isset($_SESSION['FBOOK_USER_IDS'])) unset($_SESSION['FBOOK_USER_IDS']);
if (isset($_SESSION['FB_WB'])) unset($_SESSION['FB_WB']);
if (isset($_SESSION['FBOOK_USER_EMAIL'])) unset($_SESSION['FBOOK_USER_EMAIL']);
if (isset($_SESSION['FBOOK_USER_NAME'])) unset($_SESSION['FBOOK_USER_NAME']);
if (isset($_SESSION['FBOOK_USER_IMAGE'])) unset($_SESSION['FBOOK_USER_IMAGE']);
if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN'])) unset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']); 
if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_NUMBER'])) unset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_NUMBER']);
if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_PASS'])) unset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_PASS']);

header('location: home');
exit;
