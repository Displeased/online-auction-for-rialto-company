<?php 
$current_page = 'banners';
include 'common.php';

include $main_path . 'language/' . $language . '/categories.inc.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'userbanners.php';
	header('location: user_login.php');
	exit;
}

unset($ERR);
$id = intval($_REQUEST['id']);
$seller = $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']);
$BANNERS = array();
// Retrieve user's information
$query = "SELECT id, name, company, email FROM " . $DBPrefix . "bannersusers WHERE id = :ids";
$params = array();
$params[] = array(':ids', $id, 'int');
$db->query($query, $params);
$USER = $db->result();
	
// REtrieve user's banners
$query = "SELECT * FROM " . $DBPrefix . "banners WHERE user = :user_id";
$params = array();
$params[] = array(':user_id', $USER['id'], 'int');
$db->query($query, $params);
$bg = '';
while ($row = $db->result())
{
	$template->assign_block_vars('banners', array(
			'ID' => $row['id'],
			'TYPE' => $row['type'],
			'NAME' => $row['name'],
			'BANNER' => $uploaded_path . 'banners/' . $id . '/' . $row['name'],
			'WIDTH' => $row['width'],
			'HEIGHT' => $row['height'],
			'URL' => $row['url'],
			'ALT' => $row['alt'],
			'SPONSERTEXT' => $row['sponsortext'],
			'VIEWS' => $row['views'],
			'CLICKS' => $row['clicks'],
			'PURCHASED' => $row['purchased'],
			'BG' => $bg
			));
	$bg = ($bg == '') ? 'class="bg"' : '';
}

// get fees
$query = "SELECT * FROM " . $DBPrefix . "fees";
$db->direct_query($query);
$i = 0;
while ($row = $db->result())
{
	if ($row['type'] == 'ex_banner_fee')
	{
			$template->assign_vars(array(
				'B_EX_BANNER_FEE' => ($row['value'] > 0),
				'EX_BANNER_FEE' => $system->print_money($row['value'])
				));
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'ID' => $id,
		'NAME' => $USER['name'],
		'COMPANY' => $USER['company'],
		'EMAIL' => $USER['email'],
		'NOTEDIT' => true
		));

include 'header.php';
include 'includes/user_cp.php';
$template->set_filenames(array(
		'body' => 'userbanners.tpl'
		));
$template->display('body');
include 'footer.php';
