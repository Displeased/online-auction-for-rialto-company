<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
if ($system->SETTINGS['boards'] == 'n')
{
	header('location: index.php');
}
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'boards.php';
	header('location: user_login.php');
	exit;
}
// Retrieve message boards from the database
$query = "SELECT * FROM " . $DBPrefix . "community WHERE active = :act ORDER BY name";
$params = array();
$params[] = array(':act', 1, 'int');
$db->query($query, $params);

while ($row = $db->result())
{
	$template->assign_block_vars('boards', array(
			'NAME' => $row['name'],
			'ID' => $row['id'],
			'NUMMSG' => $row['messages'],
			'LASTMSG' => (!empty($row['lastmessage'])) ? FormatDate($row['lastmessage']) : '--'
			));
}
include 'header.php';
$template->set_filenames(array(
		'body' => 'boards.tpl'
		));
$template->display('body');
include 'footer.php';