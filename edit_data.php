<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $include_path . 'countries.inc.php';

if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'edit_data.php';
	header('location: user_login.php');
	exit;
}
$_SESSION['REDIRECT_AFTER_LOGIN'] = 'edit_data.php';
// Retrieve users signup settings
$MANDATORY_FIELDS = unserialize($system->SETTINGS['mandatory_fields']);

$TIMECORRECTION = array();
for ($i = 21; $i > -15; $i--)
{
	$TIMECORRECTION[$i] = $MSG['TZ_' . $i];
}

$query = "SELECT * FROM " . $DBPrefix . "gateways LIMIT :one";
$params = array();
$params[] = array(':one', 1, 'int');
$db->query($query, $params);
$gateway_data = $db->result();

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Check data
	if ($_POST['TPL_email'])
	{
		if (strlen($_POST['TPL_password']) < 6 && strlen($_POST['TPL_password']) > 0)
		{
			$ERR = $ERR_011;
		}
		elseif ($_POST['TPL_password'] != $_POST['TPL_repeat_password'])
		{
			$ERR = $ERR_109;
		}
		elseif (strlen($_POST['TPL_email']) < 5)
		{
			$ERR = $ERR_110;
		}
		elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['TPL_email']))
		{
			$ERR = $ERR_008;
		}
		elseif (strlen($_POST['TPL_zip']) < 4 && $MANDATORY_FIELDS['zip'] == 'y')
		{
			$ERR = $ERR_616;
		}
		elseif (strlen($_POST['TPL_phone']) < 3 && $MANDATORY_FIELDS['tel'] == 'y')
		{
			$ERR = $ERR_617;
		}
		elseif ((empty($_POST['TPL_day']) || empty($_POST['TPL_month']) || empty($_POST['TPL_year'])) && $MANDATORY_FIELDS['birthdate'] == 'y')
		{
			$ERR = $MSG['948'];
		}
		elseif (!empty($_POST['TPL_day']) && !empty($_POST['TPL_month']) && !empty($_POST['TPL_year']) && !checkdate($_POST['TPL_month'], $_POST['TPL_day'], $_POST['TPL_year']))
		{
			$ERR = $ERR_117;
		}
		elseif ($gateway_data['paypal_required'] == 1 && (empty($_POST['TPL_pp_email']) || !preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['TPL_pp_email'])))
		{
			$ERR = $MSG['810'];
		}
		elseif ($gateway_data['authnet_required'] == 1 && (empty($_POST['TPL_authnet_id']) || empty($_POST['TPL_authnet_pass'])))
		{
			$ERR = $MSG['811'];
		}
		elseif ($gateway_data['skrill_required'] == 1 && (empty($_POST['TPL_skrill_email']) || !preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['TPL_skrill_email'])))
		{
			$ERR = $MSG['822'];
		}
		elseif ($gateway_data['toocheckout_required'] == 1 && (empty($_POST['TPL_toocheckout_id'])))
		{
			$ERR = $MSG['821'];
		}
		elseif ($gateway_data['worldpay_required'] == 1 && (empty($_POST['TPL_worldpay_id'])))
		{
			$ERR = $MSG['823'];
		}
		elseif ($gateway_data['bank_required'] == 1 && (empty($_POST['TPL_bank_name']) || empty($_POST['TPL_bank_account']) || empty($_POST['TPL_bank_routing'])))  
        {  
            $ERR = $MSG['30_0223'];  
        } 
        // check the email to see if it is a Disposable email and blacklisted		
		elseif ($system->SETTINGS['disposable_email_block'] == 'y' && emailblacklist($_POST['TPL_email']) == "failed") 
		{
			//Disposable email detected
			$ERR = $ERR_115_a;
		}
		else
		{
			if (!empty($_POST['TPL_day']) && !empty($_POST['TPL_month']) && !empty($_POST['TPL_year']))
			{
				$TPL_birthdate = $_POST['TPL_year'] . $_POST['TPL_month'] . $_POST['TPL_day'];
			}
			else
			{
				$TPL_birthdate = '';
			}
			$query = "UPDATE " . $DBPrefix . "users SET email='" . $system->cleanvars($_POST['TPL_email']) . "',
					birthdate = '" . ((empty($TPL_birthdate)) ? 0 : $TPL_birthdate) . "',
					address = '" . $system->cleanvars($_POST['TPL_address']) . "',
					city = '" . $system->cleanvars($_POST['TPL_city']) . "',
					prov = '" . $system->cleanvars($_POST['TPL_prov']) . "',
					country = '" . $system->cleanvars($_POST['TPL_country']) . "',
					zip = '" . $system->cleanvars($_POST['TPL_zip']) . "',
					phone = '" . $system->cleanvars($_POST['TPL_phone']) . "',
					timecorrection = '" . $system->cleanvars($_POST['TPL_timezone']) . "',
					emailtype = '" . $system->cleanvars($_POST['TPL_emailtype']) . "',
					nletter = '" . $system->cleanvars($_POST['TPL_nletter']) . "'";

			if ($gateway_data['paypal_active'] == 1)
			{
				$query .= ", paypal_email = '" . $system->cleanvars($_POST['TPL_pp_email']) . "'";
			}
			if ($gateway_data['authnet_active'] == 1)
			{
				$query .= ", authnet_id = '" . $system->cleanvars($_POST['TPL_authnet_id']) . "',
							authnet_pass = '" . $system->cleanvars($_POST['TPL_authnet_pass']) . "'";
			}
			if ($gateway_data['worldpay_active'] == 1)
			{
				$query .= ", worldpay_id = '" . $system->cleanvars($_POST['TPL_worldpay_id']) . "'";
			}

			if ($gateway_data['skrill_active'] == 1)
			{
				$query .= ", skrill_email = '" . $system->cleanvars($_POST['TPL_skrill_email']) . "'";
			}
			if ($gateway_data['toocheckout_active'] == 1)
			{
				$query .= ", toocheckout_id = '" . $system->cleanvars($_POST['TPL_toocheckout_id']) . "'";
			}
			if ($gateway_data['bank_active'] == 1)  
            {  
                $query .= ", bank_name = '" . $system->cleanvars($_POST['TPL_bank_name']) . "', 
                bank_account = '" . $system->cleanvars($_POST['TPL_bank_account']) . "', 
                bank_routing = '" . $system->cleanvars($_POST['TPL_bank_routing']) . "'"; 
            } 
			if (strlen($_POST['TPL_password']) > 0)
			{
				$hash = get_hash();
				$password = $_POST['TPL_password'];
				$hashed_password = $phpass->HashPassword($password);
				$query .= ", password = '" . $hashed_password . "'";
				$query .= ", hash = '" . $hash . "'";

			}
			if (isset($_POST['hide_online']))
			{
				$hide = $system->ctime - 310;
				$query .= ", hide_online = 'y', 
				is_online = '" . $hide  . "'";
			}
			else
			{
				$show = $system->ctime;
				$query .= ", hide_online = 'n', 
				is_online = '" . $show . "'";
			}
			if (isset($_SESSION['FBOOK_USER_IDS']))
			{
				$query .= ", fblogin_id = '" . $_SESSION['FBOOK_USER_IDS'] . "'";
			}
			$query .= " WHERE id = " . $user->user_data['id'];
			$db->direct_query($query);
			$ERR = $MSG['183'];
			unset($_SESSION['FB_ED']);
			unset($_SESSION['FBOOK_USER_IDS']);
		}
	}
	else
	{
		$ERR = $ERR_112;
	}
}

// Retrieve user's data
$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :user_id";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$db->query($query, $params);

$USER = $db->result();
if ($USER['birthdate'] != 0)
{
	$TPL_day = substr($USER['birthdate'], 6, 2);
	$TPL_month = substr($USER['birthdate'], 4, 2);
	$TPL_year = substr($USER['birthdate'], 0, 4);
}
else
{
	$TPL_day = '';
	$TPL_month = '';
	$TPL_year = '';
}

$country = '';
foreach ($countries as $code => $name)
{
	$country .= '<option value="' . $name . '"';
	if ($name == $USER['country'])
	{
		$country .= ' selected';
	}
	$country .= '>' . $name . '</option>' . "\n";
}
$dobmonth = '<select name="TPL_month">
		<option value=""></option>
		<option value="01"' . (($TPL_month == '01') ? ' selected' : '') . '>' . $MSG['MON_001E'] . '</option>
		<option value="02"' . (($TPL_month == '02') ? ' selected' : '') . '>' . $MSG['MON_002E'] . '</option>
		<option value="03"' . (($TPL_month == '03') ? ' selected' : '') . '>' . $MSG['MON_003E'] . '</option>
		<option value="04"' . (($TPL_month == '04') ? ' selected' : '') . '>' . $MSG['MON_004E'] . '</option>
		<option value="05"' . (($TPL_month == '05') ? ' selected' : '') . '>' . $MSG['MON_005E'] . '</option>
		<option value="06"' . (($TPL_month == '06') ? ' selected' : '') . '>' . $MSG['MON_006E'] . '</option>
		<option value="07"' . (($TPL_month == '07') ? ' selected' : '') . '>' . $MSG['MON_007E'] . '</option>
		<option value="08"' . (($TPL_month == '08') ? ' selected' : '') . '>' . $MSG['MON_008E'] . '</option>
		<option value="09"' . (($TPL_month == '09') ? ' selected' : '') . '>' . $MSG['MON_009E'] . '</option>
		<option value="10"' . (($TPL_month == '10') ? ' selected' : '') . '>' . $MSG['MON_010E'] . '</option>
		<option value="11"' . (($TPL_month == '11') ? ' selected' : '') . '>' . $MSG['MON_011E'] . '</option>
		<option value="12"' . (($TPL_month == '12') ? ' selected' : '') . '>' . $MSG['MON_012E'] . '</option>
	</select>';
$dobday = '<select name="TPL_day">
		<option value=""></option>';
for ($i = 1; $i <= 31; $i++)
{
	$j = (strlen($i) == 1) ? '0' . $i : $i;
	$dobday .= '<option value="' . $j . '"' . (($TPL_day == $j) ? ' selected' : '') . '>' . $j . '</option>';
}
$dobday .= '</select>';

$selectsetting = $USER['timecorrection'];
$time_correction = generateSelect('TPL_timezone', $TIMECORRECTION);

///Facebook check to see if the account is link to facebook
$query = "SELECT fblogin_id FROM " . $DBPrefix . "users WHERE id = :user_id"; 
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$db->query($query, $params);
$user_fb = $db->result('fblogin_id');

$id_checked = $_SESSION['FBOOK_USER_IDS'];
$id_checked = $user_fb;

$query = "SELECT fb_id FROM " . $DBPrefix . "fblogin WHERE fb_id = :check_id";
$params = array();
$params[] = array(':check_id', $id_checked, 'int');
$db->query($query, $params);
$fb_id_checked = $db->result('fb_id');
$check_wb_fb = $user_fb == $fb_id_checked;

if ($check_wb_fb)
{
	$fbconnected = '<img src="images/longin1.png">' . $MSG['350_10190'] . '<br><br>' . '<a class="btn btn-primary" href="edit_data.php?fbconnect=unlinked">Removed</a>';

}	
elseif ((isset($_SESSION['FBOOK_USER_IDS'])) && $user_fb == '0')
{
	$fbconnected = '<img src="' . $system->SETTINGS['siteurl'] . 'images/loadingAnimation.gif" style="float: left">&nbsp;' . $MSG['350_10203'] . '<br><br><span style="float:left"><button class="btn btn-primary" type="submit" name="Input">' . $MSG['530'] . '</button><input type="hidden" name="action" value="update"></span>';
	
}
elseif ($user_fb == '0')
{
	$fbconnected = '<img src="' . $system->SETTINGS['siteurl'] . 'images/redlogin1.png">' . $MSG['350_10191'] . '<br><br>' . '<a class="btn btn-primary" href="#" onclick="EditFBLogin();">' . $MSG['350_10204_c'] . '</a>';
}

// User Online Status
$loggedtime = $system->ctime - 300; // 5 min

$query = "SELECT is_online FROM " . $DBPrefix . "users WHERE id = :user_id"; 
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$db->query($query, $params);
 
while ($onlinecheck = $db->result()) 
{ 

    if($onlinecheck['is_online'] >  $loggedtime) 
    { 
    $online = '<img src="' . $system->SETTINGS['siteurl'] . 'images/online.png">' . $MSG['350_10111'];
    } 
    else { 
    $online = '<img src="' . $system->SETTINGS['siteurl'] . 'images/offline.png">' . $MSG['350_10112'];
    }     
} 

$template->assign_vars(array(
		'COUNTRYLIST' => $country,
		'NAME' => $USER['name'],
		'HIDEONLINE' => ($USER['hide_online'] == 'y') ? ' checked="checked"' : '',
		'IS_ONLINE' => $online,
		'NICK' => $USER['nick'],
		'EMAIL' => $USER['email'],
		'YEAR' => $TPL_year,
		'ADDRESS' => $USER['address'],
		'CITY' => $USER['city'],
		'PROV' => $USER['prov'],
		'ZIP' => $USER['zip'],
		'PHONE' => $USER['phone'],
		'DATEFORMAT' => ($system->SETTINGS['datesformat'] == 'USA') ? $dobmonth . ' ' . $dobday : $dobday . ' ' . $dobmonth,
		'TIMEZONE' => $time_correction,

		//payment stuff
		'PP_EMAIL' => $USER['paypal_email'],
		'AN_ID' => $USER['authnet_id'],
		'AN_PASS' => $USER['authnet_pass'],
		'WP_ID' => $USER['worldpay_id'],
		'TC_ID' => $USER['toocheckout_id'],
		'MB_EMAIL' => $USER['skrill_email'],
		'BANK' => $USER['bank_name'], 
        'BANK_ACCOUNT' => $USER['bank_account'], 
        'BANK_ROUTING' => $USER['bank_routing'],
        'MAXUPLOADSIZE' =>$system->SETTINGS['maxuploadsize'],

		'NLETTER1' => ($USER['nletter'] == 1) ? ' checked="checked"' : '',
		'NLETTER2' => ($USER['nletter'] == 2) ? ' checked="checked"' : '',
		'EMAILTYPE1' => ($USER['emailtype'] == 'html') ? ' checked="checked"' : '',
		'EMAILTYPE2' => ($USER['emailtype'] == 'text') ? ' checked="checked"' : '',
		
		'B_FB_LINK' => 'FBRegist',
		'B_NEWLETTER' => ($system->SETTINGS['newsletter'] == 1),
		'B_PAYPAL' => ($gateway_data['paypal_active'] == 1),
		'B_AUTHNET' => ($gateway_data['authnet_active'] == 1),
		'B_WORLDPAY' => ($gateway_data['worldpay_active'] == 1),
		'B_TOOCHECKOUT' => ($gateway_data['toocheckout_active'] == 1),
		'B_MONEYBOOKERS' => ($gateway_data['skrill_active'] == 1),
		'B_BANK_TRANSFER' => ($gateway_data['bank_active'] == 1),
		'FBOOK_EMAIL' => $fbconnected,
		'FBOOK_ID' => (isset($_SESSION['FBOOK_USER_IDS'])) ? $_SESSION['FBOOK_USER_IDS'] : ''
		));
		
$query = "SELECT avatar FROM " . $DBPrefix . "users WHERE id = " . $user->user_data['id'];
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$db->query($query, $params);

$TPL_avatar = $db->result('avatar');
$template->assign_vars(array(
'AVATAR' => $TPL_avatar,
));

$TMP_usmenutitle = $MSG['509'];
include 'header.php';
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'edit_data.tpl'
		));
$template->display('body');
include 'footer.php';