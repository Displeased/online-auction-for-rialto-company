<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-cogs"></i> {L_3500_1015624}</span>
    </div>
    <form name="changesettngs" action="" method="post" enctype="multipart/form-data">
    	<input type="hidden" name="action" value="changesettngs">
    	<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
	    <div class="mws-panel-toolbar">
	    	<div class="btn-toolbar">
	            <div class="btn-group">
	            	<input type="submit" name="act" class="btn btn-info" value="{L_089}">
	        	</div>
	   		</div>
	   	</div>
	   	<div class="mws-panel-body no-padding">
        	<table class="mws-table">
        		<tr>
        			<td colspan="3">{L_3500_1015625}</td>
        		</tr>
				<tr>
					<td>{L_3500_1015621}</td>
					<td>{L_560}: <strong>{STATUS}</strong></td>
					<td><input type="checkbox" name="cronlog" value="" {SETTINGS}></td>
				</tr>
			</table>
		</div>
	</form>
</div>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-archive"></i> {L_3500_1015627}</span>
    </div>
    <form name="allcronlogs" action="" method="post" enctype="multipart/form-data">
    	<input type="hidden" name="action" value="clearalllogs">
    	<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
	    <div class="mws-panel-toolbar">
	    	<div class="btn-toolbar">
	            <div class="btn-group">
	            	<input type="submit" name="act" class="btn btn-success" value="{L_3500_1015620}">
	        	</div>
	   		</div>
	   	</div>
	</form>
	<div class="mws-panel-body no-padding">
		<table class="mws-table">
			<tr>
				<td>{L_3500_1015626}</td>
			</tr>
			<tr>
				<td>
					<!-- BEGIN cron_log -->
					<div class="mws-panel grid_4">
						<div class="mws-panel-header">
					    	<span><i class="icon-list-2"></i> {cron_log.PAGENAME}</span>
					    </div>
					    <form name="cronlog" action="" method="post" enctype="multipart/form-data">
					    	<input type="hidden" name="action" value="clearlog">
					        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					        <input type="hidden" name="id" value="{cron_log.ID}">
							<div class="mws-panel-toolbar">
					        	<div class="btn-toolbar">
					            	<div class="btn-group">
					                	<input type="submit" name="act" class="btn btn-primary" value="{L_890}">
					                </div>
					            </div>
					      	</div>
					        <div class="mws-panel-body no-padding">
					        	<table class="mws-table">
					            	<tbody>
					                	<tr>
					                    	<td>{cron_log.ERRORLOG}</td>
					                    </tr>
					               	</tbody>
					           	</table>
					      	</div>
					  	</form>    	
					</div>
					<!-- END cron_log -->
				</td>
			</tr>
		</table>
	</div>
</div>
<!-- INCLUDE footer.tpl -->
