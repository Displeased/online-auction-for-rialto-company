<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-cogs"></i> {L_3500_1015550}</span>
    </div>
	<form name="changesettngs" action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="update">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
    	<div class="mws-panel-toolbar">
	    	<div class="btn-toolbar">
	            <div class="btn-group">
	            	<input type="submit" name="act" class="btn btn-success" value="{L_530}">
	            	<button class="btn btn-info" onclick="showDialog();return false;">{L_3500_1015568}</button> 
	        	</div>
	   		</div>
	   	</div>
		<div class="mws-panel-body no-padding">
        	<table class="mws-table">
				<!-- BEGIN block -->
				<tr valign="top">
  					<!-- IF block.B_HEADER -->
					<td colspan="2" style="padding:3px; border-top:#0083D7 1px solid; background:#ECECEC;"><b>{block.TITLE}</b></td>
  					<!-- ELSE -->
					<td width="175">{block.TITLE}</td>
					<td style="padding:3px;">{block.DESCRIPTION}
						<!-- IF block.TYPE eq 'yesno' -->
						<input type="radio" name="{block.NAME}" value="y"<!-- IF block.DEFAULT eq 'y' --> checked<!-- ENDIF -->> {block.TAGLINE1}
						<input type="radio" name="{block.NAME}" value="n"<!-- IF block.DEFAULT eq 'n' --> checked<!-- ENDIF -->> {block.TAGLINE2}
						<!-- ELSEIF block.TYPE eq 'text' -->
						<input type="text" name="{block.NAME}" value="{block.DEFAULT}" size="50" maxlength="255">
						<!-- ELSE -->
						{block.TYPE}
						<!-- ENDIF -->
					</td>
 				 	<!-- ENDIF -->
				</tr>
				<!-- END block -->
			</table>
		</div>
	</form>
</div>


<div id="dialog-modal" title="{L_3500_1015565}" style="display: none;">
	<div class="rounded-top rounded-bottom">
		{L_3500_1015687}:<br><br>
		<b>{L_3500_1015551}</b> = {MAIL_PROTOCOL}<br>
		<b>{L_3500_1015560}</b> = {SMTP_AUTH}<br>
		<b>{L_3500_1015559}</b> = {SMTP_SEC}<br>
		<b>{L_3500_1015558}</b> = {SMTP_PORT}<br>
		<b>{L_3500_1015556}</b> = {SMTP_USER}<br>
		<b>{L_3500_1015557}</b> = {SMTP_PASS}<br>
		<b>{L_3500_1015554}</b> = {SMTP_HOST}<br>
		<b>{L_3500_1015561}</b> = {ALERT_EMAILS}<br>
		{L_3500_1015784} <button class="btn btn-success" onclick="$('form[name=conf]').submit();">{L_530}</button>
	</div>
	{L_3500_1015566}
	<div class="test_m">hi</div>
	<div class="form-style" id="contact_form">
		<p><button class="test_button btn btn-primary" onclick="showDialog();" style="button">{L_3500_1015567}</button></p>
		<div id="contact_results"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
$(document).on('click', '.test_button',  function(e) {
	e.preventDefault();
	if ($('#text_testmail').val() == '')
	alert('Empty messages cause errors!');
	        post_data = {
                'user_name'     : '{L_110}', 
                'user_email'    : '{ADMIN_EMAIL}', 
                'subject'       : '{L_3500_1015569}', 
                'message'       : $('#text_testmail').val(),
				'admincsrftoken'     : $('input[name=admincsrftoken]').val()
			};
            //Ajax post data to server
            $.post('emailsettings.php?test_email', post_data, function(response){  
                if(response.type == 'error'){ //load json data from server and output message     
                    output = '<div class="error-box">'+response.text+'</div>';
                }else{
                    output = '<div class="success-box">'+response.text+'</div>';
                 }
                $("#contact_form #contact_results").hide().html(output).slideDown();
			 }, 'json');
   });
}); 
function showDialog()
{
    $("#dialog-modal").dialog(
    {
        width: 600,
        height: 550,
		buttons: {
        "{L_3500_1015571}": function() {
            $(this).dialog("close");
         }
		},
        open: function(event, ui)
        {
            var textarea = $('<input type="textarea" id="text_testmail" name="text_testmail" style="height: 50px; width:90%;">');
            $('.test_m').html(textarea);
        }
     });
}
$(document).ready(function() {
	if ($('select[name=mail_protocol] option:selected').val() == 2) {
		$('.smtp').parent().parent().show();
		$('.non_smtp').parent().parent().hide();
	} else {
		$('.smtp').parent().parent().hide();
		$('.non_smtp').parent().parent().show();
	}
	if ($('select[name=mail_protocol] option:selected').val() == 0) {
		$('.para').parent().parent().show();
	} else {
		$('.para').parent().parent().hide();
    }
	
	$('select[name=mail_protocol]').on('change', function() {
	//alert('changid');
		if ($(this).val() == 2) {
			$('.smtp').parent().parent().show(300);
			$('.non_smtp').parent().parent().hide();
		} else {
			$('.smtp').parent().parent().hide();
			$('.non_smtp').parent().parent().show(300);
			}
		if ($(this).val() == 0) {
			$('.para').parent().parent().show(300);
		} else {
			$('.para').parent().parent().hide();
			}	
	});
});
</script>
<!-- INCLUDE footer.tpl -->