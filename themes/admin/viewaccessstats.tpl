<script>
		$(function() {
			$(".meter > span").each(function() {
				$(this)
					.data("origWidth", $(this).width())
					.width(0)
					.animate({
						width: $(this).data("origWidth")
					}, 1200);
			});
		});
	</script>

<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-calculate"></i> {L_5164}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        	<thead>
                            	<tr>
                                		<th>{L_5161}</th>
                                        <th>{L_5162}</th>
                                        <th>{L_5163}</th>
                               </tr>
                            </thead>
                            <tbody>
                            <tr>
                                    <td class="meter nostripes"><span style="width: 100%"><span style="margin-left:20px"><b>{TOTAL_PAGEVIEWS}</b></span></span></td>
                                    <td class="meter red nostripes"><span style="width: 100%"><span style="margin-left:20px"><b>{TOTAL_UNIQUEVISITORS}</b></span></span></td>
                                    <td class="meter orange nostripes"><span style="width: 100%"><span style="margin-left:20px"><b>{TOTAL_USERSESSIONS}</b></span></span></td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                </div>
                
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-pie-chart"></i> {L_5158}<i>{SITENAME}</i> 
{STATSMONTH}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                    <table class="mws-table">
                <tr>
                    <td align="center"><b>{STATSTEXT}</b></td>
                    <td style="text-align:right;">{L_829}<a href="viewaccessstats.php?type=d">{L_109}</a>/ <a href="viewaccessstats.php?type=w">{L_828}</a>/ <a href="viewaccessstats.php?type=m">{L_830}</a></td>
                </tr>
<!-- BEGIN sitestats -->
                <tr class="bg">
                    <td align="center" height="45"><b>{sitestats.DATE}</b></td>
                    <td>
	<!-- IF sitestats.PAGEVIEWS eq 0 -->
						<div style="height:15px;"><b>0</b></div>
	<!-- ELSE -->
						<div class="meter nostripes">
							<span style="width:{sitestats.PAGEVIEWS_WIDTH}%"><span style="margin-left:20px"><b>{sitestats.PAGEVIEWS}</b></span></span>
						</div>
	<!-- ENDIF -->
	<!-- IF sitestats.UNIQUEVISITORS eq 0 -->
						<div style="height:15px;"><b>0</b></div>
	<!-- ELSE -->
						<div class="meter red nostripes">
							<span style="width:{sitestats.UNIQUEVISITORS_WIDTH}%"><span style="margin-left:20px"><b>{sitestats.UNIQUEVISITORS}</b></span></span>
						</div>
	<!-- ENDIF -->
	<!-- IF sitestats.USERSESSIONS eq 0 -->
						<div style="height:15px;"><b>0</b></div>
	<!-- ELSE -->
						<div class="meter orange nostripes">
							<span style="width:{sitestats.USERSESSIONS_WIDTH}%"><span style="margin-left:20px"><b>{sitestats.USERSESSIONS}</b></span></span>
						</div>
	<!-- ENDIF -->
                    </td>
                </tr>
<!-- END sitestats -->
				</table>
  				</div>
                </div>
                
<!-- INCLUDE footer.tpl -->