<!-- INCLUDE header.tpl -->
<script type="text/javascript">
$(document).ready(function() {
	$("#deleteall").click(function() {
		var checked_status = this.checked;
        $("input[id=delete]").each(function()
        { 
        	this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
        });	
	});
});
</script>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icol-pencil"></i> {L_3500_1015633}</span>
   	</div>
    <form name="mascat" action="" method="post">
    	<input type="hidden" name="parent" value="{PARENT}">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="action" class="btn btn-success" value="{L_518}">
               </div>
           	</div>
      	</div>
		<div class="mws-panel-body no-padding">
        	<table class="mws-table">
            	<thead>
                    <tr>
                    	<th>&nbsp;</th>
                        <th>{L_087}</th>
                        <th>{L_328}</th>
                        <th>{L_329}</th>
                  	</tr>
              	</thead>
                <tbody>
                	<tr>
                    	<td>{L_394}</td>
                        <td><input type="text" name="new_category" size="25"></td>
                        <td><input type="text" name="cat_colour" size="25"></td>
                        <td><input type="text" name="cat_image" size="25"></td>
                   	</tr>
                    <tr>
                    	<td>&nbsp;</td>
                        <td>{L_368}</td>
                        <td colspan="3"><textarea name="mass_add" cols="55" rows="6"></textarea></td>       
                   	</tr>
				</tbody>
			</table>
		</div>
  	</form>    	
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icol-application-view-list"></i> {PAGENAME}</span>
   	</div>
    <form name="newcat" action="" method="post">
    	<input type="hidden" name="parent" value="{PARENT}">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="action" class="btn btn-success" value="{L_089}">
               </div>
           	</div>
      	</div>
        <div class="mws-panel-body no-padding">
        	<table class="mws-table">
            	<thead>
                	<tr>
                    	<th colspan="5">{L_845}</th>
                    </tr>
                    <tr>
                    	<th>&nbsp;</th>
                        <th>{L_087}</th>
                        <th>{L_328}</th>
                        <th>{L_329}</th>
                        <th>{L_008}</th>
                  	</tr>
              	</thead>
                <tbody>
                    <tr>
                    	<td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>{L_30_0102}</td>
                        <td><input type="checkbox" id="deleteall"></td>
                   	</tr>
                    <!-- BEGIN cats -->
                    <tr>
                   		<td><a href="categories.php?parent={cats.CAT_ID}"><img src="{SITEURL}images/plus.gif" border="0" alt="Browse Subcategories"></a></td>
                        <td><input type="text" name="categories[{cats.CAT_ID}]" value="{cats.CAT_NAME}" size="33"></td>
                        <td><input type="text" name="colour[{cats.CAT_ID}]" value="{cats.CAT_COLOUR}"></td>
                        <td><input type="text" name="image[{cats.CAT_ID}]" value="{cats.CAT_IMAGE}"></td>
                        <td>
	                        <input type="checkbox" name="delete[]" value="{cats.CAT_ID}" id="delete">
	    					<!-- IF cats.B_SUBCATS -->
	                        <img src="{SITEURL}themes/admin/images/bullet_blue.png">
	    					<!-- ENDIF -->
	    					<!-- IF cats.B_AUCTIONS -->
	                        <img src="{SITEURL}themes/admin/images/bullet_red.png">
	    					<!-- ENDIF -->
						</td>
                  	</tr>
                    <!-- END cats -->
            	</tbody>
          	</table>
     	</div>
  	</form>    	
</div>
<!-- INCLUDE footer.tpl -->