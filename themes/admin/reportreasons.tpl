<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="payments" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>{L_1405}</th>
                                    <th colspan="3">{L_1411}</th>
                                    <th>{L_008}</th>
                                </tr>
                            </thead>
                            <tbody>
                           
                           <tr>
                           			<td>{L_518} 
                            		<input type="text" name="new_report_reason[]" size="55" maxlength="55"></td>
                         			<!-- <td><input type="text" name="new_report_class[]" size="2" maxlength="2"></td>-->

                           			<td>
                                    	<input type="radio" name="new_report_class[]" value="1" checked="checked" > {L_029}
									</td>
									<td>  
										<input type="radio" name="new_report_class[]"  value="2" > {L_030}
									</td>
									<td> 
										<input type="radio" name="new_report_class[]"  value="3" > {L_1436}
									</td>
                                    <td>&nbsp;</td>
                             </tr>
                             <tr>
                            		<td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>{L_30_0102}</td>
                                    <td><input type="checkbox" class="selectall" value="delete"></td>
                           </tr>
                           </tbody>
                            <tbody>
                            <!-- BEGIN reasons -->
                            <tr>
                                    <td><input type="text" name="new_report_reason[{reasons.ID}]" value="{reasons.REPORT_REASON}" size="55"></td>
									<!-- <td><input type="text" name="new_report_class[{reasons.ID}]" value="{reasons.REPORT_CLASS}" size="2"></td>-->
                                	<td>
                                    	<input type="radio" name="new_report_class[{reasons.ID}]" value="1" {reasons.REPORT_CLASS1}> {L_029}
									</td>
                                    <td>
                                    	<input type="radio" name="new_report_class[{reasons.ID}]"  value="2" {reasons.REPORT_CLASS2}> {L_030}
                                    </td>
                                    <td>
                                    	<input type="radio" name="new_report_class[{reasons.ID}]"  value="3" {reasons.REPORT_CLASS3}> {L_1436}
									</td>
                                    <td><input type="checkbox" name="delete[]" value="{reasons.ID}"></td>
                           </tr>
                           <!-- END reasons -->
                           </tbody>
                           
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->