<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="id" value="{ID}">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <!-- BEGIN flangs -->
                            <tr>
                            		<!-- IF flangs.S_FIRST_ROW -->
                            		<td>{L_5284}</td>
                                    <!-- ELSE -->
                                    <td>&nbsp;</td>
                                    <!-- ENDIF -->
                                    <td><img src="{SITEURL}inc/flags/{flangs.LANGUAGE}.gif"></td>
                                    <td><input type="text" name="category[{flangs.LANGUAGE}]" size="50" maxlength="150" value="{flangs.TRANSLATION}"></td>
                            </tr>
                            <!-- END langs -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
