<!-- INCLUDE header.tpl -->
				<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {mod.PAGE}</span>
                    </div>
                    <form name="banips" action="" method="post">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                    <input type="hidden" name="offset" value="{OFFSET}">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="id" value="{ID}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_2_0015}">
                            <a href="listusers.php?offset={OFFSET}" class="btn btn-danger">{L_5279}</a>
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        <thead>
                        		<tr>
                                	<th colspan="4">
                                    	{L_5117}&nbsp;{PAGE}&nbsp;{L_5118}&nbsp;{PAGES}
                                        <br>
                                        {PREV}
            							<!-- BEGIN pages -->
                                        {pages.PAGE}&nbsp;&nbsp;
            							<!-- END pages -->
                                        {NEXT}
                                    </th>
                                </tr>
                        		<tr>
                                	<th colspan="3">{L_667} <b>{NICK}</b></th>
                                    <th align="right">{L_559}: {LASTLOGIN}</th>
                                </tr>
                                <tr>
                                    <th>{L_087}</th>
                                    <th>{L_2_0009}</th>
                                    <th>{L_560}</th>
                                    <th>{L_5028}</th>
                                </tr>
                            </thead>
                            <tbody>
								<!-- BEGIN ips -->
	                            <tr>
	                            		<td>
                                       	 	<!-- IF ips.TYPE eq 'first' -->
    										{L_2_0005}
    										<!-- ELSE -->
    										{L_221}
    										<!-- ENDIF -->
                                        </td>
	                                    <td>{ips.IP}</td>
                                        <td>
                                        	<!-- IF ips.ACTION eq 'accept' -->
                                            {L_2_0012}
                    						<!-- ELSE -->
                                            {L_2_0013}
                    						<!-- ENDIF -->
                                        </td>
                                        <td>
                                        	<!-- IF ips.ACTION eq 'accept' -->
                            				<input type="checkbox" name="deny[]" value="{ips.ID}">&nbsp;{L_2_0006}
    										<!-- ELSE -->
                            				<input type="checkbox" name="accept[]" value="{ips.ID}">&nbsp;{L_2_0007}
    										<!-- ENDIF -->
                                        </td>
	                            </tr>
	                        </tbody>
	                        <!-- END mod -->
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->