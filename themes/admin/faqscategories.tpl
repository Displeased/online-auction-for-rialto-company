<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="newfaqcat" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            	<!-- IF B_ADDCAT -->
                            	<input type="submit" name="action" class="btn btn-success" value="{L_5204}">
                            	<!-- ELSE -->
                            	<a class="btn btn-success" href="faqscategories.php?do=add">{L_5234}</a>
                            	<!-- ENDIF -->
                            	<input type="submit" name="action" class="btn btn-danger" value="{L_008}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        	<thead>
                                    <!-- IF B_ADDCAT -->
                                <tr>
                                        <th>{L_165}</th>
                                        <th colspan="2">
                                            <!-- BEGIN lang -->
                                            <p>{lang.LANG}:&nbsp;<input type="text" name="cat_name[{lang.LANG}]" size="25" maxlength="200"></p>
                                            <!-- END lang -->
                                        </th>
                                </tr>
                               <!-- ENDIF -->
                                <tr>
                                    <th>{L_5237}</th>
                                    <th>{L_287}</th>
                                    <th>{L_008}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN cats -->
                            <tr>
                            		<td>{cats.ID}</td>
                                    <td>
                                    	<a href="editfaqscategory.php?id={cats.ID}">{cats.CATEGORY}</a> <!-- IF cats.FAQS gt 0 -->{cats.FAQSTXT}<!-- ENDIF -->	
                                    </td>
                                    <td><input type="checkbox" name="delete[]" value="{cats.ID}"></td>
                            </tr>
                            <!-- END cats -->

                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->