<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="payments" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>{L_097}</th>
                                    <th>{L_087}</th>
                                    <th>{L_008}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN dur -->
                            <tr>
                            		<td>&nbsp;</td>
                                    <td>
                                    	<input type="text" name="new_days[{dur.ID}]" value="{dur.DAYS}" size="5"></td>
                                    <td><input type="text" name="new_durations[{dur.ID}]" value="{dur.DESC}" size="25"></td>
                                    <td><input type="checkbox" name="delete[]" value="{dur.ID}"></td>
                            </tr>
                            <!-- END dur -->
                            <tr>
                                	<td>{L_518}</td>
                                    <td><input type="text" name="new_days[]" size="5" maxlength="5"></td>
                                    <td><input type="text" name="new_durations[]" size="25"></td>
                                    <td>&nbsp;</td>
                           </tr>
                           <tr>
                            		<td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>{L_30_0102}</td>
                                    <td><input type="checkbox" class="selectall" value="delete"></td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
