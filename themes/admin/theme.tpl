<!-- INCLUDE header.tpl -->

<!-- IF B_EDIT_FILE -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {L_26_0003}</span>
                    </div>
                    <form name="payments" action="" method="post">
                    <input type="hidden" name="filename" value="{FILENAME}">
                    <input type="hidden" name="theme" value="{THEME}">
                    <input type="hidden" name="action" value="<!-- IF FILENAME ne '' -->edit<!-- ELSE -->add<!-- ENDIF -->">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_812}</td>
                                    <td>
                                    	<!-- IF FILENAME ne '' --><b>{FILENAME}</b><!-- ELSE --><input type="text" name="new_filename" value="" style="width:600px;"><!-- ENDIF -->
                            </tr>
                            <tr>
                                	<td>{L_813}</td>
                                    <td><textarea style="width:600px; height:400px;" name="content">{FILECONTENTS}</textarea>
</td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
           <!-- ENDIF -->
           <div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icon-picture"></i> {L_30_0031a}</span>
                    </div>
                    <form name="logo" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="update_logo">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_3500_1015665}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                                <tr>
                                        <td><img height="120px" src="{IMAGEURL}"></td>
                               </tr>
                               <tr>
                                    <td>{L_602}</td>
                               </tr>
                               <tr>
                               		<td>
                                    <input type="file" name="logo" size="40">
                                </td>
                               </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
           		<div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icol-arrow-refresh"></i> {L_3500_1015466}</span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                    <input type="hidden" name="action" value="clear_cache">
                    <div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_30_0031}">
                            </div>
                        </div>
                    </div>
                    </form>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                                <tr>
                                    <td>{L_30_0032}</td>
                                </tr>      
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-business-card"></i> {L_26_0002}</span>
                    </div>
                    <form name="theme" action="" method="post">
                    <input type="hidden" name="action" value="update" id="action">
                    <input type="hidden" name="theme" value="" id="theme">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="" class="btn btn-success" value="{L_26_0000}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <!-- BEGIN themes -->
                                <tr>
                                     <td>
                                        	<!-- IF themes.B_NOTADMIN -->
                                        	<input type="radio" name="dtheme" value="{themes.NAME}" <!-- IF themes.B_CHECKED -->checked="checked" <!-- ENDIF -->/>
                            				<b>{themes.NAME}</b>
											<!-- ELSE -->
        									<b>{L_841}</b>
											<!-- ENDIF -->
                                    </td>
                                    <td>
                                    	<p><a href="theme.php?do=listfiles&theme={themes.NAME}">{L_26_0003}</a></p>
                            			<p><a href="theme.php?do=addfile&theme={themes.NAME}">{L_26_0004}</a></p>
                                    </td>
                               </tr>
                               <!-- IF themes.B_LISTFILES -->
                               <tr>
                               		<td colspan="2">
                                        <select name="file" multiple size="24" style="font-weight:bold; width:350px"
                                     ondblclick="document.getElementById('action').value = ''; document.getElementById('theme').value = '{themes.NAME}'; this.form.submit();">
                    				<!-- BEGIN files -->
                                    	<option value="{themes.files.FILE}">{themes.files.FILE}</option>
                    				<!-- END files -->
                                    	</select>
                                    </td>
                               </tr>
                               <!-- ENDIF -->
                               <!-- END themes -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
           
<!-- INCLUDE footer.tpl -->
