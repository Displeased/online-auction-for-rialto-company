<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="payments" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>{L_25_0171}</th>
                                    <th>{L_25_0167}</th>
                                    <th>&nbsp;</th>
                                    <th>{L_008}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN mtype -->
                            <tr>
                            		<td>&nbsp;</td>
                                    <td>
                                    	<input type="hidden" name="old_membertypes[{mtype.ID}][feedbacks]" value="{mtype.FEEDBACK}">
                                		<input type="text" name="new_membertypes[{mtype.ID}][feedbacks]" value="{mtype.FEEDBACK}" size="5">
									</td>
                                    <td><input type="hidden" name="old_membertypes[{mtype.ID}][icon]" value="{mtype.ICON}">
                                		<input type="text" name="new_membertypes[{mtype.ID}][icon]" value="{mtype.ICON}" size="25"></td>
                                    <td><img src="../images/icons/{mtype.ICON}" align="middle"></td>
                                    <td><input type="checkbox" name="delete[]" value="{mtype.ID}"></td>
                            </tr>
                            <!-- END mtype -->
                            <tr>
                            		<td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                	<td>{L_30_0102}</td>
                                    <td><input type="checkbox" class="selectall" value="delete"></td>
                           </tr>
                           <tr>
                            		<td>{L_518}</td>
                                    <td><input type="text" name="new_membertype[feedbacks]" size="5"></td>
                                    <td><input type="text" name="new_membertype[icon]" size="30"></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
