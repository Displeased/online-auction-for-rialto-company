<!-- INCLUDE header.tpl -->
<script type="text/javascript">
	$(document).ready(function() {
		$("#download").submit(function() {
			if (confirm('{L_3500_1015736}')){
				return true;
			} 
			else 
			{
				return false;
			}
		});
		$("#upgrade").submit(function() {
			if (confirm('{L_3500_1015737}')){
				return true;
			} 
			else 
			{
				return false;
			}
		});
</script>

<!-- BEGIN upgrade_list -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-info-sign"></i> {upgrade_list.PAGE_NAME}</span>
    </div>
    <form id="download" name="{upgrade_list.NEW_VERSION}" action="" method="post">
    	<input type="hidden" name="action" value="upgrading">
    	<input type="hidden" name="path" value="{upgrade_list.PATH}">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
        <!-- IF upgrade_list.HIDE_BUTTON -->
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="act" class="btn btn-success" value="Download {upgrade_list.NEW_VERSION}"> 
                </div>
           	</div>
        </div>
        <!-- ENDIF -->
	    <div class="mws-panel-body no-padding">
	    	<table class="mws-table">
	        	<tbody>
	            	<tr>
	                	<th width="17%">{L_3500_1015710}</th>
	                    <td>{upgrade_list.TITLE}</td>
	               	</tr>
 	               	<tr>
	                	<th>{L_3500_1015711}</th>
	                    <td>{upgrade_list.INSTALLED_VERSION}</td>
	               	</tr>
					<tr>
	                	<th>{L_3500_1015712}</th>
	                    <td>{upgrade_list.OLD_VERSION}
	                    	<input type="hidden" name="required_version" value="{upgrade_list.OLD_VERSION}"></td>	                    	<input type="hidden" name="old_version" value="{upgrade_list.OLD_VERSION}"></td>
	               	</tr>
	               	<tr>
	                	<th>{L_3500_1015713}</th>
	                    <td>{upgrade_list.NEW_VERSION}
	                    	<input type="hidden" name="new_version" value="{upgrade_list.NEW_VERSION}"></td>
	               	</tr>
	          	</tbody>
	     	</table>
		</div>  	
	</form>
</div>
<!-- END upgrade_list -->
<!-- IF PAGE -->
<form id="upgrade" name="upgrade_details" action="" method="post">
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-info-sign"></i> {L_3500_1015697}</span>
    </div>
    	<input type="hidden" name="action" value="start_upgrading">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="act" class="btn btn-success" value="{L_3500_1015718}"> 
                </div>
           	</div>
        </div>
	    <div class="mws-panel-body no-padding">
	    	<table class="mws-table">
	        	<tbody>
	        		<tr>
	               		<th colspan="2">{L_3500_1015709}</th>
	               	</tr>
	        		<!-- BEGIN upgrade_info-->
	            	<tr>
	                	<th width="17%">{L_3500_1015710}</th>
	                    <td>{upgrade_info.TITLE}</td>
	               	</tr>
 	               	<tr>
	                	<th>{L_3500_1015711}</th>
	                    <td>{upgrade_info.INSTALLED_VERSION}</td>
	               	</tr>
					<tr>
	                	<th>{L_3500_1015712}</th>
	                    <td>{upgrade_info.OLD_VERSION}</td>
	               	</tr>

	               	<tr>
	                	<th>{L_3500_1015713}</th>
	                    <td>{upgrade_info.NEW_VERSION}</td>
	               	</tr>
	               	<tr>
	                	<th>{L_3500_1015726}</th>
	                    <td>{upgrade_info.SCRIPT_BITBUCKET}</td>
	               	</tr>
	               	<tr>
	                	<th>{L_3500_1015731}</th>
	                    <td>{upgrade_info.LANGUAGE_BITBUCKET}</td>
	               	</tr>
	               	<tr>
	               		<th>{L_1063}</th>
	               		<td>{upgrade_info.HELP}</td>
	               	</tr>
	               	<tr>
	               		<th>{L_3500_1015734}</th>
	               		<td>{upgrade_info.EXTRAS}</td>
	               	</tr>

	               	<tr>
	               		<th colspan="2">{L_3500_1015714}</th>
	               	</tr>
	               	<tr>
	                    <td colspan="2">{upgrade_info.DEC}</td>
	               	</tr>
	               	<!-- END upgrade_info-->
	          	</tbody>
	     	</table>
		</div>  	
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-archive"></i> {L_3500_1015715}</span>
    </div>
    <div class="mws-panel-body no-padding">
	    <table class="mws-table">
			<tbody>
				<tr>
					<td>
						<!-- BEGIN upgrade_dir -->
						<div class="mws-panel grid_4">
							<div class="mws-panel-header">
						    	<span><i class="icon-folder-closed"></i> {L_3500_1015716} {upgrade_dir.FOLDER}</span>
						    	<input type="hidden" name="folders[]" value="{upgrade_dir.FOLDER}">
						    </div>
							<div class="mws-panel-body no-padding">
							    <table class="mws-table">
							    	<thead>
							    		<tr>
							    			<th>{L_3500_1015717}</th>
							    		</tr>
							    	</thead>
							        <tbody>
							        	<!-- BEGIN upgrade_pages -->
						 	            <tr>
							                <td>{upgrade_pages.PAGE}
							                <input type="hidden" name="pages[]" value="{upgrade_pages.PAGE}"></td>
							            </tr>
							         	<!-- END upgrade_pages -->
							    	</tbody>
							    </table>
							</div>  	
						</div>
						<!-- END upgrade_dir -->
					</td>
				</tr>
			</tbody>
	    </table>
	</div>  	
</div>
</form>
<!-- ENDIF -->
<!-- INCLUDE footer.tpl -->
