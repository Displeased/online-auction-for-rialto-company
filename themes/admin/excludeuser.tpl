<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {QUESTION}</span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="id" value="{ID}">
                    <input type="hidden" name="offset" value="{OFFSET}">
                    <input type="hidden" name="mode" value="{MODE}">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            	<input type="submit" name="action" class="btn btn-success" value="{L_030}">
                            	<input type="submit" name="action" class="btn btn-warning" value="{L_029}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_302}</td>
                            		<td>{REALNAME}</td>
                            </tr>
                            <tr>
                            		<td>{L_003}</td>
                                	<td>{USERNAME}</td>
                            </tr>
                            <tr>
                                	<td>{L_303}</td>
                                	<td>{EMAIL}</td>
                            </tr>
                            <tr>
                            		<td>{L_252}</td>
                                	<td>{DOB}</td>
                            </tr>
                            <tr>
		                        <td>{L_009}</td>
		                        <td>{ADDRESS}</td>
		                    </tr>
		                    <tr>
		                        <td>{L_011}</td>
		                        <td>{PROV}</td>
		                    </tr>
		                    <tr>
		                        <td>{L_012}</td>
		                        <td>{ZIP}</td>
		                    </tr>
		                    <tr>
		                        <td>{L_014}</td>
		                        <td>{COUNTRY}</td>
		                    </tr>
		                    <tr>
		                        <td>{L_013}</td>
		                        <td>{PHONE}</td>
		                    </tr>
		                    <tr>
		                        <td>{L_222}</td>
		                        <td><p><a href="userfeedback.php?id={ID}">{L_208}</a></p></td>
		                    </tr>
		                  </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>

<!-- INCLUDE footer.tpl -->