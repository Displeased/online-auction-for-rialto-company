<!-- INCLUDE header.tpl -->  
            
            	<!-- Statistics Button Container -->
            	<div class="mws-stat-container clearfix">
                	
                    <!-- Statistic Item -->
                	<a class="mws-stat" href="{SITEURL}{ADMIN_FOLDER}/viewaccessstats.php">
                    	<!-- Statistic Icon (edit to change icon) -->
                    	<span class="mws-stat-icon icol32-folder-explore"></span>
                        
                        <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">{L_5161}</span>
                            <span class="mws-stat-value">{A_PAGEVIEWS}</span>
                        </span>
                    </a>

                	<a class="mws-stat" href="{SITEURL}{ADMIN_FOLDER}/viewaccessstats.php">
                    	<!-- Statistic Icon (edit to change icon) -->
                    	<span class="mws-stat-icon icol32-walk"></span>
                        
                        <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">{L_5162}</span>
                            <span class="mws-stat-value">{A_UVISITS}</span>
                        </span>
                    </a>

                	<a class="mws-stat" href="{SITEURL}{ADMIN_FOLDER}/viewaccessstats.php">
                    	<!-- Statistic Icon (edit to change icon) -->
                    	<span class="mws-stat-icon icol32-user"></span>
                        
                        <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">{L_5163}</span>
                            <span class="mws-stat-value">{A_USESSIONS}</span>
                        </span>
                    </a>
                    
                	<a class="mws-stat imaging" data-fancybox-type="iframe" href="https://u-auctions.com/forum/viewforum.php?f=3">
                    	<!-- Statistic Icon (edit to change icon) -->
                    	<span class="mws-stat-icon <!-- IF THIS_VERSION eq REALVERSION -->icol32-flag-green<!-- ELSE -->icol32-flag-red<!-- ENDIF -->"></span>
                        
                        <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">{L_30_0214}</span>
                            <span class="mws-stat-value">{MYVERSION} ({REALVERSION})</span>
                        </span>
                    </a>
                    
                </div>
                
                <!-- Panels Start -->
                
            	<div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icol-server"></i> {L_25_0025}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                                <tr>
                                    <td><strong>{L_528}</strong></td>
                                    <td>{SITEURL}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_527}</strong></td>
                                    <td>{SITENAME}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_540}</strong></td>
                                    <td>{ADMINMAIL}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_25_0026}</strong></td>
                                    <td>{CRON}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_663}</strong></td>
                                    <td>{GALLERY}</td>
                                </tr>
                                <tr>
                                	<td><strong>{L_3500_1015585}</strong></td>
                                	<td><strong>{CACHE}</strong></td>
                                </tr>
                                <tr>
                                	<td><strong>{L_3500_1015634}</strong></td>
                                	<td><strong>{COOKIE_DIRECTIVE}</strong></td>
                                </tr>

                                <tr>
                                    <td><strong>{L_2__0025}</strong></td>
                                    <td>{BUY_NOW}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_5008}</strong></td>
                                    <td>{CURRENCY}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_25_0035}</strong></td>
                                    <td>{TIMEZONE}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_363}</strong></td>
                                    <td>{DATEFORMAT} <small>({DATEEXAMPLE})</small></td>
                                </tr>
                                <tr>
                                    <td><strong>{L_3500_1015550}</strong></td>
                                    <td>{EMAIL_HANDLER}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_5322}</strong></td>
                                    <td>{DEFULTCONTRY}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_2__0002}</strong></td>
                                    <td>
            <!-- BEGIN langs -->
                                    <p>{langs.LANG}<!-- IF langs.B_DEFAULT --> ({L_2__0005})<!-- ENDIF --></p>
            <!-- END langs -->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icol-chart-pie"></i> {L_25_0025}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                                <tr>
                                    <td><strong>{L_25_0055}</strong></td>
                                    <td>{C_USERS}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_25_0056}</strong></td>
                                    <td>
                                    	<!-- IF USERCONF eq 0 -->
                        				<strong>{L_893}</strong>: {C_IUSERS}<br>
                        				<strong>{L_892}</strong>: {C_UUSERS} (<a href="{SITEURL}{ADMIN_FOLDER}/listusers.php?usersfilter=admin_approve">{L_5295}</a>)
										<!-- ELSE -->
                        				{C_IUSERS}
										<!-- ENDIF -->
									</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_25_0057}</strong></td>
                                    <td>{C_AUCTIONS}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_354}</strong></td>
                                    <td>{C_CLOSED}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_25_0059}</strong></td>
                                    <td>{C_BIDS}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_3500_1015549}</strong></td>
                                    <td>{C_ISOLD}</td>
                                </tr>
                                <tr>
                                    <td><strong>{L_25_0063}</strong></td>
                                    <td>
                                    	<p><strong>{L_5161}</strong>: {A_PAGEVIEWS}</p>
                            			<p><strong>{L_5162}</strong>: {A_UVISITS}</p>
                            			<p><strong>{L_5163}</strong>: {A_USESSIONS}</p>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icol-arrow-refresh"></i> {L_080}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                                <tr>
                                    <td>{L_30_0032}</td>
                                    <td>
                                    	<form action="?action=clearcache" method="post">
                            				<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                                			<input type="submit" name="submit" class="btn btn-success" value="{L_30_0031}">
                            			</form>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{L_1030}</td>
                                    <td>
                                    	<form action="?action=updatecounters" method="post">
                            				<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                            				<input type="submit" name="submit" class="btn btn-success" value="{L_1031}">
                            			</form>
                                    </td>
                                </tr>      
                            </tbody>
                        </table>
                    </div>
                </div>
<!-- INCLUDE footer.tpl -->
