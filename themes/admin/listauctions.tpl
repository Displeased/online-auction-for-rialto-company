<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                            	<tr>
                                	<th colspan="3">{NUM_AUCTIONS} {L_311}<!-- IF B_SEARCHUSER --> {L_934}{USERNAME}<!-- ENDIF --></th>
                                </tr>
                                <tr>
                                	<th colspan="3">
                                    	{L_5117}&nbsp;{PAGE}&nbsp;{L_5118}&nbsp;{PAGES}
                                        <br>
                                        {PREV}
            							<!-- BEGIN pages -->
                                        {pages.PAGE}&nbsp;&nbsp;
            							<!-- END pages -->
                                        {NEXT}
                                    </th>
                                </tr>
                                <tr>
                                    <th>{L_017}</th>
                                    <th>{L_557}</th>
                                    <th>{L_297}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN auctions -->
                            <tr>
                            		<td>
                                    	<!-- IF auctions.SUSPENDED eq 1 -->
                        				<span style="color:#FF0000">{auctions.TITLE}</span>
                        				<!-- ELSE -->
                        				{auctions.TITLE}
                        				<!-- ENDIF -->
                        				<p>[ <a href="{SITEURL}products/{auctions.SEO_TITLE}-{auctions.ID}" target="_blank">{L_5295}</a> ]</p>
                        			</td>
                                    <td>
                                        <b>{L_003}:</b> {auctions.USERNAME}<br>
                                        <b>{L_625}:</b> {auctions.START_TIME}<br>
                                        <b>{L_626}:</b> {auctions.END_TIME}<br>
                                        <b>{L_041}:</b> {auctions.CATEGORY}
                                    </td>
                                    <td>
                                        <a href="editauction.php?id={auctions.ID}&offset={PAGE}">{L_298}</a><br>
                                        <a href="deleteauction.php?id={auctions.ID}&offset={PAGE}">{L_008}</a><br>
                                        <a href="excludeauction.php?id={auctions.ID}&offset={PAGE}">
                                    <!-- IF auctions.SUSPENDED eq 0 -->
                                        {L_300}
                                    <!-- ELSE -->
                                        {L_310}
                                    <!-- ENDIF -->
                                        </a>
                                    <!-- IF auctions.B_HASWINNERS -->
                                        <br><a href="viewwinners.php?id={auctions.ID}&offset={PAGE}">{L__0163}</a>
                                    <!-- ENDIF -->
                                    </td>

                            </tr>
                            <!-- END auctions -->
                           </tbody>
                        </table>
                    </div>  	
                </div>
<!-- INCLUDE footer.tpl -->
