<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-tools"></i> {PAGENAME}</span>
    </div>
    <form name="errorlog" action="" method="post">
    	<input type="hidden" name="action" value="clearlog">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="act" class="btn btn-success" value="{L_890}">
                </div>
            </div>
      	</div>
        <div class="mws-panel-body no-padding">
        	<table class="mws-table">
            	<tbody>
                	<tr>
                    	<td>{ERRORLOG}</td>
                    </tr>
               	</tbody>
           	</table>
      	</div>
  	</form>    	
</div>
<!-- INCLUDE footer.tpl -->
