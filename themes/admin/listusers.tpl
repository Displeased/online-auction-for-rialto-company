<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icon-business-card"></i> {L_5024}</span>
                    </div>
                    <form name="search" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="submit" class="btn btn-success" value="{L_5023}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_5022}</td>
                            		<td><input type="text" name="keyword" size="25"></td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
                <div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {L__0052}</span>
                    </div>
                    <form name="filter" id="filter" action="" method="get">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" class="btn btn-success" value="{L_5029}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_1014}</td>
                            		<td><select name="usersfilter" id="userfilter">
                                    <option value="all">{L_5296}</option>
                                    <option value="active" <!-- IF USERFILTER eq 'active' -->selected<!-- ENDIF -->>{L_5291}</option>
                                    <option value="admin" <!-- IF USERFILTER eq 'admin' -->selected<!-- ENDIF -->>{L_5294}</option>
                                    <option value="fee" <!-- IF USERFILTER eq 'fee' -->selected<!-- ENDIF -->>{L_5293}</option>
                                    <option value="confirmed" <!-- IF USERFILTER eq 'confirmed' -->selected<!-- ENDIF -->>{L_5292}</option>
                                    <option value="admin_approve" <!-- IF USERFILTER eq 'admin_approve' -->selected<!-- ENDIF -->>{L_25_0136}</option>
                                </select></td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-user"></i> {PAGENAME}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                            	<tr>
                                	<th colspan="8">
                                    	{L_5117}&nbsp;{PAGE}&nbsp;{L_5118}&nbsp;{PAGES}
                                        <br>
                                        {PREV}
            						<!-- BEGIN pages -->
                                        {pages.PAGE}&nbsp;&nbsp;
            						<!-- END pages -->
                                        {NEXT}
                            		</th>
                                </tr>
                                <tr>
                                    <th>{L_293}</th>
                                    <th>{L_294}</th>
                                    <th>{L_295}</th>
                                    <th>{L_296}</th>
                                    <th>{L_25_0079}</th>
                                    <th>{L_763}</th>
                                    <th>{L_560}</th>
                                    <th>{L_297}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN users -->
                            <tr>
                       		  	<td>
                       		  		<b>{users.NICK}</b><br>
                       		  		<a href="listauctions.php?uid={users.ID}&offset={PAGE}"><small>{L_5094}</small></a><br />
                            		<a href="userfeedback.php?id={users.ID}&offset={PAGE}"><small>{L_503}</small></a><br />
                            		<a href="viewuserips.php?id={users.ID}&offset={PAGE}"><small>{L_2_0004}</small></a>
                       		  	</td>
                                <td>{users.NAME}</td>
                                <td>{users.COUNTRY}</td>
                                <td><a href="mailto:{users.EMAIL}">{users.EMAIL}</a></td>
                                <td>{users.NEWSLETTER}</td>
                              	<td>{users.BALANCE}
                        		<!-- IF users.BALANCE_CLEAN lt 0 -->
                        		<form name="payreminder" action="" method="post" enctype="multipart/form-data">
                    				<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                    				<input type="hidden" name="payreminder_email" value="send">
                    				<input type="hidden" name="user_id" value="{users.ID}">
                    				<div class="mws-button-row">
                    					<input type="submit" value="{L_764}" class="btn btn-danger">
                    				</div>
								</form>
                                <!-- ENDIF -->
							  	</td>
                                <td>
                                    <!-- IF users.SUSPENDED eq 0 -->
                                        <b><span style="color:green;">{L_5291}</span></b>
                					<!-- ELSEIF users.SUSPENDED eq 1 -->
                                        <b><span style="color:violet;">{L_5294}</span></b>
                					<!-- ELSEIF users.SUSPENDED eq 7 -->
                                        <b><span style="color:red;">{L_5297}</span></b>
                					<!-- ELSEIF users.SUSPENDED eq 8 -->
                                        <b><span style="color:orange;">{L_5292}</span></b><br>
                                        <form name="resend" action="" method="post" enctype="multipart/form-data">
		                    				<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		                    				<input type="hidden" name="resend_email" value="send">
		                    				<input type="hidden" name="user_id" value="{users.ID}">
		                    				<div class="mws-button-row">
		                    					<input type="submit" value="{L_25_0074}" class="btn btn-danger">
		                    				</div>
										</form>
                					<!-- ELSEIF users.SUSPENDED eq 9 -->
                                        <b><span style="color:red;">{L_5293}</span></b>
                					<!-- ELSEIF users.SUSPENDED eq 10 -->
                                        <b><small style="color:orange;"><a href="excludeuser.php?id={users.ID}&offset={PAGE}">{L_25_0136}</a></small></b>
                					<!-- ENDIF -->
                				</td>
                                <td>
                                    	<a href="edituser.php?userid={users.ID}&offset={PAGE}"><small>{L_298}</small></a><br />
                            			<a href="deleteuser.php?id={users.ID}&offset={PAGE}"><small>{L_008}</small></a><br />
                            			<a href="excludeuser.php?id={users.ID}&offset={PAGE}"><small>
			    						<!-- IF users.SUSPENDED eq 0 -->
			                                {L_305}
			    						<!-- ELSEIF users.SUSPENDED eq 8 -->
			                                {L_515}
			    						<!-- ELSE -->
			                                {L_299}
			    						<!-- ENDIF -->
			                    	</small></a>
                                </td>
                            </tr>
                           <!-- END users -->
                           </tbody>
                        </table>
                    </div>   	
                </div>
<!-- INCLUDE footer.tpl -->