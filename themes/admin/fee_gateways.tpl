<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-network"></i> {PAGENAME}</span>
                    </div>
                    <form name="errorlog" action="" method="post">
					<input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <input type="submit" name="act" class="btn btn-success" value="{L_530}"> 
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        <!-- BEGIN gateways -->
                            <thead>
                                <tr>
                                	<th colspan="2">
                                        {gateways.NAME}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <p><!-- IF gateways.B_BANK_NAME -->{gateways.BANK_NAME}<!-- ELSE --><a href="{gateways.WEBSITE}" target="_blank">{gateways.ADDRESS_NAME}<!-- ENDIF --></a>:<br><input type="text" name="<!-- IF gateways.B_BANK_NAME -->{gateways.PLAIN_NAME}_name<!-- ELSE -->{gateways.PLAIN_NAME}_address<!-- ENDIF -->" value="<!-- IF gateways.B_BANK_NAME -->{gateways.BANK_NAME2}<!-- ELSE -->{gateways.ADDRESS}<!-- ENDIF -->" size="50"></p>
            <!-- IF gateways.B_PASSWORD -->
                                        <p>{gateways.ADDRESS_PASS}:<br><input type="text" name="{gateways.PLAIN_NAME}_password" value="{gateways.PASSWORD}" size="50"></p>
            <!-- ENDIF -->
            <!-- IF gateways.B_BANK_ACCOUNT -->
                                            <p>{gateways.BANK_ACCOUNT}:<br><input type="text" name="{gateways.PLAIN_NAME}_account" value="{gateways.BANK_ACCOUNT2}" size="50"></p>
                                            <p>{gateways.BANK_ROUTING}:<br><input type="text" name="{gateways.PLAIN_NAME}_routing" value="{gateways.BANK_ROUTING2}" size="50"></p>
            <!-- ENDIF -->
                                    </td>
                                    <td>
                                        <p><input type="checkbox" name="{gateways.PLAIN_NAME}_required"{gateways.REQUIRED}> {L_446}</p>
                                        <p><input type="checkbox" name="{gateways.PLAIN_NAME}_active"{gateways.ENABLED}> {L_447}</p>
                                    </td>
                                </tr>
                            </tbody>
                           <!-- END gateways -->
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->