<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="wordlist" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_530}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>&nbsp;</td>
                                    <td>{L_5069}</td>
                            </tr>
                            <tr>
                                	<td>{L_5070}</td>
                                    <td>
                                    	<input type="radio" name="wordsfilter" value="y"{WFYES}> {L_030}
                            			<input type="radio" name="wordsfilter" value="n"{WFNO}> {L_029}
                            		</td>
                           </tr>
                           <tr>
                            		<td>{L_5071}</td>
                                    <td>{L_5072}<br>
                            			<textarea name="filtervalues" cols="45" rows="15">{WORDLIST}</textarea>
									</td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
