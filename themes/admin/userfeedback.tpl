<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i>{NICK} {PAGENAME}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                    	{L_5117}&nbsp;{PAGE}&nbsp;{L_5118}&nbsp;{PAGES}
			                            <br>
			                            {PREV}
									<!-- BEGIN pages -->
			                            {pages.PAGE}&nbsp;&nbsp;
									<!-- END pages -->
			                            {NEXT}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN feedback -->
                                <tr>
                                    <td>
                                    	<img align="middle" src="{SITEURL}images/{feedback.FB_TYPE}.png">&nbsp;&nbsp;<b>{feedback.FB_FROM}</b>&nbsp;&nbsp;<span class="small">({L_506}{feedback.FB_TIME})</span>
                       	 				<p>{feedback.FB_MSG}</p>
									</td>
                                    <td><a href="edituserfeed.php?id={feedback.FB_ID}">{L_298}</a> | <a href="deleteuserfeed.php?id={feedback.FB_ID}&user={ID}">{L_008}</a></td>
                                </tr>
                           <!-- END feedback -->
                           </tbody>
                        </table>
                    </div>  	
                </div>
<!-- INCLUDE footer.tpl -->
