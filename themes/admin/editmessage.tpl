<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-tools"></i> {PAGENAME}</span>
                    </div>
                    <form name="editmessage" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="id" value="{BOARD_ID}">
                    <input type="hidden" name="msg" value="{MSG_ID}">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            	<input type="submit" name="act" class="btn btn-success" value="{L_530}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
	                            <tr>
		                            <td width="24%" valign="top">{L_5059}</td>
		                            <td>
		                                <textarea rows="8" cols="40" name="message">{MESSAGE}</textarea>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>{L_5060}</td>
		                            <td>{USER} - {POSTED}</td>
		                        </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
