<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-table"></i> {L_3500_1015773}</span>
    </div>
    <form name="newMineType" action="" method="post">
    	<input type="hidden" name="action" value="new">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="act" class="btn btn-success" value="{L_518}">
                </div>
            </div>
       	</div>
        <div class="mws-panel-body no-padding">
        	<table class="mws-table">
            	<tbody>
            		<tr>
            			<td align="center" colspan="4">{L_3500_1015779}</td>
            		</tr>
                	<tr>
                    	<th>{L_302}</th>
                        <th>{L_3500_1015771}</th>
                        <th>{L_3500_1015772}</th>
                        <th>{L_3500_1015777}</th>
                    </tr>
                    <tr>
                    	<td><input type="text" size="45%" name="name" value=""></td>
                        <td><input type="text" size="44%" name="mine" value=""></td>
                        <td><input type="text" size="10%" name="extension" value=""></td>
                        <td><input type="radio" name="used" value="y">{L_030}<br>
                        <input type="radio" name="used" value="n" checked="checked">{L_029}</td>
                    </tr>
               	</tbody>
        	</table>
    	</div>
   	</form>    	
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-table"></i> {PAGENAME}</span>
    </div>
    <form name="mineType" action="" method="post">
    	<input type="hidden" name="action" value="edit">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="act" class="btn btn-success" value="{L_530}">
                </div>
            </div>
       	</div>
        <div class="mws-panel-body no-padding">
        	<table class="mws-table">
            	<tbody>
            		<tr>
            			<td align="center" colspan="4">{L_3500_1015779}</td>
            		</tr>
                	<tr>
                    	<th>{L_302}</th>
                        <th>{L_3500_1015771}</th>
                        <th>{L_3500_1015772}</th>
                        <th>{L_3500_1015777}</th>
                        <th>{L_008}</th>
                    </tr>
                    <!-- BEGIN mime_type -->
                    <tr>
                    	<td><input type="text" size="35%" name="name[{mime_type.ID}]" value="{mime_type.NAME}"></td>
                        <td><input type="text" size="40%" name="mine[{mime_type.ID}]" value="{mime_type.MINE}"></td>
                        <td><input type="text" size="10%" name="extension[{mime_type.ID}]" value="{mime_type.EXTENSION}"></td>
                        <td><input type="radio" name="used[{mime_type.ID}]" value="y" {mime_type.USED_Y}> {L_030}<br>
                        <input type="radio" name="used[{mime_type.ID}]" value="n" {mime_type.USED_N}> {L_029}</td>
                        <td><input type="checkbox" name="delete[]" value="{mime_type.ID}">
                        <input type="hidden" name="id[]" value="{mime_type.ID}"></td>
                    </tr>
                    <!-- END mime_type -->
               	</tbody>
        	</table>
    	</div>
   	</form>    	
</div>
<!-- INCLUDE footer.tpl -->
