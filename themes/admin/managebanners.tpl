<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="deleteusers" action="" method="post">
                    <input type="hidden" name="action" value="deleteusers">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L__0028}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>{L_5180}</th>
                                    <th>{L__0022}</th>
                                    <th>{L_303}</th>
                                    <th>{L__0025}</th>
                                    <th>&nbsp;</th>
                                    <th>{L_008}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN busers -->
                            <tr>
                            		<td><a href="editbannersuser.php?id={busers.ID}">{busers.NAME}</a></td>
                                    <td>{busers.COMPANY}</td>
                                    <td><a href="mailto:{busers.EMAIL}">{busers.EMAIL}</a></td>
                                    <td>{busers.NUM_BANNERS}</td>
                                    <td><a href="userbanners.php?id={busers.ID}">{L__0024}</a></td>
                                    <td><input type="checkbox" name="delete[]" value="{busers.ID}"></td>
                            </tr>
                            <!-- END busers -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->

