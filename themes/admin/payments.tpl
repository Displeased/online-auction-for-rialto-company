<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="payments" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                            	<tr>
                            		<th colspan="3">{L_3500_1015646}</th>
                            	</tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>{L_087}</th>
                                    <th>{L_008}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN payments -->
                            <tr>
                            		<td>&nbsp;</td>
                                    <td>
                                    	<input type="text" name="new_payments[]" value="{payments.PAYMENT}" size="25"></td>
                                    <td><input type="checkbox" name="delete[]" value="{payments.S_ROW_COUNT}"></td>
                            </tr>
                            <!-- END payments -->
                            <tr>
                                	<td>{L_394}</td>
                                    <td><input type="text" name="new_payments[]" size="25"></td>
                                    <td>&nbsp;</td>
                           </tr>
                           <tr>
                            		<td>&nbsp;</td>
                                    <td>{L_30_0102}</td>
                                    <td><input type="checkbox" class="selectall" value="delete"></td>

                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
