<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_2_0015}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th colspan="5">
                                    	{L_2_0021}
                                		<input type="text" name="ip">
                                		<input type="submit" class="btn btn-success btn-small" name="Submit2" value="&gt;&gt;">
                                		{L_2_0024}
                                    </th>
                                </tr>
                                <tr>
                                	<th>{L_087}</td>
                                    <th>{L_2_0009}</td>
                                    <th>{L_560}</td>
                                    <th>{L_5028}</td>
                                    <th>{L_008}</td>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN ips -->
                            <tr>
                            		<td>{L_2_0025}</td>
                                    <td>{ips.IP}</td>
                                    <td>
                                    	<!-- IF ips.ACTION eq 'accept' -->
                                        {L_2_0012}
            							<!-- ELSE -->
                                        {L_2_0013}
            							<!-- ENDIF -->
    								</td>
                                    <td>
                                    	<!-- IF ips.ACTION eq 'accept' -->
                                        <input type="checkbox" name="deny[]" value="{ips.ID}">
                                        &nbsp;{L_2_0006}
            							<!-- ELSE -->
                                        <input type="checkbox" name="accept[]" value="{ips.ID}">
                                        &nbsp;{L_2_0007}
            							<!-- ENDIF -->
                                    </td>
                                    <td><input type="checkbox" name="delete[]" value="{ips.ID}"></td>
                                    <!-- BEGINELSE -->
                                    <td colspan="5">{L_831}</td>
                                    <!-- END ips -->
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->