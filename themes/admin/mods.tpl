<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="mods" action="{FORM}" method="post">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{BUTTON_TITLE}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        <thead>
                                <tr>
                                    <th>{L_3500_1015452}</th>
                                    <th>{L_3500_1015455}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN mods -->
                                <tr>
                                    <!-- IF mods.B_NOTDOWNLOADFOLDER -->
                                        <!-- IF mods.B_NOTBACKUPFOLDER -->
                                        <td>
                                                {mods.MOD}<br>{mods.VERSION}<br>{mods.AUTHOR}<br>{mods.NAME}<br>
                                                <!-- IF mods.B_NOTINSTALLED -->
                                                
                                                <!-- ENDIF -->
                                            <!-- IF mods.B_NOTINSTALLED -->
                                            <input type="hidden" name="do" value="install">
                                            <input type="checkbox" name="mod" value="{mods.MOD_NAME}">
                                                <b>{L_3500_1015448}</b>
                                            <!-- ELSE -->
                                            <!-- IF mods.B_CHECKED --><p style="color:red">{L_3500_1015447}</p>
                                            <form name="mod" action="{FORM_2}" method="post">
                                            <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                                            <input type="hidden" name="mod" value="{mods.MOD_NAME}">
                                            <input type="hidden" name="delete_mod" value="yes">
                                            <input type="submit" name="act" value="{L_3500_1015458}">
                                            </form><!-- ENDIF -->
                                            <!-- ENDIF -->
                                        </td>
                                        <!-- ENDIF -->
                                    <!-- ENDIF -->
                                    <!-- IF mods.B_NOTDOWNLOADFOLDER -->
                                        <!-- IF mods.B_NOTBACKUPFOLDER -->
                                        <td>
                                                {mods.DESCRIPTION}
                                        </td>
                                        <!-- ENDIF -->
                                    <!-- ENDIF -->
                                </tr>
                                <!-- END mods -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
                <!-- BEGIN new_mods -->
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {new_mods.MOD}</span>
                    </div>
                    <form name="mods_list" action="{FORM_2}" method="post">
                    <input type="hidden" name="download_mod" value="yes">
                    <input type="hidden" name="download_this_mod" value="{new_mods.DOWNLOAD}">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                    <!-- IF new_mods.B_CHECKED -->
                    <!-- ELSE -->
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{BUTTON_TITLE}">
                            </div>
                        </div>
                    </div>
                    <!-- ENDIF -->
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>
                                    	{new_mods.MAKER}<br>{new_mods.VERSION}<br>
                                        <!-- IF new_mods.B_CHECKED -->
                                            <span style="color:red">{L_3500_1015457}</span>
                                            <!-- ELSE -->
											{L_3500_1015454}
                                        <!-- ENDIF -->
									</td>
                                    <td>{new_mods.INFO}</td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                <!-- END new_mods -->
                
                <!-- BEGIN mod -->
                <input type="hidden" name="update" value="yes">
                <input type="hidden" name="do" value="install">
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {mod.PAGE}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_3500_1015475}</td>
                                    <td>{mod.MAKER}</td>
                            </tr>
                            <tr>
                            		<td>{L_3500_1015476}</td>
                                    <td>{mod.MOD}</td>
                            </tr>
                            <tr>
                            		<td>{L_3500_1015477}</td>
                                    <td>{mod.VERSION}</td>
                            </tr>
                            <tr>
                            		<td>{L_3500_1015478}</td>
                                    <td>{mod.FOLDER}</td>
                            </tr>
                            <tr>
                            		<td>{L_3500_1015479}</td>
                                    <td>{mod.PAGE}</td>
                            </tr>
                            <tr>
                            		<td>{L_3500_1015480}</td>
                                    <td>{mod.FIND}</td>
                            </tr>
                            <tr>
                            		<td>{L_3500_1015481}</td>
                                    <td>{mod.REPLACE}</td>
                            </tr>
                            <tr>
                            		<td>{L_3500_1015482}</td>
                                    <td>{mod.ADDAFTER}</td>
                            </tr>
                            <tr>
                            		<td>{L_3500_1015483}</td>
                                    <td>{mod.ADDBEFORE}</td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                <!-- END mod -->
<!-- INCLUDE footer.tpl -->
