<!-- INCLUDE header.tpl -->
<!-- IF B_PREVIEW -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {L_25_0224}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{PREVIEW}</td>
                            </tr>
                           </tbody>
                        </table>
                    </div>  	
                </div>
<!-- ENDIF -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="payments" action="" method="post">
                    <!-- IF B_PREVIEW -->
                    <input type="hidden" name="action" value="submit">
                    <!-- ELSE -->
                    <input type="hidden" name="action" value="preview">
                    <!-- ENDIF -->
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="<!-- IF B_PREVIEW -->{L_007}<!-- ELSE -->{L_25_0224}<!-- ENDIF -->">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_5299}</td>
                                    <td>{SELECTBOX}</td>
                            </tr>
                            <tr>
                                	<td>{L_332}</td>
                                    <td><input type="text" name="subject" value="{SUBJECT}" size="50" maxlength="255"></td>
                           </tr>
                           <tr>
                            		<td>{L_605}</td>
                                    <td>
                                    	{L_30_0055}
										{EDITOR}
                                    </td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->