<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="payments" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>{L_240}</th>
                                    <th>{L_241}</th>
                                    <th>{L_137}</th>
                                    <th>{L_008}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN increments -->
                            <tr>
                            		<td>&nbsp;</td>
                                    <td>
                                    	<input type="hidden" name="id[]" value="{increments.ID}">
                                		<input type="text" name="lows[]" value="{increments.LOW}" size="10">
									</td>
                                    <td><input type="text" name="highs[]" value="{increments.HIGH}" size="10"></td>
                                    <td><input type="text" name="increments[]" value="{increments.INCREMENT}" size="10"></td>
                                    <td><input type="checkbox" name="delete[]" value="{increments.ID}"></td>
                            </tr>
                            <!-- END increments -->
                            <tr>
                                	<td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    
                                    <td>&nbsp;</td>
                                    <td>{L_30_0102}</td>
                                    <td><input type="checkbox" class="selectall" value="delete"></td>
                           </tr>
                           <tr>
                            		<td>{L_518}</td>
                            		<td><input type="text" name="new_lows" size="10"></td>
                                    <td><input type="text" name="new_highs" size="10"></td>
                                    <td><input type="text" name="new_increments" size="10"></td>
                                    <td>&nbsp;</td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->