<!-- INCLUDE header.tpl -->
<script src="{SITEURL}themes/admin/plugins/prettyphoto/js/jquery.prettyPhoto.min.js"></script>
<script src="{SITEURL}themes/admin/js/demo/demo.gallery.js"></script>
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/plugins/prettyphoto/css/prettyPhoto.css" media="screen">
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="payments" action="" method="post">
                    <input type="hidden" name="id" value="{ID}">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            <!-- IF B_CANRELIST -->
                            <input type="submit" name="relist" class="btn btn-warning" value="{L_2__0051}">
                            <!-- ENDIF -->
                            <!-- IF B_CLOSE -->
                            <input type="submit" name="closed" class="btn btn-warning" value="{L_3500_1015501}">
                            <!-- ENDIF -->
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_313}</td>
                                    <td>{USER}</td>
                            </tr>
                            <tr>
                                	<td>{L_017}</td>
                                    <td><input type="text" name="title" size="40" maxlength="255" value="{TITLE}"></td>
                           </tr>
                           <tr>
                            		<td>{L_806}</td>
                                    <td><input type="text" name="subtitle" size="40" maxlength="255" value="{SUBTITLE}"></td>
                           </tr>
                           
                           <!-- IF B_CONDITION -->
                           <tr>
                           		<td>{L_103600}</td>
                           		<td><span id="popoverData" href="#" data-content="{L_1047}" rel="popover" data-placement="bottom" data-original-title="{L_104400}" data-trigger="hover">{ITEM_CONDITION}</span> 
		  						<a href="{SITEURL}meanings.php" alt="description meanings" data-fancybox-type="iframe" target="_blank" class="btn converter btn-small btn-info">{L_104400}</a>
								</td>
                           </tr>
                           <tr>
                           		<td>{L_103700}</td>
                           		<td><input type="text" name="item_manufacturer" id="item_manufacturer" size="32" maxlength="32" value="{ITEM_MANUFACTURER}"></td>
                           </tr>
                           <tr>
                           		<td>{L_103800}</td>
                           		<td><input type="text" name="item_model" id="item_model" size="32" maxlength="32" value="{ITEM_MODEL}"></td>
                           </tr>
                           <tr>
                           		<td>{L_103900}</td>
                           		<td><input type="text" name="item_colour" id="item_colour" size="32" maxlength="32" value="{ITEM_COLOUR}"></td>
                           </tr>
                           <tr>
                           		<td>{L_104000}</td>
                           		<td><input type="text" name="item_year"  onkeypress="return isNumericKey(event)"  id="item_year" size="4" maxlength="4" value="{ITEM_YEAR}"></td>
                           </tr>
						<!-- ENDIF -->

                           <tr>
                            		<td>{L_287}</td>
                                    <td>{CATLIST1}</td>
                           </tr>
                           <tr>
                            		<td>{L_814}</td>
                                    <td>{CATLIST2}</td>
                           </tr>
                           <tr>
                            		<td>{L_018}</td>
                                    <td>{DESC}</td>
                           </tr>
                           <tr>
                            		<td>{L_258}</td>
                                    <td><input type="text" name="quantity" size="40" maxlength="40" value="{QTY}"></td>
                           </tr>
                           <tr>
                            		<td>{L_022}</td>
                                    <td>
                                    	<select name="duration">
                                            <option value=""> </option>
                                            {DURLIST}
                                        </select>
                                    </td>
                           </tr>                           
                           <tr>
                            		<td colspan="2">{L_817}</td>
                           </tr>
                           <tr>
                            		<td>{L_257}</td>
                                    <td>{ATYPE}</td>
                           </tr>
                           <tr>
                            		<td>{L_3500_1015744}</td>
                                    <td>
                                    	<input type="radio" name="sellType" id="free_item_yes" value="free" {FREEITEM_Y}> {L_3500_1015745} 
                                    	<input type="radio" name="sellType" id="free_item_no" value="sell" {FREEITEM_N}> {L_3500_1015746} 
                                    </td>
                           </tr>

                           <tr>
                            		<td>{L_116}</td>
                                    <td>{CURRENT_BID}</td>
                           </tr>
                           <tr>
                            		<td>{L_124}</td>
                                    <td><input type="text" name="min_bid" size="40" maxlength="40" value="{MIN_BID}"></td>
                           </tr>
                           <tr>
                            		<td>{L_023}</td>
                                    <td><input type="text" name="shipping_cost" size="40" maxlength="40" value="{SHIPPING_COST}"></td>
                           </tr>
                           <tr>
                            		<td>{L_021}</td>
                                    <td><input type="text" name="reserve_price" size="40" maxlength="40" value="{RESERVE}"></td>
                           </tr>
                           <tr>
                            		<td>{L_30_0063}</td>
                                    <td>
                                    	<input type="radio" name="buy_now_only" value="n" {BN_ONLY_N}> {L_029}
                            			<input type="radio" name="buy_now_only" value="y" {BN_ONLY_Y}> {L_030}
                                    </td>
                           </tr>
                           <tr>
                            		<td>{L_497}</td>
                                    <td><input type="text" name="buy_now" size="40" maxlength="40" value="{BN_PRICE}"></td>
                           </tr>
                           <tr>
                            		<td>{L_120}</td>
                                    <td><input type="text" name="customincrement" size="10" value="{CUSTOM_INC}"></td>
                           </tr>
                           <tr>
                            		<td colspan="2">{L_319}</td>
                           </tr>
                           <tr>
                            		<td>{L_025}</td>
                                    <td>
                                    	<input type="radio" name="shipping" value="1" {SHIPPING1}>	{L_031}<br>
                            			<input type="radio" name="shipping" value="2" {SHIPPING2}>	{L_032}<br>
                            			<input type="checkbox" name="international" value="1" {INTERNATIONAL}> {L_033}<br>
                            			<input type="checkbox" name="returns" value="1" {RETURNS}> {L_025_E}
                                    </td>
                           </tr>
                           <tr>
                            		<td>{L_25_0215}</td>
                                    <td><textarea name="shipping_terms" rows="3" cols="34">{SHIPPING_TERMS}</textarea></td>
                           </tr>
                           <tr>
                            		<td colspan="2">{L_5233}</td>
                           </tr>
                           <tr>
                            		<td>{L_026}</td>
                                    <td>{PAYMENTS}</td>
                           </tr>
                           
                           <!-- IF B_MKFEATURED or B_MKBOLD or B_MKHIGHLIGHT -->
                           <tr>
                            		<td>{L_268}</td>
                                    <td>
                                    	<!-- IF B_MKFEATURED -->
										<p><input type="checkbox" name="is_featured" {IS_FEATURED}> {L_273}</p>
                    					<!-- ENDIF -->
                    					<!-- IF B_MKBOLD -->
                                        <p><input type="checkbox" name="is_bold" {IS_BOLD}> {L_274}</p>
                    					<!-- ENDIF -->
                    					<!-- IF B_MKHIGHLIGHT -->
                                        <p><input type="checkbox" name="is_highlighted" {IS_HIGHLIGHTED}> {L_292}</p>
                    					<!-- ENDIF -->
                                    </td>
                           </tr>
                           <!-- ENDIF -->
                           <tr>
                            		<td>{L_300}</td>
                                    <td>{SUSPENDED}</td>
                           </tr>
                           
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {L_816}</span>
                    </div>
					<div class="mws-panel-body">
                		<ul class="thumbnails mws-gallery">
                			<!-- BEGIN gallery -->
                			<li>
	                			<span class="thumbnail"><img src="{SITEURL}{UPLOADEDPATH}{gallery.V}" alt=""></span>
	                			<span class="mws-gallery-overlay">
	                				<a href="{SITEURL}{UPLOADEDPATH}{gallery.V}" rel="prettyPhoto[gallery1]" class="mws-gallery-btn"><i class="icon-search"></i></a>
	                			</span>
							</li>
							<!-- END gallery -->
					    </ul>
			         </div>
                 </div>
<!-- INCLUDE footer.tpl -->
