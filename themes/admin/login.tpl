<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Admin - Login Page</title>

	<!-- Required Stylesheets -->
    <link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/bootstrap/css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/fonts/ptsans/stylesheet.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/fonts/icomoon/style.css" media="screen" />
    
    <link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/login.css" media="screen" />
    
    <link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/mws-theme.css" media="screen" />
        <script type="text/javascript" src="{SITEURL}loader.php?js=js/jquery.js{EXTRAJS}"></script>

    <!-- JavaScript Plugins -->
    <script src="{SITEURL}themes/admin/js/libs/jquery.placeholder.min.js"></script>
    <script src="{SITEURL}themes/admin/custom-plugins/fileinput.js"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="{SITEURL}themes/admin/jui/js/jquery-ui-effects.min.js"></script>

    <!-- Plugin Scripts -->
    <script src="{SITEURL}themes/admin/plugins/validate/jquery.validate-min.js"></script>

    <!-- Login Script -->
    <script src="{SITEURL}themes/admin/js/core/login.js"></script>
</head>

<body>
    <div id="mws-login-wrapper">
        <div id="mws-login">
            <h1>{L_052}</h1>
            <div class="mws-login-lock"><i class="icon-lock" style="margin-top:9px"></i></div>
            <div id="mws-login-form">
                <form class="mws-form" action="login.php" method="post">
                <!-- IF PAGE eq 1 -->
            	<h4 style="color:red;">{L_441}</h4>
                <h2 style="color:aqua">{L_3500_1015459}</h2>
            	<!-- ENDIF -->
            	<!-- IF ERROR ne '' -->
					<p style="color:red;">{ERROR}</p>
				<!-- ENDIF -->
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="username" class="mws-login-username required" placeholder="{L_003}">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="password" name="password" class="mws-login-password required" placeholder="{L_004}">
                        </div>
                    </div>
                    <!-- IF PAGE eq 1 -->
                    <h2 style="color:aqua">{L_3500_1015460}</h2>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="full_name" class="mws-login-username required" placeholder="{L_002}">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="user_name" class="mws-login-username required" placeholder="{L_003}">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="password" name="pass_word" class="mws-login-password required" placeholder="{L_004}">
                        </div>
                    </div>
                    <p style="color:aqua">{L_3500_1015461}</p>
                    <input type="hidden" name="action" value="insert">
                    <div class="mws-form-row">
                        <input type="submit" value="{L_5204}" class="btn btn-success mws-login-button">
                    </div>
                    <!-- ELSE -->
                    <input type="hidden" name="action" value="login">
                    <div class="mws-form-row">
                        <input type="submit" value="{L_052}" class="btn btn-success mws-login-button">
                    </div>
                    <!-- ENDIF -->
                </form>
                <!-- IF B_MULT_LANGS -->
						<h1>{L_2__0001}</h1>
                    	<br /><p>{FLAGS} </p>
				<!-- ENDIF -->
            </div>
        </div>


