<!-- INCLUDE header.tpl -->
<script type="text/javascript">
$(document).ready(function() {
	$("#selectall").click(function() {
		var checked_status = this.checked;
        $("input[id=delete]").each(function()
        { 
        	this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
        });	
	});
});
</script>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-table"></i> {PAGENAME}</span>
    </div>
    <form name="deletefaqs" action="" method="post">
    	<input type="hidden" name="action" value="delete">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="act" class="btn btn-warning" value="{L_008}">
                    <a class="btn btn-success" href="newfaq.php">{L_5231}</a>
                </div>
            </div>
       	</div>
        <div class="mws-panel-body no-padding">
        	<table class="mws-table">
            	<tbody>
                	<tr>
                    	<td>{L_30_0102}</td>
                        <td><input type="checkbox" id="selectall" value="delete"></td>
                    </tr>
                    <!-- BEGIN cats -->
                    <tr>
                    	<td>{cats.CAT}</td>
                        <td>&nbsp;</td>
                    </tr>
                    <!-- BEGIN faqs -->
                    <tr>
                    	<td><a href="editfaq.php?id={faqs.ID}">{faqs.FAQ}</a></td>
                        <td><input type="checkbox" name="delete[]" value="{faqs.ID}" id="delete"></td>
                    </tr>
                    <!-- END faqs -->
                    <!-- END cats -->
               	</tbody>
        	</table>
    	</div>
   	</form>    	
</div>
<!-- INCLUDE footer.tpl -->
