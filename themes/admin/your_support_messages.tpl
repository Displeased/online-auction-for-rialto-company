<!-- INCLUDE header.tpl -->
<script type="text/javascript">
$(document).ready(function () {
    $("#checkboxall").click(function () {
        var checked_status = this.checked
        $(".deleteid").each(function () {
            this.checked = checked_status;
        });
    });
});

$(".form1").submit(function () {
    if ($(".to").val() == "") {
        return false;
    }
    if ($(".subject").val() == "") {
        return false;
    }
    if ($(".message").val() == "") {
        return false;
    }
    return true;
});
</script>
<!-- IF B_OPEN -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-comment-2"></i> {L_3500_1015439j}</span>
    </div>
    <div class="mws-panel-toolbar">
    	<div class="btn-toolbar">
            	<div class="btn-group">
            	<a href="{SITEURL}{ADMIN_FOLDER}/admin_support_messages.php?x={ID}" class="btn btn-danger">{L_618}</a>
        	</div>
    	</div>
    </div>
	<div class="mws-panel-body no-padding">
		<form action="{SITEURL}{ADMIN_FOLDER}/admin_support_messages.php?x={ID}" id="form1" method="post" name="reply_back" enctype="multipart/form-data" class="mws-form">
			<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		    <input type="hidden" name="reply" value="reply_back">
		    <div class="mws-form-inline">
			    <div class="mws-form-row">
			    	<label class="mws-form-label" for="to">{L_241}:</label>
			      	<div class="mws-form-item">
			      		<span>{USER}</span>
			    	</div>
			    </div>
			    <div class="mws-form-row">
			      	<label class="mws-form-label">{L_332}:</label>
			      	<div class="mws-form-item">
			      		<input class="small" type="hidden" name="subject" value="{SUBJECT}">{SUBJECT}
			      	</div>
			    </div>
			    <div class="mws-form-row">
			      	<label class="mws-form-label">{L_333}:</label>
			      	<div class="mws-form-item">
			      		{MESSAGE}
			      	</div>
			    </div>
			    <div class="mws-button-row">
			      	<input name="submit" type="submit" value="{L_007}" class="btn btn-success">
			    </div>
			</div>
		</form>
	</div>
</div>
<!-- ENDIF -->
<form action="{SITEURL}{ADMIN_FOLDER}/admin_support.php" method="post" name="closemessages" enctype="multipart/form-data">
	<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
	<div class="mws-panel grid_8">
        <div class="mws-panel-header">
            <span><i class="icon-comment-2"></i> {L_3500_1015439f}</span>
        </div>
        <div class="mws-panel-toolbar">
    		<div class="btn-toolbar">
            		<div class="btn-group">
            		<a href="{SITEURL}{ADMIN_FOLDER}/admin_support_messages.php?x={ID}&reply" class="btn btn-success">{L_3500_1015439j}</a>
            		<a href="{SITEURL}{ADMIN_FOLDER}/admin_support.php" class="btn btn-info">{L_285}</a>
        		</div>
    		</div>
    	</div>
		<div class="mws-panel-body no-padding">
			<table class="mws-table">
				<thead>
					<tr>
						<th>{L_3500_1015439d}</th>
					    <th>{L_332}</th>
					    <th>{L_3500_1015439e}</th>
					    <th>{L_3500_1015439f}</th>
					    <th>{L_3500_1015439g}</th>
					    <!-- IF B_OPENED -->
					    <th>&nbsp;</th>
					   	<!-- ENDIF -->
					</tr>
				</thead>
				<tbody>
					<!-- IF MSGCOUNT eq 0 -->
					<tr>
						<td colspan="5">{L_3500_1015439s}</td>
					</tr>
					<!-- ELSE -->
					<!-- BEGIN ticket -->
						<tr>
						<td><small><span class="muted">{ticket.CREATED}</span></small></td>
					    <td>{ticket.TICKET_TITLE}</td>
					    <td>{ticket.LAST_UPDATE_USER}</td>
					    <td><!-- IF ticket.TICKET_STATUS --> <span style="color:green"><b>{L_3500_1015439a}</b></span><!-- ELSE --><span style="color:red"><b>{L_3500_1015439b}</b></span><!-- ENDIF --></td>
					    <td>{ticket.LAST_UPDATED_TIME}</td>
					    <!-- IF ticket.TICKET_STATUS -->
					    <td style="text-align:center">
					    	<input type="hidden" name="user" value="{USER_ID}">
					    	<input type="hidden" name="closeid[]" class="closeid" value="{ticket.TICKET_ID}">
					       	<input class="btn btn-danger" type="submit" name="submit" value="{L_678}">
					    </td>
					   	<!-- ENDIF -->
					</tr>
					<!-- END ticket -->
					<!-- ENDIF -->
				</tbody>
			</table>
		</div>
	</div>
</form>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-comment-2"></i> {L_3500_1015438}</span>
    </div>
    <div class="mws-panel-body no-padding">
    	<table class="mws-table">
    		<thead>
				<tr>
			    	<th width="15%">{L_240}</th>
			        <th>{L_3500_1015438}</th>
				</tr>
			</thead>
			<tbody>
				<!-- IF MSGCOUNT eq 0 -->
			    <tr>
			    	<td colspan="5">{L_2__0029}</td>
			    </tr>
			    <!-- ELSE -->
			    <!-- BEGIN ticket_mess -->
			    <tr>
			    	<td>{ticket_mess.LAST_USER}<br><small><span class="muted">{ticket_mess.CREATED}</span></small></td>
			        <td>{ticket_mess.TICKET_MESSAGE}</td>
			    </tr>
			    <!-- END ticket_mess -->
			    <!-- ENDIF -->
			</tbody>
    	</table>
    </div>
</div>

<!-- INCLUDE footer.tpl -->