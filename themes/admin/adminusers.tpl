<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            	<input type="submit" name="Submit" class="btn btn-success" value="{L_561}">
                            	<a href="{SITEURL}{ADMIN_FOLDER}/newadminuser.php" class="btn btn-warning">{L_367}</a>
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>{L_003}</th>
                                    <th>{L_558}</th>
                                    <th>{L_559}</th>
                                    <th>{L_560}</th>
                                    <th>{L_561}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN users -->
                            <tr>
                            		<td><a href="editadminuser.php?id={users.ID}">{users.USERNAME}</a></td>
                                	<td>{users.CREATED}</td>
                                	<td>{users.LASTLOGIN}</td>
                                	<td>{users.STATUS}</td>
                                	<td><input type="checkbox" name="delete[]" value="{users.ID}"></td>
                            </tr>
                            <!-- END users -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->