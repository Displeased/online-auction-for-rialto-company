<!-- INCLUDE header.tpl -->
<!-- IF B_EDIT -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-table"></i> {PAGENAME}</span>
    </div>
    <form name="edit_group" action="" method="post">
    	<input type="hidden" name="action" value="update">
        <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
		<div class="mws-panel-toolbar">
        	<div class="btn-toolbar">
            	<div class="btn-group">
                	<input type="submit" name="act" class="btn btn-success" value="{L_530}">
                    <a class="btn btn-warning" href="{SITEURL}{ADMIN_FOLDER}/usergroups.php">{L_285}</a>
                </div>
          	</div>
        </div>
        <div class="mws-panel-body no-padding">
        	<table class="mws-table">
        		<thead>
        			<tr>
        				<th colspan="8">{L_3500_1015724}</th>
        			</tr>
        		</thead>
            	<thead>
                	<tr>
                    	<th>{L_449}</th>
                        <th>{L_450}</th>
                        <th>{L_451}</th>
                        <th>{L_578}</th>
                        <th>{L_579}</th>
                        <th>{L_580}</th>
                        <th>{L_3500_1015673}</th>
                   	</tr>
              	</thead>
                <tbody>
                	<!-- IF B_EDIT -->
                    <tr>
                    	<td><input type="hidden" name="id" value="{GROUP_ID}">{GROUP_ID}</td>
                        <td><input type="text" name="group_name" value="{EDIT_NAME}"></td>
                        <td><input type="text" name="user_count" placeholder="{L_3500_1015674}" value="{USER_COUNT}"></td>
                        <td>
                        	<select name="can_sell">
                            	<option value="1" {CAN_SELL_Y}>{L_030}</option>
                                <option value="0" {CAN_SELL_N}>{L_029}</option>
                            </select>
                        </td>
                        <td>
                        	<select name="can_buy">
	                            <option value="1" {CAN_BUY_Y}>{L_030}</option>
	                           	<option value="0" {CAN_BUY_N}>{L_029}</option>
                            </select>
                      	</td>
                        <td>
                       		<select name="auto_join">
                            	<option value="1" {AUTO_JOIN_Y}>{L_030}</option>
                                <option value="0" {AUTO_JOIN_N}>{L_029}</option>
                            </select>
                        </td>
                       	<td>
                       		<select name="no_fees">
                            	<option value="1" {NO_FEES_Y}>{L_030}</option>
                                <option value="0" {NO_FEES_N}>{L_029}</option>
                            </select>
                       	</td>
                	</tr>
                  	<!-- ENDIF -->
				</tbody>
         	</table>
      	</div>
	</form>
</div>
<!-- ELSE -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-table"></i> {PAGENAME}</span>
    </div>
    <div class="mws-panel-toolbar">
    	<div class="btn-toolbar">
        	<div class="btn-group">
                <a class="btn btn-success" href="{SITEURL}{ADMIN_FOLDER}/usergroups.php?action=new">{L_518}</a>
            </div>
        </div>
   	</div>
    <div class="mws-panel-body no-padding">
    	<table class="mws-table">
        	<thead>
            	<tr>
                	<th>{L_449}</th>
                    <th>{L_450}</th>
                    <th>{L_451}</th>
                    <th>{L_578}</th>
                    <th>{L_579}</th>
                    <th>{L_580}</th>
                    <th>{L_3500_1015673}</th>
                    <th>&nbsp;</th>
                </tr>
          	</thead>
      		<tbody>
           		<!-- BEGIN groups -->
                <tr>
                	<td>{groups.ID}</td>
                    <td>{groups.NAME}</td>
                    <td>{groups.USER_COUNT}</td>
                    <td>{groups.CAN_SELL}</td>
                    <td>{groups.CAN_BUY}</td>
                    <td>{groups.AUTO_JOIN}</td>
                    <td>{groups.NO_FEES}</td>
                    <td>
                    	<a href="{SITEURL}{ADMIN_FOLDER}/usergroups.php?id={groups.ID}&action=edit">{L_298}</a><br>
                    	<a href="{SITEURL}{ADMIN_FOLDER}/usergroups.php?id={groups.ID}&action=delete">{L_008}</a>
                    </td>
                </tr>
            	<!-- END groups -->
            </tbody>
       	</table>
  	</div>
    <!-- ENDIF -->
</div>
<!-- INCLUDE footer.tpl -->
