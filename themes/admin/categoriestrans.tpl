<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        	<thead>
                            <!-- BEGIN langs -->
                            	<tr>
                                	<th>{L_161}</th>
                                    <th><a href="categoriestrans.php?lang={langs.LANG}"><img align="middle" src="{SITEURL}inc/flags/{langs.LANG}.gif" border="0"></a></th>
                                </tr>
                                <!-- END langs -->
                                <tr>
                                    <th>{L_771}</th>
                                    <th>{L_772}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN cats -->
                            <tr {cats.BG}>
                            		<td><input type="text" name="categories_o[]" value="{cats.CAT_NAME}" size="45" disabled></td>
                                    <td><input type="text" name="categories[{cats.CAT_ID}]" value="{cats.TRAN_CAT}" size="45"></td>
                            </tr>
                            <!-- END cats -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->