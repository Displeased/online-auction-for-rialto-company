<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="id" value="{ID}">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_530}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_5034}</td>
                                    <td><input type="text" name="name" size="25" maxlength="255" value="{NAME}"></td>
                            </tr>
                            <tr>
                                	<td>{L_5043}</td>
                                    <td>{MESSAGES} (<a href="editmessages.php?id={ID}">{L_5063}</a>)</td>
                           </tr>
                           <tr>
                            		<td>{L_5053}</td>
                                    <td>{LAST_POST}</td>
                           </tr>
                           <tr>
                           			<td>{L_5035}</td>
                                    <td>
                                    	<p>{L_5036}</p>
                            			<input type="text" name="msgstoshow" size="4" maxlength="4" value="{MSGTOSHOW}">
                                    </td>
                           </tr>
                           <tr>
                           			<td>&nbsp;</td>
                                    <td>
                                    	<input type="radio" name="active" value="1"<!-- IF B_ACTIVE --> checked="checked"<!-- ENDIF -->> {L_5038}<br>
                            			<input type="radio" name="active" value="2"<!-- IF B_DEACTIVE --> checked="checked"<!-- ENDIF -->> {L_5039}
                                    </td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
