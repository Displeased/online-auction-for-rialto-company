<link rel="stylesheet" type="text/css" href="{SITEURL}inc/calendar.css">
<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {L_103}</span>
                    </div>
                    <form action="" method="post">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_275}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_855}</td>
                                    <td>{L_5281} <input type="radio" name="type" value="m"<!-- IF TYPE eq 'm' --> checked="checked"<!-- ENDIF -->>
                        	{L_827} <input type="radio" name="type" value="w"<!-- IF TYPE eq 'w' --> checked="checked"<!-- ENDIF -->>
                        	{L_5285} <input type="radio" name="type" value="d"<!-- IF TYPE eq 'd' --> checked="checked"<!-- ENDIF -->>
                        	{L_2__0027} <input type="radio" name="type" value="a"<!-- IF TYPE eq 'a' --> checked="checked"<!-- ENDIF -->>
									</td>
                            </tr>

                            <tr>
                                	<td>{L_856}</td>
                                    <td><input type="text" name="from_date" id="from_date" value="{FROM_DATE}" size="20" maxlength="19">
                        <script type="text/javascript">
							new tcal ({'id': 'from_date','controlname': 'from_date'});
						</script>
                        -
                        <input type="text" name="to_date" id="to_date" value="{TO_DATE}" size="20" maxlength="19">
                        <script type="text/javascript">
							new tcal ({'id': 'to_date','controlname': 'to_date'});
						</script>
                        		</td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        	<thead>
                            	<!-- IF PAGNATION -->
                            	<tr>
                                	<th colspan="4">{L_5117}&nbsp;{PAGE}&nbsp;{L_5118}&nbsp;{PAGES}
                            			<br>
                                        {PREV}
										<!-- BEGIN pages -->
                            			{pages.PAGE}&nbsp;&nbsp;
										<!-- END pages -->
                            			{NEXT}</th>
                                </tr>
                                <!-- ENDIF -->
                            	<!-- IF PAGNATION -->
                                <tr>
                                    <th>{L_313}</th>
                                    <th>{L_766}</th>
                                    <th>{L_314}</th>
                                    <th>{L_391}</th>
                                </tr>
                                <!-- ELSE -->
                                <tr>
                                	<th>{L_314}</th>
                                    <th>{L_857}</th>
                                </tr>
                                <!-- ENDIF -->
                            </thead>
                            
                            <tbody>
                            <!-- BEGIN accounts -->
							<!-- IF PAGNATION -->

                            <tr>
                            		<td>{accounts.RNAME} ({accounts.NICK})</td>
                                    <td>{accounts.TEXT}</td>
                                    <td>{accounts.DATE}</td>
                                    <td>{accounts.AMOUNT}</td>
                            </tr>
							<!-- ELSE -->
                            <tr>
                                	<td>{accounts.DATE}</td>
                                    <td>{accounts.AMOUNT}</td>
                           </tr>
                           <!-- ENDIF -->
						   <!-- END accounts -->
                           </tbody>
                        </table>
                    </div>  	
                </div>
<!-- INCLUDE footer.tpl -->