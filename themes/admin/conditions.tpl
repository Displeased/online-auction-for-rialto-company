<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="conditions" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                    <div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>{L_3500_1015488}</th>
                                    <th>{L_087}</th>
                                    <th>{L_008}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN conditions -->
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><input type="text" name="new_item_condition[{conditions.ID}]" value="{conditions.ITEM_CONDITION}" size="40"></td>
                                    <td><textarea rows="4" cols="40" wrap="physical" name="new_condition_desc[{conditions.ID}]" value="{conditions.CONDITION_DESC}">{conditions.CONDITION_DESC} </textarea>
</td>
                                    <td><input type="checkbox" name="delete[]" value="{conditions.ID}"></td>
                                </tr>
                                <!-- END conditions -->
                                <tr>
                                	<td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>{L_30_0102}</td>
                                    <td><input type="checkbox" class="selectall" value="delete"></td>
                                </tr>
                                <!-- below is for add new condition -->
                                <tr>
                                    <td>{L_518}</td>
                                    <td><input type="text" name="new_item_condition[]" size="40" maxlength="40"></td>
                                    <td><textarea rows="4" cols="40" wrap="physical" name="new_condition_desc[]"> </textarea>
</td>
								<td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>    	
                </div>
<!-- INCLUDE footer.tpl -->