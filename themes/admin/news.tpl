<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-newspaper"></i> {PAGENAME}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th colspan="3">{NEWS_COUNT}{L_517} <a href="addnew.php">{L_518}</a></th>
                                </tr>
                                <tr>
                                	<th colspan="3">
                                        {L_5117}&nbsp;{PAGE}&nbsp;{L_5118}&nbsp;{PAGES}
                                        <br>
                                        {PREV}
            						<!-- BEGIN pages -->
                                        {pages.PAGE}&nbsp;&nbsp;
            						<!-- END pages -->
                                        {NEXT}
                                    </th>
                                </tr>
                                <tr>
                                    <th>{L_314}</th>
                                    <th>{L_312}</th>
                                    <th>{L_297}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN news -->
                            <tr>
                            		<td>{news.DATE}</td>
                                    <td <!-- IF news.SUSPENDED eq 1 -->style="background: #FAD0D0; color: #B01717; font-weight: bold;"<!-- ENDIF -->>{news.TITLE}</td>
                                    <td>
                                    	<a href="editnew.php?id={news.ID}&PAGE={PAGE}">{L_298}</a>
                                        <a href="deletenew.php?id={news.ID}&PAGE={PAGE}">{L_008}</a>
                                    </td>
                            </tr>
                            <!-- END news -->
                            
                           </tbody>
                        </table>
                    </div>
                </div>
<!-- INCLUDE footer.tpl -->