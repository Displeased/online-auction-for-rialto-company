<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th colspan="4">
                                    	<form name="clearmessages" action="" method="post">
                                    		<input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
	                                        {L_5065}
	                                        <input type="text" name="days">
	                                        {L_5115}&nbsp;&nbsp;&nbsp;
	                                        <input type="hidden" name="action" value="purge">
	                                        <input type="hidden" name="id" value="{ID}">
	                                        <input type="submit" name="submit" class="btn btn-success" value="{L_008}">
                                        </form>
                                    </th>
                                </tr>
                                <tr>
                                	<th>{L_5059}</th>
                                    <th>{L_5060}</th>
                                    <th>{L_314}</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN msgs -->
                            <tr>
                            		<td>{msgs.MESSAGE}</td>
                                    <td>{msgs.POSTED_BY}</td>
                                    <td>{msgs.POSTED_AT}</td>
                                    <td><a href="editmessage.php?id={ID}&msg={msgs.ID}">{L_298}</a>&nbsp;|&nbsp;<a href="deletemessage.php?board_id={ID}&id={msgs.ID}">{L_008}</a></td>
                            </tr>
                            <!-- END msgs -->
                           </tbody>
                        </table>
                    </div>  	
                </div>
<!-- INCLUDE footer.tpl -->
