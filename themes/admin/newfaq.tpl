<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="newfaq" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_530}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_5230}</td>
                            		<td colspan="2">
                                    	<select name="category">
										<!-- BEGIN cats -->
                                		<option value="{cats.ID}">{cats.CATEGORY}</option>
										<!-- END cats -->
                                		</select>
                                    </td>
                            </tr>
                            <!-- BEGIN lang -->
                            <tr>
                            		<!-- IF lang.S_FIRST_ROW -->
                                	<td>{L_5239}</td>
                                    <!-- ELSE -->
                                    <td>&nbsp;</td>
                                    <!-- ENDIF -->
                                    <td><img src="../inc/flags/{lang.LANG}.gif"></td>
                                    <td><input type="text" name="question[{lang.LANG}]" size="40" maxlength="255" value="{lang.TITLE}"></td>
                           </tr>
                           <!-- END lang -->
						   <!-- BEGIN lang -->
                           <tr>
                           			<!-- IF lang.S_FIRST_ROW -->
                            		<td>{L_5240}</td>
                                    <!-- ELSE -->
                                    <td>&nbsp;</td>
                                    <!-- ENDIF -->
                                    <td><img src="../inc/flags/{lang.LANG}.gif"></td>
                                    <td>{lang.CONTENT}</td>
                           </tr>
                           <!-- END lang -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
