<!-- INCLUDE header.tpl -->
				<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {mod.PAGE}</span>
                    </div>
                    <form name="mods" action="{FORM_2}" method="post">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{BUTTON_TITLE}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        <thead>
                                <tr>
                                    <th>{L_3500_1015452}452345</th>
                                    <th>{L_3500_1015455}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN mod -->
                            <input type="hidden" name="update" value="yes">
                			<input type="hidden" name="do" value="install">

	                            <tr>
	                            		<td>{L_3500_1015475}</td>
	                                    <td>{mod.MAKER}</td>
	                            </tr>
	                            <tr>
	                            		<td>{L_3500_1015476}</td>
	                                    <td>{mod.MOD}</td>
	                            </tr>
	                            <tr>
	                            		<td>{L_3500_1015477}</td>
	                                    <td>{mod.VERSION}</td>
	                            </tr>
	                            <tr>
	                            		<td>{L_3500_1015478}</td>
	                                    <td>{mod.FOLDER}</td>
	                            </tr>
	                            <tr>
	                            		<td>{L_3500_1015479}</td>
	                                    <td>{mod.PAGE}</td>
	                            </tr>
	                            <tr>
	                            		<td>{L_3500_1015480}</td>
	                                    <td>{mod.FIND}</td>
	                            </tr>
	                            <tr>
	                            		<td>{L_3500_1015481}</td>
	                                    <td>{mod.REPLACE}</td>
	                            </tr>
	                            <tr>
	                            		<td>{L_3500_1015482}</td>
	                                    <td>{mod.ADDAFTER}</td>
	                            </tr>
	                            <tr>
	                            		<td>{L_3500_1015483}</td>
	                                    <td>{mod.ADDBEFORE}</td>
	                            </tr>
	                        </tbody>
	                        <!-- END mod -->
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->


