<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="editadmin" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="id" value="{ID}">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_530}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_003}</td>
                                    <td>{USERNAME}</td>
                            </tr>
                            <tr>
                                <td>{L_558}</td>
                                <td>{CREATED}</td>
                            </tr>
                            <tr>
                                <td>{L_559}</td>
                                <td>{LASTLOGIN}</td>
                            </tr>
                            <tr>
                                <td colspan="2">{L_563}</td>
                            </tr>
                            <tr>
                                <td>{L_004}</td>
                                <td><input type="password" name="password" size="25"></td>
                            </tr>
                            <tr>
                                <td>{L_564}</td>
                                <td><input type="password" name="repeatpassword" size="25"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="radio" name="status" value="1"<!-- IF B_ACTIVE --> checked="checked"<!-- ENDIF -->> {L_566}<br>
                                    <input type="radio" name="status" value="2"<!-- IF B_INACTIVE --> checked="checked"<!-- ENDIF -->> {L_567}
                                </td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->