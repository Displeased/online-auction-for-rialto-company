<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="editbanneruser" action="" method="post">
                    <input type="hidden" name="id" value="{ID}">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_530}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_302}</td>
                                    <td><input type="text" name="name" value="{NAME}"></td>
                            </tr>
                            <tr>
                                	<td>{L__0022}</td>
                                    <td><input type="text" name="company" value="{COMPANY}"></td>  
                           </tr>
                           <tr>
                            		<td>{L_107}</td>
                                    <td><input type="text" name="email" value="{EMAIL}"></td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->

