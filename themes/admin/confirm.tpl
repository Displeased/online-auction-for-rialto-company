<!-- INCLUDE header.tpl -->
				<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form action="" method="post">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                    <!-- IF TYPE eq 1 -->
                            <input type="hidden" name="id" value="{ID}">
                    <!-- ELSEIF TYPE eq 2 -->
                            <input type="hidden" name="id" value="{ID}">
                            <input type="hidden" name="user" value="{USERID}">
                    <!-- ENDIF -->
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="action" class="btn btn-success" value="{L_030}">
                            <input type="submit" name="action" class="btn btn-danger" value="{L_029}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
	                            <tr>
	                            		<td>{MESSAGE}</td>
	                            </tr>
	                        </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->