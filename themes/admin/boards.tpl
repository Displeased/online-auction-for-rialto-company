<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="deletelogs" action="" method="post">
                    <input type="hidden" name="action" value="delete">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_008}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>{L_129}</th>
                                    <th>{L_294}</th>
                                    <th>{L_5046}</th>
                                    <th>{L_5043}</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN boards -->
                            <tr>
                            		<td>{boards.ID}</td>
                                    <td>
                                    	<a href="editboards.php?id={boards.ID}">{boards.NAME}</a>
										<!-- IF boards.ACTIVE eq 2 -->
										<b>[{L_5039}]</b>
										<!-- ENDIF -->
                                    </td>
                                    <td>{boards.MSGTOSHOW}</td>
                                    <td>{boards.MSGCOUNT}</td>
                                    <td><input type="checkbox" name="delete[]" value="{boards.ID}"></td>
                            </tr>
                            <!-- END boards -->
                            <tr>
                                	<td colspan="4">{L_30_0102}</td>
                                    <td><input type="checkbox" class="selectall" value="delete"></td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
