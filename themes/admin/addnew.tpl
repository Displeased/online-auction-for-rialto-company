<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-newspaper"></i> {PAGENAME}</span>
                    </div>
                    <form name="addnew" action="" method="post">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                    <!-- IF ID ne '' -->
                    <input type="hidden" name="id" value="{ID}">
					<!-- ENDIF -->
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" class="btn btn-success" value="{BUTTON}">
                            <a class="btn" href="{SITEURL}{ADMIN_FOLDER}/news.php"><i class="icol-cross"></i> {L_516}</a>
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <table class="mws-table">
                            <tbody>
                            	<tr>
                                    <td>{L_521}</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <input type="radio" name="suspended" value="0"<!-- IF B_ACTIVE --> checked="checked"<!-- ENDIF -->> {L_030}
                                        <input type="radio" name="suspended" value="1"<!-- IF B_INACTIVE --> checked="checked"<!-- ENDIF -->> {L_029}
                                    </td>
                                </tr>
                            <!-- BEGIN lang -->
                                <tr>
            					<!-- IF lang.S_FIRST_ROW -->
                                    <td>{L_519}:</td>
            					<!-- ELSE -->
                                    <td>&nbsp;</td>
            					<!-- ENDIF -->
                                    <td width="35" align="right"><img src="../inc/flags/{lang.LANG}.gif"></td>
                                    <td><input type="text" name="title[{lang.LANG}]" size="40" maxlength="255" value="{lang.TITLE}"></td>
                                </tr>
        						<!-- END lang -->
        						<!-- BEGIN lang -->
                                <tr>
            					<!-- IF lang.S_FIRST_ROW -->
                                    <td>{L_520}:</td>
            					<!-- ELSE -->
                                    <td>&nbsp;</td>
            					<!-- ENDIF -->
                                    <td><img src="../inc/flags/{lang.LANG}.gif"></td>
                                    <td>{lang.CONTENT}</td>
                                </tr>
        						<!-- END lang -->
                                </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
