<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                            	<tr>
                                	<th colspan="4">
                                    	{L_5117}&nbsp;{PAGE}&nbsp;{L_5118}&nbsp;{PAGES}
                                        <br>
                                        {PREV}
            							<!-- BEGIN pages -->
                                        {pages.PAGE}&nbsp;&nbsp;
            							<!-- END pages -->
                                        {NEXT}
                                    </th>
                                </tr>
                                <tr>
                                    <th>{L_294}</th>
                                    <th>{L_293}</th>
                                    <th>{L_3500_1015468}</th>
                                    <th>{L_3500_1015469}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN active_users -->
                            <tr>
                            		<td>{active_users.NAME}</td>
                                    <td>{active_users.NICK}</td>
                                    <td>{active_users.LASTLOGIN}</td>
                                    <td>{active_users.ONLINESTATUS}</td>
                            </tr>
                            <!-- END active_users -->
                           </tbody>
                        </table>
                    </div>    	
                </div>
<!-- INCLUDE footer.tpl -->
