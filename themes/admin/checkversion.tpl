<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-table"></i> {VERSION_TITLE}</span>
    </div>
    <div class="mws-panel-body no-padding">
    	<table class="mws-table">
        	<tbody>
            	<tr>
                	<td>{L_3500_1015473}</td>
                    <td>{MYVERSION}</td>
               	</tr>
                <tr>
                    <td>{L_3500_1015474}</td>
                    <td>{REALVERSION}</td>
               	</tr>
               	<tr>
                	<td colspan="2">{TEXT}</td>
                </tr>
          	</tbody>
     	</table>
	</div>  	
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-table"></i> {REQUIREMENTS_TITLE}</span>
    </div>
    <div class="mws-panel-body no-padding">
    	<table class="mws-table">
        	<tbody>
            	<tr>
                	<td>{L_3500_1015648}</td>
                    <td><!-- IF PHP_VERSION --><span style="color:green"><b>{CURRENT_PHP_VERSION}</b></span><!-- ELSE --><span style="color:red"><b>{CURRENT_PHP_VERSION}</b></span><!-- ENDIF --></td>
               	</tr>
                <tr>
                    <td>{L_3500_1015649}</td>
                    <td><!-- IF GD --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015650}</td>
                    <td><!-- IF BCMATH --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
				<tr>
                    <td>{L_3500_1015651}</td>
                    <td><!-- IF PDO --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015652}</td>
                    <td><!-- IF MBSTRING --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015653}</td>
                    <td><!-- IF HEADERS_SUPPORT --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015796}</td>
                    <td><!-- IF MCRYPT_ENCRYPT --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015654}</td>
                    <td><!-- IF MCRYPT_ENCRYPT --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015655}</td>
                    <td><!-- IF OPEN_BASEDIR --><span style="color:red"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:green"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015656}</td>
                    <td><!-- IF ALLOW_URL_FOPEN --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015657}</td>
                    <td><!-- IF FOPEN --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
				<tr>
                    <td>{L_3500_1015658}</td>
                    <td><!-- IF FREAD --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
               	<tr>
                    <td>{L_3500_1015659}</td>
                    <td><!-- IF FILE_GET_CONTENTS --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
				<tr>
                    <td>{L_3500_1015660}</td>
                    <td><!-- IF CURL --><span style="color:green"><b>{L_2__0066}</b></span><!-- ELSE --><span style="color:red"><b>{L_2__0067}</b></span><!-- ENDIF --></td>
               	</tr>
          	</tbody>
     	</table>
	</div>  	
</div>
<!-- INCLUDE footer.tpl -->
