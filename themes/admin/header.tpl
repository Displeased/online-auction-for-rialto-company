<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="{LANGUAGE}" dir="{DOCDIR}"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="{LANGUAGE}" dir="{DOCDIR}"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="{LANGUAGE}" dir="{DOCDIR}"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="{LANGUAGE}" dir="{DOCDIR}"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset={CHARSET}">
<title>Admin panel</title>

<!-- Viewport Metatag -->
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="icon" type="image/ico" href="{SITEURL}favicon.ico"/>
<!-- Plugin Stylesheets first to ease overrides -->
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/custom-plugins/wizard/wizard.css" media="screen">

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/bootstrap/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/fonts/ptsans/stylesheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/js/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/mws-style.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/icons/icol16.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/icons/icol32.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/plugins/imgareaselect/css/imgareaselect-default.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/plugins/colorpicker/colorpicker.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/plugins/jgrowl/jquery.jgrowl.css" media="screen">

<!-- jQuery-UI Stylesheet -->
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/jui/css/jquery.ui.all.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/jui/css/jquery.ui.timepicker.css" media="screen">

<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="{SITEURL}themes/admin/css/mws-theme.css" media="screen">

<script type="text/javascript" src="{SITEURL}loader.php?js=js/jquery.js{EXTRAJS}"></script>
<script type="text/javascript" src="{SITEURL}inc/ckeditor/ckeditor.js"></script>
<!-- JavaScript Plugins -->
    <script src="{SITEURL}themes/admin/js/libs/jquery-1.8.3.min.js"></script>
    <script src="{SITEURL}themes/admin/js/libs/jquery.mousewheel.min.js"></script>
    <script src="{SITEURL}themes/admin/js/libs/jquery.placeholder.min.js"></script>
    <script src="{SITEURL}themes/admin/custom-plugins/fileinput.js"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="{SITEURL}themes/admin/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="{SITEURL}themes/admin/jui/jquery-ui.custom.min.js"></script>
    <script src="{SITEURL}themes/admin/jui/js/jquery.ui.touch-punch.js"></script>
    <script src="{SITEURL}themes/admin/jui/js/timepicker/jquery-ui-timepicker.min.js"></script>

    <!-- Plugin Scripts -->
    <script src="{SITEURL}themes/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <!--[if lt IE 9]>
    <script src="{SITEURL}themes/admin/js/libs/excanvas.min.js"></script>
    <![endif]-->
    <script src="{SITEURL}themes/admin/plugins/flot/jquery.flot.min.js"></script>
    <script src="{SITEURL}themes/admin/plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
    <script src="{SITEURL}themes/admin/plugins/flot/plugins/jquery.flot.pie.min.js"></script>
    <script src="{SITEURL}themes/admin/plugins/flot/plugins/jquery.flot.stack.min.js"></script>
    <script src="{SITEURL}themes/admin/plugins/flot/plugins/jquery.flot.resize.min.js"></script>
    <script src="{SITEURL}themes/admin/plugins/colorpicker/colorpicker-min.js"></script>
    <script src="{SITEURL}themes/admin/plugins/validate/jquery.validate-min.js"></script>
    <script src="{SITEURL}themes/admin/plugins/jgrowl/jquery.jgrowl-min.js"></script>
    <script src="{SITEURL}themes/admin/custom-plugins/wizard/wizard.min.js"></script>

    <!-- Core Script -->
    <script type="text/javascript" src="{SITEURL}themes/admin/js/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="{SITEURL}themes/admin/js/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="{SITEURL}themes/admin/js/source/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="{SITEURL}themes/admin/bootstrap/js/bootstrap.min.js"></script>
    <script src="{SITEURL}themes/admin/js/core/mws.js"></script>

    <!-- Fancybox -->
    <script type="text/javascript">
	$(document).ready(function() {
	$('.fancybox-buttons').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		prevEffect : 'none',
		nextEffect : 'none',
		closeBtn  : false,
		helpers : {
			title : {
				type : 'float'
				},
				buttons	: {}
				},
		afterLoad : function() {
			this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
		}
	});
	$(".converter").fancybox({
		maxWidth	: 600,
		maxHeight	: 700,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	$(".infoboxs").fancybox({
		maxWidth	: 750,
		maxHeight	: 850,
		fitToView	: false,
		width		: '80%',
		height		: '80%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	$(".imaging").fancybox({
		maxWidth	: 1300,
		maxHeight	: 1300,
		fitToView	: false,
		width		: '95%',
		height		: '95%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

});
</script>
</head>
<body>

	<!-- Header -->
	<div id="mws-header" class="clearfix">
    
    	<!-- Logo Container -->
    	<div id="mws-logo-container">
        
        	<!-- Logo Wrapper, images put within this wrapper will always be vertically centered -->
        	<div id="mws-logo-wrap">
            	<img src="{SITEURL}themes/admin/images/mws-logo.png" alt="mws admin">
			</div>
        </div>
        
        <!-- User Tools (notifications, logout, profile, change password) -->
        <div id="mws-user-tools" class="clearfix">
        	
                
                <!-- Notifications -->
        		<div id="mws-user-notif" class="mws-dropdown-menu">
            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-info-sign"></i></a>
                
                <!-- Unread notification count -->
                <span class="mws-dropdown-notif">8</span>
                
                <!-- Notifications dropdown -->
                <div class="mws-dropdown-box">
                	<div class="mws-dropdown-content">
                        <ul class="mws-notifications">
                        	<li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/forum/portal.php">
                                    <span class="message">
                                        {L_1063}
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/wiki/doku.php">
                                    <span class="message">
                                        {L_3500_1015515}
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/Themes">
                                    <span class="message">
                                        {L_1069}
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/Mods">
                                    <span class="message">
                                        {L_1072}
                                    </span>
                                </a>
                            </li>
                            <li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/Languages">
                                    <span class="message">
                                        {L_1073}
                                    </span>
                                </a>
                            </li>
                            <li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/forum/viewforum.php?f=22">
                                    <span class="message">
                                        {L_1076}
                                    </span>
                                </a>
                            </li>
                            <li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/forum/donate.php">
                                    <span class="message">
                                        {L_1080}
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            
            <!-- Notifications -->
        		<div id="mws-user-notif" class="mws-dropdown-menu">
            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-cogs"></i></a>
                
                <!-- Unread notification count -->
                <span class="mws-dropdown-notif">2</span>
                
                <!-- Notifications dropdown -->
                <div class="mws-dropdown-box">
                	<div class="mws-dropdown-content">
                        <ul class="mws-notifications">
                        	<li class="unread">
                            	<a href="{SITEURL}{ADMIN_FOLDER}/errorlog.php">
                                    <span class="message">
                                        {L_891}
                                    </span>
                                </a>
                            </li>
                            <li class="unread">
                            	<a href="{SITEURL}{ADMIN_FOLDER}/cronlog.php">
                                    <span class="message">
                                        {L_3500_1015588}
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <!-- Notifications -->
        		<div id="mws-user-notif" class="mws-dropdown-menu">
            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-network"></i></a>
                
                <!-- Unread notification count -->
                <span class="mws-dropdown-notif">2</span>
                
                <!-- Notifications dropdown -->
                <div class="mws-dropdown-box">
                	<div class="mws-dropdown-content">
                        <ul class="mws-notifications">
                        	<li class="unread">
                            	<a href="{SITEURL}{ADMIN_FOLDER}/logout.php">
                                    <span class="message">
                                        {L_245}
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a href="{SITEURL}" target="_blank">
                                    <span class="message">
                                        {L_3500_1015516}
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        	
            <!-- User Information and functions section -->
            <div id="mws-user-info" class="mws-inset">
            
            	<!-- User Photo -->
            	<div id="mws-user-photo">
                	<img src="{SITEURL}themes/admin/images/personal.png" alt="User Photo">
                </div>
                
                <!-- Username and Functions -->
                <div id="mws-user-functions">
                    <div id="mws-username">
                        {L_200} {ADMIN_USER}<br />
                        {L_559}: {LAST_LOGIN}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Start Main Wrapper -->
    <div id="mws-wrapper">
    
    	<!-- Necessary markup, do not remove -->
		<div id="mws-sidebar-stitch"></div>
		<div id="mws-sidebar-bg"></div>
        
        <!-- Sidebar Wrapper -->
        <div id="mws-sidebar">
        
            <!-- Hidden Nav Collapse Button -->
            <div id="mws-nav-collapse">
                <span></span>
                <span></span>
                <span></span>
            </div>
            
            <div id="mws-searchbox" class="mws-inset">
            	<ul>
                    	<li>
                    	 || 
                        </li>
                </ul>
            </div>
            
            <!-- Main Navigation -->
            <div id="mws-navigation">
                <ul>
                    <li class="active"><a href="{SITEURL}{ADMIN_FOLDER}/index.php"><i class="icon-home"></i> {L_166}</a></li>
                    <li><a href="#"><i class="icon-cogs"></i> {L_5139}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/settings.php"> {L_526}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/auctions.php">{L_5087}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/minetype.php">{L_3500_1015778}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/displaysettings.php">{L_788}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/usersettings.php">{L_894}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/errorhandling.php">{L_409}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/conditions.php">{L_104100}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/countries.php">{L_081}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/payments.php">{L_075}</a></li>
                          	<li><a href="{SITEURL}{ADMIN_FOLDER}/durations.php">{L_069}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/increments.php">{L_128}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/membertypes.php">{L_25_0169}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/currency.php">{L_5004}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/time.php">{L_344}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/buyitnow.php">{L_3500_1015728}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/defaultcountry.php">{L_5322}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/counters.php">{L_2__0057}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/multilingual.php">{L_2__0002}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/metatags.php">{L_25_0178}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/contactseller.php">{L_25_0216}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/buyerprivacy.php">{L_236}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-list"></i> {L_3500_1015421}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/catsorting.php">{L_25_0146}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/categories.php">{L_078}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/categoriestrans.php">{L_132}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-tags"></i> {L_25_0012}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/fees.php">{L_25_0012}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/fee_gateways.php">{L_445}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/enablefees.php">{L_395}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/accounts.php">{L_854}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/invoice_settings.php">{L_1094}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/invoice.php">{L_766}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/tax.php">{L_1088}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/tax_levels.php">{L_1083}</a></li>
                        </ul>
                    </li>
                    <li><a href="{SITEURL}{ADMIN_FOLDER}/theme.php"><i class="icon-business-card"></i> {L_26_0002}</a>
                    </li>
                    <li><a href="#"><i class="icon-bullhorn"></i> {L_25_0011}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/banners.php">{L_5205}</a></li>
                    		<li><a href="{SITEURL}{ADMIN_FOLDER}/managebanners.php">{L__0008}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-user"></i> {L_25_0010}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/admin_support.php">{L_3500_1015432}</a></li>
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/listusers.php">{L_045}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/useractivity.php">{L_350_10210}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/usergroups.php">{L_448}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/profile.php">{L_048}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/activatenewsletter.php">{L_25_0079}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/newsletter.php">{L_607}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/banips.php">{L_2_0017}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/newadminuser.php">{L_367}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/adminusers.php">{L_525}</a></li>
                        </ul>
                    </li>
                    <li><a href="{SITEURL}{ADMIN_FOLDER}/security.php"><i class="icon-key-2"></i> {L_3500_1015543}</a></li>
                    <li><a href="#"><i class="icon-android"></i> {L_3500_1015418}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/spam.php">{L_749}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/email_block.php">{L_3500_1015416}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-comments"></i> {L_5030}</a>
                        <ul class="closed">
							<li><a href="{SITEURL}{ADMIN_FOLDER}/boardsettings.php">{L_5047}</a></li>
							<li><a href="{SITEURL}{ADMIN_FOLDER}/newboard.php">{L_5031}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/boards.php">{L_5032}</a></li>
						</ul>
					</li>
                    <li><a href="#"><i class="icon-legal"></i> {L_239}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/listauctions.php">{L_067}</a></li>
                    		<li><a href="{SITEURL}{ADMIN_FOLDER}/listclosedauctions.php">{L_214}</a></li>
                    		<li><a href="{SITEURL}{ADMIN_FOLDER}/listsuspendedauctions.php">{L_5227}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-info-sign"></i> {L_5236}</a>
                        <ul class="closed">
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/faqscategories.php">{L_5230}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/newfaq.php">{L_5231}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/faqs.php">{L_5232}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-edit"></i> {L_25_0018}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/news.php">{L_516}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/aboutus.php">{L_5074}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/terms.php">{L_5075}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/privacypolicy.php">{L_402}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/cookiespolicy.php">{L_30_0233}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-bars"></i> {L_25_0023}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/stats_settings.php">{L_3500_1015768}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/viewaccessstats.php">{L_5143}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/viewbrowserstats.php">{L_5165}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/viewplatformstats.php">{L_5318}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/viewbotstats.php">{L_3500_1015742}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-tools"></i> {L_5436}</a>
                        <ul class="closed">
                        	<li><a href="{SITEURL}{ADMIN_FOLDER}/checkversion.php">{L_25_0169a}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/maintainance.php">{L__0001}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/wordsfilter.php">{L_5068}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/errorlog.php">{L_891}</a></li>
                            <li><a href="{SITEURL}{ADMIN_FOLDER}/cronlog.php">{L_3500_1015588}</a></li>
                    		<li><a href="help.php">{L_148}</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-pushpin"></i> {L_1061}</a>
                        <ul class="closed">
                        	<li>
                            	<form name="anotes" action="" method="post">
                                    <textarea rows="15" style="width:100%" name="anotes" class="anotes">{ADMIN_NOTES}</textarea><br />
                                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                                    <input type="hidden" name="submitdata" value="data">
                                    <input type="submit" class="btn btn-success" name="act" value="{L_007}">
                                    <span style="float:right">
                                    <input type="submit" class="btn btn-danger" name="act" value="{L_008}"></span><br /><br />
								</form>
                        	</li>
                        </ul>
                    </li>     
                </ul>
            </div>         
        </div>
                <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- error messages -->
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-info-sign"></i> {L_3500_1015582}</span>
                    </div>
                <!-- IF SUPPORTMESSAGE -->
                    <div class="mws-panel-body no-padding">
                        	<div class="mws-form-message error">
                            	{MESSAGES} <a href="{SITEURL}{ADMIN_FOLDER}/admin_support.php">{L_3500_1015439p}</a>
                            </div>
                    </div>
                <!-- ENDIF -->
                <div class="mws-panel-body no-padding">
                    <div class="mws-form-message info">{CHECK_DONATED}</div>
                </div>
                <!-- IF WARNINGREPORT -->
                    <div class="mws-panel-body no-padding">
                        	<div class="mws-form-message error">
                            	{L_3500_1015581}
                                <ul>
                                	<li>{WARNINGMESSAGE}</li>
                                </ul>
                            </div>
                    </div>
                    <!-- ENDIF -->
                    <!-- IF ERROR ne '' -->
                    <div class="mws-panel-body no-padding">
                        	<div class="mws-form-message info">
                            	{ERROR}
                            </div>
                    </div>
                    <!-- ENDIF -->
                    <!-- IF THIS_VERSION eq REALVERSION -->
                    	<div class="mws-panel-body no-padding">
                     	   	<div class="mws-form-message info">
                     	       	{L_30_0212}
                            </div>
                    	</div>
                    <!-- ELSE -->
                    	<div class="mws-panel-body no-padding">
                        	<div class="mws-form-message error">
                            	{L_30_0211}
                            </div>
                    	</div>
                    <!-- ENDIF -->
                </div>
                
                    
                    
                    
                    
                    
                    
                    
