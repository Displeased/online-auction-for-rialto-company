<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-shopping-cart"></i> {PAGENAME}</span>
                    </div>
                    <form name="tax_edit" action="" method="post">
                    <input type="hidden" name="tax_id" value="{TAX_ID}">
                    <input type="hidden" name="action" value="add">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_089}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>{L_1082}</th>
                                    <th>{L_1083}</th>
                                    <th>{L_1084}</th>
                                    <th>{L_1085}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                            		<td><input type="text" name="tax_name" value="{TAX_NAME}"></td>
                                    <td><input style="width:90%" type="text" name="tax_rate" value="{TAX_RATE}"> %</td>
                                    <td><select name="seller_countries[]" multiple>{TAX_SELLER}</select></td>
                                    <td><select name="buyer_countries[]" multiple>{TAX_BUYER}</select></td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-globe"></i> {L_1045}</span>
                    </div>
                    <form name="tax_update" action="" method="post">
                    <input type="hidden" name="action" value="sitefee">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th>{L_1082}</th>
                                    <th>{L_1083}</th>
                                    <th>{L_1084}</th>
                                    <th>{L_1085}</th>
                                    <th>{L_1086}</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- BEGIN tax_rates -->
                            <tr>
                            		<td>{tax_rates.TAX_NAME}</td>
                                    <td>{tax_rates.TAX_RATE}%</td>
                                    <td>{tax_rates.TAX_SELLER}</td>
                                    <td>{tax_rates.TAX_BUYER}</td>
                                    <td><input type="radio" name="site_fee" value="{tax_rates.ID}"<!-- IF tax_rates.TAX_SITE_RATE eq 1 --> checked="checked"<!-- ENDIF -->></td>
                                    <td>
                                    	<a href="tax_levels.php?id={tax_rates.ID}&type=edit">{L_298}</a><br>
										<a href="tax_levels.php?id={tax_rates.ID}&type=delete" onClick="return confirm('{L_1087}')">{L_008}</a>
                                    </td>

                            </tr>
                            <!-- END tax_rates -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>

<!-- INCLUDE footer.tpl -->
