<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {PAGENAME}</span>
                    </div>
                    <form name="editfaq" action="" method="post">
                    <input type="hidden" name="action" value="update">
			 		<input type="hidden" name="id" value="{ID}">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{L_530}">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td>{L_5238}</td>
                                    <td>&nbsp;</td>
                                    <td>
                                    	<select name="category">
            							<!-- BEGIN cats -->
                                            <option value="{cats.ID}"<!-- IF cats.ID eq FAQ_CAT -->selected="selected"<!-- ENDIF -->>{cats.CAT}</option>
            							<!-- END cats -->
                                        </select>
                                    </td>
                            </tr>
                            <!-- BEGIN qs -->
                            <tr>
                            <!-- IF qs.S_FIRST_ROW -->
                                	<td>{L_5239}</td>
                                    <!-- ELSE -->
                                    <td>&nbsp;</td>
                                    <!-- ENDIF -->
                                    <td><img src="../inc/flags/{qs.LANG}.gif"></td>
                                    <td><input type="text" name="question[{qs.LANG}]" maxlength="200" value="{qs.QUESTION}"></td>
                           </tr>
                           <!-- END qs -->
							<!-- BEGIN as -->
                           <tr>
                           <!-- IF as.S_FIRST_ROW -->
                            		<td>{L_5240}</td>
                                    <!-- ELSE -->
                                    <td>&nbsp;</td>
                                    <!-- ENDIF -->
                                    <td><img src="../inc/flags/{as.LANG}.gif"></td>
                                    <td>{as.ANSWER}</td>
                           </tr>
                           <!-- END as -->
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<!-- INCLUDE footer.tpl -->
