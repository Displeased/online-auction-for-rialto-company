<!-- INCLUDE header.tpl -->
<!-- BEGIN new_mods -->
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> {new_mods.MOD}</span>
                    </div>
                    <!-- IF new_mods.B_CHECKED -->
                    <!-- ELSE -->
                    <form name="mods" action="{FORM_2}" method="post">
                    <input type="hidden" name="admincsrftoken" value="{_ACSRFTOKEN}">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="{BUTTON_TITLE}">
                            </div>
                        </div>
                    </div>
                    <!-- ENDIF -->
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        <thead>
                                <tr>
                                    <th>{L_3500_1015452}</th>
                                    <th>{L_3500_1015455}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                            		<td>
                                    	{new_mods.MAKER}<br>{new_mods.VERSION}<br>
                                        <!-- IF new_mods.B_CHECKED -->
                                            <span style="color:red">{L_3500_1015457}</span>
                                            <!-- ELSE -->
                                            <input type="hidden" name="download_mod" value="yes">
                    						<input type="hidden" name="download_this_mod" value="{new_mods.DOWNLOAD}">
											{L_3500_1015454}
                                        <!-- ENDIF -->
									</td>
                                    <td>{new_mods.INFO}</td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                <!-- END new_mods -->
                <!-- INCLUDE footer.tpl -->

