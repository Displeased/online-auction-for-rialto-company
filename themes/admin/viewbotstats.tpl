<!-- INCLUDE header.tpl -->
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-pie-chart"></i> {L_3500_1015743}<i>{SITENAME}</i> {STATSMONTH}</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                    <table class="mws-table">
                <tr>
                    <td width="15%">{L_5156}</td>
                    <td width="15%">{L_5155}</td>
                    <td width="25%">&nbsp;</td>
                    <td width="5%">&nbsp;</td>

                </tr>
<!-- BEGIN sitestats -->
                <tr class="bg">
                    <td height="45"><b>{sitestats.PLANTFORM}</b></td>
                    <td height="45"><b>{sitestats.BROWSER}</b></td>
                    <td>
	<!-- IF sitestats.NUM eq 0 -->
						<div style="height:15px;"><b>0</b></div>
	<!-- ELSE -->
						<div class="meter nostripes"><span style="width:{sitestats.PERCENTAGE}%"><span style="margin-left:5px"></span><b>{sitestats.PERCENTAGE}%</b></span></span></div>
	<!-- ENDIF -->
                    </td>
                    <td>{sitestats.COUNT}</td>
                </tr>
<!-- END sitestats -->
				</table>
  				</div>
                </div>
<!-- INCLUDE footer.tpl -->