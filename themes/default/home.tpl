<div class="row">
	<div class="span12">
		<div class="row">
			<div class="span3 hidden-phone">
 				<ul class="well nav nav-list ">
    				<li class="nav-header">{L_276}</li>
    				<li class="divider"></li>
    				<li><a href="{SITEURL}cat/{BROWSE_SEO}-0"><i class="icon-tags"></i> {L_277}</a></li>
    				<li class="divider"></li>
    				<!-- BEGIN cat_list -->
    				<li> 
					<a href="{SITEURL}cat/{cat_list.SEO_NAME}-{cat_list.ID}">{cat_list.IMAGE}{cat_list.NAME}</a></li>
    				<!-- END cat_list -->
  				</ul><br>
  					{INDEX_ADSENSE_1}
			</div>
			
			<div class="span6">
			<div class="row">
  				<div class="span6">
  					{INDEX_ADSENSE_2}
  				</div>
			</div>
			<!-- IF B_FEATU_ITEMS -->
  				<div class="row">
  					<div class="span6">
      					<h3>{L_350_10206}</h3>
    				</div>
    				<!-- BEGIN featured -->
    				<div class="span2" align="center"> 
    					<ul class="thumbnails">
    						<li class="span2">
    							<div class="thumbnail">
    								<div><a class="thumbnail" href="{SITEURL}products/{featured.SEO_TITLE}-{featured.ID}"><img class="img-rounded media-grid" src="{featured.IMAGE}" alt="{featured.TITLE}" style="max-width:{MAXIMAGESIZELIST}px; max-height:{MAXIMAGESIZELIST}px; width: auto; height: auto;"></a>
      								<h5><a href="{SITEURL}products/{featured.SEO_TITLE}-{featured.ID}">{featured.TITLE}</a></h5>
         							<p><small>{featured.BID} <br /><span class="muted">{L_171}<br>{featured.ENDS}</span></small></p></div>
      							</div>
      						</li>
      					</ul>
      				</div>
    				<!-- END featured -->
  				</div>
  			<!-- ENDIF --> 
  			<!-- IF B_HOT_ITEMS -->
  				<hr  />
  				<div class="row">
    				<div class="span6">
      					<h3>{L_279}</h3>
    				</div>
  				</div>
  				<div class="row">
    				<!-- BEGIN hotitems -->
    				<div class="span2" align="center">
    					<ul class="thumbnails">
    						<li class="span2">
    							<div class="thumbnail">
    								<div><a class="thumbnail" href="{SITEURL}products/{hotitems.SEO_TITLE}-{hotitems.ID}"><img class="img-rounded" src="{hotitems.IMAGE}" alt="{hotitems.TITLE}" style="max-width:{MAXIMAGESIZELIST}px; max-height:{MAXIMAGESIZELIST}px; width: auto; height: auto;"></a>
      								<h5><a href="{SITEURL}products/{hotitems.SEO_TITLE}-{hotitems.ID}">{hotitems.TITLE}</a></h5>
         							<p><small>{hotitems.BID} <br /><span class="muted">{L_171}<br>{hotitems.ENDS}</span></small></p></div>
      							</div>
      						</li>
      					</ul>
      				</div>
    				<!-- END hotitems -->
  				</div>
  			<!-- ENDIF -->
   			<!-- IF B_AUC_LAST -->
  				<hr  />
  				<div class="row">
    				<div class="span3">
      					<h3>{L_278}</h3>
      					<table class="table table-condensed">
        					<!-- BEGIN auc_last -->
        					<tr class="well">
          						<td style="text-align:center; width:50px;"><a href="{SITEURL}products/{auc_last.SEO_TITLE}-{auc_last.ID}"> <img class="img-polaroid" src="{auc_last.IMAGE}" alt="{auc_last.TITLE}" style="max-height:40px; max-width:40px; width: auto; height: auto;"/></a></td>
          						<td><a href="{SITEURL}products/{auc_last.SEO_TITLE}-{auc_last.ID}">{auc_last.TITLE}</a><br />
            						<span class="muted"><small>{auc_last.DATE}</small></span>
            					</td>
        					</tr>
        					<!-- END auc_last -->
      					</table>
    			</div>
    		<!-- ENDIF -->
    		<!-- IF B_AUC_ENDSOON -->
    			<div class="span3">
      				<h3>{L_280}</h3>
      					<table class="table table-condensed">
        					<!-- BEGIN end_soon -->
        					<tr class="well">
          						<td style="text-align:center; width:50px;"><a href="{SITEURL}products/{end_soon.SEO_TITLE}-{end_soon.ID}"> <img class="img-polaroid" src="{end_soon.IMAGE}" alt="{end_soon.TITLE}" style="max-height:40px; max-width:40px; width: auto; height: auto;"/></a></td>
          						<td><a href="{SITEURL}products/{end_soon.SEO_TITLE}-{end_soon.ID}">{end_soon.TITLE}</a><br />
            						<span class="muted"><small>{end_soon.DATE}</small></span>
            					</td>
        					</tr>
        					<!-- END end_soon -->
      					</table>
    			</div>
    		<!-- ENDIF -->
  			</div>
		</div>
		<div class="span3">
  			  		<!-- IF B_NEWS_BOX -->
  		<div style="padding: 8px 0;">
    		<ul class="well nav nav-list ">
      			<li class="nav-header">{L_282}</li>
      			<li class="divider"></li>
      			<!-- BEGIN newsbox -->
      			<small><a data-fancybox-type="iframe" class="infoboxs" href="{SITEURL}news/{newsbox.SEO_TITLE}-{newsbox.ID}">{newsbox.TITLE}</a><br />
      			<span class="muted">{newsbox.DATE}</span></small>
      			<hr />
      			<!-- END newsbox -->
      			<div class="clearfix"></div>
    		</ul>
  		</div>
  		<!-- ENDIF -->
  		<!-- IF B_HELPBOX -->
  		<div style=" padding: 8px 0;">
    		<ul class="well nav nav-list">
     			<li class="nav-header">{L_281}</li>
      			<li class="divider"></li>
      			<!-- IF B_BOARDS -->
      			<li><a href="{SITEURL}boards.php">{L_5030}</a></li>
      			<!-- ENDIF -->
      			<!-- IF B_FEES -->
      			<li><a href="{SITEURL}fees.php">{L_25_0012}</a></li>
      			<!-- ENDIF -->
      			<li><a href="{SITEURL}email_request_support.php">{L_350_10207}</a></li>
      			<!-- BEGIN helpbox -->
      			<li><a href="{SITEURL}viewhelp.php?cat={helpbox.ID}" alt="faqs" data-fancybox-type="iframe" class="infoboxs">{helpbox.TITLE}</a></li>
      			<!-- END helpbox -->
   		 	</ul>
  		</div>
  	<!-- ENDIF -->
  	<br>{INDEX_ADSENSE_3}
</div>

