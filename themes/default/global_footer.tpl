</div>

<div class="clearfix"></div>
<div class="footer well"> 
  <small><span class="muted"> <a href="{SITEURL}home">{L_166}</a>
  <!-- IF B_CAN_SELL -->
  | <a href="{SITEURL}select_category.php?">{L_028}</a>
  <!-- ENDIF -->
  <!-- IF B_LOGGED_IN -->
  | <a href="{SITEURL}user_menu.php?">{L_622}</a> | <a href="{SSLURL}logout.php?">{L_245}</a>
  <!-- ELSE -->
  | <a href="{SSLURL}new_account">{L_235}</a> | <a href="{SSLURL}user_login.php?">{L_052}</a>
  <!-- ENDIF -->
  | <a href="{SITEURL}help.php" alt="faqs" data-fancybox-type="iframe" class="infoboxs">{L_148}</a>
  <!-- IF B_FEES -->
  | <a href="{SITEURL}fees.php">{L_25_0012}</a>
  <!-- ENDIF -->
  <!-- IF B_VIEW_ABOUTUS -->
  | <a href="{SITEURL}contents.php?show=aboutus" >{L_5085}</a>
  <!-- ENDIF -->
  <!-- IF B_VIEW_PRIVPOL -->
  | <a href="{SITEURL}contents.php?show=priv">{L_401}</a>
  <!-- ENDIF -->
  <!-- IF B_VIEW_COOKIES -->
  | <a href="{SITEURL}contents.php?show=cookies"> {L_30_0239} </a> 
<!-- ENDIF --> 
  <!-- IF B_VIEW_TERMS -->
  | <a href="{SITEURL}contents.php?show=terms">{L_5086}</a>
  <!-- ENDIF -->
  | <a href="{SITEURL}email_request_support.php">{L_350_10207}</a>
  </span></small> <br>
</div>
<div align="center">
{L_COPY}<br>
<!--
			We request you retain the full copyright notice below including the link to www.u-auctions.com.
			This not only gives respect to the large amount of time given freely by the developers
			but also helps build interest, traffic and use of u-Auctions. If you (honestly) cannot retain
			the full copyright we ask you at least leave in place the "Powered by u-Auctions" line, with
			"u-Auctions" linked to https://u-auctions.com. If you must remove thte copyright message pelase make
            a donation at https://www.u-auctions.com/forum/donate.php to help pay for future developments
		-->
		<small><span>Powered by <a href="https://www.u-auctions.com/">u-Auctions</a> &copy; 2013 - 2015 <a href="https://www.u-auctions.com/">u-Auctions</a></span></small>
</div>

<!-- IF B_SUB_ADMIN -->
<br />
<div align="center">
<a href="{SITEURL}{ADMIN_FOLDER}/login.php" class="btn btn-primary" target="_blank">{L_3500_1015692}</a>
<br />&nbsp;
</div>
<!-- ENDIF -->
<!-- IF B_MAIN_ADMIN -->
<br />
<div align="center">
<a href="{SITEURL}{ADMIN_FOLDER}/login.php" class="btn btn-primary" target="_blank">{L_3500_1015405}</a>
<br />&nbsp;
</div>
<!-- ENDIF -->
</div><br />&nbsp;<br />
</body></html>