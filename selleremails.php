<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'selleremails.php';
	header('location: user_login.php');
	exit;
}

// Create new list
if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	$query = "UPDATE " . $DBPrefix . "users SET endemailmode = :endemailmod,
			  startemailmode = :startemailmod,
			  emailtype = :emailtype  WHERE id = :id";
	$params = array();
	$params[] = array(':endemailmod', $system->cleanvars($_POST['endemailmod']), 'bool');
	$params[] = array(':startemailmod', $system->cleanvars($_POST['startemailmod']), 'bool');
	$params[] = array(':emailtype', $system->cleanvars($_POST['emailtype']), 'bool');
	$params[] = array(':id', $user->user_data['id'], 'int');
	$db->query($query, $params);

	$ERR = $MSG['25_0192'];
	$user->user_data = array_merge($user->user_data, $_POST); //update the array
}

$template->assign_vars(array(
		'B_AUCSETUPY' => ($user->user_data['startemailmode'] == 'yes') ? ' checked="checked"' : '',
		'B_AUCSETUPN' => ($user->user_data['startemailmode'] == 'no') ? ' checked="checked"' : '',
		'B_CLOSEONE' => ($user->user_data['endemailmode'] == 'one') ? ' checked="checked"' : '',
		'B_CLOSEBULK' => ($user->user_data['endemailmode'] == 'cum') ? ' checked="checked"' : '',
		'B_CLOSENONE' => ($user->user_data['endemailmode'] == 'none') ? ' checked="checked"' : '',
		'B_EMAILTYPET' => ($user->user_data['emailtype'] == 'text') ? ' checked="checked"' : '',
		'B_EMAILTYPEH' => ($user->user_data['emailtype'] == 'html') ? ' checked="checked"' : ''
		));

include 'header.php';
$TMP_usmenutitle = $MSG['25_0188'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'sellermails.tpl'
		));
$template->display('body');
include 'footer.php';