<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

$template->assign_vars(array(
		'L_COPY' => empty($system->SETTINGS['copyright']) ? '' : '<p>' . $system->uncleanvars($system->SETTINGS['copyright']) . '</p>',

		'B_VIEW_TERMS' => ($system->SETTINGS['terms'] == 'y'),
		'B_VIEW_PRIVPOL' => ($system->SETTINGS['privacypolicy'] == 'y'),
		'B_VIEW_ABOUTUS' => ($system->SETTINGS['aboutus'] == 'y'),
		'B_SUB_ADMIN' => (isset($user->user_data['admin']) && $user->user_data['admin'] == 1),
		'B_MAIN_ADMIN' => (isset($user->user_data['admin']) && $user->user_data['admin'] == 2),
		'ADMIN_FOLDER' => ($system->SETTINGS['admin_folder']),
		'B_VIEW_COOKIES' => ($system->SETTINGS['cookiespolicy'] == 'y')
		));

$template->set_filenames(array(
		'footer' => 'global_footer.tpl'
		));
$template->display('footer');

// if the page has loaded OK you dont need this data anymore :)
unset($_SESSION['SESSION_ERROR']);
