<?php
$current_page = 'banners';
include 'common.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'editbanneruser.php';
	header('location: user_login.php');
	exit;
}

unset($ERR);
$id = $_REQUEST['id'];

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (empty($_POST['name']) || empty($_POST['company']) || empty($_POST['email']))
	{
		$ERR = $ERR_047;
		$USER = $_POST;
	}
	elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['email']))
	{
		$ERR = $ERR_008;
		$USER = $_POST;
	}
	else
	{
		// Update database
		$query = "UPDATE " . $DBPrefix . "bannersusers SET
				  name = :user_id,
				  company = :user_company,
				  email = :user_email
				  WHERE id = :id";
		$params = array();
		$params[] = array(':user_name', $_POST['name'], 'str');
		$params[] = array(':user_company', $_POST['company'], 'str');
		$params[] = array(':user_email', $_POST['email'], 'str');
		$params[] = array(':id', $_POST['email'], 'str');
		$db->query($query, $params);

		header('location: managebanners.php');
		exit;
	}
}
else
{
	$query = "SELECT * FROM " . $DBPrefix . "bannersusers WHERE id = :id";
	$params = array();
	$params[] = array(':id', $id, 'int');
	$db->query($query, $params);
	if ($db->numrows() > 0)
	{
		$USER = $db->result();
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'ID' => $id,
		'NAME' => (isset($USER['name'])) ? $USER['name'] : '',
		'COMPANY' => (isset($USER['company'])) ? $USER['company'] : '',
		'EMAIL' => (isset($USER['email'])) ? $USER['email'] : ''
		));

include 'header.php';
include 'includes/user_cp.php';
$template->set_filenames(array(
		'body' => 'editbanneruser.tpl'
		));
$template->display('body');
include 'footer.php';