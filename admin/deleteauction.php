<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

// Data check
if (!isset($_REQUEST['id']))
{
	$URL = $_SESSION['RETURN_LIST'];
	unset($_SESSION['RETURN_LIST']);
	header('location: ' . $URL);
	exit;
}

if (isset($_POST['action']) && $_POST['action'] == $MSG['030'])
{
	$catscontrol = new MPTTcategories();
	$auc_id = intval($_POST['id']);

	// get auction data
	$query = "SELECT category, num_bids, suspended, closed FROM " . $DBPrefix . "auctions WHERE id = :i";
	$params = array();
	$params[] = array(':i', $auc_id, 'int');
	$db->query($query, $params);
	$auc_data = $db->result();

	// Delete related values
	$query = "DELETE FROM " . $DBPrefix . "auctions WHERE id = :i";
	$params = array();
	$params[] = array(':i', $auc_id, 'int');
	$db->query($query, $params);

	// delete bids
	$query = "DELETE FROM " . $DBPrefix . "bids WHERE auction = :i";
	$params = array();
	$params[] = array(':i', $auc_id, 'int');
	$db->query($query, $params);

	// Delete proxybids
	$query = "DELETE FROM " . $DBPrefix . "proxybid WHERE itemid = :i";
	$params = array();
	$params[] = array(':i', $auc_id, 'int');
	$db->query($query, $params);

	// Delete file in counters
	$query = "DELETE FROM " . $DBPrefix . "auccounter WHERE auction_id = :i";
	$params = array();
	$params[] = array(':i', $auc_id, 'int');
	$db->query($query, $params);

	if ($auc_data['suspended'] == 0 && $auc_data['closed'] == 0)
	{
		// update main counters
		$query = "UPDATE " . $DBPrefix . "counters SET auctions = (auctions - :a), bids = (bids - :nb)";
		$params = array();
		$params[] = array(':a', 1, 'int');
		$params[] = array(':nb', $auc_data['num_bids'], 'int');
		$db->query($query, $params);

		// update recursive categories
		$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :c";
		$params = array();
		$params[] = array(':c', $auc_data['category'], 'int');
		$db->query($query, $params);
		
		$parent_node = $db->result();
		$crumbs = $catscontrol->get_bread_crumbs($parent_node['left_id'], $parent_node['right_id']);

		for ($i = 0; $i < count($crumbs); $i++)
		{
			$query = "UPDATE " . $DBPrefix . "categories SET sub_counter = sub_counter - :sc WHERE cat_id = :c";
			$params = array();
			$params[] = array(':sc', 1, 'int');
			$params[] = array(':c', $crumbs[$i]['cat_id'], 'int');
			$db->query($query, $params);
		}
	}

	// Delete auctions images
	if ($dir = @opendir($upload_path . $auc_id))
	{
		while ($file = readdir($dir))
		{
			if ($file != '.' && $file != '..')
			{
				@unlink($upload_path . $auc_id . '/' . $file);
			}
		}
		closedir($dir);
		@rmdir($upload_path . $auc_id);
	}

	$URL = $_SESSION['RETURN_LIST'];
	unset($_SESSION['RETURN_LIST']);
	header('location: ' . $URL);
	exit;
}
elseif (isset($_POST['action']) && $_POST['action'] == $MSG['029'])
{
	$URL = $_SESSION['RETURN_LIST'];
	unset($_SESSION['RETURN_LIST']);
	header('location: ' . $URL);
	exit;
}

$query = "SELECT title FROM " . $DBPrefix . "auctions WHERE id = :i";
$params = array();
$params[] = array(':i', $_GET['id'], 'int');
$db->query($query, $params);
$title = $db->result('title');

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => $MSG['325'],
		'ID' => $_GET['id'],
		'MESSAGE' => sprintf($MSG['833'], $title),
		'TYPE' => 1
		));

$template->set_filenames(array(
		'body' => 'confirm.tpl'
		));
$template->display('body');
?>