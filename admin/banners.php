<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'banners';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
		$query = "UPDATE ".$DBPrefix."settings SET banners = :set_banner, banner_width = :set_width, banner_height = :set_height, banner_types = :set_types";
		$params = array();
		$params[] = array(':set_banner', $system->cleanvars($_POST['banners']), 'int');
		$params[] = array(':set_width', $system->cleanvars($_POST['banner_width']), 'int');
		$params[] = array(':set_height', $system->cleanvars($_POST['banner_height']), 'int');
		$params[] = array(':set_types', $system->cleanvars($_POST['banner_types']), 'str');
		$db->query($query, $params);   
		$system->SETTINGS['banners'] = $_POST['banners'];
		$system->SETTINGS['banner_width'] = $_POST['banner_width'];
		$system->SETTINGS['banner_height'] = $_POST['banner_height'];
		$system->SETTINGS['banner_types'] = $_POST['banner_types'];
		$ERR = $MSG['600'];
}

loadblock($MSG['597'], '', 'batch', 'banners', $system->SETTINGS['banners'], array($MSG['030'], $MSG['029']));
loadblock($MSG['3500_1015422'], $MSG['3500_1015424'], 'text', 'banner_width', $system->SETTINGS['banner_width']);
loadblock($MSG['3500_1015423'], $MSG['3500_1015425'], 'text', 'banner_height', $system->SETTINGS['banner_height']);
loadblock($MSG['3500_1015426'], $MSG['3500_1015427'], 'text', 'banner_types', $system->SETTINGS['banner_types']);

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'TYPENAME' => $MSG['25_0011'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_enabledisable" target="_blank">' . $MSG['5205'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
