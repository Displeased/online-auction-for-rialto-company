<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'tools';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Update database
	$query = "UPDATE " . $DBPrefix . "settings SET wordsfilter = :f";
	$params = array();
	$params[] = array(':f', $_POST['wordsfilter'], 'str');
	$db->query($query, $params);


	//purge the old wordlist
	$query = "DELETE FROM " . $DBPrefix . "filterwords";
	$db->direct_query($query);

	
	//rebuild the wordlist
	$TMP = explode("\n", $_POST['filtervalues']);
	if (is_array($TMP))
	{
		foreach ($TMP as $k => $v)
		{
			$v = trim($v);
			if (!empty($v))
			{
				$query = "INSERT INTO " . $DBPrefix . "filterwords VALUES (:f)";
				$params = array();
				$params[] = array(':f', $v, 'int');
				$db->query($query, $params);
			}
		}
	}
	$ERR = $MSG['5073'];
	$system->SETTINGS['wordsfilter'] = $_POST['wordsfilter'];
}

$query = "SELECT * FROM " . $DBPrefix . "filterwords";
$db->direct_query($query);

$WORDSLIST = '';
while ($word = $db->result())
{
	$WORDSLIST .= $word['word'] . "\n";
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'PAGENAME' => $MSG['5068'],
		'WORDLIST' => $WORDSLIST,
		'WFYES' => ($system->SETTINGS['wordsfilter'] == 'y') ? ' checked="checked"' : '',
		'WFNO' => ($system->SETTINGS['wordsfilter'] == 'n') ? ' checked="checked"' : ''
		));

$template->set_filenames(array(
		'body' => 'wordfilter.tpl'
		));
$template->display('body');
?>
