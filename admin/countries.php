<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $include_path . 'functions_rebuild.php';

unset($ERR);
function delete_countries($column)
{
	global $DBPrefix, $system, $db;

	// we use a single SQL query to quickly do ALL our deletes
		$query = "DELETE FROM " . $DBPrefix . "countries WHERE ";
		$params = array();
		// if this is the first country being deleted it don't
		// precede it with an " or " in the SQL string
		for ($i = 0; $i < count($column); $i++)
		{
			if ($i > 0)
			{
				$query .= " OR ";
			}
			$query .= "country = :c";
			$params[] = array(':c', stripslashes($system->cleanvars($column[$i])), 'str');
		}
		$db->query($query, $params);
}

function update_countries($old_column, $new_column)
{
	global $DBPrefix, $db, $system;
	
	$query = "UPDATE " . $DBPrefix . "countries SET country = :c WHERE country = :wc";
	$params = array();
	$params[] = array(':c', stripslashes($system->cleanvars($new_column)), 'str');
	$params[] = array(':wc', stripslashes($system->cleanvars($old_column)), 'str');
	$db->query($query, $params);
}
function new_countries($column)
{
	global $DBPrefix, $db, $system;
	
	$query = "INSERT INTO " . $DBPrefix . "countries (country) VALUES (:c)";
	$params = array();
	$params[] = array(':c', stripslashes($system->cleanvars($column)), 'str');
	$db->query($query, $params);
}

if (isset($_POST['act']))
{
	// remove any countries that need to be
	if (isset($_POST['delete']) && count($_POST['delete']) > 0)
	{
		delete_countries($_POST['delete']);
	}

	//update countries with new names
	for ($i = 0; $i < count($_POST['old_countries']); $i++)
	{
		$old = $_POST['old_countries'][$i];
		$new = $_POST['new_countries'][$i];
		if ($old != $new)
		{
			update_countries($old, $new);
		}
	}

	// If a new country was added, insert it into database
	$add = $_POST['add_new_countries'][(count($_POST['add_new_countries']) - 1)];
	if (!empty($add))
	{
		new_countries($add);
	}
	rebuild_html_file('countries');
	$ERR = $MSG['1028'];
}

include $include_path . 'countries.inc.php';

$i = 1;
while ($i < count($countries))
{
	$j = $i - 1;
	// check if the country is being used by a user
	$query = "SELECT id FROM " . $DBPrefix . "users WHERE country = :c LIMIT :l";
	$params = array();
	$params[] = array(':c', $countries[$i], 'str');
	$params[] = array(':l', 1, 'int');
	$db->query($query, $params);
	$USEDINUSERS = $db->numrows('id');
	
	$template->assign_block_vars('countries', array(
			'COUNTRY' => $countries[$i],
			'SELECTBOX' => ($USEDINUSERS == 0) ? '<input type="checkbox" name="delete[]" value="' . $countries[$i] . '">' : '<img src="../images/nodelete.gif" alt="You cannot delete this">'
			));
	$i++;
}

$template->assign_vars(array(
		'PAGENAME' => $MSG['081'],
		'ERROR' => isset($ERR) ? $ERR : ''
		));

$template->set_filenames(array(
		'body' => 'countries.tpl'
		));
$template->display('body');

?>