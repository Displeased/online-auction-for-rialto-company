<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include $include_path . 'dates.inc.php';
include 'loggedin.inc.php';

$id = intval($_GET['id']);

if (isset($_POST['action']) && $_POST['action'] == 'purge')
{
	if (is_numeric($_POST['days']))
	{
		// Build date
		$DATE = $system->ctime - $_POST['days'] * 3600 * 24;
		$query = "DELETE FROM " . $DBPrefix . "comm_messages WHERE msgdate <= $DATE AND boardid = :i";
		$params = array();
		$params[] = array(':i', $id, 'int');
		$db->query($query, $params);

		// Update counter
		$query = "SELECT count(id) as COUNTER from " . $DBPrefix . "comm_messages WHERE boardid = :i";
		$params = array();
		$params[] = array(':i', $id, 'int');
		$db->query($query, $params);
		$comm_messages = $db->result('COUNTER');

		$query = "UPDATE " . $DBPrefix . "community SET messages = :cm WHERE id = :i";
		$params = array();
		$params[] = array(':i', $id, 'int');
		$params[] = array(':cm', $comm_messages, 'int');
		$db->query($query, $params);
	}
}

// Retrieve board name for breadcrumbs
$query = "SELECT name FROM " . $DBPrefix . "community WHERE id = :i";
$params = array();
$params[] = array(':i', $id, 'int');
$db->query($query, $params);
$board_name = $db->result('name');

// Retrieve board's messages from the database
$query = "SELECT * FROM " . $DBPrefix . "comm_messages WHERE boardid = :i";
$params = array();
$params[] = array(':i', $id, 'int');
$db->query($query, $params);

$bg = '';
while ($msg_data = $db->result())
{
    $template->assign_block_vars('msgs', array(
			'ID' => $msg_data['id'],
			'MESSAGE' => nl2br($msg_data['message']),
			'POSTED_BY' => $msg_data['username'],
			'POSTED_AT' => FormatDate($msg_data['msgdate']),
			'BG' => $bg
			));
	$bg = ($bg == '') ? 'class="bg"' : '';
}

$template->assign_vars(array(
		'BOARD_NAME' => $board_name,
		'PAGENAME' => $MSG['5278'],
		'ID' => $id
		));

$template->set_filenames(array(
		'body' => 'editmessages.tpl'
		));
$template->display('body');

?>