<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'auctions';
include '../common.php';
include $include_path . 'dates.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

// If $id is not defined -> error
if (!isset($_GET['id']))
{
	$URL = $_SESSION['RETURN_LIST'];
	unset($_SESSION['RETURN_LIST']);
	header('location: ' . $URL);
	exit;
}

$id = intval($_GET['id']);

// Retrieve auction's data
$query = "SELECT a.title, a.minimum_bid, a.starts, a.ends, a.auction_type, u.name, u.nick FROM " . $DBPrefix . "auctions a
		LEFT JOIN " . $DBPrefix . "users u ON (u.id = a.user)
		WHERE a.id = :i";
$params = array();
$params[] = array(':i', $id, 'int');
$db->query($query, $params);
if ($db->numrows() == 0)
{
	$URL = $_SESSION['RETURN_LIST'];
	unset($_SESSION['RETURN_LIST']);
	header('location: ' . $URL);
	exit;
}

$AUCTION = $db->result();

// Retrieve winners
$query = "SELECT w.bid, w.qty, u.name, u.nick FROM " . $DBPrefix . "winners w
		LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.winner)
		WHERE w.auction = :i";
$params = array();
$params[] = array(':i', $id, 'int');
$db->query($query, $params);
$winners = false;
while ($row = $db->result())
{
	$winners = true;
	$template->assign_block_vars('winners', array(
		'W_NICK' => $row['nick'],
		'W_NAME' => $row['name'],
		'BID' => $system->print_money($row['bid']),
		'QTY' => $row['qty']
		));
}

// Retrieve bids
$query = "SELECT b.bid, b.quantity, u.name, u.nick FROM " . $DBPrefix . "bids b
		LEFT JOIN " . $DBPrefix . "users u ON (u.id = b.bidder)
		WHERE b.auction = :i";
$params = array();
$params[] = array(':i', $id, 'int');
$db->query($query, $params);
$bids = false;
while ($row = $db->result())
{
	$bids = true;
	$template->assign_block_vars('bids', array(
		'W_NICK' => $row['nick'],
		'W_NAME' => $row['name'],
		'BID' => $system->print_money($row['bid']),
		'QTY' => $row['quantity']
		));
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ID' => $id,
		'TITLE' => $AUCTION['title'],
		'S_NICK' => $AUCTION['nick'],
		'S_NAME' => $AUCTION['name'],
		'MIN_BID' => $system->print_money($AUCTION['minimum_bid']),
		'STARTS' => FormatDate($AUCTION['starts']),
		'ENDS' => FormatDate($AUCTION['ends']),
		'AUCTION_TYPE' => $system->SETTINGS['auction_types'][$AUCTION['auction_type']],

		'B_WINNERS' => $winners,
		'B_BIDS' => $bids
		));

$template->set_filenames(array(
		'body' => 'viewwinners.tpl'
		));
$template->display('body');

?>