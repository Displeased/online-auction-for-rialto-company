<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);
$html = '';

// Create currencies array
$query = "SELECT id, valuta, symbol, ime FROM " . $DBPrefix . "rates ORDER BY :o";
$params = array();
$params[] = array(':o', 'ime', 'str');
$db->query($query, $params);
if ($db->numrows() > 0)
{
	while ($row = $db->result())
	{
		$CURRENCIES[$row['id']] = $row['symbol'] . '&nbsp;' . $row['ime'] . '&nbsp;(' . $row['valuta'] . ')';
		$CURRENCIES_SYMBOLS[$row['id']] = $row['symbol'];
	}
}

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Data check
	if (empty($_POST['currency']))
	{
		$ERR = $ERR_047;
	}
	elseif (!empty($_POST['moneydecimals']) && !is_numeric($_POST['moneydecimals']))
	{
		$ERR = $ERR_051;
	}
	else
	{
		// Update database
		$query = "UPDATE " . $DBPrefix . "settings SET currency = :c, moneyformat = :mf, moneydecimals = :md, moneysymbol = :ms";
		$params = array();
		$params[] = array(':c', $system->cleanvars($CURRENCIES_SYMBOLS[$_POST['currency']]), 'str');
		$params[] = array(':mf', intval($_POST['moneyformat']), 'int');
		$params[] = array(':md', intval($_POST['moneydecimals']), 'int');
		$params[] = array(':ms', intval($_POST['moneysymbol']), 'int');
		$db->query($query, $params);

		$system->SETTINGS['currency'] = $CURRENCIES_SYMBOLS[$_POST['currency']];
		$system->SETTINGS['moneyformat'] = $_POST['moneyformat'];
		$system->SETTINGS['moneydecimals'] = $_POST['moneydecimals'];
		$system->SETTINGS['moneysymbol'] = $_POST['moneysymbol'];
		$ERR = $MSG['553'];
	}
	//Adding new currency if the POST are set
	if (!empty($_POST['country']) && !empty($_POST['currency_type']) && !empty($_POST['currency_abbreviation']))
	{
		$query = "INSERT INTO " . $DBPrefix . "rates VALUES (NULL, :ime, :valuta, :symbol);";
		$params = array();
		$params[] = array(':ime', $_POST['country'], 'str');
		$params[] = array(':valuta', $_POST['currency_type'], 'str');
		$params[] = array(':symbol', $_POST['currency_abbreviation'], 'str');
		$db->query($query, $params);
		$ERR = $MSG['3500_1015804'];
		unset($_POST['country']); unset($_POST['currency_type']); unset($_POST['currency_abbreviation']);
	}
}

$link = "javascript:window_open('" . $system->SETTINGS['siteurl'] . "converter.php','incre',650,250,30,30)";

foreach ($CURRENCIES_SYMBOLS as $k => $v)
{
	if ($v == $system->SETTINGS['currency'])
		$selectsetting = $k;
}
loadblock($MSG['5008'], '', generateSelect('currency', $CURRENCIES));
loadblock('', $MSG['5138'], 'link', 'currenciesconverter', '', array($MSG['5010']));
loadblock($MSG['544'], '', 'batchstacked', 'moneyformat', $system->SETTINGS['moneyformat'], array($MSG['545'], $MSG['546']));
loadblock($MSG['548'], $MSG['547'], 'decimals', 'moneydecimals', $system->SETTINGS['moneydecimals']);
loadblock($MSG['549'], '', 'batchstacked', 'moneysymbol', $system->SETTINGS['moneysymbol'], array($MSG['550'], $MSG['551']));

loadblock($MSG['3500_1015798'], '', '', '', '', array(), true);
loadblock($MSG['014'], $MSG['3500_1015801'], 'text', 'country', $_POST['country']);
loadblock($MSG['3500_1015799'], $MSG['3500_1015802'], 'text', 'currency_type', $_POST['currency_type']);
loadblock($MSG['3500_1015800'], $MSG['3500_1015803'], 'text', 'currency_abbreviation', $_POST['currency_abbreviation']);

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'LINKURL' => $link,
		'OPTIONHTML' => $html,
		'TYPENAME' => $MSG['25_0008'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_currencies_settings" target="_blank">' . $MSG['5004'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
