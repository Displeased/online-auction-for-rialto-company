<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include $include_path . 'functions_rebuild.php';
include $include_path . 'membertypes.inc.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] = 'update')
{
	$old_membertypes = $_POST['old_membertypes'];
	$new_membertypes = $_POST['new_membertypes'];
	$new_membertype = $_POST['new_membertype'];

	// delete with the deletes
	if (isset($_POST['delete']) && is_array($_POST['delete']))
	{
		$idslist = implode(',', $_POST['delete']);
		$query = "DELETE FROM " . $DBPrefix . "membertypes WHERE id IN (:l)";
		$params = array();
		$params[] = array(':l', $idslist, 'str');
		$db->query($query, $params);
	}

	// now update everything else
	if (is_array($old_membertypes))
	{
		foreach ($old_membertypes as $id => $val)
		{
			if ( $val != $new_membertypes[$id])
			{
				$query = "UPDATE " . $DBPrefix . "membertypes SET feedbacks = :f, icon = :ic WHERE id = :i";
				$params = array();
				$params[] = array(':f', $new_membertypes[$id]['feedbacks'], 'int');
				$params[] = array(':ic', $system->cleanvars($new_membertypes[$id]['icon']), 'str');
				$params[] = array(':i', $id, 'int');
				$db->query($query, $params);
			}
		}
	}

	// If a new membertype was added, insert it into database
	if (!empty($new_membertype['feedbacks']))
	{
		$query = "INSERT INTO " . $DBPrefix . "membertypes VALUES (NULL, :f, :ic);";
		$params = array();
		$params[] = array(':f', $new_membertype['feedbacks'], 'int');
		$params[] = array(':ic', $system->cleanvars($new_membertype['icon']), 'int');
		$db->query($query, $params);
	}
	rebuild_table_file('membertypes');
	$ERR = $MSG['836'];
}

foreach ($membertypes as $id => $quest)
{
    $template->assign_block_vars('mtype', array(
			'ID' => $id,
			'FEEDBACK' => $quest['feedbacks'],
			'ICON' => $quest['icon']
			));
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_membership_levels" target="_blank">' . $MSG['25_0169'] . '</a>',
		));
		
$template->set_filenames(array(
		'body' => 'membertypes.tpl'
		));
$template->display('body');

?>