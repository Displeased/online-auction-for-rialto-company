<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'banners';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);
$id = $_REQUEST['id'];

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (empty($_POST['name']) || empty($_POST['company']) || empty($_POST['email']))
	{
		$ERR = $ERR_047;
		$USER = $_POST;
	}
	elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['email']))
	{
		$ERR = $ERR_008;
		$USER = $_POST;
	}
	else
	{
		// Update database
		$query = "UPDATE " . $DBPrefix . "bannersusers SET
				  name = :n,
				  company = :c,
				  email = :e
				  WHERE id = :i";
		$params = array();
		$params[] = array(':n', $_POST['name'], 'str');
		$params[] = array(':c', $_POST['company'], 'str');
		$params[] = array(':e', $_POST['email'], 'str');
		$params[] = array(':i', $id, 'int');
		$db->query($query, $params);

		header('location: managebanners.php');
		exit;
	}
}
else
{
	$query = "SELECT * FROM " . $DBPrefix . "bannersusers WHERE id = :i";
	$params = array();
	$params[] = array(':i', $id, 'int');
	$db->query($query, $params);
	if ($db->numrows() > 0)
	{
		$USER = $db->result();
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'ID' => $id,
		'NAME' => (isset($USER['name'])) ? $USER['name'] : '',
		'COMPANY' => (isset($USER['company'])) ? $USER['company'] : '',
		'PAGENAME' => $MSG['511'],
		'EMAIL' => (isset($USER['email'])) ? $USER['email'] : ''
		));

$template->set_filenames(array(
		'body' => 'editbanneruser.tpl'
		));
$template->display('body');
?>