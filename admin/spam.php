<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (($_POST['spam_sendtofriend'] == 2 || $_POST['spam_register'] == 2) && empty($_POST['recaptcha_public']) && empty($_POST['recaptcha_private']))
	{
		$ERR = $MSG['751'];
	}
	else
	{
		if (empty($_POST['disposable_email']) && $_POST['disposable_email'] != 'y') $_POST['disposable_email'] = 'n';
		
		if($_POST['image_captcha_characters_length'] == 3)
		{
			$image_captcha_width = 115;
		}
		elseif($_POST['image_captcha_characters_length'] == 4)
		{
			$image_captcha_width = 145;
		}
		elseif($_POST['image_captcha_characters_length'] == 5)
		{
			$image_captcha_width = 175;
		}
		elseif($_POST['image_captcha_characters_length'] == 6)
		{
			$image_captcha_width = 205;
		}
		elseif($_POST['image_captcha_characters_length'] == 7)
		{
			$image_captcha_width = 235;
		}
		elseif($_POST['image_captcha_characters_length'] == 8)
		{
			$image_captcha_width = 265;
		}
		elseif($_POST['image_captcha_characters_length'] == 9)
		{
			$image_captcha_width = 295;
		}
		elseif($_POST['image_captcha_characters_length'] == 10)
		{
			$image_captcha_width = 325;
		}
		
		$query = "UPDATE " . $DBPrefix . "settings SET recaptcha_public = :rp, recaptcha_private = :rpv, spam_sendtofriend = :ssf, spam_register = :sr, disposable_email_block = :deb, image_captcha_characters_length = :iccl, image_captcha_width = :icw";
		$params = array();
		$params[] = array(':rp', $_POST['recaptcha_public'], 'str');
		$params[] = array(':rpv', $_POST['recaptcha_private'], 'str');
		$params[] = array(':ssf', $_POST['spam_sendtofriend'], 'int');
		$params[] = array(':sr', $_POST['spam_register'], 'int');
		$params[] = array(':deb', $_POST['disposable_email'], 'bool');
		$params[] = array(':iccl', $_POST['image_captcha_characters_length'], 'int');
		$params[] = array(':icw', $image_captcha_width, 'int');
		$db->query($query, $params);

		$system->SETTINGS['recaptcha_public'] = $_POST['recaptcha_public'];
		$system->SETTINGS['recaptcha_private'] = $_POST['recaptcha_private'];
		$system->SETTINGS['spam_sendtofriend'] = $_POST['spam_sendtofriend'];
		$system->SETTINGS['spam_register'] = $_POST['spam_register'];
		$system->SETTINGS['disposable_email_block'] = $_POST['disposable_email'];
		$system->SETTINGS['image_captcha_characters_length'] = $_POST['image_captcha_characters_length'];
		$ERR = $MSG['750'];
	}
}

loadblock($MSG['746'], $MSG['748'], 'text', 'recaptcha_public', $system->SETTINGS['recaptcha_public']);
loadblock($MSG['747'], '', 'text', 'recaptcha_private', $system->SETTINGS['recaptcha_private']);
loadblock($MSG['743'], $MSG['745'], 'select3num', 'spam_register', $system->SETTINGS['spam_register'], array($MSG['740'], $MSG['741'], $MSG['742']));
loadblock($MSG['744'], '', 'select3num', 'spam_sendtofriend', $system->SETTINGS['spam_sendtofriend'], array($MSG['740'], $MSG['741'], $MSG['742']));
loadblock($MSG['3500_1015545'], $MSG['3500_1015546'], 'checkbox', 'disposable_email', $system->SETTINGS['disposable_email_block']);

loadblock($MSG['3500_1015661'], '', '', '', '', array(), true);
loadblock($MSG['3500_1015662'], $MSG['3500_1015663'], 'text', 'image_captcha_characters_length', $system->SETTINGS['image_captcha_characters_length']);


$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['5142'],
		'PAGENAME' => $MSG['749']
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
