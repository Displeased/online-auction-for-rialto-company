<?php
/*******************************************************************************
 *   copyright				: (C) 2008 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com/
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/


if(isset($_SESSION['admincsrftoken']))
{
	# Token should exist as soon as a user is logged in
	if($_SERVER["REQUEST_METHOD"] == 'POST' || $_SERVER["REQUEST_METHOD"] == 'GET')
	{
		if(1 < count($_POST))		# More than 2 parameters in a POST (csrftoken + 1 more) => check
		{
			if($security->decrypt($_POST['admincsrftoken']) == $security->decrypt($_SESSION['admincsrftoken']))  # Checking if the POST and SESSION Tokens match
			{
				$valid_req = false;   # POST and SESSION Tokens match so pass
			}
			else
			{
				$valid_req = true;  # POST and SESSION Tokens don't match so fail
			}
		}
	}
	if($valid_req) 
	{ 
		global $MSG, $ERR_077; 

		$_SESSION['msg_title'] = $MSG['936']; 
		$_SESSION['msg_body'] = $ERR_077; 
		header('location: ../message.php');
		exit; // kill the page 
	}
}
else
{
	header("location: login.php");
	exit;
}

if (checklogin())
{
	header("location: login.php");
	exit;
}
else
{
	// inform admin there has been a reported listing since last login 
	$query = "SELECT report_date FROM " . $DBPrefix . "report_listing 
	WHERE report_closed = :close ORDER BY report_date desc limit :limit" ; 
	$params = array();
	$params[] = array(':close', 0, 'int');
	$params[] = array(':limit', 1, 'int');
	$db->query($query, $params);
 
	while ($report = $db->result('report_date')) 
	{ 
		$last_report = $report; 
	} 
	if (isset($last_report)) 
	{ 
	    $warning_report = 1; 
	    $warningmsg = 'you have a reported listing';
	} 
	else 
	{ 
	     $warning_report='';    
	} 
	
	//check to see if there is any new support tickets
	$query = "SELECT id FROM " . $DBPrefix . "support 
		WHERE ticket_reply_status = :reply_status AND status =:ticket_status";
	$params = array();
	$params[] = array(':reply_status', 'support', 'bool');
	$params[] = array(':ticket_status', 'open', 'bool');
	$db->query($query, $params);
	if ($db->numrows('id') > 0)
	{
		$supports = $MSG['3500_1015439q'];
		$support_report = 1;
	}
	
	//Deleteing the admin note
	if (isset($_POST['act']) && $_POST['act'] == $MSG['008'])
	{
		if ((isset($_POST['submitdata'])) && $_POST['submitdata'] == 'data')
		{
			delete_admin_note($security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']));
		}
	}
	
	//Add a new admin note
	if (isset($_POST['act']) && $_POST['act'] == $MSG['007'])
	{
		if ((isset($_POST['submitdata'])) && $_POST['submitdata'] == 'data')
		{
			if (isset($_POST['anotes']) && !empty($_POST['anotes']))
			{
				add_admin_note($security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']), $_POST['anotes']);
			}
		}
	}
	
	$template->assign_vars(array(
			'DOCDIR' => $DOCDIR,
			'THEME' => $system->SETTINGS['theme'],
			'SITEURL' => $system->SETTINGS['siteurl'],
			'CHARSET' => $CHARSET,
			'EXTRAJS' => (isset($extraJs)) ? $extraJs : '',
			'ADMIN_USER' => $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_USER']),
			'ADMIN_ID' => $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']),
			'CURRENT_PAGE' => $current_page,
			'LAST_LOGIN' => last_login($security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_TIME'])),
			'LASTLOGIN' => (isset($last_login)),
       		'LASTREPORT' => (isset($last_report)),
       		'WARNINGREPORT' => $warning_report,
       		'WARNINGMESSAGE' => (isset($warningmsg)),
       		'SUPPORTMESSAGE' => ($support_report == 1) ? true : false,
       		'MESSAGES' => $supports,
       		'ADMIN_FOLDER' => $system->SETTINGS['admin_folder'],
			'ADMIN_NOTES' => getAdminNotes(),
			'TEXT' => $text,
			'MYVERSION' => version_check(check()),
			'THIS_VERSION' => $system->SETTINGS['version'],
			'REALVERSION' => check(),
			'CHECK_DONATED' => stripslashes($system->uncleanvars($system->SETTINGS['donated']))
			));
	
	foreach ($LANGUAGES as $lang => $value)
	{
		$template->assign_block_vars('langs', array(
				'LANG' => $value,
				'B_DEFAULT' => ($lang == $system->SETTINGS['defaultlanguage'])
				));
	}
}
?>