<?php
 /*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// update durations table
	$rebuilt_condition_desc = array();
	$rebuilt_item_condition = array();

	foreach ($_POST['new_condition_desc'] as $k => $v)
	{
		if ((isset($_POST['delete']) && !in_array($k, $_POST['delete']) || !isset($_POST['delete'])) && !empty($_POST['new_condition_desc'][$k]) && !empty($_POST['new_item_condition'][$k]))
		{
			$rebuilt_condition_desc[] = $_POST['new_condition_desc'][$k];
			$rebuilt_item_condition[] = $_POST['new_item_condition'][$k];
		}
	}

	$query = "DELETE FROM " . $DBPrefix . "conditions";
	$db->direct_query($query);

	//$rebuilt_item_condition
	for ($i = 0; $i < count($rebuilt_item_condition); $i++)
	{
		$query = "INSERT INTO " . $DBPrefix . "conditions VALUES (:ic, :cd)";
		$params = array();
		$params[] = array(':ic', $rebuilt_item_condition[$i], 'str');
		$params[] = array(':cd', stripslashes($system->cleanvars($rebuilt_condition_desc[$i])), 'str');
		$db->query($query, $params);
	}
	$ERR = $MSG['104300'];
}

$query = "SELECT * FROM " . $DBPrefix . "conditions ORDER BY :ic";
$params = array();
$params[] = array(':ic', 'item_condition', 'str');
$db->query($query, $params);

$i = 0;
while ($row = $db->result())
{
	$template->assign_block_vars('conditions', array(
			'ID' => $i,
			'ITEM_CONDITION' => $row['item_condition'],
			'CONDITION_DESC' => $row['condition_desc']
			));
	$i++;
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_conditions_table" target="_blank">' . $MSG['104100'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'conditions.tpl'
		));
$template->display('body');

?>