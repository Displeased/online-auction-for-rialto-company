<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/
define('InAdmin', 1);
$current_page = 'Support Center';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

$ERR = isset($_SESSION['support_err_message']) ? $_SESSION['support_err_message'] : '';
$link = $system->SETTINGS['siteurl'] . 'support';

if (isset($_POST['deleteid']) && is_array($_POST['deleteid']))
{
	foreach ($_POST['deleteid'] as $k => $v)
	{
		$query = "DELETE FROM " . $DBPrefix . "support_messages WHERE reply_of = :replayof";
		$params = array();
		$params[] = array(':replayof', $v, 'int');
		$db->query($query, $params);
		
		$query = "DELETE FROM " . $DBPrefix . "support WHERE ticket_id = :replayof";
		$params = array();
		$params[] = array(':replayof', $v, 'int');
		$db->query($query, $params);
	}
	$ERR = $MSG['444'];
}

if (isset($_POST['closeid']) && is_array($_POST['closeid']))
{
	foreach($_POST['closeid'] as $k => $v)
	{
		$query = "UPDATE " . $DBPrefix . "support SET last_reply_time = :update_time, ticket_reply_status = 'user', status = 'close', last_reply_user = 0 WHERE ticket_id = :id";
		$params = array();
		$params[] = array(':update_time', $system->ctime, 'int');
		$params[] = array(':id', $v, 'int');
		$db->query($query, $params);
	}
	$ERR = $MSG['3500_1015439k'];
}

$query = "SELECT t.*, u.nick FROM " . $DBPrefix . "support t
	LEFT JOIN " . $DBPrefix . "users u ON (u.id = t.user)
	ORDER BY last_reply_time DESC";
// get users messages
$db->direct_query($query);
$messages = $db->numrows();
while ($array = $db->result())
{
	// formatting the created time
	$created_time = $array['created_time'];
	$mth = 'MON_0' . gmdate('m', $created_time);
	if($system->SETTINGS['datesformat'] == 'EUR')
	{
		$created =  gmdate('j', $created_time) . ' ' . $MSG[$mth] . ' ' . gmdate('Y', $created_time) . ' ' . gmdate('H:i:s', $created_time);
	}
	else
	{
		$created = $MSG[$mth] . ' ' . gmdate('j,Y', $created_time) . ' ' . gmdate('H:i:s', $created_time);;
	}
	
	$last_reply_time = $array['last_reply_time'];
	$mth = 'MON_0' . gmdate('m', $last_reply_time);
	if($system->SETTINGS['datesformat'] == 'EUR')
	{
		$last_reply =  gmdate('j', $last_reply_time) . ' ' . $MSG[$mth] . ' ' . gmdate('Y', $last_reply_time) . ' ' . gmdate('H:i:s', $last_reply_time);
	}
	else
	{
		$last_reply = $MSG[$mth] . ' ' . gmdate('j,Y', $last_reply_time) . ' ' . gmdate('H:i:s', $last_reply_time);
	}

	$template->assign_block_vars('ticket', array(
		'LAST_UPDATED_TIME' => $last_reply, //when the ticket was updated
		'TICKET_ID' => $array['ticket_id'],
		'LAST_UPDATE_USER' => $array['ticket_reply_status'] == 'user' ? $array['nick'] : $MSG['3500_1015436'],
		'TICKET_TITLE' => ($array['ticket_reply_status'] == 'support' && $array['status'] == 'open') ? '<b>' . $array['title'] . '</b>' : $array['title'],
		'CREATED' => $created, //time that the ticket was created
		'TICKET_STATUS' => $array['status'] == 'open' ? true : false, //ticket is open or closed
		'USER_ID' => $array['user'],
		'USER' => $array['nick'],
	));
}


$template->assign_vars(array(
	'ERROR' => isset($ERR) ? $ERR : '',
	'B_ISERROR' => isset($ERR) ? true : false,
	'MSGCOUNT' => $messages,
));

unset($_SESSION['support_err_message']);
$template->set_filenames(array(
		'body' => 'my_support.tpl'
		));
$template->display('body');
?>
