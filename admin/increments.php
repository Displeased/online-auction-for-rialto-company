<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

function ToBeDeleted($index)
{
	global $delete;

	if (in_array($index, $delete))
	{
		return true;
	}
	return false;
}

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] = 'update')
{
	//edit increments
	$ids = $_POST['id'];
	$increments = $_POST['increments'];
	$lows = $_POST['lows'];
	$highs = $_POST['highs'];
	$delete = (isset($_POST['delete'])) ? $_POST['delete'] : array();
	
	//new increments
	$new_increments = $_POST['new_increments'];
	$new_lows = $_POST['new_lows'];
	$new_highs = $_POST['new_highs'];

	if(!empty($new_increments) && !empty($new_lows) && !empty($new_highs))
	{
		$query = "INSERT INTO " . $DBPrefix . "increments VALUES (NULL, :low, :high, :inc)";
		$params = array();
		$params[] = array(':low', $system->input_money($new_lows), 'float');
		$params[] = array(':high', $system->input_money($new_highs), 'float');
		$params[] = array(':inc', $system->input_money($new_increments), 'float');
		$db->query($query, $params);
		$ERR = $MSG['160_a'];
	}
	else
	{
		for ($i = 0; $i < count($increments); $i++)
		{
			if (!ToBeDeleted($ids[$i]))
			{
				if ($system->input_money($lows[$i]) + $system->input_money($highs[$i]) + $system->input_money($increments[$i]) > 0)
				{
					$query = "UPDATE " . $DBPrefix . "increments SET low = :low, high = :high, increment = :inc WHERE id = :inc_id";
					$params = array();
					$params[] = array(':low', $system->input_money($lows[$i]), 'float');
					$params[] = array(':high', $system->input_money($highs[$i]), 'float');
					$params[] = array(':inc', $system->input_money($increments[$i]), 'float');
					$params[] = array(':inc_id', $ids[$i], 'int');
					$db->query($query, $params);
					$ERR = $MSG['160'];
				}
			}
			else
			{
				$query = "DELETE FROM " . $DBPrefix . "increments WHERE id = :inc_id";
				$params = array();
				$params[] = array(':inc_id', $ids[$i], 'int');
				$db->query($query, $params);
				$ERR = $MSG['160_b'];
			}
		}
	}
}

$query = "SELECT * FROM " . $DBPrefix . "increments ORDER BY low ASC";
$db->direct_query($query);
while ($row = $db->result())
{
	$template->assign_block_vars('increments', array(
		'ID' => $row['id'],
		'HIGH' => $system->print_money_nosymbol($row['high']),
		'LOW' => $system->print_money_nosymbol($row['low']),
		'INCREMENT' => $system->print_money_nosymbol($row['increment'])
	));
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_bid_increments" target="_blank">' . $MSG['128'] . '</a>',
		'ERROR' => (isset($ERR)) ? $ERR : ''
		));

$template->set_filenames(array(
		'body' => 'increments.tpl'
		));
$template->display('body');
?>