<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'tools';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

$log = (isset($_POST['cronlog'])) ? 'y' : 'n';

if($system->SETTINGS['cronlog'] !== $log && isset($_POST['action']) && $_POST['action'] == 'changesettngs')
{
	$query = "UPDATE ". $DBPrefix . "settings SET cronlog = :log";  
	$params = array();
	$params[] = array(':log', $log, 'bool');
	$db->query($query, $params);
	$system->SETTINGS['cronlog'] = $log;
	$ERR = $MSG['3500_1015623'];
}

if (isset($_POST['action']) && $_POST['action'] == 'clearlog' && isset($_POST['id']))
{
	$query = "DELETE FROM " . $DBPrefix . "logs WHERE type = :cron AND id = :log_id";
	$params = array();
	$params[] = array(':cron', 'cron', 'str');
	$params[] = array(':log_id', $_POST['id'], 'int');
	$db->query($query, $params);
	$ERR = $MSG['3500_1015587'];
}
elseif(isset($_POST['action']) && $_POST['action'] == 'clearalllogs')
{
	$query = "DELETE FROM " . $DBPrefix . "logs WHERE type = :cron";
	$params = array();
	$params[] = array(':cron', 'cron', 'str');
	$db->query($query, $params);
	$ERR = $MSG['3500_1015622'];
}


$query = "SELECT * FROM " . $DBPrefix . "logs WHERE type = :cron ORDER BY timestamp DESC";
$params = array();
$params[] = array(':cron', 'cron', 'str');
$db->query($query, $params);
while ($row = $db->result())
{
	$template->assign_block_vars('cron_log', array(
	'ERRORLOG' => $row['message'],
	'PAGENAME' => $MSG['3500_1015589'] . gmdate('F d, Y H:i:s', $row['timestamp']),
	'ID' => $row['id']
	));
}

if ($db->numrows() == 0)
{
	$template->assign_block_vars('cron_log', array(
		'ERRORLOG' => $MSG['3500_1015586'],
		'PAGENAME' => $MSG['3500_1015588']
	));
}

$template->assign_vars(array(
	'ERROR' => (isset($ERR)) ? $ERR : '',
	'SITEURL' => $system->SETTINGS['siteurl'],
	'SETTINGS' => $system->SETTINGS['cronlog'] == 'y' ? 'checked' : '',
	'STATUS' => $system->SETTINGS['cronlog'] == 'y' ? $MSG['3500_1015583'] : $MSG['3500_1015584']
));

$template->set_filenames(array(
		'body' => 'cronlog.tpl'
		));
$template->display('body');
?>
