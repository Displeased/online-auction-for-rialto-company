<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $main_path . 'inc/ckeditor/ckeditor.php';
unset($ERR);

// Insert new message
if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (empty($_POST['question'][$system->SETTINGS['defaultlanguage']]) || empty($_POST['answer'][$system->SETTINGS['defaultlanguage']]))
	{
		$system->SETTINGS = $_POST;
		$ERR = $ERR_067;
	}
	else
	{
		$query = "INSERT INTO " . $DBPrefix . "faqs values (NULL, :q, :d, :c)";
		$params = array();
		$params[] = array(':q', stripslashes($_POST['question'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':d', stripslashes($_POST['answer'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':c', $_POST['category'], 'int');
		$db->query($query, $params);
		$id = $db->lastInsertId();
		// Insert into translation table.
		reset($LANGUAGES);
		foreach ($LANGUAGES as $k => $v)
		{
			$query = "INSERT INTO ".$DBPrefix."faqs_translated VALUES (:i, :l, :q, :a)";
			$params = array();
			$params[] = array(':i', $id, 'int');
			$params[] = array(':l', $k, 'str');
			$params[] = array(':q', stripslashes($_POST['question'][$k]), 'str');
			$params[] = array(':a', stripslashes($_POST['answer'][$k]), 'str');
			$db->query($query, $params);
		}
		header('location: faqs.php');
		exit;
	}
}

// Get data from the database
$query = "SELECT * FROM " . $DBPrefix . "faqscategories";
$db->direct_query($query);

while ($row = $db->result())
{
	$template->assign_block_vars('cats', array(
			'ID' => $row['id'],
			'CATEGORY' => $row['category']
			));
}

$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;
$CKEditor->config['width'] = 550;
$CKEditor->config['height'] = 400;

foreach ($LANGUAGES as $k => $language)
{
	$template->assign_block_vars('lang', array(
			'LANG' => $language,
			'TITLE' => (isset($_POST['question'][$k])) ? $_POST['question'][$k] : '',
			'CONTENT' => $CKEditor->editor('answer[' . $k . ']', $_POST['answer'][$k])
			));
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => $MSG['5231'],
		));

$template->set_filenames(array(
		'body' => 'newfaq.tpl'
		));
$template->display('body');

?>