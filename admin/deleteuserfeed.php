<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/



define('InAdmin', 1);
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

$id = intval($_REQUEST['id']);
$user_id = intval($_REQUEST['user']);

if (isset($_POST['action']) && $_POST['action'] == $MSG['030'])
{
	$query = "DELETE FROM " . $DBPrefix . "feedbacks WHERE id = :i";
	$params = array();
	$params[] = array(':i', $id, 'int');
	$db->query($query, $params);

	$query = "SELECT SUM(rate) as FSUM, count(feedback) as FNUM FROM " . $DBPrefix . "feedbacks WHERE rated_user_id = :i";
	$params = array();
	$params[] = array(':i', $user_id, 'int');
	$db->query($query, $params);
	$fb_data = $db->result();
	
	$query = "UPDATE " . $DBPrefix . "users SET rate_sum = :s, rate_num = :n WHERE id = :u";
	$params = array();
	$params[] = array(':s', $fb_data['SUM'], 'int');
	$params[] = array(':n', $fb_data['NUM'], 'int');
	$params[] = array(':u', $user_id, 'int');
	$db->query($query, $params);

	header('location: userfeedback.php?id=' . $user_id);
	exit;
}
elseif (isset($_POST['action']) && $_POST['action'] == $MSG['029'])
{
	header('location: userfeedback.php?id=' . $user_id);
	exit;
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'ID' => $id,
		'USERID' => $user_id,
		'MESSAGE' => sprintf($MSG['848'], $id),
		'TYPE' => 2
		));

$template->set_filenames(array(
		'body' => 'confirm.tpl'
		));
$template->display('body');

?>