<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'tools';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $main_path . "inc/ckeditor/ckeditor.php";
include $include_path . 'htmLawed.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Check if the specified user exists
	$superuser = $system->cleanvars($_POST['superuser']);
	$query = "SELECT id FROM " . $DBPrefix . "users WHERE nick = :n";
	$params = array();
	$params[] = array(':n', $superuser, 'int');
	$db->query($query, $params);
	if ($db->numrows() == 0 && $_POST['active'] == 'y')
	{
		$ERR = $ERR_025;
	}
	else
	{
		// Update database
		$text = htmLawed($_POST['maintainancetext'], array('safe'=>1));
		$query = "UPDATE " . $DBPrefix . "maintainance SET superuser = :s, maintainancetext = :t, active = :a";
		$params = array();
		$params[] = array(':s', $superuser, 'int');
		$params[] = array(':t', $text, 'str');
		$params[] = array(':a', $_POST['active'], 'str');
		$db->query($query, $params);

		$ERR = $MSG['_0005'];
	}
	$system->SETTINGS['superuser'] = $_POST['superuser'];
	$system->SETTINGS['maintainancetext'] = $_POST['maintainancetext'];		
	$system->SETTINGS['active'] = $_POST['active'];
}
else
{
	$query = "SELECT * FROM " . $DBPrefix . "maintainance LIMIT :l";
	$params = array();
	$params[] = array(':l', 1, 'int');
	$db->query($query, $params);
	$data = $db->result();
	$system->SETTINGS['superuser'] = $data['superuser'];
	$system->SETTINGS['maintainancetext'] = $data['maintainancetext'];		
	$system->SETTINGS['active'] = $data['active'];
}

loadblock('', $MSG['_0002']);
loadblock($MSG['_0006'], '', 'yesno', 'active', $system->SETTINGS['active'], array($MSG['030'], $MSG['029']));
loadblock($MSG['003'], '', 'text', 'superuser', $system->SETTINGS['superuser'], array($MSG['030'], $MSG['029']));

$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;

loadblock($MSG['_0004'], '', $CKEditor->editor('maintainancetext', stripslashes($system->SETTINGS['maintainancetext'])));

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['5436'],
		'PAGENAME' => $MSG['_0001']
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
