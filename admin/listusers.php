<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['resend_email']) && $_POST['resend_email'] == 'send' && isset($_POST['user_id']) && is_numeric($_POST['user_id']))
{
	$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :user_id";
	$params = array();
	$params[] = array(':user_id', $_POST['id'], 'int');
	$db->query($query, $params);
	if ($db->numrows() == 1)
	{
		$USER = $db->result();
		$send_email->reply_user($USER['id'], $USER['nick'], $USER['name'], $USER['email']);
		$ERR = $MSG['059'];
	}
}
elseif(isset($_POST['payreminder_email']) && $_POST['payreminder_email'] == 'send' && isset($_POST['user_id']) && is_numeric($_POST['user_id']))
{
	$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :user_id";
	$params = array();
	$params[] = array(':user_id', $_POST['user_id'], 'int');
	$db->query($query, $params);
	if ($db->numrows() == 1)
	{
		$USER = $db->result();

		// send message
		$send_email->payment_reminder($USER['name'], $USER['balance'], $USER['id'], $USER['email']);
		$ERR = $MSG['765'];
	}
}

if (isset($_GET['usersfilter']))
{
	$_SESSION['usersfilter'] = $_GET['usersfilter'];
	switch($_GET['usersfilter'])
	{
		case 'all':
			unset($_SESSION['usersfilter']);
			unset($Q);
		break;
		case 'active':
			$Q = 0;
		break;
		case 'admin':
			$Q = 1;
		break;
		case 'confirmed':
			$Q = 8;
		break;
		case 'fee':
			$Q = 9;
		break;
		case 'admin_approve':
			$Q = 10;
		break;
	}
}
elseif (!isset($_GET['usersfilter']) && isset($_SESSION['usersfilter']))
{
	switch($_SESSION['usersfilter'])
	{
		case 'active':
			$Q = 0;
		break;
		case 'admin':
			$Q = 1;
		break;
		case 'confirmed':
			$Q = 8;
		break;
		case 'fee':
			$Q = 9;
		break;
		case 'admin_approve':
			$Q = 10;
		break;
	}
}
else
{
	unset($_SESSION['usersfilter']);
	unset($Q);
}

// Retrieve active auctions from the database
if (isset($Q))
{
	$query = "SELECT COUNT(id) as COUNT FROM " . $DBPrefix . "users WHERE suspended = :s";
	$params = array();
	$params[] = array(':s', $Q, 'int');
	$db->query($query, $params);
}
elseif (isset($_POST['keyword']))
{
	$keyword = $system->cleanvars($_POST['keyword']);
	$query = "SELECT COUNT(id) as COUNT FROM " . $DBPrefix . "users WHERE name LIKE :nl OR nick LIKE :nkl OR email LIKE :el";
	$params = array();
	$params[] = array(':nl', '%' . $keyword . '%', 'str');
	$params[] = array(':nkl', '%' . $keyword . '%', 'str');
	$params[] = array(':el', '%' . $keyword . '%', 'str');
	$db->query($query, $params);
}
else
{
	$query = "SELECT COUNT(id) as COUNT FROM " . $DBPrefix . "users";
	$db->direct_query($query);
}
$TOTALUSERS = $db->result('COUNT');

// get page limits
if (isset($_GET['PAGE']) && is_numeric($_GET['PAGE']))
{
	$PAGE = intval($_GET['PAGE']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}
elseif (isset($_SESSION['RETURN_LIST_OFFSET']) && $_SESSION['RETURN_LIST'] == 'listusers.php')
{
	$PAGE = intval($_SESSION['RETURN_LIST_OFFSET']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}
else
{
	$OFFSET = 0;
	$PAGE = 1;
}

$_SESSION['RETURN_LIST'] = 'listusers.php';
$_SESSION['RETURN_LIST_OFFSET'] = $PAGE;
$PAGES = ($TOTALUSERS == 0) ? 1 : ceil($TOTALUSERS / $system->SETTINGS['perpage']);
$params = array();
if (isset($Q))
{
	$query = "SELECT * FROM " . $DBPrefix . "users WHERE suspended = :s";
	$params[] = array(':s', $Q, 'str');
}
elseif (isset($_POST['keyword']))
{
	$query = "SELECT * FROM " . $DBPrefix . "users WHERE name LIKE :ln OR nick LIKE :lnk OR email LIKE :le";
	$params[] = array(':ln', '%' . $keyword . '%', 'str');
	$params[] = array(':lnk', '%' . $keyword . '%', 'str');
	$params[] = array(':le', '%' . $keyword . '%', 'str');
}
else
{
	$query = "SELECT * FROM " . $DBPrefix . "users";
}
$query .= " ORDER BY nick"; // ordered by
$query .= " LIMIT :o, :p";
$params[] = array(':o', $OFFSET, 'int');
$params[] = array(':p', $system->SETTINGS['perpage'], 'int');
$db->query($query, $params);

$bg = '';
while ($row = $db->result())
{
	$template->assign_block_vars('users', array(
			'ID' => $row['id'],
			'NICK' => $row['nick'],
			'NAME' => $row['name'],
			'COUNTRY' => $row['country'],
			'EMAIL' => $row['email'],
			'NEWSLETTER' => ($row['nletter'] == 1) ? $MSG['030'] : $MSG['029'],
			'SUSPENDED' => $row['suspended'],
			'BALANCE' => $system->print_money($row['balance'], true, false),
			'BALANCE_CLEAN' => $row['balance'],
			'PAGENAME' => $MSG['045'],
			'BG' => $bg
			));
	$bg = ($bg == '') ? 'class="bg"' : '';
}

// get pagenation
$PREV = intval($PAGE - 1);
$NEXT = intval($PAGE + 1);
if ($PAGES > 1)
{
	$LOW = $PAGE - 5;
	if ($LOW <= 0) $LOW = 1;
	$COUNTER = $LOW;
	while ($COUNTER <= $PAGES && $COUNTER < ($PAGE + 6))
	{
		$template->assign_block_vars('pages', array(
				'PAGE' => ($PAGE == $COUNTER) ? '<b>' . $COUNTER . '</b>' : '<a href="' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/listusers.php?PAGE=' . $COUNTER . '"><u>' . $COUNTER . '</u></a>'
				));
		$COUNTER++;
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'TOTALUSERS' => $TOTALUSERS,
		'PAGENAME' => $MSG['045'],
		'USERFILTER' => (isset($_SESSION['usersfilter'])) ? $_SESSION['usersfilter'] : '',

		'PREV' => ($PAGES > 1 && $PAGE > 1) ? '<a href="' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/listusers.php?PAGE=' . $PREV . '"><u>' . $MSG['5119'] . '</u></a>&nbsp;&nbsp;' : '',
		'NEXT' => ($PAGE < $PAGES) ? '<a href="' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/listusers.php?PAGE=' . $NEXT . '"><u>' . $MSG['5120'] . '</u></a>' : '',
		'PAGE' => $PAGE,
		'PAGES' => $PAGES
		));
		
$template->set_filenames(array(
		'body' => 'listusers.tpl'
		));
$template->display('body');
?>
