<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'fees';
$extraJs = ';js/calendar.php';
include '../common.php';
include $include_path . 'functions_admin.php';
include $include_path . 'dates.inc.php';
include 'loggedin.inc.php';

unset($ERR);

// get form variables
$list_type = isset($_GET['type']) ? intval($_GET['type']) : 'a';
$from_date = isset($_GET['from_date']) ? intval($_GET['from_date']) : 0;
$to_date = isset($_GET['to_date']) ? intval($_GET['to_date']) : 0;

// Set offset and limit for pagination
if (isset($_GET['PAGE']) && is_numeric($_GET['PAGE']))
{
	$PAGE = intval($_GET['PAGE']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}
elseif (isset($_SESSION['RETURN_LIST_OFFSET']) && $_SESSION['RETURN_LIST'] == 'accounts.php')
{
	$PAGE = intval($_SESSION['RETURN_LIST_OFFSET']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}
else
{
	$OFFSET = 0;
	$PAGE = 1;
}

$where_sql = '';
if ($from_date != 0)
{
	$where_sql = 'paid_date > \'' . FormatTimeStamp($from_date, '-') . '\'';
}
if ($to_date != 0)
{
	if (!empty($where_sql))
	{
		$where_sql .= ' AND ';
	}
	$where_sql .= 'paid_date < \'' . FormatTimeStamp($to_date, '-') . '\'';
}

if ($list_type == 'm' || $list_type == 'w' || $list_type == 'd')
{
	$OFFSET = 0;
	$PAGE = 1;
	$PAGES = 1;
	$show_pagnation = false;
	
	if ($list_type == 'm')
	{
		$query = "SELECT *, SUM(amount) As total FROM " . $DBPrefix . "accounts
				" . ((!empty($where_sql)) ? ' WHERE ' . $where_sql : '') . "
				GROUP BY month, year ORDER BY year, month";
	}
	elseif ($list_type == 'w')
	{
		$query = "SELECT *, SUM(amount) As total FROM " . $DBPrefix . "accounts
				" . ((!empty($where_sql)) ? ' WHERE ' . $where_sql : '') . "
				GROUP BY week, year ORDER BY year, week";
	}
	else
	{
		$query = "SELECT *, SUM(amount) As total FROM " . $DBPrefix . "accounts
				" . ((!empty($where_sql)) ? ' WHERE ' . $where_sql : '') . "
				GROUP BY day, year ORDER BY year, day";
	}
	$db->direct_query($query);
	
	$bg = '';
	while ($row = $db->result())
	{
		if ($list_type == 'm')
		{
			$date = $MSG['MON_0' . $row['month'] . 'E'] . ', ' . $row['year'];
		}
		elseif ($list_type == 'w')
		{
			$date = $MSG['828'] . ' ' . $row['week'] . ', ' . $row['year'];
		}
		else
		{
			$date = FormatDate($row['paid_date']);
		}
		$template->assign_block_vars('accounts', array(
				'DATE' => $date,
				'AMOUNT' => $system->print_money($row['amount'], true, false),
				'BG' => $bg
				));
		$bg = ($bg == '') ? 'class="bg"' : '';
	}
}
else
{
	$_SESSION['RETURN_LIST'] = 'accounts.php';
	$_SESSION['RETURN_LIST_OFFSET'] = $PAGE;
	$show_pagnation = true;

	$query = "SELECT COUNT(id) As accounts FROM " . $DBPrefix . "accounts" . ((!empty($where_sql)) ? ' WHERE :sql' : '');
	$params = array();
	$params[] = array(':sql', $where_sql, 'str');
	$db->query($query, $params);
	$num_accounts = $db->result('accounts');
	$PAGES = ($num_accounts == 0) ? 1 : ceil($num_accounts / $system->SETTINGS['perpage']);
	$query = "SELECT * FROM " . $DBPrefix . "accounts
			" . ((!empty($where_sql)) ? ' WHERE :sql' : '') . " ORDER BY paid_date LIMIT :off, :page";
	$params = array();
	$params[] = array(':sql', $where_sql, 'str');
	$params[] = array(':off', $OFFSET, 'int');
	$params[] = array(':page', $system->SETTINGS['perpage'], 'int');
	$db->query($query, $params);

	$bg = '';
	while ($row = $db->result())
	{
		$template->assign_block_vars('accounts', array(
				'ID' => $row['id'],
				'NICK' => $row['nick'],
				'RNAME' => $row['name'],
				'DATE' => ArrangeDateNoCorrection($row['paid_date']),
				'AMOUNT' => $system->print_money($row['amount'], true, false),
				'TEXT' => $row['text'],
				'BG' => $bg
				));
		$bg = ($bg == '') ? 'class="bg"' : '';
	}
}

// get pagenation
$PREV = intval($PAGE - 1);
$NEXT = intval($PAGE + 1);
if ($PAGES > 1)
{
	$LOW = $PAGE - 5;
	if ($LOW <= 0) $LOW = 1;
	$COUNTER = $LOW;
	while ($COUNTER <= $PAGES && $COUNTER < ($PAGE + 6))
	{
		$template->assign_block_vars('pages', array(
				'PAGE' => ($PAGE == $COUNTER) ? '<b>' . $COUNTER . '</b>' : '<a href="' . $system->SETTINGS['siteurl'] . 'admin/accounts.php?PAGE=' . $COUNTER . '"><u>' . $COUNTER . '</u></a>'
				));
		$COUNTER++;
	}
}

$template->assign_vars(array(
		'TYPE' => $list_type,
		'PAGENAME' => $MSG['854'],
		'FROM_DATE' => ($from_date == 0) ? '' : $from_date,
		'TO_DATE' => ($to_date == 0) ? '' : $to_date,

		'PAGNATION' => $show_pagnation,
		'PREV' => ($PAGES > 1 && $PAGE > 1) ? '<a href="' . $system->SETTINGS['siteurl'] . 'admin/accounts.php?PAGE=' . $PREV . '"><u>' . $MSG['5119'] . '</u></a>&nbsp;&nbsp;' : '',
		'NEXT' => ($PAGE < $PAGES) ? '<a href="' . $system->SETTINGS['siteurl'] . 'admin/accounts.php?PAGE=' . $NEXT . '"><u>' . $MSG['5120'] . '</u></a>' : '',
		'PAGE' => $PAGE,
		'PAGES' => $PAGES
		));

$template->set_filenames(array(
		'body' => 'accounts.tpl'
		));
$template->display('body');
?>
