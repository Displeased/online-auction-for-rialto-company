<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'tools';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'clearlog')
{
	$query = "DELETE FROM " . $DBPrefix . "logs WHERE type = :errors";
	$params = array();
	$params[] = array(':errors', 'error', 'str');
	$db->query($query, $params);
	$ERR = $MSG['889'];
}

$data = '';
$query = "SELECT * FROM " . $DBPrefix . "logs WHERE type = :errors";
$params = array();
$params[] = array(':errors', 'error', 'str');
$db->query($query, $params);
while ($row = $db->result())
{
	$data .= '<strong>' . date('d-m-Y, H:i:s', $row['timestamp']) . '</strong>: ' . $row['message'] . '<br>';
}

if ($data == '')
{
	$data = $MSG['888'];
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'PAGENAME' => $MSG['891'],
		'ERRORLOG' => $data
		));

$template->set_filenames(array(
		'body' => 'errorlog.tpl'
		));
$template->display('body');
?>
