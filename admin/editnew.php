<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include $include_path . 'htmLawed.php';
include 'loggedin.inc.php';
include $main_path . 'inc/ckeditor/ckeditor.php';

if (!isset($_POST['id']) && (!isset($_GET['id']) || empty($_GET['id'])))
{
	header('location: news.php');
	exit;
}

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Data check
	if (empty($_POST['title']) || empty($_POST['content']))
	{
		$ERR = $ERR_112;
	}
	else
	{
		// clean up everything

		$news_id = intval($_POST['id']);
		$query = "UPDATE " . $DBPrefix . "news SET title = :t, content = :c, suspended = :s WHERE id = :i";
		$params = array();
		$params[] = array(':t', stripslashes($_POST['title'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':c', stripslashes($_POST['content'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':s', intval($_POST['suspended']), 'int');
		$params[] = array(':i', $news_id, 'int');
		$db->query($query, $params);


		foreach ($LANGUAGES as $k => $v)
		{
			$query = "SELECT id FROM " . $DBPrefix . "news_translated WHERE lang = :l AND id = :i";
			$params = array();
			$params[] = array(':l', $k, 'str');
			$params[] = array(':i', $news_id, 'int');
			$db->query($query, $params);
			
			$ex_params = array();
			if ($db->numrows('id') > 0)
			{
				$query = "UPDATE " . $DBPrefix . "news_translated SET title = :t, content = :c WHERE lang = :l AND id = :i";
				$ex_params[] = array(':t', stripslashes($_POST['title'][$k]), 'str');
				$ex_params[] = array(':c', stripslashes($_POST['content'][$k]), 'str');
				$ex_params[] = array(':l', $k, 'str');
				$ex_params[] = array(':i', $news_id, 'int');
			}
			else
			{
				$query = "INSERT INTO " . $DBPrefix . "news_translated VALUES (:i, :l, :t, :c)";
				$ex_params[] = array(':i', $news_id, 'int');
				$ex_params[] = array(':l', $k, 'str');
				$ex_params[] = array(':t', stripslashes($_POST['title'][$k]), 'str');
				$ex_params[] = array(':c', stripslashes($_POST['content'][$k]), 'str');
			}
			$db->query($query, $ex_params);
		}
		header('location: news.php');
		exit;
	}
}


// get news story
$query = "SELECT t.*, n.suspended FROM " . $DBPrefix . "news_translated t
		LEFT JOIN " . $DBPrefix . "news n ON (n.id = t.id) WHERE t.id = :i";
$params = array();
$params[] = array(':i', intval($_GET['id']), 'int');
$db->query($query, $params);

$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;
$CKEditor->config['width'] = 550;
$CKEditor->config['height'] = 400;

$CONT_tr = array();
$TIT_tr = array();
while ($arr = $db->result())
{
	$suspended = $arr['suspended'];
	$template->assign_block_vars('lang', array(
			'LANG' => $arr['lang'],
			'TITLE' => $arr['title'],
			'CONTENT' => $CKEditor->editor('content[' . $arr['lang'] . ']', $system->uncleanvars($arr['content']))
			));
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'TITLE' => $MSG['343'],
		'BUTTON' => $MSG['530'],
		'ID' => intval($_GET['id']),
		'PAGENAME' => $MSG['5278'],
		'B_ACTIVE' => ((isset($suspended) && $suspended == 0) || !isset($suspended)),
		'B_INACTIVE' => (isset($suspended) && $suspended == 1),
		));

$template->set_filenames(array(
		'body' => 'addnew.tpl'
		));
$template->display('body');

?>