<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

// Insert new message board
if (isset($_POST['action']) && $_POST['action'] == 'insert')
{
	if (empty($_POST['name']) || empty($_POST['msgstoshow']) || empty($_POST['active']))
	{
		$ERR = $ERR_047;
	}
	elseif (!is_numeric($_POST['msgstoshow']))
	{
		$ERR = $ERR_5000;
	}
	elseif (intval($_POST['msgstoshow'] == 0))
	{
		$ERR = $ERR_5001;
	}
	else
	{
		$query = "INSERT INTO " . $DBPrefix . "community VALUES (NULL, :n, 0, 0, :m, :a)";
		$params = array();
		$params[] = array(':n', $system->cleanvars($_POST['name']), 'str');
		$params[] = array(':m', intval($_POST['msgstoshow']), 'int');
		$params[] = array(':a', intval($_POST['active']), 'int');
		$db->query($query, $params);

		header('location: boards.php');
		exit;
	}
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ERROR' => (isset($ERR)) ? $ERR : '',

		'NAME' => (isset($_POST['name'])) ? $_POST['name'] : '',
		'PAGENAME' => $MSG['5031'],
		'MSGTOSHOW' => (isset($_POST['msgstoshow'])) ? $_POST['msgstoshow'] : '',
		'B_ACTIVE' => ((isset($_POST['active']) && $_POST['active'] == 1) || !isset($_POST['active'])),
		'B_DEACTIVE' => (isset($_POST['active']) && $_POST['active'] == 2)
		));

$template->set_filenames(array(
		'body' => 'newboard.tpl'
		));
$template->display('body');

?>