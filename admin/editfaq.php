<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $main_path . 'inc/ckeditor/ckeditor.php';
// Default for error message (blank)
unset($ERR);

// Update message
if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (empty($_POST['question'][$system->SETTINGS['defaultlanguage']])
		|| empty($_POST['answer'][$system->SETTINGS['defaultlanguage']]))
	{
		$ERR = $ERR_067;
		$faq = $_POST;
	}
	else
	{
		$query = "UPDATE " . $DBPrefix . "faqs SET category = :c, question = :q, answer = :a WHERE id = :i";
		$params = array();
		$params[] = array(':c', $_POST['category'], 'int');
		$params[] = array(':q', stripslashes($_POST['question'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':a', stripslashes($_POST['answer'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':i', $_POST['id'], 'int');
		$db->query($query, $params);

		reset($LANGUAGES);
		foreach ($LANGUAGES as $k => $v)
		{
			$query = "SELECT question FROM " . $DBPrefix . "faqs_translated WHERE lang = :l AND id = :i";
			$params = array();
			$params[] = array(':l', $k, 'str');
			$params[] = array(':i', $_POST['id'], 'int');
			$db->query($query, $params);
			if ($db->numrows() > 0)
			{
				$query = "UPDATE " . $DBPrefix . "faqs_translated SET question = :q, answer = :a WHERE id = :i AND lang = :l";
				$params = array();
				$params[] = array(':q', stripslashes($_POST['question'][$k]), 'str');
				$params[] = array(':a', stripslashes($_POST['answer'][$k]), 'str');
				$params[] = array(':i', $_POST['id'], 'int');
				$params[] = array(':l', $k, 'str');
			}
			else
			{
				$query = "INSERT INTO " . $DBPrefix . "faqs_translated VALUES (:i, :l, :q, :a)";
				$params = array();
				$params[] = array(':i', $_POST['id'], 'int');
				$params[] = array(':l', $k, 'str');
				$params[] = array(':q', stripslashes($_POST['question'][$k]), 'str');
				$params[] = array(':a', stripslashes($_POST['answer'][$k]), 'str');
			}
			$db->query($query, $params);
		}  
		header('location: faqs.php');
		exit;
	}
}

// load categories
$query = "SELECT * FROM " . $DBPrefix . "faqscategories ORDER BY :o";
$params = array();
$params[] = array(':o', 'category', 'str');
$db->query($query, $params);
while ($row = $db->result())
{
	$template->assign_block_vars('cats', array(
			'ID' => $row['id'],
			'CAT' => $row['category']
			));
}

$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;
$CKEditor->config['width'] = 550;
$CKEditor->config['height'] = 400;

// Get data from the database
$query = "SELECT * FROM " . $DBPrefix . "faqs_translated WHERE id = :i";
$params = array();
$params[] = array(':i', $_GET['id'], 'int');
$db->query($query, $params);
while ($row = $db->result())
{
	$QUESTION_tr[$row['lang']] = $row['question'];
	$ANSWER_tr[$row['lang']] = $row['answer'];
}
				
reset($LANGUAGES);
foreach ($LANGUAGES as $k => $v)
{
	$template->assign_block_vars('qs', array(
			'LANG' => $k,
			'QUESTION' => (isset($_POST['question'][$k])) ? $_POST['question'][$k] : $QUESTION_tr[$k]
			));
	$template->assign_block_vars('as', array(
			'LANG' => $k,
			'ANSWER' => $CKEditor->editor('answer[' . $k . ']', $ANSWER_tr[$k])
			));
}

// Get data from the database
$query = "SELECT * FROM " . $DBPrefix . "faqs WHERE id = :i";
$params = array();
$params[] = array(':i', $_GET['id'], 'int');
$db->query($query, $params);
$faq = $db->result();

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'ID' => $faq['id'],
		'FAQ_NAME' => $faq['question'],
		'PAGENAME' => $MSG['5241'],
		'FAQ_CAT' => $faq['category']
		));

$template->set_filenames(array(
		'body' => 'editfaq.tpl'
		));
$template->display('body');

?>