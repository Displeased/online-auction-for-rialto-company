<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	//checking to see if any posts are N and preset them
	if (empty($_POST['auctions']) && $_POST['auctions'] != 'y') $_POST['auctions'] = 'n';
	if (empty($_POST['online']) && $_POST['online'] != 'y') $_POST['online'] = 'n';
	if (empty($_POST['users_online']) && $_POST['users_online'] != 'y') $_POST['users_online'] = 'n';
	if (empty($_POST['cat_counters']) && $_POST['cat_counters'] != 'y') $_POST['cat_counters'] = 'n';
	
	//set the salts
	$auctions = $_POST['auctions'];
	$guest = $_POST['online'];
	$users_online = $_POST['users_online'];
	$cat = $_POST['cat_counters'];
	
	// Update database
	$query = "UPDATE " . $DBPrefix . "settings SET counter_auctions = :auc, counter_online = :guest, counter_users_online = :user_online, cat_counters = :cat";
	$params = array();
	$params[] = array(':auc', $auctions, 'bool');
	$params[] = array(':guest', $guest, 'bool');
	$params[] = array(':user_online', $users_online, 'bool');
	$params[] = array(':cat', $cat, 'bool');
	$db->query($query, $params);

	$system->SETTINGS['counter_auctions'] = $auctions;
	$system->SETTINGS['counter_online'] = $guest;
	$system->SETTINGS['counter_users_online'] = $users_online;
	$system->SETTINGS['cat_counters'] = $cat;
	$ERR = $MSG['2__0063'];
}

loadblock($MSG['2__0062'], $MSG['2__0058']);
loadblock($MSG['2__0060'], '', 'checkbox', 'auctions', $system->SETTINGS['counter_auctions']);
loadblock($MSG['2__00642'], '', 'checkbox', 'online', $system->SETTINGS['counter_online']);
loadblock($MSG['2__100591'], '', 'checkbox', 'users_online', $system->SETTINGS['counter_users_online']);
loadblock($MSG['276'], '', 'checkbox', 'cat_counters', $system->SETTINGS['cat_counters']);

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['25_0008'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_show_counters" target="_blank">' . $MSG['2__0057'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
