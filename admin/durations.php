<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// update durations table
	$rebuilt_durations = array();
	$rebuilt_days = array();

	foreach ($_POST['new_durations'] as $k => $v)
	{
		if ((isset($_POST['delete']) && !in_array($k, $_POST['delete']) || !isset($_POST['delete'])) && !empty($_POST['new_durations'][$k]) && !empty($_POST['new_days'][$k]))
		{
			$rebuilt_durations[] = $_POST['new_durations'][$k];
			$rebuilt_days[] = $_POST['new_days'][$k];
		}
	}

	$query = "DELETE FROM " . $DBPrefix . "durations";
	$db->direct_query($query);


	for ($i = 0; $i < count($rebuilt_durations); $i++)
	{
		$query = "INSERT INTO " . $DBPrefix . "durations VALUES (:dy, :ds)";
		$params = array();
		$params[] = array(':dy', $rebuilt_days[$i], 'int');
		$params[] = array(':ds', $system->cleanvars($rebuilt_durations[$i]), 'str');
		$db->query($query, $params);
	}

	$ERR = $MSG['123'];
}

$query = "SELECT * FROM " . $DBPrefix . "durations ORDER BY :d";
$params = array();
$params[] = array(':d', 'days', 'str');
$db->query($query, $params);

$i = 0;
while ($row = $db->result())
{
	$template->assign_block_vars('dur', array(
			'ID' => $i,
			'DAYS' => $row['days'],
			'DESC' => $row['description']
			));
	$i++;
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_auctions_duration" target="_blank">' . $MSG['069'] . '</a>',
		'ERROR' => (isset($ERR)) ? $ERR : ''
		));

$template->set_filenames(array(
		'body' => 'durations.tpl'
		));
$template->display('body');

?>