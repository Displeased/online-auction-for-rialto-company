<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'Digital Item MINE Type Settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

if(isset($_POST['action']) && $_POST['action'] == 'edit' && is_array($_POST['id']) && is_array($_POST['mine']) && is_array($_POST['extension']) && is_array($_POST['used']))
{
	foreach($_POST['id'] as $id)
	{
		$query = "UPDATE " . $DBPrefix . "digital_item_mime SET name = :name, mine_type = :mine, file_extension = :extension, use_mime = :used WHERE id = :id";
		$params = array();
		$params[] = array(':name', $_POST['name'][$id], 'str');
		$params[] = array(':mine', $_POST['mine'][$id], 'str');
		$params[] = array(':extension', $_POST['extension'][$id], 'str');
		$params[] = array(':used', $_POST['used'][$id], 'bool');
		$params[] = array(':id', $id, 'int');
		$db->query($query, $params);
		$ERR = $MSG['3500_1015775'];
	}
}
if(isset($_POST['action']) && $_POST['action'] == 'new')
{
	$query = "INSERT INTO " . $DBPrefix . "digital_item_mime VALUES (NULL, :name, :mine, :extension, :used)";
	$params = array();
	$params[] = array(':name', $_POST['name'], 'str');
	$params[] = array(':mine', $_POST['mine'], 'str');
	$params[] = array(':extension', $_POST['extension'], 'str');
	$params[] = array(':used', $_POST['used'], 'bool');
	$db->query($query, $params);
	$ERR = $MSG['3500_1015774'];

}
if (isset($_POST['delete']) && is_array($_POST['delete']))
{
	foreach ($_POST['delete'] as $v)
	{
		$query = "DELETE FROM " . $DBPrefix . "digital_item_mime WHERE id = :id";
		$params = array();
		$params[] = array(':id', $v, 'int');
		$db->query($query, $params);
		$ERR = $MSG['3500_1015776'];
	}
}

// Get data from the database
$query = "SELECT * FROM " . $DBPrefix . "digital_item_mime ORDER BY name ASC";
$db->direct_query($query);
while ($row = $db->result())
{
	$template->assign_block_vars('mime_type', array(
		'ID' => $row['id'],
		'NAME' => $row['name'],
		'MINE' => $row['mine_type'],
		'EXTENSION' => $row['file_extension'],
		'USED_Y' => $row['use_mime'] == 'y' ? 'checked' : '',
		'USED_N' => $row['use_mime'] == 'n' ? 'checked' : '',
	));
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['5142'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_asettings" target="_blank">' . $MSG['3500_1015770'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'minetype.tpl'
		));
$template->display('body');
?>
