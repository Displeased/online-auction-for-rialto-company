<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Update database
	$query = "UPDATE " . $DBPrefix . "settings set timecorrection = :tc, datesformat = :df, daylight_savings = :dst";
	$params = array();
	$params[] = array(':tc', $_POST['timecorrection'], 'str');
	$params[] = array(':df', $_POST['datesformating'], 'str');
	$params[] = array(':dst', $_POST['dst'], 'bool');
	$db->query($query, $params);
	
	$system->SETTINGS['timecorrection'] = $_POST['timecorrection'];
	$system->SETTINGS['datesformat'] = $_POST['datesformating'];
	$system->SETTINGS['daylight_savings'] = $_POST['dst'];
	$ERR = $MSG['347'];
}

$TIMECORRECTION = array();
for ($i = 21; $i > -15; $i--)
{
	$TIMECORRECTION[$i] = $MSG['TZ_' . $i];
}
$selectsetting = $system->SETTINGS['timecorrection'];

$html = generateSelect('timecorrection', $TIMECORRECTION);

//load the template
loadblock($MSG['363'], $MSG['379'], 'datestacked', 'datesformating', $system->SETTINGS['datesformat'], array($MSG['382'], $MSG['383']));
loadblock($MSG['346'], $MSG['345'], 'dropdown', 'timecorrection', $system->SETTINGS['timecorrection']);
loadblock($MSG['3500_1015735'], '', 'yesno', 'dst', $system->SETTINGS['daylight_savings'], array($MSG['030'], $MSG['029']));

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'OPTIONHTML' => $html,
		'TYPENAME' => $MSG['25_0008'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_time_settings" target="_blank">' . $MSG['344'] . '</a>',
		'DROPDOWN' => $html
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
