<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['delete']) && is_array($_POST['delete']))
{
	foreach ($_POST['delete'] as $val)
	{
		$query = "DELETE FROM " . $DBPrefix . "faqs WHERE id = :i";
		$params = array();
		$params[] = array(':i', $val, 'int');
		$db->query($query, $params);

		$query = "DELETE FROM " . $DBPrefix . "faqs_translated WHERE id = :i";
		$params = array();
		$params[] = array(':i', $val, 'int');
		$db->query($query, $params);
	}
}

// Get data from the database
$query = "SELECT id, category FROM " . $DBPrefix . "faqscategories";
$db->direct_query($query);
foreach ($db->fetchall() as $row)
{
	$template->assign_block_vars('cats', array(
		'CAT' => '<strong>' . $row['category'] . '<strong>'
	));

	$query = "SELECT id, question FROM " . $DBPrefix . "faqs WHERE category = :id";
	$params = array();
	$params[] = array(':id', $row['id'], 'int');
	$db->query($query, $params);
	
	while ($cat_row = $db->result())
	{
		$template->assign_block_vars('cats.faqs', array(
			'ID' => $cat_row['id'],
			'FAQ' => $cat_row['question']
		));
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => $MSG['5232']
		));

$template->set_filenames(array(
		'body' => 'faqs.tpl'
		));
$template->display('body');

?>