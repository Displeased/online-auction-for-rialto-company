<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'banners';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

// Delete users and banners if necessary
if (isset($_POST['delete']) && is_array($_POST['delete']))
{
	foreach ($_POST['delete'] as $k => $v)
	{
		$query = "DELETE FROM " . $DBPrefix . "banners WHERE user = :i";
		$params = array();
		$params[] = array(':i', $v, 'int');
		$db->query($query, $params);

		$query = "DELETE FROM " . $DBPrefix . "bannersusers WHERE id = :i";
		$params = array();
		$params[] = array(':i', $v, 'int');
		$db->query($query, $params);
	}
}

// Retrieve users from the database
$query = "SELECT u.*, COUNT(b.user) as count FROM " . $DBPrefix . "bannersusers u
		LEFT JOIN " . $DBPrefix . "banners b ON (b.user = u.id)
		GROUP BY u.id ORDER BY u.name";
$db->direct_query($query);
$bg = '';
while ($row = $db->result())
{
	$template->assign_block_vars('busers', array(
			'ID' => $row['id'],
			'NAME' => $row['name'],
			'COMPANY' => $row['company'],
			'EMAIL' => $row['email'],
			'NUM_BANNERS' => $row['count'],
			'BG' => $bg
			));
	$bg = ($bg == '') ? 'class="bg"' : '';
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_banners_administration" target="_blank">' . $MSG['_0008'] . '</a>',
		));

$template->set_filenames(array(
		'body' => 'managebanners.tpl'
		));
$template->display('body');
?>
