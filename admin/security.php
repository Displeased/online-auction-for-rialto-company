<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{	
	// Update data
	$query = "UPDATE " . $DBPrefix . "settings SET https = :ssl, https_url = :ssl_url";
	$params = array();
	$params[] = array(':ssl', $_POST['https'], 'str');
	$params[] = array(':ssl_url', $_POST['https_url'], 'str');
	$params[] = array(':cookie', $_POST['cookie_name'], 'str');
	$params[] = array(':sessions', $_POST['sessions_name'], 'str');
	$params[] = array(':custom', $_POST['custom_code'], 'str');
	$db->query($query, $params);
	
	// changing the admin folder name and updating the robots.txt
	$stored_folder = $main_path . $system->SETTINGS['admin_folder'];
	if ((is_dir($stored_folder)) && (isset($_POST['new_admin_folder'])))
	{
		cheange_admin_folder($_POST['new_admin_folder'], $stored_folder);
	}	
	
	$system->SETTINGS['cookie_name'] = $_POST['cookie_name'];
	$system->SETTINGS['sessions_name'] = $_POST['sessions_name'];
	$system->SETTINGS['custom_code'] = $_POST['custom_code'];
	$system->SETTINGS['https'] = $_POST['https'];
	$system->SETTINGS['https_url'] = $_POST['https_url'];
	$system->SETTINGS['admin_folder'] = $_POST['new_admin_folder'];
	$ERR = $MSG['3500_1015544'];
}

// SLL settings
loadblock($MSG['1022'], '', '', '', '', array(), true);
loadblock($MSG['1023'], $MSG['1024'], 'yesno', 'https', $system->SETTINGS['https'], array($MSG['030'], $MSG['029']));
loadblock($MSG['801'], $MSG['802'], 'text', 'https_url', $system->SETTINGS['https_url']);

// Changing the admin folder name/path
loadblock($MSG['3500_1015573'], '', '', '', '', array(), true);

$block_2 = '
<script type="text/javascript">
	function randomFolderName(length)
	{
	  chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	  pass = "";
	  for(x=0;x<length;x++)
	  {
	    i = Math.floor(Math.random() * 52);
	    pass += chars.charAt(i);
	  }
	  return pass;
	}
	function folderNameSubmit()
	{
	  conf.new_admin_folder.value = randomFolderName(conf.folder_length.value);
	  return false;
	}
</script>
' . $MSG['30_0230'] . '<br>
<input name="new_admin_folder" type="text" style="width:100%" value="' . $system->SETTINGS['admin_folder'] . '"> <br>
<select name="folder_length">
  <option value="1">01</option>
  <option value="2">02</option>
  <option value="3">03</option>
  <option value="4">04</option>
  <option value="5">05</option>
  <option value="6">06</option>
  <option value="7">07</option>
  <option value="8">08</option>
  <option value="9">09</option>
  <option value="10">10</option>
  <option value="11">11</option>
  <option value="12">12</option>
  <option value="13">13</option>
  <option value="14">14</option>
  <option value="15">15</option>
  <option value="16">16</option>
  <option value="17">17</option>
  <option value="18">18</option>
  <option value="19">19</option>
  <option value="20">20</option>
  <option value="21">21</option>
  <option value="22">22</option>
  <option value="23">23</option>
  <option value="24">24</option>
  <option value="25">25</option>
  <option value="26">26</option>
  <option value="27">27</option>
  <option value="28">28</option>
  <option value="29">29</option>
  <option value="30">30</option>
  <option value="31">31</option>
  <option value="32">32</option>
  <option value="33">33</option>
  <option value="34">34</option>
  <option value="35">35</option>
  <option value="36">36</option>
  <option value="37">37</option>
  <option value="38">38</option>
  <option value="39">39</option>
  <option value="40">40</option>
  <option value="41">41</option>
  <option value="42">42</option>
  <option value="43">43</option>
  <option value="44">44</option>
  <option value="45">45</option>
  <option value="46">46</option>
  <option value="47">47</option>
  <option value="48">48</option>
  <option value="49">49</option>
  <option value="50">50</option>
  <option value="51">51</option>
  <option value="52">52</option>
  <option value="53">53</option>
  <option value="54">54</option>
  <option value="55">55</option>
  <option value="56">56</option>
  <option value="57">57</option>
  <option value="58">58</option>
  <option value="59">59</option>
  <option value="60">60</option>
  <option value="71">71</option>
  <option value="72">72</option>
  <option value="73">73</option>
  <option value="74">74</option>
  <option value="75">75</option>
  <option value="76">76</option>
  <option value="77">77</option>
  <option value="78">78</option>
  <option value="79">79</option>
  <option value="80">80</option>
  <option value="81">81</option>
  <option value="82">82</option>
  <option value="83">83</option>
  <option value="84">84</option>
  <option value="85">85</option>
  <option value="86">86</option>
  <option value="87">87</option>
  <option value="88">88</option>
  <option value="89">89</option>
  <option value="90">90</option>
  <option value="91">91</option>
  <option value="92">92</option>
  <option value="93">93</option>
  <option value="94">94</option>
  <option value="95">95</option>
  <option value="96">96</option>
  <option value="97">97</option>
  <option value="98">98</option>
  <option value="99">99</option>
  <option value="100">100</option>
</select>
<input type="button" class="btn btn-success btn-small" value="' . $MSG['3500_1015631'] . '" onClick="javascript:folderNameSubmit()"></button>
</div>';
loadblock($MSG['30_0229'], $block_2);

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['5142'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_security_settings" target="_blank">' . $MSG['3500_1015543'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
