<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

if (isset($_SESSION['csrftoken'])) unset($_SESSION['csrftoken']);
if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_NUMBER'])) unset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_NUMBER']);
if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_PASS'])) unset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_PASS']);
if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN'])) unset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']);
if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_USER'])) unset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_USER']);

?>
<script type="text/javascript">
parent.location.href = 'index.php';
</script>