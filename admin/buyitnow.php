<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	$bn_only_percent = ($_POST['bn_only_percent'] > 100) ? 100 : ($_POST['bn_only_percent'] < 0) ? 0 : intval($_POST['bn_only_percent']);
	// reset the bn_only blockers
	if ($bn_only_percent > $system->SETTINGS['bn_only_percent'])
	{
		$query = "UPDATE " . $DBPrefix . "users SET bn_only = :y WHERE id = bn_only = :n";
		$params = array();
		$params[] = array(':y', 'y', 'str');
		$params[] = array(':n', 'n', 'str');
		$db->query($query, $params);
	}
	$query = "UPDATE " . $DBPrefix . "settings SET buy_now = :bn, bn_only = :only, bn_only_disable = :disable, bn_only_percent = :percent";
	$params = array();
	$params[] = array(':bn', intval($_POST['buy_now']), 'int');
	$params[] = array(':only', $_POST['bn_only'], 'str');
	$params[] = array(':disable', $_POST['bn_only_disable'], 'str');
	$params[] = array(':percent', $bn_only_percent, 'str');
	$db->query($query, $params);

	$system->SETTINGS['buy_now'] = $_POST['buy_now'];
	$system->SETTINGS['bn_only'] = $_POST['bn_only'];
	$system->SETTINGS['bn_only_disable'] = $_POST['bn_only_disable'];
	$system->SETTINGS['bn_only_percent'] = $_POST['bn_only_percent'];
	$ERR = $MSG['30_0066'];
}

loadblock($MSG['920'], $MSG['921'], 'batch', 'buy_now', $system->SETTINGS['buy_now'], array($MSG['029'], $MSG['030']));
loadblock($MSG['30_0064'], $MSG['30_0065'], 'yesno', 'bn_only', $system->SETTINGS['bn_only'], array($MSG['030'], $MSG['029']));
loadblock($MSG['355'], $MSG['358'], 'yesno', 'bn_only_disable', $system->SETTINGS['bn_only_disable'], array($MSG['030'], $MSG['029']));
loadblock($MSG['356'], '', 'percent', 'bn_only_percent', $system->SETTINGS['bn_only_percent'], array($MSG['357']));

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['25_0008'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_buy_it_now" target="_blank">' . $MSG['3500_1015728'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
