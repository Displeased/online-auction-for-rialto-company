<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']))
{
	// add category
	if ($_POST['action'] == $MSG['5204'])
	{
		if (empty($_POST['cat_name'][$system->SETTINGS['defaultlanguage']]))
		{
			$ERR = $ERR_047;
		}
		else
		{
			$query = "INSERT INTO " . $DBPrefix . "faqscategories values (NULL, :c)";
			$params = array();
			$params[] = array(':c', $_POST['cat_name'][$system->SETTINGS['defaultlanguage']], 'str');
			$db->query($query, $params);
			$id = $db->lastInsertId();
			reset($LANGUAGES);
			foreach ($LANGUAGES as $k => $v)
			{
				$query = "INSERT INTO " . $DBPrefix . "faqscat_translated VALUES (:i, :l, :c)";
				$params = array();
				$params[] = array(':i', $id, 'int');
				$params[] = array(':l', $k, 'str');
				$params[] = array(':c', $_POST['cat_name'][$k], 'str');
				$db->query($query, $params);
			}
			header('location: ' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/faqscategories.php');
			exit;
		}
	}

	// Delete categories
	if ($_POST['action'] == $MSG['030'] && isset($_POST['delete']) && is_array($_POST['delete']))
	{
		foreach ($_POST['delete'] as $k => $v)
		{
			if ($v == 'delete')
			{
				$query = "SELECT id FROM " . $DBPrefix . "faqs WHERE category = :i";
				$params = array();
				$params[] = array(':i', $k, 'int');
				$db->query($query, $params);
				$ids = '0';
				while ($row = $db->result('id'))
				{
					$ids .= ',' . $row;
				}
				
				$query = "DELETE FROM " . $DBPrefix . "faqs WHERE category = :i";
				$params = array();
				$params[] = array(':i', $k, 'int');
				$db->query($query, $params);

				$query = "DELETE FROM " . $DBPrefix . "faqs_translated WHERE id IN (:i)";
				$params = array();
				$params[] = array(':i', $ids, 'int');
				$db->query($query, $params);
			}
			else
			{
				$move = explode(':', $v);
				$query = "UPDATE " . $DBPrefix . "faqs SET category = :c WHERE category = :cg";
				$params = array();
				$params[] = array(':c', $move[1], 'int');
				$params[] = array(':cg', $k, 'int');
				$db->query($query, $params);
			}
			$query = "DELETE FROM " . $DBPrefix . "faqscategories WHERE id = :i";
			$params = array();
			$params[] = array(':i', $k, 'int');
			$db->query($query, $params);

			$query = "DELETE FROM " . $DBPrefix . "faqscat_translated WHERE id = :i";
			$params = array();
			$params[] = array(':i', $k, 'int');
			$db->query($query, $params);
			header('location: ' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/faqscategories.php');
			exit;
		}
	}

	// delete check
	if ($_POST['action'] == $MSG['008'] && isset($_POST['delete']) && is_array($_POST['delete']))
	{
		// get cats FAQs can be moved to
		$delete = implode(',', $_POST['delete']);
		$query = "SELECT category, id FROM " . $DBPrefix . "faqscategories WHERE id NOT IN (:i)";
		$params = array();
		$params[] = array(':i', $delete, 'int');
		$db->query($query, $params);

		$move = '';
		while ($row = $db->result())
		{
			$move .= '<option value="move:' . $row['id'] . '">' . $MSG['840'] . $row['category'] . '</option>';
		}
		// Get data from the database
		$query = "SELECT COUNT(f.id) as COUNT, c.category, c.id FROM " . $DBPrefix . "faqscategories c
					LEFT JOIN " . $DBPrefix . "faqs f ON ( f.category = c.id ) 
					WHERE c.id IN (:i) GROUP BY c.id ORDER BY category";
		$params = array();
		$params[] = array(':i', $delete, 'int');
		$db->query($query, $params);

		$message = $MSG['839'] . '<table cellpadding="0" cellspacing="0">';
		$names = array();
		$counter = 0;
		while ($row = $db->result())
		{
			$names[] = $row['category'] . '<input type="hidden" name="delete[' . $row['id'] . ']" value="delete">';
			if ($row['COUNT'] > 0)
			{
				$message .= '<tr>';
				$message .= '<td>' . $row['category'] . '</td><td>';
				$message .= '<select name="delete[' . $row['id'] . ']">';
				$message .= '<option value="delete">' . $MSG['008'] . '</option>';
				$message .= $move;
				$message .= '</select>';
				$message .= '</td>';
				$message .= '</tr>';
				$counter++;
			}
		}
		$message .= '</table>';
		// build message
		$template->assign_vars(array(
				'ERROR' => (isset($ERR)) ? $ERR : '',
				'ID' => '',
				'MESSAGE' => (($counter > 0) ? $message : '') . '<p>' . $MSG['838'] . implode(', ', $names) . '</p>',
				'TYPE' => 1
				));

		$template->set_filenames(array(
				'body' => 'confirm.tpl'
				));
		$template->display('body');
		exit;
	}
}

// Get data from the database
$query = "SELECT COUNT(f.id) as COUNT, c.category, c.id FROM " . $DBPrefix . "faqscategories c
			LEFT JOIN " . $DBPrefix . "faqs f ON ( f.category = c.id )
			GROUP BY c.id ORDER BY :o";
$params = array();
$params[] = array(':o', 'category', 'str');
$db->query($query, $params);

$bg = '';
while ($row = $db->result())
{
	$template->assign_block_vars('cats', array(
			'ID' => $row['id'],
			'CATEGORY' => $row['category'],
			'FAQSTXT' => sprintf($MSG['837'], $row['COUNT']),
			'FAQS' => $row['COUNT'],
			'BG' => $bg
			));
	$bg = ($bg == '') ? 'class="bg"' : '';
}

foreach ($LANGUAGES as $k => $v)
{
	$template->assign_block_vars('lang', array(
			'LANG' => $k,
			'B_NODEFAULT' => ($k != $system->SETTINGS['defaultlanguage'])
			));
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => $MSG['5230'],
		'B_ADDCAT' => (isset($_GET['do']) && $_GET['do'] == 'add')
		));

$template->set_filenames(array(
		'body' => 'faqscategories.tpl'
		));
$template->display('body');

?>