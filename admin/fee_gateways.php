<?php
/*******************************************************************************
 *   copyright				: (C) 2008 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com/
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'fees';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

$links = array(
	'paypal' => 'http://paypal.com/',
	'authnet' => 'http://authorize.net/',
	'worldpay' => 'http://rbsworldpay.com/',
	'skrill' => 'http://skrill.com/',
	'toocheckout' => 'http://2checkout.com/',
	);
$varialbes = array(
	'paypal_address' => $MSG['720'],
	'authnet_address' => $MSG['773'],
	'authnet_password' => $MSG['774'],
	'worldpay_address' => $MSG['824'],
	'skrill_address' => $MSG['825'],
	'toocheckout_address' => $MSG['826'],
	'bank_name' => $MSG['30_0216'],
	'bank_account' => $MSG['30_0217'],
	'bank_routing' => $MSG['30_0218']
	);

$query = "SELECT * FROM " . $DBPrefix . "gateways LIMIT :l";
$params = array();
$params[] = array(':l', 1, 'int');
$db->query($query, $params);
$gateway_data = $db->result();

$gateways = explode(',', $gateway_data['gateways']);

if (isset($_POST['action']))
{
	// build the sql
	$query = 'UPDATE ' . $DBPrefix . 'gateways SET ';
	for ($i = 0; $i < count($gateways); $i++)
	{
		if ($i != 0)
			$query .= ', ';
		$gateway = $gateways[$i];
		$query .= $gateway . '_active = ' . (isset($_POST[$gateway . '_active']) ? 1 : 0) . ', ';
		$query .= $gateway . '_required = ' . (isset($_POST[$gateway . '_required']) ? 1 : 0) . ', ';
		if (isset($_POST[$gateway . '_address']))
		{
			$query .= $gateway . "_address = '" . $_POST[$gateway . '_address'] . "'";
		}
		if (isset($_POST[$gateway . '_name']))
		{
			$query .= $gateway . "_name = '" . $_POST[$gateway . '_name'] . "'";
		}
		if (isset($_POST[$gateway . '_account']))
		{
			$query .= ', ' . $gateway . "_account = '" . $_POST[$gateway . '_account'] . "'";
		}
		if (isset($_POST[$gateway . '_routing']))
		{
			$query .= ', ' . $gateway . "_routing = '" . $_POST[$gateway . '_routing'] . "'";
		}
		if (isset($_POST[$gateway . '_password']))
		{
			$query .= ', ' . $gateway . "_password = '" . $_POST[$gateway . '_password'] . "'";
			$gateway_data[$gateway . '_password'] = $_POST[$gateway . '_password'];
		}		
		$gateway_data[$gateway . '_active'] = (isset($_POST[$gateway . '_active']) ? 1 : 0);
		$gateway_data[$gateway . '_required'] = (isset($_POST[$gateway . '_required']) ? 1 : 0);
		if (isset($_POST[$gateway . '_address']))
		{
			$gateway_data[$gateway . '_address'] = $_POST[$gateway . '_address'];
		}
		if (isset($_POST[$gateway . '_name']))
		{
			$gateway_data[$gateway . '_name'] = $_POST[$gateway . '_name'];
		}
		if (isset($_POST[$gateway . '_account']))
		{
			$gateway_data[$gateway . '_account'] = $_POST[$gateway . '_account'];
		}
		if (isset($_POST[$gateway . '_routing']))
		{
			$gateway_data[$gateway . '_routing'] = $_POST[$gateway . '_routing'];
		}
	}
	$db->direct_query($query);
	$ERR = $MSG['762'];
}

for ($i = 0; $i < count($gateways); $i++)
{
	$gateway = $gateways[$i];
	$template->assign_block_vars('gateways', array(
			'NAME' => $system->SETTINGS['gatways'][$gateway],
			'PLAIN_NAME' => $gateway,
			'ENABLED' => ($gateway_data[$gateway . '_active'] == 1) ? 'checked' : '',
			'REQUIRED' => ($gateway_data[$gateway . '_required'] == 1) ? 'checked' : '',
			'ADDRESS' => $gateway_data[$gateway . '_address'],
			'PASSWORD' => (isset($gateway_data[$gateway . '_password'])) ? $gateway_data[$gateway . '_password'] : '',
			'WEBSITE' => $links[$gateway],
			'ADDRESS_NAME' => $varialbes[$gateway . '_address'],
			'ADDRESS_PASS' => (isset($varialbes[$gateway . '_password'])) ? $varialbes[$gateway . '_password'] : '',
			
			'BANK_NAME' => (isset($varialbes[$gateway . '_name'])) ? $varialbes[$gateway . '_name'] : '',
			'BANK_NAME2' => $gateway_data[$gateway . '_name'],
			'BANK_ACCOUNT' => (isset($varialbes[$gateway . '_account'])) ? $varialbes[$gateway . '_account'] : '',
			'BANK_ACCOUNT2' => $gateway_data[$gateway . '_account'],
			'BANK_ROUTING' => (isset($varialbes[$gateway . '_routing'])) ? $varialbes[$gateway . '_routing'] : '',
			'BANK_ROUTING2' => $gateway_data[$gateway . '_routing'],

			'B_PASSWORD' => (isset($gateway_data[$gateway . '_password'])),
			'B_BANK_NAME' => (isset($gateway_data[$gateway . '_name'])),
			'B_BANK_ACCOUNT' => (isset($gateway_data[$gateway . '_account'])),
			));
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'PAGENAME' => $MSG['445'],
		'ERROR' => (isset($ERR)) ? $ERR : ''
		));

$template->set_filenames(array(
		'body' => 'fee_gateways.tpl'
		));
$template->display('body');

?>