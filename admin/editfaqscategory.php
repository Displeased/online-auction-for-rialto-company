<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if ($_POST['action'] == 'update')
{
	if (strlen($_POST['category']) == 0)
	{
		$ERR = $ERR_049;
	}
	else
	{
		$query = "UPDATE " . $DBPrefix . "faqscategories SET category = :c WHERE id = :i";
		$params = array();
		$params[] = array(':c', $system->cleanvars($_POST['category'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':i', $_POST['id'], 'int');
		$db->query($query, $params);

	}
	
	foreach ($_POST['category'] as $k => $v)
	{
		$query = "SELECT category FROM " . $DBPrefix . "faqscat_translated WHERE lang = :l AND id = :i";
		$params = array();
		$params[] = array(':l', $k, 'str');
		$params[] = array(':i', $_POST['id'], 'str');
		$db->query($query, $params);
		if ($db->numrows('category') > 0)
		{
			$query = "UPDATE " . $DBPrefix . "faqscat_translated SET category = :c WHERE lang = :l AND id = :i";
			$params = array();
			$params[] = array(':c', $system->cleanvars($_POST['category'][$k]), 'str');
			$params[] = array(':l', $k, 'str');
			$params[] = array(':i', $_POST['id'], 'str');
		}
		else
		{
			$query = "INSERT INTO " . $DBPrefix . "faqscat_translated VALUES (:i, :l, :c)";
			$params = array();
			$params[] = array(':i', $_POST['id'], 'str');
			$params[] = array(':l', $k, 'str');
			$params[] = array(':c', $system->cleanvars($_POST['category'][$k]), 'str');
		}
		$db->query($query, $params);
	}
	header('location: faqscategories.php');
	exit;
}

$query = "SELECT * FROM " . $DBPrefix . "faqscat_translated WHERE id = :i";
$params = array();
$params[] = array(':i', $_GET['id'], 'int');
$db->query($query, $params);

// get all translations
$tr = array();
while ($row = $db->result())
{
	$tr[$row['lang']] = $row['category'];
}

foreach ($LANGUAGES as $k => $v)
{
	$k = trim($k);
	$template->assign_block_vars('flangs', array(
			'LANGUAGE' => $k,
			'TRANSLATION' => $tr[$k]
			));
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'FAQ_NAME' => $tr[$system->SETTINGS['defaultlanguage']],
		'PAGENAME' => $MSG['5232'],
		'ID' => $_GET['id']
		));

$template->set_filenames(array(
		'body' => 'editfaqscategory.tpl'
		));
$template->display('body');
?>