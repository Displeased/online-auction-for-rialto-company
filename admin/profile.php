<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

$MANDATORY_FIELDS = unserialize($system->SETTINGS['mandatory_fields']);
$DISPLAYED_FIELDS = unserialize($system->SETTINGS['displayed_feilds']);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	$MANDATORY_FIELDS = array(
			'birthdate' => $_POST['birthdate'],
			'address' => $_POST['address'],
			'city' => $_POST['city'],
			'prov' => $_POST['prov'],
			'country' => $_POST['country'],
			'zip' => $_POST['zip'],
			'tel' => $_POST['tel']
			);
			
	$DISPLAYED_FIELDS = array(
			'birthdate_regshow' => $_POST['birthdate_regshow'],
			'address_regshow' => $_POST['address_regshow'],
			'city_regshow' => $_POST['city_regshow'],
			'prov_regshow' => $_POST['prov_regshow'],
			'country_regshow' => $_POST['country_regshow'],
			'zip_regshow' => $_POST['zip_regshow'],
			'tel_regshow' => $_POST['tel_regshow']
			);

	// common sense check field cant be required if its not visible
	$required = array_keys($MANDATORY_FIELDS);
	$display = array_keys($DISPLAYED_FIELDS);
	for ($i = 0; $i <= 7; $i++)
	{
		if ($MANDATORY_FIELDS[$required[$i]] == 'y' && $DISPLAYED_FIELDS[$display[$i]] == 'n')
		{
			$ERR = $MSG['809'];
		}
	}
	
	if(empty($_POST['username_size']))
	{
		$ERR = $MSG['3500_1015644'];
	}
	elseif(empty($_POST['username_size']))
	{
		$ERR = $MSG['3500_1015645'];
	}
	
	if (!isset($ERR))
	{
		
		$mdata = serialize($MANDATORY_FIELDS);
		$sdata = serialize($DISPLAYED_FIELDS);
		$query = "UPDATE " . $DBPrefix . "settings SET
				  mandatory_fields = :mandatory_fields,
				  displayed_feilds = :displayed_feilds,
				  minimum_username_length = :username_length,
				  minimum_password_length = :password_length";
		$params = array();
		$params[] = array(':mandatory_fields', $mdata, 'str');
		$params[] = array(':displayed_feilds', $sdata, 'str');
		$params[] = array(':username_length', $_POST['username_size'], 'int');
		$params[] = array(':password_length', $_POST['password_size'], 'int');
		$db->query($query, $params);
		$system->SETTINGS['minimum_username_length'] = $_POST['username_size'];
		$system->SETTINGS['minimum_password_length'] = $_POST['password_size'];
		$ERR = $MSG['779'];
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => $MSG['048'],
		'SITEURL' => $system->SETTINGS['siteurl'],
		'USERNAME_SIZE' => $system->SETTINGS['minimum_username_length'],
		'PASSWORD_SIZE' => $system->SETTINGS['minimum_password_length'],
		'REQUIRED_0' => ($MANDATORY_FIELDS['birthdate'] == 'y') ? true : false,
		'REQUIRED_1' => ($MANDATORY_FIELDS['address'] == 'y') ? true : false,
		'REQUIRED_2' => ($MANDATORY_FIELDS['city'] == 'y') ? true : false,
		'REQUIRED_3' => ($MANDATORY_FIELDS['prov'] == 'y') ? true : false,
		'REQUIRED_4' => ($MANDATORY_FIELDS['country'] == 'y') ? true : false,
		'REQUIRED_5' => ($MANDATORY_FIELDS['zip'] == 'y') ? true : false,
		'REQUIRED_6' => ($MANDATORY_FIELDS['tel'] == 'y') ? true : false,
		'DISPLAYED_0' => ($DISPLAYED_FIELDS['birthdate_regshow'] == 'y') ? true : false,
		'DISPLAYED_1' => ($DISPLAYED_FIELDS['address_regshow'] == 'y') ? true : false,
		'DISPLAYED_2' => ($DISPLAYED_FIELDS['city_regshow'] == 'y') ? true : false,
		'DISPLAYED_3' => ($DISPLAYED_FIELDS['prov_regshow'] == 'y') ? true : false,
		'DISPLAYED_4' => ($DISPLAYED_FIELDS['country_regshow'] == 'y') ? true : false,
		'DISPLAYED_5' => ($DISPLAYED_FIELDS['zip_regshow'] == 'y') ? true : false,
		'DISPLAYED_6' => ($DISPLAYED_FIELDS['tel_regshow'] == 'y') ? true : false
		));
		
$template->set_filenames(array(
		'body' => 'profile.tpl'
		));
$template->display('body');

?>