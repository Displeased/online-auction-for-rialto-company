<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


reset($LANGUAGES);
foreach ($LANGUAGES as $k => $v)
{
	include $main_path . 'language/' . $k . '/messages.inc.php';
	include $main_path . 'language/' . $k . '/categories.inc.php';

	$query = "SELECT cat_id FROM " . $DBPrefix . "categories WHERE parent_id = -:pi";
	$params = array();
	$params[] = array(':pi', 1, 'int');
	$db->query($query, $params);
	$DATA = $db->result('cat_id');
	$query = "SELECT cat_id FROM " . $DBPrefix . "categories WHERE parent_id = :pi ORDER BY cat_name";
	$params = array();
	$params[] = array(':pi', $DATA, 'int');
	$db->query($query, $params);
	$output = '<select name="id">' . "\n";
	$output.= "\t" . '<option value="0">' . $MSG['277'] . '</option>' . "\n";
	$output.= "\t" . '<option value="0">----------------------</option>' . "\n";

	$num_rows = $db->numrows();

	$i = 0;
	while ($row = $db->result())
	{
		$category_id = $row['cat_id'];
		$cat_name = $category_names[$category_id];
		$output .= "\t" . '<option value="' . $category_id . '">' . $cat_name . '</option>' . "\n";
		$i++;
	}

	$output.= '</select>'."\n";

	$handle = fopen ($main_path . 'language/' . $k . '/categories_select_box.inc.php', 'w');
	fputs($handle, $output);
	fclose($handle);
}
?>