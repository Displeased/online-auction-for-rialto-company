<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $main_path . 'inc/ckeditor/ckeditor.php';

unset($ERR);

$subject = (isset($_POST['subject'])) ? stripslashes($_POST['subject']) : '';
$content = (isset($_POST['content'])) ? stripslashes($_POST['content']) : '';
$is_preview = false;

if (isset($_POST['action']) && $_POST['action'] == 'submit')
{
	if (empty($subject) || empty($content))
	{
		$ERR = $ERR_5014;
	}
	else
	{
		$COUNTER = 0;
		switch($_POST['usersfilter'])
		{
			case 'active':
				$extra = ' AND suspended = 0';
				break;
			case 'admin':
				$extra = ' AND suspended = 1';
				break;
			case 'fee':
				$extra = ' AND suspended = 9';
				break;
			case 'confirmed':
				$extra = ' AND suspended = 8';
				break;
		}
		$query = "SELECT email, id FROM " . $DBPrefix . "users WHERE nletter = :yes" . $extra;
		$params = array();
		$params[] = array(':yes', 1, 'int');
		$db->query($query, $params);
		while ($row = $db->result())
		{
			if ($send_email->send_newsletter($row['id'], $row['email'], $content, $subject))
			{
				$COUNTER++;
			}
		}
		$ERR = $COUNTER . $MSG['5300'];
	}
}
elseif (isset($_POST['action']) && $_POST['action'] == 'preview')
{
	$is_preview = true;
}

$USERSFILTER = array('all' => $MSG['5296'],
	'active' => $MSG['5291'],
	'admin' => $MSG['5294'],
	'fee' => $MSG['5293'],
	'confirmed' => $MSG['5292']);

$selectsetting = (isset($_POST['usersfilter'])) ? $_POST['usersfilter'] : '';

$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'SELECTBOX' => generateSelect('usersfilter', $USERSFILTER),
		'SUBJECT' => $subject,
		'EDITOR' => $CKEditor->editor('content', stripslashes($content)),
		'PREVIEW' => $content,
		'PAGENAME' => $MSG['607'],

		'B_PREVIEW' => $is_preview
		));

$template->set_filenames(array(
		'body' => 'newsletter.tpl'
		));
$template->display('body');
?>
