<?php
/*******************************************************************************
 *   copyright				: (C) 2008 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com/
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $include_path . 'countries.inc.php';

unset($ERR);
$userid = intval($_REQUEST['userid']);

// Data check
if (empty($userid) || $userid <= 0)
{
	header('location: listusers.php?PAGE=' . intval($_GET['offset']));
	exit;
}

// Retrieve users signup settings
$MANDATORY_FIELDS = unserialize($system->SETTINGS['mandatory_fields']);

if (isset($_POST['admin']) && $_POST['admin'] == 'sub')
{
	$query = "UPDATE " . $DBPrefix . "users SET admin = '1' WHERE id = :i";
	$params = array();
	$params[] = array(':i', $userid, 'int');
	$db->query($query, $params);
	header('location: edituser.php?userid=' . $userid .'&offset=' . intval($_POST['offset']));
	exit;
}
elseif (isset($_POST['admin']) && $_POST['admin'] == 'main')
{
	$query = "UPDATE " . $DBPrefix . "users SET admin = :a WHERE id = :i";
	$params = array();
	$params[] = array(':a', 2, 'int');
	$params[] = array(':i', $userid, 'int');
	$db->query($query, $params);
	header('location: edituser.php?userid=' . $userid .'&offset=' . intval($_POST['offset']));
	exit;
}
elseif (isset($_POST['admin']) && $_POST['admin'] == 'normal')
{
	$query = "UPDATE " . $DBPrefix . "users SET admin = :a WHERE id = :i";
	$params = array();
	$params[] = array(':a', 0, 'int');
	$params[] = array(':i', $userid, 'int');
	$db->query($query, $params);
	header('location: edituser.php?userid=' . $userid .'&offset=' . intval($_POST['offset']));
	exit;
}

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if ($_POST['name'] && $_POST['email'])
	{
		if (!empty($_POST['birthdate']))
		{
			$DATE = explode('/', $_POST['birthdate']);
			if ($system->SETTINGS['datesformat'] == 'USA')
			{
				$birth_day = $DATE[1];
				$birth_month = $DATE[0];
				$birth_year = $DATE[2];
			}
			else
			{
				$birth_day = $DATE[0];
				$birth_month = $DATE[1];
				$birth_year = $DATE[2];
			}

			if (strlen($birth_year) == 2)
			{
				$birth_year = '19' . $birth_year;
			}
		}

		if (strlen($_POST['password']) > 0 && ($_POST['password'] != $_POST['repeat_password']))
		{
			$ERR = $ERR_006;
		}
		elseif (strlen($_POST['email']) < 5) //Primitive mail check
		{
			$ERR = $ERR_110;
		}
		elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['email']))
		{
			$ERR = $ERR_008;
		}
		elseif (!preg_match('/^([0-9]{2})\/([0-9]{2})\/([0-9]{2,4})$/', $_POST['birthdate']) && $MANDATORY_FIELDS['birthdate'] == 'y')
		{ //Birthdate check
			$ERR = $ERR_043;
		}
		elseif (strlen($_POST['zip']) < 4 && $MANDATORY_FIELDS['zip'] == 'y')
		{ //Primitive zip check
			$ERR = $ERR_616;
		}
		elseif (strlen($_POST['phone']) < 3 && $MANDATORY_FIELDS['tel'] == 'y')
		{ //Primitive phone check
			$ERR = $ERR_617;
		}
		elseif (empty($_POST['address']) && $MANDATORY_FIELDS['address'] == 'y')
		{
			$ERR = $ERR_5034;
		}
		elseif (empty($_POST['city']) && $MANDATORY_FIELDS['city'] == 'y')
		{
			$ERR = $ERR_5035;
		}
		elseif (empty($_POST['prov']) && $MANDATORY_FIELDS['prov'] == 'y')
		{
			$ERR = $ERR_5036;
		}
		elseif (empty($_POST['country']) && $MANDATORY_FIELDS['country'] == 'y')
		{
			$ERR = $ERR_5037;
		}
		elseif (count($_POST['group']) == 0)
		{
			$ERR = $ERR_044;
		}
		else
		{
			if (!empty($_POST['birthdate']))
			{
				$birthdate = $birth_year . $birth_month . $birth_day;
			}
			else
			{
				$birthdate = 0;
			}
			
			if($_POST['balance'] >= $system->SETTINGS['fee_max_debt'])
			{
				$suspend = 7;
			}
			else
			{
				$suspend = 0;
			}
			
			$query = "UPDATE " . $DBPrefix . "users SET 
				  name = '" . $system->cleanvars($_POST['name']) . "',
				  email = '" . $system->cleanvars($_POST['email']) . "',
				  address = '" . $system->cleanvars($_POST['address']) . "',
				  city = '" . $system->cleanvars($_POST['city']) . "',
				  prov = '" . $system->cleanvars($_POST['prov']) . "',
				  country = '" . $system->cleanvars($_POST['country']) . "',
				  zip = '" . $system->cleanvars($_POST['zip']) . "',
				  phone = '" . $system->cleanvars($_POST['phone']) . "',
				  birthdate = '" . $system->cleanvars($birthdate) . "',
				  groups = '" . implode(',', $_POST['group']) . "',
				  suspended = '" . $suspend . "', 
				  balance = '" . $_POST['balance'] . "'";
			if ($system->SETTINGS['fee_max_debt'] <= (-1 * $_POST['balance']) && $_POST['email_sent'] == 'y')
			{
				//this will let the system resend the payment reminder email when the user logs in
				$query .=  ", payment_reminder_sent = 'n'"; 
			}
			if (strlen($_POST['password']) > 0)
			{
				$password = $_POST['password'];	
				$hashed_password = $phpass->HashPassword($password);
				$query .=  ", password = '" . $hashed_password . "'";
			}
			$query .=  " WHERE id = " . $userid;
			$db->direct_query($query);
			
			header('location: listusers.php?PAGE=' . intval($_POST['offset']));
			exit;
		}
	}
	else
	{
		$ERR = $ERR_112;
	}
}

// load the page
$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :user_id";
$params = array();
$params[] = array(':user_id', $userid, 'int');
$db->query($query, $params);
$user_data = $db->result();

if ($user_data['birthdate'] != 0)
{
	$birth_day = substr($user_data['birthdate'], 6, 2);
	$birth_month = substr($user_data['birthdate'], 4, 2);
	$birth_year = substr($user_data['birthdate'], 0, 4);

	if ($system->SETTINGS['datesformat'] == 'USA')
	{
		$birthdate = $birth_month . '/' . $birth_day . '/' . $birth_year;
	}
	else
	{
		$birthdate = $birth_day . '/' . $birth_month . '/' . $birth_year;
	}
}
else
{
	$birthdate = '';
}

$country_list = '';
foreach ($countries as $code => $descr)
{
	$country_list .= '<option value="' . $descr . '"';
	if ($descr == $user_data['country'])
	{
		$country_list .= ' selected';
	}
	$country_list .= '>' . $descr . '</option>' . "\n";
}

$query = "SELECT id, group_name FROM ". $DBPrefix . "groups";
$db->direct_query($query);
$usergroups = '';
$groups = explode(',', $user_data['groups']);
while ($row = $db->result())
{
	$member = (in_array($row['id'], $groups)) ? ' checked' : '';
	$usergroups .= '<p><input type="checkbox" name="group[]" value="' . $row['id'] . '"' . $member . '> ' . $row['group_name'] . '</p>';
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'REALNAME' => $user_data['name'],
		'USERNAME' => $user_data['nick'],
		'EMAIL' => $user_data['email'],
		'ADDRESS' => $user_data['address'],
		'CITY' => $user_data['city'],
		'PROV' => $user_data['prov'],
		'ZIP' => $user_data['zip'],
		'COUNTRY' => $user_data['country'],
		'PHONE' => $user_data['phone'],
		'BALANCE' => $user_data['balance'],
		'DOB' => $birthdate,
		'COUNTRY_LIST' => $country_list,
		'ID' => $userid,
		'EMAIL_SENT' => $user_data['payment_reminder_sent'],
		'PAGENAME' => $MSG['511'],
		'OFFSET' => $_GET['offset'],
		'USERGROUPS' => $usergroups,
		'B_SUB_ADMIN' => $user_data['admin'] == 0,
		'B_MAIN_ADMIN' => $user_data['admin'] == 0,
		'B_UPGRADE_ADMIN' => $user_data['admin'] == 1,
		'B_UPGRADE_NORMAL' => $user_data['admin'] == 1 || $user_data['admin'] == 2,
		'REQUIRED' => array(
					($MANDATORY_FIELDS['birthdate'] == 'y') ? ' *' : '',
					($MANDATORY_FIELDS['address'] == 'y') ? ' *' : '',
					($MANDATORY_FIELDS['city'] == 'y') ? ' *' : '',
					($MANDATORY_FIELDS['prov'] == 'y') ? ' *' : '',
					($MANDATORY_FIELDS['country'] == 'y') ? ' *' : '',
					($MANDATORY_FIELDS['zip'] == 'y') ? ' *' : '',
					($MANDATORY_FIELDS['tel'] == 'y') ? ' *' : ''
					)
		));
		
$template->set_filenames(array(
		'body' => 'edituser.tpl'
		));
$template->display('body');
?> 
