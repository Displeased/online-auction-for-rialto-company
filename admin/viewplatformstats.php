<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'stats';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

// Retrieve data
$query = "SELECT * FROM " . $DBPrefix . "currentplatforms WHERE month = :m AND year = :y ORDER BY counter DESC";
$params = array();
$params[] = array(':m', date('m'), 'int');
$params[] = array(':y', date('Y'), 'int');
$db->query($query, $params);

$MAX = 0;
$TOTAL = 0;
while ($row = $db->result())
{
	$PLATFORMS[$row['platform']] = $row['counter'];
	$TOTAL = $TOTAL + $row['counter'];
	if ($row['counter'] > $MAX)
	{
		$MAX = $row['counter'];
	}
}
if (is_array($PLATFORMS))
{
	foreach ($PLATFORMS as $k => $v)
	{
		$template->assign_block_vars('sitestats', array(
			'PLATFORM' => $k,
			'COUNT' => $PLATFORMS[$k],
			'NUM' => $PLATFORMS[$k],
			'WIDTH' => ($PLATFORMS[$k] * 100) / $MAX,
			'PERCENTAGE' => ceil(intval($PLATFORMS[$k] * 100 / $TOTAL))
			));
	}
}

$template->assign_vars(array(
		'SITENAME' => $system->SETTINGS['sitename'],
		'PAGENAME' => $MSG['5318'],
		'STATSMONTH' => date('F Y')
		));

$template->set_filenames(array(
		'body' => 'viewplatformstats.tpl'
		));
$template->display('body');
?>
