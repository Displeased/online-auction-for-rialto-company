<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

if (!isset($_GET['banner']) || empty($_GET['banner']))
{
	header('location: managebanners.php');
	exit;
}

$banner = $_GET['banner'];

$query = "SELECT name, user FROM " . $DBPrefix . "banners WHERE id = :banner_id";
$params = array();
$params[] = array(':banner_id', $banner, 'int');
$db->query($query, $params);
$data = $db->result();
$bannername = $data['name'];
$banneruser = $data['user'];


$query = "DELETE FROM " . $DBPrefix . "banners WHERE id = :banner_id";
$params = array();
$params[] = array(':banner_id', $banner, 'int');
$db->query($query, $params);

$query = "DELETE FROM " . $DBPrefix . "bannerscategories WHERE banner = :banner_id";
$params = array();
$params[] = array(':banner_id', $banner, 'int');
$db->query($query, $params);

$query = "DELETE FROM " . $DBPrefix . "bannerskeywords WHERE banner = :banner_id";
$params = array();
$params[] = array(':banner_id', $banner, 'int');
$db->query($query, $params);

@unlink($upload_path . 'banners/' . $banneruser . '/' . $bannername);

// Redirect
header('location: userbanners.php?id=' . $banneruser);
?>