<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'fees';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// update users
	if ($system->SETTINGS['fee_max_debt'] < $_POST['fee_max_debt'])
	{
		$query = "UPDATE " . $DBPrefix . "users SET suspended = :s WHERE suspended = :sp AND balance > :f";
		$params = array();
		$params[] = array(':s', 0, 'int');
		$params[] = array(':sp', 7, 'int');
		$params[] = array(':f', $_POST['fee_max_debt'], 'int');
		$db->query($query, $params);
	}
	// Update database
	$query = "UPDATE ". $DBPrefix . "settings SET fees = :f, fee_type = :ft, fee_max_debt = :fmb, fee_signup_bonus = :fsb, fee_disable_acc = :disable_acc";
	$params = array();
	$params[] = array(':f', $_POST['fees'], 'str');
	$params[] = array(':ft', $_POST['fee_type'], 'int');
	$params[] = array(':fmb', $system->input_money($_POST['fee_max_debt']), 'int');
	$params[] = array(':fsb', $system->input_money($_POST['fee_signup_bonus']), 'int');
	$params[] = array(':disable_acc', $_POST['fee_disable_acc'], 'str');
	$db->query($query, $params);

	$system->SETTINGS['fees'] = $_POST['fees'];
	$system->SETTINGS['fee_type'] = $_POST['fee_type'];
	$system->SETTINGS['fee_max_debt'] = $_POST['fee_max_debt'];
	$system->SETTINGS['fee_signup_bonus'] = $_POST['fee_signup_bonus'];
	$system->SETTINGS['fee_disable_acc'] = $_POST['fee_disable_acc'];
	$ERR = $MSG['761'];
}

loadblock($MSG['395'], $MSG['397'], 'yesno', 'fees', $system->SETTINGS['fees'], array($MSG['3500_1015638'], $MSG['3500_1015637']));
loadblock($MSG['729'], $MSG['730'], 'batchstacked', 'fee_type', $system->SETTINGS['fee_type'], array($MSG['731'], $MSG['732']));

loadblock($MSG['733'], '', '', '', '', array(), true);
loadblock($MSG['734'], $MSG['735'], 'decimals', 'fee_max_debt', $system->SETTINGS['fee_max_debt']);
loadblock($MSG['736'], $MSG['737'], 'decimals', 'fee_signup_bonus', $system->SETTINGS['fee_signup_bonus']);
loadblock($MSG['738'], $MSG['739'], 'yesno', 'fee_disable_acc', $system->SETTINGS['fee_disable_acc'], array($MSG['030'], $MSG['029']));

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['25_0012'],
		'PAGENAME' => $MSG['395'],
		'B_TITLES' => true
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
