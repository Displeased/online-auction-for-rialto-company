<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $main_path . 'inc/ckeditor/ckeditor.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Update database
	$query = "UPDATE " . $DBPrefix . "settings SET cookiespolicy = :policy, cookiespolicytext = :cookiespolicy";
	$params = array();
	$params[] = array(':policy', $_POST['cookiespolicy'], 'str');
	$params[] = array(':cookiespolicy', stripslashes($_POST['cookiespolicytext']), 'str');
	$db->query($query, $params);

	$system->SETTINGS['cookiespolicy'] = $_POST['cookiespolicy'];
	$system->SETTINGS['cookiespolicytext'] = $_POST['cookiespolicytext'];
	$ERR = $MSG['30_0237'];
}
loadblock($MSG['30_0234'], $MSG['30_0236'], 'yesno', 'cookiespolicy', $system->SETTINGS['cookiespolicy'], array($MSG['030'], $MSG['029']));

$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;

loadblock($MSG['30_0238'], $MSG['5080'], $CKEditor->editor('cookiespolicytext', stripslashes($system->SETTINGS['cookiespolicytext'])));

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['25_0236'],
		'PAGENAME' => $MSG['30_0233']
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
