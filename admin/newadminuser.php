<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/

define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (empty($_POST['username']) || empty($_POST['password']) || empty($_POST['repeatpassword']))
	{
		$ERR = $ERR_047;
	}
	elseif ((!empty($_POST['password']) && empty($_POST['repeatpassword'])) || empty($_POST['password']) && !empty($_POST['repeatpassword']))
	{
		$ERR = $ERR_054;
	}
	elseif ($_POST['password'] != $_POST['repeatpassword'])
	{
		$ERR = $ERR_006;
	}
	else
	{
		// Check if "username" already exists in the database
		$query = "SELECT id FROM " . $DBPrefix . "adminusers WHERE username = :u";
		$params = array();
		$params[] = array(':u', $_POST['username'], 'str');
		$db->query($query, $params);
		if ($db->result() > 0)
		{
			$ERR = sprintf($ERR_055, $_POST['username']);
		}
		else
		{
			$password = $_POST['password'];
			$PASS = $phpass->HashPassword($password);
			$query = "INSERT INTO " . $DBPrefix . "adminusers VALUES
					(NULL, '" . addslashes($_POST['username']) . "', '" . $PASS . "', '" . get_hash() . "', '" . gmdate('Ymd') . "', '0', " . intval($_POST['status']) . ", '')";
			$db->direct_query($query);
			header('location: adminusers.php');
			exit;
		}
	}
}

loadblock($MSG['003'], '', 'text', 'username', $system->SETTINGS['username']);
loadblock($MSG['004'], '', 'password', 'password', $system->SETTINGS['password']);
loadblock($MSG['564'], '', 'password', 'repeatpassword', $system->SETTINGS['repeatpassword']);
loadblock('', '', 'batch', 'status', $system->SETTINGS['status'], array($MSG['566'], $MSG['567']));

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['25_0010'],
		'PAGENAME' => $MSG['367']
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
