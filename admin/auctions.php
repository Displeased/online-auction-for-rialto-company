<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if ($_POST['status'] == 'enabled' && (!is_numeric($_POST['timebefore']) || !is_numeric($_POST['extend'])))
	{
		$ERR = $MSG['2_0038'];
	}
	elseif ($_POST['maxpicturesize'] == 0)
	{
		$ERR = $ERR_707;
	}
	elseif (!empty($_POST['maxpicturesize']) && !intval($_POST['maxpicturesize']))
	{
		$ERR = $ERR_708;
	}
	elseif (!empty($_POST['maxpictures']) && !intval($_POST['maxpictures']))
	{
		$ERR = $ERR_706;
	}
	else
	{
		// Update database
		$query = "UPDATE ". $DBPrefix . "settings SET
				  proxy_bidding = '" . $_POST['proxy_bidding'] . "',
				  edit_starttime = '" . $_POST['edit_starttime'] . "',
				  cust_increment = " . $_POST['cust_increment'] . ",
				  hours_countdown = '" . $_POST['hours_countdown'] . "',
				  ao_hpf_enabled = '" . $_POST['ao_hpf_enabled'] . "',
				  ao_hi_enabled = '" . $_POST['ao_hi_enabled'] . "',
				  ao_bi_enabled = '" . $_POST['ao_bi_enabled'] . "',
				  subtitle = '" . $_POST['subtitle'] . "',
				  extra_cat = '" . $_POST['extra_cat'] . "',
				  autorelist = '" . $_POST['autorelist'] . "',
				  autorelist_max = '" . $_POST['autorelist_max'] . "',
				  ae_status = '" . $_POST['status'] . "',
				  ae_timebefore = " . intval($_POST['timebefore']) . ",
				  ae_extend = " . intval($_POST['extend']) . ",
				  picturesgallery = " . $_POST['picturesgallery'] . ",
				  maxpictures = " . $_POST['maxpictures'] . ",
				  maxuploadsize = " . ($_POST['maxpicturesize'] * 1024) . ",
				  dites_upload_size = " . ($_POST['dites_upload_size'] * 1024) . ",
				  item_conditions = '" . $_POST['conditions'] . "',
				  dutch_auctions = '" . $_POST['dutch'] . "',
				  max_image_width = '" . $_POST['max_image_width'] . "',
				  auction_setup_types = '" . $_POST['setup_types'] . "',
				  max_image_height = '" . $_POST['max_image_height'] . "',
				  di_auctions = '" . $_POST['di_auctions'] . "',
				  standard_auctions = '" . $_POST['standard'] . "',
				  freemaxpictures = '" . $_POST['freemaxpictures'] . "',
				  shipping_conditions = '" . $_POST['shipping_conditions'] . "',
				  shipping_terms = '" . $_POST['shipping_terms'] . "',
				  thumb_show = " . $_POST['thumb_show'];
		if($_POST['setup_types'] == 2 || $_POST['setup_types'] == 0 || $_POST['di_auctions'] == 'y')
		{
			$query .=", buy_now = 2, bn_only = 'y'";
		}
		$db->direct_query($query);
		$ERR = $MSG['5088'];
	}
	$system->SETTINGS['edit_starttime'] = $_POST['edit_starttime'];
	$system->SETTINGS['cust_increment'] = $_POST['cust_increment'];
	$system->SETTINGS['hours_countdown'] = $_POST['hours_countdown'];
	$system->SETTINGS['ao_hpf_enabled'] = $_POST['ao_hpf_enabled'];
	$system->SETTINGS['ao_hi_enabled'] = $_POST['ao_hi_enabled'];
	$system->SETTINGS['ao_bi_enabled'] = $_POST['ao_bi_enabled'];
	$system->SETTINGS['proxy_bidding'] = $_POST['proxy_bidding'];
	$system->SETTINGS['subtitle'] = $_POST['subtitle'];
	$system->SETTINGS['extra_cat'] = $_POST['extra_cat'];
	$system->SETTINGS['autorelist'] = $_POST['autorelist'];
	$system->SETTINGS['autorelist_max'] = $_POST['autorelist_max'];

	$system->SETTINGS['ae_status'] = $_POST['status'];
	$system->SETTINGS['ae_timebefore'] = $_POST['timebefore'];
	$system->SETTINGS['ae_extend'] = $_POST['extend'];

	$system->SETTINGS['picturesgallery'] = $_POST['picturesgallery'];
	$system->SETTINGS['maxpictures'] = $_POST['maxpictures'];
	$system->SETTINGS['maxuploadsize'] = $_POST['maxpicturesize'] * 1024;
	$system->SETTINGS['thumb_show'] = $_POST['thumb_show'];
	$system->SETTINGS['max_image_width'] = $_POST['max_image_width'];
	$system->SETTINGS['max_image_height'] = $_POST['max_image_height'];
	
	$system->SETTINGS['di_auctions'] = $_POST['di_auctions'];
	$system->SETTINGS['di_upload_size'] = $_POST['di_upload_size'];
	$system->SETTINGS['item_conditions'] = $_POST['conditions'];
	$system->SETTINGS['dutch_auctions'] = $_POST['dutch'];
	$system->SETTINGS['standard_auctions'] = $_POST['standard'];
	$system->SETTINGS['auction_setup_types'] = $_POST['setup_types'];
	$system->SETTINGS['freemaxpictures'] = $_POST['freemaxpictures'];
	$system->SETTINGS['shipping_conditions'] = $_POST['shipping_conditions'];
	$system->SETTINGS['shipping_terms'] = $_POST['shipping_terms'];

}
loadblock($MSG['257'], '', '', '', '', array(), true);
loadblock($MSG['1021'], $MSG['3500_1015738'], 'yesno', 'standard', $system->SETTINGS['standard_auctions'], array($MSG['030'], $MSG['029']));
loadblock($MSG['1020'], $MSG['3500_1015514'], 'yesno', 'dutch', $system->SETTINGS['dutch_auctions'], array($MSG['030'], $MSG['029']));
loadblock($MSG['350_1010'], $MSG['350_10185'], 'yesno', 'di_auctions', $system->SETTINGS['di_auctions'], array($MSG['030'], $MSG['029']));

//digital item options
loadblock($MSG['350_10179'], '', '', '', '', array(), true);
loadblock($MSG['350_10180'], $MSG['350_10181'], 'decimals', 'dites_upload_size', ($system->SETTINGS['dites_upload_size'] / 1024), array($MSG['672']));

loadblock($MSG['897'], '', '', '', '', array(), true);
loadblock($MSG['3500_1015744'], $MSG['3500_1015757'], 'select3num', 'setup_types', $system->SETTINGS['auction_setup_types'], array($MSG['3500_1015754'], $MSG['3500_1015755'], $MSG['3500_1015756']));
loadblock($MSG['427'], $MSG['428'], 'yesno', 'proxy_bidding', $system->SETTINGS['proxy_bidding'], array($MSG['030'], $MSG['029']));
loadblock($MSG['3500_1015762'], $MSG['3500_1015764'], 'yesno', 'shipping_conditions', $system->SETTINGS['shipping_conditions'], array($MSG['030'], $MSG['029']));
loadblock($MSG['3500_1015763'], $MSG['3500_1015765'], 'yesno', 'shipping_terms', $system->SETTINGS['shipping_terms'], array($MSG['030'], $MSG['029']));


loadblock($MSG['5090'], $MSG['5089'], 'batch', 'edit_starttime', $system->SETTINGS['edit_starttime'], array($MSG['030'], $MSG['029']));
loadblock($MSG['068'], $MSG['070'], 'batch', 'cust_increment', $system->SETTINGS['cust_increment'], array($MSG['030'], $MSG['029']));
loadblock($MSG['5091'], $MSG['5095'], 'days', 'hours_countdown', $system->SETTINGS['hours_countdown'], array($MSG['25_0037']));
loadblock($MSG['3500_1015488'], $MSG['3500_1015492'], 'yesno', 'conditions', $system->SETTINGS['item_conditions'], array($MSG['030'], $MSG['029']));
loadblock($MSG['142'], $MSG['157'], 'yesno', 'ao_hpf_enabled', $system->SETTINGS['ao_hpf_enabled'], array($MSG['030'], $MSG['029']));
loadblock($MSG['162'], $MSG['164'], 'yesno', 'ao_hi_enabled', $system->SETTINGS['ao_hi_enabled'], array($MSG['030'], $MSG['029']));
loadblock($MSG['174'], $MSG['194'], 'yesno', 'ao_bi_enabled', $system->SETTINGS['ao_bi_enabled'], array($MSG['030'], $MSG['029']));
loadblock($MSG['797'], $MSG['798'], 'yesno', 'subtitle', $system->SETTINGS['subtitle'], array($MSG['030'], $MSG['029']));
loadblock($MSG['799'], $MSG['800'], 'yesno', 'extra_cat', $system->SETTINGS['extra_cat'], array($MSG['030'], $MSG['029']));
loadblock($MSG['849'], $MSG['850'], 'yesno', 'autorelist', $system->SETTINGS['autorelist'], array($MSG['030'], $MSG['029']));
loadblock($MSG['851'], $MSG['852'], 'days', 'autorelist_max', $system->SETTINGS['autorelist_max']);

// auction extension options
loadblock($MSG['2_0032'], '', '', '', '', array(), true); // :O
loadblock($MSG['2_0034'], $MSG['2_0039'], 'yesno', 'status', $system->SETTINGS['ae_status'], array($MSG['030'], $MSG['029']));
$string = $MSG['2_0035'] . '<input type="text" name="extend" value="' . $system->SETTINGS['ae_extend'] . '" size="5">' . $MSG['2_0036'] . '<input type="text" name="timebefore" value="' . $system->SETTINGS['ae_timebefore'] . '" size="5">' . $MSG['2_0037'];
loadblock('', $string, '');

// picture gallery options
loadblock($MSG['663'], '', '', '', '', array(), true);
loadblock($MSG['665'], $MSG['664'], 'batch', 'picturesgallery', $system->SETTINGS['picturesgallery'], array($MSG['030'], $MSG['029']));
loadblock($MSG['666'], $MSG['3500_1015759'], 'days', 'maxpictures', $system->SETTINGS['maxpictures']);
loadblock($MSG['3500_1015758'], $MSG['3500_1015760'], 'days', 'freemaxpictures', $system->SETTINGS['freemaxpictures']);
loadblock($MSG['671'], $MSG['25_0187'], 'decimals', 'maxpicturesize', ($system->SETTINGS['maxuploadsize'] / 1024), array($MSG['672']));
loadblock($MSG['25_0107'], $MSG['896'], 'decimals', 'thumb_show', $system->SETTINGS['thumb_show'], array($MSG['2__0045']));

loadblock($MSG['3500_1015722'], $MSG['3500_1015719'], 'decimals', 'max_image_width', $system->SETTINGS['max_image_width'], array($MSG['3500_1015721']));
loadblock($MSG['3500_1015723'], $MSG['3500_1015720'], 'decimals', 'max_image_height', $system->SETTINGS['max_image_height'], array($MSG['3500_1015721']));


$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['5142'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_asettings" target="_blank">' . $MSG['5087'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
