<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include $include_path . 'dates.inc.php';
include 'loggedin.inc.php';

// Insert new currency
if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (empty($_POST['name']) || empty($_POST['msgstoshow']) || empty($_POST['active']))
	{
		$ERR = $ERR_047;
	}
	elseif (!is_numeric($_POST['msgstoshow']))
	{
		$ERR = $ERR_5000;
	}
	elseif (intval($_POST['msgstoshow'] == 0))
	{
		$ERR = $ERR_5001;
	}
	else
	{
		$query = "UPDATE " . $DBPrefix . "community
				  SET name = :n,
				  msgstoshow = :ms,
				  active = :a
				  WHERE id = :i";
		$params = array();
		$params[] = array(':n', $system->cleanvars($_POST['name']), 'str');
		$params[] = array(':ms', intval($_POST['msgstoshow']), 'int');
		$params[] = array(':a', intval($_POST['active']), 'int');
		$params[] = array(':i', intval($_POST['id']), 'int');
		$db->query($query, $params);

		header('location: boards.php');
		exit;
	}
}

$id = intval($_GET['id']);

// Retrieve board's data from the database
$query = "SELECT * FROM " . $DBPrefix . "community WHERE id = :i";
$params = array();
$params[] = array(':i', $id, 'int');
$db->query($query, $params);
$board_data = $db->result();

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'NAME' => $board_data['name'],
		'MESSAGES' => $board_data['messages'],
		'LAST_POST' => ($board_data['lastmessage'] > 0) ? FormatDate($board_data['lastmessage']) : '--',
		'MSGTOSHOW' => $board_data['msgstoshow'],

		'B_ACTIVE' => ($board_data['active'] == 1),
		'B_DEACTIVE' => ($board_data['active'] == 2),
		'ID' => $id,
		'PAGENAME' => $MSG['5052']
		));

$template->set_filenames(array(
		'body' => 'editboards.tpl'
		));
$template->display('body');

?>