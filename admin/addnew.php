<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include $include_path . 'htmLawed.php';
include 'loggedin.inc.php';
include $main_path . 'inc/ckeditor/ckeditor.php';
unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Data check
	if (!isset($_POST['title']) || !isset($_POST['content']))
	{
		$ERR = $ERR_112;
	}
	else
	{
		// clean up everything
		$conf = array();
		$conf['safe'] = 1;
		foreach ($_POST['title'] as $k => $v)
		{
			$_POST['title'][$k] = htmLawed($v, $conf);
			$_POST['content'][$k] = htmLawed($_POST['content'][$k], $conf);
		}

		$query = "INSERT INTO " . $DBPrefix . "news VALUES (NULL, :t, :c, :tm, :s)";
		$params = array();
		$params[] = array(':t', stripslashes($_POST['title'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':c', stripslashes($_POST['content'][$system->SETTINGS['defaultlanguage']]), 'str');
		$params[] = array(':tm', $system->ctime, 'int');
		$params[] = array(':s', intval($_POST['suspended']), 'int');
		$db->query($query, $params);

		$news_id = $db->lastInsertId();

		// Insert into translation table
		foreach ($LANGUAGES as $k => $v)
		{
			$query = "INSERT INTO " . $DBPrefix . "news_translated VALUES (:id, :l, :t, :c)";
			$params = array();
			$params[] = array(':id', $news_id, 'int');
			$params[] = array(':l', $k, 'str');
			$params[] = array(':t', stripslashes($_POST['title'][$k]), 'str');
			$params[] = array(':c', stripslashes($_POST['content'][$k]), 'str');
			$db->query($query, $params);
		}
		header('location: news.php');
		exit;
	}
}
$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;
$CKEditor->config['width'] = 550;
$CKEditor->config['height'] = 400;

foreach ($LANGUAGES as $k => $language)
{
	$template->assign_block_vars('lang', array(
			'LANG' => $language,
			'TITLE' => (isset($_POST['title'][$k])) ? $_POST['title'][$k] : '',
			'CONTENT' => $CKEditor->editor('content[' . $k . ']', $_POST['content'][$k]) 
			));
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'TITLE' => $MSG['518'],
		'BUTTON' => $MSG['518'],
		'PAGENAME' => $MSG['518'],

		'B_ACTIVE' => ((isset($_POST['suspended']) && $_POST['suspended'] == 0) || !isset($_POST['suspended'])),
		'B_INACTIVE' => (isset($_POST['suspended']) && $_POST['suspended'] == 1)
		));

$template->set_filenames(array(
		'body' => 'addnew.tpl'
		));
$template->display('body');
?>