<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

$language = (isset($_GET['lang'])) ? $_GET['lang'] : 'EN';
$catscontrol = new MPTTcategories();

function search_cats($parent_id, $level)
{
	global $DBPrefix, $catscontrol;
	$catstr = '';
	$root = $catscontrol->get_virtual_root();
	$tree = $catscontrol->display_tree($root['left_id'], $root['right_id'], '|___');
	foreach ($tree as $k => $v)
	{
		$v = str_replace("'", "\'", $v);
		$catstr .= ",\n" . $k . " => '" . addslashes($v) . "'";
	}
	return $catstr;
}

function rebuild_cat_file($cats)
{
	global $language, $main_path;
	$output = "<?php\n";
	$output.= "$" . "category_names = array(\n";

	$num_rows = count($cats);

	$i = 0;
	foreach ($cats as $k => $v)
	{
		$v = str_replace("'", "\'", $v);
		$output .= "$k => '$v'";
		$i++;
		if ($i < $num_rows)
			$output .= ",\n";
		else
			$output .= "\n";
	}

	$output .= ");\n\n";

	$output .= "$" . "category_plain = array(\n0 => ''";

	$output .= search_cats(0, 0);

	$output .= ");\n?>";

	$handle = fopen ($main_path . 'language/' . $language . '/categories.inc.php', 'w');
	fputs($handle, $output);
	fclose($handle);
}

if (isset($_POST['categories']))
{
	rebuild_cat_file($_POST['categories']);
	include 'util_cc1.php';
}

include $main_path . 'language/' . $language . '/categories.inc.php';

$query = "SELECT cat_id, cat_name FROM " . $DBPrefix . "categories ORDER BY cat_name";
$db->direct_query($query);
$bg = '';
while ($row = $db->result())
{
	// set category data
	$template->assign_block_vars('cats', array(
		'CAT_ID' => $row['cat_id'],
		'CAT_NAME' => $system->uncleanvars($row['cat_name']),
		'TRAN_CAT' => $category_names[$row['cat_id']],
		'BG' => $bg
	));
	$bg = ($bg == '') ? 'class="bg"' : '';
}

$template->assign_vars(array(
	'ERROR' => (isset($ERR)) ? $ERR : '',
	'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_categories_trans_table" target="_blank">' . $MSG['132'] . '</a>',
	'SITEURL' => $system->SETTINGS['siteurl']
));

$template->set_filenames(array(
		'body' => 'categoriestrans.tpl'
		));
$template->display('body');
?>