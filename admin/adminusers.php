<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['delete']) && is_array($_POST['delete']))
{
	if (in_array($security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']), $_POST['delete']))
	{
		$ERR = $MSG['1100'];
	}
	else
	{
		foreach ($_POST['delete'] as $id)
		{
			$query = "DELETE FROM " . $DBPrefix . "adminusers WHERE id = :delete";
			$params = array();
			$params[] = array(':delete', $id, 'int');
			$db->query($query, $params);
		}
		$ERR = $MSG['1101'];
	}
}

$query = "SELECT * FROM " . $DBPrefix . "adminusers ORDER BY :u";
$params = array();
$params[] = array(':u', 'username', 'str');
$db->query($query, $params);

$STATUS = array(
	1 => '<span style="color:#00AF33"><b>Active</b></span>',
	2 => '<span style="color:#FF0000"><b>Not active</b></span>'
);

$bg = '';
while ($User = $db->result())
{
    $created = substr($User['created'], 4, 2) . '/' . substr($User['created'], 6, 2) . '/' . substr($User['created'], 0, 4);
    if ($User['lastlogin'] == 0)
    {
		$lastlogin = $MSG['570'];
    }
    else
    {
		$lastlogin = date('d/m/Y H:i:s', $User['lastlogin']);
    }

    $template->assign_block_vars('users', array(
			'ID' => $User['id'],
			'USERNAME' => $User['username'],
			'STATUS' => $STATUS[$User['status']],
			'CREATED' => $created,
			'LASTLOGIN' => $lastlogin,
			'BG' => $bg
			));
	$bg = ($bg == '') ? 'class="bg"' : '';
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'PAGENAME' => $MSG['525']
		));
		
$template->set_filenames(array(
		'body' => 'adminusers.tpl'
		));
$template->display('body');

?>