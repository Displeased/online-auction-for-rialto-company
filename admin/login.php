<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/

define('InAdmin', 1);
include '../common.php';
include $include_path . 'functions_admin.php';

if (isset($_POST['action']))
{
	switch ($_POST['action'])
	{
		case 'insert': //add new admin account
			// Additional security check
			$query = "SELECT id FROM " . $DBPrefix . "adminusers";
			$db->direct_query($query);
			if ($db->numrows() > 0)
			{
				header('location: login.php');
				exit;
			}
			$password = $_POST['password'];
			$admin_password = $phpass->HashPassword($password);
			$query = "INSERT INTO " . $DBPrefix . "adminusers VALUES (NULL, :user, :pass, :hash, :create, :login, :s, NULL)";
			$params = array();
			$params[] = array(':user', $system->cleanvars($_POST['username']), 'str');
			$params[] = array(':pass', $admin_password, 'str');
			$params[] = array(':hash', get_hash(), 'str');
			$params[] = array(':create', gmdate('Ymd'), 'int');
			$params[] = array(':login', $system->ctime, 'int');
			$params[] = array(':s', 1, 'int');
			$db->query($query, $params);
			$admin_id = $db->lastInsertId();
			
			//checking to see if the admin user was added and if it did now make the normail user account
			if (isset($admin_id))
			{
				$pass_word = (isset($_POST['pass_word'])) ? $_POST['pass_word'] : $admin_password;
				$query = "SELECT id FROM " . $DBPrefix . "groups WHERE auto_join = :j";
				$params = array();
				$params[] = array(':j', 1, 'int');
				$db->query($query, $params);
				$groups = array();
				while ($gid = $db->result('id'))
				{
						$groups[] = $gid;
				}
				$hashed_pass_word = $phpass->HashPassword($pass_word);
				$query = "INSERT INTO " . $DBPrefix . "users (nick, password, hash, name, admin, groups, reg_date, emailtype, startemailmode, endemailmode, nletter, email, language, rate_sum, rate_num, suspended, bn_only) VALUES 
				('" . $_POST['user_name'] . "', '" . $hashed_pass_word . "', '" . get_hash() . "', '" . $_POST['full_name'] . "', 2, '" . implode(',', $groups) . "', '" . $system->ctime . "', 'html', 'yes', 'one', 1, '" . $_POST['email'] . "', '" . $system->SETTINGS['defaultlanguage'] . "', 0, 0, 0, 'y')";
				$db->direct_query($query);
			}
			// Redirect
			header('location: login.php');
			exit;
		break;

		case 'login':
			if (strlen($_POST['username']) == 0 || strlen($_POST['password']) == 0)
			{
				$ERR = $ERR_047;
			}
			elseif (!preg_match('([a-zA-Z0-9]*)', $_POST['username']))
			{
				$ERR = $ERR_071;
			}
			else
			{
				$query = "SELECT id, password, hash FROM " . $DBPrefix . "adminusers WHERE username = :user_name";
				$params = array();
				$params[] = array(':user_name', $system->cleanvars($_POST['username']), 'str');
				$db->query($query, $params);
				
				// generate a random unguessable token
				$admin = $db->result();
				if($phpass->CheckPassword($_POST['password'], $admin['password']))
				{
					// Set sessions vars
					$_SESSION['admincsrftoken'] = $security->encrypt($security->genRandString(32));
					$_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_NUMBER'] = $security->encrypt(strspn($admin['password'], $admin['hash']));
					$_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_PASS'] = $security->encrypt($admin['password']);
					$_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN'] = $security->encrypt($admin['id']);
					$_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_USER'] = $security->encrypt($_POST['username']);
					$_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_TIME'] = $security->encrypt($system->ctime);
					
					
					$_SESSION['interface_user'] = $_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_USER'];
					$_SESSION['interface_pass'] = $_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_PASS'];
					// Update last login information for this user
					$query = "UPDATE " . $DBPrefix . "adminusers SET lastlogin = :timer WHERE id = :user_id";
					$params = array();
					$params[] = array(':timer', $system->ctime, 'int');
					$params[] = array(':user_id', $admin['id'], 'int');
					$db->query($query, $params);
					// Redirect
					print '<script type="text/javascript">parent.location.href = \'index.php\';</script>';
					exit;
				}
				else
				{
					$ERR = $ERR_048;
				}
			}
		break;
	}
}

$query = "SELECT id FROM " . $DBPrefix . "adminusers LIMIT :setlimit";
$params = array();
$params[] = array(':setlimit', 1, 'int');
$db->query($query, $params);

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'FLAGS' => ShowFlags(),
		'B_MULT_LANGS' => (count($LANGUAGES) > 1),
		'SITEURL' => $system->SETTINGS['siteurl'],
		'THEME' => $system->SETTINGS['theme'],
		'PAGE' => ($db->numrows() == 0) ? 1 : 2
		));

$template->set_filenames(array(
		'body' => 'login.tpl'
		));
$template->display('body'); 
?>