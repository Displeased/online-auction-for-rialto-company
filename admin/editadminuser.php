<?php
/*******************************************************************************
 *   copyright				: (C) 2008 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com/
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include $include_path . 'dates.inc.php';
include 'loggedin.inc.php';

unset($ERR);

$id = intval($_REQUEST['id']);
if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if ((!empty($_POST['password']) && empty($_POST['repeatpassword'])) || (empty($_POST['password']) && !empty($_POST['repeatpassword'])))
	{
		$ERR = $ERR_054;
	}
	elseif ($_POST['password'] != $_POST['repeatpassword'])
	{
		$ERR = $ERR_006;
	}
	else
	{ 
		// Update
		$password = $_POST['password'];
		$hashed_password = $phpass->HashPassword($password);		
		$query = "UPDATE " . $DBPrefix . "adminusers SET";
		$params = array();
		if (!empty($_POST['password']))
		{
			$query .= " password = :p, ";
			$params[] = array(':p', $hashed_password, 'str');
		}
		$query .= " status = :s	WHERE id = :i";
		$params[] = array(':s', intval($_POST['status']), 'int');
		$params[] = array(':i', $id, 'int');
		$db->query($query, $params);

		header('location: adminusers.php');
		exit;
	}
}

$query = "SELECT * FROM " . $DBPrefix . "adminusers WHERE id = :i";
$params = array();
$params[] = array(':i', $id, 'int');
$db->query($query, $params);
$user_data = $db->result();

if ($system->SETTINGS['datesformat'] == 'USA')
{
	$CREATED = substr($user_data['created'], 4, 2) . '/' . substr($user_data['created'], 6, 2) . '/' . substr($user_data['created'], 0, 4);
}
else
{
	$CREATED = substr($user_data['created'], 6, 2) . '/' . substr($user_data['created'], 4, 2) . '/' . substr($user_data['created'], 0, 4);
}

if ($user_data['lastlogin'] == 0)
{
	$LASTLOGIN = $MSG['570'];
}
else
{
	$LASTLOGIN = FormatDate($user_data['lastlogin']);
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'ID' => $id,
		'USERNAME' => $user_data['username'],
		'CREATED' => $CREATED,
		'LASTLOGIN' => $LASTLOGIN,
		'PAGENAME' => $MSG['511'],

		'B_ACTIVE' => ($user_data['status'] == 1),
		'B_INACTIVE' => ($user_data['status'] == 2)
		));

$template->set_filenames(array(
		'body' => 'editadminuser.tpl'
		));
$template->display('body');

?>