<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

function ToBeDeleted($index)
{
	if (!isset($_POST['delete']))
		return false;

	$i = 0;
	while ($i < count($_POST['delete']))
	{
		if ($_POST['delete'][$i] == $index) return true;
		$i++;
	}
	return false;
}


if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Build new payments array
	$rebuilt_array = array();
	for ($i = 0; $i < count($_POST['new_payments']); $i++)
	{
		if (!ToBeDeleted($i) && strlen($_POST['new_payments'][$i]) != 0)
		{
			$rebuilt_array[] = $_POST['new_payments'][$i];
		}
	}

	$system->SETTINGS['payment_options'] = serialize($rebuilt_array);
	$query = "UPDATE " . $DBPrefix . "settings SET payment_options = :po";
	$params = array();
	$params[] = array(':po', $system->SETTINGS['payment_options'], 'str');
	$db->query($query, $params);

	$ERR = $MSG['093'];
}

$payment_options = unserialize($system->SETTINGS['payment_options']);
foreach ($payment_options as $k => $v)
{
	$template->assign_block_vars('payments', array(
			'PAYMENT' => $v,
			'ID' => $k
			));
}


$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_payment_methods" target="_blank">' .  $MSG['075'] . '</a>',
		'ERROR' => (isset($ERR)) ? $ERR : ''
		));

$template->set_filenames(array(
		'body' => 'payments.tpl'
		));
$template->display('body');

?>