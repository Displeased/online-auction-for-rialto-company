<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'contents';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

$msg = intval($_REQUEST['msg']);
$board_id = intval($_REQUEST['id']);

// Insert new currency
if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (!isset($_POST['message']) || empty($_POST['message']))
	{
		$ERR = $ERR_047;
	}
	else
	{
		$query = "UPDATE " . $DBPrefix . "comm_messages SET message = :m WHERE id = :i";
		$params = array();
		$params[] = array(':m', $system->cleanvars($_POST['message']), 'str');
		$params[] = array(':i', $_POST['msg'], 'int');
		$db->query($query, $params);

		header("Location: editmessages.php?id=" . $_POST['id']);
		exit;
	}
}

// Retrieve board name for breadcrumbs
$query = "SELECT name FROM " . $DBPrefix . "community WHERE id = :i";
$params = array();
$params[] = array(':i', $board_id, 'int');
$db->query($query, $params);
$board_name = $db->result('name');

// Retrieve message from the database
$query = "SELECT * FROM " . $DBPrefix . "comm_messages WHERE id = :i";
$params = array();
$params[] = array(':i', $msg, 'int');
$db->query($query, $params);
$data = $db->result();

$template->assign_vars(array(
		'BOARD_NAME' => $board_name,
		'MESSAGE' => nl2br((isset($_POST['message'])) ? $_POST['message'] : $data['message']),
		'USER' => ($data['user'] > 0) ? $data['username'] : $MSG['5061'],
		'POSTED' => FormatDate($data['msgdate']),
		'BOARD_ID' => $board_id,
		'MSG_ID' => $msg
		));

$template->set_filenames(array(
		'body' => 'editmessage.tpl'
		));
$template->display('body');
?>