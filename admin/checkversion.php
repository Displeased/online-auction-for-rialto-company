<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'Check Version';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

$template->assign_vars(array(
	'ERROR' => (isset($ERR)) ? $ERR : '',
	'VERSION_TITLE' => $MSG['25_0169a'],
	'REQUIREMENTS_TITLE' => $MSG['3500_1015647'],
	'PHP_VERSION' => phpversion() > '5.3.0' ? true : false,
	'CURRENT_PHP_VERSION' => phpversion(),
	'GD' => extension_loaded('gd') && function_exists('gd_info') ? true : false,
	'BCMATH' => extension_loaded('bcmath') ? true : false,
	'PDO' => extension_loaded('pdo') ? true : false,
	'HASH_HMAC' => function_exists('hash_hmac') ? true : false,
	'MBSTRING' => extension_loaded('mbstring') ? true : false,
	'HEADERS_SUPPORT' => function_exists('getallheaders') ? true : false,
	'MCRYPT_ENCRYPT' => function_exists('mcrypt_encrypt') ? true : false,
	'OPEN_BASEDIR' => function_exists('open_basedir') ? true : false,
	'ALLOW_URL_FOPEN' => ini_get('allow_url_fopen') ? true : false,
	'FOPEN' => function_exists('fopen') ? true : false,
	'FREAD' => function_exists('fread') ? true : false,
	'FILE_GET_CONTENTS' => function_exists('file_get_contents') ? true : false,
	'CURL' => function_exists('curl_init') && function_exists('curl_setopt') && function_exists('curl_exec') && function_exists('curl_close') ? true : false
));

$template->set_filenames(array(
		'body' => 'checkversion.tpl'
		));
$template->display('body');
?>
