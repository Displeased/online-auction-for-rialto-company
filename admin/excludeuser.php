<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $include_path . 'countries.inc.php';
include $include_path . 'membertypes.inc.php';

if (!isset($_REQUEST['id']))
{
	header('location: listusers.php?PAGE=' . intval($_REQUEST['offset']));
	exit;
}

if (isset($_POST['action']) && $_POST['action'] == $MSG['030'])
{
	if ($_POST['mode'] == 'activate')
	{
		$query = "UPDATE " . $DBPrefix . "users SET suspended = :s WHERE id = :i";
		$params = array();
		$params[] = array(':s', 0, 'int');
		$params[] = array(':i', $_POST['id'], 'int');
		$db->query($query, $params);

		$query = "UPDATE " . $DBPrefix . "counters SET inactiveusers = inactiveusers - :i, users = users + :u";
		$params = array();
		$params[] = array(':i', 1, 'int');
		$params[] = array(':u', 1, 'int');
		$db->query($query, $params);

		$query = "SELECT name, email FROM " . $DBPrefix . "users WHERE id = :i";
		$params = array();
		$params[] = array(':i', $_POST['id'], 'int');
		$db->query($query, $params);
		$USER = $db->result();
		$send_email->approved($USER['name'], $language, $USER['email']);
	}
	else
	{
		$query = "UPDATE " . $DBPrefix . "users SET suspended = :s WHERE id = :i";
		$params = array();
		$params[] = array(':s', 1, 'int');
		$params[] = array(':i', $_POST['id'], 'int');
		$db->query($query, $params);

		$query = "UPDATE " . $DBPrefix . "counters SET inactiveusers = inactiveusers + :i, users = users - :u";
		$params = array();
		$params[] = array(':i', 1, 'int');
		$params[] = array(':u', 1, 'int');
		$db->query($query, $params);
	}

	header('location: listusers.php?PAGE=' . intval($_POST['offset']));
	exit;
}
elseif (isset($_POST['action']) && $_POST['action'] == $MSG['029'])
{
	header('location: listusers.php?PAGE=' . intval($_POST['offset']));
	exit;
}

// load the page
$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :i";
$params = array();
$params[] = array(':i', intval($_GET['id']), 'int');
$db->query($query, $params);
$user_data = $db->result();

// create tidy DOB string
if ($user_data['birthdate'] == 0)
{
	$birthdate = '';
}
else
{
	$birth_day = substr($user_data['birthdate'], 6, 2);
	$birth_month = substr($user_data['birthdate'], 4, 2);
	$birth_year = substr($user_data['birthdate'], 0, 4);

	if ($system->SETTINGS['datesformat'] == 'USA')
	{
		$birthdate = $birth_month . '/' . $birth_day . '/' . $birth_year;
	}
	else
	{
		$birthdate = $birth_day . '/' . $birth_month . '/' . $birth_year;
	}
}

$mode = 'activate';
switch ($user_data['suspended'])
{
	case 0:
		$action = $MSG['305'];
		$question = $MSG['308'];
		$mode = 'suspend';
		break;
	case 8:
		$action = $MSG['515'];
		$question = $MSG['815'];
		break;
	case 10:
		$action = $MSG['299'];
		$question = $MSG['418'];
		break;
	default:
		$action = $MSG['306'];
		$question = $MSG['309'];
		break;
}

$template->assign_vars(array(
		'ACTION' => $action,
		'REALNAME' => $user_data['name'],
		'USERNAME' => $user_data['nick'],
		'EMAIL' => $user_data['email'],
		'ADDRESS' => $user_data['address'],
		'PROV' => $user_data['prov'],
		'ZIP' => $user_data['zip'],
		'COUNTRY' => $user_data['country'],
		'PHONE' => $user_data['phone'],
		'DOB' => $birthdate,
		'QUESTION' => $question,
		'MODE' => $mode,
		'ID' => $_GET['id'],
		'OFFSET' => $_GET['offset']
		));

$template->set_filenames(array(
		'body' => 'excludeuser.tpl'
		));
$template->display('body');
?>