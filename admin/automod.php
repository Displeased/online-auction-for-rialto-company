<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

define('InAdmin', 1);
$current_page = 'tools';
include '../common.php';
include $include_path . 'functions_mods.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

//display the downloaded mods or install mods info
if ($install_files)
{
	$title = 'Install a New Mod';
	$mod_buttons = 'Install Mod';
	foreach($xml->edit as $child)
	{
	    $page = $xml_array->simplexml_to_array($child);
	    if (isset($child) && is_array($page))
	    {
	    	$pages = $page[open];
	    }
	    $find = $xml_array->simplexml_to_array($child->find);
	    if (isset($child->find) && is_array($find))
	    {
	    	$finding = $find[find];
	    }
	    $replace = $xml_array->simplexml_to_array($child->replace);
	    if (isset($child->replace) && is_array($replace))
	    {
	    	$replacing = $replace[replace];
	    	$replaceWith = $replacing;
	    }
	    else
	    {
	    	$replacing = ' ';
	    	$replaceWith = $replacing;
	    }
	    $page_folder = $xml_array->simplexml_to_array($child->directory_path);
	    if (isset($child->directory_path) && is_array($page_folder))
	    {
	    	$path = $page_folder[directory_path];
	    	$get_path = $main_path . $path;
			$get_both = $get_path . $pages;
		}
		else
		{
			$get_path = $main_path;
			$get_both = $get_path . $pages;
		}
	   	$get_page = $pages;
	   	$mod_name = (string) $xml->mod;
	   	$mod_version = (string) $xml->version;
		
		$contents = htmlentities(file_get_contents($get_both));
		$template->assign_block_vars('mod', array(
			'MAKER' => $xml->author,
			'MOD' => $xml->mod,
			'VERSION' => $xml->version,
			'PAGE' => $pages,
			'FOLDER' => $path,
			'FIND' => $finding,
			'REPLACE' => $replacing,
			'ADDAFTER' => $add_after,
			'ADDBEFORE' => $add_before,
		));
	if ($_POST['update'] == 'yes')
	{
			$change->changefile($get_path, $get_page, $finding, $replaceWith, $mod_name, $mod_version, $path);
	}
	//unseting all the arrays
	unset($pages);
	unset($finding);
	unset($replacing); 
	unset($path);
	} 
}

$ERR = $GLOBALS['ERR'];
$template->assign_vars(array(
		'B_ERROR' => isset($ERR),
		'ERROR' => $ERR,
		'FORM_2' => 'automod.php',
		'PAGE_TITLE' => $title,
		'BUTTON_TITLE' => $mod_buttons,
		'B_CHANGE' => true
		));
		
$template->set_filenames(array(
		'body' => 'automod.tpl'
		));
$template->display('body');
?>
