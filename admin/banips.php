<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'users';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (!empty($_POST['ip']))
	{
		$query = "INSERT INTO " . $DBPrefix . "usersips VALUES (NULL, :no,  :ban, :n, :d)";
		$params = array();
		$params[] = array(':no', 'NOUSER', 'str');
		$params[] = array(':ban', $_POST['ip'], 'int');
		$params[] = array(':n', 'next', 'str');
		$params[] = array(':d', 'deny', 'str');
		$db->query($query, $params);

	}
	if (is_array($_POST['delete']))
	{
		foreach ($_POST['delete'] as $k => $v)
		{
			$query = "DELETE FROM " . $DBPrefix . "usersips WHERE id = :i";
			$params = array();
			$params[] = array(':i', intval($v), 'int');
			$db->query($query, $params);
		}
	}
	if (is_array($_POST['accept']))
	{
		foreach ($_POST['accept'] as $k => $v)
		{
			$query = "UPDATE " . $DBPrefix . "usersips SET action = :a WHERE id = :i";
			$params = array();
			$params[] = array(':a', 'accept', 'str');
			$params[] = array(':i', intval($v), 'int');
			$db->query($query, $params);
		}
	}
	if (is_array($_POST['deny']))
	{
		foreach ($_POST['deny'] as $k => $v)
		{
			$query = "UPDATE " . $DBPrefix . "usersips SET action = :d WHERE id = :i";
			$params = array();
			$params[] = array(':d', 'deny', 'str');
			$params[] = array(':i', intval($v), 'int');
			$db->query($query, $params);
		}
	}
}

$query = "SELECT * FROM " . $DBPrefix . "usersips WHERE action = :d";
$params = array();
$params[] = array(':d', 'deny', 'str');
$db->query($query, $params);
$bg = '';
while ($row = $db->result())
{
	$template->assign_block_vars('ips', array(
			'ID' => $row['id'],
			'IP' => $row['ip'],
			'ACTION' => $row['action'],
			'BG' => $bg
			));
	$bg = ($bg == '') ? 'class="bg"' : '';
}

$template->assign_vars(array(
		'PAGENAME' => $MSG['2_0017'],
));

$template->set_filenames(array(
		'body' => 'banips.tpl'
		));
$template->display('body');
?>