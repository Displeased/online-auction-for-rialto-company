<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
unset($ERR);


if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	$domain = $system->SETTINGS['https'] == 'y' ? 'https://' . $_POST['siteurl'] : 'http://' . $_POST['siteurl'];
	
	// Data check
	if (empty($_POST['sitename']) || empty($_POST['siteurl']) || empty($_POST['adminmail']))
	{
		$ERR = $ERR_047;
	}
	elseif (!is_numeric($_POST['archiveafter']))
	{
		$ERR = $ERR_043;
	}
	else
	{		
		// Update data
		$query = "UPDATE " . $DBPrefix . "settings SET sitename = :name, adminmail = :email, siteurl = :site_url, copyright = :copy, cron = :cronjob, archiveafter = :delete_auctions, cache_theme = :cache";
		$params = array();
		$params[] = array(':name', $_POST['sitename'], 'str');
		$params[] = array(':email', $_POST['adminmail'], 'str');
		$params[] = array(':site_url', get_domain($domain), 'str');
		$params[] = array(':copy', $_POST['copyright'], 'str');
		$params[] = array(':cronjob', $_POST['cron'], 'int');
		$params[] = array(':delete_auctions', $_POST['archiveafter'], 'int');
		$params[] = array(':cache', $_POST['cache_theme'], 'str');
		$db->query($query, $params);
		$ERR = $MSG['542'];
	}

	$system->SETTINGS['sitename'] = $_POST['sitename'];
	$system->SETTINGS['siteurl'] = $system->SETTINGS['https'] == 'y' ? 'https://' . get_domain($domain) : 'http://' . get_domain($domain);
	$system->SETTINGS['adminmail'] = $_POST['adminmail'];
	$system->SETTINGS['copyright'] = $_POST['copyright'];
	$system->SETTINGS['cron'] = $_POST['cron'];
	$system->SETTINGS['archiveafter'] = $_POST['archiveafter'];
	$system->SETTINGS['cache_theme'] = $_POST['cache_theme'];
}

// general settings
loadblock($MSG['527'], $MSG['535'], 'text', 'sitename', $system->SETTINGS['sitename']);
loadblock($MSG['528'], $MSG['536'], 'text', 'siteurl', get_domain($system->SETTINGS['siteurl']));
loadblock($MSG['540'], $MSG['541'], 'text', 'adminmail', $system->SETTINGS['adminmail']);
loadblock($MSG['191'], $MSG['192'], 'text', 'copyright', $system->SETTINGS['copyright']);

// batch settings
loadblock($MSG['348'], '', '', '', '', array(), true);
loadblock($MSG['372'], $MSG['371'], 'batch', 'cron', $system->SETTINGS['cron'], array($MSG['373'], $MSG['374']));
loadblock($MSG['376'], $MSG['375'], 'days', 'archiveafter', $system->SETTINGS['archiveafter'], array($MSG['377']));

// optimisation settings
loadblock($MSG['725'], '', '', '', '', array(), true);
loadblock($MSG['726'], $MSG['727'], 'yesno', 'cache_theme', $system->SETTINGS['cache_theme'], array($MSG['030'], $MSG['029']));

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['5142'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_gsettings" target="_blank">' . $MSG['526'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
