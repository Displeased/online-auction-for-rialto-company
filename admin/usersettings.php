<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

$mail_protocol = array('0' => 'u-Auctions MAIL', '1' => 'MAIL', '2' => 'SMTP', '4' => 'SENDMAIL', '5'=> 'QMAIL', '3' => 'NEVER SEND EMAILS (may be useful for testing purposes)');
$smtp_secure_options =array('none' => 'None', 'tls' => 'TLS', 'ssl' => 'SSL');

if (isset($_POST['action']) && $_POST['action'] == 'update')
{	
	// Update database
	$query = "UPDATE ". $DBPrefix . "settings SET usersauth = :u, activationtype = :at, facebook_login = :fl, facebook_app_id = :fai, facebook_app_secret = :fas";	
	$params = array();
	$params[] = array(':u', $_POST['usersauth'], 'str');
	$params[] = array(':at', intval($_POST['usersconf']), 'int');
	$params[] = array(':fl', $_POST['facebook_login'], 'str');
	$params[] = array(':fai', $_POST['facebook_app_id'], 'str');
	$params[] = array(':fas', $_POST['facebook_app_secret'], 'str');
	$db->query($query, $params);

	$ERR = $MSG['895'];

	$system->SETTINGS['usersauth'] = $_POST['usersauth'];
	$system->SETTINGS['activationtype'] = $_POST['usersconf'];
	$system->SETTINGS['facebook_login'] = $_POST['facebook_login'];
	$system->SETTINGS['facebook_app_id'] = $_POST['facebook_app_id'];
	$system->SETTINGS['facebook_app_secret'] = $_POST['facebook_app_secret'];
}

loadblock($MSG['25_0151'], $MSG['25_0152'], 'yesnostacked', 'usersauth', $system->SETTINGS['usersauth'], array($MSG['2__0066'], $MSG['2__0067']));
loadblock($MSG['25_0151_a'], $MSG['25_0152_a'], 'select3num', 'usersconf', $system->SETTINGS['activationtype'], array($MSG['25_0152_b'], $MSG['25_0152_c'], $MSG['25_0152_d']));


////connect with facebook
loadblock($MSG['350_10201'], $MSG['350_10200'], '', '', array(), true);
loadblock($MSG['350_10196'], $MSG['350_10197'], 'yesnostacked', 'facebook_login', $system->SETTINGS['facebook_login'], array($MSG['030'], $MSG['029']));
loadblock($MSG['350_10194'], '', 'text', 'facebook_app_id', $system->SETTINGS['facebook_app_id']);
loadblock($MSG['350_10195'], '', 'text', 'facebook_app_secret', $system->SETTINGS['facebook_app_secret']);
 
$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['25_0008'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_usettings" target="_blank">' . $MSG['894'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>