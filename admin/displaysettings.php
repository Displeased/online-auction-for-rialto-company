<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


define('InAdmin', 1);
$current_page = 'settings';
include '../common.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Update database
	$query = "UPDATE ". $DBPrefix . "settings SET perpage = :p, thumb_list = :t, lastitemsnumber = :l, hotitemsnumber = :h, endingsoonnumber = :e, loginbox = :lb, newsbox = :n, newstoshow = :ns, helpbox = :help, featureditemsnumber = :featured";
	$params = array();
	$params[] = array(':p', $_POST['perpage'], 'int');
	$params[] = array(':t', intval($_POST['thumb_list']), 'int');
	$params[] = array(':l', intval($_POST['lastitemsnumber']), 'int');
	$params[] = array(':h', intval($_POST['hotitemsnumber']), 'int');
	$params[] = array(':e', intval($_POST['endingsoonnumber']), 'int');
	$params[] = array(':lb', intval($_POST['loginbox']), 'int');
	$params[] = array(':n', intval($_POST['newsbox']), 'int');
	$params[] = array(':ns', intval($_POST['newstoshow']), 'int');
	$params[] = array(':help', intval($_POST['helpbox']), 'int');
	$params[] = array(':featured', intval($_POST['featureditemsnumber']), 'int');
	$db->query($query, $params);

	$system->SETTINGS['perpage'] = $_POST['perpage'];
	$system->SETTINGS['thumb_list'] = $_POST['thumb_list'];
	$system->SETTINGS['loginbox'] = $_POST['loginbox'];
	$system->SETTINGS['newsbox'] = $_POST['newsbox'];
	$system->SETTINGS['newstoshow'] = $_POST['newstoshow'];
	$system->SETTINGS['lastitemsnumber'] = $_POST['lastitemsnumber'];
	$system->SETTINGS['hotitemsnumber'] = $_POST['hotitemsnumber'];
	$system->SETTINGS['endingsoonnumber'] = $_POST['endingsoonnumber'];
	$system->SETTINGS['helpbox'] = $_POST['helpbox'];
	$system->SETTINGS['featureditemsnumber'] = $_POST['featureditemsnumber'];
	$ERR = $MSG['795'];
}

loadblock($MSG['789'], $MSG['790'], 'days', 'perpage', $system->SETTINGS['perpage']);
loadblock($MSG['25_0107'], $MSG['808'], 'decimals', 'thumb_list', $system->SETTINGS['thumb_list'], array($MSG['2__0045']));

loadblock($MSG['807'], '', '', '', '', array(), true);
loadblock($MSG['350_10206'], $MSG['3500_1015797'], 'days', 'featureditemsnumber', $system->SETTINGS['featureditemsnumber']);
loadblock($MSG['5015'], $MSG['5016'], 'days', 'hotitemsnumber', $system->SETTINGS['hotitemsnumber']);
loadblock($MSG['5013'], $MSG['5014'], 'days', 'lastitemsnumber', $system->SETTINGS['lastitemsnumber']);
loadblock($MSG['5017'], $MSG['5018'], 'days', 'endingsoonnumber', $system->SETTINGS['endingsoonnumber']);
loadblock($MSG['3500_1015766'], $MSG['3500_1015767'], 'batch', 'helpbox', $system->SETTINGS['helpbox'], array($MSG['030'], $MSG['029']));
loadblock($MSG['532'], $MSG['537'], 'batch', 'loginbox', $system->SETTINGS['loginbox'], array($MSG['030'], $MSG['029']));
loadblock($MSG['533'], $MSG['538'], 'batch', 'newsbox', $system->SETTINGS['newsbox'], array($MSG['030'], $MSG['029']));
loadblock('', $MSG['554'], 'days', 'newstoshow', $system->SETTINGS['newstoshow']);

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['5142'],
		'PAGENAME' => '<a style="color:lime" href="https://www.u-auctions.com/wiki/doku.php?id=u-auctions_dsettings" target="_blank">' . $MSG['788'] . '</a>'
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
