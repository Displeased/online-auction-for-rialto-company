<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
include 'common.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'selling.php';
	header('location: user_login.php');
	exit;
}

//set item shipped
$update = false;
if (isset($_POST['update']) && $_POST['update'] == 'paid')
{
	$query = "UPDATE " . $DBPrefix . "winners SET paid = :set_paid, is_counted = :set_counted WHERE id = :wid AND seller = :user_id";
	$params = array();
	$params[] = array(':set_paid', 1, 'int');
	$params[] = array(':set_counted', 'y', 'bool');
	$params[] = array(':wid', $_POST['db_id'], 'int');
	$params[] = array(':user_id', $_POST['user_id'], 'int');
	$db->query($query, $params);
	
	$query = "UPDATE " . $DBPrefix . "counters SET items_sold = :sold";
	$params = array();
	$params[] = array(':sold', $system->COUNTERS['items_sold'] + 1, 'int');
	$db->query($query, $params);

	if (!empty($_POST['item']))
	{
		//Digital item was paid now send email with digital item link
		//Get auction id, winner id and seller id 
		$query = "SELECT DISTINCT w.seller, d.seller, u.nick, u.email
			FROM " . $DBPrefix . "winners w
		    LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.seller) 
		    LEFT JOIN " . $DBPrefix . "digital_items d ON (d.auctions = w.auction)
		    WHERE w.id = :get_paid AND d.hash = :get_item";
		$params = array();
		$params[] = array(':get_paid', $_POST['db_id'], 'int');
		$params[] = array(':get_item', $_POST['item'], 'str');
		$db->query($query, $params);
		$sellerdata = $db->result();
		
		$query = "SELECT DISTINCT w.bid, w.winner, w.auction, w.bid, b.pict_url, b.title, d.item, d.seller, d.hash, u.nick, u.email, u.name, pict_url
			FROM " . $DBPrefix . "winners w
		    LEFT JOIN " . $DBPrefix . "auctions b ON (b.id = w.auction) 
		    LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.winner) 
		    LEFT JOIN " . $DBPrefix . "digital_items d ON (d.auctions = w.auction)
		    WHERE w.id = :get_paid AND d.hash = :get_item"; 
		$params = array();
		$params[] = array(':get_paid', $_POST['db_id'], 'int');
		$params[] = array(':get_item', $security->decrypt($_POST['item']), 'str');
		$db->query($query, $params); 
			
		while ($data = $db->result())
		{
			$send_email->digital_item_email_pt2($data['title'], $data['name'], $data['auction'], $data['bid'], $sellerdata['nick'], $sellerdata['email'], $security->encrypt($data['hash']), $data['pict_url'], $data['winner'], $data['email']);
		}
	}
	$update = true;
}
if (isset($_POST['update']) && $_POST['update'] == 'shipped')
{
	$query = "UPDATE " . $DBPrefix . "winners SET shipped = :set_shipped WHERE id = :wid AND seller = :user_id";
	$params = array();
	$params[] = array(':set_shipped', 1, 'int');
	$params[] = array(':wid', $_POST['shipped'], 'int');
	$params[] = array(':user_id', $_POST['user_id'], 'int');
	$db->query($query, $params);
	$update = true;
}

//checking to see if the ID is being used
$wid = (isset($_GET['id'])) ? intval($_GET['id']) : false;

//prevent users from refreshing the pages to change the DB
if ($update == true)
{
	if(!empty($wid))
	{
		header('location: selling.php?id=' . $wid);
		exit;
	}
	else
	{
		header('location: selling.php');
		exit;
	}
}

//checking if the ID is used
$params = array();
if (!empty($wid))
{
	$searchid = ' AND a.id = :auc_id ';
	$params[] = array(':auc_id', $wid, 'int');
}
else
{
	$searchid = ' ';
}

// Retrieve active auctions from the database
$query = "SELECT w.id
	FROM " . $DBPrefix . "auctions a
	LEFT JOIN " . $DBPrefix . "winners w ON (w.auction = a.id)
	LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.winner)
	LEFT JOIN " . $DBPrefix . "digital_items d ON (d.auctions = a.id)
	WHERE (a.closed = 1 OR a.bn_only = 'y') AND a.suspended = 0 AND a.user = :seller_id" . $searchid . "ORDER BY w.closingdate DESC";
$params[] = array(':seller_id', $user->user_data['id'], 'int');
$db->query($query, $params);
$TOTALAUCTIONS = $db->numrows('id');

if (!isset($_GET['PAGE']) || $_GET['PAGE'] <= 1 || $_GET['PAGE'] == '')
{
	$OFFSET = 0;
	$PAGE = 1;
}
else
{
	$PAGE = intval($_GET['PAGE']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}
$PAGES = ($TOTALAUCTIONS == 0) ? 1 : ceil($TOTALAUCTIONS / $system->SETTINGS['perpage']);

//checking if the ID is used
$params = array();
if (!empty($wid))
{
	$searchid = ' AND a.id = :auc_id ';
	$params[] = array(':auc_id', $wid, 'int');
}
else
{
	$searchid = ' ';
}

// Get closed auctions with winners
$query = "SELECT a.title, a.ends, w.id, w.is_read, w.closingdate, w.seller, d.hash, w.auction, w.shipped, w.bid, w.qty, w.winner, w.seller, w.paid, w.feedback_sel, u.nick, d.item
	FROM " . $DBPrefix . "auctions a
	LEFT JOIN " . $DBPrefix . "winners w ON (w.auction = a.id)
	LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.winner)
	LEFT JOIN " . $DBPrefix . "digital_items d ON (d.auctions = a.id)
	WHERE (a.closed = 1 OR a.bn_only = 'y') AND a.suspended = 0 AND a.user = :seller_id" . $searchid . "ORDER BY w.closingdate DESC LIMIT :offset, :perpage";
$params[] = array(':seller_id', $user->user_data['id'], 'int');
$params[] = array(':offset', $OFFSET, 'int');
$params[] = array(':perpage', $system->SETTINGS['perpage'], 'int');
$db->query($query, $params);
$i = 0;
while ($row = $db->result())
{
	$i++;

	if ((isset($wid) > 0) && $row['is_read'] == 0)
	{
		$query = "UPDATE " . $DBPrefix . "winners SET is_read = :set_is_read WHERE auction = :get_id AND seller = :user_id";
		$params[] = array(':set_is_read', 1, 'int');
		$params[] = array(':get_id', $wid, 'int');
		$params[] = array(':user_id', $row['seller'], 'int');
		$db->query($query, $params);
	}

	$template->assign_block_vars('a', array(
		'TITLE' => $row['title'],
		'COUNTER' => $i,
		'ENDS' => FormatDate($row['closingdate']),
		'SEO_TITLE' => generate_seo_link($row['title']),
		'AUCTIONID' => $row['auction'],
		'BGCOLOUR' => (!($i % 2)) ? '' : 'class="warning"',
		'ID' => $row['id'],
		'BIDF' => $system->print_money($row['bid']),
		'QTY' => (!empty($row['item'])) ? 1 : $row['qty'],
		'NICK' => $row['nick'],
		'WINNERID' => $row['winner'],
		'SELLERID' => $row['seller'],
		'DIGITAL_ITEM' => $security->encrypt($row['hash']),
		
		'B_SHIPPED_0' => ($row['shipped'] == 0) ? true : false,
		'B_SHIPPED_1' => ($row['shipped'] == 1) ? true : false,
		'B_SHIPPED_2' => ($row['shipped'] == 2) ? true : false,
		
		'B_DIGITAL_ITEM' => (isset($row['item'])),
		'B_PAID' => ($row['paid'] == 1),
		'B_FB' => ($row['feedback_sel'] == 0) ? true : false,
		'MARK_PAID_MSG' => sprintf($MSG['3500_1015534'], '( ' . $row['title'] . ' )'),
		'MARK_SHIPPED_MSG' => sprintf($MSG['3500_1015535'], '( ' . $row['title'] . ' )'),
	));
}

// get pagenation
$PREV = intval($PAGE - 1);
$NEXT = intval($PAGE + 1);
if ($PAGES > 1)
{
	$LOW = $PAGE - 5;
	if ($LOW <= 0) $LOW = 1;
	$COUNTER = $LOW;
	while ($COUNTER <= $PAGES && $COUNTER < ($PAGE + 6))
	{
		$template->assign_block_vars('pages', array(
				'PAGE' => ($PAGE == $COUNTER) ? '<b>' . $COUNTER . '</b>' : isset($wid) ? '<a href="' . $system->SETTINGS['siteurl'] . 'selling.php?id=' . $wid . '&PAGE=' . $COUNTER . '"><u>' . $COUNTER . '</u></a>' : '<a href="' . $system->SETTINGS['siteurl'] . 'selling.php?PAGE=' . $COUNTER . '"><u>' . $COUNTER . '</u></a>'
				));
		$COUNTER++;
	}
}

$template->assign_vars(array(
	'PREV' => ($PAGES > 1 && $PAGE > 1) ? isset($wid) ? '<a href="' . $system->SETTINGS['siteurl'] . 'selling.php?id=' . $wid . '&PAGE=' . $PREV . '"><u>' . $MSG['5119'] . '</u></a>&nbsp;&nbsp;' : '<a href="' . $system->SETTINGS['siteurl'] . 'selling.php?PAGE=' . $PREV . '"><u>' . $MSG['5119'] . '</u></a>&nbsp;&nbsp;' : '',
	'NEXT' => ($PAGE < $PAGES) ? isset($wid) ? '<a href="' . $system->SETTINGS['siteurl'] . 'selling.php?id=' . $wid . '&PAGE=' . $NEXT . '"><u>' . $MSG['5120'] . '</u></a>' : '<a href="' . $system->SETTINGS['siteurl'] . 'selling.php?PAGE=' . $NEXT . '"><u>' . $MSG['5120'] . '</u></a>' : '',
	'PAGE' => $PAGE,
	'PAGES' => $PAGES,
));

$template->assign_vars(array(
	'NUM_WINNERS' => $i,
	'SELLER_ID' => $user->user_data['id']
));

include 'header.php';
$TMP_usmenutitle = $MSG['453'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'selling.tpl'
		));
$template->display('body');
include 'footer.php';