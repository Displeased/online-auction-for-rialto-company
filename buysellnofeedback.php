<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'buysellnofeedback.php';
	header('location: user_login.php');
	exit;
}

$query = "SELECT DISTINCT a.auction, a.seller, a.winner, a.bid, b.id, b.current_bid, b.title, a.qty, a.closingdate
		FROM " . $DBPrefix . "winners a
		LEFT JOIN " . $DBPrefix . "auctions b ON (a.auction = b.id)
		WHERE (b.closed = 1 OR b.bn_only = 'y')
		AND b.suspended = 0
		AND ((a.seller = :user_id AND a.feedback_sel = 0) OR (a.winner = :user_ids AND a.feedback_win = 0))
		AND a.paid = 1";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$params[] = array(':user_ids', $user->user_data['id'], 'int');
$db->query($query, $params);

$k = 0;
while ($row = $db->result())
{
	$them = ($row['winner'] == $user->user_data['id']) ? $row['seller'] : $row['winner'];
	// Get details
	$query = "SELECT u.nick, u.email
			FROM " . $DBPrefix . "users u
			WHERE u.id = :them";
	$params = array();
	$params[] = array(':them', $them, 'int');
	$db->query($query, $params);
	$info = $db->result();

	$template->assign_block_vars('fbs', array(
			'ID' => $row['id'],
			'ROWCOLOUR' => ($k % 2) ? 'bgcolor="#FFFEEE"' : '',
			'TITLE' => $row['title'],
			'WINORSELLNICK' => $info['nick'],
			'WINORSELL' => ($row['winner'] == $user->user_data['id']) ? $MSG['25_0002'] : $MSG['25_0001'],
			'WINORSELLEMAIL' => $info['email'],
			'SEO_TITLE' => generate_seo_link($row['title']),
			'BID' => $row['bid'],
			'BIDFORM' => $system->print_money($row['bid']),
			'QTY' => ($row['qty'] == 0) ? 1 : $row['qty'],
			'WINNER' => $row['winner'],
			'SELLER' => $row['seller'],
			'CLOSINGDATE' => FormatDate($row['closingdate']),
			'WS' => ($row['winner'] == $user->user_data['id']) ? 'w' : 's'
			));
	$k++;
}

$template->assign_vars(array(
		'NUM_AUCTIONS' => $k
		));

$TPL_rater_nick = $user->user_data['nick'];
include 'header.php';
$TMP_usmenutitle = $MSG['207'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'sellbuyfeedback.tpl'
		));
$template->display('body');
include 'footer.php';