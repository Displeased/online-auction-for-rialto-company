<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
include 'common.php';
include $include_path . 'dates.inc.php';
include $include_path . 'membertypes.inc.php';
include $main_path . 'language/' . $language . '/categories.inc.php';

if(!empty($_SESSION['SELL_title']))
{
	include $include_path . 'functions_sell.php';
	unsetsessions();
}

// Get parameters from the URL
foreach ($membertypes as $idm => $memtypearr)
{
	$memtypesarr[$memtypearr['feedbacks']] = $memtypearr;
}
ksort($memtypesarr, SORT_NUMERIC);


$get_name = $_REQUEST['id'] ;  //NEW get the full product name
$get_array = explode('-',htmlentities($system->uncleanvars($get_name), ENT_COMPAT, $CHARSET)) ;  //NEW split the product name into segments and put into an array (product id must be separated by '-' )
$get_id = end($get_array) ; //NEW extract the last array i.e product id form full product name
$id = (isset($_SESSION['CURRENT_ITEM'])) ? intval($_SESSION['CURRENT_ITEM']) : 0;
$id = (isset($get_id)) ? intval($get_id) : 0; if (!is_numeric($id)) $id = 0;
$bidderarray = array();
$bidderarraynum = 1;
$catscontrol = new MPTTcategories();

$_SESSION['CURRENT_ITEM'] = $id;

// get auction all needed data
$query = "SELECT a.*, ac.counter, u.city, u.nick, u.reg_date, u.country, u.zip FROM " . $DBPrefix . "auctions a
LEFT JOIN " . $DBPrefix . "users u ON (u.id = a.user)
LEFT JOIN " . $DBPrefix . "auccounter ac ON (ac.auction_id = a.id)
WHERE a.id = :auction_id LIMIT :limit";
$params = array();
$params[] = array(':auction_id', $id, 'int');
$params[] = array(':limit', 1, 'int');
$db->query($query, $params);
if ($db->numrows() == 0)
{
	$_SESSION['msg_title'] = $ERR_622;
	$_SESSION['msg_body'] = $ERR_623;
	header('location: ' . $system->SETTINGS['siteurl'] . 'message.php');
	exit;
}
$auction_data = $db->result();
$category = $auction_data['category'];
$auction_type = $auction_data['auction_type'];
$ends = $auction_data['ends'];
$start = $auction_data['starts'];
$user_id = $auction_data['user'];
$minimum_bid = $auction_data['minimum_bid'];
$high_bid = $auction_data['current_bid'];
$customincrement = $auction_data['increment'];
$seller_reg = FormatDate($auction_data['reg_date'], '/', false);
$item_condition = $auction_data['item_condition'];
$item_manufacturer = $auction_data['item_manufacturer'];
$item_model = $auction_data['item_model'];
$item_colour = $auction_data['item_colour'];
$item_year = $auction_data['item_year'];
$titel = $auction_data['title'];
$setmetatags = $auction_data['description'];
$city = $auction_data['city'];
$page_title = $titel;

$_SESSION['REDIRECT_AFTER_LOGIN'] = $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($titel) . '-' . $id;

if(isset($_POST['faveseller']) && $_POST['faveseller'] == 'yes')
{
	if ($user->logged_in)
	{
		if(is_fave($user_id))
		{
			add_fave($user_id);
			$fsm = $MSG['FSM3'];
		}
	}
}
else
{
	if ($user->logged_in)
	{
		if (is_fave($user_id))
		{
			$faveset = 1;
		}
		else
		{
			$fsm = $MSG['FSM3'];
		}
	}
	else
	{
		$fsm = $MSG['FSM2'];
	}
}

// sort out counter
if (empty($auction_data['counter']))
{
	$query = "INSERT INTO `" . $DBPrefix . "auccounter` (`auction_id`, `counter`) VALUES (:counter, :limit)";
	$params = array();
	$params[] = array(':counter', $id, 'int');
	$params[] = array(':limit', 1, 'int');
	$db->query($query, $params);
	$auction_data['counter'] = 1;
}
else
{
	if (!isset($_SESSION[$system->SETTINGS['sessions_name'] . '_VIEWED_AUCTIONS']))
	{
		$_SESSION[$system->SETTINGS['sessions_name'] . '_VIEWED_AUCTIONS'] = array();
	}
	if (!in_array($id, $_SESSION[$system->SETTINGS['sessions_name'] . '_VIEWED_AUCTIONS']))
	{
		$query = "UPDATE " . $DBPrefix . "auccounter set counter = counter + 1 WHERE auction_id = :auction_id";
		$params = array();
		$params[] = array(':auction_id', $id, 'int');
		$db->query($query, $params);
		$_SESSION[$system->SETTINGS['sessions_name'] . '_VIEWED_AUCTIONS'][] = $id;
	}
}

// get watch item data
if ($user->logged_in)
{
	// Check if this item is not already added
	$query = "SELECT item_watch FROM " . $DBPrefix . "users WHERE id = :user_id";
	$params = array();
	$params[] = array(':user_id', $user->user_data['id'], 'int');
	$db->query($query, $params);

	$watcheditems = $db->result();
	$auc_ids = explode(' ', $watcheditems['item_watch']);
	if (in_array($id, $auc_ids))
	{
		$watch_var = 'delete';
		$watch_string = $MSG['5202_0'];
	}
	else
	{
		$watch_var = 'add';
		$watch_string = $MSG['5202'];
	}
}
else
{
	$watch_var = '';
	$watch_string = '';
}

// get ending time
$difference = $ends - $system->ctime;
$showendtime = false;
$has_ended = false;
if ($start > $system->ctime)
{
	$ending_time = '<span class="errfont">' . $MSG['668'] . '</span>';
}
elseif ($difference > 0)
{
	$ending_time = '';
	$d = 0;
	$days_difference = floor($difference / 86400);
	if ($days_difference > 0)
	{
		$daymsg = ($days_difference == 1) ? $MSG['126b'] : $MSG['126'];
		$ending_time .= $days_difference . ' ' . $daymsg . ' ';
		$d++;
	}
	$difference = $difference % 86400;
	$hours_difference = floor($difference / 3600);
	if ($hours_difference > 0)
	{
		$ending_time .= $hours_difference . $MSG['25_0037'] . ' ';
		$d++;
	}
	$difference = $difference % 3600;
	$minutes_difference = floor($difference / 60);
	$seconds_difference = $difference % 60;
	if ($minutes_difference > 0 && $d < 2)
	{
		$ending_time .= $minutes_difference . $MSG['25_0032'] . ' ';
		$d++;
	}
	if ($seconds_difference > 0 && $d < 2)
	{
		$ending_time .= $seconds_difference . $MSG['25_0033'];
	}
	$showendtime = true;
}
else
{
	$ending_time = '<span class="errfont">' . $MSG['911'] . '</span>';
	$has_ended = true;
}

// build bread crumbs
$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :cat_id";
$params = array();
$params[] = array(':cat_id', $auction_data['category'], 'int');
$db->query($query, $params);
$parent_node = $db->result();

$cat_value = '';
$crumbs = $catscontrol->get_bread_crumbs($parent_node['left_id'], $parent_node['right_id']);
for ($i = 0; $i < count($crumbs); $i++)
{
	if ($crumbs[$i]['cat_id'] > 0)
	{
		if ($i > 0)
		{
			$cat_value .= ' > ';
		}
		$cat_value .= '<a href="' . $system->SETTINGS['siteurl'] . 'browse.php?id=' . $crumbs[$i]['cat_id'] . '">' . $category_names[$crumbs[$i]['cat_id']] . '</a>';
	}
}

$secondcat_value = '';
if ($system->SETTINGS['extra_cat'] == 'y' && intval($auction_data['secondcat']) > 0)
{
	$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :sec_cat_id";
	$params = array();
	$params[] = array(':sec_cat_id', $auction_data['secondcat'], 'int');
	$db->query($query, $params);
	$parent_node = $db->result();

	$crumbs = $catscontrol->get_bread_crumbs($parent_node['left_id'], $parent_node['right_id']);
	for ($i = 0; $i < count($crumbs); $i++)
	{
		if ($crumbs[$i]['cat_id'] > 0)
		{
			if ($i > 0)
			{
				$secondcat_value .= ' > ';
			}
			$secondcat_value .= '<a href="' . $system->SETTINGS['siteurl'] . 'browse.php?id=' . $crumbs[$i]['cat_id'] . '">' . $category_names[$crumbs[$i]['cat_id']] . '</a>';
		}
	}
}

// history
$query = "SELECT b.*, u.nick, u.rate_sum FROM " . $DBPrefix . "bids b
LEFT JOIN " . $DBPrefix . "users u ON (u.id = b.bidder)
WHERE b.auction = :auct_ids ORDER BY b.bid DESC, b.quantity DESC, b.id DESC";
$params = array();
$params[] = array(':auct_ids', $id, 'int');
$db->query($query, $params);
$num_bids = $db->numrows();
$i = 0;
$left = $auction_data['quantity'];
$hbidder_data = array();
foreach ($db->fetchall() as $bidrec)
{
	if (!isset($bidderarray[$bidrec['nick']]))
	{
		if ($system->SETTINGS['buyerprivacy'] == 'y' && $user->user_data['id'] != $auction_data['user'] && $user->user_data['id'] != $bidrec['bidder'])
		{
			$bidderarray[$bidrec['nick']] = $MSG['176'] . ' ' . $bidderarraynum;
			$bidderarraynum++;
		}
		else
		{
			$bidderarray[$bidrec['nick']] = $bidrec['nick'];
		}
	}
	if ($left > 0 && !in_array($bidrec['bidder'], $hbidder_data)) //store highest bidder details
	{
		$hbidder_data[] = $bidrec['bidder'];
		$fb_pos = $fb_neg = 0;
		// get seller feebacks
		$query = "SELECT rate FROM " . $DBPrefix . "feedbacks WHERE rated_user_id = :rate_users_ids";
		$params = array();
		$params[] = array(':rate_users_ids', $bidrec['bidder'], 'int');
		$db->query($query, $params);
		// count numbers
		$fb_pos = $fb_neg = 0;
		while ($fb_arr = $db->fetchall())
		{
			$arr = isset($fb_arr['rate']) ? $fb_arr['rate'] : '';
			if ($arr == 1)
			{
				$fb_pos++;
			}
			elseif ($arr == - 1)
			{
				$fb_neg++;
			}
		}

		$total_rate = $fb_pos - $fb_neg;

		foreach ($memtypesarr as $k => $l)
		{
			if ($k >= $total_rate || $i++ == (count($memtypesarr) - 1))
			{
				$buyer_rate_icon = $l['icon'];
				break;
			}
		}
			$template->assign_block_vars('high_bidders', array(
					'BUYER_ID' => $bidrec['bidder'],
					'BUYER_NAME' => $bidderarray[$bidrec['nick']],
					'BUYER_FB' => $bidrec['rate_sum'],
					'BUYER_FB_ICON' => (!empty($buyer_rate_icon) && $buyer_rate_icon != 'transparent.gif') ? '<img src="' . $system->SETTINGS['siteurl'] . 'images/icons/' . $buyer_rate_icon . '" alt="' . $buyer_rate_icon . '" class="fbstar">' : ''
					));
		
	}
	$template->assign_block_vars('bidhistory', array(
			'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
			'ID' => $bidrec['bidder'],
			'NAME' => $bidderarray[$bidrec['nick']],
			'BID' => $system->print_money($bidrec['bid']),
			'WHEN' => ArrangeDateNoCorrection($bidrec['bidwhen'] + $system->tdiff) . ':' . gmdate('s', $bidrec['bidwhen']),
			'QTY' => $bidrec['quantity']
			));
	$left -= $bidrec['quantity'];
	$i++;
}

$userbid = false;
if ($user->logged_in && $num_bids > 0)
{
	// check if youve bid on this before
	$query = "SELECT bid FROM " . $DBPrefix . "bids WHERE auction = :auction AND bidder = :bidder LIMIT 1";
	$params = array();
	$params[] = array(':auction', $id, 'int');
	$params[] = array(':bidder', $user->user_data['id'], 'int');
	$db->query($query, $params);
	if ($db->numrows('bid') > 0)
	{
		if (in_array($user->user_data['id'], $hbidder_data))
		{
			$yourbidmsg = $MSG['25_0088'];
			$yourbidclass = 'yourbidwin';
			if ($difference <= 0 && $auction_data['reserve_price'] > 0 && $auction_data['current_bid'] < $auction_data['reserve_price'])
			{
				$yourbidmsg = $MSG['514'];
				$yourbidclass = 'yourbidloss';
			}
			elseif ($difference <= 0 || $auction_data['bn_only'] == 'y')
			{
				$yourbidmsg = $MSG['25_0089'];
			}
		}
		else
		{
			$yourbidmsg = $MSG['25_0087'];
			$yourbidclass = 'yourbidloss';
		}
		$userbid = true;
	}
}

// sort out user questions
$query = "SELECT id FROM " . $DBPrefix . "messages WHERE reply_of = 0 AND public = 1 AND question = :question_ids";
$params = array();
$params[] = array(':question_ids', $id, 'int');
$db->query($query, $params);
$num_questions = $db->numrows();
while ($message_id = $db->result('id'))
{
	$template->assign_block_vars('questions', array()); // just need to create the block
	$query = "SELECT sentfrom, message FROM " . $DBPrefix . "messages WHERE question = :id AND reply_of = :message_id OR id = :message_id ORDER BY sentat ASC";
	$params = array();
	$params[] = array(':id', $id, 'int');
	$params[] = array(':message_id', $message_id, 'int');
	$db->query($query, $params);

	while ($rows = $db->result())	
	{
		$template->assign_block_vars('questions.conv', array(
			'MESSAGE' => $rows['message'],
			'BY_WHO' => ($user_id == $rows['sentfrom']) ? $MSG['125'] : $MSG['555']
		));
	}
}

$high_bid = ($num_bids == 0) ? $minimum_bid : $high_bid;
if ($customincrement == 0)
{
	// Get bid increment for current bid and calculate minimum bid
	$query = "SELECT increment FROM " . $DBPrefix . "increments WHERE
			((low <= :val AND high >= :val) OR
			(low < :val AND high < :val)) ORDER BY increment DESC";
	$params = array();
	$params[] = array(':val ', $high_bid, 'float');
	$db->query($query, $params);
	if ($db->numrows() != 0)
	{
		$increment = $db->result('increment');
	}
}
else
{
	$increment = $customincrement;
}

if ($auction_type == 2)
{
	$increment = 0;
}

if ($customincrement > 0)
{
	$increment = $customincrement;
}

if ($num_bids == 0 || $auction_type == 2)
{
	$next_bidp = $minimum_bid;
}
else
{
	$incr = isset($increment) ? + $increment : '';
	$next_bidp = $high_bid . $incr;
}

$view_history = '';
if ($num_bids > 0)
{
	$view_history = '';
}
else
{
	$view_history = 1;
}
$min_bid = $system->print_money($minimum_bid);
$high_bid = $system->print_money($high_bid);
if ($difference > 0)
{
	$next_bid = $system->print_money($next_bidp);
}
else
{
	$next_bid = '--';
}

// get seller feebacks
$query = "SELECT rate FROM " . $DBPrefix . "feedbacks WHERE rated_user_id = :user_ids";
$params = array();
$params[] = array(':user_ids', $user_id, 'int');	
$db->query($query, $params);
$num_feedbacks = $db->numrows('rate');
// count numbers
$fb_pos = $fb_neg = 0;
while ($fb_arr = $db->result())
{
	if ($fb_arr['rate'] == 1)
	{
		$fb_pos++;
	}
	elseif ($fb_arr['rate'] == - 1)
	{
		$fb_neg++;
	}
}

$total_rate = $fb_pos - $fb_neg;

if ($total_rate > 0)
{
	$i = 0;
	foreach ($memtypesarr as $k => $l)
	{
		if ($k >= $total_rate || $i++ == (count($memtypesarr) - 1))
		{
			$seller_rate_icon = $l['icon'];
			break;
		}
	}
}

// Pictures Gellery
$K = 0;
$UPLOADED_PICTURES = array();
if (file_exists($uploaded_path . $id))
{
	$dir = @opendir($uploaded_path . $id);
	if ($dir)
	{
		while ($file = @readdir($dir))
		{
			if ($file != '.' && $file != '..' && strpos($file, 'thumb-') === false)
			{
				$UPLOADED_PICTURES[$K] = $file;
				$K++;
			}
		}
		@closedir($dir);
	}
	$GALLERY_DIR = $id;

	if (is_array($UPLOADED_PICTURES))
	{
		foreach ($UPLOADED_PICTURES as $k => $v)
		{
			$TMP = @getimagesize($uploaded_path . $id . '/' . $v);
			if ($TMP[2] >= 1 && $TMP[2] <= 3)
			{
				$template->assign_block_vars('gallery', array(
						'THUMB_V' => $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $security->encrypt($id . '/' . $v),
						'V' => $system->SETTINGS['siteurl'] . 'getthumb.php?fromfile=' . $security->encrypt($id . '/' . $v)
						));
			}
		}
	}
}

// payment methods
$payment = explode(', ', $auction_data['payment']);
$payment_methods = '';
$query = "SELECT * FROM " . $DBPrefix . "gateways";	
$db->direct_query($query);
$gateways_data = $db->result();
$gateway_list = explode(',', $gateways_data['gateways']);
$p_first = true;
foreach ($gateway_list as $v)
{
	$v = strtolower($v);
	if ($gateways_data[$v . '_active'] == 1 && _in_array($v, $payment))
	{
		if (!$p_first)
		{
			$payment_methods .= ', ';
		}
		else
		{
			$p_first = false;
		}
		$payment_methods .= $system->SETTINGS['gatways'][$v];
	}
}

$payment_options = unserialize($system->SETTINGS['payment_options']);
foreach ($payment_options as $k => $v)
{
	if (_in_array($k, $payment))
	{
		if (!$p_first)
		{
			$payment_methods .= ', ';
		}
		else
		{
			$p_first = false;
		}
		$payment_methods .= $v;
	}
}

if (!$has_ended)
{
	if ($user->logged_in)
	{
		$bn_link = ' <a href="' . $system->SETTINGS['siteurl'] . 'buy_now.php?id=' . $id . '"><img border="0" align="absbottom" alt="' . $MSG['496'] . '" src="' . get_lang_img('buy_it_now.gif') . '"></a>';
		$bn_link2 = '&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="' . $system->SETTINGS['siteurl'] . 'buy_now.php?id=' . $id . '">' . $MSG['350_1015402'] . '</a><hr />';
	}
}


$query = "SELECT item_condition, condition_desc FROM " . $DBPrefix . "conditions ";
$db->direct_query($query);
while ($row = $db->result())
{
	if ($row['item_condition'] == $item_condition) 
	{
		$condition_desc =   ' ' . $row['condition_desc'] ;
	}
}
//See if seller is online
$loggedtime = $system->ctime - 320; // 5 min
$query = "SELECT is_online, hide_online FROM " . $DBPrefix . "users WHERE id = :user_id"; 
$params = array();
$params[] = array(':user_id', $user_id, 'int');	
$db->query($query, $params);
 
while ($onlinecheck = $db->result()) 
{ 

    if($onlinecheck['is_online'] > $loggedtime && $onlinecheck['hide_online'] == 'n') 
    { 
    	$online = true;
    } 
    else 
    { 
    	$online = false; 
    }     
} 

function is_fave($seller)
{
	global $DBPrefix, $user, $db;
	
	$yes = '0';
	$sql = "SELECT id, user_id, seller_id FROM " . $DBPrefix . "favesellers WHERE user_id = :user_id AND seller_id = :seller_id";
	$params = array();
	$params[] = array(':user_id', $user->user_data['id'], 'int');	
	$params[] = array(':seller_id', $seller, 'int');
	$db->query($sql, $params);
	$checkss = $db->result();
	if(count($checkss['id']) > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function add_fave($seller)
{
	global $user, $DBPrefix, $MSG, $db;

	$query = "INSERT INTO `" . $DBPrefix . "favesellers` (`user_id`, `seller_id`) VALUES (:user_id, :seller_id);";
	$params = array();
	$params[] = array(':user_id', $user->user_data['id'], 'int');	
	$params[] = array(':seller_id', $seller, 'int');
	$db->query($query, $params);
	$checknewfave = $db->lastInsertId();
	if($checknewfave)
	{
		$fsm = $MSG['FSM4'];
	}
	else
	{
		$fsm = $MSG['FSM8'];
	}
}

$shipping = '';
if ($auction_data['shipping'] == 1) $shipping = $MSG['031'];
elseif ($auction_data['shipping'] == 2) $shipping = $MSG['032'];
elseif ($auction_data['shipping'] == 3) $shipping = $MSG['867'];

if ($auction_data['sell_type'] == 'free') $B_freeItem = true;
elseif ($auction_data['sell_type'] == 'sell') $B_freeItem = false;

$template->assign_vars(array(
		'ID' => $auction_data['id'],
		'TITLE' => $auction_data['title'],
		'SUBTITLE' => $auction_data['subtitle'],
		'SEO_TITLE' => generate_seo_link($auction_data['title']),
		'AUCTION_DESCRIPTION' => $auction_data['description'],
		'PIC_URL' => (!empty($auction_data['pict_url'])) ? 'getthumb.php?fromfile=' . $security->encrypt($auction_data['id'] . '/' . $auction_data['pict_url']) : 'images/email_alerts/default_item_img.jpg',
		'SHIPPING_COST' => $system->print_money($auction_data['shipping_cost']),
		'ADDITIONAL_SHIPPING_COST' => $system->print_money($auction_data['shipping_cost_additional']),
		'COUNTRY' => $auction_data['country'],
		'CITY' => $city,
		'ZIP' => $auction_data['zip'],
		'QTY' => $auction_data['quantity'],
		'ENDS' => $ending_time,
		'ENDS_IN' => ($ends - $system->ctime),
		'STARTTIME' => ArrangeDateNoCorrection($start + $system->tdiff),
		'ENDTIME' => ArrangeDateNoCorrection($ends + $system->tdiff),
		'BUYNOW' => $system->print_money($auction_data['buy_now']),
		'NUMBIDS' => $num_bids,
		'MINBID' => $min_bid,
		'MAXBID' => $high_bid,
		'NEXTBID' => $next_bid,
		'INTERNATIONAL' => ($auction_data['international'] == 1) ? $MSG['033'] : $MSG['043'],
		'SHIPPING' => $shipping,
		'SHIPPINGTERMS' => nl2br($auction_data['shipping_terms']),
		'PAYMENTS' => $payment_methods,
		'AUCTION_VIEWS' => $auction_data['counter'],
		'AUCTION_TYPE' => ($auction_data['bn_only'] == 'n') ? $system->SETTINGS['auction_types'][$auction_type] : $MSG['933'],
		'ATYPE' => $auction_type,
		'THUMBWIDTH' => $system->SETTINGS['thumb_show'],
		'TOPCATSPATH' => ($system->SETTINGS['extra_cat'] == 'y' && isset($_SESSION['browse_id']) && $_SESSION['browse_id'] == $auction_data['secondcat']) ? $secondcat_value : $cat_value,
		'CATSPATH' => $cat_value,
		'SECCATSPATH' => $secondcat_value,
		'CAT_ID' => $auction_data['category'],
		'UPLOADEDPATH' => $uploaded_path,
		'BNIMG' => get_lang_img('buy_it_now.gif'),
		'IS_ONLINE' => $online,
		'RETURNS' => ($auction_data['returns'] == 1) ? $MSG['025_B'] : $MSG['025_D'],

		'SELLER_REG' => $seller_reg,
		'SELLER_ID' => $auction_data['user'],
		'SELLER_NICK' => $auction_data['nick'],
		'SELLER_TOTALFB' => $total_rate,
		'SELLER_FBICON' => (!empty($seller_rate_icon) && $seller_rate_icon != 'transparent.gif') ? '<img src="' . $system->SETTINGS['siteurl'] . 'images/icons/' . $seller_rate_icon . '" alt="' . $seller_rate_icon . '" class="fbstar">' : '',
		'SELLER_NUMFB' => $num_feedbacks,
		'SELLER_FBPOS' => ($num_feedbacks > 0) ? '(' . ceil($fb_pos * 100 / $num_feedbacks) . '%)' : $MSG['000'],
		'SELLER_FBNEG' => ($fb_neg > 0) ? $MSG['5507'] . ' (' . ceil($fb_neg * 100 / $total_rate) . '%)' : '0',
		
		'WATCH_VAR' => $watch_var,
		'WATCH_STRING' => $watch_string,
		'FSM' => isset($fsm) ? $fsm : '',
		'B_SETFSM' => (isset($faveset)),

		'YOURBIDMSG' => (isset($yourbidmsg)) ? $yourbidmsg : '',
		'YOURBIDCLASS' => (isset($yourbidclass)) ? $yourbidclass : '',
		'BIDURL' => $sslurl,
		
		'B_HASENDED' => $has_ended,
		'B_CANEDIT' => ($user->logged_in && $user->user_data['id'] == $auction_data['user'] && $num_bids == 0 && $difference > 0),
		'B_CANCONTACTSELLER' => (($system->SETTINGS['contactseller'] == 'always' || ($system->SETTINGS['contactseller'] == 'logged' && $user->logged_in)) && (!$user->logged_in || $user->user_data['id'] != $auction_data['user'])),
		'B_HASIMAGE' => (!empty($auction_data['pict_url'])),
		'B_NOTBNONLY' => ($auction_data['bn_only'] == 'n'),
		'B_HASRESERVE' => ($auction_data['reserve_price'] > 0 && $auction_data['reserve_price'] > $auction_data['current_bid']),
		'B_BNENABLED' => ($system->SETTINGS['buy_now'] == 2),
		'B_HASGALELRY' => (count($UPLOADED_PICTURES) > 0),
		'B_SHOWHISTORY' => (isset($view_history) && $num_bids > 0),
		'B_FREE_ITEM' => $B_freeItem,
		'B_BUY_NOW' => ($auction_data['buy_now'] > 0 && ($auction_data['bn_only'] == 'y' || $auction_data['bn_only'] == 'n' && ($auction_data['num_bids'] == 0 || ($auction_data['reserve_price'] > 0 && $auction_data['current_bid'] < $auction_data['reserve_price'])))),
		'B_BUY_NOW_ONLY' => ($auction_data['bn_only'] == 'y'),
		'B_ADDITIONAL_SHIPPING_COST' => ($auction_data['auction_type'] == '2'),
		'B_USERBID' => $userbid,
		'B_BIDDERPRIV' => ($system->SETTINGS['buyerprivacy'] == 'y' && (!$user->logged_in || ($user->logged_in && $user->user_data['id'] != $auction_data['user']))),
		'B_HASBUYER' => (count($hbidder_data) > 0),
		'B_COUNTDOWN' => ($system->SETTINGS['hours_countdown'] > ($ends - $system->ctime)),
		'B_HAS_QUESTIONS' => ($num_questions > 0),
		'B_CAN_BUY' => $user->can_buy && !($start > $system->ctime),
		'B_CANSEE' => ($user->logged_in && $user->user_data['id'] != $auction_data['user']),
		'B_SHOWENDTIME' => $showendtime,
		'B_CONDITION'=> $system->SETTINGS['item_conditions'] == 'y',
		'ITEM_CONDITION'=> $item_condition,
		'CONDITION_DESCRIPTION'=> $condition_desc,
		'ITEM_MANUFACTURER'=> $item_manufacturer,
    	'ITEM_MODEL'=>   $item_model,
		'ITEM_COLOUR'=> $item_colour,
		'ITEM_YEAR' =>  $item_year,
		'FBLOGIN' => $_SESSION['REDIRECT_AFTER_LOGIN'] . '-fbconnect',
		
		'B_SHIPPING_TERMS' => $system->SETTINGS['shipping_terms'],
		'B_SHIPPING_CONDITIONS' => $system->SETTINGS['shipping_conditions'],
		'B_FB_LINK' => 'ItemFBLogin',
		'B_ITEM_CONDITION' => (!empty($item_condition)),
		'B_ITEM_MANUFACTURER' => (!empty($item_manufacturer)),
		'B_ITEM_MODEL' => (!empty($item_model)),
		'B_ITEM_COLOUR' => (!empty($item_colour)),
		'B_ITEM_YEAR' => (!empty($item_year))
		));


$query = "SELECT avatar FROM " . $DBPrefix . "users WHERE id = :id";
$params = array();
$params[] = array(':id', $auction_data['user'], 'int');	
$db->query($query, $params);
$TPL_avatar = $db->result('avatar');
$template->assign_vars(array(
'AVATAR' => $TPL_avatar,
));  

include $include_path . 'functions_bid.php';
include 'header.php';
$template->set_filenames(array(
		'body' => 'item.tpl'
		));
$template->display('body');
include 'footer.php';
unset($_SESSION['browse_id']);
