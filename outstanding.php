<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'outstanding.php';
	header('location: user_login.php');
	exit;
}

if (!isset($_GET['PAGE']) || $_GET['PAGE'] == 1)
{
	$OFFSET = 0;
	$PAGE = 1;
}
else
{
	$PAGE = intval($_GET['PAGE']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}

$query = "SELECT id FROM " . $DBPrefix . "winners WHERE paid = 0 AND winner = :id";
$params = array();
$params[] = array(':id', $user->user_data['id'], 'int');
$db->query($query, $params);
$TOTALAUCTIONS = $db->numrows('id');
$PAGES = ($TOTALAUCTIONS == 0) ? 1 : ceil($TOTALAUCTIONS / $system->SETTINGS['perpage']);

$query = "SELECT w.id, w.winner, w.auction, a.title, a.shipping_cost, w.bid, w.qty, d.item, d.auctions, d.seller, a.shipping, a.shipping_cost_additional 
	FROM " . $DBPrefix . "winners w
	LEFT JOIN " . $DBPrefix . "auctions a ON (a.id = w.auction)
	LEFT JOIN " . $DBPrefix . "digital_items d ON (d.auctions = w.auction)
	WHERE w.paid = 0 AND w.winner = :user_id
	LIMIT " . intval($OFFSET) . "," . $system->SETTINGS['perpage'];
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$db->query($query, $params);

while ($row = $db->result())
{
	$template->assign_block_vars('to_pay', array(
		'TITLE' => $row['title'],
		'SEO_TITLE' => generate_seo_link($row['title']),
		'SHIPPING' => ($row['shipping'] == 1) ? $system->print_money($row['shipping_cost']) : $system->print_money(0),
		'ADDITIONAL_SHIPPING_COST' => $system->print_money($row['shipping_cost_additional'] * ($row['qty'] - 1)),
		'ADDITIONAL_SHIPPING' => $system->print_money($row['shipping_cost_additional']),
		'ADDITIONAL_SHIPPING_QUANTITYS' => $row['qty'] - 1,
		'QUANTITY' => $row['qty'],
		'BID' => $system->print_money($row['bid'] * $row['qty']),
		'TOTAL' => $system->print_money($row['shipping_cost'] + ($row['bid'] * $row['qty']) + ($row['shipping_cost_additional'] * ($row['qty'] - 1))),
		'ID' => $row['id'],
		'AUC_ID' => $row['auction'],
		'WINID'=> $row['winner'],
		'DIGITAL_ITEM_TOTAL' => $system->print_money($row['bid']),
		'DIGITAL_ITEM_SHIPPING' => $system->print_money('0.00'),
		'DIGITAL_ITEM_QUANTITY' => '1',
		'DIGITAL_ITEM_BID' => $system->print_money($row['bid']),
		'PAY_LINK' => (empty($row['item'])) ? 'pay.php?a=2' : 'pay.php?a=10',
						
		'B_DIGITAL_ITEM' => (empty($row['item'])),
		'B_NOTITLE' => (empty($row['title']))
	));
}

// get pagenation
$PREV = intval($PAGE - 1);
$NEXT = intval($PAGE + 1);
if ($PAGES > 1)
{
	$LOW = $PAGE - 5;
	if ($LOW <= 0) $LOW = 1;
	$COUNTER = $LOW;
	while ($COUNTER <= $PAGES && $COUNTER < ($PAGE + 6))
	{
		$template->assign_block_vars('pages', array(
				'PAGE' => ($PAGE == $COUNTER) ? '<li class="active"><a href="#">' . $COUNTER . '</a></li>' : '<li><a href="' . $system->SETTINGS['siteurl'] . 'outstanding.php?PAGE=' . $COUNTER . '">' . $COUNTER . '</a></li>'
				));
		$COUNTER++;
	}
}

$query = "SELECT balance FROM " . $DBPrefix . "users WHERE id = :user_balance";
$params = array();
$params[] = array(':user_balance', $user->user_data['id'], 'int');
$db->query($query, $params);
$user_balance = $db->result('balance');

$_SESSION['INVOICE_RETURN'] = 'outstanding.php';
$template->assign_vars(array(
	'USER_BALANCE' => $system->print_money($user_balance),
	'PAY_BALANCE' => $system->print_money_nosymbol(($user_balance < 0) ? 0 - $user_balance : 0),
	'CURRENCY' => $system->SETTINGS['currency'],

	'PREV' => ($PAGES > 1 && $PAGE > 1) ? '<li><a href="' . $system->SETTINGS['siteurl'] . 'outstanding.php?PAGE=' . $PREV . '"><u>' . $MSG['5119'] . '</u></li>' : '',
	'NEXT' => ($PAGE < $PAGES) ? '<li><a href="' . $system->SETTINGS['siteurl'] . 'outstanding.php?PAGE=' . $NEXT . '"><u>' . $MSG['5120'] . '</u></li>' : '',
	'PAGE' => $PAGE,
	'PAGES' => $PAGES
));

include 'header.php';
$TMP_usmenutitle = $MSG['422'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'outstanding.tpl'
		));
$template->display('body');
include 'footer.php';
