<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
include 'common.php';

$cat = (isset($_GET['cat'])) ? intval($_GET['cat']) : intval($_POST['cat']);
if ($cat > 0)
{
	// Retrieve category's name
	$query = "SELECT category FROM " . $DBPrefix . "faqscategories WHERE id = :cats";
	$params = array();
	$params[] = array(':cats', $cat, 'int');
	$db->query($query, $params);

	$FAQ_ctitle = $db->result('category');
	$template->assign_vars(array(
			'DOCDIR' => $DOCDIR, // Set document direction (set in includes/messages.XX.inc.php) ltr/rtl
			'PAGE_TITLE' => $system->SETTINGS['sitename'] . ' ' . $MSG['5236'] . ' - ' . $FAQ_ctitle,
			'CHARSET' => $CHARSET,
			'LOGO' => ($system->SETTINGS['logo']) ? '<a href="' . $system->SETTINGS['siteurl'] . 'home"><img src="' . $incurl . $uploaded_path . 'logos/' . $system->SETTINGS['logo'] . '" border="0" alt="' . $system->SETTINGS['sitename'] . '"></a>' : '&nbsp;',
			'SITEURL' => $system->SETTINGS['siteurl'],
			'THEME' => $system->SETTINGS['theme'],

			'FNAME' => $FAQ_ctitle
			));
	// Retrieve FAQs categories from the database
	$query = "SELECT * FROM " . $DBPrefix . "faqscategories ORDER BY category ASC";
	$db->direct_query($query);
	while ($cats = $db->result())
	{
		$template->assign_block_vars('cats', array(
				'CAT' => stripslashes($cats['category']),
				'ID' => $cats['id']
				));
	}
	// Retrieve FAQs from the database
	$query = "SELECT f.question As q, f.answer As a, t.* FROM " . $DBPrefix . "faqs f
			LEFT JOIN " . $DBPrefix . "faqs_translated t ON (t.id = f.id)
			WHERE f.category = :cat AND t.lang = :languages";
	$params = array();
	$params[] = array(':cat', $cat, 'int');
	$params[] = array(':languages', $language, 'int');
	$db->query($query, $params);

	while ($row = $db->result())
	{
		if (!empty($row['question']) && !empty($row['answer']))
		{
			$question = stripslashes($row['question']);
			$answer = stripslashes($row['answer']);
		}
		else
		{
			$question = stripslashes($row['q']);
			$answer = stripslashes($row['a']);
		}

		$template->assign_block_vars('faqs', array(
				'Q' => $question,
				'A' => $answer,
				'ID' => $row['id']
				));
	}

	$template->set_filenames(array(
			'body' => 'viewhelp.tpl'
			));
	$template->display('body');
}
else
{
	header('location: help.php');
}
