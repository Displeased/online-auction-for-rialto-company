<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $include_path . 'functions_sell.php';

if (!$user->is_logged_in())
{
	//if your not logged in you shouldn't be here
	header("location: user_login.php");
	exit;
}

//uploading a digital item
if(isset($_GET['diupload']) && $_GET['diupload'] == 1 && isset($_FILES['file_up']))
{
	include $include_path . 'functions_digital_item.php';
	echo upload_digital_item();
	exit;
}

$query = "SELECT file_extension FROM " . $DBPrefix . "digital_item_mime WHERE use_mime = 'y'";
$db->direct_query($query);
$allowtype = '';
while ($row = $db->result())
{
	$allowtype .= $row['file_extension'] . ', ';
}

$max_size = formatSizeUnits($system->SETTINGS['dites_upload_size']); 

if (is_dir($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id))
{
	// Check to see if a upload file is stored in the SQL
	$query = "SELECT item FROM " . $DBPrefix . "digital_items WHERE auctions = :auct_id AND seller = :seller_id"; 
	$params = array();
	$params[] = array(':auct_id', $_SESSION['SELL_auction_id'], 'int');
	$params[] = array(':seller_id', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']), 'int');
	$db->query($query, $params);
	if($db->numrows() > 0)
	{
		$di_items = true;
		$diitem = $db->result();
	}
}
$template->assign_vars(array(
	'STORED' => (isset($di_items)) ? $diitem['item'] :'',
	'SITENAME' => $system->SETTINGS['sitename'],
	'FILE_UPLOADED' => (isset($_SESSION['SELL_upload_file'])) ? $_SESSION['SELL_upload_file'] : '',
	'SIZE' => sprintf($MSG['350_10176'], $max_size),
	'SITEURL' => $system->SETTINGS['siteurl'],
	'TYPES' => sprintf($MSG['350_10175'], $allowtype),
	'THEME' => $system->SETTINGS['theme'],
));
		
$template->set_filenames(array(
		'body' => 'digital_item.tpl'
		));
$template->display('body');