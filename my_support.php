<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $main_path . 'inc/ckeditor/ckeditor.php';
// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'support';
	header('location: user_login.php');
	exit;
}

$ERR = isset($_SESSION['err_message']) ? $_SESSION['err_message'] : '';
$subject = isset($_POST['subject']) ? stripslashes($system->cleanvars($_POST['subject'])) : '';
$nowmessage = isset($_POST['message']) ? stripslashes($system->cleanvars($_POST['message'])) : '';
$link = $system->SETTINGS['siteurl'] . 'support';

if (isset($_POST['subject']) && isset($_POST['message']))
{
	//making sessions in case the ticket does not submit
	$_SESSION['message'] = $_POST['message'];
	$_SESSION['subject'] = $_POST['subject'];
	
	//making the ticket id
	$hash = md5(rand(1, 9999));
	
	// submit the ticket to DB
	$query = "INSERT INTO " . $DBPrefix . "support VALUES (NULL, :user, :title, :ticket_id, :status, :last_reply_user, :last_reply_time, :created_time, :ticket_reply_status)";
	$params = array();
	$params[] = array(':user', $user->user_data['id'], 'int');
	$params[] = array(':title', $subject, 'str');
	$params[] = array(':ticket_id', $hash, 'str');
	$params[] = array(':status', 'open', 'bool');
	$params[] = array(':last_reply_user', $user->user_data['id'], 'int');
	$params[] = array(':last_reply_time', $system->ctime, 'int');
	$params[] = array(':created_time', $system->ctime, 'int');
	$params[] = array(':ticket_reply_status', 'support', 'bool');
	$db->query($query, $params);
	if($db->lastInsertId() > 0)
	{
		// submit the message to DB and linking the ticket id
		$query = "INSERT INTO " . $DBPrefix . "support_messages VALUES (NULL, 0, :sender_id, :from_email, :times, :nowmessages, :subjects, :replayof)";
		$params = array();
		$params[] = array(':sender_id', $user->user_data['id'], 'int');
		$params[] = array(':from_email', $user->user_data['email'], 'str');
		$params[] = array(':times', $system->ctime, 'str');
		$params[] = array(':nowmessages', $nowmessage, 'str');
		$params[] = array(':subjects', $subject, 'str');
		$params[] = array(':replayof', $hash, 'str');
		$db->query($query, $params);
		if($db->lastInsertId() > 0)
		{
			// send the email
			$send_email->submit_new_ticket($subject, $system->uncleanvars($nowmessage), $user->user_data['nick'], $user->user_data['email']);

			//deteling the sessions
			$_SESSION['message'] = '';
			$_SESSION['subject'] = '';
			$ERR = $MSG['3500_1015439l'];
		}
	}
}

if (isset($_POST['deleteid']) && is_array($_POST['deleteid']))
{
	foreach ($_POST['deleteid'] as $k => $v)
	{
		$query = "DELETE FROM " . $DBPrefix . "support_messages WHERE reply_of = :replayof";
		$params = array();
		$params[] = array(':replayof', $v, 'int');
		$db->query($query, $params);
		
		$query = "DELETE FROM " . $DBPrefix . "support WHERE ticket_id = :replayof";
		$params = array();
		$params[] = array(':replayof', $v, 'int');
		$db->query($query, $params);
	}
	$ERR = $MSG['444'];
}

if (isset($_POST['closeid']) && is_array($_POST['closeid']))
{
	foreach ($_POST['closeid'] as $k => $v)
	{
		$query = "UPDATE " . $DBPrefix . "support SET last_reply_time = :update_time, ticket_reply_status = :set_status, status = :set_close, last_reply_user = :set_user WHERE ticket_id = :id AND user = :user";
		$params = array();
		$params[] = array(':update_time', $system->ctime, 'int');
		$params[] = array(':set_status', 'user', 'bool');
		$params[] = array(':set_close', 'close', 'bool');
		$params[] = array(':set_user', $user->user_data['id'], 'int');
		$params[] = array(':user', $user->user_data['id'], 'int');
		$params[] = array(':id', $v, 'int');
		$db->query($query, $params);
	}
	$ERR = $MSG['3500_1015439k'];
}

$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;

$query = "SELECT t.*, u.nick FROM " . $DBPrefix . "support t
	LEFT JOIN " . $DBPrefix . "users u ON (u.id = t.user)
	WHERE t.user = :to_ids ORDER BY last_reply_time DESC";
// get users messages
$params = array();
$params[] = array(':to_ids', $user->user_data['id'], 'int');
$db->query($query, $params);
$messages = $db->numrows();
while ($array = $db->result())
{
	// formatting the created time
	$created_time = $array['created_time'];
	$mth = 'MON_0' . gmdate('m', $created_time);
	if($system->SETTINGS['datesformat'] == 'EUR')
	{
		$created =  gmdate('j', $created_time) . ' ' . $MSG[$mth] . ' ' . gmdate('Y', $created_time) . ' ' . gmdate('H:i:s', $created_time);
	}
	else
	{
		$created = $MSG[$mth] . ' ' . gmdate('j,Y', $created_time) . ' ' . gmdate('H:i:s', $created_time);;
	}
	
	$last_reply_time = $array['last_reply_time'];
	$mth = 'MON_0' . gmdate('m', $last_reply_time);
	if($system->SETTINGS['datesformat'] == 'EUR')
	{
		$last_reply =  gmdate('j', $last_reply_time) . ' ' . $MSG[$mth] . ' ' . gmdate('Y', $last_reply_time) . ' ' . gmdate('H:i:s', $last_reply_time);
	}
	else
	{
		$last_reply = $MSG[$mth] . ' ' . gmdate('j,Y', $last_reply_time) . ' ' . gmdate('H:i:s', $last_reply_time);
	}

	$template->assign_block_vars('ticket', array(
		'LAST_UPDATED_TIME' => $last_reply, //when the ticket was updated
		'TICKET_ID' => $array['ticket_id'],
		'LAST_UPDATE_USER' => $array['ticket_reply_status'] == 'user' ? $array['nick'] : $MSG['3500_1015436'],
		'TICKET_TITLE' => ($array['ticket_reply_status'] == 'user' && $array['status'] == 'open') ? '<b>' . $array['title'] . '</b>' : $array['title'],
		'CREATED' => $created, //time that the ticket was created
		'TICKET_STATUS' => $array['status'] == 'open' ? true : false, //ticket is open or closed
	));
}

$check_mess = (isset($_SESSION['message'])) ? $system->uncleanvars($_SESSION['message']) : '';
$template->assign_vars(array(
	'ERROR' => isset($ERR) ? $ERR : '',
	'B_ISERROR' => isset($ERR) ? true : false,
	'MSGCOUNT' => $messages,
	'SUBJECT' => (isset($_SESSION['subject'])) ? $_SESSION['subject'] : '',
	'MESSAGE' => $CKEditor->editor('message', $check_mess)
));

unset($_SESSION['err_message']);
include 'header.php';
$TMP_usmenutitle = $MSG['3500_1015432'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'my_support.tpl'
		));
$template->display('body');
include 'footer.php';