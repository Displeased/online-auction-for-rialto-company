<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
 * your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
 
include 'common.php';

if (!$user->is_logged_in())
{
	//if your not logged in you shouldn't be here
	header("location: user_login.php");
	exit;
}

$query = "SELECT value FROM " . $DBPrefix . "fees WHERE type = :fee";
$params = array();
$params[] = array(':fee', 'picture_fee', 'str');
$db->query($query, $params);
$pictureFees = $db->result('value');

$template->assign_vars(array(
		'SITENAME' => $system->SETTINGS['sitename'],
		'SITEURL' => $system->SETTINGS['siteurl'],
		'THEME' => $system->SETTINGS['theme'],
		'PICURL' => (isset($_SESSION['SELL_pict_url'])) ? $_SESSION['SELL_pict_url'] : 'Default',
		'B_PICURL' => (isset($_SESSION['SELL_pict_url'])),
		'MAXPICS' => sprintf($MSG['673'], $system->SETTINGS['maxpictures'], formatSizeUnits($system->SETTINGS['maxuploadsize'])),
		'FREEMAXPIC' => $pictureFees > 0 ? sprintf($MSG['3500_1015761'], $system->SETTINGS['freemaxpictures'], $system->SETTINGS['freemaxpictures'], $system->print_money($pictureFees)) : ''
		));
$template->set_filenames(array(
		'body' => 'upldgallery.tpl'
		));
$template->display('body');