<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

if ($system->SETTINGS['fees'] == 'n')
{
	header('location: home');
	exit;
}

// get fees
$query = "SELECT * FROM " . $DBPrefix . "fees";
$db->direct_query($query);
$setup = $buyer_fee = $endauc_fee = false;
$i = 0;
while ($row = $db->result())
{
	if ($row['type'] == 'setup')
	{
		if ($row['fee_from'] != $row['fee_to'])
		{
			$setup = true;
			$template->assign_block_vars('setup_fees', array(
					'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
					'FROM' => $system->print_money($row['fee_from']),
					'TO' => $system->print_money($row['fee_to']),
					'VALUE' => ($row['fee_type'] == 'flat') ? $system->print_money($row['value']) : $row['value'] . '%'
					));
		}
	}
	elseif ($row['type'] == 'buyer_fee')
	{
		if ($row['fee_from'] != $row['fee_to'])
		{
			$buyer_fee = true;
			$template->assign_block_vars('buyer_fee', array(
					'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
					'FROM' => $system->print_money($row['fee_from']),
					'TO' => $system->print_money($row['fee_to']),
					'VALUE' => ($row['fee_type'] == 'flat') ? $system->print_money($row['value']) : $row['value'] . '%'
					));
		}
	}
	elseif ($row['type'] == 'endauc_fee')
	{
		if ($row['fee_from'] != $row['fee_to'])
		{
			$endauc_fee = true;
			$template->assign_block_vars('endauc_fee', array(
					'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
					'FROM' => $system->print_money($row['fee_from']),
					'TO' => $system->print_money($row['fee_to']),
					'VALUE' => ($row['fee_type'] == 'flat') ? $system->print_money($row['value']) : $row['value'] . '%'
					));
		}
	}
	elseif ($row['type'] == 'signup_fee')
	{
		$template->assign_vars(array(
				'B_SIGNUP_FEE' => ($row['value'] > 0),
				'SIGNUP_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'hpfeat_fee')
	{
		$template->assign_vars(array(
				'B_HPFEAT_FEE' => ($row['value'] > 0),
				'HPFEAT_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'bolditem_fee')
	{
		$template->assign_vars(array(
				'B_BOLD_FEE' => ($row['value'] > 0),
				'BOLD_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'hlitem_fee')
	{
		$template->assign_vars(array(
				'B_HL_FEE' => ($row['value'] > 0),
				'HL_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'rp_fee')
	{
		$template->assign_vars(array(
				'B_RP_FEE' => ($row['value'] > 0),
				'RP_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'picture_fee')
	{
		$template->assign_vars(array(
				'B_PICTURE_FEE' => ($row['value'] > 0),
				'PICTURE_FEE' => $system->print_money($row['value']),
				'FREE_PICTURES' => $system->SETTINGS['freemaxpictures'] > 0 ? sprintf($MSG['3500_1015769'], $system->SETTINGS['freemaxpictures']) : ''
				));
	}
	elseif ($row['type'] == 'relist_fee')
	{
		$template->assign_vars(array(
				'B_RELIST_FEE' => ($row['value'] > 0),
				'RELIST_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'buyout_fee')
	{
		$template->assign_vars(array(
				'B_BUYNOW_FEE' => ($row['value'] > 0),
				'BUYNOW_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'excat_fee')
	{
		$template->assign_vars(array(
				'B_EXCAT_FEE' => ($row['value'] > 0),
				'EXCAT_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'subtitle_fee')
	{
		$template->assign_vars(array(
				'B_SUBTITLE_FEE' => ($row['value'] > 0),
				'SUBTITLE_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'banner_fee')
	{
		$template->assign_vars(array(
				'B_BANNER_FEE' => ($row['value'] > 0),
				'BANNER_FEE' => $system->print_money($row['value'])
				));
	}
	elseif ($row['type'] == 'ex_banner_fee')
	{
		$template->assign_vars(array(
				'B_EX_BANNER_FEE' => ($row['value'] > 0),
				'EX_BANNER_FEE' => $system->print_money($row['value'])
				));
	}
	$i++;
}
$query = "SELECT fee_signup_bonus FROM " . $DBPrefix . "settings";
$db->direct_query($query);
while ($bonus = $db->result('fee_signup_bonus'))
{
$template->assign_vars(array(
'BONUS' => $system->print_money($bonus),
'B_BONUS_FEE' => ($bonus > 0),
));
}

$template->assign_vars(array(
		'B_SETUP_FEE' => $setup,
		'B_FB_LINK' => 'IndexFBLogin',
		'B_BUYER_FEE' => $buyer_fee,
		'B_ENDAUC_FEE' => $endauc_fee
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'fees.tpl'
		));
$template->display('body');
include 'footer.php';