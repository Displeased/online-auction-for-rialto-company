<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
include 'common.php';

if (isset($_POST['action']) && isset($_POST['username']) && isset($_POST['password']))
{
	$query = "SELECT * FROM " . $DBPrefix . "users WHERE (nick = :check_nick OR email = :check_email)";
	$params = array();
	$params[] = array(':check_nick', $system->cleanvars($_POST['username']), 'str');
	$params[] = array(':check_email', $system->cleanvars($_POST['username']), 'str');
	$db->query($query, $params);
	$user_data = $db->result();
	if ($phpass->CheckPassword($_POST['password'], $user_data['password']))
	{
		//get server time
		$NOW = $system->ctime;
		
		//generator user token
		$_SESSION['csrftoken'] = $security->encrypt($security->genRandString(32));
	
		if ($user_data['suspended'] == 9)
		{
			$_SESSION['signup_id'] = $user_data['id'];
			header('location: pay.php?a=3');
			exit;
		}
		elseif ($user_data['suspended'] == 1)
		{
			$ERR = $ERR_618;
		}
		elseif ($user_data['suspended'] == 8)
		{
			$ERR = $ERR_620;
		}
		elseif ($user_data['suspended'] == 10)
		{
			$ERR = $ERR_621;
		}
		else
		{
			$_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN'] 		= $security->encrypt($user_data['id']);
			$_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_NUMBER'] 	= $security->encrypt(strspn($user_data['password'], $user_data['hash']));
			$_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_PASS'] 		= $security->encrypt($user_data['password']);
			
			$overdue = false;	
			// Update "last login" fields in users table
			$query = "UPDATE " . $DBPrefix . "users SET lastlogin = :date WHERE id = :user_id";
			$params = array();
			$params[] = array(':date', gmdate("Y-m-d H:i:s"), 'str');
			$params[] = array(':user_id', $user_data['id'], 'int');
			$db->query($query, $params);
				
			// Remember me option
			if (isset($_POST['rememberme']))
			{
				$remember_key = md5($NOW);
				$query = "INSERT INTO " . $DBPrefix . "rememberme VALUES (:user_id, :remember_key)";
				$params = array();
				$params[] = array(':remember_key', $remember_key, 'str');
				$params[] = array(':user_id', $user_data['id'], 'int');
				$db->query($query, $params);
				setcookie($system->SETTINGS['cookie_name'] . '-RM_ID', $remember_key, $NOW + (3600 * 24 * 365));
			}
			
			if ($system->SETTINGS['fee_type'] == 1 && $system->SETTINGS['fee_disable_acc'] == 'y' && $user_data['payment_reminder_sent'] == 'n')
			{
				if ($system->SETTINGS['fee_max_debt'] <= (-1 * $user_data['balance']))
				{
					$query = "UPDATE " . $DBPrefix . "users SET suspended = :susp, payment_reminder_sent = :payment WHERE id = :user";
					$params = array();
					$params[] = array(':susp', 7, 'int');
					$params[] = array(':payment', 'y', 'bool');
					$params[] = array(':user', $user_data['id'], 'int');
					$db->query($query, $params);
					$overdue = true;
					// send email
					$send_email->payment_reminder($user_data['name'], $user_data['balance'], $user_data['id'], $user_data['email']);
				}
			}

			$query = "SELECT id FROM " . $DBPrefix . "usersips WHERE USER = :user_id AND ip = :user_ip";
			$params = array();
			$params[] = array(':user_ip', $_SERVER['REMOTE_ADDR'], 'str');
			$params[] = array(':user_id', $user_data['id'], 'int');
			$db->query($query, $params);
			if ($db->numrows() == 0)
			{
				$query = "INSERT INTO " . $DBPrefix . "usersips VALUES (NULL, :user_id, :user_ip, 'after','accept')";
				$params = array();
				$params[] = array(':user_ip', $_SERVER['REMOTE_ADDR'], 'str');
				$params[] = array(':user_id', $user_data['id'], 'int');
				$db->query($query, $params);
			}
				
			if (isset($_POST['hide_online']))
			{
				$query = "UPDATE " . $DBPrefix . "users SET hide_online = :hide WHERE id = :user_id";
				$params = array();
				$params[] = array(':user_id', $user_data['id'], 'int');
				$params[] = array(':hide', 'y', 'bool');
				$db->query($query, $params);
			}
			else
			{
				$query = "UPDATE " . $DBPrefix . "users SET hide_online = :hide WHERE id = :user_id";
				$params = array();
				$params[] = array(':user_id', $user_data['id'], 'int');
				$params[] = array(':hide', 'n', 'bool');
				$db->query($query, $params);
			}
	
			// delete your old session
			if (isset($_COOKIE[$system->SETTINGS['cookie_name'] . '-ONLINE']))
			{
				$query = "DELETE from " . $DBPrefix . "online WHERE SESSION = :SESSION";
				$params = array();
				$params[] = array(':SESSION', alphanumeric($_COOKIE[$system->SETTINGS['cookie_name'] . '-ONLINE']), 'str');
				$db->query($query, $params);				
			}
	
			if (in_array($user_data['suspended'], array(5, 6, 7)) || $overdue)
			{
				$_SESSION['msg_title'] = $MSG['753'];
				$_SESSION['msg_body'] = $ERR;
				$URL = $system->SETTINGS['siteurl'] . 'message.php';
			}
			elseif (isset($_SESSION['REDIRECT_AFTER_LOGIN']))
			{
				$URL = str_replace('\r', '', str_replace('\n', '', $_SESSION['REDIRECT_AFTER_LOGIN']));
				unset($_SESSION['REDIRECT_AFTER_LOGIN']);
			}
			else
			{
				$URL = $system->SETTINGS['siteurl'] . 'user_menu.php';
			}
			header('location: ' . $URL);
			exit;
		}
	}
	else
	{
		$ERR = $ERR_038;
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'B_FB_LINK' => 'FBUserLogin',
		'USER' => (isset($_POST['username'])) ? $_POST['username'] : ''
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'user_login.tpl'
		));
$template->display('body');
include 'footer.php';
