<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

if (isset($_GET['fail']) || isset($_GET['completed']))
{
	$template->assign_vars(array(
			'TITLE_MESSAGE' => (isset($_GET['fail'])) ? $MSG['425'] :  $MSG['423'],
			'BODY_MESSAGE' => (isset($_GET['fail'])) ? $MSG['426'] :  $MSG['424']
			));
	include 'header.php';
	$template->set_filenames(array(
			'body' => 'message.tpl'
			));
	$template->display('body');
	include 'footer.php';
	exit;
}

$fees = new fees;
$fees->data = $_POST;

if (isset($_GET['paypal']))
{
	$fees->paypal_validate();
}
if (isset($_GET['authnet']))
{
	$fees->authnet_validate();
}
if (isset($_GET['worldpay']))
{
	$fees->worldpay_validate();
}
if (isset($_GET['skrill']))
{
	$fees->skrill_validate();
}
if (isset($_GET['toocheckout']))
{
	$fees->toocheckout_validate();
}