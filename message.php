<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

define('ErrorPage', 1);
include 'common.php';

if (isset($_SESSION['msg_title']) && isset($_SESSION['msg_body']))
{
	$title = $_SESSION['msg_title'];
	$body = $_SESSION['msg_body'];
}
elseif ($user->logged_in && $user->user_data['suspended'] == 7)
{
	$title = $MSG['753'];
	$body = $MSG['754'];
}
elseif ($user->logged_in && $user->user_data['suspended'] == 6)
{
	$query = "SELECT a.title, a.id FROM " . $DBPrefix . "auctions a
			LEFT JOIN " . $DBPrefix . "winners w ON (w.auction = a.id)
			WHERE w.bf_paid = 0 AND w.winner = :user_id";
	$params = array();
	$params[] = array(':user_id', $user->user_data['id'], 'int');
	$db->query($query, $params);
	$auction_data = $db->result();
	$title = $MSG['753'];
	$url =  $system->SETTINGS['siteurl'] . 'pay.php?a=6&auction_id=' . $auction_data['id'];
	$body = sprintf($MSG['777'], $auction_data['title'], $url);
}
elseif ($user->logged_in && $user->user_data['suspended'] == 5)
{
	$query = "SELECT a.title, a.id FROM " . $DBPrefix . "auctions a
			LEFT JOIN " . $DBPrefix . "winners w ON (w.auction = a.id)
			WHERE w.ff_paid = 0 AND w.seller = :user_id";
	$params = array();
	$params[] = array(':user_id', $user->user_data['id'], 'int');
	$db->query($query, $params);
	$auction_data = $db->result();
	$title = $MSG['753'];
	$url = $system->SETTINGS['siteurl'] . 'pay.php?a=7&auction_id=' . $auction_data['id'];
	$body = sprintf($MSG['796'], $auction_data['title'], $url);
}
else
{
	header('location: home');
	exit;
}

$template->assign_vars(array(
		'TITLE_MESSAGE' => $title,
		'BODY_MESSAGE' => $body
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'message.tpl'
		));
$template->display('body');
include 'footer.php';