<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
if (!defined('InuAuctions')) exit();

function generate_id()
{
	global $_SESSION;
	if (!isset($_SESSION['SELL_auction_id']))
	{
		$auction_id = md5(uniqid(rand()));
		$_SESSION['SELL_auction_id'] = $auction_id;
	}
	else
	{
		$auction_id = $_SESSION['SELL_auction_id'];
	}
	return $auction_id;
}

function setvars()
{
	global $with_reserve, $reserve_price, $minimum_bid, $pict_url, $imgtype, $title, $subtitle, $description, $atype, $iquantity, $buy_now, $buy_now_price, $is_taxed, $tax_included, $additional_shipping_cost, $freeItime;
	global $duration, $relist, $increments, $customincrement, $shipping, $shipping_terms, $payment, $international, $sellcat1, $sellcat2, $buy_now_only, $a_starts, $shipping_cost, $is_bold, $is_highlighted, $is_featured, $digital_item, $item_condition, $item_manufacturer, $item_model, $item_colour, $item_year, $returns, $start_now;
	global $_POST, $_SESSION, $system;
	
	//checking to see what auction types are turned on and 
	//setting it to the auction setup page. This is only 
	//gets used 1 time after picking their Categories.
	if(!$_SESSION['SELL_atype'])
	{
		if($system->SETTINGS['standard_auctions'] == 'y')
		{
			$_SESSION['SELL_atype'] = 1;
		}
		elseif($system->SETTINGS['dutch_auctions'] == 'y')
		{
			$_SESSION['SELL_atype'] = 2;
		}
		elseif($system->SETTINGS['di_auctions'] == 'y')
		{
			$_SESSION['SELL_atype'] = 3;
			$_SESSION['SELL_iquantity'] = '999999';
		}
	}
		
	$with_reserve = (isset($_POST['with_reserve'])) ? $_POST['with_reserve'] : $_SESSION['SELL_with_reserve'];
	$reserve_price = (isset($_POST['reserve_price'])) ? $_POST['reserve_price'] : $_SESSION['SELL_reserve_price'];
	$minimum_bid = (isset($_POST['minimum_bid'])) ? $_POST['minimum_bid'] : $_SESSION['SELL_minimum_bid'];
	$default_minbid = ($system->SETTINGS['moneyformat'] == 1) ? '0.99' : '0,99';
	$minimum_bid = (empty($minimum_bid)) ? $default_minbid : $minimum_bid;
	$shipping_cost = (isset($_POST['shipping_cost'])) ? $_POST['shipping_cost'] : $_SESSION['SELL_shipping_cost'];
	$shipping_cost = (empty($shipping_cost)) ? 0 : $shipping_cost;
	$additional_shipping_cost = (isset($_POST['additional_shipping_cost'])) ? $_POST['additional_shipping_cost'] : $_SESSION['SELL_additional_shipping_cost'];
	$additional_shipping_cost = (empty($additional_shipping_cost)) ? 0 : $additional_shipping_cost;
	$imgtype = (isset($_POST['imgtype'])) ? $_POST['imgtype'] : $_SESSION['SELL_file_uploaded'];
	$title = (isset($_POST['title'])) ? $_POST['title'] : $_SESSION['SELL_title'];
	$subtitle = (isset($_POST['subtitle'])) ? $_POST['subtitle'] : $_SESSION['SELL_subtitle'];
	$description = (isset($_POST['description'])) ? $_POST['description'] : $_SESSION['SELL_description'];
	$pict_url = (isset($_POST['pict_url'])) ? $_POST['pict_url'] : $_SESSION['SELL_pict_url'];
	$atype = (isset($_POST['atype'])) ? $_POST['atype'] : $_SESSION['SELL_atype'];
	$iquantity = (int)(isset($_POST['iquantity'])) ? $_POST['iquantity'] : $_SESSION['SELL_iquantity'];
	$iquantity = (empty($iquantity)) ? 1 : round($iquantity);
	$buy_now = (isset($_POST['buy_now'])) ? $_POST['buy_now'] : $_SESSION['SELL_with_buy_now'];
	$buy_now_price = (isset($_POST['buy_now_price'])) ? $_POST['buy_now_price'] : $_SESSION['SELL_buy_now_price'];
	$duration = (isset($_POST['duration'])) ? $_POST['duration'] : $_SESSION['SELL_duration'];
	$relist = (isset($_POST['autorelist'])) ? $_POST['autorelist'] : $_SESSION['SELL_relist'];
	$increments = (isset($_POST['increments'])) ? $_POST['increments'] : $_SESSION['SELL_increments'];
	$customincrement = (isset($_POST['customincrement'])) ? $_POST['customincrement'] : $_SESSION['SELL_customincrement'];
	$shipping = (isset($_POST['shipping'])) ? $_POST['shipping'] : $_SESSION['SELL_shipping'];
	$shipping_terms = (isset($_POST['shipping_terms'])) ? $_POST['shipping_terms'] : $_SESSION['SELL_shipping_terms'];
	$payment = (isset($_POST['payment'])) ? $_POST['payment'] : $_SESSION['SELL_payment'];
	$payment = (is_array($payment)) ? $payment : array();
	$international = (isset($_POST['international'])) ? $_POST['international'] : ''; 
	$international = (isset($_SESSION['SELL_international']) && (!isset($_POST['action']) && $_POST['action'] != 2)) ? $_SESSION['SELL_international'] : $international; 
	$sellcat1 = $_SESSION['SELL_sellcat1'];
	$_SESSION['SELL_sellcat2'] = (isset($_SESSION['SELL_sellcat2'])) ? $_SESSION['SELL_sellcat2'] : '';
	$sellcat2 = $_SESSION['SELL_sellcat2'];
	$buy_now_only = (isset($_POST['buy_now_only'])) ? $_POST['buy_now_only'] : $_SESSION['SELL_buy_now_only'];
	$buy_now_only = (empty($buy_now_only)) ? 'n' : $buy_now_only;
	$a_starts = (isset($_POST['a_starts'])) ? $_POST['a_starts'] : $_SESSION['SELL_starts'];
	$is_bold = (isset($_POST['is_bold'])) ? 'y' : $_SESSION['SELL_is_bold'];
	$is_featured = (isset($_POST['is_featured'])) ? 'y' : $_SESSION['SELL_is_featured'];
	$is_highlighted = (isset($_POST['is_highlighted'])) ? 'y' : $_SESSION['SELL_is_highlighted'];
	$digital_item = (isset($_SESSION['SELL_upload_file'])) ? $_SESSION['SELL_upload_file'] : '';
	$item_condition = (isset($_POST['item_condition'])) ? $_POST['item_condition'] : $_SESSION['SELL_item_condition'];
	$item_manufacturer = (isset($_POST['item_manufacturer'])) ? $_POST['item_manufacturer'] : $_SESSION['SELL_item_manufacturer'];
	$item_model = (isset($_POST['item_model'])) ? $_POST['item_model'] : $_SESSION['SELL_item_model'];
	$item_colour = (isset($_POST['item_colour'])) ? $_POST['item_colour'] : $_SESSION['SELL_item_colour'];
	$item_year = (isset($_POST['item_year'])) ? $_POST['item_year'] : $_SESSION['SELL_item_year'];
	$returns = (isset($_POST['returns'])) ? $_POST['returns'] : '0';  
    $returns = (isset($_SESSION['SELL_returns']) && (!isset($_POST['action']) || $_POST['action'] != 3)) ? $_SESSION['SELL_returns'] : $returns;
	$freeItime = (isset($_POST['sellType'])) ? $_POST['sellType'] : $_SESSION['SELL_sell_type'];
	
	if (isset($_POST['a_starts']))
	{
		if (isset($_POST['start_now']))
		{
			$start_now = 1;
		}
		else
		{
			$start_now = 0;
		}
	}
	else
	{
		$start_now = $_SESSION['SELL_start_now'];
	}

	$is_taxed = (isset($_POST['is_taxed'])) ? 'y' : $_SESSION['SELL_is_taxed'];
	$tax_included = (isset($_POST['tax_included'])) ? 'y' : $_SESSION['SELL_tax_included'];
	if (isset($_POST['action']) && $_POST['action'] == 2)
	{
		$is_bold = (isset($_POST['is_bold'])) ? 'y' : 'n';
		$is_featured = (isset($_POST['is_featured'])) ? 'y' : 'n';
		$is_highlighted = (isset($_POST['is_highlighted'])) ? 'y' : 'n';
		$is_taxed = (isset($_POST['is_taxed'])) ? 'y' : 'n';
		$tax_included = (isset($_POST['tax_included'])) ? 'y' : 'n';
		$payment = (isset($_POST['payment'])) ? $payment : array();
	}
}

function makesessions()
{
	global $with_reserve, $reserve_price, $minimum_bid, $pict_url, $imgtype, $title, $subtitle, $description, $pict_url, $atype, $iquantity, $buy_now, $buy_now_price, $is_taxed, $tax_included, $additional_shipping_cost, $freeItime;
	global $duration, $relist, $increments, $customincrement, $shipping, $shipping_terms, $payment, $international, $sendemail, $buy_now_only, $a_starts, $shipping_cost, $is_bold, $is_highlighted, $is_featured, $item_condition, $item_manufacturer, $item_model, $item_colour, $item_year, $returns, $digital_item, $start_now, $_SESSION;

	$_SESSION['SELL_with_reserve'] = $with_reserve;
	$_SESSION['SELL_reserve_price'] = $reserve_price;
	$_SESSION['SELL_minimum_bid'] = $minimum_bid;
	$_SESSION['SELL_shipping_cost'] = $shipping_cost;
	$_SESSION['SELL_additional_shipping_cost'] = $additional_shipping_cost;
	$_SESSION['SELL_file_uploaded'] = $imgtype;
	$_SESSION['SELL_title'] = $title;
	$_SESSION['SELL_subtitle'] = $subtitle;
	$_SESSION['SELL_description'] = $description;
	$_SESSION['SELL_pict_url'] = $pict_url;
	$_SESSION['SELL_atype'] = $atype;
	$_SESSION['SELL_iquantity'] = $iquantity;
	$_SESSION['SELL_with_buy_now'] = $buy_now;
	$_SESSION['SELL_buy_now_price'] = $buy_now_price;
	$_SESSION['SELL_duration'] = $duration;
	$_SESSION['SELL_relist'] = $relist;
	$_SESSION['SELL_increments'] = $increments;
	$_SESSION['SELL_customincrement'] = $customincrement;
	$_SESSION['SELL_shipping'] = $shipping;
	$_SESSION['SELL_shipping_terms'] = $shipping_terms;
	$_SESSION['SELL_payment'] = $payment;
	$_SESSION['SELL_international'] = $international;
	$_SESSION['SELL_buy_now_only'] = $buy_now_only;
	$_SESSION['SELL_starts'] = $a_starts;
	$_SESSION['SELL_is_bold'] = $is_bold;
	$_SESSION['SELL_is_highlighted'] = $is_highlighted;
	$_SESSION['SELL_is_featured'] = $is_featured;
	$_SESSION['SELL_start_now'] = $start_now;
	$_SESSION['SELL_is_taxed'] = $is_taxed;
	$_SESSION['SELL_tax_included'] = $tax_included;
	$_SESSION['SELL_upload_file'] = $digital_item;
	$_SESSION['SELL_item_condition'] = $item_condition;
	$_SESSION['SELL_item_manufacturer'] = $item_manufacturer;
	$_SESSION['SELL_item_model'] = $item_model;
	$_SESSION['SELL_item_colour'] = $item_colour;
	$_SESSION['SELL_item_year'] = $item_year;
	$_SESSION['SELL_returns'] = $returns;
	$_SESSION['SELL_sell_type'] = $freeItime;
}

function unsetsessions()
{
	global $_SESSION, $system;

	$_SESSION['SELL_with_reserve'] = '';
	$_SESSION['SELL_reserve_price'] = '';
	$_SESSION['SELL_minimum_bid'] = ($system->SETTINGS['moneyformat'] == 1) ? 0.99 : '0,99';
	$_SESSION['SELL_shipping_cost'] = 0;
	$_SESSION['SELL_additional_shipping_cost'] = 0;
	$_SESSION['SELL_file_uploaded'] = '';
	$_SESSION['SELL_title'] = '';
	$_SESSION['SELL_subtitle'] = '';
	$_SESSION['SELL_description'] = '';
	$_SESSION['SELL_pict_url'] = '';
	$_SESSION['SELL_pict_url_temp'] = '';
	$_SESSION['SELL_atype'] = '';
	$_SESSION['SELL_iquantity'] = '';
	$_SESSION['SELL_with_buy_now'] = '';
	$_SESSION['SELL_buy_now_price'] = '';
	$_SESSION['SELL_duration'] = '';
	$_SESSION['SELL_relist'] = '';
	$_SESSION['SELL_increments'] = '';
	$_SESSION['SELL_customincrement'] = 0;
	$_SESSION['SELL_shipping'] = 1;
	$_SESSION['SELL_shipping_terms'] = '';
	$_SESSION['SELL_payment'] = array();
	$_SESSION['SELL_international'] = '';
	$_SESSION['SELL_sendemail'] = '';
	$_SESSION['SELL_starts'] = '';
	$_SESSION['SELL_action'] = '';
	$_SESSION['SELL_is_bold'] = 'n';
	$_SESSION['SELL_is_highlighted'] = 'n';
	$_SESSION['SELL_is_featured'] = 'n';
	$_SESSION['SELL_start_now'] = '';
	$_SESSION['SELL_is_taxed'] = 'n';
	$_SESSION['SELL_tax_included'] = 'y';
	$_SESSION['SELL_upload_file'] = '';
	$_SESSION['SELL_item_condition'] = '';
	$_SESSION['SELL_item_manufacturer'] = '';
	$_SESSION['SELL_item_model'] = '';
	$_SESSION['SELL_item_colour'] = '';
	$_SESSION['SELL_item_year'] = '';
	$_SESSION['SELL_digital_item'] = '';
	$_SESSION['SELL_returns'] = '';
	$_SESSION['SELL_upload_file'] = '';
	$_SESSION['SELL_sell_type'] = '';
}

//update auction database
function updateauction($type)
{
	global $_SESSION, $DBPrefix, $a_starts, $a_ends, $payment_text, $system, $fee, $db;
	
	//only runs if the auction is being relisted
	$extraquery = ($type == 2) ? ",relisted = relisted + 1, current_bid = 0, starts = '" . $a_starts . "', num_bids = 0" : '';
	
	$query =
		"UPDATE " . $DBPrefix . "auctions SET
		title = '" . stripslashes($system->cleanvars($_SESSION['SELL_title'])) . "',
		subtitle = '" . stripslashes($system->cleanvars($_SESSION['SELL_subtitle'])) . "',
		description = '" . $_SESSION['SELL_description'] . "',
		pict_url = '" . $system->cleanvars($_SESSION['SELL_pict_url']) . "',
		category = '" . $_SESSION['SELL_sellcat1'] . "',
		secondcat = '" . intval($_SESSION['SELL_sellcat2']) . "',
		minimum_bid = '" . $system->input_money(($_SESSION['SELL_buy_now_only'] == 'n') ? $_SESSION['SELL_minimum_bid'] : $_SESSION['SELL_buy_now_price']) . "',
		shipping_cost = '" . $system->input_money($_SESSION['SELL_shipping_cost']) . "',
		shipping_cost_additional = '" . $system->input_money($_SESSION['SELL_additional_shipping_cost']) . "',
		reserve_price = '" . $system->input_money(($_SESSION['SELL_with_reserve'] == 'yes') ? $_SESSION['SELL_reserve_price'] : 0) . "',
		buy_now = '" . $system->input_money(($_SESSION['SELL_with_buy_now'] == 'yes') ? $_SESSION['SELL_buy_now_price'] : 0) . "',
		bn_only = '" . $_SESSION['SELL_buy_now_only'] . "',
		auction_type = '" . $_SESSION['SELL_atype'] . "',
		duration = '" . $_SESSION['SELL_duration'] . "',
		increment = " . $system->input_money($_SESSION['SELL_customincrement']) . ",
		shipping = '" . $_SESSION['SELL_shipping'] . "',
		payment = '" . $payment_text . "',
		international = '" . (($_SESSION['SELL_international']) ? 1 : 0) . "',
		ends = '" . $a_ends . "',
		photo_uploaded = '" . (($_SESSION['SELL_file_uploaded'])? 1 : 0) . "',
		quantity = '" . $_SESSION['SELL_iquantity'] . "',
		relist = " . intval($_SESSION['SELL_relist']) . ",
		shipping_terms = '" . $system->cleanvars($_SESSION['SELL_shipping_terms']) . "',
		closed = '0',
		bold = '" . $_SESSION['SELL_is_bold'] . "',
		highlighted = '" . $_SESSION['SELL_is_highlighted'] . "',
		featured = '" . $_SESSION['SELL_is_featured'] . "',
		tax = '" . $_SESSION['SELL_is_taxed'] . "',
		taxinc = '" . $_SESSION['SELL_tax_included'] . "',
		item_condition = '" . $_SESSION['SELL_item_condition'] . "',
		item_manufacturer = '" . $_SESSION['SELL_item_manufacturer'] . "',
		item_model = '" . $_SESSION['SELL_item_model'] . "',
		item_colour = '" . $_SESSION['SELL_item_colour'] . "',
		item_year = '" . $_SESSION['SELL_item_year'] . "',
		returns = '" . (($_SESSION['SELL_returns']) ? 1 : 0) . "',
		sell_type = '" . $_SESSION['SELL_sell_type'] . "',
		current_fee = current_fee + " . $fee . $extraquery . " 
		WHERE id = " . $_SESSION['SELL_auction_id'];
		$db->direct_query($query);
}

//add new auction to database
function addauction()
{
	global $DBPrefix, $_SESSION, $user, $a_starts, $a_ends, $payment_text, $system, $fee, $db;

	// set values
	$min_bid = $system->input_money(($_SESSION['SELL_buy_now_only'] == 'n') ? $_SESSION['SELL_minimum_bid'] : $_SESSION['SELL_buy_now_price']);
	$reserve_price = $system->input_money(($_SESSION['SELL_with_reserve'] == 'yes') ? $_SESSION['SELL_reserve_price'] : 0);
	$bn_price = $system->input_money(($_SESSION['SELL_with_buy_now'] == 'yes') ? $_SESSION['SELL_buy_now_price'] : 0);
	$query = "INSERT INTO " . $DBPrefix . "auctions (id,user,title,subtitle,starts,description,pict_url,category,secondcat,minimum_bid,shipping_cost,shipping_cost_additional,reserve_price,buy_now,auction_type,duration,increment,shipping,payment,international,ends,photo_uploaded,quantity,relist,shipping_terms,bn_only,bold,highlighted,featured,current_fee,tax,taxinc, item_condition, item_manufacturer, item_model, item_colour, item_year, returns, sell_type) VALUES
	('NULL', '" . $user->user_data['id'] . "', '" . stripslashes($system->cleanvars($_SESSION['SELL_title'])) . "', '" . stripslashes($system->cleanvars($_SESSION['SELL_subtitle'])) . "', '" .  $a_starts . "', '" . addslashes($_SESSION['SELL_description']) . "', '" . $system->cleanvars($_SESSION['SELL_pict_url']) . "', '" . $_SESSION['SELL_sellcat1'] . "', '" . intval($_SESSION['SELL_sellcat2']) . "', '" . $min_bid . "', '" . $system->input_money($_SESSION['SELL_shipping_cost']) . "', '" . $system->input_money($_SESSION['SELL_additional_shipping_cost']) . "', '" . $reserve_price . "', '" . $bn_price . "', '" . $_SESSION['SELL_atype'] . "', '" . $_SESSION['SELL_duration'] . "', '" . $system->input_money($_SESSION['SELL_customincrement']) . "', '" . $_SESSION['SELL_shipping'] . "', '" . $payment_text . "', '" . (($_SESSION['SELL_international']) ? 1 : 0) . "', '" . $a_ends . "', '" . (($_SESSION['SELL_file_uploaded']) ? 1 : 0) . "', '" . $_SESSION['SELL_iquantity'] . "', '" . intval($_SESSION['SELL_relist']) . "', '" . $system->cleanvars($_SESSION['SELL_shipping_terms']) . "', '" . $_SESSION['SELL_buy_now_only'] . "', '" . $_SESSION['SELL_is_bold'] . "', '" . $_SESSION['SELL_is_highlighted'] . "', '" . $_SESSION['SELL_is_featured'] . "', '" . $fee . "', '" . $_SESSION['SELL_is_taxed'] . "', '" . $_SESSION['SELL_tax_included'] . "', '" . $system->cleanvars($_SESSION['SELL_item_condition']) . "', '" . $system->cleanvars($_SESSION['SELL_item_manufacturer']) . "', '" . $system->cleanvars($_SESSION['SELL_item_model']) . "', '" . $system->cleanvars($_SESSION['SELL_item_colour']) . "', '" . $system->cleanvars($_SESSION['SELL_item_year']) . "', '" . (($_SESSION['SELL_returns']) ? 1 : 0) . "', '" . $_SESSION['SELL_sell_type'] . "')";
	$db->direct_query($query);
}


function remove_bids($auction_id)
{
	global $DBPrefix, $db;
	$query = "DELETE FROM " . $DBPrefix . "bids WHERE auction = :auction_id";
	$params = array();
	$params[] = array(':auction_id', $auction_id, 'int');
	$db->query($query, $params);
}

function update_cat_counters($add, $category)
{
	global $DBPrefix, $system, $catscontrol, $db;

	$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :cat_id";
	$params = array();
	$params[] = array(':cat_id', $category, 'int');
	$db->query($query, $params);
	
	$parent_node = $db->result();
	$crumbs = $catscontrol->get_bread_crumbs($parent_node['left_id'], $parent_node['right_id']);

	$addsub = ($add) ? '+' : '-';
	for ($i = 0; $i < count($crumbs); $i++)
	{
		$query = "UPDATE " . $DBPrefix . "categories SET sub_counter = sub_counter " . $addsub . " 1 WHERE cat_id = :cat_id";
		$params = array();
		$params[] = array(':cat_id', $crumbs[$i]['cat_id'], 'int');
		$db->query($query, $params);
	}
}

function get_category_string($sellcat)
{
	global $DBPrefix, $system, $catscontrol, $category_names, $db;

	if (empty($sellcat) || !isset($sellcat))
		return '';

	$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :cat_id";
	$params = array();
	$params[] = array(':cat_id', $sellcat, 'int');
	$db->query($query, $params);
	$parent_node = $db->result();

	$TPL_categories_list = '';
	$crumbs = $catscontrol->get_bread_crumbs($parent_node['left_id'], $parent_node['right_id']);
	for ($i = 0; $i < count($crumbs); $i++)
	{
		if ($crumbs[$i]['cat_id'] > 0)
		{
			if ($i > 0)
			{
				$TPL_categories_list .= ' <img class="bc_divider" src="images/bc_divider.png" alt="">';
			}
			$TPL_categories_list .= $category_names[$crumbs[$i]['cat_id']];
		}
	}
	return $TPL_categories_list;
}

/// Work out the auction setup fees
function get_fee($minimum_bid, $just_fee = true)
{
	global $system, $DBPrefix, $buy_now_price, $reserve_price, $is_bold, $is_highlighted, $is_featured, $_SESSION, $subtitle, $sellcat2, $relist, $db, $user;

	if ($_SESSION['SELL_action'] == 'edit' && !$user->no_fees)
	{
		$query = "SELECT * FROM " . $DBPrefix . "useraccounts WHERE auc_id = :auction_id AND user_id = :user_id";
		$params = array();
		$params[] = array(':auction_id', $_SESSION['SELL_auction_id'], 'int');
		$params[] = array(':user_id', $user->user_data['id'], 'int');
		$db->query($query, $params);
		// build an array full of everything the user has been charged for the auction do far
		$past_fees = array();
		while ($row = $db->result())
		{
			foreach ($row as $k => $v)
			{
				if (isset($past_fees[$k]))
				{
					$past_fees[$k] += $v;
				}
				else
				{
					$past_fees[$k] = $v;
				}
			}
		}

		$diff = 0; // difference from last payment
		$fee_data['setup'] = 0; // shouldn't have to pay setup for an edit...
		$diff = bcadd($diff, $past_fees['setup'], $system->SETTINGS['moneydecimals']);
		if ($past_fees['bold'] == $fee_data['bolditem_fee'] && !$user->no_bolditem_fee)
		{
			$diff = bcadd($diff, $fee_data['bolditem_fee'], $system->SETTINGS['moneydecimals']);
			$fee_data['bolditem_fee'] = 0;
		}
		if ($past_fees['highlighted'] == $fee_data['hlitem_fee'] && !$user->no_hlitem_fee)
		{
			$diff = bcadd($diff, $fee_data['hlitem_fee'], $system->SETTINGS['moneydecimals']);
			$fee_data['hlitem_fee'] = 0;
		}
		if ($past_fees['subtitle'] == $fee_data['subtitle_fee'] && !$user->no_subtitle_fee)
		{
			$diff = bcadd($diff, $fee_data['subtitle_fee'], $system->SETTINGS['moneydecimals']);
			$fee_data['subtitle_fee'] = 0;
		}
		if ($past_fees['relist'] == $fee_data['relist_fee'] && !$user->no_relist_fee)
		{
			$diff = bcadd($diff, $fee_data['relist_fee'], $system->SETTINGS['moneydecimals']);
			$fee_data['relist_fee'] = 0;
		}
		if ($past_fees['reserve'] == $fee_data['rp_fee'] && !$user->no_rp_fee)
		{
			$diff = bcadd($diff, $fee_data['rp_fee'], $system->SETTINGS['moneydecimals']);
			$fee_data['rp_fee'] = 0;
		}
		if ($past_fees['buynow'] == $fee_data['buyout_fee'] && !$user->no_buyout_fee)
		{
			$diff = bcadd($diff, $fee_data['buyout_fee'], $system->SETTINGS['moneydecimals']);
			$fee_data['buyout_fee'] = 0;
		}
		if ($past_fees['image'] == $fee_data['picture_fee'] && !$user->no_picture_fee)
		{
			$diff = bcadd($diff, $fee_data['picture_fee'], $system->SETTINGS['moneydecimals']);
			$fee_data['picture_fee'] = 0;
		}
		if ($past_fees['extcat'] == $fee_data['excat_fee'] && !$user->no_excat_fee)
		{
			$diff = bcadd($diff, $fee_data['excat_fee'], $system->SETTINGS['moneydecimals']);
			$fee_data['excat_fee'] = 0;
		}
		$fee_value = bcsub($fee_value, $diff, $system->SETTINGS['moneydecimals']);
		if ($fee_value < 0)
		{
			$fee_value = 0;
		}
	}
	else
	{
		$query = "SELECT * FROM " . $DBPrefix . "fees ORDER BY type, fee_from ASC";
		$db->direct_query($query);
	
		$fee_value = 0;
		$fee_data = array();
		if(!$user->no_fees)
		{
			while ($row = $db->result())
			{
				if ($minimum_bid >= $row['fee_from'] && $minimum_bid <= $row['fee_to'] && $row['type'] == 'setup' && !$user->no_setup_fee)
				{
					if ($row['fee_type'] == 'flat')
					{
						$fee_data['setup'] = $row['value'];
						$fee_value = bcadd($fee_value, $row['value'], $system->SETTINGS['moneydecimals']);
					}
					else
					{
						$tmp = bcdiv($row['value'], '100', $system->SETTINGS['moneydecimals']);
						$tmp = bcmul($tmp, $minimum_bid, $system->SETTINGS['moneydecimals']);
						$fee_data['setup'] = $tmp;
						$fee_value = bcadd($fee_value, $tmp, $system->SETTINGS['moneydecimals']);
					}
				}
				if ($row['type'] == 'buyout_fee' && $buy_now_price > 0 && !$user->no_buyout_fee)
				{
					$fee_data['buyout_fee'] = $row['value'];
					$fee_value = bcadd($fee_value, $row['value'], $system->SETTINGS['moneydecimals']);
				}
				if ($row['type'] == 'rp_fee' && $reserve_price > 0 && !$user->no_rp_fee)
				{
					$fee_data['rp_fee'] = $row['value'];
					$fee_value = bcadd($fee_value, $row['value'], $system->SETTINGS['moneydecimals']);
				}
				if ($row['type'] == 'bolditem_fee' && $is_bold == 'y' && !$user->no_bolditem_fee)
				{
					$fee_data['bolditem_fee'] = $row['value'];
					$fee_value = bcadd($fee_value, $row['value'], $system->SETTINGS['moneydecimals']);
				}
				if ($row['type'] == 'hlitem_fee' && $is_highlighted == 'y' && !$user->no_hlitem_fee)
				{
					$fee_data['hlitem_fee'] = $row['value'];
					$fee_value = bcadd($fee_value, $row['value'], $system->SETTINGS['moneydecimals']);
				}
				if ($row['type'] == 'hpfeat_fee' && $is_featured == 'y' && !$user->no_hpfeat_fee)
				{
					$fee_data['hpfeat_fee'] = $row['value'];
					$fee_value = bcadd($fee_value, $row['value'], $system->SETTINGS['moneydecimals']);
				}
				if ($row['type'] == 'picture_fee' && count($_SESSION['UPLOADED_PICTURES']) > 0 && !$user->no_picture_fee)
				{
					//count the total number of pictures uploaded and subtract the max. free pictures allowed
					$picCount = count($_SESSION['UPLOADED_PICTURES']) - $system->SETTINGS['freemaxpictures'];
					$tmp = bcmul($picCount, $row['value'], $system->SETTINGS['moneydecimals']);
					$fee_data['picture_fee'] = $tmp;
					$fee_value = bcadd($fee_value, $tmp, $system->SETTINGS['moneydecimals']);
				}
				if ($row['type'] == 'subtitle_fee' && !empty($subtitle) && !$user->no_subtitle_fee)
				{
					$fee_data['subtitle_fee'] = $row['value'];
					$fee_value = bcadd($fee_value, $row['value'], $system->SETTINGS['moneydecimals']);
				}
				if ($row['type'] == 'excat_fee' && $sellcat2 > 0 && !$user->no_excat_fee)
				{
					$fee_data['excat_fee'] = $row['value'];
					$fee_value = bcadd($fee_value, $row['value'], $system->SETTINGS['moneydecimals']);
				}
				if ($row['type'] == 'relist_fee' && $relist > 0 && !$user->no_relist_fee)
				{
					$fee_data['relist_fee'] = ($row['value'] * $relist);
					$fee_value = bcadd($fee_value, ($row['value'] * $relist), $system->SETTINGS['moneydecimals']);
				}
			}
		}
	}
	if ($just_fee)
	{
		$return = $fee_value;
	}
	else
	{
		$return = array($fee_value, $fee_data);
	}
	return $return;
}

function addoutstanding()
{
	global $DBPrefix, $fee_data, $user, $system, $fee, $_SESSION, $db;
	
	if ($_SESSION['SELL_action'] == 'edit')
	{
		$query = "SELECT * FROM " . $DBPrefix . "useraccounts 		
			WHERE auc_id = :auct_id AND user_id = :users_ids";
		$params = array();
		$params[] = array(':auct_id', $_SESSION['SELL_auction_id'], 'int');
		$params[] = array(':users_ids', $user->user_data['id'], 'int');
		$db->query($query, $params);
		$stored_fee = $db->result();
		if(!$user->no_fees)
		{
			if(!$user->no_setup_fee)
			{
				if ($stored_fee['setup'] > 0)
				{
					$setup_data = $stored_fee['setup'];
				}
				else
				{
					$setup_data = $fee_data['setup'];
				}
			}
			else
			{
				$setup = $system->print_money_nosymbol(0);
			}
	
			if(!$user->no_hpfeat_fee)
			{
				if ($stored_fee['featured'] > 0)
				{
					$hpfeat_fee_data = $stored_fee['featured'];
				}
				else
				{
					$hpfeat_fee_data = $fee_data['hpfeat_fee'];
				}
			}
			else
			{
				$hpfeat_fee_data = $system->print_money_nosymbol(0);
			}
			if(!$user->no_bolditem_fee)
			{
				if ($stored_fee['bold'] > 0)
				{
					$bold_data = $stored_fee['bold'];
				}
				else
				{
					$bold_data = $fee_data['bolditem_fee'];
				}
			}
			else
			{
				$bold_data = $system->print_money_nosymbol(0);
			}
			if(!$user->no_hlitem_fee)
			{
				if ($stored_fee['highlighted'] > 0)
				{
					$highlighted_data = $stored_fee['highlighted'];
				}
				else
				{
					$highlighted_data = $fee_data['hlitem_fee'];
				}
			}
			else
			{
				$highlighted_data = $system->print_money_nosymbol(0);
			}
			if(!$user->no_subtitle_fee)
			{
				if ($stored_fee['subtitle'] > 0)
				{
					$subtitle_data = $stored_fee['subtitle'];
				}
				else
				{
					$subtitle_data = $fee_data['subtitle_fee'];
				}
			}
			else
			{
				$subtitle_data = $system->print_money_nosymbol(0);
			}
			if(!$user->no_relist_fee)
			{
				if ($stored_fee['relist'] > 0)
				{
					$relist_data = $stored_fee['relist'];
				}
				else
				{
					$relist_data = $fee_data['relist_fee'];
				}
			}
			else
			{
				$relist_data = $system->print_money_nosymbol(0);
			}
			if(!$user->no_rp_fee)
			{
				if ($stored_fee['reserve'] > 0)
				{
					$reserve_data = $stored_fee['reserve'];
				}
				else
				{
					$reserve_data = $fee_data['rp_fee'];
				}
			}
			else
			{
				$reserve_data = $system->print_money_nosymbol(0);
			}
			if(!$user->no_buyout_fee)
			{
				if ($stored_fee['buynow'] > 0)
				{
					$buynow_data = $stored_fee['buynow'];
				}
				else
				{
					$buynow_data = $fee_data['buyout_fee'] && !$user->no_buyout_fee;
				}
			}
			else
			{
				$buynow_data = $system->print_money_nosymbol(0);
			}
			if(!$user->no_picture_fee)
			{
				if ($stored_fee['image'] > 0)
				{
					$image_data = $stored_fee['image'];
				}
				else
				{
					$image_data = $fee_data['picture_fee'];
				}
			}
			else
			{
				$image_data = $system->print_money_nosymbol(0);
			}
			if(!$user->no_excat_fee)
			{
				if ($stored_fee['extcat'] > 0)
				{
					$extcat_data = $stored_fee['extcat'];
				}
				else
				{
					$extcat_data = $fee_data['excat_fee'];
				}
			}
			else
			{
				$extcat_data = $system->print_money_nosymbol(0);
			}
		}
		if ($fee > 0)
		{
			$total_data = $stored_fee['total'];
		}
		else
		{
			$total = $hpfeat_fee_data + $bold_data + $highlighted_data + $subtitle_data + $relist_data + $reserve_data + $buynow_data + $image_data + $extcat_data;
			$total_data = $total;
		}	
		
		$query = "UPDATE " . $DBPrefix . "useraccounts SET 
			setup = '" . $setup_data . "',
			featured = '" . $hpfeat_fee_data . "',
			bold = '" . $bold_data . "',
			highlighted = '" . $highlighted_data . "',
			subtitle = '" . $subtitle_data . "',
			relist = '" . $relist_data . "',
			reserve = '" . $reserve_data . "',
			buynow = '" . $buynow_data . "',
			image = '" . $image_data . "',
			extcat = '" . $extcat_data . "',
			total = '" . $total_data . "',
			paid = 0
			WHERE auc_id = " . $_SESSION['SELL_auction_id'] . " AND user_id = " . $user->user_data['id'];
		$db->direct_query($query);
	}
	else
	{
		//build the fees
		$setup = !$user->no_setup_fee ? $fee_data['setup'] : $system->print_money_nosymbol(0);
		$hpfeat = !$user->no_hpfeat_fee ? $fee_data['hpfeat_fee'] : $system->print_money_nosymbol(0);
		$bolditem = !$user->no_bolditem_fee ? $fee_data['bolditem_fee'] : $system->print_money_nosymbol(0);
		$hlitem = !$user->no_hlitem_fee ? $fee_data['hlitem_fee'] : $system->print_money_nosymbol(0);
		$subtitle = !$user->no_subtitle_fee ? $fee_data['subtitle_fee'] : $system->print_money_nosymbol(0);
		$relist = !$user->no_relist_fee ? $fee_data['relist_fee'] : $system->print_money_nosymbol(0);
		$rp = !$user->no_rp_fee ? $fee_data['rp_fee'] : $system->print_money_nosymbol(0);
		$buyout = !$user->no_buyout_fee ? $fee_data['buyout_fee'] : $system->print_money_nosymbol(0);
		$picture = !$user->no_picture_fee ? $fee_data['picture_fee'] : $system->print_money_nosymbol(0);
		$excat = !$user->no_excat_fee ? $fee_data['excat_fee'] : $system->print_money_nosymbol(0);
		
		$query = "INSERT INTO " . $DBPrefix . "useraccounts (auc_id,user_id,date,setup,featured,bold,highlighted,subtitle,relist,reserve,buynow,image,extcat,total,paid) VALUES
			('" . $_SESSION['SELL_auction_id'] . "','" . $user->user_data['id'] . "', '" . $system->ctime . "', '" . $setup . "', '" . $hpfeat . "', '" . $bolditem . "', '" . $hlitem . "', '" . $subtitle . "', '" . $relist . "', '" . $rp . "', '" . $buyout . "', '" . $picture . "', '" . $excat . "', '" . $fee . "', 0)";
		$db->direct_query($query);
	}
}

function check_gateway($gateway)
{
	global $user;
	if ($gateway == 'paypal' && !empty($user->user_data['paypal_email']))
		return true;
	if ($gateway == 'authnet' && !empty($user->user_data['authnet_id']) && !empty($user->user_data['authnet_pass']))
		return true;
	if ($gateway == 'worldpay' && !empty($user->user_data['worldpay_id']))
		return true;
	if ($gateway == 'skrill' && !empty($user->user_data['skrill_email']))
		return true;
	if ($gateway == 'toocheckout' && !empty($user->user_data['toocheckout_id']))
		return true;
	if ($gateway == 'bank' && !empty($user->user_data['bank_name']) && !empty($user->user_data['bank_account']) && !empty($user->user_data['bank_routing']))
		return true;
	return false;
}

function add_digital_item($digital_item, $auction_id)
{
	global $user, $db, $upload_path, $system, $DBPrefix;
	
	// delete any existing files so only 1 file is stored
	if (isset($digital_item))
	{
		if (is_dir($upload_path . session_id() . '/' . 'items'))
		{
			if (is_dir($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id))
			{
				if ($dir = opendir($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id))
				{
					while (($file = readdir($dir)) !== false)
					{
						if (!is_dir($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id . '/' . $file))
							unlink($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id . '/' . $file);
					}
					closedir($dir);
				}
			}
		}
	}

	// Copy digital item file
	if (isset($digital_item))
	{	
		$digital_pass = false;					
		if (is_dir($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id))
		{	
			// update the file name that is stored in the sql
			$query = "UPDATE  " . $DBPrefix . "digital_items SET item = :di_items WHERE auctions = :auct_ids AND seller = :user_ids";
			$params = array();
			$params[] = array(':di_items', $digital_item, 'str');
			$params[] = array(':auct_ids', $auction_id, 'int');
			$params[] = array(':user_ids', $user->user_data['id'], 'int');
			$db->query($query, $params);
														
			// move the file from the temp folder to the main folder
			$system->move_file($upload_path . session_id() . '/' . 'items' . '/' . $digital_item, $upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id . '/' . $digital_item);
			chmod($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id . '/' . $digital_item, 0700);
			$digital_pass = true;
		}
		elseif(file_exists($upload_path . session_id() . '/' . 'items' . '/' . $digital_item))
		{
			// store the file name to the sql
			$query = "INSERT INTO " . $DBPrefix . "digital_items VALUES (NULL, :auct_ids, :user_ids, :di_items, :di_hash)";
			$params = array();
			$params[] = array(':auct_ids', $auction_id, 'int');
			$params[] = array(':user_ids', $user->user_data['id'], 'int');
			$params[] = array(':di_items', $digital_item, 'str');
			$params[] = array(':di_hash', md5(uniqid(rand(0,99), true)), 'str');
			$db->query($query, $params);
									
			//check to if the folders was made
			if (!is_dir($upload_path . 'items' . '/' . $user->user_data['id'])) mkdir($upload_path . 'items' . '/' . $user->user_data['id'], 0700);
			if (!is_dir($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id)) mkdir($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id, 0700);
									
			// move the file from the temp folder to the main folder
			$system->move_file($upload_path . session_id() . '/' . 'items' . '/' . $digital_item, $upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id . '/' . $digital_item);
			chmod($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id . '/' . $digital_item, 0700);
			$digital_pass = true;
		}
							
		//deleting all the temp folders and files
		if(is_dir($upload_path . 'items' . '/' . $user->user_data['id'] . '/' . $auction_id) && $digital_pass == true)
		{
			// Delete files, using dir (to eliminate eventual odd files)
			if (is_dir($upload_path . session_id() . '/' . 'items'))
			{
				if ($dir = opendir($upload_path . session_id() . '/' . 'items'))
				{
					while (($file = readdir($dir)) !== false)
					{
						if (!is_dir($upload_path . session_id() . '/' . 'items' . '/' . $file))
							unlink($upload_path . session_id() . '/' . 'items' . '/' . $file);
					}
					closedir($dir);
				}
			}
			if (is_dir($upload_path . session_id()))
			{
				if ($dir = opendir($upload_path . session_id()))
				{
					while (($file = readdir($dir)) !== false)
					{
						if (!is_dir($upload_path . session_id() . '/' . $file))
							unlink($upload_path . session_id() . '/' . $file);
					}
					closedir($dir);
				}
			}
									
			// making sure to delete the session folders
			if (is_dir($upload_path . session_id() . '/' . 'items')) rmdir($upload_path . session_id() . '/' . 'items');
			if (is_dir($upload_path . session_id())) rmdir($upload_path . session_id());
		}
	}
}
?>