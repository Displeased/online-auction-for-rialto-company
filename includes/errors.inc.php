<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
 
if (!defined('InuAuctions')) exit();

// Errors handling functions
if (!function_exists('MySQLError'))
{
	function MySQLError($Q, $line = '', $page = '')
	{
		global 	$ERR_001, $system, $_SESSION;

		$SESSION_ERROR = $ERR_001 . "\t" . $Q . "\n\t" . mysql_error() . "\n\tpage:" . $page . " line:" . $line;
		if (!isset($_SESSION['SESSION_ERROR']) || !is_array($_SESSION['SESSION_ERROR']))
		{
			$_SESSION['SESSION_ERROR'] = array();
		}
		$_SESSION['SESSION_ERROR'][] = $SESSION_ERROR;
		$system->log('error', $SESSION_ERROR);
	}
}

if (!function_exists('uAuctionsErrorHandler'))
{
	function uAuctionsErrorHandler($errno, $errstr, $errfile, $errline)
	{
		global $system, $_SESSION;
		switch ($errno)
		{
			case E_USER_ERROR:
				$error = "<b>My ERROR</b> [$errno] $errstr\n";
				$error .= "  Fatal error on line $errline in file $errfile";
				$error .= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")\n";
				$error .= "Aborting...\n";
				break;
		
			case E_USER_WARNING:
				$error = "<b>My WARNING</b> [$errno] $errstr on $errfile line $errline\n";
				break;
		
			case E_USER_NOTICE:
				$error = "<b>My NOTICE</b> [$errno] $errstr on $errfile line $errline\n";
				break;
		
			default:
				$error = "Unknown error type: [$errno] $errstr on $errfile line $errline\n";
				break;
		}
		if (!isset($_SESSION['SESSION_ERROR']) || !is_array($_SESSION['SESSION_ERROR']))
		{
			$_SESSION['SESSION_ERROR'] = array();
		}
		$_SESSION['SESSION_ERROR'][] = $error;
		// log the error
		$system->log('error', $error);
		if ($errno == E_USER_ERROR)
			exit(1);
		return true;
	}
}
?>