<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


if (!defined('InuAuctions')) exit();

//language cookie name
$cookie_lang = $system->SETTINGS['cookie_name'] . '-USERLANGUAGE';

// Language management
if (isset($_GET['lan']) && !empty($_GET['lan']))
{
	$language = preg_replace("/[^a-zA-Z\s]/", '', $_GET['lan']);
	if ($user->logged_in)
	{
		$query = "UPDATE " . $DBPrefix . "users SET language = :lang WHERE id = :user_id";
		$params = array();
		$params[] = array(':lang', $language, 'str');
		$params[] = array(':user_id', $user->user_data['id'], 'int');
		$db->query($query, $params);
	}
}
elseif ($user->logged_in)
{
	$language = $user->user_data['language'];
}

elseif (isset($_COOKIE[$cookie_lang]))
{
	$language = preg_replace("/[^a-zA-Z\s]/", '', $_COOKIE[$cookie_lang]);
}
else
{
	$language = $system->SETTINGS['defaultlanguage'];
}

include $main_path . 'language/' . $language . '/messages.inc.php';

//find installed languages
$LANGUAGES = array();
if ($handle = opendir($main_path . 'language'))
{
	while (false !== ($file = readdir($handle)))
	{ 
		if (preg_match('/^([A-Z]{2})$/i', $file, $regs))
		{
			$LANGUAGES[$regs[1]] = $regs[1];
		}
	}
}
closedir($handle);

// check language exists
if (!in_array($language, $LANGUAGES))
{
	$language = $system->SETTINGS['defaultlanguage'];
}


if(isset($language))
{
	get_domain($system->SETTINGS['siteurl'], 1, 1, $cookie_lang, $language, time() + (86400 * 30));
}
else
{
	get_domain($system->SETTINGS['siteurl'], 1, 1, $cookie_lang, $system->SETTINGS['defaultlanguage'], time() + (86400 * 30));
}

function get_lang_img($string)
{
	global $system, $language;
	return $system->SETTINGS['siteurl'] . 'language/' . $language . '/images/' . $string;
}
?>
