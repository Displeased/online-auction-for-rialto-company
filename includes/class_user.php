<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


if (!defined('InuAuctions')) exit('Access denied');

class user
{
	var $user_data, $numbers, $logged_in;

	function user()
	{
		global $_SESSION, $system, $DBPrefix, $db, $security;

		$this->numbers = '1234567890';
		$this->logged_in = false;
		$this->can_sell = false;
		$this->can_buy = false;
		
		//fees settings
		$this->no_fees = false;
		$this->no_setup_fee = false;
		$this->no_excat_fee = false;
		$this->no_subtitle_fee = false;
		$this->no_relist_fee = false;
		$this->no_picture_fee = false;
		$this->no_hpfeat_fee = false;
		$this->no_hlitem_fee = false;
		$this->no_bolditem_fee = false;
		$this->no_rp_fee = false;
		$this->no_buyout_fee = false;
		$this->no_fp_fee = false;
		
		$this->user_data = array();
		
		if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_NUMBER']) && isset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']) && isset($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_PASS']))
		{
			$query = "SELECT * FROM " . $DBPrefix . "users WHERE password = :pass AND id = :login";
			$params = array();
			$params[] = array(':pass', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_PASS']), 'str');
			$params[] = array(':login', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']), 'str');
			$db->query($query, $params);

			if ($db->numrows() > 0)
			{
				$user_data = $db->result();
				if (strspn($user_data['password'], $user_data['hash']) == $security->decrypt($_SESSION[$system->SETTINGS['sessions_name']. '_LOGGED_NUMBER']))
				{
					$this->logged_in = true;
					$this->user_data = $user_data;
					
					if ($this->user_data['suspended '] != 7)
					{
						// check if user can sell or buy
						if (count($user_data['groups']) < 1) $user_data['groups'] = 0; // just in case
						
						$query = "SELECT can_sell, can_buy, no_fees, no_setup_fee, no_excat_fee, no_subtitle_fee, no_relist_fee, no_picture_fee, no_hpfeat_fee, no_hlitem_fee, no_bolditem_fee, no_rp_fee, no_buyout_fee, no_fp_fee FROM " . $DBPrefix . "groups WHERE id IN (" . $user_data['groups'] . ") AND (can_sell = :sell OR can_buy = :buy)";
						$params = array();
						$params[] = array(':sell', 1, 'int');
						$params[] = array(':buy', 1, 'int');
						$db->query($query, $params);

						while ($row = $db->result())
						{
							if ($row['can_sell'] == 1)
							{
								$this->can_sell = true;
							}
							if ($row['can_buy'] == 1)
							{
								$this->can_buy = true;
							}
							if ($row['no_fees'] == 1)
							{
								$this->no_fees = true;
							}
							if ($row['no_setup_fee'] == 1)
							{
								$this->no_setup_fee = true;
							}
							if ($row['no_excat_fee'] == 1)
							{
								$this->no_excat_fee = true;
							}
							if ($row['no_subtitle_fee'] == 1)
							{
								$this->no_subtitle_fee = true;
							}
							if ($row['no_relist_fee'] == 1)
							{
								$this->no_relist_fee = true;
							}
							if ($row['no_picture_fee'] == 1)
							{
								$this->no_picture_fee = true;
							}
							if ($row['no_hpfeat_fee'] == 1)
							{
								$this->no_hpfeat_fee = true;
							}
							if ($row['no_hlitem_fee'] == 1)
							{
								$this->no_hlitem_fee = true;
							}
							if ($row['no_bolditem_fee'] == 1)
							{
								$this->no_bolditem_fee = true;
							}
							if ($row['no_rp_fee'] == 1)
							{
								$this->no_rp_fee = true;
							}
							if ($row['no_buyout_fee'] == 1)
							{
								$this->no_buyout_fee = true;
							}
							if ($row['no_fp_fee'] == 1)
							{
								$this->no_fp_fee = true;
							}
						}
					}
				}
			}
			$this->check_balance();
		}
	}

	function is_logged_in()
	{
		global $security, $_SESSION, $_POST, $page;
		
		# Token should exist as soon as a user is logged in
		if(isset($_SESSION['csrftoken']))
		{
			if($_SERVER["REQUEST_METHOD"] == 'POST')
			{
				if(1 < count($_POST))		# More than 2 parameters in a POST (csrftoken + 1 more) => check
				{
					if($security->decrypt($_POST['csrftoken']) == $security->decrypt($_SESSION['csrftoken']))  # Checking if the POST and SESSION Tokens match
					{
						$valid_req = false;   # POST and SESSION Tokens match so pass
					}
					elseif($page == 'confirmed' || $page == 'refused')
					{
						//only used if the confirm button was clicked on the comfirm.php page to activate a account
						//this wll be skiped in all the pages
						$valid_req = false; 
					}
					else
					{
						$valid_req = true;  # POST and SESSION Tokens don't match so fail
					}
				}
			}

			if($valid_req) # There was a problem so display a error message
	       	{
	            global $MSG, $ERR_077; 
	                 
	            $_SESSION['msg_title'] = $MSG['936']; 
	            $_SESSION['msg_body'] = $ERR_077; 
	            header('location: message.php'); 
	        	exit; // kill the page 
	        }
	    }
		return $this->logged_in;
	}

	function check_balance()
	{
		global $system, $DBPrefix, $MSG, $db, $send_email;

		// check if user needs to be suspended
		if ($system->SETTINGS['fee_type'] == 1 && $this->logged_in && $system->SETTINGS['fee_disable_acc'] == 'y' && $this->user_data['payment_reminder_sent'] == 'n')
		{
			if ($system->SETTINGS['fee_max_debt'] <= (-1 * $this->user_data['balance']))
			{
				$query = "UPDATE " . $DBPrefix . "users SET suspended = :susp, payment_reminder_sent = :payment WHERE id = :user";
				$params = array();
				$params[] = array(':susp', 7, 'int');
				$params[] = array(':payment', 'y', 'bool');
				$params[] = array(':user', $this->user_data['id'], 'int');
				$db->query($query, $params);
	
				// send email
				$send_email->payment_reminder($this->user_data['name'], $this->user_data['balance'], $this->user_data['id'], $this->user_data['email']);
			}
		}
	}

	function is_valid_user($id) 
    { 
        global $system, $MSG, $ERR_025, $DBPrefix, $db;
         
        $query = "SELECT id FROM " . $DBPrefix . "users WHERE id = :user"; 
        $params = array();
		$params[] = array(':user', intval($id), 'int');
		$db->query($query, $params);
 
        if ($db->numrows('id') == 0) 
        { 
            $_SESSION['msg_title'] = $MSG['415']; 
            $_SESSION['msg_body'] = $ERR_025; 
            header('location: message.php'); 
            exit; 
        } 
    }
}
?>