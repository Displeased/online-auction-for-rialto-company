<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

if (!defined('InuAuctions')) exit();

function printLog($str)
{
	global $system;

	if (defined('LogCron') && LogCron == true)
	{
		$system->log('cron', $str);
	}
}

function constructCategories()
{
	global $DBPrefix, $system, $db;

	$query = "SELECT cat_id, parent_id, sub_counter, counter
			 FROM " . $DBPrefix . "categories ORDER BY :b";
	$params = array();
	$params[] = array(':b', 'cat_id', 'str');
	$db->query($query, $params);

	while ($row = $db->result())
	{
		$row['updated'] = false;
		$categories[$row['cat_id']] = $row;
	}
	return $categories;
}

function sendWatchEmails($id)
{
	global $DBPrefix, $system, $db;

	$query = "SELECT name, email, item_watch, id FROM " . $DBPrefix . "users WHERE item_watch LIKE '% " . $id . " %'";
	$params = array();
	$params[] = array(':auc_id', $k, 'int');
	$db->query($query, $params);

	while ($watchusers = $db->result())
	{
		$keys = explode(' ', $watchusers['item_watch']);
		// If keyword matches with opened auction title or/and desc send user a mail
		if (in_array($id, $keys))
		{
			// send message
			$send_email->watch_emails($Auction['title'], $id, $watchusers['name'], $watchusers['id'], $watchusers['email']);
		}
	}	
}

function sortFees()
{
	global $DBPrefix, $db, $system, $Winner, $Seller, $Auction, $buyer_emails;
	global $endauc_fee, $buyer_fee, $buyer_fee_type, $bf_paid, $ff_paid, $NOW;
	
	// insert buyer fees
	if(check_user_groups_fees($Winner['id'], true, false, false, false, false, false, false, false, false, false, true, false))
	{
		if ($system->SETTINGS['fee_type'] == 1 || $buyer_fee <= 0)
		{
			if ($buyer_fee_type == 'flat')
			{
				$fee_value = $buyer_fee;
			}
			else
			{
				$fee_value = ($buyer_fee / 100) * floatval($Auction['current_bid']);
			}
			// add balance & invoice
			$query = "UPDATE " . $DBPrefix . "users SET balance = balance - :f WHERE id = :i";
			$params = array();
			$params[] = array(':f', $buyer_fee, 'int');
			$params[] = array(':i', $Winner['id'], 'int');
			$db->query($query, $params);
	
			$query = "INSERT INTO " . $DBPrefix . "useraccounts (user_id, auc_id, date, buyer, total, paid) VALUES
					(" . $Winner['id'] . ", " . $Auction['id'] . ", " . $system->ctime . ", " . $buyer_fee . ", " . $buyer_fee . ", 1)";
			$db->direct_query($query);
		}
		else
		{
			$bf_paid = 0;
			$query = "UPDATE " . $DBPrefix . "users SET suspended = :s WHERE id = :i";
			$params = array();
			$params[] = array(':s', 6, 'int');
			$params[] = array(':i', $Winner['id'], 'int');
			$db->query($query, $params);
	
			$buyer_emails[] = array(
				'name' => $Winner['name'],
				'email' => $Winner['email'],
				'uid' => $Winner['id'],
				'id' => $Auction['id'],
				'title' => $Auction['title']
			);
		}
	}
	
	// insert final value fees
	if(check_user_groups_fees($Seller['id'], 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1))
	{
		$fee_value = 0;
		for ($i = 0; $i < count($endauc_fee); $i++)
		{
			if ($Auction['current_bid'] >= $endauc_fee[$i]['fee_from'] && $Auction['current_bid'] <= $endauc_fee[$i]['fee_to'])
			{
				if ($endauc_fee[$i]['fee_type'] == 'flat')
				{
					$fee_value = $endauc_fee[$i]['value'];
				}
				else
				{
					$fee_value = ($endauc_fee[$i]['value'] / 100) * $Auction['current_bid'];
				}
			}
		}
		
		if ($system->SETTINGS['fee_type'] == 1)
		{
			// add balance & invoice
			$query = "UPDATE " . $DBPrefix . "users SET balance = balance - :b WHERE id = :user_id";
			$params = array();
			$params[] = array(':b', $fee_value, 'int');
			$params[] = array(':user_id', $Seller['id'], 'int');
			$db->query($query, $params);
	
			$query = "INSERT INTO " . $DBPrefix . "useraccounts (user_id, auc_id, date, finalval, total, paid) VALUES
					(" . $Seller['id'] . ", " . $Auction['id'] . ", " . $system->ctime . ", " . $fee_value . ", " . $fee_value . ", 1)";
			$db->direct_query($query);
		}
		else
		{
			$ff_paid = 0;
			$query = "UPDATE " . $DBPrefix . "users SET suspended = 5 WHERE id = :user_id";
			$params = array();
			$params[] = array(':user_id', $Seller['id'], 'int');
			$db->query($query, $params);
	
			$seller_emails[] = array(
				'name' => $Seller['name'],
				'email' => $Seller['email'],
				'uid' => $Seller['id'],
				'id' => $Auction['id'],
				'title' => $Auction['title']
				);
		}	
	}
}
?>