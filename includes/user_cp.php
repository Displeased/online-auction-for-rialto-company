<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


if (!defined('InuAuctions')) exit();
if(!empty($ERR))
{
	$error = $ERR;
}
elseif(!empty($_SESSION['AVATAR_ERRORS']))
{
	$error = $_SESSION['AVATAR_ERRORS'];
}
elseif(!empty($_SESSION['FACEBOOK_LINKED']))
{
	$error = $_SESSION['FACEBOOK_LINKED'];
}

$template->assign_vars(array(
	'B_ISERROR' => ($error),
	'B_MENUTITLE' => (!empty($TMP_usmenutitle)),
	'UCP_ERROR' => (isset($error)) ? $error : '',
	'UCP_TITLE' => (isset($TMP_usmenutitle)) ? $TMP_usmenutitle : ''
));
if (isset($_SESSION['AVATAR_ERRORS'])) unset($_SESSION['AVATAR_ERRORS']);
if (isset($_SESSION['FACEBOOK_LINKED'])) unset($_SESSION['FACEBOOK_LINKED']);

?>