<?php
/*******************************************************************************
 *   copyright				: (C) 2008 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com/
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/


if (!defined('InuAuctions')) exit('Access denied');

function converter_call($post_data = true, $data = array())
{
	global $system;
	include $include_path . 'converter.inc.php';

	// get convertion data
	if ($post_data)
	{
		global $_REQUEST;
		$amount = $_REQUEST['amount'];
		$from = $_REQUEST['from'];
		$to = $_REQUEST['to'];
	}
	else
	{
		$amount = $data['amount'];
		$from = $data['from'];
		$to = $data['to'];
	}
	$amount = $system->input_money($amount);

	$CURRENCIES = CurrenciesList();

	$conversion = ConvertCurrency($from, $to, $amount);
	// construct string
	echo $amount . ' ' . $CURRENCIES[$from] . ' = ' . $system->print_money_nosymbol($conversion, true) . ' ' . $CURRENCIES[$to];
}

// reload the gallery table on upldgallery.php page
function getupldtable()
{
	global $_SESSION, $uploaded_path;
	foreach ($_SESSION['UPLOADED_PICTURES'] as $k => $v)
	{
		echo '<tr>
			<td>
				<img src="' . $uploaded_path . session_id() . '/' . $v . '" width="60" border="0">
			</td>
			<td width="46%">
				' . $v . '
			</td>
			<td align="center">
				<a href="?action=delete&img=' . $k . '"><IMG SRC="images/trash.gif" border="0"></a>
			</td>
			<td align="center">
				<a href="?action=makedefault&img=' . $v . '"><img src="images/' . (($v == $_SESSION['SELL_pict_url_temp']) ? 'selected.gif' : 'unselected.gif') . '" border="0"></a>
			</td>
		</tr>';
	}
}
?>