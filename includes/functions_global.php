<?php
/*******************************************************************************
 *   copyright				: (C) 2008 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com/
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/

if (!defined('InuAuctions')) exit('Access denied');

class global_class
{
	var $SETTINGS, $ADSENSE, $ctime, $tdiff, $COUNTERS;

	function global_class()
	{
		global $DBPrefix, $db, $MSG, $main_path;
				
		// Load settings
		$this->loadsettings();
		
		//load google adsense
		$this->loadadsense();
		
		//load counters db
		$this->counterdb();
		
		//setting up the time and date with dst
		if($this->SETTINGS['daylight_savings'] == 'y')
		{
			if(date('I') == 1)
			{
				$this->ctime = (time_correction($this->SETTINGS['timecorrection']) + 3600);
				$this->tdiff = (time_correction($this->SETTINGS['timecorrection'], 1) + 3600);
			}
			elseif(date('I') == 0)
			{
				$this->ctime = time_correction($this->SETTINGS['timecorrection']);
				$this->tdiff = time_correction($this->SETTINGS['timecorrection'], 1);
			}
		}
		elseif($this->SETTINGS['daylight_savings'] == 'n')
		{
			$this->ctime = time_correction($this->SETTINGS['timecorrection']);
			$this->tdiff = time_correction($this->SETTINGS['timecorrection'], 1);
		}		
		
		// Check ip
		if (!defined('ErrorPage') && !defined('InAdmin'))
		{
			$query = "SELECT id FROM " . $DBPrefix . "usersips WHERE ip = :user_ip AND action = 'deny'";
			$params = array();
			$params[] = array(':user_ip', $_SERVER['REMOTE_ADDR'], 'str');
			$db->query($query, $params);
			if ($db->numrows() > 0)
			{
				$_SESSION['msg_title'] = $MSG['2_0027'];
				$_SESSION['msg_body'] = $MSG['2_0026'];
				header('location: message.php');
				exit;
			}
		}
	}

	function loadsettings()
	{
		global $DBPrefix, $db;
		
		$query = "SELECT * FROM " . $DBPrefix . "settings";
		$db->direct_query($query);
		
		$this->SETTINGS = $db->result();
		$this->SETTINGS['gatways'] = array(
			'paypal' => 'PayPal',
			'authnet' => 'Authorize.net',
			'worldpay' => 'WorldPay',
			'skrill' => 'Skrill',
			'toocheckout' => '2Checkout',
			'bank' => 'Bank Transfer'
		);
				
		if ($this->SETTINGS['https'] == 'y')
		{
			$full_url = 'https://' . $this->SETTINGS['siteurl'];
		}
		else
		{
			$full_url = 'http://' . $this->SETTINGS['siteurl'];
		}
		$this->SETTINGS['siteurl'] = $full_url;
	}
	
	function counterdb()
	{
		global $DBPrefix, $db;
		
		//Google adsense banner
		$query = "SELECT * FROM " . $DBPrefix . "counters";
		$db->direct_query($query);
		$this->COUNTERS = $db->result();
	}

	function loadadsense()
	{
		global $DBPrefix, $db;
		
		//Google adsense banner
		$query = "SELECT * FROM " . $DBPrefix . "adsense";
		$db->direct_query($query);
		$this->ADSENSE = $db->result();
	}

	/* possible types cron, error, admin, user, mod */
	function log($type, $message, $user = 0, $action_id = 0)
	{
		global $DBPrefix, $db;
		$query = "INSERT INTO " . $DBPrefix . "logs (type, message, ip, action_id, user_id, timestamp) VALUES
				('" . $type . "', '" . $message . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . $action_id . "', '" . $user . "', '" . $this->ctime . "')";
		$db->direct_query($query);
	}

	function check_maintainance_mode()
	{
		global $DBPrefix, $user, $db;

		if (!isset($this->SETTINGS['MAINTAINANCE']))
		{
			$query = "SELECT * FROM " . $DBPrefix . "maintainance";
			$db->direct_query($query);
			if ($db->numrows() > 0)
			{
				$this->SETTINGS['MAINTAINANCE'] = $db->result();
			}
			else
			{
				return false;
			}
		}

		if ($this->SETTINGS['MAINTAINANCE']['active'] == 'y')
		{
			if ($user->logged_in && ($user->user_data['nick'] == $this->SETTINGS['MAINTAINANCE']['superuser'] || $user->user_data['id'] == $this->SETTINGS['MAINTAINANCE']['superuser']))
			{
				return false;
			}
			return true;
		}

		return false;
	}

	function cleanvars($i, $trim = false)
	{ 
		if ($trim)
			$i = trim($i);
		if (!get_magic_quotes_gpc())
			$i = addslashes($i);
		$i = rtrim($i);
		$look = array('&', '#', '<', '>', '"', '\'', '(', ')', '%');
		$safe = array('&amp;', '&#35;', '&lt;', '&gt;', '&quot;', '&#39;', '&#40;', '&#41;', '&#37;');
		$i = str_replace($look, $safe, $i);
		return $i;
	}

	function uncleanvars($i)
	{
		$look = array('&', '#', '<', '>', '"', '\'', '(', ')', '%');
		$safe = array('&amp;', '&#35;', '&lt;', '&gt;', '&quot;', '&#39;', '&#40;', '&#41;', '&#37;');
		$i = str_replace($safe, $look, $i);
		return $i;
	}

	function filter($txt)
	{
		global $DBPrefix, $db;
		
		$query = "SELECT * FROM " . $DBPrefix . "filterwords";
		$db->direct_query($query);
		while ($word = $db->result())
		{
			$txt = preg_replace('(' . $word['word'] . ')', '', $txt); //best to use str_ireplace but not avalible for PHP4
		}
		return $txt;
	}

	function move_file($from, $to, $removeorg = 1)
	{
		$upload_mode = (@ini_get('open_basedir') || @ini_get('safe_mode') || strtolower(@ini_get('safe_mode')) == 'on') ? 'move' : 'copy';
		switch ($upload_mode)
		{
			case 'copy':
				if (!@copy($from, $to))
				{
					if (!@move_uploaded_file($from, $to))
					{
						return false;
					}
				}
				if ($removeorg == 1)
					@unlink($from);
				break;

			case 'move':
				if (!@move_uploaded_file($from, $to))
				{
					if (!@copy($from, $to))
					{
						return false;
					}
				}
				if ($removeorg == 1)
					@unlink($from);
				break;
		}
		@chmod($to, 0644);
		return true;
	}

	//CURRENCY FUNCTIONS
	function input_money($str)
	{
		if (empty($str))
			return 0;

		$str = preg_replace("/[^0-9\.\,]/", '', $str);
		if ($this->SETTINGS['moneyformat'] == 1)
		{
			// Drop thousands separator
			$str = str_replace(',', '', $str);
		}
		elseif ($this->SETTINGS['moneyformat'] == 2)
		{
			// Drop thousands separator
			$str = str_replace('.', '', $str);

			// Change decimals separator
			$str = str_replace(',', '.', $str);
		}

		return floatval($str);
	}

	function CheckMoney($amount)
	{
		if ($this->SETTINGS['moneyformat'] == 1)
		{
			if (!preg_match('#^([0-9]+|[0-9]{1,3}(,[0-9]{3})*)(\.[0-9]{0,3})?$#', $amount))
				return false;
		}
		elseif ($this->SETTINGS['moneyformat'] == 2)
		{
			if (!preg_match('#^([0-9]+|[0-9]{1,3}(.[0-9]{3})*)(\,[0-9]{0,3})?$#', $amount))
				return false;
		}
		return true;
	}
	
	function print_money($str, $from_database = true, $link = true, $bold = true)
	{
		$str = $this->print_money_nosymbol($str, $from_database);

		if ($link)
		{
			$currency = '<a href="' . $this->SETTINGS['siteurl'] . 'converter.php?AMOUNT=' . $str . '" alt="converter" data-fancybox-type="iframe" class="converter">' . $this->SETTINGS['currency'] . '</a>';
		}
		else
		{
			$currency = $this->SETTINGS['currency'];
		}

		if ($bold)
		{
			$str = '<b>' . $str . '</b>';
		}

		if ($this->SETTINGS['moneysymbol'] == 2) // Symbol on the right
		{
			return $str . ' ' . $currency;
		}
		elseif ($this->SETTINGS['moneysymbol'] == 1) // Symbol on the left
		{
			return $currency . ' ' . $str;
		}
	}

	function print_money_nosymbol($str, $from_database = true)
	{
		$a = ($this->SETTINGS['moneyformat'] == 1) ? '.' : ',';
		
		$b = ($this->SETTINGS['moneyformat'] == 1) ? ',' : '.';
		if (!$from_database)
		{
			$str = $this->input_money($str, $from_database);
		}

		return number_format($str, $this->SETTINGS['moneydecimals'], $a, $b);
	}
}

//Encrypt and Decrypt data useing AES-256
class security 
{
	private static $salt;
	private static $public_key;
	private static $algorithm;
	private static $mode;
	
	public function security()
	{
		global $system;
		        
		# Private key that was generated from the AdminCP
		self::$salt = $system->SETTINGS['encryption_key'];
		
		self::$algorithm = MCRYPT_RIJNDAEL_128; # encryption algorithm
        self::$mode = MCRYPT_MODE_CBC; # encryption mode
	}
	
    # Encrypt a value using AES-256.
    public static function encrypt($plain, $hmacSalt = null) 
    {
    	# Generating a new public key and will only be used when encrypting data
		self::$public_key = self::genRandString(32);
		
		# Checking to see if the public key is 32 bytes
        if(self::_checkKey(self::$public_key, 'encrypt()') === false)
        {
        	return false;
        }
 
        if ($hmacSalt === null) {
            $hmacSalt = self::$salt;
        }
 
        $key = substr(hash('sha256', self::$public_key . $hmacSalt), 0, 32); # Generate the encryption and hmac key
 
        $ivSize = mcrypt_get_iv_size(self::$algorithm, self::$mode); # Returns the size of the IV belonging to a specific cipher/mode combination
        $iv = mcrypt_create_iv($ivSize, MCRYPT_DEV_URANDOM); # Creates an initialization vector (IV) from a random source
        $ciphertext = $iv . mcrypt_encrypt(self::$algorithm, $key, $plain, self::$mode, $iv); # Encrypts plaintext with given parameters
        $hmac = hash_hmac('sha256', $ciphertext, $key); # Generate a keyed hash value using the HMAC method
        # We are adding the base64_encode and rawurlencode so it can passed in a URL and adding the public key that was used
        # Normaly we would pass the public key separated form the encrypted data then passed it with the encrypted data to decrypt the data
        # By adding the public key with the encrypted data it makes it lot easier to pass it back and forth with less coding
        # By using base64_encode it putting 1 more extra security on top of what was encrypted and public key and it helps us be able to pass it in URL
        $encryption = base64_encode(rawurlencode($hmac . $ciphertext . '=K=' . base64_encode(self::$public_key)));
        $encrypted = str_replace('+','^',$encryption); # We are replacing the + with ^ because the + some times gives problems in URL like downloading digital item
       # Sending the encrypted data back and it will be a really long string that is something like this (Good luck trying to hack that string)
       # MDdlOWNmZjk5ZTRmNzg5NGU2MzljMmEzMTllMzgyMjIzOThkYjFlZGJlYmZkYTFhYTJjY2ExMDBlYzE3ZDZiMCVFNiVBM1NEJTFCJUUxJTA4eSVCNCU5NCU4N2UlNDAlNUMlRTIlOENLVSVFQSUxRSVDNCU5RiVGOCVEMSU4Mkt4JTk4JUJCZSU5MiU4MSUzREslM0RVMk5zVTFGRWRqWnJWV3A2VFdsT2MwVldZa2wxU21WNFNVdHdTbVpGZURRJTNE
        return $encrypted;
    }
 
    # Check key
    protected static function _checkKey($key, $mode) 
    {
        if (strlen($key) < 32) {
            return false;
        }
    }
 
    # Decrypt a value using AES-256.
    public static function decrypt($encrypted, $hmacSalt = null)
    {
    	$fixCode = str_replace('^','+',$encrypted); # We are replacing back the + that we change to ^ so that the data can be decrypted
    	$decode = base64_decode($fixCode); # decoding the base64 so that we can start decrypting the data
    	$urldecode = rawurldecode($decode); # Decoding the rawurlcode so it can be read and used
    	$split = explode('=K=', $urldecode); # Splitting the encrypting data and public key from the encrypted data so we can use them
    	$cipher = $split[0]; # Getting the encrypted data
    	# The public key was encoded with base64 so we need to decode the public key
    	self::$public_key = base64_decode($split[1]); # Getting the public key that was stored with the encrypted data so that it can be decrypted with the private key
    	
    	//// Now do all the decrypting ////
    	# Checking to see if the public key is 32 bytes
        if(self::_checkKey(self::$public_key, 'decrypt()') === false)
        {
        	return false;
        }
        
        if (empty($cipher)) {
            return false;
        }
        if ($hmacSalt === null) {
            $hmacSalt = self::$salt;
        }
 
        $key = substr(hash('sha256', self::$public_key . $hmacSalt), 0, 32); # Generate the encryption and hmac key.
 
        # Split out hmac for comparison
        $hmac = substr($cipher, 0, 64);
        $cipher = substr($cipher, 64);
 
        $compareHmac = hash_hmac('sha256', $cipher, $key);
        if ($hmac !== $compareHmac) {
            return false;
        }
 
        $ivSize = mcrypt_get_iv_size(self::$algorithm, self::$mode); # Returns the size of the IV belonging to a specific cipher/mode combination
        $iv = substr($cipher, 0, $ivSize);
        $cipher = substr($cipher, $ivSize);
        $plain = mcrypt_decrypt(self::$algorithm, $key, $cipher, self::$mode, $iv);
        return rtrim($plain, "\0");
    }
        
    #Get Random String - Usefull for public key
    public static function genRandString($length = 0) 
    {
        $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $str = '';
        $count = strlen($charset);
        while ($length-- > 0) {
            $str .= $charset[mt_rand(0, $count-1)];
        }
		return $str;
    }
}

// global functions
function _gmmktime($hr, $min, $sec, $mon, $day, $year)
{
	global $system;
	if ($system->SETTINGS['datesformat'] != 'USA')
	{
		$mon_ = $mon;
		$mon = $day;
		$day = $mon_;
	}

	if (@phpversion() >= '5.1.0')
	{
		return gmmktime($hr, $min, $sec, $mon, $day, $year); // is_dst is deprecated
	}

    if (gmmktime(0,0,0,6,1,2008, 0) == 1212282000)
    {
        //Seems to be running PHP (like 4.3.11).
        //At least if current local timezone is Europe/Stockholm with DST in effect, skipping the ,0 helps:
        return gmmktime($hr, $min, $sec, $mon, $day, $year); //without is_dst-parameter at the end
    }
    return gmmktime($hr, $min, $sec, $mon, $day, $year, 0);
}

function load_counters()
{
	global $system, $DBPrefix, $MSG, $_COOKIE, $user, $db;
	
	$counters = '';
	//counting all activate auctions
	if ($system->SETTINGS['counter_sold_items'] == 'y')
	{
		$counters .= '<b>' . $system->COUNTERS['items_sold'] . '</b> ' . strtoupper($MSG['3500_1015548']) . ' | ';
	}
	//counting all activate auctions
	if ($system->SETTINGS['counter_auctions'] == 'y')
	{
		$count_auctions = $system->COUNTERS['auctions'] - $system->COUNTERS['suspendedauctions'];
		$counters .= '<b>' . $count_auctions . '</b> ' . strtoupper($MSG['232']) . '| ';
	}
	//counting all registered account
	if ($system->SETTINGS['counter_users'] == 'y')
	{
		$counters .= '<b>' . $system->COUNTERS['users'] . '</b> ' . strtoupper($MSG['231']) . ' | ';
	}
	//counting all users/guests
	if ($system->SETTINGS['counter_online'] == 'y')
	{
		$cookie_name = $system->SETTINGS['cookie_name'] . '-ONLINE';
		if (!$user->logged_in)
		{		
			if(isset($_COOKIE[$cookie_name]))
			{
				$s = alphanumeric($_COOKIE[$system->SETTINGS['cookie_name'] . '-ONLINE']);
				$cookie_time = time() + 86400;
				get_domain($system->SETTINGS['siteurl'], 1, 1, $cookie_name, $s, $cookie_time);
				$g = $s;
			}
			else
			{
				$s = md5(rand(0, 99) . session_id());
				$cookie_time = time() + 86400;
				get_domain($system->SETTINGS['siteurl'], 1, 1, $cookie_name, $s, $cookie_time);
			}
		}
		else
		{
			$s = 'uId-' . $user->user_data['id'];
			//deleting the cookie we dont need it after logged in
			if(isset($_COOKIE[$system->SETTINGS['cookie_name'] . '-ONLINE']))
			{
				$cookie_hash = '';
				$cookie_time = time() - 86450;
				get_domain($system->SETTINGS['siteurl'], 1, 1, $cookie_name, $cookie_hash, $cookie_time);
			}
		}
		$u = isset($g) ? $g : '';
		$query = "SELECT ID FROM " . $DBPrefix . "online WHERE SESSION = :user";
		$params = array();
		$params[] = array(':user', $s, 'str');
		$db->query($query, $params);

		if ($db->numrows('ID') == 0)
		{
			$query = "INSERT INTO " . $DBPrefix . "online (SESSION, time) VALUES (:user, :timer)";
			$params = array();
			$params[] = array(':user', $s, 'str');
			$params[] = array(':timer', $system->ctime, 'int');
			$db->query($query, $params);
		}
		else
		{
			$oID = $db->result('ID');
			$query = "UPDATE " . $DBPrefix . "online SET time = :timer WHERE ID = :online_id";
			$params = array();
			$params[] = array(':timer', $system->ctime, 'int');
			$params[] = array(':online_id', $oID, 'int');
			$db->query($query, $params);
		}
		$query = "DELETE from " . $DBPrefix . "online WHERE time <= :timer";
		$params = array();
		$params[] = array(':timer', $system->ctime - 300, 'int');
		$db->query($query, $params);
		
		$query = "SELECT id FROM " . $DBPrefix . "online";
		$db->direct_query($query);
		
		$counters .= '<b>' . $db->numrows('id') . '</b> ' . $MSG['2__0064'] . ' | ';
	}
	
	//counting users logged in
	if ($system->SETTINGS['counter_users_online'] == 'y')
	{
		if($user->logged_in)
		{
			$query = "SELECT hide_online, id FROM " . $DBPrefix . "users WHERE id = :user_id"; 
			$params = array();
			$params[] = array(':user_id', $user->user_data['id'], 'int');
			$db->query($query, $params);	
			$user_time = $db->result();
			
			if($user_time['hide_online'] == 'y') 
			{
				$query = "UPDATE " . $DBPrefix . "users SET is_online = :loggedout WHERE id = :user_id";
			    $params = array();
			    $params[] = array(':loggedout', $system->ctime - 320, 'int');
				$params[] = array(':user_id', $user_time['id'], 'int');
				$db->query($query, $params);
			}
			else
			{
				$query = "UPDATE " . $DBPrefix . "users SET is_online = :time WHERE id = :user_id";
				$params = array();
				$params[] = array(':time', $system->ctime + 300, 'int');
				$params[] = array(':user_id', $user_time['id'], 'int');
				$db->query($query, $params);       
			}
		}
		
		$query = "SELECT id FROM " . $DBPrefix . "users WHERE is_online > :user_timer AND hide_online = :hide"; 
		$params = array();
		$params[] = array(':hide', 'n', 'str');
		$params[] = array(':user_timer', $system->ctime, 'int');
		$db->query($query, $params);
		
		$counters .= '<b>' . $db->numrows('id') . '</b> ' . $MSG['2__10059'] . ' | ';
	}	
	
	// Display current Date/Time fotmat
	$NOW = $system->ctime;
	$mth = 'MON_0' . gmdate('m', $NOW);
	if($system->SETTINGS['datesformat'] == 'EUR')
	{
		$setdate =  gmdate('j', $NOW) . ' ' . $MSG[$mth] . ' ' . gmdate('Y', $NOW);
	}
	else
	{
		$setdate = $MSG[$mth] . ' ' . gmdate('j,Y', $NOW);
	}
	$counters .= $setdate . ' <span id="servertime">' . date('H:i:s', $NOW) . '</span>';
	return $counters;
}

function _in_array($needle, $haystack)
{
	$needle = "$needle"; //important turns integers into strings
	foreach ($haystack as $val)
	{
		if ($val == $needle)
			return true;
	}
	return false;
}

// strip none alpha-numeric characters
function alphanumeric($str)
{
	$str = preg_replace("/[^a-zA-Z0-9\s]/", '', $str);
	return $str;
}

// this is a stupid way of doing things these need to be changed to bools
function ynbool($str)
{
	$str = preg_replace("/[^yn]/", '', $str);
	return $str;
}


function CheckAge($day, $month, $year) // check if the users > 18
{
	$NOW_year = gmdate('Y');
	$NOW_month = gmdate('m');
	$NOW_day = gmdate('d');

	if (($NOW_year - $year) > 18)
	{
		return 1;
	}
	elseif ((($NOW_year - $year) == 18) && ($NOW_month > $month))
	{
		return 1;
	}
	elseif ((($NOW_year - $year) == 18) && ($NOW_month == $month) && ($NOW_day >= $day))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

function get_hash()
{
	$string = '0123456789abcdefghijklmnopqrstuvyxz';
	$hash = '';
	for ($i = 0; $i < 5; $i++)
	{
		$rand = rand(0, (34 - $i));
		$hash .= $string[$rand];
		$string = str_replace($string[$rand], '', $string);
	}
	return $hash;
}

//Check to see if the user email domain is blacklisted
function emailblacklist($email) 
{
	//Split the email and make a array
	$email_split = explode('@', $email);
	// Set the api key so that we can do a domain check
	$api_key = 'vixgkGHRV1YKJnCXewX9w15x60cJsUwiJ';
	//Set the data that is getting sent
	$content = load_file_from_url("https://u-auctions.com/api/check.php?action=api-check&email=" . $email_split[1] . "&api=" . $api_key);
	//Decode the json data to get the value
	$IsSpammer = json_decode($content, true); 
	//Return the value to the if() statemant 
	//that is checking the user email domain
	return $IsSpammer['check'];
}

function arrangeCountries($data, $default, $countries)
{
	global $system;
	
	if($default == true && is_array($countries))
	{
		foreach($countries as $k => $v)
		{
			if($system->SETTINGS['defaultcountry'] == $k)
			{
				return $v;
			}
		}
	}
}
function generateSelect($name = '', $options = array())
{
	global $selectsetting;
	$html = '<select name="' . $name . '">';
	foreach ($options as $option => $value)
	{
		if ($selectsetting == $option)
		{
			$html .= '<option value=' . $option . ' selected>' . $value . '</option>';
		}
		else
		{
			$html .= '<option value=' . $option . '>' . $value . '</option>';
		}
	}
	$html .= '</select>';
	return $html;
}

function checkMissing()
{
	global $missing;
	foreach ($missing as $value)
	{
		if ($value)
		{
			return true;
		}
	}
	return false;
}

function ShowFlags()
{
	global $system, $LANGUAGES;
	$counter = 0;
	$flags = '';
	foreach ($LANGUAGES as $lang => $value)
	{
		if ($counter > 7)
		{
			$flags .= '<br>';
			$counter = 0;
		}
		$flags .= '<a href="login.php?lan=' . $lang . '"><img vspace="2" hspace="2" src="' . $system->SETTINGS['siteurl'] . 'inc/flags/' . $lang . '.gif" border="0" alt="' . $lang . '"></a>';
		$counter++;
	}
	return $flags;
}

function ShowingFlags()
{
	global $system, $LANGUAGES;
	$counter = 0;
	$flags = '';
	foreach ($LANGUAGES as $lang => $value)
	{
		$flags .= '<a href="' . $system->SETTINGS['siteurl'] . 'home-lan=' . $lang . '"><img vspace="2" hspace="2" src="' . $system->SETTINGS['siteurl'] . 'inc/flags/' . $lang . '.gif" border="0" alt="' . $lang . '"></a>';
		$counter++;
	}
	return $flags;
}

function formatSizeUnits($bytes)
{
	if ($bytes >= 1073741824)
    {
    	$bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
    	$bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
    	$bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
    	$bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
    	$bytes = $bytes . ' byte';
    }
    else
    {
    	$bytes = '0 bytes';
    }
    return $bytes;
}

function load_file_from_url($url)
{
	if(false !== ($str = file_get_contents($url)))
	{
		return $str; 
	}
	elseif(($handle = @fopen($url, 'r')) !== false)
	{
		$str = fread($handle, 5);
		if(false !== $str)
		{
			fclose($handle);
			return $str;
		}
	}
	elseif (function_exists('curl_init') && function_exists('curl_setopt')
	&& function_exists('curl_exec') && function_exists('curl_close'))
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_REFERER, $system->SETTINGS['siteurl']);
		$str = curl_exec($curl);
		curl_close($curl);
		return $str;
	}
	return false;
}

//still not finish and needs the cookie path fixed for sub-Directory so the cookie is not only using / for all sub-Directory
//and should be /test/ = sub-Directory folder
function get_domain($domain, $strip = 0, $cookie = 0, $cookie_name = false, $cookie_value = false, $cookie_expire = false)
{
	global $system;
	
	$stipped_domain = array_filter(explode('//',$domain)); // strip the domain from the http and https
	$stipped_slash = array_filter(explode('/',$stipped_domain[1])); // strip the ending slash's
	
	// strip all slash's and is only used in making cookies so don't need to loop the domain just need the main domain name
	if($strip == 1) 
	{
		if(!isset($stipped_slash[0])) //only runs if the ending slash is missing and the array is empty
		{
			$build_domain = $stipped_domain[1];
		}
		else //only runs if the ending slash is found
		{
			$build_domain = $stipped_slash[0];
		}
		if($cookie == 1)
		{
			setcookie($cookie_name, $cookie_value, $cookie_expire, '/', $build_domain, $system->SETTINGS['https'] == 'y' ? true : false);	
		}
	}
	else //only runs if we are not stripping the ending slash in the URL or making cookies
	{
		foreach($stipped_slash as $k => $v)
		{
			if(!isset($v)) //only runs if the ending slash is missing and readds the ending slash
			{
				$build_domain .= $stipped_domain[1] . '/';
			}
			else //only runs if the ending slash was found
			{
				$build_domain .= $v  . '/';
			}
		}
	}
	return $build_domain; //the domain was rebuilt and display the result
}


//this only runs if it is adding fees to a user that is not logged in
function check_user_groups_fees($user_id, $no_fees, $no_setup_fee, $no_excat_fee, $no_subtitle_fee, $no_relist_fee, $no_picture_fee, $no_hpfeat_fee, $no_hlitem_fee, $no_bolditem_fee, $no_rp_fee, $no_buyout_fee, $no_fp_fee) 
{ 
	global $DBPrefix, $db;
	
	//this will add the fees if the $no_fees salt is not false but if 1 
	//$no_fees salt is false then no fees will be added to to that fee it is checking for
	$no_fees = true; 
		
	// pulling the user groups from the user it is adding fees to
	$query = "SELECT groups " . $DBPrefix . "users WHERE id = :id";
	$params = array();
	$params[] = array(':id', $user_id, 'int');
	$db->direct_query($query, $params);
	$groups = $db->result('groups');
	
	//checking the groups to see what fee to add 
	$query = "SELECT no_fees, , no_excat_fee, no_subtitle_fee, no_relist_fee, no_picture_fee, no_hpfeat_fee, no_hlitem_fee, no_bolditem_fee, no_rp_fee, no_buyout_fee, no_fp_fee FROM " . $DBPrefix . "groups WHERE id IN (" . $groups . ")";
	$db->query($query, $params); 
	while ($row = $db->result())
	{
		if ($row['no_fees'] == 1 && $no_fees == 1) //checking to see if the user don't hves to pay no fees
		{
			$no_fees = false;
		}
		if($no_fees) //will run if the $no_fees is true
		{
			if ($row['no_setup_fee'] == 1 && $no_setup_fee == 1) //checking to see if the user has to pay Setup fee
			{
				$no_fees = false;
			}
			if ($row['no_excat_fee'] == 1 && $no_excat_fee == 1) //checking to see if the user has to pay Extra Category fees
			{
				$no_fees = false;
			}
			if ($row['no_subtitle_fee'] == 1 && $no_subtitle_fee == 1) //checking to see if the user has to pay Subtitle fee
			{
				$no_fees = false;
			}
			if ($row['no_relist_fee'] == 1 && $no_relist_fee == 1) //checking to see if the user has to pay Relist fee
			{
				$no_fees = false;
			}
			if ($row['no_picture_fee'] == 1 && $no_picture_fee == 1) //checking to see if the user has to pay Picture fee
			{
				$no_fees = false;
			}
			if ($row['no_hpfeat_fee'] == 1 && $no_hpfeat_fee == 1) //checking to see if the user has to pay Featured fee
			{
				$no_fees = false;
			}
			if ($row['no_hlitem_fee'] == 1 && $no_hpfeat_fee == 1) //checking to see if the user has to pay Highlighted fee
			{
				$no_fees = false;
			}
			if ($row['no_bolditem_fee'] == 1 && $no_bolditem_fee == 1) //checking to see if the user has to pay Bold fee
			{
				$no_fees = false;
			}
			if ($row['no_rp_fee'] == 1 && $no_rp_fee == 1) //checking to see if the user has to pay Reserve fee
			{
				$no_fees = false;
			}
			if ($row['no_buyout_fee'] == 1 && $no_buyout_fee == 1) //checking to see if the user has to pay Buyer fee
			{
				$no_fees = false;
			}
			if ($row['no_fp_fee'] == 1 && $no_fp_fee == 1) //checking to see if the user has to pay Final Price fee
			{
				$no_fees = false;
			}
		}
	}
	return $no_fees;
}


?>