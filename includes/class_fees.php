<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


if (!defined('InuAuctions')) exit('Access denied');

class fees
{
	var $ASCII_RANGE;
	var $data;
	var $fee_types;

	function fees()
	{
		$this->ASCII_RANGE = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$this->fee_types = $this->get_fee_types();
	}

	function get_fee_types()
	{
		global $system, $DBPrefix, $db;
		$query = "SELECT type FROM " . $DBPrefix . "fees GROUP BY type";
		$db->direct_query($query);
		$fee_types = array();
		while ($row = $db->result())
		{
			$fee_types[] = $row;
		}
		return $fee_types;
	}

	function add_to_account($text, $type, $amount)
	{
		global $system, $DBPrefix, $user, $db;

		$date_values = date('z|W|m|Y', $system->ctime);
		$date_values = explode('|', $date_values);
		$query = "INSERT INTO " . $DBPrefix . "accounts VALUES (NULL, :user_nick, :user_name, :user_text, :user_type, :user_time, :user_amount, " . $date_values[0] . ", " . $date_values[1] . ", " . $date_values[2] . ", " . $date_values[3] . ")";
		$params = array();
		$params[] = array(':user_nick', $user->user_data['nick'], 'str');
		$params[] = array(':user_name', $user->user_data['name'], 'str');
		$params[] = array(':user_text', $text, 'str');
		$params[] = array(':user_type', $type, 'str');
		$params[] = array(':user_time', $system->ctime, 'int');
		$params[] = array(':user_amount', $amount, 'int');
		$db->query($query, $params);
	}

	function hmac($key, $data)
	{
		// RFC 2104 HMAC implementation for php.
		// Creates an md5 HMAC.
		// Eliminates the need to install mhash to compute a HMAC
		// Hacked by Lance Rushing

		$b = 64; // byte length for md5
		if (strlen($key) > $b)
		{
			$key = pack("H*", md5($key));
		}
		$key  = str_pad($key, $b, chr(0x00));
		$ipad = str_pad('', $b, chr(0x36));
		$opad = str_pad('', $b, chr(0x5c));
		$k_ipad = $key ^ $ipad ;
		$k_opad = $key ^ $opad;

		return md5($k_opad  . pack("H*", md5($k_ipad . $data)));
	}

	function paypal_validate()
	{
		global $system;
		
		$sandbox = $system->SETTINGS['paypal_sandbox'] == 'y' ? true : false;
		$https = $system->SETTINGS['https'] == 'y' ? true : false; 
		// we ensure that the txn_id (transaction ID) contains only ASCII chars...
		$pos = strspn($this->data['txn_id'], $this->ASCII_RANGE);
		$len = strlen($this->data['txn_id']);

		if ($pos != $len)
		{
			return;
		}

		//validate payment
		$req = 'cmd=_notify-validate';
		foreach ($this->data as $key => $value)
		{
			// Handle escape characters, which depends on setting of magic quotes  
			if(get_magic_quotes_gpc())
				$value = urlencode(stripslashes($value));
			else
				$value = urlencode($value);
			$req .= '&' . $key . '=' . $value;
		}

		// Post back to PayPal system to validate
		$header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		
		if ($sandbox == true)
		{
			$header .= "Host: www.sandbox.paypal.com\r\n";
		}
		else
		{
			$header .= "Host: www.paypal.com\r\n";
		}
		
		$header .= "Content-Length: " . strlen($req) . "\r\n";
		$header .= "Connection: close\r\n\r\n";  
		
		if ($sandbox == true)
		{
			if ($https == true)
			{
				// connect via SSL
				$fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
			}
			else
			{
				// connect via HTTP
				$fp = fsockopen ('www.sandbox.paypal.com', 80, $errno, $errstr, 30);
			}			
		}
		else
		{
			if ($https == true)
			{
				// connect via SSL
				$fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);
			}
			else
			{
				// connect via HTTP
				$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
			}
		}
		if (!$fp)
		{
			$error_output = $errstr . ' (' . $errno . ')';
		}
		else
		{
			// Assign posted variables to local variables
			$payment_status = $this->data['payment_status'];
			$payment_amount = floatval ($this->data['mc_gross']);
			list($custom_id, $fee_type) = explode($system->SETTINGS['custom_code'], $this->data['custom']);
			fputs ($fp, $header . $req);

			while (!feof($fp))
			{
				$resl = trim(fgets ($fp, 1024));

				if (strcmp ($resl, 'VERIFIED') == 0)
				{
					// We can do various checks to make sure nothing is wrong and   
					// that txn_id has not been previously processed
					if ($payment_status == 'Completed')
					{
						// everything seems to be OK
						$this->callback_process($custom_id, $fee_type, $payment_amount);
					}
				}
				elseif (strcmp ($resl, 'INVALID') == 0)
				{
					// payment failed
					$redirect_url = $system->SETTINGS['siteurl'] . 'validate.php?fail';
					header('location: '. $redirect_url);
					exit;
				}
			}
			fclose ($fp);
		}
	}
	 	
	function authnet_validate()
	{
		global $system;

		$payment_amount = floatval ($this->data['x_amount']);
		list($custom_id, $fee_type) = explode($system->SETTINGS['custom_code'], $this->data['custom']);

		if ($this->data['x_response_code'] == 1)
		{
			$this->callback_process($custom_id, $fee_type, $payment_amount);
		}
		else
		{
			$redirect_url = $system->SETTINGS['siteurl'] . 'validate.php?fail';
			header('location: '. $redirect_url);
			exit;
		}
	}

	function worldpay_validate()
	{
		global $system;

		$payment_amount = floatval ($this->data['amount']);
		list($custom_id, $fee_type) = explode($system->SETTINGS['custom_code'],$this->data['cartId']);

		if ($this->data['transStatus'] == 'Y')
		{
			$this->callback_process($custom_id, $fee_type, $payment_amount);
		}
		else
		{
			$redirect_url = $system->SETTINGS['siteurl'] . 'validate.php?fail';
			header('location: '. $redirect_url);
			exit;
		}
	}

	function skrill_validate()
	{
		global $system;

		$payment_amount = floatval ($this->data['amount']);
		list($custom_id, $fee_type) = explode($system->SETTINGS['custom_code'],$this->data['trans_id']);

		if ($this->data['status'] == 2)
		{
			$this->callback_process($custom_id, $fee_type, $payment_amount);
		}
		else
		{
			$redirect_url = $system->SETTINGS['siteurl'] . 'validate.php?fail';
			header('location: '. $redirect_url);
			exit;
		}
	}

	function toocheckout_validate()
	{
		global $system;

		$payment_amount = floatval ($this->data['total']);
		list($custom_id, $fee_type) = explode($system->SETTINGS['custom_code'],$this->data['cart_order_id']);

		if ($this->data['cart_order_id'] != '' && $this->data['credit_card_processed'] == 'Y')
		{
			$this->callback_process($custom_id, $fee_type, $payment_amount);
		}
		else
		{
			$redirect_url = $system->SETTINGS['siteurl'] . 'validate.php?fail';
			header('location: '. $redirect_url);
			exit;
		}
	}

	function callback_process($custom_id, $fee_type, $payment_amount, $currency = NULL)
	{
		global $system, $DBPrefix, $db, $send_email, $security;
		
		switch ($fee_type)
		{
			case 1: // add to account balance
				$addquery = '';
				if ($system->SETTINGS['fee_disable_acc'] == 'y')
				{
					$query = "SELECT suspended, balance FROM " . $DBPrefix . "users WHERE id = :get_id";
					$params = array();
					$params[] = array(':get_id', $custom_id, 'int');
					$db->query($query, $params);
					$data = $db->result();
					// reable user account if it was disabled
					$params = array();
					if ($data['suspended'] == 7 && ($data['balance'] + $payment_amount) >= 0)
					{
						$addquery = ', suspended = :suspe, payment_reminder_sent = :payment ';
						$params[] = array(':suspe', 0, 'int');
						$params[] = array(':payment', 'n', 'bool');
					}
				}
				$query = "UPDATE " . $DBPrefix . "users SET balance = balance + :payment" . $addquery . " WHERE id = :user_id";
				$params[] = array(':payment', $payment_amount, 'float');
				$params[] = array(':user_id', $custom_id, 'int');
				$db->query($query, $params);

				// add invoice
				$query = "INSERT INTO " . $DBPrefix . "useraccounts (user_id, date, balance, total, paid) VALUES
						(:user_id, :time_stamp, :payment, :extra_payment, 1)";
				$params = array();
				$params[] = array(':user_id', $custom_id, 'int');
				$params[] = array(':time_stamp', $system->ctime, 'int');
				$params[] = array(':payment', $payment_amount, 'float');
				$params[] = array(':extra_payment', $payment_amount, 'float');
				$db->query($query, $params);
			break;
			case 2: // pay for an item
				$query = "UPDATE " . $DBPrefix . "winners SET paid = 1, is_counted = 'y' WHERE id = :get_id";
				$params = array();
				$params[] = array(':get_id', $custom_id, 'int');
				$db->query($query, $params);
				
				//update the sold items counter
				$query = "UPDATE " . $DBPrefix . "counters SET items_sold = :sold";
				$params = array();
				$params[] = array(':sold', $system->COUNTERS['items_sold'] + 1, 'int');
				$db->query($query, $params);
			break;
			case 3: // pay signup fee (live mode)
				$query = "UPDATE " . $DBPrefix . "users SET suspended = 0 WHERE id = :get_id";
				$params = array();
				$params[] = array(':get_id', $custom_id, 'int');
				$db->query($query, $params);

				// add invoice
				$query = "INSERT INTO " . $DBPrefix . "useraccounts (user_id, date, signup, total, paid) VALUES
						(:get_id, :time_stamp, :payment, :extra_payment, 1)";
				$params = array();
				$params[] = array(':get_id', $custom_id, 'int');
				$params[] = array(':time_stamp', $system->ctime, 'int');
				$params[] = array(':payment', $payment_amount, 'float');
				$params[] = array(':extra_payment', $payment_amount, 'float');
				$db->query($query, $params);
			break;
			case 4: // pay auction fee (live mode)
				global $user, $MSG;
				$catscontrol = new MPTTcategories();

				$query = "SELECT auc_id FROM " . $DBPrefix . "useraccounts WHERE useracc_id = :useraccounts_id";
				$params = array();
				$params[] = array(':useraccounts_id', $custom_id, 'int');
				$db->query($query, $params);
				$auc_id = $db->result('auc_id');
				
				$query = "UPDATE " . $DBPrefix . "auctions SET suspended = 0 WHERE id = :auctions_id";
				$params = array();
				$params[] = array(':auctions_id', $auc_id, 'int');
				$db->query($query, $params);

				$query = "UPDATE " . $DBPrefix . "useraccounts SET paid = 1 WHERE auc_id = :useraccounts_id AND setup > 0";
				$params = array();
				$params[] = array(':useraccounts_id', $auc_id, 'int');
				$db->query($query, $params);

				$query = "UPDATE " . $DBPrefix . "counters SET auctions = auctions + :add_auction";
				$params = array();
				$params[] = array(':add_auction', 1, 'int');
				$db->query($query, $params);

				$query = "UPDATE " . $DBPrefix . "useraccounts SET paid = 1 WHERE useracc_id = :useraccounts_id";
				$params = array();
				$params[] = array(':useraccounts_id', $custom_id, 'int');
				$db->query($query, $params);

				$query = "SELECT id, category, title, minimum_bid, pict_url, buy_now, reserve_price, auction_type, ends
					FROM " . $DBPrefix . "auctions WHERE id = :auction_id";
				$params = array();
				$params[] = array(':auction_id', $auc_id, 'int');
				$db->query($query, $params);	
				$auc_data = $db->result();

				// auction data
				$auction_id = $auc_data['id'];
				$title = $auc_data['title'];
				$atype = $auc_data['auction_type'];
				$pict_url = $auc_data['pict_url'];
				$minimum_bid = $auc_data['minimum_bid'];
				$reserve_price = $auc_data['reserve_price'];
				$buy_now_price = $auc_data['buy_now'];
				$a_ends = $auc_data['ends'];

				if ($user->user_data['startemailmode'] == 'yes')
				{
					$send_email->confirmation($auction_id, $title, $atype, $pict_url, $minimum_bid, $reserve_price, $buy_now_price, $a_ends);
				}

				// update recursive categories
				$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :cats_id";
				$params = array();
				$params[] = array(':cats_id', $auc_data['category'], 'int');
				$db->query($query, $params);	
				$parent_node = $db->result();
				$crumbs = $catscontrol->get_bread_crumbs($parent_node['left_id'], $parent_node['right_id']);

				for ($i = 0; $i < count($crumbs); $i++)
				{
					$query = "UPDATE " . $DBPrefix . "categories SET sub_counter = sub_counter + 1 WHERE cat_id = :cats_id";
					$params = array();
					$params[] = array(':cats_id', $crumbs[$i]['cat_id'], 'int');
					$db->query($query, $params);
				}
			break;
			case 5: // pay relist fee (live mode)
				$query = "UPDATE " . $DBPrefix . "auctions SET suspended = 0 WHERE id = :auction_id";
				$params = array();
				$params[] = array(':auction_id', $custom_id, 'int');
				$db->query($query, $params);
				// add invoice
				$query = "INSERT INTO " . $DBPrefix . "useraccounts (user_id, auc_id, date, relist, total, paid) VALUES
						(:get_id, :get_extra_id, :time_stamp, :payment, :extra_payment, 1)";
				$params = array();
				$params[] = array(':get_id', $custom_id, 'int');
				$params[] = array(':get_extra_id', $custom_id, 'int');
				$params[] = array(':time_stamp', $system->ctime, 'int');
				$params[] = array(':payment', $payment_amount, 'float');
				$params[] = array(':extra_payment', $payment_amount, 'float');
				$db->query($query, $params);
			break;
			case 6:  // pay buyer fee (live mode)
				$query = "UPDATE " . $DBPrefix . "winners SET bf_paid = 1 WHERE bf_paid = 0 AND auction = :auction_id AND winner = :winner_id";
				$params = array();
				$params[] = array(':auction_id', $custom_id, 'int');
				$params[] = array(':winner_id', $user->user_data['id'], 'int');
				$db->query($query, $params);

				$query = "UPDATE " . $DBPrefix . "users SET suspended = 0 WHERE id = :user_id";
				$params = array();
				$params[] = array(':user_id', $user->user_data['id'], 'int');
				$db->query($query, $params);

				// add invoice
				$query = "INSERT INTO " . $DBPrefix . "useraccounts (user_id, auc_id, date, buyer, total, paid) VALUES
						(:user_id, :get_id, :time_stamp, :payment, :extra_payment, 1)";
				$params = array();
				$params[] = array(':user_id', $user->user_data['id'], 'int');
				$params[] = array(':get_id', $custom_id, 'int');
				$params[] = array(':time_stamp', $system->ctime, 'int');
				$params[] = array(':payment', $payment_amount, 'float');
				$params[] = array(':extra_payment', $payment_amount, 'float');
				$db->query($query, $params);
			break;
			case 7: // pay final value fee (live mode)
				$query = "UPDATE " . $DBPrefix . "winners SET ff_paid = 1 WHERE ff_paid = 0 AND auction = :auction_id AND seller = :user_id";
				$params = array();
				$params[] = array(':auction_id', $custom_id, 'int');
				$params[] = array(':user_id', $user->user_data['id'], 'int');
				$db->query($query, $params);

				$query = "UPDATE " . $DBPrefix . "users SET suspended = 0 WHERE id = :user_id";
				$params = array();
				$params[] = array(':user_id', $user->user_data['id'], 'int');
				$db->query($query, $params);

				// add invoice
				$query = "INSERT INTO " . $DBPrefix . "useraccounts (user_id, auc_id, date, finalval, total, paid) VALUES
						(:user_id, :get_id, :time_stamp, :payment, :extra_payment, 1)";
				$params = array();
				$params[] = array(':user_id', $user->user_data['id'], 'int');
				$params[] = array(':get_id', $custom_id, 'int');
				$params[] = array(':time_stamp', $system->ctime, 'int');
				$params[] = array(':payment', $payment_amount, 'float');
				$params[] = array(':extra_payment', $payment_amount, 'float');
				$db->query($query, $params);

			break;
			case 8: //Activate New Banner Account 
                $query = "UPDATE " . $DBPrefix . "bannersusers SET paid = 1 WHERE id = :bannersusers_id"; 
                $params = array();
				$params[] = array(':bannersusers_id', $custom_id, 'int');
				$db->query($query, $params);
            break; 
            case 9: //Activate Extra Banner on banner user account 
                $query = "UPDATE " . $DBPrefix . "bannersusers SET ex_banner_paid = 'y' WHERE id = :bannersusers_id";
                $params = array();
				$params[] = array(':bannersusers_id', $custom_id, 'int');
				$db->query($query, $params); 
            break; 
			case 10: //Digital item was paid now send email with digital item link
            	//Get auction id, winner id and seller id 
                $query = "SELECT DISTINCT w.bid, w.winner, w.auction, w.bid, b.title, d.item, d.seller, d.hash, u.nick, u.email, u.name
                FROM " . $DBPrefix . "winners w
                LEFT JOIN " . $DBPrefix . "auctions b ON (b.id = w.auction) 
        		LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.winner) 
                LEFT JOIN " . $DBPrefix . "digital_items d ON (d.auctions = w.auction)
                WHERE w.id = :digital_id"; 
				$params = array();
				$params[] = array(':digital_id', $custom_id, 'int');
				$db->query($query, $params); 
				while ($data = $db->result())
				{
					$send_email->digital_item_email($data['title'], $data['name'], $data['auction'], $data['bid'], $data['nick'], $data['email'], $security->encrypt($data['hash']), $data['pict_url'], $data['id'], $data['email']);
				}
				//update winners db that the item was paid for
				$query = "UPDATE " . $DBPrefix . "winners SET paid = 1, is_counted = 'y' WHERE id = :paid_id";
				$params = array();
				$params[] = array(':paid_id', $custom_id, 'int');
				$db->query($query, $params); 
				
				//update the sold items counter
				$query = "UPDATE " . $DBPrefix . "counters SET items_sold = :sold";
				$params = array();
				$params[] = array(':sold', $system->COUNTERS['items_sold'] + 1, 'int');
				$db->query($query, $params);
            break;
			
		}
	}
}
?>