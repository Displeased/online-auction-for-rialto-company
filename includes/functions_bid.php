<?php
/*******************************************************************************
 *   copyright				: (C) 2008 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com/
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/

include 'datacheck.inc.php';

$NOW = $system->ctime;
$get_name = $_REQUEST['id'] ;  //NEW get the full product name
$get_array = explode('-',htmlentities($system->uncleanvars($get_name), ENT_COMPAT, $CHARSET)) ;  //NEW split the product name into segments and put into an array (product id must be separated by '-' )
$get_id = end($get_array) ; //NEW extract the last array i.e product id form full product name
$id = (isset($_SESSION['CURRENT_ITEM'])) ? intval($_SESSION['CURRENT_ITEM']) : 0;
$id = (isset($get_id)) ? intval($get_id) : 0; if (!is_numeric($id)) $id = 0;

$bid = isset($_POST['bid']) ? $_POST['bid'] : '';
$qty = (isset($_POST['qty'])) ? intval($_POST['qty']) : 1;
$bid_action = isset($_POST['action']) ? $_POST['action'] : '';
$bidder_id = isset($user->user_data['id']) ? $user->user_data['id'] : '';
$bidding_ended = false;

function get_increment($val, $input_check = true)
{
	global $system, $DBPrefix, $db;

	if ($input_check)
		$val = $system->input_money($val);
	$query = "SELECT increment FROM " . $DBPrefix . "increments 
	WHERE ((low <= :low AND high >= :high) OR (low <= :lower AND high <= :higher)) ORDER BY increment DESC";
	$params = array();
	$params[] = array(':low', $val, 'float');
	$params[] = array(':high', $val, 'float');
	$params[] = array(':lower', $val, 'float');
	$params[] = array(':higher', $val, 'float');
	$db->query($query, $params);
	$increment = $db->result('increment');
	return $increment;
}

function extend_auction($id, $ends)
{
	global $system, $DBPrefix, $db;

	if ($system->SETTINGS['ae_status'] == 'y' && ($ends - $system->SETTINGS['ae_timebefore']) < $NOW)
	{		
		$extend = $ends + $system->SETTINGS['ae_extend'];
		$query = "UPDATE " . $DBPrefix . "auctions SET ends = :ends WHERE id = :id";
		$params = array();
		$params[] = array(':id', $id, 'int');
		$params[] = array(':ends', $extend, 'int');
		$db->query($query, $params);
	}
}

// first check if valid auction ID passed
$query = "SELECT a.*, u.nick, u.email, u.id AS uId FROM " . $DBPrefix . "auctions a
LEFT JOIN " . $DBPrefix . "users u ON (a.user = u.id)
WHERE a.id = :id";
$params = array();
$params[] = array(':id', $id, 'int');
$db->query($query, $params);

// such auction does not exist
if ($db->numrows() == 0)
{
	$template->assign_vars(array(
			'TITLE_MESSAGE' => $MSG['415'],
			'BODY_MESSAGE' => $ERR_606
			));
	include 'header.php';
	$template->set_filenames(array(
			'body' => 'message.tpl'
			));
	$template->display('body');
	include 'footer.php';
	exit; // kill the page
}

// reformat bid to valid number
$bid = $system->input_money($bid);

$Data = $db->result();
$item_title = $Data['title'];
$item_id = $Data['id'];
$seller_name = $Data['nick']; 
$seller_email = $Data['email'];
$atype = $Data['auction_type'];
$aquantity = $Data['quantity'];
$minimum_bid = $Data['minimum_bid'];
$customincrement = $Data['increment'];
$current_bid = $Data['current_bid'];
$pict_url_plain = $Data['pict_url'];
$reserve = $Data['reserve_price'];
$c = $Data['ends'];
$cbid = ($current_bid == 0) ? $minimum_bid : $current_bid;

if (($Data['ends'] <= $NOW || $Data['closed'] == 1) && !isset($errmsg))
{
	$errmsg = $ERR_614;
}
if (($Data['starts'] > $NOW) && !isset($errmsg))
{
	$errmsg = $ERR_073;
}

$query = "SELECT bid, bidder FROM " . $DBPrefix . "bids WHERE auction = :auction ORDER BY bid DESC, id DESC LIMIT 1";
$params = array();
$params[] = array(':auction', $id, 'int');
$db->query($query, $params);
if ($db->numrows() > 0)
{
	$high_bid = $db->result('bid');
	$WINNING_BIDDER = $db->result('bidder');
	$ARETHEREBIDS = ' | <a href="' . $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($item_title) . '-' . $id . '-' . '&history=view#history">' . $MSG['105'] . '</a>';
}
else
{
	$high_bid = $current_bid;
}

if ($customincrement > 0)
{
	$increment = $customincrement;
}
else
{
	$increment = get_increment($high_bid, false);
}

if (ceil($high_bid) == 0 || $atype == 2)
{
	$next_bid = $minimum_bid;
}
else
{
	$next_bid = $high_bid + $increment;
}
if ($bid_action == 'bid')
{
	// check user entered a bid
	if (empty($bid) && !isset($errmsg))
	{
		$errmsg = $ERR_072;
	}
	
	$tmpmsg = CheckBidData();
	if ($tmpmsg != 0 && !isset($errmsg))
	{
		$errmsg = ${'ERR_' . $tmpmsg};
	}
	
	if (isset($_POST['action']) && !isset($errmsg))
	{
		if (!$user->is_logged_in())
		{
			header("location: " . $system->SETTINGS['siteurl'] . "user_login.php");
			exit;
		}

		if ($system->SETTINGS['usersauth'] == 'y')
		{
			if (strlen($_POST['password']) == 0)
			{
				$errmsg = $ERR_004;
			}
			$check = $phpass->CheckPassword($_POST['password'], $user->user_data['password']);
			if ($check == 0)
			{
				$errmsg = $ERR_611;
			}
		}
		$send_emails = false;
		// make the bid
		if ($atype == 1 && !isset($errmsg)) // normal auction
		{
			if ($system->SETTINGS['proxy_bidding'] == 'n')
			{
				// is it the highest bid?
				if ($current_bid < $bid)
				{
					// did you outbid someone?
					$query = "SELECT u.id FROM " . $DBPrefix . "bids b, " . $DBPrefix . "users u WHERE b.auction = :auc_id AND b.bidder = u.id and u.suspended = 0 ORDER BY bid DESC";
					$params = array();
					$params[] = array(':auc_id', $id, 'int');
					$db->query($query, $params);
					if ($db->numrows() == 0 || $db->result('id') != $bidder_id)
					{
						$send_emails = true;
					}
					$query = "UPDATE " . $DBPrefix . "auctions SET current_bid = :bid, num_bids = num_bids + 1 WHERE id = :auc_id";
					$params = array();
					$params[] = array(':bid', $bid, 'float');
					$params[] = array(':auc_id', $id, 'int');
					$db->query($query, $params);
					// Also update bids table
					$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :auc_id, :bidder_id, :bid, :time, :qty)";
					$params = array();
					$params[] = array(':bid', $bid, 'float');
					$params[] = array(':auc_id', $id, 'int');
					$params[] = array(':bidder_id', $bidder_id, 'int');
					$params[] = array(':time', $NOW, 'int');
					$params[] = array(':qty', $qty, 'int');
					$db->query($query, $params);
					extend_auction($item_id, $c);
					$bidding_ended = true;
					
				}
			}
			elseif ($WINNING_BIDDER == $bidder_id)
			{
				$query = "SELECT bid FROM " . $DBPrefix . "proxybid p
						LEFT JOIN " . $DBPrefix . "users u ON (p.userid = u.id)
						WHERE userid = :user_id AND itemid = :item_id ORDER BY bid DESC";
				$params = array();
				$params[] = array(':user_id', $user->user_data['id'], 'int');
				$params[] = array(':item_id', $id, 'int');
				$db->query($query, $params);
				$checkrows = $db->numrows();
				if ($checkrows == 0)
				{
					$WINNER_PROXYBID = $db->result('bid');
					if ($WINNER_PROXYBID >= $bid)
					{
						$errmsg = $ERR_040;
					}
					else
					{
						// Just update proxy_bid
						$query = "UPDATE " . $DBPrefix . "proxybid SET bid = :newbid
								  WHERE userid = :user_id
								  AND itemid = :item_id AND bid = :oldbid";
						$params = array();
						$params[] = array(':user_id', $user->user_data['id'], 'int');
						$params[] = array(':item_id', $id, 'int');
						$params[] = array(':oldbid', $WINNER_PROXYBID, 'float');
						$params[] = array(':newbid', $bid, 'float');
						$db->query($query, $params);
	
						if ($reserve > 0 && $reserve > $current_bid && $bid >= $reserve)
						{
							$query = "UPDATE " . $DBPrefix . "auctions SET current_bid = :reserve, num_bids = num_bids + 1 WHERE id = :auc_id";
							$params = array();
							$params[] = array(':reserve', $reserve, 'float');
							$params[] = array(':auc_id', $id, 'int');
							$db->query($query, $params);
							
							// Also update bids table
							$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :auc_id, :bidder_id, :reserve, :time, :qty)";
							$params = array();
							$params[] = array(':reserve', $reserve, 'float');
							$params[] = array(':auc_id', $id, 'int');
							$params[] = array(':bidder_id', $bidder_id, 'int');
							$params[] = array(':time', $NOW, 'int');
							$params[] = array(':qty', $qty, 'int');
							$db->query($query, $params);					
						}
						extend_auction($item_id, $c);
						$bidding_ended = true;
					}
				}
			}
			if (!$bidding_ended && !isset($errmsg) && $system->SETTINGS['proxy_bidding'] == 'y')
			{
				$query = "SELECT * FROM " . $DBPrefix . "proxybid p, " . $DBPrefix . "users u WHERE itemid = :item_id AND p.userid = u.id and u.suspended = 0 ORDER by bid DESC";
				$params = array();
				$params[] = array(':item_id', $id, 'int');
				$db->query($query, $params);
				if ($db->numrows() == 0) // First bid
				{
					$query = "INSERT INTO " . $DBPrefix . "proxybid VALUES (:id, :bidder_id, :bid)";
					$params = array();
					$params[] = array(':id', $id, 'int');
					$params[] = array(':bidder_id', $bidder_id, 'int');
					$params[] = array(':bid', $bid, 'float');
					$db->query($query, $params);	
							
					if ($reserve > 0 && $reserve > $current_bid && $bid >= $reserve)
					{
						$next_bid = $reserve;
					}
					// Only updates current bid if it is a new bidder, not the current one
					$query = "UPDATE " . $DBPrefix . "auctions SET current_bid = :next_bid, num_bids = num_bids + 1 WHERE id = :id";
					$params = array();
					$params[] = array(':next_bid',  $next_bid, 'float');
					$params[] = array(':id',  $id, 'int');
					$db->query($query, $params);
					
					// Also update bids table
					$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :id, :bidder_id, :next_bid, :now, :qty)";
					$params = array();
					$params[] = array(':id',  $id, 'int');
					$params[] = array(':bidder_id',  $bidder_id, 'int');
					$params[] = array(':next_bid',  $next_bid, 'float');
					$params[] = array(':now',   $NOW, 'int');
					$params[] = array(':qty',   $qty, 'int');
					$db->query($query, $params);
	
					$query = "UPDATE " . $DBPrefix . "counters SET bids = (bids + :one)";
					$params = array();
					$params[] = array(':one',  1, 'int');
					$db->query($query, $params);
				}
				else // This is not the first bid
				{
					$proxy_data = $db->result();
					$proxy_bidder_id = $proxy_data['userid'];
					$proxy_max_bid = $proxy_data['bid'];
					
					if ($proxy_max_bid < $bid)
					{
						if ($proxy_bidder_id != $bidder_id)
						{
							$send_emails = true;
						}
						$next_bid = $proxy_max_bid + $increment;
						if (($proxy_max_bid + $increment) > $bid)
						{
							$next_bid = $bid;
						}
	
						$query = "INSERT INTO " . $DBPrefix . "proxybid VALUES (:id, :bidder_id, :bid)";
						$params = array();
						$params[] = array(':id', $id, 'int');
						$params[] = array(':bidder_id', $bidder_id, 'int');
						$params[] = array(':bid', $bid, 'float');
						$db->query($query, $params);
	
						if ($reserve > 0 && $reserve > $current_bid && $bid >= $reserve)
						{
							$next_bid = $reserve;
						}
						// Fake bid to maintain a coherent history
						if ($current_bid < $proxy_max_bid)
						{
							$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :id, :bidder_id, :next_bid, :now, :qty)";
							$params = array();
							$params[] = array(':id',  $id, 'int');
							$params[] = array(':bidder_id',  $proxy_bidder_id, 'int');
							$params[] = array(':next_bid',  $proxy_max_bid, 'float');
							$params[] = array(':now',   $NOW, 'int');
							$params[] = array(':qty',   $qty, 'int');
							$db->query($query, $params);
	
							$fakebids = 1;
						}
						else
						{
							$fakebids = 0;
						}
						// Update bids table
						$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :id, :bidder_id, :next_bid, :now, :qty)";
						$params = array();
						$params[] = array(':id',  $id, 'int');
						$params[] = array(':bidder_id',  $bidder_id, 'int');
						$params[] = array(':next_bid',  $next_bid, 'float');
						$params[] = array(':now',   $NOW, 'int');
						$params[] = array(':qty',   $qty, 'int');
						$db->query($query, $params);
	
						$query = "UPDATE " . $DBPrefix . "counters SET bids = (bids + (1 + :fakebids))";
						$params = array();
						$params[] = array(':fakebids',  $fakebids, 'int');
						$db->query($query, $params);
	
						$query = "UPDATE " . $DBPrefix . "auctions SET current_bid = :next_bid, num_bids = (num_bids + 1 + :fakebids) WHERE id = :id";
						$params = array();
						$params[] = array(':next_bid',  $next_bid, 'float');
						$params[] = array(':fakebids',  $fakebids, 'int');
						$params[] = array(':id',  $id, 'int');
						$db->query($query, $params);
					}
					if ($proxy_max_bid == $bid)
					{
						$cbid = $proxy_max_bid;
						$errmsg = $MSG['701'];
						// Update bids table
						$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :id, :bidder_id, :bid, :now, :qty)";
						$params = array();
						$params[] = array(':id',  $id, 'int');
						$params[] = array(':bidder_id',  $bidder_id, 'int');
						$params[] = array(':bid',  $bid, 'float');
						$params[] = array(':now',   $NOW, 'int');
						$params[] = array(':qty',   $qty, 'int');
						$db->query($query, $params);
	
						$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :id, :proxy_bidder_id, :cbid, :now, :qty)";
						$params = array();
						$params[] = array(':id',  $id, 'int');
						$params[] = array(':proxy_bidder_id',  $proxy_bidder_id, 'int');
						$params[] = array(':cbid',  $cbid, 'float');
						$params[] = array(':now',   $NOW, 'int');
						$params[] = array(':qty',   $qty, 'int');
						$db->query($query, $params);
	
						$query = "UPDATE " . $DBPrefix . "counters SET bids = (bids + :two)";
						$params[] = array(':two',  2, 'int');
						$db->query($query, $params);
	
						$query = "UPDATE " . $DBPrefix . "auctions SET current_bid = :cbid, num_bids = num_bids + 2 WHERE id = :id";
						$params = array();
						$params[] = array(':cbid',  $cbid, 'float');
						$params[] = array(':id',   $id, 'int');
						$db->query($query, $params);
	
						if ($customincrement == 0)
						{
							// get new increment
							$increment = get_increment($cbid);
						}
						else
						{
							$increment = $customincrement;
						}
						$next_bid = $cbid + $increment;
					}
					if ($proxy_max_bid > $bid)
					{
						// Update bids table
						$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :id, :bidder_id, :bid, :now, :qty)";
						$params = array();
						$params[] = array(':id',  $id, 'int');
						$params[] = array(':bidder_id',  $bidder_id, 'int');
						$params[] = array(':bid',  $bid, 'float');
						$params[] = array(':now',   $NOW, 'int');
						$params[] = array(':qty',   $qty, 'int');
						$db->query($query, $params);
	
						if ($customincrement == 0)
						{
							// get new increment
							$increment = get_increment($bid);
						}
						else
						{
							$increment = $customincrement;
						}
						if ($bid + $increment - $proxy_max_bid >= 0)
						{
							$cbid = $proxy_max_bid;
						}
						else
						{
							$cbid = $bid + $increment;
						}
						$errmsg = $MSG['701'];
						// Update bids table
						$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :id, :proxy_bidder_id, :cbid, :now, :qty)";
						$params = array();
						$params[] = array(':id',  $id, 'int');
						$params[] = array(':proxy_bidder_id',  $proxy_bidder_id, 'int');
						$params[] = array(':cbid',  $cbid, 'float');
						$params[] = array(':now',   $NOW, 'int');
						$params[] = array(':qty',   $qty, 'int');
						$db->query($query, $params);
	
						$query = "UPDATE " . $DBPrefix . "counters SET bids = (bids + :two)";
						$params[] = array(':two',  2, 'int');
						$db->query($query, $params);
	
						$query = "UPDATE " . $DBPrefix . "auctions SET current_bid = :cbid, num_bids = num_bids + 2 WHERE id = :id";
						$params = array();
						$params[] = array(':cbid',  $cbid, 'float');
						$params[] = array(':id',   $id, 'int');
						$db->query($query, $params);
	
						if ($customincrement == 0)
						{
							// get new increment
							$increment = get_increment($cbid);
						}
						else
						{
							$increment = $customincrement;
						}
						$next_bid = $cbid + $increment;
					}
				}
				extend_auction($item_id, $c);
			}
		}
		elseif ($atype == 2 && !isset($errmsg)) // dutch auction
		{
			// If the bidder already bid on this auction there new bbid must be higher
			$query = "SELECT bid, quantity FROM " . $DBPrefix . "bids WHERE bidder = :bidder_id AND auction = :id ORDER BY bid DESC LIMIT 1";
			$params = array();
			$params[] = array(':bidder_id',  $bidder_id, 'int');
			$params[] = array(':id',   $id, 'int');
			$db->query($query, $params);
	
			if ($db->numrows() > 0)
			{
				$PREVIOUSBID = $db->result();
				if (($bid * $qty) <= ($PREVIOUSBID['bid'] * $PREVIOUSBID['quantity']))
				{
					$errmsg = $ERR_059;
				}
			}
			if (!isset($errmsg))
			{
				$query = "INSERT INTO `" . $DBPrefix . "bids` (`id`, `auction`, `bidder`, `bid`, `bidwhen`, `quantity`) VALUES (NULL, :id, :bidder_id, :bid, :now, :qty)";
				$params = array();
				$params[] = array(':id',  $id, 'int');
				$params[] = array(':bidder_id',  $bidder_id, 'int');
				$params[] = array(':bid',  $bid, 'float');
				$params[] = array(':now',   $NOW, 'int');
				$params[] = array(':qty',   $qty, 'int');
				$db->query($query, $params);
	
				$query = "UPDATE " . $DBPrefix . "counters SET bids = (bids + :one)";
				$params = array();
				$params[] = array(':one',  1, 'int');
				$db->query($query, $params);
	
				$query = "UPDATE " . $DBPrefix . "auctions SET current_bid = :bid, num_bids = num_bids + 1 WHERE id = :id";
				$params = array();
				$params[] = array(':bid',  $bid, 'float');
				$params[] = array(':id',  $id, 'int');
				$db->query($query, $params);
			}
		}
		// send emails where needed
		$query = "SELECT bidder, bid FROM " . $DBPrefix . "bids WHERE auction = :id ORDER BY bid DESC";
		$params = array();
		$params[] = array(':id',  $id, 'int');
		$db->query($query, $params);
	
		if ($db->numrows() > 1)
		{
			$OldWinner_id = $db->result('bidder');
			$new_bid = $next_bid;
			$OldWinner_bid = $system->print_money($new_bid - $increment);
	
			$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :OldWinner_id";
			$params = array();
			$params[] = array(':OldWinner_id',  $OldWinner_id, 'int');
			$db->query($query, $params);
	
			$OldWinner_nick = $db->result('nick');
			$OldWinner_name = $db->result('name');
			$OldWinner_email = $db->result('email');
		}
		// Update counters table with the new bid
		// Send notification if users keyword matches (Item Watch)
		$query = "SELECT id, email, name, item_watch FROM " . $DBPrefix . "users WHERE item_watch != '' AND item_watch IS NOT NULL AND id != :user_id";
		$params = array();
		$params[] = array(':user_id', $bidder_id, 'int');
		$db->query($query, $params);
		
		foreach ($db->fetchall() as $row)
		{
			// If keyword matches with opened auction title or/and desc send user a mail
			if (strstr($row['item_watch'], $id) !== false)
			{
				// Get data about the auction
				$query = "SELECT title, current_bid FROM " . $DBPrefix . "auctions WHERE id = :id";
				$params = array();
				$params[] = array(':id',  $id, 'int');
				$db->query($query, $params);
				$data = $db->result();			
				// Send e-mail message	
				$send_email->item_watch_emails($row['name'], $data['title'], $data['current_bid'], $id, $row['id'], $row['email']);
			}
		}
		// End of Item watch
		if ($send_emails)
		{
			$month = gmdate('m', $c + $system->tdiff);
			$ends_string = $MSG['MON_0' . $month] . ' ' . gmdate('d, Y H:i', $c + $system->tdiff);
			$new_bid = $system->print_money($next_bid);
			// Send e-mail message
			$send_email->outbid($item_title, $OldWinner_name, $OldWinner_bid, $new_bid, $ends_string, $item_id, $pict_url_plain, $OldWinner_id, $OldWinner_email);
		}
	
		if (defined('TrackUserIPs'))
		{
			// log auction bid IP
			$system->log('user', 'Bid on Item', $bidder_id, $id);
		}
		$template->assign_vars(array(
			'PAGE' => 2,
			'BID_HISTORY' => (isset($ARETHEREBIDS)) ? $ARETHEREBIDS : '',
			'ID' => $id,
			'BID' => $system->print_money($bid)
		));
	}
}
if (!isset($_POST['action']) || isset($errmsg))
{
	// just set the needed template variables
	$template->assign_vars(array(
			'PAGE' => 1,
			'ERROR' => (isset($errmsg)) ? $errmsg : '',
			'BID_HISTORY' => (isset($ARETHEREBIDS)) ? $ARETHEREBIDS : '',
			'ID' => $id,
			'SEO_TITLE' => generate_seo_link($item_title),
			'TITLE' => $item_title,
			'CURRENT_BID' => $system->print_money($cbid),
			'ATYPE' => $atype,
			'BID' => $system->print_money_nosymbol($bid),
			'NEXT_BID' => $system->print_money($next_bid),
			'QTY' => $qty,
			'TQTY' => $aquantity,
			'AGREEMENT' => sprintf($MSG['25_0086'], $system->print_money($qty * $bid)),
			'CURRENCY' => $system->SETTINGS['currency'],

			'B_USERAUTH' => ($system->SETTINGS['usersauth'] == 'y')
			));
}
?>
