<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


if (!defined('InuAuctions')) exit();

function rebuild_table_file($table)
{
	global $DBPrefix, $system, $include_path, $db;
	
	switch($table)
	{
		case 'membertypes':
			$output_filename = $include_path . 'membertypes.inc.php';
			$field_name = array('id', 'feedbacks', 'icon');
			$sort_field = 1;
			$array_name = 'membertypes';
			$output = '<?php' . "\n";
			$output.= '$' . $array_name . ' = array(' . "\n";
		break;
	}

	$query = "SELECT " . join(',', $field_name) . " FROM " . $DBPrefix . "" . $table . " ORDER BY " .$field_name[$sort_field] . ";";
	$params = array();
	$params[] = array(':au_id', $id, 'int');
	$db->query($query, $params);
	$num_rows = $db->numrows();

	$i = 0;
	while ($row = $db->result())
	{
		$output .= '\'' . $row[$field_name[0]] . '\' => array(' . "\n";
		$field_count = count($field_name);
		$j = 0;
		foreach ($field_name as $field)
		{
			$output .= '\'' . $field . '\' => \'' . $row[$field] . '\'';
			$j++;
			if ($j < $field_count)
				$output .= ', ';
			else
				$output .= ')';
		}
		$i++;
		if ($i < $num_rows)
			$output .= ',' . "\n";
		else
			$output .= "\n";
	}

	$output .= ');' . "\n" . '?>';

	$handle = fopen($output_filename, 'w');
	fputs($handle, $output);
	fclose($handle);
}

function rebuild_html_file($table)
{
	global $DBPrefix, $system, $include_path, $db;
	switch($table)
	{
		case 'countries':
			$output_filename = $include_path . 'countries.inc.php';
			$field_name = 'country';
			$array_name = 'countries';
		break;
	}

	$query = "SELECT " . $field_name . " FROM " . $DBPrefix . $table . " ORDER BY " . $field_name . ";";
	$params = array();
	$params[] = array(':au_id', $id, 'int');
	$db->query($query, $params);
	$num_rows = $db->numrows();

	$output = '<?php' . "\n";
	$output.= '$' . $array_name . ' = array(\'\', ' . "\n";

	$i = 0;
	while ($row = $db->result())
	{
		$output .= '\'' . $row[$field_name] . '\'';
		$i++;
		if ($i < $num_rows)
			$output .= ',' . "\n";
		else
			$output .= "\n";
	}

	$output .= ');' . "\n" . '?>';

	$handle = fopen($output_filename, 'w');
	fputs($handle, $output);
	fclose($handle);
}
?>