<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

if (!defined('InuAuctions')) exit();

class send_email
{
	function confirmation($auction_id, $title, $atype, $pict_url, $minimum_bid, $reserve_price, $buy_now_price, $a_ends)
	{
		global $system, $user, $MSG, $security;
	
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'SITE_URL' => $system->SETTINGS['siteurl'],
				'SITENAME' => $system->SETTINGS['sitename'],
	
				'A_ID' => $auction_id,
				'A_TITLE' => $title,
				'SEO_TITLE' => generate_seo_link($title),
				'A_TYPE' => ($atype == 1) ? $MSG['642'] : $MSG['641'],
				'A_PICURL' => ($pict_url != '') ? $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $security->encrypt($auction_id . '/' . $pict_url) : 'images/email_alerts/default_item_img.jpg',
				'A_MINBID' => $system->print_money($minimum_bid, true, false),
				'A_RESERVE' => $system->print_money($reserve_price, true, false),
				'A_BNPRICE' => $system->print_money($buy_now_price, true, false),
				'A_ENDS' => ArrangeDateNoCorrection($a_ends + $system->tdiff),
				'MAXIMAGESIZE' => $system->SETTINGS['thumb_show'],
				'C_NAME' => $user->user_data['name']
				));
		$emailer->email_uid = $user->user_data['id'];
		$subject = $system->SETTINGS['sitename'] . ' ' . $MSG['099'] . ': ' . $title . ' (' . $auction_id . ')';
		$emailer->email_sender($user->user_data['email'], 'auctionmail.inc.php', $subject);
	}
	
	function cumulative($report, $name, $email, $id)
	{
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'SITE_URL' => $system->SETTINGS['siteurl'],
				'SITENAME' => $system->SETTINGS['sitename'],
				'ADMINMAIL' => $system->SETTINGS['adminmail'],
		
				'REPORT' => $report,
		
				'S_NAME' => $name
				));
		$emailer->email_uid = $id;
		$emailer->email_sender($email, 'endauction_cumulative.inc.php', $MSG['25_0199']);

	}
	
	function nowinner($title, $id, $ends_string, $pict_url, $Sid)
	{
		global $system, $MSG, $DBPrefix, $db, $security;
		
		$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :seller"; 
		$params = array();
		$params[] = array(':seller', $Sid, 'int');
		$db->query($query, $params);
		$seller = $db->result();

		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'S_NAME' => $seller['name'],
				'S_NICK' => $seller['nick'],
				'S_EMAIL' => $seller['email'],
				'A_TITLE' => $title,
				'A_ID' => $id,
				'A_END' => $ends_string,
				'A_URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($title) . '-' . $id,
				'SITE_URL' => $system->SETTINGS['siteurl'],
				'A_PICURL' => ($pict_url != '') ? $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $security->encrypt($id . '/' . $pict_url) : $system->SETTINGS['siteurl'] . 'images/email_alerts/default_item_img.jpg',
				'SITENAME' => $system->SETTINGS['sitename'],
				'MAXIMAGESIZE' => $system->SETTINGS['thumb_show']
				));
		$emailer->email_uid = $Sid;
		$subject = $system->SETTINGS['sitename'] . ' ' . $MSG['112'];
		$emailer->email_sender($seller['email'], 'endauction_nowinner.inc.php', $subject);
	}
	
	function winner($title, $id, $pict_url, $current_bid, $quantity, $ends_string, $seller_id, $winner_id, $win_id)
	{
		global $system, $MSG, $db, $DBPrefix, $security;
		
		//winners table data to see if the item was paid
		$query = "SELECT paid FROM " . $DBPrefix . "winners WHERE id = :win"; 
		$params = array();
		$params[] = array(':win', $win_id, 'int');
		$db->query($query, $params);
		$paid = $db->result('paid');

		//seller data
		$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :seller"; 
		$params = array();
		$params[] = array(':seller', $seller_id, 'int');
		$db->query($query, $params);
		$seller = $db->result();
		
		//winner data
		$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :winner"; 
		$params = array();
		$params[] = array(':winner', $winner_id, 'int');
		$db->query($query, $params);
		$winner = $db->result();

		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'S_NAME' => $seller['name'],
			'A_URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($title) . '-' . $id,
			'A_PICURL' => ($pict_url != '') ? $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $security->encrypt($id . '/' . $pict_url) : $system->SETTINGS['siteurl'] . 'images/email_alerts/default_item_img.jpg',
			'A_TITLE' => $title,
			'A_CURRENTBID' => $system->print_money($current_bid, true, false),
			'A_QTY' => $quantity,
			'A_ENDS' => $ends_string,
			'PAID' => $paid == 1 ? '<span style="color:green">' . $MSG['755'] . '</span>' : '<span style="color:red">' . $MSG['350_10024'] . '</span>',
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'SITENAME' => $system->SETTINGS['sitename'],
			'WINNER_NAME' => $winner['name'],
			'WINNER_EMAIL' => $winner['email'],
			'WINNER_ADDRESS' => $winner['address'],
			'WINNER_CITY' => $winner['city'],
			'WINNER_PROV' => $winner['prov'],
			'WINNER_COUNTRY' => $winner['country'],
			'WINNER_ZIP' => $winner['zip'],
			'MAXIMAGESIZE' => $system->SETTINGS['thumb_show']
		));
		$emailer->email_uid = $seller['id'];
		$subject = $system->SETTINGS['sitename'] . ' ' . $MSG['079'] . ' ' . $MSG['907'] . ' ' . $system->uncleanvars($title);
		$emailer->email_sender($seller['email'], 'endauction_winner.inc.php', $subject);
	}
	
	function youwin($Auc_description, $wanted, $quantity, $title, $Aid, $current_bid, $ends_string, $seller_id, $payment_details, $Winner_id)
	{
		global $system, $MSG, $WINNERS_BID, $DBPrefix;
		
		//winner data
		$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :winner"; 
		$params = array();
		$params[] = array(':winner', $Winner_id, 'int');
		$db->query($query, $params);
		$winner = $db->result();

		//seller data
		$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :seller"; 
		$params = array();
		$params[] = array(':seller', $seller_id, 'int');
		$db->query($query, $params);
		$seller = $db->result();

		if(strlen(strip_tags($Auc_description)) > 60)
		{
			$description = substr(strip_tags($Auc_description), 0, 50) . '...';
		}
		else
		{
			$description = $Auc_description;
		}
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'W_NAME' => $winner['name'],
			'W_WANTED' => $wanted,
			'W_GOT' => $quantity,
			'A_URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($title) . '-' . $Aid,
			'A_TITLE' => $title,
			'A_DESCRIPTION' => $description,
			'A_CURRENTBID' => $system->print_money($WINNERS_BID[$current_bid], true, false),
			'A_ENDS' => $ends_string,
			'S_NICK' => $seller['nick'],
			'S_EMAIL' => $seller['email'],
			'S_PAYMENT' => $payment_details,
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'SITENAME' => $system->SETTINGS['sitename'],
			'ADMINEMAIL' => $system->SETTINGS['adminmail']
		));
		$emailer->email_uid = $winner['id'];
		$subject = $MSG['909'] . ' ' . $title;
		$emailer->email_sender($winner['email'], 'endauction_youwin.inc.php', $subject);
	}
	
	function youwin_nodutch($Auc_title, $Auction_pict_url, $Auction_id, $Auction_current_bid, $ends_string, $Seller_id, $Winner_id)
	{
		global $system, $MSG, $db, $DBPrefix, $security;
		
		//seller data
		$query = "SELECT nick, email FROM " . $DBPrefix . "users WHERE id = :seller"; 
		$params = array();
		$params[] = array(':seller', $Seller_id, 'int');
		$db->query($query, $params);
		$seller = $db->result();
		
		//winner data
		$query = "SELECT id, name, email FROM " . $DBPrefix . "users WHERE id = :winner"; 
		$params = array();
		$params[] = array(':winner', $Winner_id, 'int');
		$db->query($query, $params);
		$winner = $db->result();

		$item_title = $system->uncleanvars($Auc_title);
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'W_NAME' => $winner['name'],
			'A_PICURL' => ($Auction_pict_url != '') ? 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $security->encrypt($Auction_id . '/' . $Auction_pict_url) : 'images/email_alerts/default_item_img.jpg',
			'A_URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($Auc_title) . '-' . $Auction_id,
			'A_TITLE' => $Auc_title,
			'A_CURRENTBID' => $system->print_money($Auction_current_bid, true, false),
			'A_ENDS' => $ends_string,
			'S_NICK' => $seller['nick'],
			'S_EMAIL' => $seller['email'],		
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'SITENAME' => $system->SETTINGS['sitename'],
			'MAXIMAGESIZE' => $system->SETTINGS['thumb_show']
		));
		$emailer->email_uid = $winner['id'];
		$subject = $system->SETTINGS['sitename'] . $MSG['909'] . ': ' . $item_title;
		$emailer->email_sender($winner['email'], 'endauction_youwin_nodutch.inc.php', $subject);
	}
	
	function outbid($item_title, $OldWinner_name, $OldWinner_bid, $new_bid, $ends_string, $item_id, $pict_url_plain, $OldWinner_id, $OldWinner_email)
	{
		global $system, $MSG, $security;
	
		$title = $system->uncleanvars($item_title);

		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'SITE_URL' => $system->SETTINGS['siteurl'],
				'SITENAME' => $system->SETTINGS['sitename'],
				'C_NAME' => $OldWinner_name,
				'C_BID' => $OldWinner_bid,
				'N_BID' => $new_bid,
				'A_TITLE' => $title,
				'A_ENDS' => $ends_string,
				'MAXIMAGESIZE' => $system->SETTINGS['thumb_show'],
				'A_PICURL' => ($pict_url_plain != '') ? 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $security->encrypt($item_id . '/' . $pict_url_plain) : 'images/email_alerts/default_item_img.jpg',
				'A_URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($title) . '-' . $item_id
				));
		$emailer->email_uid = $OldWinner_id;
		$subject = $system->SETTINGS['sitename'] . ' ' . $MSG['906'] . ': ' . $title;
		$emailer->email_sender($OldWinner_email, 'no_longer_winner.inc.php', $subject);
	}
	
	function approved($name, $language, $email)
	{
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'SITE_URL' => $system->SETTINGS['siteurl'],
				'SITENAME' => $system->SETTINGS['sitename'],
				'C_NAME' => $name
				));
		$emailer->userlang = $language;
		$subject = $system->SETTINGS['sitename'] . ' ' . $MSG['095'];
		$emailer->email_sender(array($email, $system->SETTINGS['adminmail']), 'user_approved.inc.php', $subject);
	}
	
	function user_confirmation($TPL_name_hidden, $TPL_id_hidden, $TPL_email_hidden, $TPL_nick_hidden)
	{
		global $system, $MSG, $uploaded_path, $MD5_PREFIX;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'SITENAME' => $system->SETTINGS['sitename'],
				'SITEURL' => $system->SETTINGS['siteurl'],
				'ADMINMAIL' => $system->SETTINGS['adminmail'],
				'CONFIRMURL' => $system->SETTINGS['siteurl'] . 'confirm.php?id=' . $TPL_id_hidden . '&hash=' . md5($MD5_PREFIX . $TPL_nick_hidden),
				'C_NAME' => $TPL_name_hidden
				));
		$emailer->email_uid = $TPL_id_hidden;
		$emailer->email_sender($TPL_email_hidden, 'usermail.inc.php', $system->SETTINGS['sitename'] . ' ' . $MSG['098']);
	}
	
	function needapproval($TPL_id_hidden, $TPL_name_hidden, $TPL_nick_hidden, $TPL_address, $TPL_city, $TPL_prov, $TPL_zip, $TPL_country, $TPL_phone, $TPL_email, $TPL_password_hidden, $TPL_email_hidden)
	{
		global $system, $MSG, $uploaded_path, $MD5_PREFIX;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'C_ID' => addslashes($TPL_id_hidden),
				'C_NAME' => addslashes($TPL_name_hidden),
				'C_NICK' => addslashes($TPL_nick_hidden),
				'C_ADDRESS' => addslashes($TPL_address),
				'C_CITY' => addslashes($TPL_city),
				'C_PROV' => addslashes($TPL_prov),
				'C_ZIP' => addslashes($TPL_zip),
				'C_COUNTRY' => addslashes($TPL_country),
				'C_PHONE' => addslashes($TPL_phone),
				'C_EMAIL' => addslashes($TPL_email),
				'C_PASSWORD' => addslashes($TPL_password_hidden),
				'SITENAME' => $system->SETTINGS['sitename'],
				'SITEURL' => $system->SETTINGS['siteurl'],
				'ADMINEMAIL' => $system->SETTINGS['adminmail'],
				'CONFIRMATION_PAGE' => $system->SETTINGS['siteurl'] . 'confirm.php?id=' . $TPL_id_hidden . '&hash=' . md5($MD5_PREFIX . $TPL_nick_hidden),
				'LOGO' => $system->SETTINGS['siteurl'] . 'themes/' . $system->SETTINGS['theme'] . '/' . $system->SETTINGS['logo']
				));
		$emailer->email_uid = $TPL_id_hidden;
		$emailer->email_sender(array($TPL_email_hidden, $system->SETTINGS['adminmail']), 'user_needapproval.inc.php', $system->SETTINGS['sitename']. ' '.$MSG['098']);
	}
	
	function requesttoadmin($name, $nick, $email, $id)
	{
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'NAME' => $name,
				'NICK' => $nick,
				'EMAIL' => $email,
				'ID' => $id
				));
		$emailer->email_sender($system->SETTINGS['adminmail'], 'buyer_request.inc.php', $MSG['820']);
	}
	
	function admin_support($sendto, $sendto, $subject, $nowmessage, $sellerdata_email, $title, $userarray_email)
	{
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'NAME' => $sendto,
			'SUBJECT' => $subject,
			'MESSAGE' => $nowmessage,
			'A_URL' => $system->SETTINGS['siteurl'] . 'support',
			'S_EMAIL' => $sellerdata_email,
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'SITENAME' => $system->SETTINGS['sitename'],
		));
		$item_title = $system->uncleanvars($title);
		$email_subject = $MSG['335'] . ' ' . $system->SETTINGS['sitename'] . ' ( ' . $MSG['3500_1015435'] . ' ) ';
		$from_id = $system->SETTINGS['adminmail'];
		$id_type = 'fromemail';
		$emailer->email_sender($userarray_email, 'support_ticket.inc', $email_subject);
	}
	
	function reply_user($id, $nick, $name, $email)
	{
		global $system, $MSG, $MD5_PREFIX;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'SITENAME' => $system->SETTINGS['sitename'],
				'SITEURL' => $system->SETTINGS['siteurl'],
				'ADMINMAIL' => $system->SETTINGS['adminmail'],
				'CONFIRMURL' => $system->SETTINGS['siteurl'] . 'confirm.php?id=' . $id . '&hash=' . md5($MD5_PREFIX . $nick),
				'C_NAME' => $name
				));
		$emailer->email_uid = $id;
		$emailer->email_sender($email, 'usermail.inc.php', $system->SETTINGS['sitename'] . ' ' . $MSG['098']);
	}
	
	function final_value_fee_email($id, $title, $name, $Aid, $uid, $email)
	{
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'ID' => $id,
				'TITLE' => $title,
				'NAME' => $name,
				'LINK' => $system->SETTINGS['siteurl'] . 'pay.php?a=7&auction_id=' . $Aid
				));
		$emailer->email_uid = $uid;
		$emailer->email_sender($email, 'final_value_fee.inc.php', $system->SETTINGS['sitename'] . ' - ' . $MSG['523']);
	}
	
	function buyer_fee_email($id, $title, $name, $Aid, $uid, $email)
	{
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'ID' => $id,
			'TITLE' => $title,
			'NAME' => $name,
			'LINK' => $system->SETTINGS['siteurl'] . 'pay.php?a=6&auction_id=' . $Aid
		));
		$emailer->email_uid = $uid;
		$emailer->email_sender($email, 'buyer_fee.inc.php', $system->SETTINGS['sitename'] . ' - ' . $MSG['522']);
	}
	
	function email_request($user_id, $subject, $email, $message, $user_name, $from_email)
	{		
		$emailer = new email_handler();
		$emailer->email_uid = $user_id;
		$emailer->email_basic($subject, $email, nl2br($message), $user_name . '<'. $from_email . '>');
	}
	
	function email_request_support($senders_name, $cleaned_question, $cleaned_subject, $senders_email, $admin_nick, $user_id, $admin_email, $sender_email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'SENDER_NAME' => $senders_name,
			'SENDER_QUESTION' => stripslashes($cleaned_question),
			'SUBJECT' => stripslashes($cleaned_subject),
			'SENDER_EMAIL' => $senders_email,
			'SITENAME' => $system->SETTINGS['sitename'],
			'SITEURL' => $system->SETTINGS['siteurl'],
			'SELLER_NICK' => $admin_nick
		));
		$subject = $system->SETTINGS['sitename'] . '-' . 'contact us';
		$subject2 = $system->SETTINGS['sitename'] . '-' . 'contact us' . '-' . '(Copy)';
		$emailer->email_uid = $user_id;
		$emailer->email_sender($admin_email, 'email_request.inc.php', $subject);
		$emailer->email_sender($sender_email, 'email_request2.inc.php', $subject2);
	}
	
	function forgot_password($name, $random_pwd, $id, $email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'REALNAME' => $name,
			'NEWPASS' => $random_pwd,
			'SITENAME' => $system->SETTINGS['sitename']
		));
		$emailer->email_uid = $id;
		$emailer->email_sender($email, 'newpasswd.inc.php', $MSG['024']);
	}
	
	function send_friend_email($sender_name, $sender_email, $sender_comment, $friend_name, $TPL_item_title, $id, $friend_email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'S_NAME' => $sender_name,
			'S_EMAIL' => $sender_email,
			'S_COMMENT' => $sender_comment,
			'F_NAME' => $friend_name,
			'TITLE' => $TPL_item_title,
			'URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($TPL_item_title) . '-' . $id,
			'SITENAME' => $system->SETTINGS['sitename'],
			'THEME' => $system->SETTINGS['theme'],
			'SITEURL' => $system->SETTINGS['siteurl'],
			'ADMINEMAIL' => $system->SETTINGS['adminmail']
		));
		$emailer->email_sender($friend_email, 'friendmail.inc.php', $MSG['905']);
	}
	
	function digital_item_email($title, $name, $auction, $bid, $nick, $email, $hash, $pict_url, $id)
	{		
		global $system, $MSG, $security;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'A_TITLE' => $title,
			'W_NAME' => $name,
			'A_URL' => $system->SETTINGS['siteurl'] . 'products/'. generate_seo_link($title).'-' . $auction,
			'BID' => $system->print_money($bid),
			'S_NICK' => $nick,
			'S_EMAIL' => $email,
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'SITENAME' => $system->SETTINGS['sitename'],
			'MAXIMAGESIZE' => $system->SETTINGS['thumb_show'],
							
			'DIGITAL_ITEMS' => 'my_downloads.php?diupload=3&fromfile=' . $hash,
			'A_PICURL' => ($pict_url != '') ? 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $security->encrypt($auction . '/' . $pict_url) : 'images/no-photo.jpg',
		));
		$emailer->email_uid = $id;
		$emailer->email_sender($email, 'endauction_youwin_digital_item.inc', $system->SETTINGS['sitename'] . ' - ' . 'Download' . ' - ' . $title);
	}
	
	function digital_item_email_pt2($title, $name, $auction, $bid, $seller_nick, $seller_email, $hash, $pict_url, $winner, $email)
	{		
		global $system, $MSG, $user, $security;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'A_TITLE' => $title,
			'W_NAME' => $name,
			'A_URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($title) . '-' . $auction,
			'BID' => $system->print_money($bid),
			'S_NICK' => $seller_nick,
			'S_EMAIL' => $seller_email,
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'SITENAME' => $system->SETTINGS['sitename'],
			'DIGITAL_ITEMS' => 'my_downloads.php?diupload=3&fromfile=' . $hash,
			'MAXIMAGESIZE' => $system->SETTINGS['thumb_show'],
			'A_PICURL' => ($pict_url != '') ? 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $security->encrypt($auction . '/' . $pict_url) : 'images/email_alerts/default_item_img.jpg',		
		));
		$item_title = $system->uncleanvars($title);
		$subject = $MSG['335'] . ' ' . $system->SETTINGS['sitename'] . ' ( ' . $MSG['337_1'] . ' ' . $item_title . ' ) ';
		$from_id = (!$user->logged_in) ? $user->user_data['email'] : $user->user_data['id'];
		$id_type = (!$user->logged_in) ? 'fromemail' : 'sentfrom';
		$emailer->email_uid = $winner;
		$emailer->email_sender($email, 'endauction_youwin_digital_item.inc', $subject);
	}
	
	function balance_limit($name, $balance, $id, $email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'SITENAME' => $system->SETTINGS['sitename'],

			'NAME' => $name,
			'BALANCE' => $system->print_money($balance, true, false),
			'OUTSTANDING' => $system->SETTINGS['siteurl'] . 'outstanding.php'
		));
		$emailer->email_uid = $id;
		$emailer->email_sender($email, 'suspended_balance.inc.php', $system->SETTINGS['sitename'] . ' - ' . $MSG['753']);
	}
	
	function item_watch_emails($name, $title, $bid, $id, $uid, $email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'REALNAME' => $name,
			'TITLE' => $title,
			'BID' => $system->print_money($bid, false),
			'AUCTION_URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($title) . '-' . $id
		));
		$emailer->email_uid = $uid;
		$emailer->email_sender($email, 'item_watch.inc.php', $system->SETTINGS['sitename'] . ' - ' . $MSG['472']);
	}
	
	function messages($subject, $sendto, $nowmessage, $from_email)
	{		
		$emailer = new email_handler();
		$emailer->email_basic($subject, $sendto, $nowmessage, $from_email);
	}
	
	function submit_new_ticket($subject, $nowmessage, $user_nick)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'SUBJECT' => $subject,
			'MESSAGE' => $nowmessage,
			'USER' => $user_nick,
			'REPLY' => $MSG['3500_1015439t'],
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'SITENAME' => $system->SETTINGS['sitename'],
		));
		$emailer->email_sender($system->SETTINGS['adminmail'], 'admin_ticket.inc', $MSG['335'] . ' ' . $system->SETTINGS['sitename'] . ' ( ' . $MSG['3500_1015439t'] . ' )');
	}
	
	function reply_to_ticket($subject, $nowmessage, $user_nick, $user_email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'SUBJECT' => $subject,
			'MESSAGE' => $nowmessage,
			'USER' => $user_nick,
			'REPLY' => ($user_email == $system->SETTINGS['adminmail']) ? $MSG['3500_1015439v'] : $MSG['3500_1015435'],
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'SITENAME' => $system->SETTINGS['sitename'],
		));
		$build_subtitle = ($user_email == $system->SETTINGS['adminmail']) ? $MSG['3500_1015439v'] : $MSG['3500_1015435'];
		$subtitle = $MSG['335'] . ' ' . $system->SETTINGS['sitename'] . ' ( ' . $build_subtitle . ' )';
		$emailer->email_sender($user_email, 'admin_ticket.inc', $subtitle);
	}
	
	function auction_watch($auction_id, $title, $name, $auc_watch, $id, $email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($title) . '-' . $auction_id,
			'SITENAME' =>  $system->SETTINGS['sitename'],
			'TITLE' => $title,
			'REALNAME' => $name,
			'KWORD' => $auc_watch
		));
		$emailer->email_uid = $id;
		$emailer->email_sender($email, 'auction_watchmail.inc.php', $system->SETTINGS['sitename'] . '  ' . $MSG['471']);
	}
	
	function auction_question($sender_name, $cleaned_question, $sender_email, $auction_id, $title, $seller_nick, $seller_id, $seller_email)
	{		
		global $system, $MSG, $user;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'SENDER_NAME' => $sender_name,
			'SENDER_QUESTION' => $cleaned_question,
			'SENDER_EMAIL' => $sender_email,
			'SITENAME' => $system->SETTINGS['sitename'],
			'SITEURL' => $system->SETTINGS['siteurl'],
			'AID' => $auction_id,
			'TITLE' => $title,
			'SELLER_NICK' => $seller_nick
		));
		$item_title = $system->uncleanvars($title);
		$subject = $MSG['335'] . ' ' . $system->SETTINGS['sitename'] . ' ' . $MSG['336'] . ' ' . $item_title;
		$from_id = (!$user->logged_in) ? $sender_email : $user->user_data['id'];
		$id_type = (!$user->logged_in) ? 'fromemail' : 'sentfrom';
		$emailer->email_uid = $seller_id;
		$emailer->email_sender($seller_email, 'send_email.inc.php', $subject);
	}
	
	function watch_emails($Auction_title, $id, $watchusers_name, $watchusers_id, $watchusers_email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
			'URL' => $system->SETTINGS['siteurl'] . 'products/' . generate_seo_link($Auction_title) . '-' . $id,
			'TITLE' => $Auction_title,
			'NAME' => $watchusers_name
		));
		$emailer->email_uid = $watchusers_id;
		$emailer->email_sender($watchusers_email, 'auctionend_watchmail.inc.php', $system->SETTINGS['sitename'] . ' - ' . $MSG['471']);
	}
	
	function payment_reminder($name, $balance, $id, $email)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'SITENAME' => $system->SETTINGS['sitename'],
				'LINK' => $system->SETTINGS['siteurl'] . 'outstanding.php',
				'C_NAME' => $name,
				'BALANCE' => $system->print_money($balance)
				));
		$emailer->email_uid = $id;
		$emailer->email_sender($email, 'payment_reminder.inc.php', $system->SETTINGS['sitename'] . ' ' . $MSG['766']);
	}
	
	function send_newsletter($id, $email, $content, $subject)
	{		
		global $system, $MSG;
		
		$emailer = new email_handler();
		$emailer->assign_vars(array(
				'SITEURL' => $system->SETTINGS['siteurl'],
				'SITENAME' => $system->SETTINGS['sitename'],
				'CONTENT' => $content,
				'SUBJECT' => $system->SETTINGS['sitename'] . ' ' . $MSG['25_0079'] . ' (' .$subject . ')'
				));
		$emailer->email_uid = $id;
		$emailer->email_sender($email, 'newsletter.inc.php', $system->SETTINGS['sitename'] . ' ' . $MSG['25_0079'] . ' (' .$subject . ')');
		return true;
	}
}
?>