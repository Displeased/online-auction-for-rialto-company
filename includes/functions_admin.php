<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


if (!defined('InuAuctions')) exit();

if (!defined('AdminFuncCall'))
{
	function checklogin()
	{
		global $_SESSION, $system, $DBPrefix, $db, $security;

		if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_NUMBER']) && isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']) && isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_PASS']))
		{
			$query = "SELECT hash, password FROM " . $DBPrefix . "adminusers WHERE password = :pass_id AND id = :admin_id";
			$params = array();
			$params[] = array(':pass_id', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_PASS']), 'str');
			$params[] = array(':admin_id', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']), 'int');
			$db->query($query, $params);

			if ($db->numrows() == 1)
			{
				$user_data = $db->result();

				if (strspn($user_data['password'], $user_data['hash']) == $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_NUMBER']))
				{
					return false;
					print_r('test');
				}
			}
		}
		return true;
	}

	function getAdminNotes()
	{
		global $_SESSION, $system, $DBPrefix, $db, $security;

		if (isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']) && isset($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_PASS']))
		{
			$query = "SELECT notes FROM " . $DBPrefix . "adminusers WHERE password = :admin_passw AND id = :admin_ids";
			$params = array();
			$params[] = array(':admin_passw', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_PASS']), 'str');
			$params[] = array(':admin_ids', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_ADMIN_IN']), 'int');
			$db->query($query, $params);
			$get_notes = $db->result('notes');
			if ($db->numrows() > 0)
			{	
				return $get_notes;
			}
		}
		return '';
	}

	function loadblock($title = '', $description = '', $type = '', $name = '', $default = '', $tagline = array(), $header = false)
	{
		global $template;

		$template->assign_block_vars('block', array(
				'TITLE' => $title,
				'DESCRIPTION' => (!empty($description)) ? $description . '<br>' : '',
				'TYPE' => $type,
				'NAME' => $name,
				'DEFAULT' => $default,
				'TAGLINE1' => (isset($tagline[0])) ? $tagline[0] : '',
				'TAGLINE2' => (isset($tagline[1])) ? $tagline[1] : '',
				'TAGLINE3' => (isset($tagline[2])) ? $tagline[2] : '',

				'B_HEADER' => $header
				));
	}
	
	// Display Date/Time
	function last_login($admin_time)
	{
		global $system, $MSG;
	
		$mth = 'MON_0' . gmdate($MSG['ta_m2'], $admin_time);
		if($system->SETTINGS['datesformat'] == 'EUR')
		{
			$date =  gmdate($MSG['ta_j'], $admin_time) . ' ' . $MSG[$mth] . ' ' . gmdate($MSG['ta_Y'], $admin_time);
		}
		else
		{
			$date = $MSG[$mth] . ' ' . gmdate($MSG['ta_j'] . ',' . $MSG['ta_Y'], $admin_time);
		}
		$time = $date . ' <span id="servertime">' . gmdate($MSG['ta_His'], $admin_time) . '</span>';
		
		return $time;
	}
	
	//Change the admin folder name
	function cheange_admin_folder($new_folder, $stored_admin_folder)
	{
		global $system, $MSG, $DBPrefix, $main_path, $db;
		
		//Changing the admin folder name
		$new_admin_folder = $main_path . $new_folder; 
		rename($stored_admin_folder, $new_admin_folder);
			
		//Updating the robots.txt file this helps prevent crawling 
		$replacing = str_replace('/' . $system->SETTINGS['admin_folder'] . '/', '/' . $new_folder . '/', file_get_contents('../robots.txt')); 
		file_put_contents('../robots.txt', $replacing); //edit the file
					
		//Updating the admin_folder column in the database
		$query = "UPDATE " . $DBPrefix . "settings SET admin_folder = :folder_path";
		$params = array();
		$params[] = array(':folder_path', $new_folder, 'str');
		$db->query($query, $params);
	
		$_SESSION['EROR'] = $MSG['30_0231'];
		$link = $system->SETTINGS['siteurl'] . $new_folder . '/security.php';
		return header('location: ' . $link);
		exit();
	}
	
	//Delete the admin note
	function delete_admin_note($admin_id)
	{
		global $system, $DBPrefix, $db;
	
		$query = "UPDATE " . $DBPrefix . "adminusers SET notes = :setnote WHERE id = :admin_id";
		$params = array();
		$params[] = array(':setnote', ' ', 'str');
		$params[] = array(':admin_id', $admin_id, 'int');
		$db->query($query, $params);
	}
	
	//add a admin note
	function add_admin_note($admin_id,$note)
	{
		global $system, $DBPrefix, $db;
		
		$query = "UPDATE " . $DBPrefix . "adminusers SET notes = :setnote WHERE id = :admin_id";
		$params = array();
		$params[] = array(':setnote', stripslashes($system->cleanvars($note)), 'str');
		$params[] = array(':admin_id', $admin_id, 'int');
		$db->query($query, $params);
	}
	
	function check()
	{	
		// version check
		if (!($realversion1 = load_file_from_url('https://www.u-auctions.com/products/freeversion.txt'))) //getting the updated version from www.u-auctions.com server with SSL
		{
			if (!($realversion2 = load_file_from_url('http://www.u-auctions.com/products/freeversion.txt'))) //getting the updated version from www.u-auctions.com server with none SSL 
			{
				return 'Unknown';
			}
			else
			{
				return $realversion2;
			}
		}
		else
		{
			return $realversion1;
		}
	}

	function version_check($realversion)
	{
		global $system;
		
		if ($realversion != $system->SETTINGS['version'])
		{
			$myversion = '<span style="color:red;">' . $system->SETTINGS['version'] . '</span>';
		}
		else
		{
			$myversion = '<span style="color:green;">' . $system->SETTINGS['version'] . '</span>';
		}
		return $myversion;
	}
	//Do not remove this code
	function httpPost($url)
	{
		global $system;
		$params = array(
		"action" => 'api-check_donation',
	   	"site_name" => $system->SETTINGS['sitename'],
	   	"site_url" => $system->SETTINGS['siteurl'],
	   	"site_admin" => $system->SETTINGS['adminmail']
		);

	  $postData = '';
	   //create name value pairs seperated by &
	   foreach($params as $k => $v) 
	   { 
	      $postData .= $k . '='.$v.'&'; 
	   }
	   rtrim($postData, '&');	 
	    $ch = curl_init();  	 
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	    curl_setopt($ch,CURLOPT_POST, count($postData));
	    curl_setopt($ch,CURLOPT_POSTFIELDS, $postData);    
	    $output=curl_exec($ch);	 
	    curl_close($ch);
	}
	httpPost("http://u-auctions.com/api/check.php");
			

	define('AdminFuncCall', 1);
}
?>