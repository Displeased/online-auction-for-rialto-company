<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/


if (!defined('InuAuctions')) exit();

include $include_path . 'currencies.php';

function CurrenciesList()
{
	global $system, $DBPrefix, $db;

	$query = "SELECT * FROM " . $DBPrefix . "rates";
	$db->direct_query($query);
	$CURRENCIES = array();
	while ($row = $db->result())
	{
		$CURRENCIES[$row['symbol']] = $row['valuta'];
	}
	return $CURRENCIES;
}

function ConvertCurrency($FROM, $INTO, $AMOUNT)
{
	global $conversionarray;

	$data = array(
		'amount'	=> $AMOUNT,
		'from' 		=> $FROM,
		'to' 		=> $INTO
		);
	if ($FROM == $INTO) return $AMOUNT;

	$CURRENCIES = CurrenciesList();

	$rate = findconversionrate($FROM, $INTO);
	if ($rate == 0)
	{
		$conversion = googleconvert($AMOUNT, $FROM, $INTO);
		$conversionarray[1][] = array('from' => $FROM, 'to' => $INTO, 'rate' => $conversion);
		buildcache($conversionarray[1]);
		return $AMOUNT * $conversion;
	}
	else
	{
		return $AMOUNT * $rate;
	}
}

function buildcache($newaarray)
{
	global $include_path;

	$output_filename = $include_path . 'currencies.php';
	$output = "<?php\n";
	$output.= "\$conversionarray[] = '" . time() . "';\n";
	$output.= "\$conversionarray[] = array(\n";

	for ($i = 0; $i < count($newaarray); $i++)
	{
		$output .= "\t" . "array('from' => '" . $newaarray[$i]['from'] . "', 'to' => '" . $newaarray[$i]['to'] . "', 'rate' => '" . floatval($newaarray[$i]['rate']) . "')";
		if ($i < (count($newaarray) - 1))
		{
			$output .= ",\n";
		}
		else
		{
			$output .= "\n";
		}
	}

	$output .= ");\n?>\n";

	$handle = fopen($output_filename, 'w');
	fputs($handle, $output);
	fclose($handle);
}

function findconversionrate($FROM, $INTO)
{
	global $conversionarray;

	if (time() - (3600 * 24) < $conversionarray[0])
	{
		for ($i = 0; $i < count($conversionarray[1]); $i++)
		{
			if ($conversionarray[1][$i]['from'] == $FROM && $conversionarray[1][$i]['to'] == $INTO)
				return $conversionarray[1][$i]['rate'];
		}
	}
	else
	{
		$conversionarray = array(0, array());
	}
	return 0;
}

function googleconvert($amount, $fromCurrency , $toCurrency)
{
	//Call Google API
	$fromCurrency = urlencode($fromCurrency);
	$toCurrency = urlencode($toCurrency);
	$get = file_get_contents("https://www.google.com/finance/converter?a=1&from=$fromCurrency&to=$toCurrency");
	$get = explode("<span class=bld>",$get);
	$get = explode("</span>",$get[1]);  
	$converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);	
	return $converted_amount;
}
?>