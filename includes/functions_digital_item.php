<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

if (!defined('InuAuctions')) exit('Access denied');

function upload_digital_item()
{
	global $system, $db, $DBPrefix, $upload_path, $_FILES, $_SESSION;
	
	$max_size = formatSizeUnits($system->SETTINGS['dites_upload_size']); 
	//makeing some salt codes for the folder dir
	$session_dir = $upload_path . session_id();
	$updir = $upload_path . session_id() . '/' . 'items';   // sets the folder

	// If the folder from $updir or $session_dir not exists, attempts to create it (with mkdir or CHMOD 0777)
	if (is_dir($session_dir) == false) mkdir($session_dir, 0755);
	if (is_dir($session_dir) == false) chmod($session_dir, 0755);
	if (is_dir($updir) == false) mkdir($updir, 0755);
	if (is_dir($updir) == false) chmod($updir, 0755);

	if ($dir = opendir($updir))
	{
		while (($file = readdir($dir)) !== false)
		{
			if (!is_dir($updir . '/' . $file))
			unlink($updir . '/' . $file);
		}
		closedir($dir);
	}					

	// file types allowed
	// Control parameters and file existence
	$query = "SELECT file_extension FROM " . $DBPrefix . "digital_item_mime WHERE use_mime = 'y'";
	$db->direct_query($query);
	$allowtype = array();
	while ($row = $db->fetch())
	{
		$allowtype[] = $row['file_extension'];
	}	
	
	// Variable to store the messages that will be returned by the script
	$rezultat = array();

	/** Code for. Uploading files to server **/
	// If is received a valid file from the form
	if (isset($_FILES['file_up'])) 
	{
		// Traverse the array elements, with data from the form fields with name="file_up[]"
		// Check the files received for upload
		if(count($_FILES['file_up']['name']) > 0)
		{
			for($f=0; $f<count($_FILES['file_up']['name']); $f++) 
			{
			    $nume_f = $_FILES['file_up']['name'][$f];     // get the name of the current file
			    // if the name has minimum 4 characters
			    if (strlen($nume_f)>3) 
			    {
			      	// get and checks the file type (extension)
			      	//we have to put the explode code like this or
			      	//we will get an error
			      	$split = explode('.', strtolower($nume_f));
			      	$type = end($split);
			      	if (in_array($type, $allowtype)) 
			      	{
			        	// check the file size
			        	if ($_FILES['file_up']['size'][$f] <= $system->SETTINGS['dites_upload_size'] * 1024) 
			        	{
			          		// if there aren't errors in the copying process
			          		if ($_FILES['file_up']['error'][$f]==0) 
			          		{
			            		// Set location and name for uploading file on the server
			            		$thefile = $updir . '/'. $nume_f;
			            		$_SESSION['SELL_upload_file'] = $nume_f;
			            		// if the file can't be uploaded, returns a message
			            		if (!move_uploaded_file ($_FILES['file_up']['tmp_name'][$f], $thefile)) 
			            		{
			              			$rezultat[$f] = 'The file '. $nume_f. ' could not be copied, try again<br>'.$thefile;
			              			$di_message = $rezultat[$f];	
			            		}
			            		else 
			            		{
			              			// stores the name of the file
			              			$rezultat[$f] = '<b>New File Uploaded:</b>&nbsp;&nbsp;'.$nume_f.'';
			              			$di_message = $rezultat[$f]; 
			            		}
			          		}
			          		else 
			          		{ 
			          			$rezultat[$f] = 'There was an Error with <b>'. $nume_f. '.</b><br>Your File was not uploaded please try again.'; 
			          			$di_message = $rezultat[$f];
			          		}
			        	}
			        	else 
			        	{ 
			        		$rezultat[$f] = 'The file <b>'. $nume_f. '</b> exceeds the maximum permitted size, <i>'. $max_size. 'KB</i>'; 
			        		$di_message = $rezultat[$f];
			        	}
			      	}
			      	else 
			      	{ 
			      		$rezultat[$f] = 'The file ('. $nume_f. ') is not an allowed file type'; 
			      		$di_message = $rezultat[$f];
			      	}
				}
			   	else 
			  	 { 
			   		$rezultat[$f] = 'The file name must have more than 4 characters long'; 
		    		$di_message = $rezultat[$f];
		    	}
			}
		}
		else 
		{ 
			$rezultat[$f] = 'No file found'; 
	    	$di_message = $rezultat[$f];
		}
	}
	else 
	{ 
		$rezultat[$f] = 'No file found'; 
	    $di_message = $rezultat[$f];
	}
	return $di_message;
}

// Preparing to send the file and checking the in coming data
function download_digital_item($hash, $passed)
{
	global $system, $db, $upload_path, $DBPrefix, $security;
		
	// Making sure that the $passed has data and is set to word 'passed'
	if($passed == 'passed')
	{
		// Making sure that the $hash has data
		if (isset($hash))
		{
			// Getting the correct data using the hash that was sent
			$query = "SELECT seller, auctions, item FROM " . $DBPrefix . "digital_items WHERE hash = :id";
			$params = array();
			$params[] = array(':id', $security->decrypt($hash), 'str');
			$db->query($query, $params);

			// Making sure there was a file stored in the database
			if ($db->numrows() == 1)
			{
				// Getting data from the database that is needed
				$data = $db->result();
				$filename = $data['item'];
				$file = 'uploaded/items/' . $data['seller'] . '/' . $data['auctions'] . '/' . $filename;
				
				// Making sure that the $filename has data
				if (isset($filename))
				{
					if (!file_exists($file))
					{
						header('location: ' . $system->SETTINGS['siteurl'] . 'home');
						return;
					}
					else
    				{
						if(!is_readable($file)) 
						{
							header('location: ' . $system->SETTINGS['siteurl'] . 'home');
							return;
					    }
					    else 
					    {	
					    	// braking down the file name to get the file existence	
					    	$file_extension = strtolower(substr(strrchr($filename,"."),1));
					    	
					    	// Control parameters and file existence
					    	$query = "SELECT mine_type FROM " . $DBPrefix . "digital_item_mime WHERE file_extension = :extexn AND use_mime = 'y'";
							$params = array();
							$params[] = array(':extexn', $file_extension, 'str');
							$db->query($query, $params);
							if($db->numrows() == 1)
							{
								$ctype = $db->result('mine_type');
							}		
							if (!empty($ctype))
							{
								header('Content-Description: File Transfer'); 
								header('Pragma: public');
								header('Expires: 0');
								header('Cache-Control: must-revalidate, no-cache'); 
								header('Content-Type: ' . $ctype);
								header('Content-Disposition: attachment; filename="' . $filename . '"'); 
								header('Content-Transfer-Encoding: binary');
								header("Content-Length: " . filesize($file));  
								ob_clean();
            					flush();
            					readfile($file);
								exit;
							}
							else
							{
								// kill the page if the $check did not match the data
								header('location: ' . $system->SETTINGS['siteurl'] . 'home');
								exit;
							}	    
					    }
    				}
				}
			}
		}
	}
	else {
		// kill the page if the $check did not match the pre-set data
		header('location: ' . $system->SETTINGS['siteurl'] . 'home');
		exit;
	}

}

?>