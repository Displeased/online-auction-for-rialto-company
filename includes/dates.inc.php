<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

if (!defined('InuAuctions')) exit('Access denied');

if (!function_exists('GetLeftSeconds'))
{
	function GetLeftSeconds()
	{
		global $MSG;
		
		$today = explode('|', gmdate('j|n|Y|G|i|s'));
		$month = $today[1];
		$mday = $today[0];
		$year = $today[2];
		$lday = 31;
		// Calculate last day
		while (!checkdate($month, $lday, $year))
		{
			$lday--;
		}
		// Days left t the end of the month
		$daysleft = intval($lday - gmdate($MSG['ta_d']));
		$hoursleft = 24 - $today[3];
		$minsleft = 60 - $today[4];
		$secsleft = 60 - $today[5];
		$left = $secsleft + ($minsleft * 60) + ($hoursleft * 3600) + ($daysleft * 86400);
		
		return $left;
	}
}

if (!function_exists('FormatDate'))
{
	function FormatDate($DATE, $spacer = '/', $GMT = true)
	{
		global $system, $MSG;

		if (!$GMT)
		{
			$DATE += $system->tdiff;
		}

		if ($system->SETTINGS['datesformat'] == 'USA')
		{
			$F_date = gmdate($MSG['ta_m'] . $spacer . $MSG['ta_d'] . $spacer . $MSG['ta_Y'], $DATE);
		}
		else
		{
			$F_date = gmdate($MSG['ta_d'] . $spacer . $MSG['ta_m'] . $spacer . $MSG['ta_Y'], $DATE);
		}
		return $F_date;
	}
}

if (!function_exists('FormatTimeStamp'))
{
	function FormatTimeStamp($DATE, $spacer = '/')
	{
		global $system;

		$DATE = explode($spacer, $DATE);
		if ($system->SETTINGS['datesformat'] == 'USA')
		{
			$F_date = _gmmktime(0, 0, 0, $DATE[0], $DATE[1], $DATE[2]);
		}
		else
		{
			$F_date = _gmmktime(0, 0, 0, $DATE[1], $DATE[0], $DATE[2]);
		}
		return $F_date;
	}
}

if (!function_exists('FormatTimeLeft'))
{
	function FormatTimeLeft($diff)
	{
		global $MSG;

		$days_difference = floor($diff / 86400);
		$difference = $diff % 86400;
		$hours_difference = floor($difference / 3600);
		$difference = $difference % 3600;
		$minutes_difference = floor($difference / 60);
		$seconds_difference = $difference % 60;
		$secshow = false;
		$timeleft = '';

		if ($days_difference > 0)
		{
			$timeleft = $days_difference . $MSG['ta_d'];
		}
		if ($hours_difference > 0)
		{
			$timeleft .= $hours_difference . $MSG['ta_h'];
		}
		else
		{
			$secshow = true;
		}
		if ($diff > 60)
		{
			$timeleft .= $minutes_difference . $MSG['ta_m'];
		}
		elseif ($diff > 60 && !$seconds)
		{
			$timeleft = '<1m';
		}
		if ($secshow)
		{
			$timeleft .= $seconds_difference . $MSG['ta_s'];
		}
		if ($diff < 0)
		{
			$timeleft = $MSG['911'];
		}
		if (($diff * 60) < 15)
		{
			$timeleft = '<span style="color:#FF0000;">' . $timeleft . '</span>';
		}

		return $timeleft;
	}
}

//-- Date and time hanling functions
if (!function_exists('ActualDate'))
{
	function ActualDate()
	{
		global $system, $MSG;
		return gmdate($MSG['ta_Md'] . ', ' . $MSG['ta_YHis'], $system->ctime);
	}
}

if (!function_exists('ArrangeDateNoCorrection'))
{
	function ArrangeDateNoCorrection($DATE)
	{
		global $MSG, $system;
		$mth = 'MON_0' . gmdate($MSG['ta_m2'], $DATE);
		if ($system->SETTINGS['datesformat'] == 'USA')
		{
			$return = $MSG[$mth] . ' ' . gmdate($MSG['ta_d'] . ', ' . $MSG['ta_YHi'], $DATE);
		}
		else
		{
			$return = gmdate($MSG['ta_d'], $DATE) . ' ' . $MSG[$mth] . ', ' . gmdate($MSG['ta_YHi'], $DATE);
		}
		return $return;
	}
}
?>