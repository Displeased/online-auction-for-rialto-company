<?php 

include 'common.php';
include $main_path . 'language/' . $language . '/messages.inc.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	header('location: user_login.php');
	exit;
}
//Get seller info
function getseller($user_id) 
{
	global $_SESSION, $system, $DBPrefix, $db;
		
		$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :seller_id";
		$params = array();
		$params[] = array(':seller_id', (int)$user_id, 'int');
		$db->query($query, $params);
		$result = $db->result(); 
		$address_data = array(
		    //Just remove the  // to enable to show on page
			//'user_id'   => $result['id'],
			//'nick'      => $result['nick'],
			'name'      => $result['name'],
			'company'   => (isset($result['company'])) ? $result['company'] : '',
			'address'   => $result['address'],
			'city'      => $result['city'],
			'prov'      => $result['prov'],
			'postcode'  => $result['zip'],
			'country'   => $result['country'],
			'email'     => $result['email'],
		);
	return $address_data;
}

//Get winner info
function getwinner($user_id) 
{
	global $_SESSION, $system, $DBPrefix, $db;
		
		$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :winner_id";
		$params = array();
		$params[] = array(':winner_id', (int)$user_id, 'int');
		$db->query($query, $params);
		$result = $db->result(); 
		$address_data = array(
		    //Just remove the  // to enable to show on page
			//'user_id'   => $result['id'],
			//'nick'      => $result['nick'],
			'name'      => $result['name'],
			'company'   => (isset($result['company'])) ? $result['company'] : '',
			'address'   => $result['address'],
			'city'      => $result['city'],
			'prov'      => $result['prov'],
			'postcode'  => $result['zip'],
			'country'   => $result['country'],
			'email'     => $result['email'],
		);
	return $address_data;
}	

$query = "SELECT w.id, w.winner, w.closingdate, w.seller, w.bid, a.id AS auctid, a.title, a.subtitle, a.auction_type, a.bn_only, a.shipping_cost, a.shipping_cost_additional, w.qty, a.payment, u.id As uid, u.rate_sum, d.hash, d.auctions
	FROM " . $DBPrefix . "auctions a
	LEFT JOIN " . $DBPrefix . "winners w ON (a.id = w.auction)
	LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.seller)
	LEFT JOIN " . $DBPrefix . "digital_items d ON (a.id = d.auctions)
	WHERE a.id = :pfval AND w.id = :pfwon";
$params = array();
$params[] = array(':pfval', intval($_POST['pfval']), 'int');
$params[] = array(':pfwon', intval($_POST['pfwon']), 'int');
$db->query($query, $params);

//check its real
if ($db->numrows() < 1)
{
	header('location: selling.php');
	exit;
}
        
$data = $db->result();
$sender = getseller(intval($data['seller']));
foreach($sender as $k => $v)
{
	if ($v !== '')
	{
		$sendadd .= "$v<br />";
	}
}
$winner = getwinner(intval($data['winner']));
foreach($winner as $k => $v)
{
	if ($v !== '')
	{
    	$winneradd .= "$v<br />";
    }
}
$item_quantity = $data['qty'];
$title = $data['title'];
$subtitle = $data['subtitle'];
$sale = intval($_POST['pfwon']);
$bid = $data['bid'];
$qty = $data['qty'];
$qaty = $data['qty'];
$qauty = $data['qty'];
$qanty = $data['qty'];
$qaty = $data['qty'];
$additional_shipping = $data['shipping_cost_additional'];
$shipping_cost = $data['shipping_cost'];
$additional_shipping_cost = $qty - 1;
$dadditional_shipping_cost = $qty - 1;
		
//-----rating------	
include 'includes/' . 'membertypes.inc.php';
foreach ($membertypes as $idm => $memtypearr)
{$memtypesarr[$memtypearr['feedbacks']] = $memtypearr;}
ksort($memtypesarr, SORT_NUMERIC);
$TPL_rate_ratio_value = '';
	foreach ($memtypesarr as $k => $l)
	{
		if ($k >= $data['rate_sum'] || $l++ == (count($memtypesarr) - 1))
		{
			$TPL_rate_ratio_value = "images/icons/" . $l['icon'] ."";
			break;
		}
	} 
//----------rating end-------

// payment methods------
$payment = explode(', ', $data['payment']);
$payment_methods = '';
$query = "SELECT * FROM " . $DBPrefix . "gateways";
$db->direct_query($query);
$gateways_data = $db->result();
$gateway_list = explode(',', $gateways_data['gateways']);
$p_first = true;
foreach ($gateway_list as $v)
{
	$v = strtolower($v);
	if ($gateways_data[$v . '_active'] == 1 && _in_array($v, $payment))
	{
		if (!$p_first)
		{
			$payment_methods .= ', ';
		}
		else
		{
			$p_first = false;
		}
		$payment_methods .= $system->SETTINGS['gatways'][$v];
	}
}

$payment_options = unserialize($system->SETTINGS['payment_options']);
foreach ($payment_options as $k => $v)
{
	if (_in_array($k, $payment))
	{
		if (!$p_first)
		{
			$payment_methods .= ', ';
		}
		else
		{
			$p_first = false;
		}
		$payment_methods .= $v;
	}
}
// payment methods ends

$template->assign_vars(array(
	'CHARSET' => $CHARSET,
	'LANGUAGE' => $language,
	'SENDER' => $sendadd,
	'THEME' => $system->SETTINGS['theme'],
	'INCURL' => $system->SETTINGS['siteurl'],
	'WINNER' => $winneradd,
	'AUCTION_TITLE' => strtoupper($title),
	'QTY' => $qty,
	'AUCTION_ID' => $data['auctid'],
	'RATE_SUM' => $data['rate_sum'],
	'RATE_RATIO' => $TPL_rate_ratio_value,
	'SHIPPING_METHOD' => "N/A",
	'BIDF' => $system->print_money($bid * $qty),
	'SHIPPING' => $system->print_money($shipping_cost),
	'ADDITIONAL_SHIPPING_COST' => $system->print_money($data['shipping_cost_additional'] * $qty = $dadditional_shipping_cost),
	'ATOTAL' => $system->print_money($shipping_cost + $bid * $qaty + $additional_shipping * $qanty = $additional_shipping_cost),
	'DOCDIR' => $DOCDIR, // Set document direction (set in includes/messages.XX.inc.php) ltr/rtl	 		 
	'DADDITIONAL_SHIPPING_COST' => $system->print_money($data['shipping_cost_additional'] * $qaty = ($dadditional_shipping_cost)),		 
	'DTOTAL' => $system->print_money($shipping_cost + ($data['bid'] * $data['qty']) + ($data['shipping_cost_additional'] * $qaty)),
	'DIGITAL_ITEM_QUANTITY' => 1,
	'DIGITAL_ITEM_SHIPPING' => '0.00',
	'TOTAL' => $system->print_money($shipping_cost + $bid * $data['qty']),
	'PAYMENT_METHOD' => $payment_methods,	
	'SALE_DATE' => "N/A",
 	'SUBTITLE' => strtoupper($subtitle ),
	'CLOSING_DATE' => ArrangeDateNoCorrection($data['closingdate']),
	'DOWNLOAD' => $system->SETTINGS['siteurl'] . 'my_downloads.php?diupload=3&fromfile=' . $data['hash'],
	'PAYMENT' => $data['payment'],
	'ITEM_QUANTITY' => $data['qty'],
	'ORDERS' => 1,  //can link to an if statment or something to show else part in html
	'LOGO' => ($system->SETTINGS['logo']) ? '<a href="' . $system->SETTINGS['siteurl'] . 'home"><img src="' . $system->SETTINGS['siteurl'] . 'uploaded/logos/' . $system->SETTINGS['logo'] . '" border="0" alt="' . $system->SETTINGS['sitename'] . '"></a>' : '&nbsp;',

		
	'B_BUY_NOW_ONLY' => $data['bn_only'] == 'y',
	'B_DUCH' => $data['auction_type'] == 2,
	'B_STANDARD' => $data['auction_type'] == 1,
	'B_DIGITAL' => $data['auction_type'] == 3
));

$template->set_filenames(array(
		'body' => 'order_packingslip.tpl'
		));
$template->display('body');