<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////
//// Connect with facebook Mod ////////// E ///// I left nots and labeled most ////////
///////////////////////////////////////// N ///// of the codes to tell you what ///////
///////////////////////////////////////// J ///// they are for and what they do. //////
//// There is 4 different parts ///////// O ///// You set your app id and app /////////
///////////////////////////////////////// Y
////// part 1 starts here ///////////////
/////////////////////////////////////////
////// Get user info from facebook //////
/////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////////////
///////// This code only runs if the connect with ///
///////// Facebook button has been clicked        ///
///////// on in the register.php, index.php       ///
///////// or user_menu.php and edit_data.php pages //
/////////////////////////////////////////////////////
if ($system->SETTINGS['facebook_login'] == 'y' && isset($_GET["fbconnect"]))
{
	switch($_GET["fbconnect"]) 
	{
		case "fblogin": 
			include_once 'inc/facebook/facebook.php';  
			$facebook   = new Facebook(array( 
				'appId' => $system->SETTINGS['facebook_app_id'], 
			    'secret' => $system->SETTINGS['facebook_app_secret'], 
			    'cookie' => TRUE, 
			 ));
			 if ($facebook->getUser()) 
			 { 
			 	try 
			    { 
			    	$fb_profile = $facebook->api('/me'); 
			    } 
			    catch (Exception $e) 
			    { 
			    	$ERR = $e->getMessage(); 
			        exit(); 
			    } 
			    //////Get the person facebook info from facebook and set them in to salts
			    $fb_user_fbids = isset($fb_profile["id"]) ? $fb_profile["id"] : ''; 
			    $fbuser_email = isset($fb_profile["email"]) ? $fb_profile["email"] : ''; 
			    $fbuser_name = isset($fb_profile["name"]) ? $fb_profile["name"] : ''; 
			    $fbuser_address = isset($fb_profile["location"]) ? $fb_profile["location"] : ''; 
			    $fbuser_phone = isset($fb_profile["phone"]) ? $fb_profile["phone"] : ''; 
			    $fbuser_status = isset($fb_profile["status"]) ? $fb_profile["status"] : ''; 
			    $fbuser_birthday = isset($fb_profile["birthday"]) ? $fb_profile["birthday"] : ''; 
			    $fbuser_image = isset($fb_profile["id"]) ? "https://graph.facebook.com/" . $fb_user_fbids . "/picture?type=large" : ''; 
			    $post_time = time();  

				/////////This will check to see if the person has a stored FB id in the FB sql table  
				/////////If there is no stored FB id in the FB sql table then it will make a new 
				/////////FB id column in the FB sql table 
				$query = "SELECT fb_id FROM " . $DBPrefix . "fblogin WHERE email = :fb_email AND name = :fb_name"; 
				$params = array();
				$params[] = array(':fb_email', $fbuser_email, 'str');
				$params[] = array(':fb_name', $fbuser_name, 'str');
				$db->query($query, $params); 
				        
				/// This checks to see if there is a 
				/// facebook id already stored in the  
				/// sql and if there is no facebook id
				/// stored in the sql then this will 
				/// run and store there facebook id
				if ($db->numrows('fb_id') <= 0) 
				{ 
					$query = "INSERT INTO " . $DBPrefix . "fblogin (fb_id, name, email, image, postdate, address, phone, birthday, status) VALUES (:user_id, :user_name, :user_email, :user_avatar, :user_time, :user_address, :user_phone, :user_birthday, :user_status)"; 
				    $params = array();
					$params[] = array(':user_id', $fb_user_fbids, 'str');
					$params[] = array(':user_name', $fbuser_name, 'str');
					$params[] = array(':user_email', $fbuser_email, 'str');
					$params[] = array(':user_avatar', $fbuser_image, 'str');
					$params[] = array(':user_time', $post_time, 'int');
					$params[] = array(':user_address', $fbuser_address, 'str');
					$params[] = array(':user_phone', $fbuser_phone, 'str');
					$params[] = array(':user_birthday', $fbuser_birthday, 'str');
					$params[] = array(':user_status', $fbuser_status, 'str');
					$db->query($query, $params);		
		            $fb_users_id = $fb_user_fbids;
		        } 
		        else 
		        {
					$fb_users_id = $db->result('fb_id');
				}
				if ($_SESSION['REDIRECT_AFTER_LOGIN'] == 'new_account') 
				{       
					//// Make new session's that will be used in the register page  
					$_SESSION['FBOOK_USER_EMAIL'] = $fbuser_email; 
			        $_SESSION['FBOOK_USER_NAME'] = $fbuser_name;
			        $_SESSION['FBOOK_USER_IDS'] = $fb_users_id;
			        $_SESSION['FBOOK_USER_IMAGE'] = $fbuser_image;
				}       
				
				$query = "SELECT id, fblogin_id FROM " . $DBPrefix . "users WHERE fblogin_id = :fb_id";
				$params = array();
				$params[] = array(':fb_id', $fb_users_id, 'int');
				$db->query($query, $params);
				$fb_wb['id'] = 0;
				if($db->numrows() >= 0)
				{		            
					$fb_wb = $db->result();
					if (isset($fb_wb['id']) && $fb_wb['fblogin_id'] == $fb_users_id)
					{    
						//// make the session that will be needed to see
				    	//// if the user has a stored facebook id in there sql column 
				    	$_SESSION['FB_WB'] = $fb_wb['id'];
				    	$_SESSION['FBOOK_USER_IDS'] = $fb_users_id;
				    	
					}
				}
								
				if (isset($_SESSION['REDIRECT_AFTER_LOGIN']))
				{	
					if(isset($_SESSION['FB_WB']))
				    {
				        $URL = str_replace('\r', '', str_replace('\n', '', $_SESSION['REDIRECT_AFTER_LOGIN']));
				    }
				    elseif ($_SESSION['REDIRECT_AFTER_LOGIN'] == 'new_account')
				    {
				        $URL = $system->SETTINGS['siteurl'] . 'new_account';;
				    }
				    elseif ($_SESSION['REDIRECT_AFTER_LOGIN'] == 'edit_data.php')
				    {
				    	$URL = $system->SETTINGS['siteurl'] . 'edit_data.php';
				    	$_SESSION['FACEBOOK_LINKED'] = $MSG['3500_1015413'];
				    }
				    else
				    {
				    	$URL = $system->SETTINGS['siteurl'] . 'new_account';
				    }
				}
				else
				{
					$URL = $system->SETTINGS['siteurl'] . 'new_account';
				         
				}             
			}
	    	break;  
		     
			////////////////////////////////////////
			//// Part 2 starts here         	 ///
			//// unlink facebook with uAuctions  ///
			////////////////////////////////////////
			case "unlinked":
				$query = "SELECT fblogin_id FROM " . $DBPrefix . "users WHERE id = :user_id";
				$params = array();
				$params[] = array(':user_id', $user->user_data['id'], 'int');
				$db->query($query, $params);			    
				$unlink_fb = $db->result('fblogin_id');
				
				if (isset($unlink_fb))
				{
				    /// Delete the user facebook column that is stored in the facebook table in uAuctions sql
				    $query = "DELETE from " . $DBPrefix . "fblogin WHERE fb_id = :user_fb_id";
				    $params = array();
					$params[] = array(':user_fb_id', $unlink_fb, 'int');
					$db->query($query, $params);
				        
				    //// Delete the facebook id from the user column that will be turned to 0
				    $query = "UPDATE " . $DBPrefix . "users SET fblogin_id = :set_id WHERE id = :user_id";
				    $params = array();
				    $params[] = array(':set_id', 0, 'int');
					$params[] = array(':user_id', $user->user_data['id'], 'int');
					$db->query($query, $params);
				          
				    /// Making sure all facebook sessions are all deleted  
				    unset($_SESSION['FBOOK_USER_IDS']);
				    unset($_SESSION['FB_WB']);
				    unset($_SESSION['FBOOK_USER_EMAIL']);
				    unset($_SESSION['FBOOK_USER_NAME']);
					unset($_SESSION['FBOOK_USER_IMAGE']);
				}
			$URL = 'edit_data.php';      
		break;
		/// Part 2 ends here   ///
		///////////////////////////
	}
	//////Part 1 ends here  //////
	////////////////////////////////
	/////////////////////////////////
	//////////////////////////////////
	//// Part 3 starts here ////////////
	//////////////////////////////////////
	//// Facebook login code that turns // 
	//// in to uAuctions  login code. ////
	//////////////////////////////////////
	//// checking to see if the user has the FBOOK_USER_IDS session
	if(isset($_SESSION['FBOOK_USER_IDS']))
	{
		//// checking to see if the user has the FB_WB session
		if (isset($_SESSION['FB_WB']))
		{
		    ////turning the session in to a salt
			$checkusersfb = $_SESSION['FBOOK_USER_IDS'];
			        
			////Check the users sql table to see if a user has the same FB id
			////and if there is a user with the same FB id it will log them in
		    $query = "SELECT id, hash, suspended, password FROM " . $DBPrefix . "users WHERE fblogin_id = :check_id";
		    $params = array();
			$params[] = array(':check_id', $checkusersfb, 'int');
			$db->query($query, $params);
			        
		    /// From here down is what turns the facebook 
		    /// login in to uAuctions login.
		    if ($db->numrows() == 1)
		    {   
		    	$user_data = $db->result(); 
		    	$_SESSION['csrftoken'] = $security->encrypt($security->genRandString(32));
		        if ($user_data['suspended'] == 9)
			    {
		        	$_SESSION['signup_id'] = $user_data['id'];
		            header('location: pay.php?a=3');
		            exit;
		        }
			            
			    /// Here we are checking the user account
			    /// to see if the account is suspended
			    if ($user_data['suspended'] == 1)
			   	{
			    	$ERR = $ERR_618;
			    }
			    elseif ($user_data['suspended'] == 8)
			    {
			    	$ERR = $ERR_620;
			   	}
			    elseif ($user_data['suspended'] == 10)
			    {
			    	$ERR = $ERR_621;
			    }
			    else
			    {
			     	/// Here we are making the uAuctions session's to tell
			        /// uAuctions that we are loged in
			        $_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']         = $security->encrypt($user_data['id']);
			        $_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_NUMBER']     = $security->encrypt(strspn($user_data['password'], $user_data['hash']));
			        $_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_PASS']       = $security->encrypt($user_data['password']);
			                        
			        // Update "last login" fields in users table
			        $query = "UPDATE " . $DBPrefix . "users SET lastlogin = :date WHERE id = :user_id";
					$params = array();
					$params[] = array(':date', gmdate("Y-m-d H:i:s"), 'str');
					$params[] = array(':user_id', $user_data['id'], 'int');
					$db->query($query, $params);

		            ////check ip
		            $query = "SELECT id FROM " . $DBPrefix . "usersips WHERE USER = :user_id AND ip = :user_ip";
					$params = array();
					$params[] = array(':user_ip', $_SERVER['REMOTE_ADDR'], 'str');
					$params[] = array(':user_id', $user_data['id'], 'int');
					$db->query($query, $params);
			        if ($db->numrows() == 0)
			        {
		            	$query = "INSERT INTO " . $DBPrefix . "usersips VALUES (NULL, :user_id, :user_ip, 'after','accept')";
						$params = array();
						$params[] = array(':user_ip', $_SERVER['REMOTE_ADDR'], 'str');
						$params[] = array(':user_id', $user_data['id'], 'int');
						$db->query($query, $params);
			        }
			            
			        // delete your old session
			        if (isset($_COOKIE[$system->SETTINGS['cookie_name'] . '-ONLINE']))
			        {
			        	$query = "DELETE from " . $DBPrefix . "online WHERE SESSION = :SESSION";
						$params = array();
						$params[] = array(':SESSION', alphanumeric($_COOKIE[$system->SETTINGS['cookie_name'] . '-ONLINE']), 'str');
						$db->query($query, $params);			                
					}
					
					$query = "UPDATE " . $DBPrefix . "users SET hide_online = :hide WHERE id = :user_id";
					$params = array();
					$params[] = array(':user_id', $user_data['id'], 'int');
					$params[] = array(':hide', 'n', 'bool');
					$db->query($query, $params);

			                
			        /// Here we are unset unneeded session's
			        unset($_SESSION['FBOOK_USER_EMAIL']);
			        unset($_SESSION['FBOOK_USER_NAME']);
			        unset($_SESSION['FBOOK_USER_IDS']);
				}
			}
		}
	}
		
	//$URL = ''; 
	if (isset($URL)) 
	{
		header('location: ' . $URL);
		exit();
	}
}      
/////Part 3 ends here //////////
////////////////////////////////
////////////////////////////////
//// Part 4 starts here ////////
//// Post auctions to //////////
//// facebook twitter google ///
//// with the item price, //////
//// description, image ////////
//// and title. ////////////////
////////////////////////////////
///////////////////////////////////
/////////Auction price in title ///
///////////////////////////////////
			
//// Here we are getting the auctions prices
//// and making them in to salt
if (!empty($auction_data)) 
{
	if (!empty($auction_data) && !empty($minimum_bid)) 
	{
		$buy_now_price = strip_tags($system->print_money($auction_data['buy_now']));
		$bid_price = strip_tags($system->print_money($minimum_bid));
				
	}
	//Checking to see if the auction is a Buy Now only auction
	if (!empty($auction_data) && isset($auction_data['buy_now']) && $auction_data['bn_only'] == 'y');
	{
	    $fb_price = " - " . $MSG['931'] . ": " . $buy_now_price;
	}
	//Checking the auction to see if it is a Standard Auction with a buy now botton
	if (isset($min_bid) && ($auction_data['bn_only'] == 'n') && (isset($auction_data['buy_now'])))
	{
	    $fb_price = " - " . $MSG['116'] . ": " . $bid_price . " or Buy Now " . $buy_now_price;
	}
	//Checking the auction to see if it is a Standard Auction with no buy now botton
	if (isset($min_bid) && ($auction_data['bn_only'] == 'n') && ($auction_data['buy_now'] == 0))
	{
	    $fb_price = " - " . $MSG['116'] . ": " . $bid_price;
	}
}			
///////////////////////////////
///////// Auction title    ////
///////////////////////////////
			
///// Checking to see if the auction title is set
///// if the auction title is not found it will
///// display the page title.
if (isset($auction_data['title'])) 
{
    $fb_title = $auction_data['title'];
}
else         
{
    $fb_title = $system->SETTINGS['sitename'];
}
			
/////////////////////////////////
///////// Auction Description ///
/////////////////////////////////
function shortText($string,$lenght) 
{
    $string = substr($string,0,$lenght)."...";
    $string_ende = strrchr($string, " ");
    $string = str_replace($string_ende," ...", $string);
    return $string;
}

if (isset($auction_data['description'])) {
	$fbdescW = strip_tags($auction_data['description']);
	$fbdescW = trim($fbdescW);
	$fb_desc = $fbdescW;
}
else         
{
	$fb_desc = stripslashes($system->SETTINGS['descriptiontag']);
}		
$fb_desc = shortText($fb_desc,150);
			
/////////////////////////////////
///////// Auction Image      ////
/////////////////////////////////
if (isset($auction_data['pict_url'])) 
{
	$fb_img = $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_show'] . '&fromfile=' . $uploaded_path . $auction_data['id'] . '/' . $auction_data['pict_url'];
}
else
{
	$fb_img = $system->SETTINGS['siteurl'] . $uploaded_path . 'logos/' . $system->SETTINGS['logo'];
}		
//////////////////////////////////
///////// Auction URL          ///
//////////////////////////////////
$fb_url = $system->SETTINGS['https'] == 'y' ? 'https://' : 'http://';
$fb_url .= $_SERVER['HTTP_HOST'];
$fb_url .= $_SERVER['REQUEST_URI'];
			
//////////////////////////////////
/////// social network array's ///
//////////////////////////////////
$template->assign_vars(array(
	'FB_TITLE' => isset($fb_title) ? $fb_title : '',
	'FB_DESC' => isset($fb_desc) ? $fb_desc : '',
	'FB_IMG' => isset($fb_img) ? $fb_img : '',
	'FB_URL' => isset($fb_url) ? $fb_url : '',
	'FB_PRICE' => isset($fb_price) ? $fb_price : ''
 ));
  
/////Part 4 ends here
?>