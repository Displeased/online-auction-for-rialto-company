<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

// check recaptcha is enabled
$spam_html = '';
if ($system->SETTINGS['spam_register'] == 2)
{
	include $main_path . 'inc/captcha/recaptchalib.php';
	if(!$_POST["g-recaptcha-response"])
	{
		$capcha_text = '<script src="https://www.google.com/recaptcha/api.js"></script> <div class="g-recaptcha" data-sitekey="' . $system->SETTINGS['recaptcha_public'] . '"></div>';
	}
	elseif(isset($_POST["g-recaptcha-response"]))
	{
		$resp = recaptcha_check_answer($system->SETTINGS['recaptcha_private'], $_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);
	}
}
elseif ($system->SETTINGS['spam_register'] == 1)
{
	include $main_path . 'inc/captcha/securimage.php';
	
	$resp = new Securimage();
	$spam_html = $resp->show_html();
}

if (isset($_GET['id']))
{
	$_SESSION['CURRENT_ITEM'] = $_GET['id'];
}

$id = intval($_SESSION['CURRENT_ITEM']);

$TPL_error_text = '';
$emailsent = 1;
// Get item data
$query = "SELECT title, category FROM " . $DBPrefix . "auctions WHERE id = :auc_id";
$params = array();
$params[] = array(':auc_id', $id, 'int');
$db->query($query, $params);
if ($db->numrows() > 0)
{
	$TPL_item_title = $db->result('title');
}

if (isset($_POST['action']) && $_POST['action'] == 'sendmail')
{
	// check errors
	if (empty($_POST['sender_name']) || empty($_POST['sender_email']) || empty($_POST['friend_name']) || empty($_POST['friend_email']))
	{
		$TPL_error_text = $ERR_031;
	}
	elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['sender_email']) || !preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['friend_email']))
	{
		$TPL_error_text = $ERR_008;
	}
	elseif ($system->SETTINGS['spam_sendtofriend'] == 2 && !$resp)
	{
		$TPL_error_text = $MSG['752'];
	}
	elseif ($system->SETTINGS['spam_sendtofriend'] == 1 && !$resp->check($_POST['captcha_code']))
	{
		$TPL_error_text = $MSG['752'];
	}
	
	if (!empty($TPL_error_text))
	{
		$emailsent = 1;
	}
	else
	{
		$emailsent = 0;
		$send_email->send_friend_email($_POST['sender_name'], $_POST['sender_email'], $_POST['sender_comment'], $_POST['friend_name'], $TPL_item_title, $id, $_POST['friend_email']);
	}
}

$template->assign_vars(array(
		'ERROR' => $TPL_error_text,
		'SEO' => generate_seo_link($TPL_item_title) . '-' . intval($id),
		'CAPTCHATYPE' => $system->SETTINGS['spam_register'],
		'CAPCHA' => ($system->SETTINGS['spam_register'] == 2) ? $capcha_text : $spam_html,
		'TITLE' => $TPL_item_title,
		'FRIEND_NAME' => (isset($_POST['friend_name'])) ? $system->cleanvars($_POST['friend_name']) : '',
		'FRIEND_EMAIL' => (isset($_POST['friend_email'])) ?  $system->cleanvars($_POST['friend_email']) : '',
		'YOUR_NAME' => ($user->logged_in) ? $system->cleanvars($user->user_data['name']) : '',
		'YOUR_EMAIL' => ($user->logged_in) ? $system->cleanvars($user->user_data['email']) : '',
		'B_FB_LINK' => 'IndexFBLogin',
		'COMMENT' => (isset($_POST['sender_comment'])) ? $system->cleanvars($_POST['sender_comment']) : '',
		'EMAILSENT' => $emailsent
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'friend.tpl'
		));
$template->display('body');
include 'footer.php';