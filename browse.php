<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $include_path . 'dates.inc.php';
include $main_path . 'language/' . $language . '/categories.inc.php';
$catscontrol = new MPTTcategories();

if(!empty($_SESSION['SELL_title']))
{
	include $include_path . 'functions_sell.php';
	unsetsessions();
}

// Get parameters from the URL
$get_name = $_GET['id'] ;  // get the full product name
$get_array = explode('-',$get_name) ;  // split the product name into segments and put into an array (product id must be separated by '-' )
$get_id = end($get_array) ; // extract the last array i.e product id form full product name
// Get parameters from the URL
$id = (isset($get_id)) ? intval($get_id) : 0; //OLD -> $id = (isset($_GET['id'])) ? intval($_GET['id']) : 0;
$_SESSION['browse_id'] = $id;
$all_items = true;

if ($id != 0)
{
	$query = "SELECT right_id, left_id FROM " . $DBPrefix . "categories WHERE cat_id = " . $id;
	$params = array();
	$params[] = array(':id', $id, 'int');
}
else
{
	$query = "SELECT right_id, left_id, cat_id FROM " . $DBPrefix . "categories WHERE left_id = :one";
	$params = array();
	$params[] = array(':one', 1, 'int');
}
$db->query($query, $params);
$parent_node = $db->result();
$id = (isset($parent_node['cat_id'])) ? $parent_node['cat_id'] : $id;
$catalist = '';
if ($parent_node['left_id'] != 1)
{
	$children = $catscontrol->get_children_list($parent_node['left_id'], $parent_node['right_id']);
	$childarray = array($id);
	foreach ($children as $k => $v)
	{
		$childarray[] = $v['cat_id'];
	}
	$catalist = '(';
	$catalist .= implode(',', $childarray);
	$catalist .= ')';
	$all_items = false;
}

$NOW = $system->ctime;

/*
specified category number
look into table - and if we don't have such category - redirect to full list
*/
$query = "SELECT * FROM " . $DBPrefix . "categories WHERE cat_id = " . $id;
$params = array();
$params[] = array(':id', $id, 'int');
$db->query($query, $params);
$category = $db->result();
if ($db->numrows() == 0)
{
	// redirect to global categories list
	header ('location: cat/all-categories-0');
	exit;
}
else
{
	// Retrieve the translated category name
	$par_id = $category['parent_id'];
	$TPL_categories_string = '';
	$crumbs = $catscontrol->get_bread_crumbs($category['left_id'], $category['right_id']);
	for ($i = 0; $i < count($crumbs); $i++)
	{
		if ($crumbs[$i]['cat_id'] > 0)
		{
			if ($i > 0)
			{
				$TPL_categories_string .= ' &gt; ';
			}
			$TPL_categories_string .= '<li><a href="' . $system->SETTINGS['siteurl'] . 'cat/'. generate_seo_link($category_names[$crumbs[$i]['cat_id']]) .'-' . $crumbs[$i]['cat_id'] . '">' . $category_names[$crumbs[$i]['cat_id']] . '</a></li>';
			$current_cat_name = $category_names[$crumbs[$i]['cat_id']];
		}
	}
	if ($system->SETTINGS['catsorting'] == 'alpha')
	{
		$catsorting = ' ORDER BY cat_name ASC';
	}
	else
	{
		$catsorting = ' ORDER BY sub_counter DESC';
	}
	
	$query = "SELECT cat_id FROM " . $DBPrefix . "categories WHERE parent_id = :one";
	$params = array();
	$params[] = array(':one', -1, 'int');
	$db->query($query, $params);
	$parent_ids = $db->result();
	
	$query = "SELECT * FROM " . $DBPrefix . "categories WHERE parent_id = :parent_ids" . $catsorting . " LIMIT :catstoshow";
	$params = array();
	$params[] = array(':parent_ids', $parent_ids, 'int');
	$params[] = array(':catstoshow', $system->SETTINGS['catstoshow'], 'int');
	$db->query($query, $params);
	
	while ($row = $db->result())
	{
		$template->assign_block_vars('cat_list_drop', array(
				'CATAUCNUM' => ($row['sub_counter'] != 0) ? '(' . $row['sub_counter'] . ')' : '',
				'ID' => $row['cat_id'],
				'IMAGE' => (!empty($row['cat_image'])) ? '<img src="' . $row['cat_image'] . '" border=0>' : '',
				'COLOUR' => (empty($row['cat_colour'])) ? '#FFFFFF' : $row['cat_colour'],
				'NAME' => $category_names[$row['cat_id']]
				));
	}

	// get list of subcategories of this category
	$subcat_count = 0;
	$query = "SELECT * FROM " . $DBPrefix . "categories WHERE parent_id = :id ORDER BY cat_name";
	$params = array();
	$params[] = array(':id', $id, 'int');
	$db->query($query, $params);
	$need_to_continue = 1;
	$cycle = 1;

	$TPL_main_value = '';
	while ($row = $db->result())
	{
		++$subcat_count;
		if ($cycle == 1)
		{
			$TPL_main_value .= '<div class="row">' . "\n";
		}
		$sub_counter = $row['sub_counter'];
		$cat_counter = $row['counter'];
		if ($sub_counter != 0 && $system->SETTINGS['cat_counters'] == 'y')
		{
			$count_string = ' (' . $sub_counter . ')';
			
		}
		else
		{
			if ($cat_counter != 0 && $system->SETTINGS['cat_counters'] == 'y')
			{
				$count_string = ' (' . $cat_counter . ')';
			}
			else
			{
				$count_string = '';
			}
		}
		if ($row['cat_colour'] != '')
		{
			$BG = 'success';
		}
		else
		{
			$BG = '';
		}
		// Retrieve the translated category name
		$row['cat_name'] = $category_names[$row['cat_id']];
		$catimage = (!empty($row['cat_image'])) ? '<img src="' . $row['cat_image'] . '" border=0>' : '';
		$TPL_main_value .= "\t" . '<div class="span3 browse-mini ' . $BG . '">' . $catimage . '<a href="' . $system->SETTINGS['siteurl'] . 'cat/'. generate_seo_link($row['cat_name']) .'-'. $row['cat_id'] .'">' . $row['cat_name'] . $count_string . '</a></div>' . "\n";

		++$cycle;
		if ($cycle == 4)
		{
			$cycle = 1;
			$TPL_main_value .= '</div>' . "\n";
		}
	}

	if ($cycle >= 2 && $cycle <= 3)
	{
		while ($cycle < 4)
		{
			$TPL_main_value .= '	<td width="33%">&nbsp;</td>' . "\n";
			++$cycle;
		}
		$TPL_main_value .= '</tr>' . "\n";
	}

	$insql = "(category IN " . $catalist;
	if ($system->SETTINGS['extra_cat'] == 'y')
	{
		$insql .= " OR secondcat IN " . $catalist;
	}
	$insql = (!$all_items) ? $insql . ") AND" : '';

	// get total number of records
	$query = "SELECT id FROM " . $DBPrefix . "auctions
	WHERE " . $insql . " starts <= :start AND closed = 0 AND suspended = 0";
	$params = array();
	if (!empty($_POST['catkeyword']))
	{
		$query .= " AND title LIKE :title";
		$params[] = array(':title', '%' . $system->cleanvars($_POST['catkeyword']) . '%', 'str');
	}
	$params[] = array(':start', $NOW, 'int');
	$db->query($query, $params);	
	$TOTALAUCTIONS = $db->numrows('id');

	// Handle pagination
	if (!isset($_GET['PAGE']) || $_GET['PAGE'] <= 1 || $_GET['PAGE'] == '')
	{
		$OFFSET = 0;
		$PAGE = 1;
	}
	else
	{
		$PAGE = intval($_GET['PAGE']);
		$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
	}
	$PAGES = ($TOTALAUCTIONS == 0) ? 1 : ceil($TOTALAUCTIONS / $system->SETTINGS['perpage']);
	
	// get normal auctions
	$query = "SELECT * FROM " . $DBPrefix . "auctions
			WHERE " . $insql . " starts <= :times
			AND closed = :closed
			AND suspended = :suspended";
	$params = array();
	$params[] = array(':times', $NOW, 'int');
	$params[] = array(':closed', 0, 'int');
	$params[] = array(':suspended', 0, 'int');
	if (!empty($_POST['catkeyword']))
	{
		$query .= " AND title LIKE :title";
		$params[] = array(':title', '%' . $system->cleanvars($_POST['catkeyword']) . '%', 'str');
	}
	$query .= " ORDER BY ends ASC LIMIT :offset, :perpage";
	$params[] = array(':offset', $OFFSET, 'int');
	$params[] = array(':perpage', $system->SETTINGS['perpage'], 'int');

	// get featured items
	$query_feat = "SELECT * FROM " . $DBPrefix . "auctions
			WHERE " . $insql . " starts <= :times
			AND closed = :closed
			AND suspended = :suspended
			AND featured = :yes";
	$params_feat = array();
	$params_feat[] = array(':times', $NOW, 'int');
	$params_feat[] = array(':closed', 0, 'int');
	$params_feat[] = array(':suspended', 0, 'int');
	$params_feat[] = array(':yes', 'y', 'str');
	if (!empty($_POST['catkeyword']))
	{
		$query_feat .= " AND title LIKE :title";
		$params_feat[] = array(':title', '%' . $system->cleanvars($_POST['catkeyword']) . '%', 'str');
	}
	$query_feat .= " ORDER BY ends ASC LIMIT :offset, :perpage";
	$params_feat[] = array(':offset', $OFFSET, 'int');
	$params_feat[] = array(':perpage', $system->SETTINGS['perpage'], 'int');

	include $include_path . 'browseitems.inc.php';
	browseItems($query, $params, $query_feat, $params_feat, $TOTALAUCTIONS, 'browse.php', 'id=' . $id);
	$page_title = $current_cat_name;
	$template->assign_vars(array(
			'ID' => $id,
			'B_FB_LINK' => 'IndexFBLogin',
			'TOP_HTML' => $TPL_main_value,
			'CAT_STRING' => $TPL_categories_string,
			'CUR_CAT' => $current_cat_name,
			'ORDERCOL' => isset($_SESSION['oa_ord']) ? $_SESSION['oa_ord'] : '',
			'B_FB_LINK' => !$user->is_logged_in() ? 'IndexFBLogin' : '',
         	'ORDERNEXT' => isset($_SESSION['oa_nexttype']) ? $_SESSION['oa_nexttype'] : '',
         	'ORDERTYPEIMG' => isset($_SESSION['oa_type_img']) ? $_SESSION['oa_type_img'] : '',
			'NUM_AUCTIONS' => $TOTALAUCTIONS
			));
}

include 'header.php';
$template->set_filenames(array(
		'body' => 'browsecats.tpl'
		));
$template->display('body');
include 'footer.php';
