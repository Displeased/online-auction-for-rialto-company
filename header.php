<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
if (!defined('InuAuctions')) exit();

include $include_path . 'maintainance.php';
include $include_path . 'functions_banners.php';
if (basename($_SERVER['PHP_SELF']) != 'error.php')
include $include_path . 'stats.inc.php';
include $main_path . 'language/' . $language . '/categories.inc.php';
include $main_path . 'inc/facebook/facebook_cnt.php';
include $include_path . 'functions_item_counters.php';

$jsfiles = 'js/jquery.js;js/jquery.lightbox.js;';
$jsfiles .= (basename($_SERVER['PHP_SELF']) == 'sell.php') ? ';js/calendar.php' : '';

//checking to see if the u-Auctions script is running SSL

$sslurl = $system->SETTINGS['siteurl'];
if ($system->SETTINGS['https'] == 'y')
{
	$sslurl = str_replace('http://', 'https://', $system->SETTINGS['siteurl']);
	$sslurl = (!empty($system->SETTINGS['https_url'])) ? $system->SETTINGS['https_url'] : $sslurl;
}
if ($system->SETTINGS['https'] == 'y' && $_SERVER['HTTPS'] != 'on')
{
	header('Location: ' . $sslurl . 'home');
	exit;
}

// for images/ccs/javascript etc on secure pages
$incurl = (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') ? $system->SETTINGS['siteurl'] : $sslurl; 

// prepare categories list for templates/template 
// Prepare categories sorting
if ($system->SETTINGS['catsorting'] == 'alpha')
{
	$catsorting = 'ORDER BY cat_name ASC';
}
else
{
	$catsorting = 'ORDER BY sub_counter DESC';
}

$query = "SELECT cat_id FROM " . $DBPrefix . "categories WHERE parent_id = :one";
$params = array();
$params[] = array(':one', -1, 'int');
$db->query($query, $params);
$cats_ids = $db->result('cat_id');

$query = "SELECT * FROM " . $DBPrefix . "categories WHERE parent_id = :cat_ids " . $catsorting ." LIMIT :catstoshows";
$params = array();
$params[] = array(':cat_ids', $cats_ids, 'int');
$params[] = array(':catstoshows', $system->SETTINGS['catstoshow'], 'int');
$db->query($query, $params);
while ($row = $db->result())
{
	$sub_counter = $row['sub_counter'];
	$cat_counter = $row['counter'];
	if ($sub_counter != 0 && $system->SETTINGS['cat_counters'] == 'y')
	{
		$count_string = ' (' . $sub_counter . ')';
			
	}
	else
	{
		$count_string = '';
	}
	$template->assign_block_vars('cat_list_drop_2', array(
			'CATAUCNUM' => ($row['sub_counter'] != 0) ? '(' . $row['sub_counter'] . ')' : '',
			'ID' => $row['cat_id'],
			'IMAGE' => (!empty($row['cat_image'])) ? '<img src="' . $row['cat_image'] . '" border=0>' : '',
			'SEO_NAME' => generate_seo_link($category_names[$row['cat_id']]),
			'COLOUR' => (empty($row['cat_colour'])) ? '#FFFFFF' : $row['cat_colour'],
			'NAME' => $category_names[$row['cat_id']] . ' ' . $count_string
			));
}

// Build list of help topics
$query = "SELECT id, category FROM " . $DBPrefix . "faqscat_translated WHERE lang = :languages ORDER BY category ASC";
$params = array();	
$params[] = array(':languages', $language, 'str');  
$db->query($query, $params);
$i = 0;
while ($faqscat = $db->result())
{
	$template->assign_block_vars('helpbox', array(
			'ID' => $faqscat['id'],
			'TITLE' => $faqscat['category']
			));
	$i++;
}
$helpbox = ($i > 0) ? true : false;

//metatags
$metadesc = ($system->SETTINGS['descriptiontag'] !='' && $setmetatags == '') ? stripslashes(strip_tags($system->SETTINGS['descriptiontag'])) : $system->SETTINGS['sitename'];
$metadesc = ($setmetatags !='') ? stripslashes(strip_tags($setmetatags)) : $metadesc;
$wordstags = ($system->SETTINGS['keywordstag'] !='') ? stripslashes($system->SETTINGS['keywordstag']) : $system->SETTINGS['sitename'];
$wordstags = (isset($page_title)) ? $page_title : $wordstags;

$template->assign_vars(array(
		'FLAGS' => ShowingFlags(),
		'B_MULT_LANGS' => (count($LANGUAGES) > 1),
		'DOCDIR' => $DOCDIR, // Set document direction (set in includes/messages.XX.inc.php) ltr/rtl
		'THEME' => $system->SETTINGS['theme'],
		'PAGE_TITLE' => (isset($page_title)) ? $system->SETTINGS['sitename'] . ' - ' . $page_title : $system->SETTINGS['sitename'],
		'CHARSET' => $CHARSET,
		'DESCRIPTION' => $metadesc,
		'KEYWORDS' => $wordstags,
		'JSFILES' => $jsfiles,
		'LOADCKEDITOR' => (basename($_SERVER['PHP_SELF']) == 'sell.php'),
		'ACTUALDATE' => ActualDate(),
		'LOGO' => ($system->SETTINGS['logo']) ? '<a href="' . $system->SETTINGS['siteurl'] . 'home"><img src="' . $incurl . $uploaded_path . 'logos/' . $system->SETTINGS['logo'] . '" border="0" alt="' . $system->SETTINGS['sitename'] . '"></a>' : '&nbsp;',
		'B_BANNERMANAGER' => ($system->SETTINGS['banners'] == 1),
		'HEADERCOUNTER' => load_counters(),
		'SITEURL' => $system->SETTINGS['siteurl'],
		'SSLURL' => $sslurl,
		'SITENAME' => $system->SETTINGS['sitename'],
		'ASSLURL' => ($system->SETTINGS['https'] == 'y' && $system->SETTINGS['usersauth'] == 'y') ? $sslurl : $system->SETTINGS['siteurl'],
		'INCURL' => $incurl,
		'Q' => (isset($q)) ? $q : '',
		'SELECTION_BOX' => file_get_contents($main_path . 'language/' . $language . '/categories_select_box.inc.php'),
		'YOURUSERNAME' => ($user->logged_in) ? $user->user_data['nick'] : '',
		'LANGUAGE' => $language,
		'FBOOK_APPID' => $system->SETTINGS['facebook_app_id'],
		'FBOOK_APPSECRET' => $system->SETTINGS['facebook_app_secret'],
		'B_FBOOK' => ($system->SETTINGS['facebook_login'] == 'y'),
		'B_FEES' => ($system->SETTINGS['fees'] == 'y'),
		'B_HELPBOX' => ($helpbox && $system->SETTINGS['helpbox'] == 1),
		//Banner and adsense system
		'BANNER' => ($system->SETTINGS['banners'] == 1) ? view() : '',
		'HEADER_ADSENSE' => (!$system->ADSENSE['header_banner_1'] ? '' : $system->ADSENSE['header_banner_1']),
		'INDEX_ADSENSE_1' => (!$system->ADSENSE['index_banner_1'] ? '' : '<div class="hidden-phone" align="center">' . $system->ADSENSE['index_banner_1'] . '</div>'),
		'INDEX_ADSENSE_2' => (!$system->ADSENSE['index_banner_2'] ? '' : '<div class="row"><div class="hidden-phone span6">' . $system->ADSENSE['index_banner_2'] . '</div></div>'),
		'INDEX_ADSENSE_3' => (!$system->ADSENSE['index_banner_3'] ? '' : '<div class="hidden-phone" align="center">' . $system->ADSENSE['index_banner_3'] . '</div>'),
		'BROWSE_ADSENSE_1' => (!$system->ADSENSE['browse_banner_1'] ? '' : '<div class="hidden-phone" align="center">' . $system->ADSENSE['browse_banner_1'] . '</div>'),
		'BROWSE_SEO' => generate_seo_link($MSG['277_1']),
		'B_CAN_SELL' => ($user->can_sell || !$user->logged_in),
		'B_DIGITAL_ITEM_ON' => ($system->SETTINGS['di_auctions'] == 'y') ? true : false,
		'B_LOGGED_IN' => $user->logged_in,
		'B_BOARDS' => ($system->SETTINGS['boards'] == 'y') ? true : false,
		'B_FBOOK_LOGIN' => ($system->SETTINGS['facebook_login'] == 'y') ? true : false,
		'B_CAT_COUNTER' => ($system->SETTINGS['cat_counters'] == 'y') ? true : false,
		'B_COOKIE_DIRECTIVE' => ($system->SETTINGS['cookies_directive'] == 'y') ? true : false,
		'B_FEES' => ($system->SETTINGS['fees'] == 'y'),
		'MAXIMAGESIZE' => $system->SETTINGS['thumb_show'],
		'MAXIMAGESIZELIST' => $system->SETTINGS['thumb_list'],
		'ANALYTICS' => $system->SETTINGS['google_analytics'],
		));

$template->set_filenames(array(
		'header' => 'global_header.tpl'
		));
$template->display('header');
