<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'auction_watch.php';
	header('location: user_login.php');
	exit;
}

// insert a new watch item
if (isset($_GET['insert']) && $_GET['insert'] == 'true' && !empty($_REQUEST['add']))
{
	$requestadd = $system->cleanvars($_REQUEST['add']);
	// Check if this keyword is not already added
	$auctions = trim($user->user_data['auc_watch']);
	unset($match); // just incase
	if (!empty($auctions))
	{
		$checkarray = explode(' ', $requestadd);
		$requestadd = '';
		foreach ($checkarray as $check)
		{
			if (strpos($auctions, $check) === false)
			{
				$requestadd .= $check . ' ';
			}
		}
	}

	if (!isset($match) || empty($match))
	{
		$auction_watch = trim($auctions . ' ' . $requestadd);
		$query = "UPDATE " . $DBPrefix . "users SET auc_watch = :watch WHERE id = :user_id";		
		$params = array();
		$params[] = array(':watch', $system->cleanvars($auction_watch), 'str');
		$params[] = array(':user_id', $user->user_data['id'], 'int');
		$db->query($query, $params);

		$user->user_data['auc_watch'] = $auction_watch;
	}
}

// Delete auction from auction watch
if (isset($_GET['delete']))
{
	$auctions = trim($user->user_data['auc_watch']);
	$auc_id = explode(' ', $auctions);
	$auction_watch = '';
	for ($j = 0; $j < count($auc_id); $j++)
	{
		$match = strstr($auc_id[$j], $_GET['delete']);
		if ($match)
		{
			$auction_watch = $auction_watch;
		}
		else
		{
			$auction_watch = $auc_id[$j] . ' ' . $auction_watch;
		}
	}
	$auction_watch = trim($auction_watch);
	$query = "UPDATE " . $DBPrefix . "users SET auc_watch = :watch WHERE id = :user_id";
	$params = array();
	$params[] = array(':watch', $system->cleanvars($auction_watch), 'str');
	$params[] = array(':user_id', $user->user_data['id'], 'int');
	$db->query($query, $params);
	$user->user_data['auc_watch'] = $auction_watch;
}

$auctions = trim($user->user_data['auc_watch']);
if ($auctions != '')
{
	$auction = explode(' ', $auctions);	    
	for ($j = 0; $j < count($auction); $j++)
	{
		$template->assign_block_vars('items', array(
			'ITEM' => $auction[$j],
			'ITEMENCODE' => urlencode($auction[$j])
		));
	}
}

include 'header.php';
$TMP_usmenutitle = $MSG['471'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'auction_watch.tpl'
		));
$template->display('body');
include 'footer.php';
