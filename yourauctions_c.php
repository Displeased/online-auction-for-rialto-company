<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
include 'common.php';

// If user is not logged in redirect to login page
if (!$user->logged_in)
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'yourauctions_c.php';
	header('location: user_login.php');
	exit;
}

// DELETE OPEN AUCTIONS
$NOW = $system->ctime;
$NOWB = gmdate('Ymd');
$catscontrol = new MPTTcategories();

$query = "SELECT value FROM " . $DBPrefix . "fees WHERE type = :type";
$params = array();
$params[] = array(':type', 'relist_fee', 'str');
$db->query($query, $params);
$relist_fee = $db->result('value');

// Update
if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Delete auction
	if (is_array($_POST['delete']))
	{
		foreach ($_POST['delete'] as $k => $v)
		{
			$v = intval($v);
			// Pictures Gallery
			if ($dir = @opendir($upload_path . $v))
			{
				while ($file = readdir($dir))
				{
					if ($file != '.' && $file != '..')
					{
						unlink($upload_path . $v . '/' . $file);
					}
				}
				closedir($dir);
				@rmdir($upload_path . $v);
			}

			$query = "UPDATE " . $DBPrefix . "counters SET closedauctions = closedauctions - :closed";
			$params = array();
			$params[] = array(':closed', 1, 'int');
			$db->query($query, $params);

			$query = "DELETE FROM " . $DBPrefix . "auccounter WHERE auction_id = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $v, 'int');
			$db->query($query, $params);

			$query = "DELETE FROM " . $DBPrefix . "auctions WHERE id = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $v, 'int');
			$db->query($query, $params);

			$query = "DELETE FROM " . $DBPrefix . "bids WHERE auction = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $v, 'int');
			$db->query($query, $params);


			$query = "DELETE FROM " . $DBPrefix . "proxybid WHERE itemid = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $v, 'int');
			$db->query($query, $params);
		}
	}
	if (is_array($_POST['sell']))
	{
		foreach ($_POST['sell'] as $v)
		{
			$query = "UPDATE " . $DBPrefix . "auctions SET sold = :sold WHERE id = :auc_id";
			$params = array();
			$params[] = array(':sold', 's', 'str');
			$params[] = array(':auc_id', $v, 'int');
			$db->query($query, $params);
		}
		include 'cron.php';
	}
	// Re-list auctions
	if (is_array($_POST['relist']))
	{
		foreach ($_POST['relist'] as $k)
		{
			$k = intval($k);
			$query = "SELECT duration, category FROM " . $DBPrefix . "auctions WHERE id = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $k, 'int');
			$db->query($query, $params);
			$AUCTION = $db->result();

			// auction ends
			$WILLEND = $system->ctime + ($AUCTION['duration'] * 24 * 60 * 60);
			$suspend = 0;

			if ($system->SETTINGS['fees'] == 'y')
			{
				if ($system->SETTINGS['fee_type'] == 1)
				{
					// charge relist fee
					$query = "UPDATE " . $DBPrefix . "users SET balance = balance - :relist WHERE id = :user_id";
					$params = array();
					$params[] = array(':relist', $relist_fee, 'float');
					$params[] = array(':user_id', $user->user_data['id'], 'int');
					$db->query($query, $params);
				}
				else
				{
					$suspend = 8;
				}
			}

			$query = "UPDATE " . $DBPrefix . "auctions
				  SET starts = :now,
				  ends = :ends,
				  closed = :close,
				  num_bids = :num_bid,
				  relisted = relisted + :relist,
				  current_bid = :current_bids,
				  sold = :no_sold,
				  suspended = :suspend
				  WHERE id = :auc_id";
			$params = array();
			$params[] = array(':now', $NOW, 'int');
			$params[] = array(':ends', $WILLEND, 'int');
			$params[] = array(':close', 0, 'int');
			$params[] = array(':num_bid', 0, 'int');
			$params[] = array(':relist', 1, 'int');
			$params[] = array(':current_bids', 0, 'int');
			$params[] = array(':no_sold', 'n', 'str');
			$params[] = array(':suspend', $suspend, 'int');
			$params[] = array(':auc_id', $k, 'int');
			$db->query($query, $params);

			// Insert into relisted table
			$query = "INSERT INTO " . $DBPrefix . "closedrelisted VALUES (:auc_id, :relist, :new_auc_id)";
			$params = array();
			$params[] = array(':auc_id', $k, 'int');
			$params[] = array(':relist', $NOWB, 'int');
			$params[] = array(':new_auc_id', $k, 'int');
			$db->query($query, $params);

			// delete bids
			$query = "DELETE FROM " . $DBPrefix . "bids WHERE auction = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $k, 'int');
			$db->query($query, $params);

			// Proxy Bids
			$query = "DELETE FROM " . $DBPrefix . "proxybid WHERE itemid = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $k, 'int');
			$db->query($query, $params);

			// Update COUNTERS table
			$query = "UPDATE " . $DBPrefix . "counters SET auctions = auctions + :count_auc";
			$params = array();
			$params[] = array(':count_auc', 1, 'int');
			$db->query($query, $params);

			$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :cat_ids";
			$params = array();
			$params[] = array(':cat_ids', $AUCTION['category'], 'int');
			$db->query($query, $params);

			$parent_node = $db->fetchall();
			$crumbs = $catscontrol->get_bread_crumbs($parent_node['left_id'], $parent_node['right_id']);
			// update recursive categories
			for ($i = 0; $i < count($crumbs); $i++)
			{
				$query = "UPDATE " . $DBPrefix . "categories SET sub_counter = sub_counter + :sub WHERE cat_id = :cat_ids";
				$params = array();
				$params[] = array(':sub', 1, 'int');
				$params[] = array(':cat_ids', $crumbs[$i]['cat_id'], 'int');
				$db->query($query, $params);
			}
			if ($system->SETTINGS['fee_type'] == 2 && isset($relist_fee) && $relist_fee > 0)
			{
				header('location: pay.php?a=5');
				exit;
			}
		}
	}
}

// Retrieve closed auction data from the database
$query = "SELECT COUNT(id) AS COUNT FROM " . $DBPrefix . "auctions
	WHERE user = :user_id 
	AND closed = :close AND suspended = :suspend
	AND (num_bids = :bids OR (num_bids > :count_bids AND current_bid < reserve_price AND sold = :item_sold))";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$params[] = array(':close', 1, 'int');
$params[] = array(':suspend', 0, 'int');
$params[] = array(':bids', 0, 'int');
$params[] = array(':count_bids', 0, 'int');
$params[] = array(':item_sold', 'n', 'str');
$db->query($query, $params);

$TOTALAUCTIONS = $db->result('COUNT');

if (!isset($_GET['PAGE']) || $_GET['PAGE'] == 1)
{
	$OFFSET = 0;
	$PAGE = 1;
}
else
{
	$PAGE = intval($_GET['PAGE']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}

$PAGES = ($TOTALAUCTIONS == 0) ? 1 : ceil($TOTALAUCTIONS / $system->SETTINGS['perpage']);
// Handle columns sorting variables
if (!isset($_SESSION['ca_ord']) && empty($_GET['ca_ord']))
{
	$_SESSION['ca_ord'] = 'title';
	$_SESSION['ca_type'] = 'asc';
}
elseif (!empty($_GET['ca_ord']))
{
	$_SESSION['ca_ord'] = $_GET['ca_ord'];
	$_SESSION['ca_type'] = $_GET['ca_type'];
}
elseif (isset($_SESSION['ca_ord']) && empty($_GET['ca_ord']))
{
	$_SESSION['ca_nexttype'] = $_SESSION['ca_type'];
}

if (!isset($_SESSION['ca_nexttype']) || $_SESSION['ca_nexttype'] == 'desc')
{
	$_SESSION['ca_nexttype'] = 'asc';
}
else
{
	$_SESSION['ca_nexttype'] = 'desc';
}

if (!isset($_SESSION['ca_type']) || $_SESSION['ca_type'] == 'desc')
{
	$_SESSION['ca_type_img'] = '<img src="images/arrow_up.gif" align="center" hspace="2" border="0">';
}
else
{
	$_SESSION['ca_type_img'] = '<img src="images/arrow_down.gif" align="center" hspace="2" border="0">';
}

$query = "SELECT * FROM " . $DBPrefix . "auctions WHERE user = :user_id
	AND closed = :close AND suspended = :suspend
	AND (num_bids = :bids OR (num_bids > :count_bids AND reserve_price > :reserve AND current_bid < reserve_price AND sold = :item_sold))
	ORDER BY " . $_SESSION['ca_ord'] . " " . $_SESSION['ca_type'] . " LIMIT " . $OFFSET . ", " . $system->SETTINGS['perpage'];
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$params[] = array(':close', 1, 'int');
$params[] = array(':suspend', 0, 'int');
$params[] = array(':bids', 0, 'int');
$params[] = array(':count_bids', 0, 'int');
$params[] = array(':reserve', 0, 'int');
$params[] = array(':item_sold', 'n', 'str');
$db->query($query, $params);
$i = 0;
while ($item = $db->result())
{
	$canrelist = false;
	if (($item['current_bid'] > $item['reserve_price']))
	{
		$cansell = false;
	}
	else
	{
		if ($item['reserve_price'] > 0 || $item['num_bids'] == 0)
		{
			$canrelist = true;
		}
		if ($item['reserve_price'] > 0 && $item['num_bids'] > 0)
		{
			$cansell = true;
		}
		else
		{
			$cansell = false;
		}
	}

	$template->assign_block_vars('items', array(
			'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
			'ID' => $item['id'],
			'TITLE' => $item['title'],
			'STARTS' => FormatDate($item['starts']),
			'ENDS' => FormatDate($item['ends']),
			'BID' => ($item['current_bid'] == 0) ? '-' : $system->print_money($item['current_bid']),
			'BIDS' => $item['num_bids'],
			'SEO_TITLE' => generate_seo_link($item['title']),

			'B_CANRELIST' => $canrelist,
			'B_CANSSELL' => $cansell,
			'B_HASNOBIDS' => ($item['current_bid'] == 0)
			));

	$i++;
}

// get pagenation
$PREV = intval($PAGE - 1);
$NEXT = intval($PAGE + 1);
if ($PAGES > 1)
{
	$LOW = $PAGE - 5;
	if ($LOW <= 0) $LOW = 1;
	$COUNTER = $LOW;
	while ($COUNTER <= $PAGES && $COUNTER < ($PAGE + 6))
	{
		$template->assign_block_vars('pages', array(
				'PAGE' => ($PAGE == $COUNTER) ? '<b>' . $COUNTER . '</b>' : '<a href="' . $system->SETTINGS['siteurl'] . 'yourauctions_c.php?PAGE=' . $COUNTER . '"><u>' . $COUNTER . '</u></a>'
				));
		$COUNTER++;
	}
}

$template->assign_vars(array(
		'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
		'ORDERCOL' => $_SESSION['ca_ord'],
		'ORDERNEXT' => $_SESSION['ca_nexttype'],
		'ORDERTYPEIMG' => $_SESSION['ca_type_img'],
		'RELIST_FEE' => $system->print_money($relist_fee),
		'RELIST_FEE_NO' => $system->print_money_nosymbol($relist_fee),

		'PREV' => ($PAGES > 1 && $PAGE > 1) ? '<a href="' . $system->SETTINGS['siteurl'] . 'yourauctions_c.php?PAGE=' . $PREV . '"><u>' . $MSG['5119'] . '</u></a>&nbsp;&nbsp;' : '',
		'NEXT' => ($PAGE < $PAGES) ? '<a href="' . $system->SETTINGS['siteurl'] . 'yourauctions_c.php?PAGE=' . $NEXT . '"><u>' . $MSG['5120'] . '</u></a>' : '',
		'PAGE' => $PAGE,
		'PAGES' => $PAGES,

		'B_AREITEMS' => ($i > 0),
		'B_RELIST_FEE' => ($relist_fee > 0 && $system->SETTINGS['fees'] == 'y'),
		'B_AUTORELIST' => ($system->SETTINGS['autorelist'] == 'y')
		));

include 'header.php';
$TMP_usmenutitle = $MSG['354'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'yourauctions_c.tpl'
		));
$template->display('body');
include 'footer.php';