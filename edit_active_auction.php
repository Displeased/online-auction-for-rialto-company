<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

$NOW = $system->ctime;

// Is the seller logged in?
if (!$user->logged_in)
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'select_category.php';
	header('location: user_login.php');
	exit;
}

$query = "SELECT id FROM " . $DBPrefix . "bids WHERE auction = :id";
$params = array();
$params[] = array(':id', intval($_GET['id']), 'int');
$db->query($query, $params);
if ($db->numrows() > 0) 
{
	header('location: home');
	exit;
}

if (!isset($_POST['action'])) // already closed auctions
{
	// Get Closed auctions data
	unset($_SESSION['UPLOADED_PICTURES']);
	unset($_SESSION['UPLOADED_PICTURES_SIZE']);
	$query = "SELECT * FROM " . $DBPrefix . "auctions WHERE id = :id AND user = :user_id";
	$params = array();
	$params[] = array(':id', intval($_GET['id']), 'int');
	$params[] = array(':user_id', $user->user_data['id'], 'int');
	$db->query($query, $params);
	
	$RELISTEDAUCTION = $db->result();

	$difference = $RELISTEDAUCTION['ends'] - $NOW;

	if ($user->user_data['id'] == $RELISTEDAUCTION['user'] && $difference > 0)
	{
		$_SESSION['SELL_auction_id']	= $RELISTEDAUCTION['id'];
		$_SESSION['SELL_starts']		= $RELISTEDAUCTION['starts'] + $system->tdiff;
		$_SESSION['SELL_ends']			= $RELISTEDAUCTION['ends'];
		$_SESSION['SELL_title']			= $RELISTEDAUCTION['title'];
		$_SESSION['SELL_subtitle']		= $RELISTEDAUCTION['subtitle'];
		$_SESSION['SELL_description']	= stripslashes($RELISTEDAUCTION['description']);
		$_SESSION['SELL_atype']			= $RELISTEDAUCTION['auction_type'];
		$_SESSION['SELL_buy_now_only']	= $RELISTEDAUCTION['bn_only'];
		$_SESSION['SELL_suspended']		= $RELISTEDAUCTION['suspended'];
		$_SESSION['SELL_iquantity']		= $RELISTEDAUCTION['quantity'];
		$_SESSION['SELL_is_bold']			= $RELISTEDAUCTION['bold'];
		$_SESSION['SELL_is_highlighted']	= $RELISTEDAUCTION['highlighted'];
		$_SESSION['SELL_is_featured']		= $RELISTEDAUCTION['featured'];
		$_SESSION['SELL_is_taxed']			= $RELISTEDAUCTION['tax'];
		$_SESSION['SELL_tax_included']		= $RELISTEDAUCTION['taxinc'];
		$_SESSION['SELL_current_fee']		= $RELISTEDAUCTION['current_fee'];
		$_SESSION['SELL_item_condition']		= $RELISTEDAUCTION['item_condition'];
		$_SESSION['SELL_item_manufacturer']		= $RELISTEDAUCTION['item_manufacturer'];
		$_SESSION['SELL_item_model']		= $RELISTEDAUCTION['item_model'];
		$_SESSION['SELL_item_colour']		= $RELISTEDAUCTION['item_colour'];
		$_SESSION['SELL_item_year']		= $RELISTEDAUCTION['item_year'];
		$_SESSION['SELL_returns']		= $RELISTEDAUCTION['returns'];
		$_SESSION['SELL_sell_type']		= $RELISTEDAUCTION['sell_type'];
		if ($RELISTEDAUCTION['bn_only'] == 'n')
		{
			$_SESSION['SELL_minimum_bid'] = $system->print_money_nosymbol($RELISTEDAUCTION['minimum_bid']);
		}
		else
		{
			$_SESSION['SELL_minimum_bid'] = 0;
		}

		if (floatval($RELISTEDAUCTION['reserve_price']) > 0)
		{
			$_SESSION['SELL_reserve_price'] = $system->print_money_nosymbol($RELISTEDAUCTION['reserve_price']);
			$_SESSION['SELL_with_reserve'] 	= 'yes';
		}
		else
		{
			$_SESSION['SELL_reserve_price'] = '';
			$_SESSION['SELL_with_reserve'] 	= 'no';
		}

		$_SESSION['SELL_sellcat1']	= $RELISTEDAUCTION['category'];
		$_SESSION['SELL_sellcat2']	= $RELISTEDAUCTION['secondcat'];

		if (floatval($RELISTEDAUCTION['buy_now']) > 0)
		{
			$_SESSION['SELL_buy_now_price'] = $system->print_money_nosymbol($RELISTEDAUCTION['buy_now']);
			$_SESSION['SELL_with_buy_now']	= 'yes';
		}
		else
		{
			$_SESSION['SELL_buy_now_price'] = '';
			$_SESSION['SELL_with_buy_now'] 	= 'no';
		}
		$_SESSION['SELL_duration']	= $RELISTEDAUCTION['duration'];
		$_SESSION['SELL_relist']	= $RELISTEDAUCTION['relist'];
		if (floatval($RELISTEDAUCTION['increment']) > 0)
		{
			$_SESSION['SELL_increment']			= 2;
			$_SESSION['SELL_customincrement']	= $system->print_money_nosymbol($RELISTEDAUCTION['increment']);
		}
		else
		{
			$_SESSION['SELL_increment']			= 1;
			$_SESSION['SELL_customincrement']	= 0;
		}
		$_SESSION['SELL_shipping_cost']	 = $system->print_money_nosymbol($RELISTEDAUCTION['shipping_cost']);
		$_SESSION['SELL_additional_shipping_cost']	= $system->print_money_nosymbol($RELISTEDAUCTION['shipping_cost_additional']);
		$_SESSION['SELL_shipping']		 = $RELISTEDAUCTION['shipping'];
		$_SESSION['SELL_shipping_terms'] = $RELISTEDAUCTION['shipping_terms'];
		$_SESSION['SELL_payment']		 = explode(', ', $RELISTEDAUCTION['payment']);
		$_SESSION['SELL_international']	 = $RELISTEDAUCTION['international'];
		$_SESSION['SELL_file_uploaded']	 = $RELISTEDAUCTION['photo_uploaded'];
		$_SESSION['SELL_pict_url']		 = $RELISTEDAUCTION['pict_url'];
		$_SESSION['SELL_pict_url_temp']	 = str_replace('thumb-', '', $RELISTEDAUCTION['pict_url']);

		// get gallery images
		$UPLOADED_PICTURES = array();
		$file_types = array('gif', 'jpg', 'jpeg', 'png');
		if (is_dir($upload_path . intval($_GET['id'])))
		{
			$dir = opendir($upload_path . intval($_GET['id']));
			while (($myfile = readdir($dir)) !== false)
			{
				if ($myfile != '.' && $myfile != '..' && !is_file($myfile))
				{
					$file_ext = strtolower(substr($myfile, strrpos($myfile, '.') + 1));
					if (in_array($file_ext, $file_types) && (strstr($RELISTEDAUCTION['pict_url'], 'thumb-') === false || $RELISTEDAUCTION['pict_url'] != $myfile))
					{
						$UPLOADED_PICTURES[] = $myfile;
					}
				}
			}
			closedir($dir);
		}
		$_SESSION['UPLOADED_PICTURES'] = $UPLOADED_PICTURES;

		if (count($UPLOADED_PICTURES) > 0)
		{
			if (!file_exists($upload_path . session_id()))
			{
				umask();
				mkdir($upload_path . session_id(), 0755);
			}
			foreach ($UPLOADED_PICTURES as $k => $v)
			{
				$system->move_file($uploaded_path . intval($_GET['id']) . '/' . $v, $uploaded_path . session_id() . '/' . $v, false);
			}
			if (!empty($RELISTEDAUCTION['pict_url']))
			{
				$system->move_file($uploaded_path . intval($_GET['id']) . '/' . $RELISTEDAUCTION['pict_url'], $uploaded_path . session_id() . '/' . $RELISTEDAUCTION['pict_url'], false);
			}
		}
		
		//checking to see if it is a digital item auction
		$query = "SELECT item FROM " . $DBPrefix . "digital_items WHERE auctions = :id AND seller = :user_id";
		$params = array();
		$params[] = array(':id', $RELISTEDAUCTION['id'], 'int');
		$params[] = array(':user_id', $user->user_data['id'], 'int');
		$db->query($query, $params);
		if($db->numrows() == 1 && $RELISTEDAUCTION['auction_type'] == 3)
		{	
			$digital_item = $db->result('item');
			$_SESSION['SELL_upload_file'] = $digital_item;
			//make the temp folder
			if (!file_exists($uploaded_path . session_id() . '/items'))
			{
				umask();
				mkdir($uploaded_path . session_id() . '/items', 0755);
			}
			//copy the digital item to the temp folder
			$system->move_file($uploaded_path . 'items/' . $user->user_data['id'] . '/' . intval($RELISTEDAUCTION['id']) . '/' . $digital_item, $uploaded_path . session_id() . '/items/' . $digital_item, false);
		}
		
		$_SESSION['SELL_action'] = 'edit';
		if ($_SESSION['SELL_starts'] > $NOW)
		{
			$_SESSION['editstartdate'] = true;
		}
		else
		{
			$_SESSION['editstartdate'] = false;
		}
		header('location: sell.php?mode=recall');
	}
	else
	{
		header('location: home');
	}
}
