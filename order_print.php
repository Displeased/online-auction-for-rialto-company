<?php 
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $include_path . 'functions_invoices.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	header('location: user_login.php');
	exit;
}

// is this an auction invoice or fee invoice
$auction = false;
if (isset($_POST['pfval']) && isset($_POST['pfwon']))
{
	$auction = true;
	// check input data
	if (intval($_POST['pfval']) == 0 || intval($_POST['pfwon']) == 0)
	{
		invaildinvoice();
	}
}
else
{
	// check input data
	if (intval($_GET['id']) == 0 || !isset($_GET['id']))
	{
		invaildinvoice();
	}
}

$vat = 20; // NEEDS TO BE SET TO AN ADMIN OPTION
if ($auction)
{
	// get auction data
	$query = "SELECT w.id, w.winner, w.closingdate As date, a.id AS auc_id, a.title, a.shipping_cost, a.shipping_cost_additional, a.shipping, a.shipping_terms, w.bid, w.qty, a.user As seller_id, a.tax, a.taxinc 
		FROM " . $DBPrefix . "auctions a 
		LEFT JOIN " . $DBPrefix . "winners w ON (a.id = w.auction) 
		WHERE a.id = :pfval AND w.id = :pfwon";
	$params = array();
	$params[] = array(':pfval', intval($_POST['pfval']), 'str');
	$params[] = array(':pfwon', intval($_POST['pfwon']), 'int');
	$db->query($query, $params);

}
else
{
	// get fee data
	$query = "SELECT * FROM " . $DBPrefix . "useraccounts WHERE useracc_id = :user_id";
	$params = array();
	$params[] = array(':user_id', intval($_GET['id']), 'str');
	$db->query($query, $params);

}

// check its real
if ($db->numrows() < 1)
{
	invaildinvoice();
}

$data = $db->result();

if ($auction)
{
	// sort out auction data
	$seller = getSeller($data['seller_id']);
	$winner = getAddressWinner($data['winner']);
	$vat = getTax(true, $winner['country'], $seller['country']);
	$title = $system->SETTINGS['sitename'] . ' - ' . $data['title'];
	$additional_shipping = $data['shipping_cost_additional'] * ($data['qty'] - 1);
	$shipping_cost = ($shipping == 1) ? ($data['shipping_cost'] + $additional_shipping) : 0;
	$payvalue = ($data['bid'] * $data['qty']) + $shipping_cost;
	$payvalueperitem = $data['bid'];
	$paysubtotal = ($data['bid']* $data['qty']);
	$shipping_cost = ($data['shipping'] == 1) ? $data['shipping_cost'] : 0;

	// build winners address
	$winner_address = '';
	$winner_address .= (!empty($winner['address'])) ? '<br>' . $winner['address'] : '';
	$winner_address .= (!empty($winner['city'])) ? '<br>' . $winner['city'] : '';
	$winner_address .= (!empty($winner['prov'])) ? '<br>' . $winner['prov'] : '';
	$winner_address .= (!empty($winner['country'])) ? '<br>' . $winner['country'] : '';
	$winner_address .= (!empty($winner['zip'])) ? '<br>' . $winner['zip'] : '';

	if ($data['tax'] == 'n') // no tax
	{
		$unitexcl = $unitpriceincl = $paysubtotal;
		$subtotal = $totalinc = $payvalue;
		$vat = 0;
	}
	else
	{
		if ($data['taxinc'] == 'y') // tax is included in price
		{
			$unitexcl = vatexcluding($paysubtotal); // auction price - tax
			$unitpriceincl = $paysubtotal; // auction price & tax
			$subtotal = vatexcluding($payvalue); // total invoice - tax
			$totalinc = $payvalue; // total invoice & tax
		}
		else
		{
			$unitexcl = $paysubtotal; // auction price - tax
			$unitpriceincl = vat($paysubtotal); // auction price & tax
			$subtotal = $payvalue; // total invoice - tax
			$totalinc = vat($payvalue); // total invoice & tax
		}
	}

	$totalvat = $totalinc - $subtotal;
	$unitpriceincl = $totalinc / $data['qty'];
	$unitexcl = $subtotal / $data['qty'];

	// auction specific details
	$template->assign_vars(array(
			'AUCTION_TITLE' => strtoupper($title),
			'ITEM_QUANTITY' => $data['qty'],

			'UNIT_PRICE' => $system->print_money($unitexcl, true, false), // auction price
			'UNIT_PRICE_WITH_TAX' => $system->print_money($unitpriceincl, true, false),// auction price & tax
			'TOTAL' => $system->print_money($subtotal, true, false), // total invoice
			'TOTAL_WITH_TAX' => $system->print_money($totalinc, true, false) // total invoice & tax
			));
}
else
{
	$seller = getSeller($user->user_data['id']); // used as user: ??
	$vat = getTax(true, $seller['country']);
	$winner_address = '';
	$data['shipping_terms'] = '';
	$shipping_cost = 0;
	$title = $system->SETTINGS['sitename'] . ' - ' . $MSG['766'] . '#' . $data['id'];
	$payvalue = $data['total'];
	// create fee data ready for template & get totals
	$totals = setfeetemplate($data);

	// fee specific details
	$template->assign_vars(array(
			'TOTAL' => $system->print_money($totals[1], true, false),
			'TOTAL_WITH_TAX' => $system->print_money($totals[0], true, false)
			));
}

$template->assign_vars(array(
		'DOCDIR' => $DOCDIR,
		'LOGO' => ($system->SETTINGS['logo']) ? '<a href="' . $system->SETTINGS['siteurl'] . 'home"><img src="' . $incurl . $uploaded_path . 'logos/' . $system->SETTINGS['logo'] . '" border="0" alt="' . $system->SETTINGS['sitename'] . '"></a>' : '&nbsp;',
		'CHARSET' => $CHARSET,
		'LANGUAGE' => $language,
		'SENDER' => $seller['nick'],
		'WINNER_NICK' => $winner['nick'],
		'WINNER_ADDRESS' => $winner_address,
		'AUCTION_ID' => $data['auc_id'],
		'SHIPPING_METHOD' => (empty($data['shipping_terms'])) ? strtoupper($MSG['000']) : $data['shipping_terms'],
		'INVOICE_DATE' => gmdate('d/m/Y', $data['date'] + $system->tdiff),
		'SALE_ID' => (($auction) ? 'AUC' : 'FEE') . $data['id'],
		// tax start
		'TAX' => $vat . '%',
		'SHIPPING_COST' => $system->print_money($shipping_cost, true, false),
		'VAT_TOTAL' => $system->print_money($totalvat, true, false),
		'TOTAL_SUM' => $system->print_money($payvalue, true, false),
		// tax end
		'YELLOW_LINE' => $system->SETTINGS['invoice_yellow_line'],
		'THANKYOU' => $system->SETTINGS['invoice_thankyou'],

		'B_INVOICE' => true,
		'B_IS_AUCTION' => $auction
		));

$template->set_filenames(array(
		'body' => 'order_invoice.tpl'
		));
$template->display('body');
