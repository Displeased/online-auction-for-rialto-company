<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
include 'common.php';

//Used for downloading the digital items
if (isset($_GET['fromfile']) && ($_GET['diupload'] == 3))
{	
	$query = "SELECT hash FROM " . $DBPrefix . "digital_items WHERE hash = :hash_id"; 
	$params = array();
	$params[] = array(':hash_id', $security->decrypt($_GET['fromfile']), 'str');
	$db->query($query, $params);
	if($db->numrows() == 1)
	{
		include $include_path . 'functions_digital_item.php';
		download_digital_item($_GET['fromfile'], 'passed');
	}else{
		header('location: ' . $system->SETTINGS['siteurl'] . 'home');
		exit;
	}
}

// If user is not logged in redirect to login page
if (!$user->logged_in)
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'my_downloads.php';
	header('location: user_login.php');
	exit;
}

$query = "SELECT DISTINCT w.id
		FROM " . $DBPrefix . "winners w
		LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.seller)
		LEFT JOIN " . $DBPrefix . "digital_items d ON (d.auctions = w.auction)
		LEFT JOIN " . $DBPrefix . "auctions a ON (a.id = w.auction)
		WHERE w.auction = d.auctions AND w.winner = :user_id";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$db->query($query, $params);
$db->numrows('id');
if (!isset($_GET['PAGE']) || $_GET['PAGE'] <= 1 || $_GET['PAGE'] == '')
{
	$OFFSET = 0;
	$PAGE = 1;
}
else
{
	$PAGE = intval($_GET['PAGE']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}
$PAGES = ($TOTALAUCTIONS == 0) ? 1 : ceil($TOTALAUCTIONS / $system->SETTINGS['perpage']);

// Get closed auctions with winners
$query = "SELECT DISTINCT a.title, w.shipped, w.id, w.qty, w.seller, w.paid, w.feedback_win, w.bid, w.auction, d.item, d.hash, d.auctions, d.hash, d.seller, u.nick, u.email
		FROM " . $DBPrefix . "winners w
		LEFT JOIN " . $DBPrefix . "users u ON (u.id = w.seller)
		LEFT JOIN " . $DBPrefix . "digital_items d ON (d.auctions = w.auction)
		LEFT JOIN " . $DBPrefix . "auctions a ON (a.id = w.auction)
		WHERE w.auction = d.auctions AND w.winner = :user_id LIMIT :offset, :perpage";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$params[] = array(':offset', $OFFSET, 'int');
$params[] = array(':perpage', $system->SETTINGS['perpage'], 'int');
$db->query($query, $params);

while ($row = $db->result())
{
	if(!empty($row['item']))
	{
		$template->assign_block_vars('items', array(
			'AUC_ID' => $row['auction'],
			'ID' => $row['id'],
			'DIGITAL_ITEM_NAME' => $row['itme'],
			'DIGITAL_ITEM' => $security->encrypt($row['hash']),
			'SELLNICK' => $row['nick'],
			'SEO_TITLE' => generate_seo_link($row['title']),
			'SELLEMAIL' => $row['email'],
			'TITLE' => $row['title'],
			'FB_LINK' => ($row['feedback_win'] == 0) ? '<a href="' . $sslurl . 'feedback.php?auction_id=' . $row['auction'] . '&wid=' . $user->user_data['id'] . '&sid=' . $row['seller'] . '&ws=w">' . $MSG['207'] . '</a>' : '',
			'B_PAID' => ($row['paid'] == 1),
			'B_DIGITAL_ITEM' => (isset($row['item'])),
			'B_DIGITAL_ITEM_PAID' => (isset($row['item'])) ? $row['paid'] == 1 : ''
		));
	}
}

// get pagenation
$PREV = intval($PAGE - 1);
$NEXT = intval($PAGE + 1);
if ($PAGES > 1)
{
	$LOW = $PAGE - 5;
	if ($LOW <= 0) $LOW = 1;
	$COUNTER = $LOW;
	while ($COUNTER <= $PAGES && $COUNTER < ($PAGE + 6))
	{
		$template->assign_block_vars('pages', array(
				'PAGE' => ($PAGE == $COUNTER) ? '<b>' . $COUNTER . '</b>' : '<a href="' . $system->SETTINGS['siteurl'] . 'my_downloads.php?PAGE=' . $COUNTER . '"><u>' . $COUNTER . '</u></a>'
				));
		$COUNTER++;
	}
}

$template->assign_vars(array(
	'PREV' => ($PAGES > 1 && $PAGE > 1) ? '<a href="' . $system->SETTINGS['siteurl'] . 'my_downloads.php?PAGE=' . $PREV . '"><u>' . $MSG['5119'] . '</u></a>&nbsp;&nbsp;' : '',
	'NEXT' => ($PAGE < $PAGES) ? '<a href="' . $system->SETTINGS['siteurl'] . 'my_downloads.php?PAGE=' . $NEXT . '"><u>' . $MSG['5120'] . '</u></a>' : '',
	'PAGE' => $PAGE,
	'PAGES' => $PAGES,
));

include 'header.php';
$TMP_usmenutitle = $MSG['453'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'my_download.tpl'
		));
$template->display('body');
include 'footer.php';