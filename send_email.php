<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

if (($system->SETTINGS['contactseller'] == 'logged' && !$user->is_logged_in()) || $system->SETTINGS['contactseller'] == 'never')
{
	if (isset($_SESSION['REDIRECT_AFTER_LOGIN']))
	{
		header('location: ' . $_SESSION['REDIRECT_AFTER_LOGIN']);
	}
	else
	{
		header('location: home');
	}
}

if (!isset($_POST['auction_id']) && !isset($_GET['auction_id']))
{
	$auction_id = $_SESSION['CURRENT_ITEM'];
}
else
{
	$auction_id = intval($_GET['auction_id']);
}
$_SESSION['CURRENT_ITEM'] = $auction_id;

// Get item description
$query = "SELECT a.user, a.title, u.nick, u.email FROM " . $DBPrefix . "auctions a
	LEFT JOIN " . $DBPrefix . "users u ON (u.id = a.user)
	WHERE a.id = :auc_id";
$params = array();
$params[] = array(':auc_id', $auction_id, 'int');
$db->query($query, $params);

if ($db->numrows() == 0)
{
	$TPL_error_text = $ERR_606;
}
else
{
	$auction_data = $db->result();
	$seller_id = $auction_data['user'];
	$item_title = $auction_data['title'];
	$seller_nick = $auction_data['nick'];
	$seller_email = $auction_data['email'];
}

if (isset($_POST['action']) || !empty($_POST['action']))
{
	$cleaned_question = stripslashes($system->cleanvars($_POST['sender_question']));
	if ($system->SETTINGS['wordsfilter'] == 'y')
	{
		$cleaned_question = stripslashes($system->filter($cleaned_question));
	}

	// Check errors
	if (isset($_POST['action']) && (!isset($_POST['sender_name']) || !isset($_POST['sender_email']) || empty($seller_nick) || empty($seller_email)))
	{
		$TPL_error_text = $ERR_032;
	}

	if (empty($cleaned_question))
	{
		$TPL_error_text = $ERR_031;
	}

	if (isset($_POST['action']) && (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['sender_email']) || !preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $seller_email)))
	{
		$TPL_error_text = $ERR_008;
	}
	if (empty($TPL_error_text))
	{
		$mes = $MSG['337'] . ': <i>' . $seller_nick . '</i><br><br>';
		$send_email->auction_question($_POST['sender_name'], $cleaned_question, $_POST['sender_email'], $auction_id, $item_title, $seller_nick, $seller_id, $seller_email);
		
		$query = "INSERT INTO " . $DBPrefix . "messages (sentto, " . $id_type . ", sentat, message, subject, question)
			VALUES (:seller_id, :from_id, :timer, :question, :title, :auc_id)";
		$params = array();
		$params[] = array(':seller_id', $seller_id, 'int');
		$params[] = array(':from_id', $from_id, 'int');
		$params[] = array(':timer', $system->ctime, 'int');
		$params[] = array(':question', $cleaned_question, 'str');
		$params[] = array(':title', $system->cleanvars(sprintf($MSG['651'], $item_title)), 'str');
		$params[] = array(':auc_id', $auction_id, 'int');
		$db->query($query, $params);
	}
}

$template->assign_vars(array(
		'MESSAGE' => (isset($mes)) ? $mes : '',
		'ERROR' => (isset($TPL_error_text)) ? $TPL_error_text : '',
		'AUCT_ID' => $auction_id,
		'SELLER_NICK' => $seller_nick,
		'SELLER_EMAIL' => $seller_email,
		'SELLER_QUESTION' => (isset($_POST['sender_question'])) ? $_POST['sender_question'] : '',
		'ITEM_TITLE' => $item_title,
		'EMAIL' => ($user->logged_in) ? $user->user_data['email'] : ''
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'send_email.tpl'
		));
$template->display('body');
include 'footer.php';