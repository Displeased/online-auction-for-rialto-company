<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/include 'common.php';

$UPLOADED_PICTURES = $_SESSION['UPLOADED_PICTURES'];
$img = $_GET['img'];

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'IMG' => $uploaded_path . session_id() . '/' . $UPLOADED_PICTURES[$img]
		));
$template->set_filenames(array(
		'body' => 'preview_gallery.tpl'
		));
$template->display('body');
