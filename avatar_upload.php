<?php 
/*************** AVTAR UPLOAD SCRIPT ******************************/
//By nay27uk
// License: Free
/***********************************************/
include 'common.php';

$query = "SELECT * FROM " . $DBPrefix . "users WHERE id = :user_id";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$db->query($query, $params);

if($_POST['Submit'] == 'Upload') 
{
 
// This is the unique user_id
$id = $user->user_data['id'];

if (!empty ($_FILES['ifile']['tmp_name']))
{

/* Thumbnail class is required */

include_once('inc/phpthumb/ThumbLib.inc.php');

/* GetImageSize() function pulls out valid info about image such as image type, height etc. If it fails 
then it is not valid image. */

 if (!getimagesize($_FILES['ifile']['tmp_name']))
  { 
   $ERR = "Error - Invalid Image File.";
  
  }

$imgtype = array('1' => '.gif', '2' => '.jpg' , '3' => '.png', '4' => '.jpeg', '5' => '.x-png', '6' => '.pjpeg');
  
  // extract the width and height of image
   
  list($width, $height, $type, $attr) = getimagesize($_FILES['ifile']['tmp_name']);
 
 // Extract the image extension
 
  switch ($type)
  {
  case 1: $ext='.gif'; break;
  case 2: $ext = '.jpg'; break;
  case 3: $ext='.png'; break;
  case 4: $ext='.jpeg'; break;
  case 5: $ext='.x-png'; break;
  case 6: $ext='.pjpeg'; break;
  }
  // Dont allow gif files to upload as it may  contain harmful code
  
  if ( $ext == '.gif') {
    $ERR = "Sorry - GIF not allowed. Please use only PNG or JPEG formats";
    }
  
 /* Specify maximum height and width of users uploading image */
 
   if ($width > 1000 || $height > 1000)
  {
   $ERR = "ERROR: Maximum width and height exceeded.";
   
  }
  /* Specify maximum file size here in bytes */
   
  if ($_FILES['ifile']['size'] > $system->SETTINGS['maxuploadsize'])
    {
    $ERR = "Error: Large File size.";
    
    }
     
    /******** IMAGE RESIZING *********************/
    // Before we start resizing, we first have to move the image file to server
    // save it there under a unique name and then do the final resizing and save the resized image.
    
    // Specify which directory you want to upload. It should be a subfolder where the script is present
    // We also generate a unique name for picture FILE-USERID-XXX where xxx is random number
    // The uploads folder must have writable permissions.
    $uploaddir = 'uploaded/avatar/' . $id . '/';
    if (!is_dir($uploaddir)) mkdir($uploaddir, 0777);
	if (!is_dir($uploaddir)) chmod($uploaddir, 0777);
    $secondname = rand(100,99);
    $uploadfile =  $uploaddir . "img-$id-$secondname". $ext;
    
    if (!move_uploaded_file($_FILES['ifile']['tmp_name'], $uploadfile ))
     {
       $ERR = "Error moving the uploaded file";
     }
    
    $thumb = PhpThumbFactory::create($uploadfile);
    
    //specify the height and width of avatar image to resize
    
    $thumb->resize(100,100);
    $thumb->save($uploadfile);
    //$thumb->show();
    
    //MySQL query to update avatar filename in the database. You need to create a field avatar
    
    $query = "UPDATE " . $DBPrefix . "users SET avatar = '" . $system->SETTINGS['siteurl'] . $uploadfile . "' WHERE id = :user_id";
    $params = array();
	$params[] = array(':user_id', $user->user_data['id'], 'int');
	$db->query($query, $params);
    $user->user_data['avatar'] = $uploadfile;
    $ERR = 'file was uploaded successfully';
    
    //$thumb->destruct();
    }
 } 
if(isset($ERR))
{
	$_SESSION['AVATAR_ERRORS'] = $ERR;
}

header ("Location: edit_data.php");
exit;
  
?> 

