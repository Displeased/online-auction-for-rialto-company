<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
if (!defined('InuAuctions')) exit();

if (!isset($_SERVER['SCRIPT_NAME'])) $_SERVER['SCRIPT_NAME'] = 'cron.php';
if($system->SETTINGS['cronlog'] == 'y')
{
	define('LogCron', true);
}

include $include_path . 'functions_cron.php';

$NOW = $system->ctime;

// initialize cron script
$categories = constructCategories();

/**
 * ------------------------------------------------------------
 * 1) "close" expired auctions
 * closing auction means:
 * a) update database:
 * + "auctions" table
 * + "categories" table - for counters
 * + "counters" table
 * b) send email to winner (if any) - passing seller's data
 * c) send email to seller (reporting if there was a winner)
 */

$NOWB = gmdate('Ymd');
$buyer_emails = array();
$seller_emails = array();

//used for the cron logging
$corn_log = '';

// get buyer fee
$query = "SELECT value, fee_type FROM " . $DBPrefix . "fees WHERE type = :buyer_fee";
$params = array();
$params[] = array(':buyer_fee', 'buyer_fee', 'str');
$db->query($query, $params);
$row = $db->result();
$buyer_fee = $row['value'];
$buyer_fee = (empty($buyer_fee)) ? 0 : $buyer_fee;
$buyer_fee_type = $row['fee_type'];

// get closed auction fee
$query = "SELECT * FROM " . $DBPrefix . "fees WHERE type = :endauc_fee ORDER BY value ASC";
$params = array();
$params[] = array(':endauc_fee', 'endauc_fee', 'str');
$db->query($query, $params);

$endauc_fee = array();
while($row = $db->result())
{
	$endauc_fee[] = $row;
}

// get a list of all ended auctions
$query = "SELECT a.*, u.email, u.endemailmode, u.nick, u.payment_details, u.name
		FROM " . $DBPrefix . "auctions a
		LEFT JOIN " . $DBPrefix . "users u ON (a.user = u.id)
		WHERE a.ends <= :time 
		AND ((a.closed = 0) 
		OR (a.closed = 1 
		AND a.reserve_price > :reserve 
		AND a.num_bids > :bids 
		AND a.current_bid < a.reserve_price 
		AND a.sold = :item_sold))";
$params = array();
$params[] = array(':time', $NOW, 'int');
$params[] = array(':reserve', 0, 'int');
$params[] = array(':bids', 0, 'int');
$params[] = array(':item_sold', 's', 'str');
$db->query($query, $params);

$count_auctions = $num = $db->numrows();
$closed = 0;
$n = 1;
$sent_winner_emails = 0;
$sent_seller_emails = 0;
$suspend = 0;
$relisted = 0;
$dutch_winners = 0;
$standard_winners = 0;
foreach ($db->fetchall() as $Auction) // loop auctions
{
	$n++;
	$report_text = '';
	$Auction['description'] = strip_tags($Auction['description']);

	// Send notification to all users watching this auction
	sendWatchEmails($Auction['id']);

	// set seller array
	$Seller = array(
		'id' => $Auction['user'],
		'email' => $Auction['email'],
		'endemailmode' => $Auction['endemailmode'],
		'nick' => $Auction['nick'],
		'payment_details' => $Auction['payment_details'],
		'name' => $Auction['name']);

	// get an order list of bids of the item (high to low)
	$winner_present = false;
	$query = "SELECT u.* FROM " . $DBPrefix . "bids b
			LEFT JOIN " . $DBPrefix . "users u ON (b.bidder = u.id)
			WHERE auction = :auc_id ORDER BY b.bid DESC, b.quantity DESC, b.id DESC";
	$params = array();
	$params[] = array(':auc_id', $Auction['id'], 'int');
	$db->query($query, $params);
	$num_bids = $db->numrows();

	// send email to seller - to notify him
	// create a "report" to seller depending of what kind auction is
	$atype = intval($Auction['auction_type']);
	$sellType = $Auction['sell_type'];
	
	// Standard Auction
	if ($atype == 1)
	{
		if (($num_bids > 0 && ($Auction['current_bid'] >= $Auction['reserve_price'] || $Auction['sold'] == 's') && $sellType == 'sell') || $sellType == 'free')
		{
			$Winner = $db->result();
			$Winner['quantity'] = $Auction['quantity'];
			$WINNING_BID = $Auction['current_bid'];
			$winner_present = true;
		}
		if ($winner_present)
		{
			$report_text = $Winner['nick'] . "\n";
			if ($system->SETTINGS['users_email'] == 'n')
			{
				$report_text .= ' (<a href="mailto:' . $Winner['email'] . '">' . $Winner['email'] . '</a>)' . "\n";
			}
			if ($Winner['address'] != '')
			{
				$report_text .= $MSG['30_0086'] . $Winner['address'] . ' ' . $Winner['city'] . ' ' . $Winner['prov'] . ' ' . $Winner['zip'] . ', ' . $Winner['country'];
			}
			$bf_paid = 1; // buyer fee payed?
			$ff_paid = 1; // auction end fee payed?
			// work out & add fee
			if ($system->SETTINGS['fees'] == 'y')
			{
				sortFees();
			}
			
			if ($Auction['sell_type'] == 'free' && $Auction['shipping_cost'] == 0) $B_freeItem = 1;
			elseif ($Auction['sell_type'] == 'free' && $Auction['shipping_cost'] > 0) $B_freeItem = 0;
			elseif ($Auction['sell_type'] == 'sell') $B_freeItem = 0;
			
			// Add winner's data to "winners" table
			$query = "INSERT INTO " . $DBPrefix . "winners (id, auction, seller, winner, bid, closingdate, feedback_win, feedback_sel, qty, paid, bf_paid, ff_paid, shipped, is_read, is_counted) VALUES
				(NULL, :auction_id, :seller_id, :winner_id, :price, :time_stamp, 0, 0, 1, $B_freeItem, :bf_paid, :ff_paid, 0, 0, 'n')";
			$params = array();
			$params[] = array(':auction_id', $Auction['id'], 'int');
			$params[] = array(':seller_id', $Seller['id'], 'int');
			$params[] = array(':winner_id', $Winner['id'], 'int');
			$params[] = array(':price', $Auction['current_bid'], 'float');
			$params[] = array(':time_stamp', $NOW, 'int');
			$params[] = array(':bf_paid', $bf_paid, 'int');
			$params[] = array(':ff_paid', $ff_paid, 'int');
			$db->query($query, $params);
			$new_winner_id = $db->lastInsertId();
			$standard_winners++;
		}
		else
		{
			$report_text = $MSG['429'];
		}
	}
	// Dutch Auction
	if ($atype == 2 && $sellType == 'sell')
	{
		// find out winners sorted by bid
		$query = "SELECT *, MAX(bid) AS maxbid
				FROM " . $DBPrefix . "bids WHERE auction = :auc_id GROUP BY bidder
				ORDER BY maxbid DESC, quantity DESC, id DESC";
		$params = array();
		$params[] = array(':auc_id', $Auction['id'], 'int');
		$db->query($query, $params);

		$num_bids = $num_bids + $db->numrows();
		$WINNERS_ID = array();
		$winner_array = array();
		$items_count = $Auction['quantity'];
		$items_sold = 0;
		foreach ($db->fetchall() as $row) // load every bid
		{
			if (!in_array($row['bidder'], $WINNERS_ID))
			{
				$items_wanted = $row['quantity'];
				$items_got = 0;
				if ($items_wanted <= $items_count)
				{
					$items_got = $items_wanted;
				}
				else
				{
					$items_got = $items_count;
				}
				$items_count -= $items_got;
				$items_sold += $items_got;

				// Retrieve winner nick from the database
				$query = "SELECT id, nick, email, name, address, city, zip, prov, country
						FROM " . $DBPrefix . "users WHERE id = :bidder LIMIT 1";
				$params = array();
				$params[] = array(':bidder', $row['bidder'], 'int');
				$db->query($query, $params);
				$Winner = $db->result();
				// set arrays
				$WINNERS_ID[] = $row['bidder'];
				$Winner['maxbid'] = $row['maxbid'];
				$Winner['quantity'] = $items_got;
				$Winner['wanted'] = $items_wanted;
				$winner_array[] = $Winner; // set array ready for emails
				$report_text .= ' ' . $MSG['159'] . ' ' . $Winner['nick'];
				if ($system->SETTINGS['users_email'] == 'n')
				{
					$report_text .= ' (' . $Winner['email'] . ')';
				}
				$report_text .= ' ' . $items_got . ' ' . $MSG['5492'] . ', ' . $MSG['5493'] . ' ' . $system->print_money($row['bid']) . ' ' . $MSG['5495'] . ' - (' . $MSG['5494'] . ' ' . $items_wanted . ' ' . $MSG['5492'] . ')' . "\n";
				$report_text .= ' ' . $MSG['30_0086'] . $ADDRESS . "\n";

				$bf_paid = 1;
				$ff_paid = 1;
				// work out & add fee
				if ($system->SETTINGS['fees'] == 'y')
				{
					sortFees();
				}
				
				if ($Auction['sell_type'] == 'free' && $Auction['shipping_cost'] == 0) $B_freeItem = 1;
				elseif ($Auction['sell_type'] == 'free' && $Auction['shipping_cost'] > 0) $B_freeItem = 0;
				elseif ($Auction['sell_type'] == 'sell') $B_freeItem = 0;
				
				// Add winner's data to "winners" table
				$query = "INSERT INTO " . $DBPrefix . "winners (id, auction, seller, winner, bid, closingdate, feedback_win, feedback_sel, qty, paid, bf_paid, ff_paid, shipped, is_read, is_counted) VALUES
					(NULL, :auction_id, :seller_id, :winner_id, :price, :time_stamp, 0, 0, 1, $B_freeItem, :bf_paid, :ff_paid, 0, 0, 'n')";
				$params = array();
				$params[] = array(':auction_id', $Auction['id'], 'int');
				$params[] = array(':seller_id', $Seller['id'], 'int');
				$params[] = array(':winner_id', $Winner['id'], 'int');
				$params[] = array(':price', $Auction['current_bid'], 'float');
				$params[] = array(':time_stamp', $NOW, 'int');
				$params[] = array(':bf_paid', $bf_paid, 'int');
				$params[] = array(':ff_paid', $ff_paid, 'int');
				$db->query($query, $params);
				$new_winner_id = $db->lastInsertId();
				$dutch_winners++;
			}
			if ($items_count == 0)
			{
				break;
			}
		}
	} 
	// end auction ends
	$month = gmdate('m', $Auction['ends'] + $system->tdiff);
	$ends_string = $MSG['MON_0' . $month] . ' ' . gmdate('d, Y H:i', $Auction['ends'] + $system->tdiff);

	$close_auction = true;
	// deal with the automatic relists find which auctions are to be relisted
	if ($Auction['relist'] > 0 && ($Auction['relist'] - $Auction['relisted']) > 0 && $Auction['suspended'] == 0)
	{
		$query = "SELECT id FROM " . $DBPrefix . "bids WHERE auction = :auc_id";
		$params = array();
		$params[] = array(':auc_id', $Auction['id'], 'int');
		$db->query($query, $params);

		// noone won the auction so remove bids and start it again
		if ($db->numrows('id') == 0 || ($db->numrows('id') > 0 && $Auction['reserve_price'] > 0 && !$winner_present))
		{
			// Calculate end time
			$_ENDS = $NOW + ($Auction['duration'] * 24 * 60 * 60);

			$query = "DELETE FROM " . $DBPrefix . "bids WHERE auction = :bauc_id";
			$params = array();
			$params[] = array(':bauc_id', $Auction['id'], 'int');
			$db->query($query, $params);
			
			$query = "DELETE FROM " . $DBPrefix . "proxybid WHERE itemid = :pauc_id";
			$params = array();
			$params[] = array(':pauc_id', $Auction['id'], 'int');
			$db->query($query, $params);
			
			$query = "UPDATE " . $DBPrefix . "auctions SET starts = :set_time, ends = :new_end, current_bid = 0, num_bids = 0, relisted = :add_one WHERE id = :auc_id";
			$uparams = array();
			$uparams[] = array(':set_time', $NOW, 'int');
			$uparams[] = array(':new_end', $_ENDS, 'int');
			$uparams[] = array(':add_one', $Auction['relisted'] + 1, 'int');
			$uparams[] = array(':auc_id', $Auction['id'], 'int');
			$db->query($query, $uparams);
			$close_auction = false;
			$count_auctions--;
			$relisted++;
		}
	}

	if ($Auction['suspended'] != 0)
	{
		$count_auctions--;
		$suspend++;
	}

	if ($close_auction)
	{
		// update category tables
		$cat_id = $Auction['category'];
		$root_cat = $cat_id;
		$second_cat = false;
		while ($cat_id != -1 && isset($categories[$cat_id]))
		{
			// update counter for this category
			$R_counter = intval($categories[$cat_id]['counter']) - 1;
			$R_sub_counter = intval($categories[$cat_id]['sub_counter']) - 1;
			if ($cat_id == $root_cat)
				--$R_counter;
			if ($R_counter < 0)
				$R_counter = 0;
			if ($R_sub_counter < 0)
				$R_sub_counter = 0;
			$categories[$cat_id]['counter'] = $R_counter;
			$categories[$cat_id]['sub_counter'] = $R_sub_counter;
			$categories[$cat_id]['updated'] = true;
			if ($cat_id == $categories[$cat_id]['parent_id']) // incase something messes up
				break;
			$cat_id = $categories[$cat_id]['parent_id'];

			if (!$second_cat && !($cat_id != -1 && isset($categories[$cat_id])) && $system->SETTINGS['extra_cat'] == 'y' && $Auction['secondcat'] != 0)
			{
				$second_cat = true;
				$cat_id = $Auction['secondcat'];
				$root_cat = $cat_id;
			}
		}

		// Close auction
		$cparams = array();
		if ($Auction['sold'] != 's' AND $Auction['num_bids'] > 0 AND $Auction['reserve_price'] > 0 AND $Auction['current_bid'] < $Auction['reserve_price'])
		{
			$cquery = "UPDATE " . $DBPrefix . "auctions SET closed = :close, sold = :notsold WHERE id = :cauc_id";
			$cparams[] = array(':notsold', 'n', 'str');
			
        }
		else
		{
			$cquery = "UPDATE " . $DBPrefix . "auctions SET closed = :close, sold = :didsold WHERE id = :cauc_id";
			$cparams[] = array(':didsold', 'y', 'str');
        }
        $cparams[] = array(':close', 1, 'int');
		$cparams[] = array(':cauc_id', $Auction['id'], 'int');
		$db->query($cquery, $cparams);
		$closed++;
	}

	// WINNER PRESENT
	if ($winner_present)
	{
		// Send mail to the seller
		$send_email->winner($Auction['title'], $Auction['id'], $Auction['pict_url'], $Auction['current_bid'], $Auction['quantity'], $ends_string, $Auction['user'], $Winner['id'], $new_winner_id);
		$sent_winner_emails++;
		
		if (isset($winner_array) && is_array($winner_array) && count($winner_array) > 0)
		{
			for ($i = 0, $count = count($winner_array); $i < $count; $i++)
			{
				// Send mail to the buyer
				$Winner = $winner_array[$i];
				$send_email->youwin($Auction['description'], $Winner['wanted'], $Winner['quantity'], $Auction['title'], $Auction['id'], $Winner['current_bid'], $ends_string, $Seller['id'], $Seller['payment_details'], $Winner['id']);
				$sent_winner_emails++;
			}
		}
		elseif (is_array($Winner))
		{
			// Send mail to the buyer
			$send_email->youwin_nodutch($Auction['title'], $Auction['pict_url'], $Auction['id'], $Auction['current_bid'], $ends_string, $Seller['id'], $Winner['id']);
			$sent_winner_emails++;
		}
	}
	else
	{
		// Send mail to the seller if no winner
		if ($Seller['endemailmode'] != 'cum')
		{
			$send_email->nowinner($Auction['title'], $Auction['id'], $ends_string, $Auction['pict_url'], $Seller['id']);
			$sent_seller_emails++; //add to the $sent_seller_emails to count how many seller emails was sent
		}
		else
		{
			// Save in the database to send later
			$query = "INSERT INTO " . $DBPrefix . "pendingnotif VALUES
				(NULL, :auc_id, :seller_id, '', :auction_data, :seller_data, :date)";
			$params = array();
			$params[] = array(':auc_id', $Auction['id'], 'int');
			$params[] = array(':seller_id', $Auction['id'], 'int');
			$params[] = array(':auction_data', serialize($Auction), 'str');
			$params[] = array(':seller_data', serialize($Seller), 'str');
			$params[] = array(':date', gmdate('Ymd'), 'int');
			$db->query($query, $params);
			$sent_seller_emails++;
		}
	}
	// Update bid counter
	$query = "UPDATE " . $DBPrefix . "counters SET bids = (bids - :num_bids)";
	$params = array();
	$params[] = array(':num_bids', $num_bids, 'int');
	$db->query($query, $params);
}

$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015617'];
$corn_log .= '<strong>' . $standard_winners . '</strong>' . $MSG['3500_1015618'];

$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015615'];
$corn_log .= '<strong>' . $dutch_winners . '</strong>' . $MSG['3500_1015616'];

$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015613'];
$corn_log .= '<strong>' . $relisted . '</strong>' . $MSG['3500_1015614'];

$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015611'];
$corn_log .= '<strong>' . $suspend . '</strong>' . $MSG['3500_1015612'];

$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015590'];
$corn_log .= '<strong>' . $closed . '</strong>' . $MSG['3500_1015591'];

$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015607'];
$corn_log .= '<strong>' . $sent_winner_emails . '</strong>' . $MSG['3500_1015608'];

$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015609'];
$corn_log .= '<strong>' . $sent_seller_emails . '</strong>' . $MSG['3500_1015610'];

$query = "UPDATE " . $DBPrefix . "counters SET auctions = (auctions - :num_aucs), closedauctions = (closedauctions + :num_aucs)";
$params = array();
$params[] = array(':num_aucs', $count_auctions, 'int');
$db->query($query, $params);
if (count($categories) > 0)
{
	foreach ($categories as $cat_id => $category)
	{
		if ($category['updated'])
		{
			$query = "UPDATE " . $DBPrefix . "categories SET
					 counter = :counter,
					 sub_counter = :sub_counter
					 WHERE cat_id = :cat_id";
			$params = array();
			$params[] = array(':counter', $category['counter'], 'int');
			$params[] = array(':sub_counter', $category['sub_counter'], 'int');
			$params[] = array(':cat_id', $cat_id, 'int');
			$db->query($query, $params);
		}
	}
}


// "remove" old Unpaid auctions (archive them)
$expiredUnpaidTime = $system->ctime - 60 * 60 * 24 * $system->SETTINGS['expire_unpaid'];
$query = "SELECT id FROM " . $DBPrefix . "winners WHERE closingdate <= :expiredTime AND paid = 0";
$params = array();
$params[] = array(':expiredTime', $expiredUnpaidTime, 'int');
$db->query($query, $params);
$deleted_unpaid = 0;
if ($db->numrows('id') > 0)
{
	foreach ($db->fetchall() as $UnpaidInfo)
	{
		// delete Unpaid auctions
		$query = "DELETE FROM " . $DBPrefix . "winners WHERE id = :winner_id";
		$params = array();
		$params[] = array(':winner_id', $UnpaidInfo['id'], 'int');
		$db->query($query, $params);
		$deleted_unpaid++;
	}
}
$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015605'];
$corn_log .= '<strong>' . $deleted_unpaid . '</strong>' . $MSG['3500_1015606'];

// "remove" old auctions (archive them)
$expiredTime = $system->ctime - 60 * 60 * 24 * $system->SETTINGS['archiveafter'];
$query = "SELECT id FROM " . $DBPrefix . "auctions WHERE ends <= :expiredTime";
$params = array();
$params[] = array(':expiredTime', $expiredTime, 'int');
$db->query($query, $params);
$archive_auctions = 0;
if ($db->numrows('id') > 0)
{
	foreach ($db->fetchall() as $AuctionInfo)
	{
		// delete auction
		$query = "DELETE FROM " . $DBPrefix . "auctions WHERE id = :auc_id";
		$params = array();
		$params[] = array(':auc_id', $AuctionInfo['id'], 'int');
		$db->query($query, $params);

		// delete bids for this auction
		$query = "DELETE FROM " . $DBPrefix . "bids WHERE auction = :auc_id";
		$params = array();
		$params[] = array(':auc_id', $AuctionInfo['id'], 'int');
		$db->query($query, $params);

		// Delete proxybid entries
		$query = "DELETE FROM " . $DBPrefix . "proxybid WHERE itemid = :auc_id";
		$params = array();
		$params[] = array(':auc_id', $AuctionInfo['id'], 'int');
		$db->query($query, $params);

		// Delete counter entries
		$query = "DELETE FROM " . $DBPrefix . "auccounter WHERE auction_id = :auc_id";
		$params = array();
		$params[] = array(':auc_id', $AuctionInfo['id'], 'int');
		$db->query($query, $params);

		// Delete all images
		if (file_exists($upload_path . $AuctionInfo['id']))
		{
			if ($dir = @opendir($upload_path . $AuctionInfo['id']))
			{
				while ($file = readdir($dir))
				{
					if ($file != '.' && $file != '..')
					{
						@unlink($upload_path . $AuctionInfo['id'] . '/' . $file);
					}
				}
				closedir($dir);
				@rmdir($upload_path . $AuctionInfo['id']);
			}
		}
		$archive_auctions++;
	}
}
$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015594'];
$corn_log .= '<strong>' . $archive_auctions . '</strong>' . $MSG['3500_1015595'];


// send cumulative emails
$query = "SELECT id, name, email FROM " . $DBPrefix . "users WHERE endemailmode = :emailmode";
$params = array();
$params[] = array(':emailmode', 'cum', 'str');
$db->query($query, $params);
$cumulative_emails = 0;
foreach ($db->fetchall() as $row)
{
	$cum_query = "SELECT * FROM " . $DBPrefix . "pendingnotif WHERE thisdate < :date AND seller_id = :seller_id";
	$cum_params = array();
	$cum_params[] = array(':date', gmdate('Ymd'), 'int');
	$cum_params[] = array(':seller_id', $row['id'], 'int');
	$db->query($cum_query, $cum_params);

	if ($db->numrows() > 0)
	{
		foreach ($db->fetchall() as $pending)
		{
			$Auction = unserialize($pending['auction']);
			$Seller = unserialize($pending['seller']);
			$report .= "-------------------------------------------------------------------------\n" . 
						$Auction['title'] . "\n" . 
						"-------------------------------------------------------------------------\n";
			if(strlen($pending['winners']) > 0)
			{
				$report .= $MSG['453'] . ':' . "\n" . $pending['winners'] . "\n\n";
			}
			else
			{
				$report .= $MSG['1032']."\n\n";
			}
			$query = "DELETE FROM " . $DBPrefix . "pendingnotif WHERE id = :pending_id";
			$params = array();
			$params[] = array(':pending_id', $pending['id'], 'int');
			$db->query($query, $params);
		}
		$send_email->cumulative($report, $row['name'], $row['email'], $row['id']);
		$cumulative_emails++;
	}
}
$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015603'];
$corn_log .= '<strong>' . $cumulative_emails . '</strong>' . $MSG['3500_1015604'];

// send buyer fee emails
$fee_emails = 0;
if ($buyer_fee > 0)
{
	for ($i = 0; $i < count($buyer_emails); $i++)
	{
		$send_email->buyer_fee_email($buyer_emails[$i]['id'], $buyer_emails[$i]['title'], $buyer_emails[$i]['name'], $buyer_emails[$i]['name'], $Auction['id'], $buyer_emails[$i]['uid'], $buyer_emails[$i]['email']);
		$fee_emails++;
	}
}
for ($i = 0; $i < count($seller_emails); $i++)
{
	$send_email->final_value_fee_email($seller_emails[$i]['id'], $seller_emails[$i]['title'], $seller_emails[$i]['name'], $Auction['id'], $Auction['id'], $seller_emails[$i]['uid'], $seller_emails[$i]['email']);
	$fee_emails++;
}

$corn_log .= "<br>";
$corn_log .= $MSG['3500_1015602'];
$corn_log .= '<strong>' . $fee_emails . '</strong>' . $MSG['3500_1015601'];


// Purging thumbnails cache
//checking see if the cache folder is found
if (!file_exists($upload_path . 'cache'))
{
	mkdir($upload_path . 'cache', 0755);
}
//checking see if the purge page is found in the cache folder
if (!file_exists($upload_path . 'cache/purge'))
{
	touch($upload_path . 'cache/purge');
}
//now checking the cache time and deleting old cache files
if (($system->ctime - filectime($upload_path . 'cache/purge')) > 1800) //purge image cache every 30minutes
{
	$purged = 0;
	if ($dh = opendir($upload_path . 'cache'))
	{
		while (($file = readdir($dh)) !== false)
		{
			if ($file != 'purge' && !is_dir($upload_path . 'cache/' . $file) && ($system->ctime - filectime($upload_path . 'cache/' . $file)) > 900)
			{
				unlink($upload_path . 'cache/' . $file);
				$purged++;
			}
		}
		closedir($dh);
	}
	touch($upload_path . 'cache/purge');
	
	$corn_log .= "<br>";
	$corn_log .= $MSG['3500_1015596'];
	$corn_log .= '<strong>' . $purged . '</strong>' . $MSG['3500_1015597'];
}

//-----update the counters handler------//
//get all active users
$query = "SELECT id FROM " . $DBPrefix . "users WHERE suspended = :is_suspended";
$params = array();
$params[] = array(':is_suspended', 0, 'int');
$db->query($query, $params);
$active_users = $db->numrows('id');
			
//get all suspended users
$query = "SELECT id FROM " . $DBPrefix . "users WHERE suspended = :is_suspended";
$params = array();
$params[] = array(':is_suspended', 8, 'int');
$db->query($query, $params);
$inactive_users = $db->numrows('id');
			
//get all open auction
$query = "SELECT id FROM " . $DBPrefix . "auctions WHERE closed = :close AND suspended = :suspend";
$params = array();
$params[] = array(':close', 0, 'int');
$params[] = array(':suspend', 0, 'int');
$db->query($query, $params);
$active_auctions = $db->numrows('id');
			
//get all closed auction
$query = "SELECT id FROM " . $DBPrefix . "auctions WHERE closed != :close";
$params = array();
$params[] = array(':close', 0, 'int');
$db->query($query, $params);
$closed_auctions = $db->numrows('id');	

//get all suspended auctions
$query = "SELECT id FROM " . $DBPrefix . "auctions WHERE closed = :close and suspended != :suspend";
$params = array();
$params[] = array(':close', 0, 'int');
$params[] = array(':suspend', 0, 'int');
$db->query($query, $params);
$suspended_auctions = $db->numrows('id');
			
//get all bids
$query = "SELECT b.id FROM " . $DBPrefix . "bids b
	LEFT JOIN " . $DBPrefix . "auctions a ON (b.auction = a.id)
	WHERE a.closed = :close AND a.suspended = :suspend";
$params = array();
$params[] = array(':close', 0, 'int');
$params[] = array(':suspend', 0, 'int');
$db->query($query, $params);
$user_bids = $db->numrows('id');
	
$query = "SELECT id FROM " . $DBPrefix . "winners WHERE paid = :is_paid";
$params = array();
$params[] = array(':is_paid', 1, 'int');
$db->query($query, $params);
$paid_items = $db->numrows('id');

//now update all the counters in the database
$query = "UPDATE " . $DBPrefix . "counters SET users = :set_active_users, inactiveusers = :set_inactive_users, auctions = :set_open_auctions, closedauctions = :set_closed_auctions, suspendedauctions = :set_suspended_auctions, bids = :set_bids, items_sold = :set_paid_items";
$params = array();
$params[] = array(':set_active_users', $active_users, 'int');
$params[] = array(':set_inactive_users', $inactive_users, 'int');
$params[] = array(':set_open_auctions', $active_auctions, 'int');
$params[] = array(':set_closed_auctions', $closed_auctions, 'int');
$params[] = array(':set_suspended_auctions', $suspended_auctions, 'int');
$params[] = array(':set_bids', $user_bids, 'int');
$params[] = array(':set_paid_items', $paid_items, 'int');
$db->query($query, $params);
		
// we have to set the categories counters to 0 so it can be recounted
//correctly and the next code below will add the new counter 
$query = "UPDATE " . $DBPrefix . "categories set counter = 0, sub_counter = 0";
$params = array();
$db->query($query, $params);
			
$query = "SELECT COUNT(id) As COUNT, category, secondcat FROM " . $DBPrefix . "auctions
	WHERE closed = 0 AND starts <= :timer AND suspended = 0 GROUP BY category";
$params = array();
$params[] = array(':timer', $NOW, 'int');
$db->query($query, $params);
while ($row = $db->fetch())
{
	if ($row['COUNT'] * 1 > 0 && !empty($row['category'])) // avoid some errors
	{
		$query = "SELECT left_id, right_id, counter FROM " . $DBPrefix . "categories WHERE cat_id = :cat";
		$params = array();
		$params[] = array(':cat', $row['category'], 'int');
		$db->query($query, $params);
		
		$parent_node = $db->result();	
		$add_cat = $parent_node['counter'] + $row['COUNT'];
		$catscontrol = new MPTTcategories();	
		$main_crumbs = $catscontrol->get_bread_crumbs($parent_node['left_id'], $parent_node['right_id']);
		
		for ($i = 0; $i < count($main_crumbs); $i++)
		{
			$query = "UPDATE " . $DBPrefix . "categories SET sub_counter = sub_counter + :sub_counters WHERE cat_id = :cat_ids";
			$params = array();
			$params[] = array(':sub_counters', $add_cat, 'int');
			$params[] = array(':cat_ids', $main_crumbs[$i]['cat_id'], 'int');
			$db->query($query, $params);
		}
		
		$query = "UPDATE " . $DBPrefix . "categories SET counter = :count_cat WHERE cat_id = :cat_id";
		$params = array();
		$params[] = array(':count_cat', $add_cat, 'int');
		$params[] = array(':cat_id', $row['category'], 'int');
		$db->query($query, $params);
		
		//adding extra categories if the function is turned on
		if ($row['secondcat'] > 0 && !empty($row['secondcat']) && $system->SETTINGS['extra_cat'] == 'y') // avoid some errors
		{
			$query = "SELECT left_id, right_id, counter FROM " . $DBPrefix . "categories WHERE cat_id = :extra_cat";
			$params = array();
			$params[] = array(':extra_cat', $row['secondcat'], 'int');
			$db->query($query, $params);
			
			$extra_parent_node = $db->result();			
			$add_extra_cat = $extra_parent_node['counter'] + $row['COUNT'];	
			$extra_crumbs = $catscontrol->get_bread_crumbs($extra_parent_node['left_id'], $extra_parent_node['right_id']);
			
			for ($i = 0; $i < count($extra_crumbs); $i++)
			{
				$query = "UPDATE " . $DBPrefix . "categories SET sub_counter = sub_counter + :sub_counters WHERE cat_id = :extra_cat_id";
				$params = array();
				$params[] = array(':sub_counters', $add_extra_cat, 'int');
				$params[] = array(':extra_cat_id', $extra_crumbs[$i]['cat_id'], 'int');
				$db->query($query, $params);
			}
			
			$query = "UPDATE " . $DBPrefix . "categories SET counter = :count_cat WHERE cat_id = :extra_cat_id";
			$params = array();
			$params[] = array(':count_cat', $add_extra_cat, 'int');
			$params[] = array(':extra_cat_id', $row['secondcat'], 'int');
			$db->query($query, $params);
		}
	}
}
			
//delete old cron logs so we don't overload the db
$query = "SELECT id, timestamp FROM " . $DBPrefix . "logs WHERE type = :cron ORDER BY timestamp ASC";
$params = array();
$params[] = array(':cron', 'cron', 'str');
$db->query($query, $params);
$logs = 0;
foreach ($db->fetchall() as $row)
{
	if($db->numrows('id') > 35 || $NOW - 1200 < $db->numrows('timestamp'))
	{
		$query = "DELETE FROM " . $DBPrefix . "logs WHERE type = :cron AND id = :log_id";
		$params = array();
		$params[] = array(':cron', 'cron', 'str');
		$params[] = array(':log_id', $row['id'], 'int');
		$db->query($query, $params);
		$logs++;
	}
}

$corn_log .= '<br>';
$corn_log .= $MSG['3500_1015598'];
$corn_log .= '<strong>' . $logs . '</strong>' . $MSG['3500_1015600'];

// finish cron script and submit it to db
if($system->SETTINGS['cronlog'] == 'y')
{
	printLog($corn_log);
}