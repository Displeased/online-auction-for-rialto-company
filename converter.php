<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/include 'common.php';
include $include_path . 'converter.inc.php';

$CURRENCIES = CurrenciesList();
$conversion = '&nbsp;';

if (isset($_POST['action']) && $_POST['action'] == 'convert')
{
	// Convert
	$from = is_numeric($_POST['from']) ? $_POST['from'] : 0;
	$to = is_numeric($_POST['to']) ? $_POST['to'] : 0;
	$amount = is_numeric($_POST['amount']) ? $_POST['amount'] : 0;
	$conversion = ConvertCurrency($from, $to, $amount);
	// construct string
	$conversion = $system->print_money_nosymbol($amount) . ' ' . $CURRENCIES[$from] . ' = ' . $system->print_money_nosymbol($conversion, true) . ' |' . $CURRENCIES[$to];
}

foreach ($CURRENCIES as $k => $v)
{
	$fromselected = false;
	$toselected = false;
	if ($k == $system->SETTINGS['currency'])
	{
		$fromselected = true;
	}
	elseif (isset($_POST['from']) && $_POST['from'] == $k)
	{
		$fromselected = true;
	}
	if (isset($_POST['to']) && $_POST['to'] == $k)
	{
		$toselected = true;
	}
	$template->assign_block_vars('from', array(
			'VALUE' => $k,
			'NAME' => $v,
			'B_SELECTED' => $fromselected
			));
	$template->assign_block_vars('to', array(
			'VALUE' => $k,
			'NAME' => $v,
			'B_SELECTED' => $toselected
			));
}

$template->assign_vars(array(
		'SITENAME' => $system->SETTINGS['sitename'],
		'THEME' => $system->SETTINGS['theme'],
		'ERROR' => (!isset($errormsg)) ? '' : $errormsg,
		'CONVERSION' => $conversion,
		'AMOUNT' => (isset($_POST['amount'])) ? $_POST['amount'] : ((isset($_GET['AMOUNT'])) ? $_GET['AMOUNT'] : 1.00),

		'B_CONVERSION' => (isset($_POST['action']) && $_POST['action'] == 'convert')
		));

$template->set_filenames(array(
		'body' => 'converter.tpl'
		));
$template->display('body');
?>