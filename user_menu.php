<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
// Connect to sql server & inizialize configuration variables
include 'common.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	header('location: user_login.php');
	exit;
}

function get_reminders($secid)
{
	global $DBPrefix, $system, $db;
	$data = array();
	// get number of new messages
	$query = "SELECT COUNT(*) AS total FROM " . $DBPrefix . "messages
			WHERE isread = :zero AND sentto = :sec_id";
	$params = array();
	$params[] = array(':zero', 0, 'int');
	$params[] = array(':sec_id', $secid, 'int');
	$db->query($query, $params);
	$data[] = $db->result('total');
	// get number of pending feedback
	$query = "SELECT COUNT(DISTINCT a.auction) AS total FROM " . $DBPrefix . "winners a
			LEFT JOIN " . $DBPrefix . "auctions b ON (a.auction = b.id)
			WHERE (b.closed = :close OR b.bn_only = :bnonly) AND b.suspended = :suspend
			AND ((a.seller = :seller AND a.feedback_sel = :feedback_sel)
			OR (a.winner = :winner AND a.feedback_win = :feedback_win))";
	$params = array();
	$params[] = array(':close', 1, 'int');
	$params[] = array(':bnonly', 'y', 'str');
	$params[] = array(':suspend', 0, 'int');
	$params[] = array(':seller', $secid, 'int');
	$params[] = array(':feedback_sel', 0, 'int');
	$params[] = array(':winner', $secid, 'int');
	$params[] = array(':feedback_win', 0, 'int');
	$db->query($query, $params);
	$data[] = $db->result('total');
	// get auctions still requiring payment
	$query = "SELECT COUNT(DISTINCT id) AS total FROM " . $DBPrefix . "winners
			WHERE paid = :zero AND winner = :winner_id";
	$params = array();
	$params[] = array(':zero', 0, 'int');
	$params[] = array(':winner_id', $secid, 'int');
	$db->query($query, $params);
	$data[] = $db->result('total');
	// get auctions ending soon
	$query = "SELECT COUNT(DISTINCT b.auction) AS total FROM " . $DBPrefix . "bids b
			LEFT JOIN " . $DBPrefix . "auctions a ON (b.auction = a.id)
			WHERE b.bidder = :bidder AND a.ends <= :timer
			AND a.closed = :close GROUP BY b.auction";
	$params = array();
	$params[] = array(':bidder', $secid, 'int');
	$params[] = array(':timer', ($system->ctime + (3600 * 24)), 'int');
	$params[] = array(':close', 0, 'int');
	$db->query($query, $params);
	$data[] = ($db->numrows() > 0) ? $db->result('total') : 0;
	// get outbid auctions
	$query = "SELECT a.current_bid, a.id, a.title, a.ends, b.bid FROM " . $DBPrefix . "auctions a, " . $DBPrefix . "bids b
			WHERE a.id = b.auction AND a.closed = :zero AND b.bidder = :bidder
			AND a.bn_only = :only ORDER BY a.ends ASC, b.bidwhen DESC";
	$params = array();
	$params[] = array(':zero', 0, 'int');
	$params[] = array(':bidder', $secid, 'int');
	$params[] = array(':only', 'n', 'str');
	$db->query($query, $params);
	$idcheck = array();
	$auctions_count = 0;
	while ($row = $db->result())
	{
		if (!in_array($row['id'], $idcheck))
		{
			// Outbidded or winning bid
			if ($row['current_bid'] != $row['bid']) $auctions_count++;;
			$idcheck[] = $row['id'];
		}
	}
	$data[] = $auctions_count;
	// get auctions sold item
	$query = "SELECT COUNT(DISTINCT a.id) AS total FROM " . $DBPrefix . "winners a
		LEFT JOIN " . $DBPrefix . "auctions b ON (a.auction = b.id)
		WHERE b.closed = :one AND a.seller = :sellers AND a.is_read = :zero";
	$params = array();
	$params[] = array(':one', 1, 'int');
	$params[] = array(':sellers', $secid, 'int');
	$params[] = array(':zero', 0, 'int');
	$db->query($query, $params);
	$data[] =  $db->result('total');
	return $data;
}

// Send buyer's request to the administrator
if (isset($_POST['requesttoadmin']))
{
	$send_email->requesttoadmin($user->user_data['name'], $user->user_data['nick'], $user->user_data['email'], $user->user_data['id']);
	$_SESSION['TMP_MSG'] = $MSG['25_0142'];
}

$cptab = (isset($_GET['cptab'])) ? $_GET['cptab'] : '';

switch ($cptab)
{
	default:
	case 'summary':
		$_SESSION['cptab'] = 'summary';
		break;
	case 'account':
		$_SESSION['cptab'] = 'account';
		break;
	case 'selling':
		$_SESSION['cptab'] = 'selling';
		break;
	case 'buying':
		$_SESSION['cptab'] = 'buying';
		break;
	case 'avatarupload':
		$_SESSION['cptab'] = 'avatarupload';
		break;
}

switch ($_SESSION['cptab'])
{
	default:
	case 'summary':
		$reminders = get_reminders($user->user_data['id']);
		$template->assign_vars(array(
				'NEWMESSAGES' => ($reminders[0] > 0) ? '<tr><td>' . $reminders[0] . ' ' . $MSG['508'] . ' (<a href="' . $system->SETTINGS['siteurl'] . 'mail.php">' . $MSG['5295'] . '</a>)</td></tr>' : '',
				'FBTOLEAVE' => ($reminders[1] > 0) ? '<tr><td>' . $reminders[1] . $MSG['072'] . ' (<a href="' . $system->SETTINGS['siteurl'] . 'buysellnofeedback.php">' . $MSG['5295'] . '</a>)</td></tr>' : '',
				'TO_PAY' => ($reminders[2] > 0) ? '<tr><td>' . sprintf($MSG['792'], $reminders[2]) . ' (<a href="' . $system->SETTINGS['siteurl'] . 'outstanding.php">' . $MSG['5295'] . '</a>)</td></tr>' : '',
				'BENDING_SOON' => ($reminders[3] > 0) ? '<tr><td>' . $reminders[3] . $MSG['793'] . ' (<a href="' . $system->SETTINGS['siteurl'] . 'yourbids.php">' . $MSG['5295'] . '</a>)</td></tr>' : '',
				'BOUTBID' => ($reminders[4] > 0) ? '<tr><td>' . sprintf($MSG['794'], $reminders[4]) . ' (<a href="' . $system->SETTINGS['siteurl'] . 'yourbids.php">' . $MSG['5295'] . '</a>)</td></tr>' : '',
				'SOLD_ITEM8' => ($reminders[5] > 0) ? '<tr><td>' . $reminders[5] . '&nbsp;' . $MSG['3500_1015412'] . ' (<a href="' . $system->SETTINGS['siteurl'] . 'yourauctions_sold.php">' . $MSG['5295'] . '</a>)</td></tr>' : '',

				'NO_REMINDERS' => (($reminders[0] + $reminders[1] + $reminders[2] + $reminders[3] + $reminders[4] + $reminders[5]) == 0) ? $MSG['510'] : '',
				));
		break;
	case 'account':
		$reminders = get_reminders($user->user_data['id']);
		$template->assign_vars(array(
				'NEWMESSAGES' => ($reminders[0] > 0) ? '( ' . $reminders[0] . ' ' . $MSG['508'] . ' )' : '',
				'FBTOLEAVE' => ($reminders[1] > 0) ? '( ' . $reminders[1] . $MSG['072'] . ' )' : ''
				));
		break;
	case 'selling':
		break;
	case 'buying':
		break;
	case 'avatarupload':
		break;

}

$template->assign_vars(array(
		'B_CANSELL' => ($user->can_sell),

		'TMPMSG' => (isset($_SESSION['TMP_MSG'])) ? $_SESSION['TMP_MSG'] : '',
		'THISPAGE' => $_SESSION['cptab']
		));

include 'header.php';
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'user_menu.tpl'
		));
$template->display('body');
include 'footer.php';
unset($_SESSION['TMP_MSG']);
