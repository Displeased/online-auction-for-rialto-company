<?php
$current_page = 'banners';
include 'common.php';

include $main_path . 'language/' . $language . '/categories.inc.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'managebanners.php';
	header('location: user_login.php');
	exit;
}

unset($ERR);
//Add new user account to database
if (isset($_POST['action']) && $_POST['action'] == 'insert')
{
	if (empty($_POST['name']) || empty($_POST['company']) || empty($_POST['email']))
	{
		$ERR = $ERR_047;
	}
	elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['email']))
	{
		$ERR = $ERR_008;
	}
	else
	{		
		$query = "SELECT value FROM " . $DBPrefix . "fees WHERE type = :fee";
		$params = array();
		$params[] = array(':fee', 'banner_fee', 'str');
		$db->query($query, $params);
		if($db->result('value') < 0)
		{
			$paid = 'y';
		}
		else
		{
			$paid = 'n';
		}
		
		// Update database
		$query = "INSERT INTO " . $DBPrefix . "bannersusers VALUES (NULL, :user_name, :company, :email, :user_id, :newuser, :paid, :extra_banner, :time_stamp)";
		$params = array();
		$params[] = array(':user_name', $_POST['name'], 'str');
		$params[] = array(':company', $_POST['company'], 'str');
		$params[] = array(':email', $_POST['email'], 'str');
		$params[] = array(':user_id', $user->user_data['id'], 'int');
		$params[] = array(':newuser', 'y', 'str');
		$params[] = array(':paid', $paid, 'str');
		$params[] = array(':extra_banner', 'n', 'str');
		$params[] = array(':time_stamp', $system->ctime, 'int');
		$db->query($query, $params);
		$ID = $db->lastInsertId();
		
		if($paid == 'y')
		{
			header('location: newuserbanner.php?id=' . $ID);
		}
		elseif($paid == 'n')
		{
			header('location: pay.php?a=8');
		}
		exit;
	}
}

// Delete users and banners if the user click on the delete button
if (isset($_POST['delete']) && is_array($_POST['delete']))
{
	foreach ($_POST['delete'] as $k => $v)
	{	
		$query = "DELETE FROM " . $DBPrefix . "banners WHERE user = :user";
		$params = array();
		$params[] = array(':user', $v, 'int');
		$db->query($query, $params);

		$query = "DELETE FROM " . $DBPrefix . "bannersusers WHERE id = :user";
		$params = array();
		$params[] = array(':user', $v, 'int');
		$db->query($query, $params);
	}
}

// check user ids
$query = "SELECT id FROM " . $DBPrefix . "users WHERE id = :user_id";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$db->query($query, $params);
$user_id = $db->result('id');

// Retrieve users from the database
$query = "SELECT u.*, COUNT(b.user) as count FROM " . $DBPrefix . "bannersusers u
		LEFT JOIN " . $DBPrefix . "banners b ON (b.user = u.id)
		GROUP BY u.id ORDER BY u.name";
$db->direct_query($query);

$bg = '';

while ($row = $db->result())
{	
	//Checking user id and see if the user paid the fees
	if ($user->user_data['id'] == $row['seller'] && ($row['paid'] == '1') && ($row['seller'] == $user->user_data['id']))
	{		
			//Checking to see if the user has any banners uploaded
			if($row['count'] == 0)
			{
				//If the user has no banners uploaded the new user column will update so the
				//user can upload a banner for free as long as the user has an active account
				$query = "UPDATE " . $DBPrefix . "bannersusers SET newuser = :yes WHERE id = :id";
    			$params = array();
    			$params[] = array(':yes', 'y', 'str');
				$params[] = array(':id', $row['id'], 'int');
				$db->query($query, $params);
			}
			
				$template->assign_block_vars('busers', array(
					'ID' => $row['id'],
					'NAME' => $row['name'],
					'COMPANY' => $row['company'],
					'EMAIL' => $row['email'],
					'NUM_BANNERS' => $row['count'],
					'BG' => $bg,
					));
				$bg = ($bg == '') ? 'class="bg"' : '';
	}
}

// get fees
$query = "SELECT * FROM " . $DBPrefix . "fees";
$db->direct_query($query);
$i = 0;
while ($row = $db->result())
{
	if ($row['type'] == 'banner_fee')
	{
			$template->assign_vars(array(
				'B_SIGNUP_FEE' => ($row['value'] > 0),
				'SIGNUP_FEE' => $system->print_money($row['value'])
				));
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'NAME' => (isset($_POST['name'])) ? $_POST['name'] : '',
		'COMPANY' => (isset($_POST['company'])) ? $_POST['company'] : '',
		'EMAIL' => (isset($_POST['email'])) ? $_POST['email'] : ''
		));

include 'header.php';
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'managebanners.tpl'
		));
$template->display('body');
include 'footer.php';