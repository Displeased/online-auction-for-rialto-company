<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

$id = (isset($_GET['id'])) ? intval($_GET['id']) : 0;

if ($id > 0)
{
	$query = "SELECT n.title As t, n.content As c, n.new_date, t.* FROM " . $DBPrefix . "news n
			LEFT JOIN " . $DBPrefix . "news_translated t ON (t.id = n.id)
			WHERE n.id = :id AND t.lang = :lang AND n.suspended != 1";
	$params = array();
	$params[] = array(':id', $id, 'int');
	$params[] = array(':lang', $language, 'str');
	$db->query($query, $params);

	$new = $db->result();
	if (!empty($new['title']) && !empty($new['content']))
	{
		$title = stripslashes($new['title']);
		$content = stripslashes($new['content']);
	}
	else
	{
		$title = stripslashes($new['t']);
		$content = stripslashes($new['c']);
	}
	$template->assign_block_vars('news', array(
			'CONT' => $content
			));
}
else
{
	// Build news index
	$query = "SELECT n.title As t, n.new_date, t.* FROM " . $DBPrefix . "news n
			LEFT JOIN " . $DBPrefix . "news_translated t ON (t.id = n.id)
			WHERE t.lang = :lang AND n.suspended != 1 ORDER BY n.new_date DESC, n.id DESC";
	$params = array();
	$params[] = array(':lang', $language, 'str');
	$db->query($query, $params);

	while ($row = $db->result())
	{
		if (!empty($row['title']))
		{
			$title = stripslashes($row['title']);
		}
		else
		{
			$title = stripslashes($row['t']);
		}
		$template->assign_block_vars('list', array(
				'TITLE' => $title,
				'DATE' => FormatDate($row['new_date']),
				'ID' => $row['id']
				));
	}
}

$template->assign_vars(array(
		'TITLE' => ($id > 0) ? stripslashes($new['title']) . ' ' . FormatDate($new['new_date']) : $MSG['282']
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'viewnews.tpl'
		));
$template->display('body');
include 'footer.php';
