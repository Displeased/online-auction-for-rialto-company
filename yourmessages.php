<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'yourmessages.php';
	header('location: user_login.php');
	exit;
}
	$messageid = intval($_GET['id']);
	// check message is to user
	$query = "SELECT m.*, u.nick FROM " . $DBPrefix . "messages m
		LEFT JOIN " . $DBPrefix . "users u ON (u.id = m.sentfrom)
		WHERE m.sentto = :user_id AND m.id = :messa_id";
	$params = array();
	$params[] = array(':user_id', $user->user_data['id'], 'int');
	$params[] = array(':messa_id', $messageid, 'int');
	$db->query($query, $params);
	$check = $db->numrows();

	if ($check == 0)
	{
		$_SESSION['message'] = $ERR_070;
		header('location: mail.php');
	}
	
	$array = $db->result();
	$sent = ArrangeDateNoCorrection($array['sentat']);
	$subject = $array['subject'];
	$message = $array['message'];
	$hash = md5(rand(1, 9999));
	$array['message'] = str_replace('<br>', '', $array['message']);
	
	if ($array['sentfrom'] == 0 && !empty($array['fromemail']))
	{
		$sendusername = $array['fromemail'];
		$senderlink = $sendusername;
	}
	elseif ($array['sentfrom'] == 0 && empty($array['fromemail']))
	{
		$sendusername = $MSG['110'];
		$senderlink = $sendusername;
	}
	else
	{
		$sendusername = $array['nick'];
		$senderlink = '<a href="profile.php?user_id=1&auction_id=' . $array['sentfrom'] . '">' . $sendusername . '</a>';
	}
	
	// Update message
	$query = "UPDATE " . $DBPrefix . "messages SET isread = :read WHERE id = :messa_id";
	$params = array();
	$params[] = array(':read', 1, 'int');
	$params[] = array(':messa_id', $messageid, 'int');
	$db->query($query, $params);
	
	// set session for reply
	$_SESSION['subject' . $hash] = (substr($subject, 0, 3) == 'Re:') ? $subject : 'Re: ' . $subject;
	$_SESSION['sendto' . $hash] = $sendusername;
	$_SESSION['reply' . $hash] = $messageid;
	$_SESSION['reply_of' . $hash] = ($array['reply_of'] == 0) ? $messageid : $array['reply_of'];
	$_SESSION['question' . $hash] = $array['question'];
	
	$template->assign_vars(array(
			'SUBJECT' => $subject,
			'SENDERNAME' => (isset($senderlink)) ? $senderlink : $sendusername,
			'SENT' => $sent,
			'MESSAGE' => $message,
			'ID' => $messageid,
			'HASH' => $hash
			));

include 'header.php';
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'yourmessages.tpl'
		));
$template->display('body');
include 'footer.php';