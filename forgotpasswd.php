<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $include_path . 'countries.inc.php';

if (isset($_POST['action']) && $_POST['action'] == 'ok')
{
	if (isset($_POST['TPL_username']) && isset($_POST['TPL_email']))
	{
		$username = $system->cleanvars($_POST['TPL_username']);
		$email = $system->cleanvars($_POST['TPL_email']);
		$query = "SELECT email, id, name FROM " . $DBPrefix . "users WHERE nick = :username AND email = :user_email LIMIT 1";
		$params = array();
		$params[] = array(':username', $username, 'str');
		$params[] = array(':user_email', $email, 'str');
		$db->query($query, $params);

		if ($db->numrows() > 0)
		{
			// Generate a new random password and mail it to the user
			$data = $db->result();
			$email = $data['email'];
			$id = $data['id'];
			$name = $data['name'];
			function generatePassword($length=9, $strength=0) 
			{
    			$vowels = 'aeuy';
   			 	$consonants = 'bdghjmnpqrstvz';
    			if ($strength & 1) 
    			{
        			$consonants .= 'BDGHJLMNPQRSTVWXZ';
    			}
    			if ($strength & 2) {
        			$vowels .= "AEUY";
    			}
    			if ($strength & 4) 
    			{
        			$consonants .= '23456789';
    			}
    			if ($strength & 8) 
    			{
        			$consonants .= '@#$%';
    			}
    			$password = '';
    			$alt = $system->ctime % 2;
    			for ($i = 0; $i < $length; $i++) 
    			{
        			if ($alt == 1) 
        			{
            			$password .= $consonants[(rand() % strlen($consonants))];
            			$alt = 0;
        			} 
        			else 
        			{
           				$password .= $vowels[(rand() % strlen($vowels))];
            			$alt = 1;
        			}
    			}
    			return $password;
			}
						
			$random_pwd = generatePassword();
			$newpass = $phpass->HashPassword($random_pwd);

			// send message
			$send_email->forgot_password($name, $random_pwd, $id, $email);
			
			// Update database
			$query = "UPDATE " . $DBPrefix . "users SET password = :pass WHERE id = :user_id";
			$params = array();
			$params[] = array(':pass', $newpass, 'str');
			$params[] = array(':user_id', $id, 'int');
			$db->query($query, $params);
		}
		else
		{
			$ERR = $ERR_076;
		}
	}
	else
	{
		$ERR = $ERR_112;
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'USERNAME' => (isset($username)) ? $username : '',
		'EMAIL' => (isset($email)) ? $email : '',
		'B_FB_LINK' => 'IndexFBLogin',
		'B_FIRST' => (!isset($_POST['action']) || (isset($_POST['action']) && isset($ERR)))
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'forgotpasswd.tpl'
		));
$template->display('body');
include 'footer.php';