<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

switch ($_GET['show'])
{
	case 'aboutus':
		$TITLE = $MSG['5085'];
		$CONTENT = stripslashes($system->SETTINGS['aboutustext']);
		break;
	case 'terms':
		$TITLE = $MSG['5086'];
		$CONTENT = stripslashes($system->SETTINGS['termstext']);
		break;
	case 'priv':
		$TITLE = $MSG['401'];
		$CONTENT = stripslashes($system->SETTINGS['privacypolicytext']);
		break;
	case 'cookies':
        $TITLE = $MSG['30_0239'];
        $CONTENT = stripslashes($system->SETTINGS['cookiespolicytext']);
    	break;
}

$template->assign_vars(array(
		'TITLE' => $TITLE,
		'B_FB_LINK' => 'IndexFBLogin',
		'CONTENT' => $CONTENT
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'contents.tpl'
		));
$template->display('body');
include 'footer.php';
