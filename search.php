<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $main_path . 'language/' . $language . '/categories.inc.php';
include $include_path . 'dates.inc.php';

$NOW = $system->ctime;

$term = trim($_GET['q']);
$cat_id = intval($_GET['id']);

if (strlen($term) == 0)
{
	$template->assign_vars(array(
			'ERROR' => $ERR_037,
			'NUM_AUCTIONS' => 0,
			'TOP_HTML' => ''
			));
}
else
{
	$catSQL = '';
	if ($cat_id < 0)
	{
		$catscontrol = new MPTTcategories();
		$query = "SELECT right_id, left_id FROM " . $DBPrefix . "categories WHERE cat_id = :cat_id";
		$params = array();
		$params[] = array(':cat_id', $cat_id, 'int');
		$db->query($query, $params);
		$parent_node = $db->result();
		$children = $catscontrol->get_children_list($parent_node['left_id'], $parent_node['right_id']);
		$childarray = array($cat_id);
		foreach ($children as $k => $v)
		{
			$childarray[] = $v['cat_id'];
		}
		$catalist = '(';
		$catalist .= implode(',', $childarray);
		$catalist .= ')';
		$catSQL = " AND (category IN " . $catalist;
		if ($system->SETTINGS['extra_cat'] == 'y')
		{
			$catSQL .= " OR secondcat IN " . $catalist;
		}
		$catSQL .= ")";
	}
	
	$query = "SELECT * FROM " . $DBPrefix . "auctions WHERE
			(title LIKE :title OR id = :auc_id) " . $catSQL . " AND (closed = 1 OR closed = 0) AND suspended = 0 AND starts < :stime AND ends > :etime";
	$params = array();
	$params[] = array(':title', '%' . $system->cleanvars($term) . '%', 'str');
	$params[] = array(':auc_id', $term, 'int');
	$params[] = array(':stime', $NOW, 'int');
	$params[] = array(':etime', $NOW, 'int');
	$db->query($query, $params);

	// get total number of records
	$total = $db->numrows();

	// retrieve records corresponding to passed page number
	$PAGE = isset($_GET['PAGE']) ? intval($_GET['PAGE']) : 1;
	if ($PAGE == 0) $PAGE = 1;

	// determine limits for SQL query
	$left_limit = ($PAGE - 1) * $system->SETTINGS['perpage'];

	// get number of pages
	$PAGES = ceil($total / $system->SETTINGS['perpage']);

	$query_feat = $query . " AND featured = 'y' ORDER BY ends LIMIT :offset, 5";
	$params_feat = $params;
	$params_feat[] = array(':offset', (($PAGE - 1) * 5), 'int');

	$query = $query . " ORDER BY ends LIMIT :offset, :perpage";
	$params[] = array(':offset', $left_limit, 'int');
	$params[] = array(':perpage', $system->SETTINGS['perpage'], 'int');

	// to be sure about items format, I've unified the call
	include $include_path . 'browseitems.inc.php';
	browseItems($query, $params, $query_feat, $params_feat, $total, 'search.php', 'q=' . $term . '&id=' . $cat_id);
}

include 'header.php';
$template->set_filenames(array(
		'body' => 'search.tpl'
		));
$template->display('body');
include 'footer.php';