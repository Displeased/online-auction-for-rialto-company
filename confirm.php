<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

$id = (isset($_GET['id'])) ? $_GET['id'] : $id;
$id = (isset($_POST['id'])) ? $_POST['id'] : $id;
$hash = (isset($_GET['hash'])) ? $_GET['hash'] : $hash;
$hash = (isset($_POST['hash'])) ? $_POST['hash'] : $hash;

$query = "SELECT suspended, nick FROM " . $DBPrefix . "users WHERE id = :id";
$params = array();
$params[] = array(':id', intval($id), 'int');
$db->query($query, $params);
$users_data = $db->result();

if ($id && !isset($_POST['action']))
{
	if ($db->numrows() == 0)
	{
		$errmsg = $ERR_025;
	}
	elseif (!isset($hash) || md5($MD5_PREFIX . $system->uncleanvars($users_data['nick'])) != $hash)
	{
		$errmsg = $ERR_033;
	}
	elseif ($users_data['suspended'] == 0)
	{
		$errmsg = $ERR_039;
	}
	elseif ($users_data['suspended'] == 2)
	{
		$errmsg = $ERR_039;
	}

	if (isset($errmsg))
	{
		$page = 'error';
	}
	else
	{
		$page = 'confirm'; 
	}
}

if ($ids && !isset($_POST['action']))
{
	$errmsg = $ERR_025;
	$page = 'error';
}

if (isset($_POST['action']) && $_POST['action'] == $MSG['249'])
{	
	if (md5($MD5_PREFIX . $users_data['nick']) == $hash)
	{
		// User wants to confirm his/her registration
		$query = "UPDATE " . $DBPrefix . "users SET suspended = 0 WHERE id = :id AND suspended = 8";
		$params = array();
		$params[] = array(':id', $id, 'int');
		$db->query($query, $params);

		$query = "UPDATE " . $DBPrefix . "counters SET users = users + :one, inactiveusers = inactiveusers - :one";
		$params = array();
		$params[] = array(':one', 1, 'int');
		$db->query($query, $params);

		// login user
		$query = "SELECT id, hash, password FROM " . $DBPrefix . "users WHERE id = :id";
		$params = array();
		$params[] = array(':id', $id, 'int');
		$db->query($query, $params);
		if ($db->numrows() > 0)
		{	
			$page = 'confirmed';
			$login_user = $db->result();
			//generator user token
			$_SESSION['csrftoken'] = $security->encrypt($login_user['id'] . '-' . $login_user['hash'] . '-' . $system->ctime);

			$_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN'] 		= $security->encrypt($login_user['id']);
			$_SESSION[$system->SETTINGS['sessions_name'] .'_LOGGED_NUMBER'] 	= $security->encrypt(strspn($login_user['password'], $login_user['hash']));
			$_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_PASS'] 		= $security->encrypt($login_user['password']);
			
			// Update "last login" fields in users table
			$query = "UPDATE " . $DBPrefix . "users SET lastlogin = :lastlogin WHERE id = :user_id";
			$params = array();
			$params[] = array(':lastlogin', gmdate("Y-m-d H:i:s"), 'int');
			$params[] = array(':user_id', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']), 'int');
			$db->query($query, $params);

			$query = "SELECT id FROM " . $DBPrefix . "usersips WHERE USER = :user_id AND ip = :ip";
			$params = array();
			$params[] = array(':user_id', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']), 'int');
			$params[] = array(':ip', $_SERVER['REMOTE_ADDR'], 'int');
			$db->query($query, $params);
			if ($db->numrows('id') == 0)
			{
				$query = "INSERT INTO " . $DBPrefix . "usersips VALUES
						(NULL, :user_id, :ip, :after, :accept)";
				$params = array();
				$params[] = array(':user_id', $security->decrypt($_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN']), 'int');
				$params[] = array(':ip', $_SERVER['REMOTE_ADDR'], 'int');
				$params[] = array(':after', 'after', 'str');
				$params[] = array(':accept', 'accept', 'str');
				$db->query($query, $params);
			}
		}
	}
	else
	{
		$errmsg = $ERR_033;
		$page = 'error';
	}
}

if (isset($_POST['action']) && $_POST['action'] == $MSG['250'])
{
	if (md5($MD5_PREFIX . $users_data['nick']) == $hash)
	{
		// User doesn't want to confirm hid/her registration
		$query = "DELETE FROM " . $DBPrefix . "users WHERE id = :id AND suspended = :suspended";
		$params = array();
		$params[] = array(':id', intval($id), 'int');
		$params[] = array(':suspended', 8, 'int');
		$db->query($query, $params);

		$query = "UPDATE " . $DBPrefix . "counters SET inactiveusers = inactiveusers - :inactive";
		$params = array();
		$params[] = array(':inactive', 1, 'int');
		$db->query($query, $params);
		$page = 'refused';
	}
	else
	{
		$errmsg = $ERR_033;
		$page = 'error';
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($errmsg)) ? $errmsg : '',
		'USERID' => $id,
		'HASH' => $hash,
		'PAGE' => $page
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'confirm.tpl'
		));
$template->display('body');
include 'footer.php';