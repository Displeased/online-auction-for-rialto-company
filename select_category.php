<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/
include 'common.php';
include $main_path . 'language/' . $language . '/categories.inc.php';

// Is the seller logged in?
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'select_category.php';
	header('location: user_login.php');
	exit;
}

if ($user_data['suspended'] == 1)
{
	$ERR = $ERR_618;
}
elseif ($user_data['suspended'] == 8)
{
	$ERR = $ERR_620;
}
elseif ($user_data['suspended'] == 10)
{
	$ERR = $ERR_621;
}
if (in_array($user->user_data['suspended'], array(5, 6, 7)))
{
	$_SESSION['msg_title'] = $MSG['753']; 
	$_SESSION['msg_body'] = $ERR;
	header('location: message.php');
	exit;
}

if (!$user->can_sell)
{
	header('location: user_menu.php?cptab=selling');
	exit;
}

// Process category selection
$box = (isset($_POST['box'])) ? $_POST['box'] + 1 : 0;
$catscontrol = new MPTTcategories();
$cat_no = (isset($_REQUEST['cat_no'])) ? $_REQUEST['cat_no'] : 1;
$i = 0;
while (true)
{
	if (!isset($_POST['cat' . $i]))
	{
		break;
	}
	$POST['cat' . $i] = $_POST['cat' . $i];
	$i++;
}

if (isset($_POST['action']) && $_POST['action'] == 'process' && $_POST['box'] == '')
{
    $_SESSION['action'] = 1;
	$VARNAME = 'cat' . (count($POST) - 1);
	$_SESSION['SELL_sellcat' . $cat_no] = $POST[$VARNAME];
	$query = "SELECT left_id, right_id FROM " . $DBPrefix . "categories WHERE cat_id = :sellcat";
	$params = array();
	$params[] = array(':sellcat', intval($_POST[$VARNAME]), 'int');
	$db->query($query, $params);
	$lft_rgt = $db->result();
	if ($lft_rgt['left_id'] + 1 == $lft_rgt['right_id'])
	{
		if ($system->SETTINGS['extra_cat'] == 'n' || ($cat_no == 2 && $system->SETTINGS['extra_cat'] == 'y'))
		{
			header('location: sell.php');
			exit;
		}
		else
		{
			header('location: select_category.php?cat_no=2');
			exit;
		}
	}
	else
	{
		$ERR = $ERR_25_0001;
	}
}

if($system->SETTINGS['auction_setup_types'] == 0)
{
	$sellType = 'free';
}
elseif($system->SETTINGS['auction_setup_types'] == 1)
{
	$sellType = 'sell';
}
elseif($system->SETTINGS['auction_setup_types'] == 2)
{
	$sellType = 'sell';
}

// Process change mode
if (isset($_GET['change']) && $_GET['change'] == 'yes')
{
	$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :sellcat_id";
	$params = array();
	$params[] = array(':sellcat_id', intval($_SESSION['SELL_sellcat' . $cat_no]), 'int');
	$db->query($query, $params);
	$cat = $db->result();
    $crumbs = $catscontrol->get_bread_crumbs($cat['left_id'], $cat['right_id']);
	$count = count($crumbs);
	$box = $count - 1;
	for ($i = 1; $i < $count; $i++)
	{
		$POST['cat' . ($i - 1)] = $crumbs[$i]['cat_id'];
	}
}
elseif (count($_POST) == 0 && !isset($_GET['cat_no']))
{
    unset($_SESSION['UPLOADED_PICTURES_SIZE']);
	$_SESSION['SELL_starts'] = '';
	$_SESSION['UPLOADED_PICTURES'] = array();
    $_SESSION['SELL_with_reserve'] = '';
    $_SESSION['SELL_reserve_price'] = '';
    $_SESSION['SELL_minimum_bid'] = '';
    $_SESSION['SELL_file_uploaded'] = '';
    $_SESSION['SELL_title'] = '';
    $_SESSION['SELL_subtitle'] = '';
    $_SESSION['SELL_description'] = '';
    $_SESSION['SELL_pict_url'] = '';
	$_SESSION['SELL_pict_url_temp'] = '';
    $_SESSION['SELL_atype'] = '';
    $_SESSION['SELL_iquantity'] = '';
    $_SESSION['SELL_with_buy_now'] = '';
    $_SESSION['SELL_buy_now_price'] = '';
    $_SESSION['SELL_duration'] = '';
    $_SESSION['SELL_relist'] = '';
    $_SESSION['SELL_increments'] = '';
    $_SESSION['SELL_customincrement'] = 0;
    $_SESSION['SELL_shipping'] = 1;
    $_SESSION['SELL_shipping_terms'] = '';
    $_SESSION['SELL_payment'] = '';
    $_SESSION['SELL_international'] = '';
    $_SESSION['SELL_buy_now_only'] = '';
    $_SESSION['SELL_action'] = '';
    $_SESSION['SELL_shipping_cost'] = 0;
	$_SESSION['SELL_is_bold'] = 'n';
	$_SESSION['SELL_is_highlighted'] = 'n';
	$_SESSION['SELL_is_featured'] = 'n';
	$_SESSION['SELL_is_taxed'] = 'n';
	$_SESSION['SELL_upload_file'] = '';
	$_SESSION['SELL_tax_included'] = 'y';
	$_SESSION['SELL_start_now'] = '1';
	$_SESSION['SELL_item_condition'] = '';
	$_SESSION['SELL_item_manufacturer'] = '';
	$_SESSION['SELL_item_model'] = '';
	$_SESSION['SELL_item_colour'] = '';
	$_SESSION['SELL_item_year'] = '';
	$_SESSION['SELL_digital_item'] = '';
	$_SESSION['SELL_returns'] = '';
	$_SESSION['SELL_upload_file'] = '';
	$_SESSION['SELL_sell_type'] = $sellType;
}

// Build the categories arrays
$boxarray = array();
$SHOWBUTTON = false;
$pc = 0;
for ($i = 0; $i <= $box; $i++)
{
	$parent = (isset($POST['cat' . ($i - 1)])) ? $POST['cat' . ($i - 1)] : 0;
	$safe_box = true;
	$cat_params = array();
	if ($parent == 0)
	{
		$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE parent_id = -:parent_id";
		$cat_params[] = array(':parent_id', 1, 'int');
		if ($pc != 0)
		{
			$safe_box = false;
		}
		$pc++;
	}
	else
	{
		$query = "SELECT left_id, right_id, level FROM " . $DBPrefix . "categories WHERE cat_id = :parent";
		$cat_params[] = array(':parent', intval($parent), 'int');
	}
	if ($safe_box)
	{
		$db->query($query, $cat_params);
		$cat = $db->result();
		$temparray = $catscontrol->get_children($cat['left_id'], $cat['right_id'], $cat['level']);
		if (count($temparray) > 0)
		{
			for ($j = 0; $j < count($temparray); $j++)
			{
				$boxarray[$i][$temparray[$j]['cat_id']] = $temparray[$j]['cat_name'];
				$boxarray[$i][$temparray[$j]['cat_id']] .= ($temparray[$j]['left_id'] + 1 != $temparray[$j]['right_id']) ? ' ->' : '';
			}
		}
		else
		{
			$SHOWBUTTON = true;
		}
	}
}

$boxes = count($boxarray);
for ($i = 0; $i < $boxes; $i++)
{
	$template->assign_block_vars('boxes', array(
			'B_NOWLINE' => (($i % 2 == 0) && ($i > 0)),
			'I' => $i,
			'PERCENT' => ($boxes == 1) ? 100 : ($boxes == 2) ? 50 : 33
			));
	foreach ($boxarray[$i] as $k => $v)
	{
		$template->assign_block_vars('boxes.cats', array(
				'K' => $k,
				'CATNAME' => $category_names[$k],
				'SELECTED' => (isset($POST['cat' . $i]) && $POST['cat' . $i] == $k) ? ' selected' : ''
				));
	}
}

$extra_cat = 0;
if($cat_no == 2 && !$user->no_fees && !$user->no_excat_fee)
{
	$query = "SELECT value FROM " . $DBPrefix . "fees WHERE type = 'excat_fee'";
	$db->direct_query($query);
	$extra_cat = $db->result('value');
}


$template->assign_vars(array(
        'B_SHOWBUTTON' => $SHOWBUTTON,
		'CAT_NO' => $cat_no,
		'COST' => ($extra_cat > 0) ? $system->print_money($extra_cat) : '',
        'ERROR' => (isset($ERR)) ? $ERR : ''
        ));

include 'header.php';
$template->set_filenames(array(
        'body' => 'select_category.tpl'
        ));
$template->display('body');
include 'footer.php';