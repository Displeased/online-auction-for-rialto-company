<?php
/*******************************************************************************
 *   copyright				: (C) 2011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/
 
 include 'common.php';


//if (!$user->is_logged_in())
//{
	//if your not logged in you shouldn't be here
//	header("location: user_login.php");
//	exit;
//}



$query = "SELECT * FROM " . $DBPrefix . "conditions ORDER BY :order";
$params = array();
$params[] = array(':order', 'item_condition', 'str');
$db->query($query, $params);

$i = 0;
while ($row = $db->result())
{
	$template->assign_block_vars('conditions', array(
			'ID' => $i,
			'ITEM_CONDITION' => $row['item_condition'],
			'CONDITION_DESC' => $row['condition_desc']
			));
	$i++;
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'THEME' => $system->SETTINGS['theme']
		));

$template->set_filenames(array(
		'body' => 'meanings.tpl'
		));
$template->display('body');