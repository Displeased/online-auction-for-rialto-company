-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 17 2016 г., 19:07
-- Версия сервера: 5.5.45
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `tubauction`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_accesseshistoric`
--

CREATE TABLE IF NOT EXISTS `tubauction_accesseshistoric` (
  `month` char(2) NOT NULL DEFAULT '',
  `year` char(4) NOT NULL DEFAULT '',
  `pageviews` int(11) NOT NULL DEFAULT '0',
  `uniquevisitiors` int(11) NOT NULL DEFAULT '0',
  `usersessions` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_accounts`
--

CREATE TABLE IF NOT EXISTS `tubauction_accounts` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `nick` varchar(20) NOT NULL,
  `name` tinytext NOT NULL,
  `text` text NOT NULL,
  `type` varchar(15) NOT NULL,
  `paid_date` varchar(16) NOT NULL,
  `amount` double(6,2) NOT NULL,
  `day` int(3) NOT NULL,
  `week` int(2) NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_adminusers`
--

CREATE TABLE IF NOT EXISTS `tubauction_adminusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `hash` varchar(5) NOT NULL DEFAULT '',
  `created` varchar(8) NOT NULL DEFAULT '',
  `lastlogin` varchar(14) NOT NULL DEFAULT '',
  `status` int(2) NOT NULL DEFAULT '0',
  `notes` text,
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tubauction_adminusers`
--

INSERT INTO `tubauction_adminusers` (`id`, `username`, `password`, `hash`, `created`, `lastlogin`, `status`, `notes`) VALUES
(1, 'tubauction', '$2a$08$2LyTRo0ERduHTHIIjpoRYuUm5slUdJ5GIJOBGYnIUEinkS9k1Vjpu', 'seo8a', '20160215', '1455539609', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_adsense`
--

CREATE TABLE IF NOT EXISTS `tubauction_adsense` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `header_banner_1` longtext NOT NULL,
  `index_banner_1` longtext NOT NULL,
  `index_banner_2` longtext NOT NULL,
  `index_banner_3` longtext NOT NULL,
  `browse_banner_1` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tubauction_adsense`
--

INSERT INTO `tubauction_adsense` (`id`, `header_banner_1`, `index_banner_1`, `index_banner_2`, `index_banner_3`, `browse_banner_1`) VALUES
(1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_auccounter`
--

CREATE TABLE IF NOT EXISTS `tubauction_auccounter` (
  `auction_id` int(11) NOT NULL DEFAULT '0',
  `counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`auction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_auccounter`
--

INSERT INTO `tubauction_auccounter` (`auction_id`, `counter`) VALUES
(2, 2),
(3, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_auctions`
--

CREATE TABLE IF NOT EXISTS `tubauction_auctions` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `user` int(32) DEFAULT NULL,
  `title` varchar(70) DEFAULT NULL,
  `subtitle` varchar(70) DEFAULT NULL,
  `starts` varchar(14) DEFAULT NULL,
  `description` text,
  `pict_url` tinytext,
  `category` int(11) DEFAULT NULL,
  `secondcat` int(11) DEFAULT NULL,
  `minimum_bid` double(16,2) DEFAULT '0.00',
  `shipping_cost` double(16,2) DEFAULT '0.00',
  `shipping_cost_additional` double(16,2) DEFAULT '0.00',
  `reserve_price` double(16,2) DEFAULT '0.00',
  `buy_now` double(16,2) DEFAULT '0.00',
  `auction_type` char(1) DEFAULT NULL,
  `duration` varchar(7) DEFAULT NULL,
  `increment` double(8,2) NOT NULL DEFAULT '0.00',
  `shipping` char(1) DEFAULT NULL,
  `payment` tinytext,
  `international` char(1) DEFAULT NULL,
  `ends` varchar(14) DEFAULT NULL,
  `current_bid` double(16,2) DEFAULT '0.00',
  `closed` int(1) DEFAULT '0',
  `photo_uploaded` tinyint(1) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `suspended` int(1) DEFAULT '0',
  `relist` int(11) NOT NULL DEFAULT '0',
  `relisted` int(11) NOT NULL DEFAULT '0',
  `num_bids` int(11) NOT NULL DEFAULT '0',
  `sold` enum('y','n','s') NOT NULL DEFAULT 'n',
  `shipping_terms` tinytext NOT NULL,
  `bn_only` enum('y','n') NOT NULL DEFAULT 'n',
  `bold` enum('y','n') NOT NULL DEFAULT 'n',
  `highlighted` enum('y','n') NOT NULL DEFAULT 'n',
  `featured` enum('y','n') NOT NULL DEFAULT 'n',
  `current_fee` double(16,2) DEFAULT '0.00',
  `tax` enum('y','n') NOT NULL DEFAULT 'n',
  `taxinc` enum('y','n') NOT NULL DEFAULT 'y',
  `item_condition` varchar(40) DEFAULT '',
  `item_manufacturer` varchar(32) DEFAULT '',
  `item_model` varchar(32) DEFAULT '',
  `item_colour` varchar(32) DEFAULT '',
  `item_year` varchar(11) DEFAULT '',
  `returns` char(1) DEFAULT NULL,
  `sell_type` enum('free','sell') NOT NULL DEFAULT 'sell',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tubauction_auctions`
--

INSERT INTO `tubauction_auctions` (`id`, `user`, `title`, `subtitle`, `starts`, `description`, `pict_url`, `category`, `secondcat`, `minimum_bid`, `shipping_cost`, `shipping_cost_additional`, `reserve_price`, `buy_now`, `auction_type`, `duration`, `increment`, `shipping`, `payment`, `international`, `ends`, `current_bid`, `closed`, `photo_uploaded`, `quantity`, `suspended`, `relist`, `relisted`, `num_bids`, `sold`, `shipping_terms`, `bn_only`, `bold`, `highlighted`, `featured`, `current_fee`, `tax`, `taxinc`, `item_condition`, `item_manufacturer`, `item_model`, `item_colour`, `item_year`, `returns`, `sell_type`) VALUES
(2, 1, 'книга1', 'книга', '1455572861', '<p>\r\n	ввыыв</p>\r\n<p>\r\n	ввв</p>\r\n<p>\r\n	в</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	в</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	в</p>\r\n<p>\r\n	в</p>\r\n', 'f47454d1d3644127f42070181a8b9afc.jpeg', 31, 0, 0.99, 300.00, 0.00, 0.00, 0.00, '1', '1', 0.00, '1', '0, 1', '0', '1455659261', 0.00, 1, 1, 1, 0, 0, 0, 0, 'y', 'ццц', 'n', 'n', 'n', 'n', 0.00, 'n', 'y', 'Used', 'издателство 1', '22', '444', '1998', '0', 'free'),
(3, 1, 'audiobook', 'audiobookk', '1455704362', '<p>\r\n	gyguuh</p>\r\n<p>\r\n	guhughiog</p>\r\n<p>\r\n	gygu7u</p>\r\n', '5323f0c2e6dffa0d6038e620929eb015.jpeg', 32, 0, 0.99, 0.00, 0.00, 0.00, 0.00, '1', '1', 0.00, '1', '0, 1', '0', '1455790762', 0.00, 1, 1, 1, 0, 0, 0, 0, 'y', '', 'n', 'n', 'n', 'n', 0.00, 'n', 'y', 'As New Never Used', 'audiobook1', 'audiobook', 'red', '1972', '0', 'free');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_banners`
--

CREATE TABLE IF NOT EXISTS `tubauction_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` enum('gif','jpg','png','swf') DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `sponsortext` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `purchased` int(11) NOT NULL DEFAULT '0',
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `seller` int(11) NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_bannerscategories`
--

CREATE TABLE IF NOT EXISTS `tubauction_bannerscategories` (
  `banner` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_bannerskeywords`
--

CREATE TABLE IF NOT EXISTS `tubauction_bannerskeywords` (
  `banner` int(11) NOT NULL DEFAULT '0',
  `keyword` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_bannersstats`
--

CREATE TABLE IF NOT EXISTS `tubauction_bannersstats` (
  `banner` int(11) DEFAULT NULL,
  `purchased` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  KEY `id` (`banner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_bannersusers`
--

CREATE TABLE IF NOT EXISTS `tubauction_bannersusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `seller` int(11) DEFAULT NULL,
  `newuser` enum('y','n') DEFAULT NULL,
  `paid` int(2) DEFAULT NULL,
  `ex_banner_paid` enum('y','n') DEFAULT NULL,
  `time_stamp` int(20) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_bids`
--

CREATE TABLE IF NOT EXISTS `tubauction_bids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auction` int(32) DEFAULT NULL,
  `bidder` int(32) DEFAULT NULL,
  `bid` double(16,2) DEFAULT NULL,
  `bidwhen` varchar(14) DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_categories`
--

CREATE TABLE IF NOT EXISTS `tubauction_categories` (
  `cat_id` int(4) NOT NULL AUTO_INCREMENT,
  `parent_id` int(4) DEFAULT NULL,
  `left_id` int(8) NOT NULL,
  `right_id` int(8) NOT NULL,
  `level` int(1) NOT NULL,
  `cat_name` tinytext,
  `sub_counter` int(11) DEFAULT '0',
  `counter` int(11) DEFAULT '0',
  `cat_colour` varchar(15) DEFAULT '',
  `cat_image` varchar(150) DEFAULT '',
  PRIMARY KEY (`cat_id`),
  KEY `left_id` (`left_id`,`right_id`,`level`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=198 ;

--
-- Дамп данных таблицы `tubauction_categories`
--

INSERT INTO `tubauction_categories` (`cat_id`, `parent_id`, `left_id`, `right_id`, `level`, `cat_name`, `sub_counter`, `counter`, `cat_colour`, `cat_image`) VALUES
(1, -1, 1, 394, -1, 'All', 0, 0, '', ''),
(2, 1, 340, 393, 0, 'Art &amp; Antiques', 0, 0, '', ''),
(3, 2, 391, 392, 1, 'Textiles &amp; Linens', 0, 0, '', ''),
(4, 2, 389, 390, 1, 'Amateur Art', 0, 0, '', ''),
(5, 2, 387, 388, 1, 'Ancient World', 0, 0, '', ''),
(6, 2, 385, 386, 1, 'Books &amp; Manuscripts', 0, 0, '', ''),
(7, 2, 383, 384, 1, 'Cameras', 0, 0, '', ''),
(8, 2, 363, 382, 1, 'Ceramics &amp; Glass', 0, 0, '', ''),
(9, 8, 364, 381, 2, 'Glass', 0, 0, '', ''),
(10, 9, 379, 380, 3, '40s, 50s &amp; 60s', 0, 0, '', ''),
(11, 9, 377, 378, 3, 'Art Glass', 0, 0, '', ''),
(12, 9, 375, 376, 3, 'Carnival', 0, 0, '', ''),
(13, 9, 373, 374, 3, 'Chalkware', 0, 0, '', ''),
(14, 9, 371, 372, 3, 'Chintz &amp; Shelley', 0, 0, '', ''),
(15, 9, 369, 370, 3, 'Contemporary Glass', 0, 0, '', ''),
(16, 9, 367, 368, 3, 'Decorative', 0, 0, '', ''),
(17, 9, 365, 366, 3, 'Porcelain', 0, 0, '', ''),
(18, 2, 361, 362, 1, 'Fine Art', 0, 0, '', ''),
(19, 2, 359, 360, 1, 'General', 0, 0, '', ''),
(20, 2, 357, 358, 1, 'Musical Instruments', 0, 0, '', ''),
(21, 2, 355, 356, 1, 'Orientalia', 0, 0, '', ''),
(22, 2, 353, 354, 1, 'Painting', 0, 0, '', ''),
(23, 2, 351, 352, 1, 'Photographic Images', 0, 0, '', ''),
(24, 2, 349, 350, 1, 'Post-1900', 0, 0, '', ''),
(25, 2, 347, 348, 1, 'Pre-1900', 0, 0, '', ''),
(26, 2, 345, 346, 1, 'Prints', 0, 0, '', ''),
(27, 2, 343, 344, 1, 'Scientific Instruments', 0, 0, '', ''),
(28, 2, 341, 342, 1, 'Silver &amp; Silver Plate', 0, 0, '', ''),
(29, 1, 262, 339, 0, 'Books', 0, 0, '', ''),
(30, 29, 337, 338, 1, 'Animals', 0, 0, '', ''),
(31, 29, 335, 336, 1, 'Arts, Architecture &amp; Photography', 0, 0, '', ''),
(32, 29, 333, 334, 1, 'Audiobooks', 0, 0, '', ''),
(33, 29, 331, 332, 1, 'Biographies &amp; Memoirs', 0, 0, '', ''),
(34, 29, 329, 330, 1, 'Business &amp; Investing', 0, 0, '', ''),
(35, 29, 327, 328, 1, 'Catalogs', 0, 0, '', ''),
(36, 29, 325, 326, 1, 'Children', 0, 0, '', ''),
(37, 29, 323, 324, 1, 'Computers &amp; Internet', 0, 0, '', ''),
(38, 29, 321, 322, 1, 'Contemporary', 0, 0, '', ''),
(39, 29, 319, 320, 1, 'Cooking, Food &amp; Wine', 0, 0, '', ''),
(40, 29, 317, 318, 1, 'Entertainment', 0, 0, '', ''),
(41, 29, 315, 316, 1, 'Foreign Language Instruction', 0, 0, '', ''),
(42, 29, 313, 314, 1, 'General', 0, 0, '', ''),
(43, 29, 311, 312, 1, 'Health, Mind &amp; Body', 0, 0, '', ''),
(44, 29, 309, 310, 1, 'Historical', 0, 0, '', ''),
(45, 29, 307, 308, 1, 'History', 0, 0, '', ''),
(46, 29, 305, 306, 1, 'Home &amp; Garden', 0, 0, '', ''),
(47, 29, 303, 304, 1, 'Horror', 0, 0, '', ''),
(48, 29, 301, 302, 1, 'Illustrated', 0, 0, '', ''),
(49, 29, 299, 300, 1, 'Literature &amp; Fiction', 0, 0, '', ''),
(50, 29, 297, 298, 1, 'Men', 0, 0, '', ''),
(51, 29, 295, 296, 1, 'Mystery &amp; Thrillers', 0, 0, '', ''),
(52, 29, 293, 294, 1, 'News', 0, 0, '', ''),
(53, 29, 291, 292, 1, 'Nonfiction', 0, 0, '', ''),
(54, 29, 289, 290, 1, 'Parenting &amp; Families', 0, 0, '', ''),
(55, 29, 287, 288, 1, 'Poetry', 0, 0, '', ''),
(56, 29, 285, 286, 1, 'Rare', 0, 0, '', ''),
(57, 29, 283, 284, 1, 'Reference', 0, 0, '', ''),
(58, 29, 281, 282, 1, 'Regency', 0, 0, '', ''),
(59, 29, 279, 280, 1, 'Religion &amp; Spirituality', 0, 0, '', ''),
(60, 29, 277, 278, 1, 'Science &amp; Nature', 0, 0, '', ''),
(61, 29, 275, 276, 1, 'Science Fiction &amp; Fantasy', 0, 0, '', ''),
(62, 29, 273, 274, 1, 'Sports', 0, 0, '', ''),
(63, 29, 271, 272, 1, 'Sports &amp; Outdoors', 0, 0, '', ''),
(64, 29, 269, 270, 1, 'Teens', 0, 0, '', ''),
(65, 29, 267, 268, 1, 'Textbooks', 0, 0, '', ''),
(66, 29, 265, 266, 1, 'Travel', 0, 0, '', ''),
(67, 29, 263, 264, 1, 'Women', 0, 0, '', ''),
(68, 1, 254, 261, 0, 'Clothing &amp; Accessories', 0, 0, '', ''),
(69, 68, 259, 260, 1, 'Accessories', 0, 0, '', ''),
(70, 68, 257, 258, 1, 'Clothing', 0, 0, '', ''),
(71, 68, 255, 256, 1, 'Watches', 0, 0, '', ''),
(72, 1, 248, 253, 0, 'Coins &amp; Stamps', 0, 0, '', ''),
(73, 72, 251, 252, 1, 'Coins', 0, 0, '', ''),
(74, 72, 249, 250, 1, 'Philately', 0, 0, '', ''),
(75, 1, 172, 247, 0, 'Collectibles', 0, 0, '', ''),
(76, 75, 245, 246, 1, 'Advertising', 0, 0, '', ''),
(77, 75, 243, 244, 1, 'Animals', 0, 0, '', ''),
(78, 75, 241, 242, 1, 'Animation', 0, 0, '', ''),
(79, 75, 239, 240, 1, 'Antique Reproductions', 0, 0, '', ''),
(80, 75, 237, 238, 1, 'Autographs', 0, 0, '', ''),
(81, 75, 235, 236, 1, 'Barber Shop', 0, 0, '', ''),
(82, 75, 233, 234, 1, 'Bears', 0, 0, '', ''),
(83, 75, 231, 232, 1, 'Bells', 0, 0, '', ''),
(84, 75, 229, 230, 1, 'Bottles &amp; Cans', 0, 0, '', ''),
(85, 75, 227, 228, 1, 'Breweriana', 0, 0, '', ''),
(86, 75, 225, 226, 1, 'Cars &amp; Motorcycles', 0, 0, '', ''),
(87, 75, 223, 224, 1, 'Cereal Boxes &amp; Premiums', 0, 0, '', ''),
(88, 75, 221, 222, 1, 'Character', 0, 0, '', ''),
(89, 75, 219, 220, 1, 'Circus &amp; Carnival', 0, 0, '', ''),
(90, 75, 217, 218, 1, 'Collector Plates', 0, 0, '', ''),
(91, 75, 215, 216, 1, 'Dolls', 0, 0, '', ''),
(92, 75, 213, 214, 1, 'General', 0, 0, '', ''),
(93, 75, 211, 212, 1, 'Historical &amp; Cultural', 0, 0, '', ''),
(94, 75, 209, 210, 1, 'Holiday &amp; Seasonal', 0, 0, '', ''),
(95, 75, 207, 208, 1, 'Household Items', 0, 0, '', ''),
(96, 75, 205, 206, 1, 'Kitsch', 0, 0, '', ''),
(97, 75, 203, 204, 1, 'Knives &amp; Swords', 0, 0, '', ''),
(98, 75, 201, 202, 1, 'Lunchboxes', 0, 0, '', ''),
(99, 75, 199, 200, 1, 'Magic &amp; Novelty Items', 0, 0, '', ''),
(100, 75, 197, 198, 1, 'Memorabilia', 0, 0, '', ''),
(101, 75, 195, 196, 1, 'Militaria', 0, 0, '', ''),
(102, 75, 193, 194, 1, 'Music Boxes', 0, 0, '', ''),
(103, 75, 191, 192, 1, 'Oddities', 0, 0, '', ''),
(104, 75, 189, 190, 1, 'Paper', 0, 0, '', ''),
(105, 75, 187, 188, 1, 'Pinbacks', 0, 0, '', ''),
(106, 75, 185, 186, 1, 'Porcelain Figurines', 0, 0, '', ''),
(107, 75, 183, 184, 1, 'Railroadiana', 0, 0, '', ''),
(108, 75, 181, 182, 1, 'Religious', 0, 0, '', ''),
(109, 75, 179, 180, 1, 'Rocks, Minerals &amp; Fossils', 0, 0, '', ''),
(110, 75, 177, 178, 1, 'Scientific Instruments', 0, 0, '', ''),
(111, 75, 175, 176, 1, 'Textiles', 0, 0, '', ''),
(112, 75, 173, 174, 1, 'Tobacciana', 0, 0, '', ''),
(113, 1, 154, 171, 0, 'Comics, Cards &amp; Science Fiction', 0, 0, '', ''),
(114, 113, 169, 170, 1, 'Anime &amp; Manga', 0, 0, '', ''),
(115, 113, 167, 168, 1, 'Comic Books', 0, 0, '', ''),
(116, 113, 165, 166, 1, 'General', 0, 0, '', ''),
(117, 113, 163, 164, 1, 'Godzilla', 0, 0, '', ''),
(118, 113, 161, 162, 1, 'Star Trek', 0, 0, '', ''),
(119, 113, 159, 160, 1, 'The X-Files', 0, 0, '', ''),
(120, 113, 157, 158, 1, 'Toys', 0, 0, '', ''),
(121, 113, 155, 156, 1, 'Trading Cards', 0, 0, '', ''),
(122, 1, 144, 153, 0, 'Computers &amp; Software', 0, 0, '', ''),
(123, 122, 151, 152, 1, 'General', 0, 0, '', ''),
(124, 122, 149, 150, 1, 'Hardware', 0, 0, '', ''),
(125, 122, 147, 148, 1, 'Internet Services', 0, 0, '', ''),
(126, 122, 145, 146, 1, 'Software', 0, 0, '', ''),
(127, 1, 132, 143, 0, 'Electronics &amp; Photography', 0, 0, '', ''),
(128, 127, 141, 142, 1, 'Consumer Electronics', 0, 0, '', ''),
(129, 127, 139, 140, 1, 'General', 0, 0, '', ''),
(130, 127, 137, 138, 1, 'Photo Equipment', 0, 0, '', ''),
(131, 127, 135, 136, 1, 'Recording Equipment', 0, 0, '', ''),
(132, 127, 133, 134, 1, 'Video Equipment', 0, 0, '', ''),
(133, 1, 112, 131, 0, 'Home &amp; Garden', 0, 0, '', ''),
(134, 133, 129, 130, 1, 'Baby Items', 0, 0, '', ''),
(135, 133, 127, 128, 1, 'Crafts', 0, 0, '', ''),
(136, 133, 125, 126, 1, 'Furniture', 0, 0, '', ''),
(137, 133, 123, 124, 1, 'Garden', 0, 0, '', ''),
(138, 133, 121, 122, 1, 'General', 0, 0, '', ''),
(139, 133, 119, 120, 1, 'Household Items', 0, 0, '', ''),
(140, 133, 117, 118, 1, 'Pet Supplies', 0, 0, '', ''),
(141, 133, 115, 116, 1, 'Tools &amp; Hardware', 0, 0, '', ''),
(142, 133, 113, 114, 1, 'Weddings', 0, 0, '', ''),
(143, 1, 98, 111, 0, 'Movies &amp; Video', 0, 0, '', ''),
(144, 143, 109, 110, 1, 'Blueray', 0, 0, '', ''),
(145, 143, 107, 108, 1, 'DVD', 0, 0, '', ''),
(146, 143, 105, 106, 1, 'General', 0, 0, '', ''),
(147, 143, 103, 104, 1, 'HD-DVD', 0, 0, '', ''),
(148, 143, 101, 102, 1, 'Laser Discs', 0, 0, '', ''),
(149, 143, 99, 100, 1, 'VHS', 0, 0, '', ''),
(150, 1, 84, 97, 0, 'Music', 0, 0, '', ''),
(151, 150, 95, 96, 1, 'CDs', 0, 0, '', ''),
(152, 150, 93, 94, 1, 'General', 0, 0, '', ''),
(153, 150, 91, 92, 1, 'Instruments', 0, 0, '', ''),
(154, 150, 89, 90, 1, 'Memorabilia', 0, 0, '', ''),
(155, 150, 87, 88, 1, 'Records', 0, 0, '', ''),
(156, 150, 85, 86, 1, 'Tapes', 0, 0, '', ''),
(157, 1, 74, 83, 0, 'Office &amp; Business', 0, 0, '', ''),
(158, 157, 81, 82, 1, 'Briefcases', 0, 0, '', ''),
(159, 157, 79, 80, 1, 'Fax Machines', 0, 0, '', ''),
(160, 157, 77, 78, 1, 'General Equipment', 0, 0, '', ''),
(161, 157, 75, 76, 1, 'Pagers', 0, 0, '', ''),
(162, 1, 58, 73, 0, 'Other Goods &amp; Services', 0, 0, '', ''),
(163, 162, 71, 72, 1, 'General', 0, 0, '', ''),
(164, 162, 69, 70, 1, 'Metaphysical', 0, 0, '', ''),
(165, 162, 67, 68, 1, 'Property', 0, 0, '', ''),
(166, 162, 65, 66, 1, 'Services', 0, 0, '', ''),
(167, 162, 63, 64, 1, 'Tickets &amp; Events', 0, 0, '', ''),
(168, 162, 61, 62, 1, 'Transportation', 0, 0, '', ''),
(169, 162, 59, 60, 1, 'Travel', 0, 0, '', ''),
(170, 1, 50, 57, 0, 'Sports &amp; Recreation', 0, 0, '', ''),
(171, 170, 55, 56, 1, 'Apparel &amp; Equipment', 0, 0, '', ''),
(172, 170, 53, 54, 1, 'Exercise Equipment', 0, 0, '', ''),
(173, 170, 51, 52, 1, 'General', 0, 0, '', ''),
(174, 1, 2, 49, 0, 'Toys &amp; Games', 0, 0, '', ''),
(175, 174, 47, 48, 1, 'Action Figures', 0, 0, '', ''),
(176, 174, 45, 46, 1, 'Beanie Babies &amp; Beanbag Toys', 0, 0, '', ''),
(177, 174, 43, 44, 1, 'Diecast', 0, 0, '', ''),
(178, 174, 41, 42, 1, 'Fast Food', 0, 0, '', ''),
(179, 174, 39, 40, 1, 'Fisher-Price', 0, 0, '', ''),
(180, 174, 37, 38, 1, 'Furby', 0, 0, '', ''),
(181, 174, 35, 36, 1, 'Games', 0, 0, '', ''),
(182, 174, 33, 34, 1, 'General', 0, 0, '', ''),
(183, 174, 31, 32, 1, 'Giga Pet &amp; Tamagotchi', 0, 0, '', ''),
(184, 174, 29, 30, 1, 'Hobbies', 0, 0, '', ''),
(185, 174, 27, 28, 1, 'Marbles', 0, 0, '', ''),
(186, 174, 25, 26, 1, 'My Little Pony', 0, 0, '', ''),
(187, 174, 23, 24, 1, 'Peanuts Gang', 0, 0, '', ''),
(188, 174, 21, 22, 1, 'Pez', 0, 0, '', ''),
(189, 174, 19, 20, 1, 'Plastic Models', 0, 0, '', ''),
(190, 174, 17, 18, 1, 'Plush Toys', 0, 0, '', ''),
(191, 174, 15, 16, 1, 'Puzzles', 0, 0, '', ''),
(192, 174, 13, 14, 1, 'lot Cars', 0, 0, '', ''),
(193, 174, 11, 12, 1, 'Teletubbies', 0, 0, '', ''),
(194, 174, 9, 10, 1, 'Toy Soldiers', 0, 0, '', ''),
(195, 174, 7, 8, 1, 'Vintage', 0, 0, '', ''),
(196, 174, 5, 6, 1, 'Vintage Tin', 0, 0, '', ''),
(197, 174, 3, 4, 1, 'Vintage Vehicles', 0, 0, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_closedrelisted`
--

CREATE TABLE IF NOT EXISTS `tubauction_closedrelisted` (
  `auction` int(32) DEFAULT '0',
  `relistdate` varchar(8) NOT NULL DEFAULT '',
  `newauction` int(32) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_community`
--

CREATE TABLE IF NOT EXISTS `tubauction_community` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `messages` int(11) NOT NULL DEFAULT '0',
  `lastmessage` varchar(14) NOT NULL DEFAULT '0',
  `msgstoshow` int(11) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  KEY `msg_id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tubauction_community`
--

INSERT INTO `tubauction_community` (`id`, `name`, `messages`, `lastmessage`, `msgstoshow`, `active`) VALUES
(1, 'Selling', 0, '', 30, 1),
(2, 'Buying', 0, '', 30, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_comm_messages`
--

CREATE TABLE IF NOT EXISTS `tubauction_comm_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `boardid` int(11) NOT NULL DEFAULT '0',
  `msgdate` varchar(14) NOT NULL DEFAULT '',
  `user` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  KEY `msg_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_conditions`
--

CREATE TABLE IF NOT EXISTS `tubauction_conditions` (
  `item_condition` varchar(40) NOT NULL DEFAULT '',
  `condition_desc` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_conditions`
--

INSERT INTO `tubauction_conditions` (`item_condition`, `condition_desc`) VALUES
('As New', 'As New: A used item that is fully functional and in excellent cosmetic condition,possibly without original packaging'),
('As New Never Used', 'As New Never Used: A brand-new unused item may not be in original packaging or packaging may not be sealed'),
('Brand New', 'Brand New: A brand-new unused unopened undamaged item ,in it&#39;s original packaging'),
('Faulty Suit Parts Only', 'Faulty Suit Parts Only: An item that has damage or is not working and is not worth repairing but may be used for parts'),
('Faulty Suit Parts or Repair', 'Faulty Suit Parts or Repair: An item that has damage or is not working and is possibly worth repairing or may be used for parts'),
('Manufacturer Refurbished', 'Manufacturer Refurbished: An item that has been restored by either the manufacturer or a professional repairer item should be perfect working order and in good cosmetic condition'),
('New Old Stock', 'New Old Stock: An item which was manufactured long ago but that has never been used and is either out of production or discontinued from the current line of product or has been sitting stored for some'),
('New With Tags', 'New With Tags: A brand-new unused and unworn item in it&#39;s original packaging if originally sold packaged with the original manufacturers tags'),
('New Without Tags', 'New Without Tags: A brand-new unused and unworn item maybe missing it&#39;s original packaging if originally sold packaged and without the original manufacturers tags'),
('Pre-Owned', 'Pre-Owned: An item that has been worn previously'),
('Seller Refurbished', 'Seller Refurbished: An item that has been restored by either the seller or a non professional repairer item should be perfect working order and in good cosmetic condition'),
('Used', 'Used: An item that has been used and may show signs of wear and tear but should be fully functional,unless stated in the item description.'),
('Factory Second', 'Factory Second: An item that is fully functional but may have some cosmectic damage ie dents or scratches');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_counters`
--

CREATE TABLE IF NOT EXISTS `tubauction_counters` (
  `users` int(11) DEFAULT '0',
  `inactiveusers` int(11) NOT NULL DEFAULT '0',
  `auctions` int(11) DEFAULT '0',
  `closedauctions` int(11) NOT NULL DEFAULT '0',
  `bids` int(11) NOT NULL DEFAULT '0',
  `suspendedauctions` int(11) NOT NULL DEFAULT '0',
  `items_sold` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_counters`
--

INSERT INTO `tubauction_counters` (`users`, `inactiveusers`, `auctions`, `closedauctions`, `bids`, `suspendedauctions`, `items_sold`) VALUES
(1, 0, 0, 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_countries`
--

CREATE TABLE IF NOT EXISTS `tubauction_countries` (
  `country` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_countries`
--

INSERT INTO `tubauction_countries` (`country`) VALUES
('Afghanistan'),
('Albania'),
('Algeria'),
('American Samoa'),
('Andorra'),
('Angola'),
('Anguilla'),
('Antarctica'),
('Antigua And Barbuda'),
('Argentina'),
('Armenia'),
('Aruba'),
('Australia'),
('Austria'),
('Azerbaijan Republic'),
('Bahamas'),
('Bahrain'),
('Bangladesh'),
('Barbados'),
('Belarus'),
('Belgium'),
('Belize'),
('Benin'),
('Bermuda'),
('Bhutan'),
('Bolivia'),
('Bosnia and Herzegowina'),
('Botswana'),
('Bouvet Island'),
('Brazil'),
('British Indian Ocean Territory'),
('Brunei Darussalam'),
('Bulgaria'),
('Burkina Faso'),
('Burma'),
('Burundi'),
('Cambodia'),
('Cameroon'),
('Canada'),
('Cape Verde'),
('Cayman Islands'),
('Central African Republic'),
('Chad'),
('Chile'),
('China'),
('Christmas Island'),
('Cocos &#40;Keeling&#41; Islands'),
('Colombia'),
('Comoros'),
('Congo'),
('Congo, the Democratic Republic'),
('Cook Islands'),
('Costa Rica'),
('Cote d&#39;Ivoire'),
('Croatia'),
('Cyprus'),
('Czech Republic'),
('Denmark'),
('Djibouti'),
('Dominica'),
('Dominican Republic'),
('East Timor'),
('Ecuador'),
('Egypt'),
('El Salvador'),
('Equatorial Guinea'),
('Eritrea'),
('Estonia'),
('Ethiopia'),
('Falkland Islands'),
('Faroe Islands'),
('Fiji'),
('Finland'),
('France'),
('French Guiana'),
('French Polynesia'),
('French Southern Territories'),
('Gabon'),
('Gambia'),
('Georgia'),
('Germany'),
('Ghana'),
('Gibraltar'),
('Great Britain'),
('Greece'),
('Greenland'),
('Grenada'),
('Guadeloupe'),
('Guam'),
('Guatemala'),
('Guinea'),
('Guinea-Bissau'),
('Guyana'),
('Haiti'),
('Heard and Mc Donald Islands'),
('Honduras'),
('Hong Kong'),
('Hungary'),
('Iceland'),
('India'),
('Indonesia'),
('Ireland'),
('Israel'),
('Italy'),
('Jamaica'),
('Japan'),
('Jordan'),
('Kazakhstan'),
('Kenya'),
('Kiribati'),
('Korea &#40;South&#41;'),
('Kuwait'),
('Kyrgyzstan'),
('Lao People&#39;s Democratic Republic'),
('Latvia'),
('Lebanon'),
('Lesotho'),
('Liberia'),
('Liechtenstein'),
('Lithuania'),
('Luxembourg'),
('Macau'),
('Macedonia'),
('Madagascar'),
('Malawi'),
('Malaysia'),
('Maldives'),
('Mali'),
('Malta'),
('Marshall Islands'),
('Martinique'),
('Mauritania'),
('Mauritius'),
('Mayotte'),
('Mexico'),
('Micronesia, Federated States of'),
('Moldova, Republic of'),
('Monaco'),
('Mongolia'),
('Montserrat'),
('Morocco'),
('Mozambique'),
('Namibia'),
('Nauru'),
('Nepal'),
('Netherlands'),
('Netherlands Antilles'),
('New Caledonia'),
('New Zealand'),
('Nicaragua'),
('Niger'),
('Nigeria'),
('Niuev'),
('Norfolk Island'),
('Northern Mariana Islands'),
('Norway'),
('Oman'),
('Pakistan'),
('Palau'),
('Panama'),
('Papua New Guinea'),
('Paraguay'),
('Peru'),
('Philippines'),
('Pitcairn'),
('Poland'),
('Portugal'),
('Puerto Rico'),
('Qatar'),
('Reunion'),
('Romania'),
('Russian Federation'),
('Rwanda'),
('Saint Kitts and Nevis'),
('Saint Lucia'),
('Saint Vincent and the Grenadin'),
('Samoa &#40;Independent&#41;'),
('San Marino'),
('Sao Tome and Principe'),
('Saudi Arabia'),
('Senegal'),
('Seychelles'),
('Sierra Leone'),
('Singapore'),
('Slovakia'),
('Slovenia'),
('Solomon Islands'),
('Somalia'),
('South Africa'),
('South Georgia'),
('Spain'),
('Sri Lanka'),
('St. Helena'),
('St. Pierre and Miquelon'),
('Suriname'),
('Svalbard and Jan Mayen Islands'),
('Swaziland'),
('Sweden'),
('Switzerland'),
('Taiwan'),
('Tajikistan'),
('Tanzania'),
('Thailand'),
('Togo'),
('Tokelau'),
('Tonga'),
('Trinidad and Tobago'),
('Tunisia'),
('Turkey'),
('Turkmenistan'),
('Turks and Caicos Islands'),
('Tuvalu'),
('Uganda'),
('Ukraine'),
('United Arab Emiratesv'),
('United Kingdom'),
('United States'),
('Uruguay'),
('Uzbekistan'),
('Vanuatu'),
('Venezuela'),
('Viet Nam'),
('Virgin Islands &#40;British&#41;'),
('Virgin Islands &#40;U.S.&#41;'),
('Wallis and Futuna Islands'),
('Western Sahara'),
('Yemen'),
('Zambia'),
('Zimbabwe');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_currentaccesses`
--

CREATE TABLE IF NOT EXISTS `tubauction_currentaccesses` (
  `day` char(2) NOT NULL DEFAULT '0',
  `month` char(2) NOT NULL DEFAULT '0',
  `year` char(4) NOT NULL DEFAULT '0',
  `pageviews` int(11) NOT NULL DEFAULT '0',
  `uniquevisitors` int(11) NOT NULL DEFAULT '0',
  `usersessions` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_currentbots`
--

CREATE TABLE IF NOT EXISTS `tubauction_currentbots` (
  `month` char(2) NOT NULL DEFAULT '0',
  `year` varchar(4) NOT NULL DEFAULT '0',
  `platform` varchar(120) NOT NULL DEFAULT '0',
  `browser` varchar(50) NOT NULL DEFAULT '0',
  `counter` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_currentbrowsers`
--

CREATE TABLE IF NOT EXISTS `tubauction_currentbrowsers` (
  `month` char(2) NOT NULL DEFAULT '0',
  `year` varchar(4) NOT NULL DEFAULT '0',
  `browser` varchar(50) NOT NULL DEFAULT '0',
  `counter` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_currentplatforms`
--

CREATE TABLE IF NOT EXISTS `tubauction_currentplatforms` (
  `month` char(2) NOT NULL DEFAULT '0',
  `year` varchar(4) NOT NULL DEFAULT '0',
  `platform` varchar(120) NOT NULL DEFAULT '0',
  `counter` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_digital_items`
--

CREATE TABLE IF NOT EXISTS `tubauction_digital_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auctions` int(10) NOT NULL,
  `seller` int(10) NOT NULL,
  `item` tinytext NOT NULL,
  `hash` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_digital_item_mime`
--

CREATE TABLE IF NOT EXISTS `tubauction_digital_item_mime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '',
  `mine_type` varchar(250) NOT NULL DEFAULT '',
  `file_extension` varchar(250) NOT NULL DEFAULT '',
  `use_mime` enum('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Дамп данных таблицы `tubauction_digital_item_mime`
--

INSERT INTO `tubauction_digital_item_mime` (`id`, `name`, `mine_type`, `file_extension`, `use_mime`) VALUES
(1, '7-Zip', 'application/x-7z-compressed', '7z', 'n'),
(2, 'PostScript', 'application/postscript', 'ai', 'n'),
(3, 'Android Package Archive', 'application/vnd.android.package-archive', 'apk', 'n'),
(4, 'Audio Video Interleave (AVI)', 'video/x-msvideo', 'avi', 'n'),
(5, 'Bitmap Image File', 'image/bmp', 'bmp', 'n'),
(6, 'Cascading Style Sheets (CSS)', 'text/css', 'css', 'n'),
(7, 'Microsoft Word', 'application/msword', 'doc', 'n'),
(8, 'PostScript', 'application/x-7z-compressed', '7z', 'n'),
(9, 'Microsoft Application', 'application/x-msdownload', 'exe', 'n'),
(10, 'Flash Video', 'video/x-f4v', 'f4v', 'n'),
(11, 'Flash Video', 'video/x-flv', 'flv', 'n'),
(12, 'Graphics Interchange Format', 'image/gif', 'gif', 'n'),
(13, 'H.261', 'video/h261', 'h261', 'n'),
(14, 'H.263', 'video/h263', 'h263', 'n'),
(15, 'H.264', 'video/h264', 'h264', 'n'),
(16, 'HyperText Markup Language (HTM)', 'text/html', 'htm', 'n'),
(17, 'HyperText Markup Language (HTML)', 'text/html', 'html', 'n'),
(18, 'Icon Image', 'image/x-icon', 'ico', 'n'),
(19, 'Java Archive', 'application/java-archive', 'jar', 'n'),
(20, 'JPEG Image', 'image/jpeg', 'jpe', 'n'),
(21, 'JPEG Image', 'image/jpg', 'jpeg', 'n'),
(22, 'JPEG Image', 'image/jpg', 'jpg', 'n'),
(23, 'JavaScript', 'application/javascript', 'js', 'n'),
(24, 'JavaScript Object Notation (JSON)', 'application/json', 'json', 'n'),
(25, 'M4v', 'video/x-m4v', 'm4v', 'n'),
(26, 'Quicktime Video', 'video/quicktime', 'mov', 'n'),
(27, 'MPEG-3 Audio', 'audio/mpeg', 'mp3', 'n'),
(28, 'MPEG-4 Video', 'video/mp4', 'mp4', 'n'),
(29, 'MPEG-4 Audio', 'audio/mp4', 'mp4a', 'n'),
(30, 'MPEG Video', 'video/mpeg', 'mpeg', 'n'),
(31, 'MPEG Audio', 'audio/mpeg', 'mpga', 'n'),
(32, 'OpenDocument Spreadsheet', 'application/vnd.oasis.opendocument.spreadsheet', 'ods', 'n'),
(33, 'OpenDocument Text', 'application/vnd.oasis.opendocument.text', 'odt', 'n'),
(34, 'Adobe Portable Document Format', 'application/pdf', 'pdf', 'n'),
(35, 'Portable Network Graphics (PNG)', 'image/png', 'png', 'n'),
(36, 'Microsoft PowerPoint', 'application/vnd.ms-powerpoint', 'ppt', 'n'),
(37, 'PostScript', 'application/postscript', 'ps', 'n'),
(38, 'Photoshop Document', 'image/vnd.adobe.photoshop', 'psd', 'n'),
(39, 'Quicktime Video', 'video/quicktime', 'qt', 'n'),
(40, 'RAR Archive', 'application/x-rar-compressed', 'rar', 'n'),
(41, 'Rich Text Format', 'application/rtf', 'rtf', 'n'),
(42, 'Digital Video Broadcasting', 'application/vnd.dvb.service', 'svc', 'n'),
(43, 'Scalable Vector Graphics (SVG)', 'image/svg+xml', 'svg', 'n'),
(44, 'Scalable Vector Graphics (SVG)', 'image/svg+xml', 'svgz', 'n'),
(45, 'Adobe Flash', 'application/x-shockwave-flash', 'swf', 'n'),
(46, 'Tagged Image File Format', 'image/tiff', 'tif', 'n'),
(47, 'Tagged Image File Format', 'image/tiff', 'tiff', 'n'),
(48, 'Text File', 'text/plain', 'txt', 'n'),
(49, 'Microsoft Windows Media', 'video/x-ms-wm', 'wm', 'n'),
(50, 'Microsoft Windows Media Audio', 'audio/x-ms-wma', 'wma', 'n'),
(51, 'Microsoft Excel', 'application/vnd.ms-excel', 'xls', 'n'),
(52, 'XML - Extensible Markup Language', 'application/xml', 'xml', 'n'),
(53, 'Zip Archive', 'application/zip', 'zip', 'n');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_durations`
--

CREATE TABLE IF NOT EXISTS `tubauction_durations` (
  `days` int(11) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_durations`
--

INSERT INTO `tubauction_durations` (`days`, `description`) VALUES
(1, '1 day'),
(2, '2 days'),
(3, '3 days'),
(7, '1 week'),
(14, '2 weeks'),
(21, '3 weeks'),
(30, '1 month');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_email_blocks`
--

CREATE TABLE IF NOT EXISTS `tubauction_email_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_check` varchar(200) NOT NULL DEFAULT '',
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=274 ;

--
-- Дамп данных таблицы `tubauction_email_blocks`
--

INSERT INTO `tubauction_email_blocks` (`id`, `email_check`) VALUES
(1, '0815.ru0clickemail.com'),
(2, '0wnd.net'),
(3, '0wnd.org'),
(4, '10minutemail.com'),
(5, '20minutemail.com'),
(6, '2prong.com'),
(7, '3d-painting.com'),
(8, '4warding.com'),
(9, '4warding.net'),
(10, '4warding.org'),
(11, '9ox.net'),
(12, 'a-bc.net'),
(13, 'amilegit.com'),
(14, 'anonbox.net'),
(15, 'anonymbox.com'),
(16, 'antichef.com'),
(17, 'antichef.net'),
(18, 'antispam.de'),
(19, 'baxomale.ht.cx'),
(20, 'beefmilk.com'),
(21, 'binkmail.com'),
(22, 'bio-muesli.net'),
(23, 'bobmail.info'),
(24, 'bodhi.lawlita.com'),
(25, 'bofthew.com'),
(26, 'brefmail.com'),
(27, 'bsnow.net'),
(28, 'bugmenot.com'),
(29, 'bumpymail.com'),
(30, 'casualdx.com'),
(31, 'chogmail.com'),
(32, 'cool.fr.nf'),
(33, 'correo.blogos.net'),
(34, 'cosmorph.com'),
(35, 'courriel.fr.nf'),
(36, 'courrieltemporaire.com'),
(37, 'curryworld.de'),
(38, 'cust.in'),
(39, 'dacoolest.com'),
(40, 'dandikmail.com'),
(41, 'deadaddress.com'),
(42, 'despam.it'),
(43, 'devnullmail.com'),
(44, 'dfgh.net'),
(45, 'digitalsanctuary.com'),
(46, 'discardmail.com'),
(47, 'discardmail.de'),
(48, 'disposableaddress.com'),
(49, 'disposemail.com'),
(50, 'dispostable.com'),
(51, 'dm.w3internet.co.uk example.com'),
(52, 'dodgeit.com'),
(53, 'dodgit.com'),
(54, 'dodgit.org'),
(55, 'dontreg.com'),
(56, 'dontsendmespam.de'),
(57, 'dump-email.info'),
(58, 'dumpyemail.com'),
(59, 'e4ward.com'),
(60, 'email60.com'),
(61, 'emailias.com'),
(62, 'emailinfive.com'),
(63, 'emailmiser.com'),
(64, 'emailtemporario.com.br'),
(65, 'emailwarden.com'),
(66, 'ephemail.net'),
(67, 'explodemail.com'),
(68, 'fakeinbox.com'),
(69, 'fakeinformation.com'),
(70, 'fastacura.com'),
(71, 'filzmail.com'),
(72, 'fizmail.com'),
(73, 'frapmail.com'),
(74, 'garliclife.com'),
(75, 'get1mail.com'),
(76, 'getonemail.com'),
(77, 'getonemail.net'),
(78, 'girlsundertheinfluence.com'),
(79, 'gishpuppy.com'),
(80, 'great-host.in'),
(81, 'gsrv.co.uk'),
(82, 'guerillamail.biz'),
(83, 'guerillamail.com'),
(84, 'guerillamail.net'),
(85, 'guerillamail.org'),
(86, 'guerrillamail.com'),
(87, 'guerrillamailblock.com'),
(88, 'haltospam.com'),
(89, 'hotpop.com'),
(90, 'ieatspam.eu'),
(91, 'ieatspam.info'),
(92, 'ihateyoualot.info'),
(93, 'imails.info'),
(94, 'inboxclean.com'),
(95, 'inboxclean.org'),
(96, 'incognitomail.com'),
(97, 'incognitomail.net'),
(98, 'ipoo.org'),
(99, 'irish2me.com'),
(100, 'jetable.com'),
(101, 'jetable.fr.nf'),
(102, 'jetable.net'),
(103, 'jetable.org'),
(104, 'junk1e.com'),
(105, 'kaspop.com'),
(106, 'kulturbetrieb.info'),
(107, 'kurzepost.de'),
(108, 'lifebyfood.com'),
(109, 'link2mail.net'),
(110, 'litedrop.com'),
(111, 'lookugly.com'),
(112, 'lopl.co.cc'),
(113, 'lr78.com'),
(114, 'maboard.com'),
(115, 'mail.by'),
(116, 'mail.mezimages.net'),
(117, 'mail4trash.com'),
(118, 'mailbidon.com'),
(119, 'mailcatch.com'),
(120, 'maileater.com'),
(121, 'mailexpire.com'),
(122, 'mailin8r.com'),
(123, 'mailinator.com'),
(124, 'mailinator.net'),
(125, 'mailinator2.com'),
(126, 'mailincubator.com'),
(127, 'mailme.lv'),
(128, 'mailnator.com'),
(129, 'mailnull.com'),
(130, 'mailzilla.org'),
(131, 'mbx.cc'),
(132, 'mega.zik.dj'),
(133, 'meltmail.com'),
(134, 'mierdamail.com'),
(135, 'mintemail.com'),
(136, 'moncourrier.fr.nf'),
(137, 'monemail.fr.nf'),
(138, 'monmail.fr.nf'),
(139, 'mt2009.com'),
(140, 'mx0.wwwnew.eu'),
(141, 'mycleaninbox.net'),
(142, 'mytrashmail.com'),
(143, 'neverbox.com'),
(144, 'nobulk.com'),
(145, 'noclickemail.com'),
(146, 'nogmailspam.info'),
(147, 'nomail.xl.cx'),
(148, 'nomail2me.com'),
(149, 'no-spam.ws'),
(150, 'nospam.ze.tc'),
(151, 'nospam4.us'),
(152, 'nospamfor.us'),
(153, 'nowmymail.com'),
(154, 'objectmail.com'),
(155, 'obobbo.com'),
(156, 'onewaymail.com'),
(157, 'ordinaryamerican.net'),
(158, 'owlpic.com'),
(159, 'pookmail.com'),
(160, 'proxymail.eu'),
(161, 'punkass.com'),
(162, 'putthisinyourspamdatabase.com'),
(163, 'quickinbox.com'),
(164, 'rcpt.at'),
(165, 'recode.me'),
(166, 'recursor.net'),
(167, 'rediff.com'),
(168, 'regbypass.comsafe-mail.net'),
(169, 'safetymail.info'),
(170, 'sandelf.de'),
(171, 'saynotospams.com'),
(172, 'selfdestructingmail.com'),
(173, 'sendspamhere.com'),
(174, 'shiftmail.com'),
(175, '****mail.me'),
(176, 'skeefmail.com'),
(177, 'slopsbox.com'),
(178, 'smellfear.com'),
(179, 'snakemail.com'),
(180, 'sneakemail.com'),
(181, 'sofort-mail.de'),
(182, 'sogetthis.com'),
(183, 'soodonims.com'),
(184, 'spam.la'),
(185, 'spamavert.com'),
(186, 'spambob.net'),
(187, 'spambob.org'),
(188, 'spambog.com'),
(189, 'spambog.de'),
(190, 'spambog.ru'),
(191, 'spambox.info'),
(192, 'spambox.us'),
(193, 'spamcannon.com'),
(194, 'spamcannon.net'),
(195, 'spamcero.com'),
(196, 'spamcorptastic.com'),
(197, 'spamcowboy.com'),
(198, 'spamcowboy.net'),
(199, 'spamcowboy.org'),
(200, 'spamday.com'),
(201, 'spamex.com'),
(202, 'spamfree24.com'),
(203, 'spamfree24.de'),
(204, 'spamfree24.eu'),
(205, 'spamfree24.info'),
(206, 'spamfree24.net'),
(207, 'spamfree24.org'),
(208, 'spamgourmet.com'),
(209, 'spamgourmet.net'),
(210, 'spamgourmet.org'),
(211, 'spamherelots.com'),
(212, 'spamhereplease.com'),
(213, 'spamhole.com'),
(214, 'spamify.com'),
(215, 'spaminator.de'),
(216, 'spamkill.info'),
(217, 'spaml.com'),
(218, 'spaml.de'),
(219, 'spammotel.com'),
(220, 'spamobox.com'),
(221, 'spamspot.com'),
(222, 'spamthis.co.uk'),
(223, 'spamthisplease.com'),
(224, 'speed.1s.fr'),
(225, 'suremail.info'),
(226, 'tempalias.com'),
(227, 'tempemail.biz'),
(228, 'tempemail.com'),
(229, 'tempe-mail.com'),
(230, 'tempemail.net'),
(231, 'tempinbox.co.uk'),
(232, 'tempinbox.com'),
(233, 'tempomail.fr'),
(234, 'temporaryemail.net'),
(235, 'temporaryinbox.com'),
(236, 'thankyou2010.com'),
(237, 'thisisnotmyrealemail.com'),
(238, 'throwawayemailaddress.com'),
(239, 'tilien.com'),
(240, 'tmailinator.com'),
(241, 'tradermail.info'),
(242, 'trash2009.com'),
(243, 'trash-amil.com'),
(244, 'trashmail.at'),
(245, 'trash-mail.at'),
(246, 'trashmail.com'),
(247, 'trash-mail.com'),
(248, 'trash-mail.de'),
(249, 'trashmail.me'),
(250, 'trashmail.net'),
(251, 'trashymail.com'),
(252, 'trashymail.net'),
(253, 'tyldd.com'),
(254, 'uggsrock.com'),
(255, 'wegwerfmail.de'),
(256, 'wegwerfmail.net'),
(257, 'wegwerfmail.org'),
(258, 'wh4f.org'),
(259, 'whyspam.me'),
(260, 'willselfdestruct.com'),
(261, 'winemaven.info'),
(262, 'wronghead.com'),
(263, 'wuzupmail.net'),
(264, 'xoxy.net'),
(265, 'yogamaven.com'),
(266, 'yopmail.com'),
(267, 'yopmail.fr'),
(268, 'yopmail.net'),
(269, 'yuurok.com'),
(270, 'zippymail.info'),
(271, 'jnxjn.com'),
(272, 'trashmailer.com'),
(273, 'klzlk.com'),
(273, 'sina.com');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_faqs`
--

CREATE TABLE IF NOT EXISTS `tubauction_faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(200) NOT NULL DEFAULT '',
  `answer` text NOT NULL,
  `category` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `tubauction_faqs`
--

INSERT INTO `tubauction_faqs` (`id`, `question`, `answer`, `category`) VALUES
(2, 'Registering', 'To register as a new user, click on Register at the top of the window. You will be asked for your name, a username and password, and contact information, including your email address.\r\n\r\n<B>You must be at least 18 years of age to register.</B>!', 1),
(4, 'Item Watch', '<b>Item watch</b> notifies you when someone bids on the auctions that you have added to your Item Watch. ', 3),
(5, 'What is a Dutch auction?', 'Dutch auction is a type of auction where the auctioneer begins with a high asking price which is lowered until some participant is willing to accept the auctioneer''s price. The winning participant pays the last announced price.', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_faqscategories`
--

CREATE TABLE IF NOT EXISTS `tubauction_faqscategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(200) NOT NULL DEFAULT '',
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tubauction_faqscategories`
--

INSERT INTO `tubauction_faqscategories` (`id`, `category`) VALUES
(1, 'General'),
(2, 'Selling'),
(3, 'Buying');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_faqscat_translated`
--

CREATE TABLE IF NOT EXISTS `tubauction_faqscat_translated` (
  `id` int(11) NOT NULL DEFAULT '0',
  `lang` char(2) NOT NULL DEFAULT '',
  `category` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_faqscat_translated`
--

INSERT INTO `tubauction_faqscat_translated` (`id`, `lang`, `category`) VALUES
(3, 'EN', 'Buying'),
(1, 'EN', 'General'),
(2, 'EN', 'Selling');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_faqs_translated`
--

CREATE TABLE IF NOT EXISTS `tubauction_faqs_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` char(2) NOT NULL DEFAULT '',
  `question` varchar(200) NOT NULL DEFAULT '',
  `answer` text NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tubauction_faqs_translated`
--

INSERT INTO `tubauction_faqs_translated` (`id`, `lang`, `question`, `answer`) VALUES
(2, 'EN', 'Registering', 'To register as a new user, click on Register at the top of the window. You will be asked for your name, a username and password, and contact information, including your email address.\r\n\r\n<B>You must be at least 18 years of age to register.</B>!'),
(4, 'EN', 'Item Watch', '<b>Item watch</b> notifies you when someone bids on the auctions that you have added to your Item Watch. ');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_favesellers`
--

CREATE TABLE IF NOT EXISTS `tubauction_favesellers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `seller_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_fblogin`
--

CREATE TABLE IF NOT EXISTS `tubauction_fblogin` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `fb_id` varchar(150) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(600) DEFAULT NULL,
  `postdate` int(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `birthday` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_feedbacks`
--

CREATE TABLE IF NOT EXISTS `tubauction_feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rated_user_id` int(32) DEFAULT NULL,
  `rater_user_nick` varchar(20) DEFAULT NULL,
  `feedback` mediumtext,
  `rate` int(2) DEFAULT NULL,
  `feedbackdate` int(15) NOT NULL,
  `auction_id` int(32) NOT NULL DEFAULT '0',
  `auction_title` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_fees`
--

CREATE TABLE IF NOT EXISTS `tubauction_fees` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `fee_from` double(16,2) NOT NULL DEFAULT '0.00',
  `fee_to` double(16,2) NOT NULL DEFAULT '0.00',
  `fee_type` enum('flat','perc') NOT NULL DEFAULT 'flat',
  `value` double(8,2) NOT NULL DEFAULT '0.00',
  `type` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `tubauction_fees`
--

INSERT INTO `tubauction_fees` (`id`, `fee_from`, `fee_to`, `fee_type`, `value`, `type`) VALUES
(1, 0.00, 0.00, 'flat', 0.00, 'signup_fee'),
(2, 0.00, 0.00, 'flat', 0.00, 'buyer_fee'),
(3, 0.00, 0.00, 'flat', 0.00, 'setup'),
(4, 0.00, 0.00, 'flat', 0.00, 'hpfeat_fee'),
(5, 0.00, 0.00, 'flat', 0.00, 'bolditem_fee'),
(6, 0.00, 0.00, 'flat', 0.00, 'hlitem_fee'),
(7, 0.00, 0.00, 'flat', 0.00, 'rp_fee'),
(8, 0.00, 0.00, 'flat', 0.00, 'picture_fee'),
(9, 0.00, 0.00, 'flat', 0.00, 'subtitle_fee'),
(10, 0.00, 0.00, 'flat', 0.00, 'excat_fee'),
(11, 0.00, 0.00, 'flat', 0.00, 'relist_fee'),
(12, 0.00, 0.00, 'flat', 0.00, 'buyout_fee'),
(13, 0.00, 0.00, 'flat', 0.00, 'endauc_fee'),
(14, 0.00, 0.00, 'flat', 0.00, 'banner_fee'),
(15, 0.00, 0.00, 'flat', 0.00, 'ex_banner_fee');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_filterwords`
--

CREATE TABLE IF NOT EXISTS `tubauction_filterwords` (
  `word` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_filterwords`
--

INSERT INTO `tubauction_filterwords` (`word`) VALUES
('');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_gateways`
--

CREATE TABLE IF NOT EXISTS `tubauction_gateways` (
  `gateways` text,
  `paypal_address` varchar(50) NOT NULL DEFAULT '',
  `paypal_required` int(1) NOT NULL DEFAULT '0',
  `paypal_active` int(1) NOT NULL DEFAULT '0',
  `authnet_address` varchar(50) NOT NULL DEFAULT '',
  `authnet_password` varchar(50) NOT NULL DEFAULT '',
  `authnet_required` int(1) NOT NULL DEFAULT '0',
  `authnet_active` int(1) NOT NULL DEFAULT '0',
  `worldpay_address` varchar(50) NOT NULL DEFAULT '',
  `worldpay_required` int(1) NOT NULL DEFAULT '0',
  `worldpay_active` int(1) NOT NULL DEFAULT '0',
  `skrill_address` varchar(50) NOT NULL DEFAULT '',
  `skrill_required` int(1) NOT NULL DEFAULT '0',
  `skrill_active` int(1) NOT NULL DEFAULT '0',
  `toocheckout_address` varchar(50) NOT NULL DEFAULT '',
  `toocheckout_required` int(1) NOT NULL DEFAULT '0',
  `toocheckout_active` int(1) NOT NULL DEFAULT '0',
  `bank_name` varchar(50) NOT NULL DEFAULT '',
  `bank_account` varchar(50) NOT NULL DEFAULT '',
  `bank_routing` varchar(50) NOT NULL DEFAULT '',
  `bank_required` int(1) NOT NULL DEFAULT '0',
  `bank_active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_gateways`
--

INSERT INTO `tubauction_gateways` (`gateways`, `paypal_address`, `paypal_required`, `paypal_active`, `authnet_address`, `authnet_password`, `authnet_required`, `authnet_active`, `worldpay_address`, `worldpay_required`, `worldpay_active`, `skrill_address`, `skrill_required`, `skrill_active`, `toocheckout_address`, `toocheckout_required`, `toocheckout_active`, `bank_name`, `bank_account`, `bank_routing`, `bank_required`, `bank_active`) VALUES
('paypal,authnet,worldpay,skrill,toocheckout,bank', '', 0, 1, '', '', 0, 1, '', 0, 1, '', 0, 1, '', 0, 1, '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_groups`
--

CREATE TABLE IF NOT EXISTS `tubauction_groups` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL DEFAULT '',
  `can_sell` int(1) NOT NULL DEFAULT '0',
  `can_buy` int(1) NOT NULL DEFAULT '0',
  `count` int(250) NOT NULL DEFAULT '0',
  `auto_join` int(1) NOT NULL DEFAULT '0',
  `no_fees` int(1) NOT NULL DEFAULT '0',
  `no_setup_fee` int(1) NOT NULL DEFAULT '0',
  `no_excat_fee` int(1) NOT NULL DEFAULT '0',
  `no_subtitle_fee` int(1) NOT NULL DEFAULT '0',
  `no_relist_fee` int(1) NOT NULL DEFAULT '0',
  `no_picture_fee` int(1) NOT NULL DEFAULT '0',
  `no_hpfeat_fee` int(1) NOT NULL DEFAULT '0',
  `no_hlitem_fee` int(1) NOT NULL DEFAULT '0',
  `no_bolditem_fee` int(1) NOT NULL DEFAULT '0',
  `no_rp_fee` int(1) NOT NULL DEFAULT '0',
  `no_buyout_fee` int(1) NOT NULL DEFAULT '0',
  `no_fp_fee` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tubauction_groups`
--

INSERT INTO `tubauction_groups` (`id`, `group_name`, `can_sell`, `can_buy`, `count`, `auto_join`, `no_fees`, `no_setup_fee`, `no_excat_fee`, `no_subtitle_fee`, `no_relist_fee`, `no_picture_fee`, `no_hpfeat_fee`, `no_hlitem_fee`, `no_bolditem_fee`, `no_rp_fee`, `no_buyout_fee`, `no_fp_fee`) VALUES
(1, 'Sellers', 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 'Buyers', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_increments`
--

CREATE TABLE IF NOT EXISTS `tubauction_increments` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `low` double(16,2) DEFAULT '0.00',
  `high` double(16,2) DEFAULT '0.00',
  `increment` double(16,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `tubauction_increments`
--

INSERT INTO `tubauction_increments` (`id`, `low`, `high`, `increment`) VALUES
(1, 0.00, 0.99, 0.28),
(2, 1.00, 9.99, 0.50),
(3, 10.00, 29.99, 1.00),
(4, 30.00, 99.99, 2.00),
(5, 100.00, 249.99, 5.00),
(6, 250.00, 499.99, 10.00),
(7, 500.00, 999.99, 25.00);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_logs`
--

CREATE TABLE IF NOT EXISTS `tubauction_logs` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `type` varchar(5) NOT NULL,
  `message` text NOT NULL,
  `action_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(32) NOT NULL DEFAULT '0',
  `ip` varchar(45) NOT NULL,
  `timestamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- Дамп данных таблицы `tubauction_logs`
--

INSERT INTO `tubauction_logs` (`id`, `type`, `message`, `action_id`, `user_id`, `ip`, `timestamp`) VALUES
(1, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455539666),
(2, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455539673),
(3, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455540010),
(4, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455540067),
(5, 'error', 'Unknown error type: [2] number_format() expects parameter 1 to be double, string given on G:OpenServerdomains	ubauctionincludesfunctions_global.php line 310\n', 0, 0, '127.0.0.1', 1455544109),
(6, 'user', 'List Item', 1, 1, '127.0.0.1', 1455544109),
(7, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>0</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455557850),
(8, 'error', 'Unknown error type: [2] rmdir(G:OpenServerdomains	ubauctionuploaded/9msdf3i7fsq2u29g2907mmt882): Directory not empty on G:OpenServerdomains	ubauctionsell.php line 255\n', 0, 0, '127.0.0.1', 1455558302),
(9, 'user', 'List Item', 1, 1, '127.0.0.1', 1455558302),
(10, 'error', 'Unknown error type: [2] fopen(uploaded/1/b6a0a38eeda9ad3a8e7fd1b8a19c50d5.png): failed to open stream: No such file or directory on G:OpenServerdomains	ubauctiongetthumb.php line 92\n', 0, 0, '127.0.0.1', 1455558307),
(11, 'user', 'List Item', 1, 1, '127.0.0.1', 1455558403),
(12, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>3</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455559765),
(13, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455559783),
(14, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455559865),
(15, 'error', 'Unknown error type: [2] copy(G:OpenServerdomains	ubauctionuploaded/9msdf3i7fsq2u29g2907mmt882/1042970ada1e89d7be0426815b0d735e.png): failed to open stream: No such file or directory on G:OpenServerdomains	ubauctionincludesfunctions_global.php line 206\n', 0, 0, '127.0.0.1', 1455560100),
(16, 'user', 'List Item', 2, 1, '127.0.0.1', 1455560100),
(17, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>4</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455560109),
(18, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455560114),
(19, 'error', 'Unknown error type: [2] copy(G:OpenServerdomains	ubauctionuploaded/9msdf3i7fsq2u29g2907mmt882/1042970ada1e89d7be0426815b0d735e.png): failed to open stream: No such file or directory on G:OpenServerdomains	ubauctionincludesfunctions_global.php line 206\n', 0, 0, '127.0.0.1', 1455560225),
(20, 'user', 'List Item', 2, 1, '127.0.0.1', 1455560225),
(21, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455560230),
(22, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455561058),
(23, 'error', 'Unknown error type: [2] unlink(G:OpenServerdomains	ubauctionuploaded//21042970ada1e89d7be0426815b0d735e (1).png): No such file or directory on G:OpenServerdomains	ubauctionyourauctions_p.php line 49\n', 0, 0, '127.0.0.1', 1455561118),
(24, 'error', 'Unknown error type: [2] unlink(G:OpenServerdomains	ubauctionuploaded//21042970ada1e89d7be0426815b0d735e.png): No such file or directory on G:OpenServerdomains	ubauctionyourauctions_p.php line 49\n', 0, 0, '127.0.0.1', 1455561118),
(25, 'error', 'Unknown error type: [2] rmdir(G:OpenServerdomains	ubauctionuploaded//2): Directory not empty on G:OpenServerdomains	ubauctionyourauctions_p.php line 53\n', 0, 0, '127.0.0.1', 1455561118),
(26, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455561122),
(27, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455561160),
(28, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>4</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455561174),
(29, 'error', 'Unknown error type: [2] copy(G:OpenServerdomains	ubauctionuploaded/9msdf3i7fsq2u29g2907mmt882/f47454d1d3644127f42070181a8b9afc.jpeg): failed to open stream: No such file or directory on G:OpenServerdomains	ubauctionincludesfunctions_global.php line 206\n', 0, 0, '127.0.0.1', 1455572861),
(30, 'user', 'List Item', 2, 1, '127.0.0.1', 1455572861),
(31, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>4</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455572872),
(32, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455572877),
(33, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>5</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455573015),
(34, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455573109),
(35, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455573111),
(36, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455573151),
(37, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455574066),
(38, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455574082),
(39, 'cron', '<br>++++++ Standard auction winners<br><strong>2</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>2</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>4</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>5</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455704136),
(40, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>0</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455704145),
(41, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>0</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455704209),
(42, 'user', 'List Item', 3, 1, '127.0.0.1', 1455704362),
(43, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455704367),
(44, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455706580),
(45, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455726517),
(46, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455726541),
(47, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>1</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1455726600),
(48, 'cron', '<br>++++++ Standard auction winners<br><strong>1</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>1</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>2</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>2</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1456222372),
(49, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>0</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1456222396),
(50, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>0</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1456222421),
(51, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>0</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>0</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1456346352),
(52, 'cron', '<br>++++++ Standard auction winners<br><strong>0</strong> standard auction winners<br><br>++++++ Dutch auctions winners<br><strong>0</strong> dutch auctions winners<br><br>++++++ Relisted auctions<br><strong>0</strong> auctions was relisted<br><br>++++++ Suspended users<br><strong>0</strong> users was suspended<br><br>++++++ Closing expired auctions<br><strong>0</strong> auctions to close<br><br>++++++ Send emails to winners<br><strong>0</strong> winner emails was sent<br><br>++++++ Send emails to seller<br><strong>0</strong> seller emails was sent<br><br>++++++ Delete unpaid items<br><strong>0</strong> unpaid items was deleted<br><br>++++++ Archiving old auctions<br><strong>1</strong> auctions to archive<br><br>++++++ Cumulative emails<br><strong>0</strong> cumulative emails was sent<br><br>++++++ Buyer fee emails<br><strong>0</strong> buyer fee emails was sent<br><br>++++++ Purging the image cache files<br><strong>0</strong> image cache files was purged<br><br>++++++ Deleting old cron logs<br><strong>0</strong> old corn logs was Deleted<br>', 0, 0, '127.0.0.1', 1458234377);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_maintainance`
--

CREATE TABLE IF NOT EXISTS `tubauction_maintainance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` enum('y','n') DEFAULT NULL,
  `superuser` varchar(32) DEFAULT NULL,
  `maintainancetext` text,
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tubauction_maintainance`
--

INSERT INTO `tubauction_maintainance` (`id`, `active`, `superuser`, `maintainancetext`) VALUES
(1, 'n', 'uAuctions', '<br>\r\n<center>\r\n<b>Under maintainance!!!!!!!</b>\r\n</center>');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_membertypes`
--

CREATE TABLE IF NOT EXISTS `tubauction_membertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feedbacks` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `tubauction_membertypes`
--

INSERT INTO `tubauction_membertypes` (`id`, `feedbacks`, `icon`) VALUES
(14, 49, 'starY.gif'),
(15, 99, 'starB.gif'),
(16, 999, 'starT.gif'),
(17, 4999, 'starR.gif'),
(19, 24999, 'starFY.gif'),
(20, 49999, 'starFT.gif'),
(21, 99999, 'starFV.gif'),
(22, 999999, 'starFR.gif'),
(23, 9999, 'starG.gif'),
(24, 9, 'transparent.gif');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_messages`
--

CREATE TABLE IF NOT EXISTS `tubauction_messages` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `sentto` int(25) NOT NULL DEFAULT '0',
  `sentfrom` int(25) NOT NULL DEFAULT '0',
  `fromemail` varchar(50) NOT NULL DEFAULT '',
  `sentat` varchar(20) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `isread` int(1) NOT NULL DEFAULT '0',
  `subject` varchar(50) NOT NULL DEFAULT '',
  `replied` int(1) NOT NULL DEFAULT '0',
  `reply_of` int(50) NOT NULL DEFAULT '0',
  `question` int(15) NOT NULL DEFAULT '0',
  `public` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_mods`
--

CREATE TABLE IF NOT EXISTS `tubauction_mods` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `mod_name` varchar(100) NOT NULL DEFAULT '',
  `backup` varchar(100) NOT NULL DEFAULT '',
  `mod_version` varchar(100) NOT NULL DEFAULT '',
  `downloaded` enum('y','n') NOT NULL DEFAULT 'n',
  `installed` enum('y','n') NOT NULL DEFAULT 'n',
  `install_time` int(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_news`
--

CREATE TABLE IF NOT EXISTS `tubauction_news` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `new_date` int(8) NOT NULL DEFAULT '0',
  `suspended` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_news_translated`
--

CREATE TABLE IF NOT EXISTS `tubauction_news_translated` (
  `id` int(11) NOT NULL DEFAULT '0',
  `lang` char(2) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_online`
--

CREATE TABLE IF NOT EXISTS `tubauction_online` (
  `ID` bigint(21) NOT NULL AUTO_INCREMENT,
  `SESSION` varchar(32) NOT NULL DEFAULT '',
  `time` bigint(21) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `tubauction_online`
--

INSERT INTO `tubauction_online` (`ID`, `SESSION`, `time`) VALUES
(6, 'uId-1', 1458234377);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_pendingnotif`
--

CREATE TABLE IF NOT EXISTS `tubauction_pendingnotif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auction_id` int(11) NOT NULL DEFAULT '0',
  `seller_id` int(11) NOT NULL DEFAULT '0',
  `winners` text NOT NULL,
  `auction` text NOT NULL,
  `seller` text NOT NULL,
  `thisdate` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_proxybid`
--

CREATE TABLE IF NOT EXISTS `tubauction_proxybid` (
  `itemid` int(32) DEFAULT NULL,
  `userid` int(32) DEFAULT NULL,
  `bid` double(16,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_rates`
--

CREATE TABLE IF NOT EXISTS `tubauction_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` tinytext NOT NULL,
  `valuta` tinytext NOT NULL,
  `symbol` char(3) NOT NULL DEFAULT '',
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Дамп данных таблицы `tubauction_rates`
--

INSERT INTO `tubauction_rates` (`id`, `ime`, `valuta`, `symbol`) VALUES
(1, 'Great Britain', 'Pound Sterling ', 'GBP'),
(2, 'Argentina', 'Argentinian Peso', 'ARS'),
(3, 'Australia', 'Australian Dollar ', 'AUD'),
(4, 'Burma', 'Myanmar (Burma) Kyat', 'MMK'),
(5, 'Brazil', 'Brazilian Real ', 'BRL'),
(6, 'Chile', 'Chilean Peso ', 'CLP'),
(7, 'China', 'Chinese Renminbi ', 'CNY'),
(8, 'Colombia', 'Colombian Peso ', 'COP'),
(9, 'Neth. Antilles', 'Neth. Antilles Guilder', 'ANG'),
(10, 'Czech. Republic', 'Czech. Republic Koruna ', 'CZK'),
(11, 'Denmark', 'Danish Krone ', 'DKK'),
(12, 'European Union', 'EURO', 'EUR'),
(13, 'Fiji', 'Fiji Dollar ', 'FJD'),
(14, 'Jamaica', 'Jamaican Dollar', 'JMD'),
(15, 'Trinidad & Tobago', 'Trinidad & Tobago Dollar', 'TTD'),
(16, 'Hong Kong', 'Hong Kong Dollar', 'HKD'),
(17, 'Ghana', 'Ghanaian Cedi', 'GHC'),
(18, 'Iceland', 'Icelandic Krona ', 'INR'),
(19, 'India', 'Indian Rupee', 'INR'),
(20, 'Indonesia', 'Indonesian Rupiah ', 'IDR'),
(21, 'Israel', 'Israeli New Shekel ', 'ILS'),
(22, 'Japan', 'Japanese Yen', 'JPY'),
(23, 'Malaysia', 'Malaysian Ringgit', 'MYR'),
(24, 'Mexico', 'New Peso', 'MXN'),
(25, 'Morocco', 'Moroccan Dirham ', 'MAD'),
(26, 'Honduras', 'Honduras Lempira', 'HNL'),
(27, 'Hungaria', 'Hungarian Forint', 'HUF'),
(28, 'New Zealand', 'New Zealand Dollar', 'NZD'),
(29, 'Norway', 'Norwege Krone', 'NOK'),
(30, 'Pakistan', 'Pakistan Rupee ', 'PKR'),
(31, 'Panama', 'Panamanian Balboa ', 'PAB'),
(32, 'Peru', 'Peruvian New Sol', 'PEN'),
(33, 'Philippine', 'Philippine Peso ', 'PHP'),
(34, 'Poland', 'Polish Zloty', 'PLN'),
(35, 'Russian', 'Russian Rouble', 'RUR'),
(36, 'Singapore', 'Singapore Dollar ', 'SGD'),
(37, 'Slovakia', 'Koruna', 'SKK'),
(38, 'Slovenia', 'Slovenian Tolar', 'SIT'),
(39, 'South Africa', 'South African Rand', 'ZAR'),
(40, 'South Korea', 'South Korean Won', 'KRW'),
(41, 'Sri Lanka', 'Sri Lanka Rupee ', 'LKR'),
(42, 'Sweden', 'Swedish Krona', 'SEK'),
(43, 'Switzerland', 'Swiss Franc', 'CHF'),
(44, 'Taiwan', 'Taiwanese New Dollar ', 'TWD'),
(45, 'Thailand', 'Thailand Thai Baht ', 'THB'),
(46, 'Pacific Financial Community', 'Pacific Financial Community Franc', 'CFP'),
(47, 'Tunisia', 'Tunisisan Dinar', 'TND'),
(48, 'Turkey', 'Turkish Lira', 'TRL'),
(49, 'United States', 'U.S. Dollar', 'USD'),
(50, 'Venezuela', 'Bolivar ', 'VEB'),
(51, 'Bahamas', 'Bahamian Dollar', 'BSD'),
(52, 'Croatia', 'Croatian Kuna', 'HRK'),
(53, 'East Caribe', 'East Caribbean Dollar', 'XCD'),
(54, 'CFA Franc (African Financial Community)', 'African Financial Community Franc', 'CFA'),
(55, 'Canadian', 'Canadian Dollar', 'CAD'),
(56, 'Romanian', 'Romanian Leu', 'RON'),
(64, 'Ukraine', 'U.A. Hryvna', 'HRN'),
(65, 'Ukraine', 'Ukr Hryvna', 'HRN');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_rememberme`
--

CREATE TABLE IF NOT EXISTS `tubauction_rememberme` (
  `userid` int(11) NOT NULL DEFAULT '0',
  `hashkey` char(32) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_report_listing`
--

CREATE TABLE IF NOT EXISTS `tubauction_report_listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reporter_id` int(32) DEFAULT NULL,
  `reporter_nick` varchar(64) NOT NULL DEFAULT '',
  `report_reason` varchar(64) NOT NULL DEFAULT '',
  `report_comment` mediumtext,
  `report_date` int(15) NOT NULL,
  `listing_id` int(32) NOT NULL DEFAULT '0',
  `listing_title` varchar(64) NOT NULL DEFAULT '',
  `seller_id` int(32) DEFAULT NULL,
  `seller_nick` varchar(64) NOT NULL DEFAULT '',
  `report_closed` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_report_reasons`
--

CREATE TABLE IF NOT EXISTS `tubauction_report_reasons` (
  `report_reason` tinytext NOT NULL,
  `report_class` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_report_reasons`
--

INSERT INTO `tubauction_report_reasons` (`report_reason`, `report_class`) VALUES
('Adult material', 3),
('Copyright or Trademark Violation', 3),
('Duplicate listing for one item', 3),
('Firearms, Weapons, or Knives Listed', 3),
('Fraudulent Listing Activities', 3),
('Hazardous Materials', 3),
('Illegal Plant or Animal Listed', 3),
('Inappropriate Category', 3),
('Listing in an Incorrect Category', 3),
('Misleading or Excessive Keywords', 3),
('Offensive Language used in Comments', 3),
('Offensive Language used in Listing', 3),
('Offensive or Violent Material', 3),
('Offering or Requiring Additional Purchases', 3),
('Other Illegal Activity Concerns', 3),
('Other Prohibited or Restricted Items', 3),
('Replica Item listed as Original', 3),
('Stolen Property', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_settings`
--

CREATE TABLE IF NOT EXISTS `tubauction_settings` (
  `sitename` varchar(255) NOT NULL DEFAULT '',
  `siteurl` varchar(255) NOT NULL DEFAULT '',
  `copyright` varchar(255) NOT NULL DEFAULT '',
  `version` varchar(10) NOT NULL DEFAULT '',
  `loginbox` int(1) NOT NULL DEFAULT '0',
  `newsbox` int(1) NOT NULL DEFAULT '0',
  `newstoshow` int(11) NOT NULL DEFAULT '0',
  `helpbox` int(1) NOT NULL DEFAULT '0',
  `perpage` int(10) NOT NULL DEFAULT '15',
  `moneyformat` int(1) NOT NULL DEFAULT '0',
  `moneydecimals` int(11) NOT NULL DEFAULT '2',
  `moneysymbol` int(1) NOT NULL DEFAULT '0',
  `currency` varchar(10) NOT NULL DEFAULT '',
  `adminmail` varchar(100) NOT NULL DEFAULT '',
  `banners` int(1) NOT NULL DEFAULT '0',
  `newsletter` int(1) NOT NULL DEFAULT '0',
  `logo` varchar(255) NOT NULL DEFAULT '',
  `timecorrection` int(3) NOT NULL DEFAULT '0',
  `cron` int(1) NOT NULL DEFAULT '0',
  `archiveafter` int(11) NOT NULL DEFAULT '0',
  `datesformat` enum('USA','EUR') NOT NULL DEFAULT 'EUR',
  `errortext` text NOT NULL,
  `picturesgallery` int(1) NOT NULL DEFAULT '0',
  `maxpictures` int(11) NOT NULL DEFAULT '0',
  `buy_now` int(1) NOT NULL DEFAULT '1',
  `thumb_show` smallint(6) NOT NULL DEFAULT '120',
  `thumb_list` smallint(6) NOT NULL DEFAULT '120',
  `lastitemsnumber` int(11) NOT NULL DEFAULT '0',
  `hotitemsnumber` int(11) NOT NULL DEFAULT '0',
  `endingsoonnumber` int(11) NOT NULL DEFAULT '0',
  `boards` enum('y','n') NOT NULL DEFAULT 'y',
  `wordsfilter` enum('y','n') NOT NULL DEFAULT 'y',
  `aboutus` enum('y','n') NOT NULL DEFAULT 'y',
  `aboutustext` text NOT NULL,
  `terms` enum('y','n') NOT NULL DEFAULT 'y',
  `termstext` text NOT NULL,
  `privacypolicy` enum('y','n') NOT NULL DEFAULT 'y',
  `privacypolicytext` text NOT NULL,
  `defaultcountry` varchar(30) NOT NULL DEFAULT '217',
  `defaultlanguage` char(2) NOT NULL DEFAULT 'EN',
  `catsorting` enum('alpha','counter') NOT NULL DEFAULT 'alpha',
  `usersauth` enum('y','n') NOT NULL DEFAULT 'y',
  `descriptiontag` text NOT NULL,
  `keywordstag` text NOT NULL,
  `maxuploadsize` int(11) NOT NULL DEFAULT '0',
  `contactseller` enum('always','logged','never') NOT NULL DEFAULT 'always',
  `theme` tinytext,
  `catstoshow` int(11) NOT NULL DEFAULT '0',
  `bn_only` enum('y','n') NOT NULL DEFAULT 'n',
  `users_email` enum('y','n') NOT NULL DEFAULT 'y',
  `boardsmsgs` int(11) NOT NULL DEFAULT '0',
  `activationtype` int(1) NOT NULL DEFAULT '1',
  `https` enum('y','n') NOT NULL DEFAULT 'n',
  `https_url` varchar(255) NOT NULL DEFAULT '',
  `bn_only_disable` enum('y','n') NOT NULL DEFAULT 'n',
  `bn_only_percent` int(3) NOT NULL DEFAULT '50',
  `buyerprivacy` enum('y','n') NOT NULL DEFAULT 'n',
  `cust_increment` int(1) NOT NULL DEFAULT '1',
  `subtitle` enum('y','n') NOT NULL DEFAULT 'y',
  `extra_cat` enum('y','n') NOT NULL DEFAULT 'n',
  `fees` enum('y','n') NOT NULL DEFAULT 'n',
  `fee_type` int(1) NOT NULL DEFAULT '1',
  `fee_max_debt` double(16,2) NOT NULL DEFAULT '25.00',
  `fee_signup_bonus` double(16,2) NOT NULL DEFAULT '0.00',
  `fee_disable_acc` enum('y','n') NOT NULL DEFAULT 'y',
  `tax` enum('y','n') NOT NULL DEFAULT 'n',
  `taxuser` enum('y','n') NOT NULL DEFAULT 'n',
  `ae_status` enum('y','n') NOT NULL DEFAULT 'n',
  `ae_timebefore` int(11) NOT NULL DEFAULT '120',
  `ae_extend` int(11) NOT NULL DEFAULT '300',
  `cache_theme` enum('y','n') NOT NULL DEFAULT 'y',
  `hours_countdown` int(5) NOT NULL DEFAULT '24',
  `edit_starttime` int(1) NOT NULL DEFAULT '1',
  `banner_width` int(11) NOT NULL DEFAULT '468',
  `banner_height` int(11) NOT NULL DEFAULT '60',
  `banner_types` varchar(75) NOT NULL DEFAULT '',
  `counter_auctions` enum('y','n') NOT NULL DEFAULT 'y',
  `counter_users` enum('y','n') NOT NULL DEFAULT 'y',
  `counter_online` enum('y','n') NOT NULL DEFAULT 'y',
  `banemail` text NOT NULL,
  `mandatory_fields` varchar(255) NOT NULL DEFAULT '',
  `displayed_feilds` varchar(255) NOT NULL DEFAULT '',
  `ao_hpf_enabled` enum('y','n') NOT NULL DEFAULT 'y',
  `ao_hi_enabled` enum('y','n') NOT NULL DEFAULT 'y',
  `ao_bi_enabled` enum('y','n') NOT NULL DEFAULT 'y',
  `proxy_bidding` enum('y','n') NOT NULL DEFAULT 'y',
  `recaptcha_public` varchar(40) NOT NULL DEFAULT '',
  `recaptcha_private` varchar(40) NOT NULL DEFAULT '',
  `spam_sendtofriend` int(1) NOT NULL DEFAULT '1',
  `spam_register` int(1) NOT NULL DEFAULT '1',
  `mod_queue` enum('y','n') NOT NULL DEFAULT 'n',
  `payment_options` text NOT NULL,
  `autorelist` enum('y','n') NOT NULL DEFAULT 'y',
  `autorelist_max` int(3) NOT NULL DEFAULT '10',
  `invoice_yellow_line` varchar(255) NOT NULL DEFAULT '',
  `invoice_thankyou` varchar(255) NOT NULL DEFAULT '',
  `dites_upload_size` int(11) NOT NULL DEFAULT '0',
  `di_auctions` enum('y','n') NOT NULL DEFAULT 'n',
  `facebook_login` enum('y','n') NOT NULL DEFAULT 'n',
  `facebook_app_id` varchar(100) NOT NULL DEFAULT '',
  `facebook_app_secret` varchar(100) NOT NULL DEFAULT '',
  `admin_folder` varchar(100) NOT NULL DEFAULT '',
  `cookiespolicy` enum('y','n') NOT NULL DEFAULT 'y',
  `cookiespolicytext` text NOT NULL,
  `counter_users_online` enum('y','n') NOT NULL DEFAULT 'y',
  `item_conditions` enum('y','n') NOT NULL DEFAULT 'y',
  `paypal_sandbox` enum('y','n') NOT NULL DEFAULT 'n',
  `authnet_sandbox` enum('y','n') NOT NULL DEFAULT 'n',
  `worldpay_sandbox` enum('y','n') NOT NULL DEFAULT 'n',
  `skrill_sandbox` enum('y','n') NOT NULL DEFAULT 'n',
  `checkout_sandbox` enum('y','n') NOT NULL DEFAULT 'n',
  `dutch_auctions` enum('y','n') NOT NULL DEFAULT 'y',
  `expire_unpaid` int(3) NOT NULL DEFAULT '0',
  `cookie_name` varchar(150) NOT NULL DEFAULT '',
  `sessions_name` varchar(150) NOT NULL DEFAULT '',
  `custom_code` varchar(150) NOT NULL DEFAULT '',
  `cat_counters` enum('y','n') NOT NULL DEFAULT 'y',
  `disposable_email_block` enum('y','n') NOT NULL DEFAULT 'y',
  `counter_sold_items` enum('y','n') NOT NULL DEFAULT 'y',
  `mail_protocol` int(1) NOT NULL DEFAULT '0',
  `mail_parameter` varchar(128) NOT NULL,
  `smtp_authentication` enum('y','n') NOT NULL DEFAULT 'n',
  `smtp_security` enum('none','tls','ssl') NOT NULL DEFAULT 'none',
  `smtp_port` int(10) NOT NULL DEFAULT '25',
  `smtp_host` varchar(128) NOT NULL,
  `smtp_username` varchar(128) NOT NULL,
  `smtp_password` varchar(128) NOT NULL,
  `alert_emails` varchar(128) NOT NULL,
  `cronlog` enum('y','n') NOT NULL DEFAULT 'y',
  `encryption_key` varchar(32) NOT NULL,
  `cookies_directive` enum('y','n') NOT NULL DEFAULT 'y',
  `minimum_username_length` int(2) NOT NULL DEFAULT '6',
  `minimum_password_length` int(2) NOT NULL DEFAULT '6',
  `image_captcha_characters_length` int(2) NOT NULL DEFAULT '7',
  `image_captcha_width` int(3) NOT NULL DEFAULT '230',
  `max_image_width` int(5) NOT NULL DEFAULT '1000',
  `max_image_height` int(5) NOT NULL DEFAULT '800',
  `daylight_savings` enum('y','n') NOT NULL DEFAULT 'y',
  `standard_auctions` enum('y','n') NOT NULL DEFAULT 'y',
  `auction_setup_types` enum('0','1','2') NOT NULL DEFAULT '1',
  `freemaxpictures` int(11) NOT NULL DEFAULT '5',
  `shipping_conditions` enum('y','n') NOT NULL DEFAULT 'y',
  `shipping_terms` enum('y','n') NOT NULL DEFAULT 'y',
  `google_analytics` varchar(25) NOT NULL DEFAULT '',
  `watermark` varchar(255) NOT NULL DEFAULT '',
  `watermark_active` enum('y','n') NOT NULL DEFAULT 'n',
  `featureditemsnumber` int(3) NOT NULL DEFAULT '8',
  `donated` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_settings`
--

INSERT INTO `tubauction_settings` (`sitename`, `siteurl`, `copyright`, `version`, `loginbox`, `newsbox`, `newstoshow`, `helpbox`, `perpage`, `moneyformat`, `moneydecimals`, `moneysymbol`, `currency`, `adminmail`, `banners`, `newsletter`, `logo`, `timecorrection`, `cron`, `archiveafter`, `datesformat`, `errortext`, `picturesgallery`, `maxpictures`, `buy_now`, `thumb_show`, `thumb_list`, `lastitemsnumber`, `hotitemsnumber`, `endingsoonnumber`, `boards`, `wordsfilter`, `aboutus`, `aboutustext`, `terms`, `termstext`, `privacypolicy`, `privacypolicytext`, `defaultcountry`, `defaultlanguage`, `catsorting`, `usersauth`, `descriptiontag`, `keywordstag`, `maxuploadsize`, `contactseller`, `theme`, `catstoshow`, `bn_only`, `users_email`, `boardsmsgs`, `activationtype`, `https`, `https_url`, `bn_only_disable`, `bn_only_percent`, `buyerprivacy`, `cust_increment`, `subtitle`, `extra_cat`, `fees`, `fee_type`, `fee_max_debt`, `fee_signup_bonus`, `fee_disable_acc`, `tax`, `taxuser`, `ae_status`, `ae_timebefore`, `ae_extend`, `cache_theme`, `hours_countdown`, `edit_starttime`, `banner_width`, `banner_height`, `banner_types`, `counter_auctions`, `counter_users`, `counter_online`, `banemail`, `mandatory_fields`, `displayed_feilds`, `ao_hpf_enabled`, `ao_hi_enabled`, `ao_bi_enabled`, `proxy_bidding`, `recaptcha_public`, `recaptcha_private`, `spam_sendtofriend`, `spam_register`, `mod_queue`, `payment_options`, `autorelist`, `autorelist_max`, `invoice_yellow_line`, `invoice_thankyou`, `dites_upload_size`, `di_auctions`, `facebook_login`, `facebook_app_id`, `facebook_app_secret`, `admin_folder`, `cookiespolicy`, `cookiespolicytext`, `counter_users_online`, `item_conditions`, `paypal_sandbox`, `authnet_sandbox`, `worldpay_sandbox`, `skrill_sandbox`, `checkout_sandbox`, `dutch_auctions`, `expire_unpaid`, `cookie_name`, `sessions_name`, `custom_code`, `cat_counters`, `disposable_email_block`, `counter_sold_items`, `mail_protocol`, `mail_parameter`, `smtp_authentication`, `smtp_security`, `smtp_port`, `smtp_host`, `smtp_username`, `smtp_password`, `alert_emails`, `cronlog`, `encryption_key`, `cookies_directive`, `minimum_username_length`, `minimum_password_length`, `image_captcha_characters_length`, `image_captcha_width`, `max_image_width`, `max_image_height`, `daylight_savings`, `standard_auctions`, `auction_setup_types`, `freemaxpictures`, `shipping_conditions`, `shipping_terms`, `google_analytics`, `watermark`, `watermark_active`, `featureditemsnumber`, `donated`) VALUES
('TubAuctions', 'tubauction/', '', '5.3.9', 1, 1, 5, 1, 15, 1, 2, 2, 'HRN', 'vilgelmo39@gmail.com', 1, 1, 'logo.png', 1, 2, 30, 'EUR', 'An unexpected error occurred. The error has been forwarded to our technical team and will be fixed shortly', 1, 5, 2, 120, 120, 8, 8, 0, 'y', 'y', 'y', '', 'y', '', 'y', '', '217', 'EN', 'alpha', 'y', '', '', 10485760, 'always', 'default', 20, 'n', 'y', 0, 1, 'n', '', 'n', 50, 'n', 1, 'y', 'n', 'n', 1, 25.00, 0.00, 'y', 'n', 'n', 'n', 120, 300, 'y', 24, 1, 468, 60, 'gif, jpg, jpeg, png, swf', 'y', 'y', 'y', '', 'a:7:{s:9:"birthdate";s:1:"y";s:7:"address";s:1:"y";s:4:"city";s:1:"y";s:4:"prov";s:1:"y";s:7:"country";s:1:"y";s:3:"zip";s:1:"y";s:3:"tel";s:1:"y";}', 'a:7:{s:17:"birthdate_regshow";s:1:"y";s:15:"address_regshow";s:1:"y";s:12:"city_regshow";s:1:"y";s:12:"prov_regshow";s:1:"y";s:15:"country_regshow";s:1:"y";s:11:"zip_regshow";s:1:"y";s:11:"tel_regshow";s:1:"y";}', 'y', 'y', 'y', 'y', '', '', 1, 1, 'n', 'a:2:{i:0;s:13:"Wire Transfer";i:1;s:6:"Cheque";}', 'y', 10, '', 'Thank you for shopping with us and we hope to see you return soon!', 0, 'n', 'n', '', '', 'admin', 'y', '', 'y', 'y', 'n', 'n', 'n', 'n', 'n', 'y', 60, 'uAuctions', 'uAuctions', 'uAuctions', 'y', 'y', 'y', 0, '', 'n', 'none', 25, '', '', '', '', 'y', '14b55b29f74d9f0782152e1cb6a9c113', 'y', 6, 6, 7, 230, 1000, 800, 'y', 'y', '0', 5, 'y', 'y', '', '', 'n', 8, '');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_statssettings`
--

CREATE TABLE IF NOT EXISTS `tubauction_statssettings` (
  `activate` enum('y','n') NOT NULL DEFAULT 'y',
  `accesses` enum('y','n') NOT NULL DEFAULT 'y',
  `browsers` enum('y','n') NOT NULL DEFAULT 'y',
  `domains` enum('y','n') NOT NULL DEFAULT 'y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tubauction_statssettings`
--

INSERT INTO `tubauction_statssettings` (`activate`, `accesses`, `browsers`, `domains`) VALUES
('n', 'y', 'y', 'y');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_support`
--

CREATE TABLE IF NOT EXISTS `tubauction_support` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `user` int(128) NOT NULL,
  `title` tinytext,
  `ticket_id` varchar(250) NOT NULL DEFAULT '',
  `status` enum('open','close') NOT NULL DEFAULT 'open',
  `last_reply_user` int(150) NOT NULL,
  `last_reply_time` int(15) NOT NULL,
  `created_time` int(15) NOT NULL,
  `ticket_reply_status` enum('user','support') NOT NULL DEFAULT 'support',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_support_messages`
--

CREATE TABLE IF NOT EXISTS `tubauction_support_messages` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `sentto` int(25) NOT NULL DEFAULT '0',
  `sentfrom` int(25) NOT NULL DEFAULT '0',
  `fromemail` varchar(50) NOT NULL DEFAULT '',
  `sentat` varchar(20) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `subject` varchar(50) NOT NULL DEFAULT '',
  `reply_of` int(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_tax`
--

CREATE TABLE IF NOT EXISTS `tubauction_tax` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(30) NOT NULL,
  `tax_rate` double(16,2) NOT NULL,
  `countries_seller` text NOT NULL,
  `countries_buyer` text NOT NULL,
  `fee_tax` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tubauction_tax`
--

INSERT INTO `tubauction_tax` (`id`, `tax_name`, `tax_rate`, `countries_seller`, `countries_buyer`, `fee_tax`) VALUES
(1, 'Site Fees', 0.00, '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_useraccounts`
--

CREATE TABLE IF NOT EXISTS `tubauction_useraccounts` (
  `useracc_id` int(11) NOT NULL AUTO_INCREMENT,
  `auc_id` int(15) NOT NULL DEFAULT '0',
  `user_id` int(15) NOT NULL DEFAULT '0',
  `date` int(15) NOT NULL DEFAULT '0',
  `setup` double(8,2) NOT NULL DEFAULT '0.00',
  `featured` double(8,2) NOT NULL DEFAULT '0.00',
  `bold` double(8,2) NOT NULL DEFAULT '0.00',
  `highlighted` double(8,2) NOT NULL DEFAULT '0.00',
  `subtitle` double(8,2) NOT NULL DEFAULT '0.00',
  `relist` double(8,2) NOT NULL DEFAULT '0.00',
  `reserve` double(8,2) NOT NULL DEFAULT '0.00',
  `buynow` double(8,2) NOT NULL DEFAULT '0.00',
  `image` double(8,2) NOT NULL DEFAULT '0.00',
  `extcat` double(8,2) NOT NULL DEFAULT '0.00',
  `signup` double(8,2) NOT NULL DEFAULT '0.00',
  `buyer` double(8,2) NOT NULL DEFAULT '0.00',
  `finalval` double(8,2) NOT NULL DEFAULT '0.00',
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `total` double(8,2) NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`useracc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_users`
--

CREATE TABLE IF NOT EXISTS `tubauction_users` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `nick` varchar(20) NOT NULL,
  `password` varchar(255) DEFAULT '',
  `hash` varchar(5) DEFAULT '',
  `name` tinytext,
  `address` tinytext,
  `city` varchar(25) DEFAULT '',
  `prov` varchar(20) DEFAULT '',
  `country` varchar(30) DEFAULT '',
  `zip` varchar(10) DEFAULT '',
  `phone` varchar(40) DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `reg_date` int(15) DEFAULT NULL,
  `rate_sum` int(11) NOT NULL DEFAULT '0',
  `rate_num` int(11) NOT NULL DEFAULT '0',
  `birthdate` int(8) DEFAULT '0',
  `suspended` int(1) DEFAULT '0',
  `nletter` int(1) NOT NULL DEFAULT '0',
  `balance` double(16,2) NOT NULL DEFAULT '0.00',
  `auc_watch` text,
  `item_watch` text,
  `endemailmode` enum('one','cum','none') NOT NULL DEFAULT 'one',
  `startemailmode` enum('yes','no') NOT NULL DEFAULT 'yes',
  `emailtype` enum('html','text') NOT NULL DEFAULT 'html',
  `lastlogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_details` text,
  `groups` text,
  `bn_only` enum('y','n') NOT NULL DEFAULT 'y',
  `timecorrection` int(3) NOT NULL DEFAULT '0',
  `paypal_email` varchar(50) DEFAULT '',
  `authnet_id` varchar(50) DEFAULT '',
  `authnet_pass` varchar(50) DEFAULT '',
  `worldpay_id` varchar(50) DEFAULT '',
  `skrill_email` varchar(50) DEFAULT '',
  `toocheckout_id` varchar(50) DEFAULT '',
  `language` char(2) NOT NULL DEFAULT '',
  `fblogin_id` varchar(100) NOT NULL DEFAULT '',
  `avatar` varchar(250) NOT NULL DEFAULT '',
  `is_online` int(10) NOT NULL DEFAULT '0',
  `hide_online` enum('y','n') NOT NULL DEFAULT 'n',
  `bank_name` varchar(50) NOT NULL DEFAULT '',
  `bank_account` varchar(50) NOT NULL DEFAULT '',
  `bank_routing` varchar(50) NOT NULL DEFAULT '',
  `admin` int(1) NOT NULL DEFAULT '0',
  `payment_reminder_sent` enum('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tubauction_users`
--

INSERT INTO `tubauction_users` (`id`, `nick`, `password`, `hash`, `name`, `address`, `city`, `prov`, `country`, `zip`, `phone`, `email`, `reg_date`, `rate_sum`, `rate_num`, `birthdate`, `suspended`, `nletter`, `balance`, `auc_watch`, `item_watch`, `endemailmode`, `startemailmode`, `emailtype`, `lastlogin`, `payment_details`, `groups`, `bn_only`, `timecorrection`, `paypal_email`, `authnet_id`, `authnet_pass`, `worldpay_id`, `skrill_email`, `toocheckout_id`, `language`, `fblogin_id`, `avatar`, `is_online`, `hide_online`, `bank_name`, `bank_account`, `bank_routing`, `admin`, `payment_reminder_sent`) VALUES
(1, 'usertubauction', '$2a$08$LF1GENCbgWQzsjpqX5SKI.oUyWjw4VLyPwYZEVvWpHyXxv1HQ8dcO', 'j1mfv', 'usertubauction', NULL, '', '', '', '', '', '', 1455539593, 0, 0, 0, 0, 1, 0.00, NULL, NULL, 'one', 'yes', 'html', '2016-02-17 09:16:49', NULL, '1,2', 'y', 0, '', '', '', '', '', '', 'EN', '', '', 1458234677, 'n', '', '', '', 2, 'n');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_usersips`
--

CREATE TABLE IF NOT EXISTS `tubauction_usersips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(32) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `type` enum('first','after') NOT NULL DEFAULT 'first',
  `action` enum('accept','deny') NOT NULL DEFAULT 'accept',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tubauction_usersips`
--

INSERT INTO `tubauction_usersips` (`id`, `user`, `ip`, `type`, `action`) VALUES
(1, 1, '127.0.0.1', 'after', 'accept');

-- --------------------------------------------------------

--
-- Структура таблицы `tubauction_winners`
--

CREATE TABLE IF NOT EXISTS `tubauction_winners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auction` int(32) NOT NULL DEFAULT '0',
  `seller` int(32) NOT NULL DEFAULT '0',
  `winner` int(32) NOT NULL DEFAULT '0',
  `bid` double(16,2) NOT NULL DEFAULT '0.00',
  `closingdate` int(15) NOT NULL DEFAULT '0',
  `feedback_win` tinyint(1) NOT NULL DEFAULT '0',
  `feedback_sel` tinyint(1) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '1',
  `paid` int(1) NOT NULL DEFAULT '0',
  `bf_paid` int(1) NOT NULL DEFAULT '0',
  `ff_paid` int(1) NOT NULL DEFAULT '1',
  `shipped` int(1) NOT NULL DEFAULT '0',
  `is_read` int(1) NOT NULL DEFAULT '0',
  `is_counted` enum('y','n') NOT NULL DEFAULT 'n',
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
