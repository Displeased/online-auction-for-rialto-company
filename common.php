<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

session_start();

//Logging errors
$error_reporting = E_ALL^E_NOTICE;
//$error_reporting = E_ALL; // use this for debugging

define('InuAuctions', 1);
define('TrackUserIPs', 1);

// install folder check
if (!isset($_GET['step']))
{
	if (is_dir('install'))
	{
		if ($dir = opendir('install'))
		{
			while (($install_file = readdir($dir)) !== false)
			{
				if (!is_dir($dir . '/' . $install_file))
				{
					$install_php = 'install/install.php';
					$install_path = (!defined('InAdmin')) ? $install_php : $install_php;
					header('location: ' . $install_path);
					exit;
				}
			}
			closedir($install_file);
		}
	}
	else
	{
		include 'includes/config.inc.php';
	}
}
else //no install folder found run the config.inc.php page
{
	include 'includes/config.inc.php';
}

$MD5_PREFIX = (!isset($MD5_PREFIX)) ? 'fhQYBpS5FNs4' : $MD5_PREFIX; // if the user didnt set a code

//Directory paths on the server
$include_path = $main_path . 'includes/'; 
$uploaded_path = 'uploaded/';
$upload_path = $main_path . $uploaded_path;

include $main_path . 'includes/functions_timecorrections.php';

//load all the pages and classes that is needed to run the script
include $include_path . 'errors.inc.php'; //error handler functions
include $include_path . 'dates.inc.php'; //date/time handler functions

// SQL handler, class and connect to db
include $include_path . 'class_db_handle.php';
$db = new db_handle();
$db->connect($DbHost, $DbUser, $DbPassword, $DbDatabase, $DBPrefix, $CHARSET); // connect to the database


include $include_path . 'functions_global.php';
$system = new global_class();
$security = new security();

include $include_path . 'messages.inc.php'; //build the language messages

//Email pages and class
include $include_path . 'class_email_handler.php'; //email handler functions
include $include_path . 'class_send_email.php'; //send emails functions
$send_email = new send_email();

include $include_path . 'class_MPTTcategories.php'; //categories handler functions
include $include_path . 'class_fees.php'; //fees handler functions

include $include_path . 'class_user.php'; //user handler functions
$user = new user();

//template handler and class
include $include_path . 'template.php'; //build the website template handler functions
$template = new template();

//password handler and class
include $include_path . 'phpass.php'; //password handler functions
$phpass = new PasswordHash(8, false);

include $include_path . 'seo-core.php'; //SEO website links handler functions

set_error_handler('uAuctionsErrorHandler', $error_reporting); //error function for all errors to be submitted to db

// add auction types
if ($system->SETTINGS['di_auctions'] == 'n' && $system->SETTINGS['dutch_auctions'] == 'n' && $system->SETTINGS['standard_auctions'] == 'y')
{
	//only standard auction
	$system->SETTINGS['auction_types'] = array (
		1 => $MSG['1021']
	);
}
elseif ($system->SETTINGS['di_auctions'] == 'n' && $system->SETTINGS['dutch_auctions'] == 'y' && $system->SETTINGS['standard_auctions'] == 'y')
{
	//only standard auction and dutch auction
	$system->SETTINGS['auction_types'] = array (
		1 => $MSG['1021'],
		2 => $MSG['1020']
	);
}
elseif ($system->SETTINGS['di_auctions'] == 'y' && $system->SETTINGS['dutch_auctions'] == 'n' && $system->SETTINGS['standard_auctions'] == 'y')
{
	//only standard auction and digital item auction
	$system->SETTINGS['auction_types'] = array (
		1 => $MSG['1021'],
		3 => $MSG['350_1010']
	);
}
elseif ($system->SETTINGS['di_auctions'] == 'n' && $system->SETTINGS['dutch_auctions'] == 'y' && $system->SETTINGS['standard_auctions'] == 'n')
{
	//only dutch auction
	$system->SETTINGS['auction_types'] = array (
		2 => $MSG['1020']
	);
}
elseif ($system->SETTINGS['di_auctions'] == 'y' && $system->SETTINGS['dutch_auctions'] == 'y' && $system->SETTINGS['standard_auctions'] == 'n')
{
	//only dutch auction and digital item auction
	$system->SETTINGS['auction_types'] = array (
		2 => $MSG['1020'],
		3 => $MSG['350_1010']
	);
}
elseif ($system->SETTINGS['di_auctions'] == 'y' && $system->SETTINGS['dutch_auctions'] == 'n' && $system->SETTINGS['standard_auctions'] == 'n')
{
	//only digital item auction
	$system->SETTINGS['auction_types'] = array (
		3 => $MSG['350_1010']
	);
}
elseif ($system->SETTINGS['di_auctions'] == 'y' && $system->SETTINGS['dutch_auctions'] == 'y' && $system->SETTINGS['standard_auctions'] == 'y')
{
	//all 3 auctions
	$system->SETTINGS['auction_types'] = array (
		1 => $MSG['1021'],
		2 => $MSG['1020'],
		3 => $MSG['350_1010']
	);
}


// Atuomatically login user is necessary "Remember me" option
if (!$user->logged_in && isset($_COOKIE[$system->SETTINGS['cookie_name'] . '-RM_ID']))
{
	$query = "SELECT userid FROM " . $DBPrefix . "rememberme WHERE hashkey = :RM_ID";
	$params = array();
	$params[] = array(':RM_ID', alphanumeric($_COOKIE[$system->SETTINGS['cookie_name'] . '-RM_ID']), 'str');
	$db->query($query, $params);
	if ($db->numrows() > 0)
	{
		// generate a random unguessable token
		$id = $db->result('userid');
		$query = "SELECT hash, password FROM " . $DBPrefix . "users WHERE id = :user_id";
		$params = array();
		$params[] = array(':user_id', $id, 'int');
		$db->query($query, $params);
		$user_data = $db->result();
		$_SESSION['csrftoken'] = $security->encrypt($user_data['id'] . '-' . $user_data['hash'] . '-' . $system->ctime);
		$_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_IN'] 		= $security->encrypt($id);
		$_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_NUMBER'] 	= $security->encrypt(strspn($user_data['password'], $user_data['hash']));
		$_SESSION[$system->SETTINGS['sessions_name'] . '_LOGGED_PASS'] 		= $security->encrypt($user_data['password']);
		
	}
}
$donated = isset($_POST['donated']) ? $_POST['donated'] : '';
if(isset($donated) && !empty($donated))
{
	$query = "UPDATE " . $DBPrefix . "settings SET donated = :donate_check";
	$params = array();
	$params[] = array(':donate_check', stripslashes($system->cleanvars($donated)), 'str');
	$db->query($query, $params);
}

$template->set_template();