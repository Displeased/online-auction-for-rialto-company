<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $main_path . 'inc/ckeditor/ckeditor.php';
// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'support';
	header('location: user_login.php');
	exit;
}

$messageid = $_GET['x'];

if(isset($_POST['message']) && $_POST['reply'] == 'reply_back' && isset($messageid))
{
	$subject = isset($_POST['subject']) ? stripslashes($system->cleanvars($_POST['subject'])) : '';
	$nowmessage = isset($_POST['message']) ? stripslashes($system->cleanvars($_POST['message'])) : '';

	$_SESSION['reply_message'] = $subject;
	$_SESSION['reply_subject'] = $nowmessage;

	// submit the message to DB and linking the ticket id
	$query = "INSERT INTO " . $DBPrefix . "support_messages VALUES (NULL, 0, :sender_id, :from_email, :times, :nowmessages, :subjects, :replayof)";
	$params = array();
	$params[] = array(':sender_id', $user->user_data['id'], 'int');
	$params[] = array(':from_email', $user->user_data['email'], 'str');
	$params[] = array(':times', $system->ctime, 'str');
	$params[] = array(':nowmessages', $nowmessage, 'str');
	$params[] = array(':subjects', $subject, 'str');
	$params[] = array(':replayof', $messageid, 'str');
	$db->query($query, $params);
	if($db->lastInsertId() > 0)
	{
		$query = "UPDATE " . $DBPrefix . "support SET last_reply_time = :update_time, ticket_reply_status = :set_status, last_reply_user = :set_user WHERE ticket_id = :id AND user = :user";
		$params = array();
		$params[] = array(':update_time', $system->ctime, 'int');
		$params[] = array(':set_status', 'support', 'bool');
		$params[] = array(':set_user', $user->user_data['id'], 'int');
		$params[] = array(':user', $user->user_data['id'], 'int');
		$params[] = array(':id', $messageid, 'int');
		$db->query($query, $params);

		// send the email
		$send_email->reply_to_ticket($subject, $system->uncleanvars($nowmessage), $user->user_data['nick'], $system->SETTINGS['adminmail']);
		
		//deteling the sessions
		$_SESSION['reply_message'] = '';
		$_SESSION['reply_subject'] = '';
		$ERR = $MSG['3500_1015439n'];
	}
}

$CKEditor = new CKEditor();
$CKEditor->basePath = $system->SETTINGS['siteurl'] . 'inc/ckeditor/';
$CKEditor->returnOutput = true;

// check message is to user
$query = "SELECT * FROM " . $DBPrefix . "support_messages WHERE reply_of = :ticket_id ORDER BY sentat DESC";
$params = array();
$params[] = array(':ticket_id', $messageid, 'str');
$db->query($query, $params);
$messages = $db->numrows();

if ($messages == 0)
{
	$_SESSION['err_message'] = $MSG['3500_1015439m'];
	header('location: ' . $system->SETTINGS['siteurl'] . 'support');
}
	
while ($array = $db->result())
{
	$_SESSION['reply_subject'] = $array['subject'];
	$sentat = $array['sentat'];
	$mth = 'MON_0' . gmdate('m', $sentat);
	if($system->SETTINGS['datesformat'] == 'EUR')
	{
		$sent_time =  gmdate('j', $sentat) . ' ' . $MSG[$mth] . ' ' . gmdate('Y', $sentat) . ' ' . gmdate('H:i:s', $sentat);
	}
	else
	{
		$sent_time = $MSG[$mth] . ' ' . gmdate('j,Y', $sentat) . ' ' . gmdate('H:i:s', $sentat);
	}
	
	$check_user = $array['sentfrom'] == 0 ? $MSG['3500_1015436'] :  $user->user_data['nick'];
	$template->assign_block_vars('ticket_mess', array(
		'LAST_UPDATED_TIME' => $sent_time, //when the ticket was updated
		'TICKET_ID' => $array['reply_of'],
		'LAST_USER' => $check_user,
		'TICKET_MESSAGE' => $system->uncleanvars($array['message']),
		'CREATED' => $sent_time, //time that the ticket was created
		'TICKET_STATUS' => $array['status'] == 'open' ? true : false, //ticket is open or closed
	));
}	

$query = "SELECT t.*, u.nick FROM " . $DBPrefix . "support t
	LEFT JOIN " . $DBPrefix . "users u ON (u.id = t.user)
	WHERE t.ticket_id = :id";
// get users messages
$params = array();
$params[] = array(':id', $messageid, 'int');
$db->query($query, $params);

if($db->numrows() > 0)
{
	$array = $db->result();
	// formatting the created time
	$created_time = $array['created_time'];
	$mth = 'MON_0' . gmdate('m', $created_time);
	if($system->SETTINGS['datesformat'] == 'EUR')
	{
		$created =  gmdate('j', $created_time) . ' ' . $MSG[$mth] . ' ' . gmdate('Y', $created_time) . ' ' . gmdate('H:i:s', $created_time);
	}
	else
	{
		$created = $MSG[$mth] . ' ' . gmdate('j,Y', $created_time) . ' ' . gmdate('H:i:s', $created_time);;
	}
	
	$last_reply_time = $array['last_reply_time'];
	$mth = 'MON_0' . gmdate('m', $last_reply_time);
	if($system->SETTINGS['datesformat'] == 'EUR')
	{
		$last_reply =  gmdate('j', $last_reply_time) . ' ' . $MSG[$mth] . ' ' . gmdate('Y', $last_reply_time) . ' ' . gmdate('H:i:s', $last_reply_time);
	}
	else
	{
		$last_reply = $MSG[$mth] . ' ' . gmdate('j,Y', $last_reply_time) . ' ' . gmdate('H:i:s', $last_reply_time);
	}
	
	$template->assign_block_vars('ticket', array(
		'LAST_UPDATED_TIME' => $last_reply, //when the ticket was updated
		'TICKET_ID' => $array['ticket_id'],
		'LAST_UPDATE_USER' => $array['ticket_reply_status'] == 'user' ? $array['nick'] : $MSG['3500_1015436'],
		'TICKET_TITLE' => ($array['ticket_reply_status'] == 'user' && $array['status'] == 'open') ? '<b>' . $array['title'] . '</b>' : $array['title'],
		'CREATED' => $created, //time that the ticket was created
		'TICKET_STATUS' => $array['status'] == 'open' ? true : false, //ticket is open or closed
	));
}

$check_mess = (isset($_SESSION['reply_message'])) ? $system->uncleanvars($_SESSION['reply_message']) : '';
$template->assign_vars(array(
	'ERROR' => isset($ERR) ? $ERR : '',
	'B_ISERROR' => isset($ERR) ? true : false,
	'MSGCOUNT' => $messages,
	'B_OPEN' => $array['status'] == 'open' ? true : false,
	'ID' => $messageid,
	'SUBJECT' => (isset($_SESSION['reply_subject'])) ? $_SESSION['reply_subject'] : '',
	'MESSAGE' => $CKEditor->editor('message', $check_mess)
));

include 'header.php';
$TMP_usmenutitle = $MSG['3500_1015432'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'your_support_messages.tpl'
		));
$template->display('body');
include 'footer.php';