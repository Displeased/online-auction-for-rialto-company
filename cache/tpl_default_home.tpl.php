<div class="row">
	<div class="span12">
		<div class="row">
			<div class="span3 hidden-phone">
 				<ul class="well nav nav-list ">
    				<li class="nav-header"><?php echo ((isset($this->_rootref['L_276'])) ? $this->_rootref['L_276'] : ((isset($MSG['276'])) ? $MSG['276'] : '{ L_276 }')); ?></li>
    				<li class="divider"></li>
    				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>cat/<?php echo (isset($this->_rootref['BROWSE_SEO'])) ? $this->_rootref['BROWSE_SEO'] : ''; ?>-0"><i class="icon-tags"></i> <?php echo ((isset($this->_rootref['L_277'])) ? $this->_rootref['L_277'] : ((isset($MSG['277'])) ? $MSG['277'] : '{ L_277 }')); ?></a></li>
    				<li class="divider"></li>
    				<?php $_cat_list_count = (isset($this->_tpldata['cat_list'])) ? sizeof($this->_tpldata['cat_list']) : 0;if ($_cat_list_count) {for ($_cat_list_i = 0; $_cat_list_i < $_cat_list_count; ++$_cat_list_i){$_cat_list_val = &$this->_tpldata['cat_list'][$_cat_list_i]; ?>
    				<li> 
					<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>cat/<?php echo $_cat_list_val['SEO_NAME']; ?>-<?php echo $_cat_list_val['ID']; ?>"><?php echo $_cat_list_val['IMAGE']; echo $_cat_list_val['NAME']; ?></a></li>
    				<?php }} ?>
  				</ul><br>
  					<?php echo (isset($this->_rootref['INDEX_ADSENSE_1'])) ? $this->_rootref['INDEX_ADSENSE_1'] : ''; ?>
			</div>
			
			<div class="span6">
			<div class="row">
  				<div class="span6">
  					<?php echo (isset($this->_rootref['INDEX_ADSENSE_2'])) ? $this->_rootref['INDEX_ADSENSE_2'] : ''; ?>
  				</div>
			</div>
			<?php if ($this->_rootref['B_FEATU_ITEMS']) {  ?>
  				<div class="row">
  					<div class="span6">
      					<h3><?php echo ((isset($this->_rootref['L_350_10206'])) ? $this->_rootref['L_350_10206'] : ((isset($MSG['350_10206'])) ? $MSG['350_10206'] : '{ L_350_10206 }')); ?></h3>
    				</div>
    				<?php $_featured_count = (isset($this->_tpldata['featured'])) ? sizeof($this->_tpldata['featured']) : 0;if ($_featured_count) {for ($_featured_i = 0; $_featured_i < $_featured_count; ++$_featured_i){$_featured_val = &$this->_tpldata['featured'][$_featured_i]; ?>
    				<div class="span2" align="center"> 
    					<ul class="thumbnails">
    						<li class="span2">
    							<div class="thumbnail">
    								<div><a class="thumbnail" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_featured_val['SEO_TITLE']; ?>-<?php echo $_featured_val['ID']; ?>"><img class="img-rounded media-grid" src="<?php echo $_featured_val['IMAGE']; ?>" alt="<?php echo $_featured_val['TITLE']; ?>" style="max-width:<?php echo (isset($this->_rootref['MAXIMAGESIZELIST'])) ? $this->_rootref['MAXIMAGESIZELIST'] : ''; ?>px; max-height:<?php echo (isset($this->_rootref['MAXIMAGESIZELIST'])) ? $this->_rootref['MAXIMAGESIZELIST'] : ''; ?>px; width: auto; height: auto;"></a>
      								<h5><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_featured_val['SEO_TITLE']; ?>-<?php echo $_featured_val['ID']; ?>"><?php echo $_featured_val['TITLE']; ?></a></h5>
         							<p><small><?php echo $_featured_val['BID']; ?> <br /><span class="muted"><?php echo ((isset($this->_rootref['L_171'])) ? $this->_rootref['L_171'] : ((isset($MSG['171'])) ? $MSG['171'] : '{ L_171 }')); ?><br><?php echo $_featured_val['ENDS']; ?></span></small></p></div>
      							</div>
      						</li>
      					</ul>
      				</div>
    				<?php }} ?>
  				</div>
  			<?php } if ($this->_rootref['B_HOT_ITEMS']) {  ?>
  				<hr  />
  				<div class="row">
    				<div class="span6">
      					<h3><?php echo ((isset($this->_rootref['L_279'])) ? $this->_rootref['L_279'] : ((isset($MSG['279'])) ? $MSG['279'] : '{ L_279 }')); ?></h3>
    				</div>
  				</div>
  				<div class="row">
    				<?php $_hotitems_count = (isset($this->_tpldata['hotitems'])) ? sizeof($this->_tpldata['hotitems']) : 0;if ($_hotitems_count) {for ($_hotitems_i = 0; $_hotitems_i < $_hotitems_count; ++$_hotitems_i){$_hotitems_val = &$this->_tpldata['hotitems'][$_hotitems_i]; ?>
    				<div class="span2" align="center">
    					<ul class="thumbnails">
    						<li class="span2">
    							<div class="thumbnail">
    								<div><a class="thumbnail" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_hotitems_val['SEO_TITLE']; ?>-<?php echo $_hotitems_val['ID']; ?>"><img class="img-rounded" src="<?php echo $_hotitems_val['IMAGE']; ?>" alt="<?php echo $_hotitems_val['TITLE']; ?>" style="max-width:<?php echo (isset($this->_rootref['MAXIMAGESIZELIST'])) ? $this->_rootref['MAXIMAGESIZELIST'] : ''; ?>px; max-height:<?php echo (isset($this->_rootref['MAXIMAGESIZELIST'])) ? $this->_rootref['MAXIMAGESIZELIST'] : ''; ?>px; width: auto; height: auto;"></a>
      								<h5><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_hotitems_val['SEO_TITLE']; ?>-<?php echo $_hotitems_val['ID']; ?>"><?php echo $_hotitems_val['TITLE']; ?></a></h5>
         							<p><small><?php echo $_hotitems_val['BID']; ?> <br /><span class="muted"><?php echo ((isset($this->_rootref['L_171'])) ? $this->_rootref['L_171'] : ((isset($MSG['171'])) ? $MSG['171'] : '{ L_171 }')); ?><br><?php echo $_hotitems_val['ENDS']; ?></span></small></p></div>
      							</div>
      						</li>
      					</ul>
      				</div>
    				<?php }} ?>
  				</div>
  			<?php } if ($this->_rootref['B_AUC_LAST']) {  ?>
  				<hr  />
  				<div class="row">
    				<div class="span3">
      					<h3><?php echo ((isset($this->_rootref['L_278'])) ? $this->_rootref['L_278'] : ((isset($MSG['278'])) ? $MSG['278'] : '{ L_278 }')); ?></h3>
      					<table class="table table-condensed">
        					<?php $_auc_last_count = (isset($this->_tpldata['auc_last'])) ? sizeof($this->_tpldata['auc_last']) : 0;if ($_auc_last_count) {for ($_auc_last_i = 0; $_auc_last_i < $_auc_last_count; ++$_auc_last_i){$_auc_last_val = &$this->_tpldata['auc_last'][$_auc_last_i]; ?>
        					<tr class="well">
          						<td style="text-align:center; width:50px;"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_auc_last_val['SEO_TITLE']; ?>-<?php echo $_auc_last_val['ID']; ?>"> <img class="img-polaroid" src="<?php echo $_auc_last_val['IMAGE']; ?>" alt="<?php echo $_auc_last_val['TITLE']; ?>" style="max-height:40px; max-width:40px; width: auto; height: auto;"/></a></td>
          						<td><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_auc_last_val['SEO_TITLE']; ?>-<?php echo $_auc_last_val['ID']; ?>"><?php echo $_auc_last_val['TITLE']; ?></a><br />
            						<span class="muted"><small><?php echo $_auc_last_val['DATE']; ?></small></span>
            					</td>
        					</tr>
        					<?php }} ?>
      					</table>
    			</div>
    		<?php } if ($this->_rootref['B_AUC_ENDSOON']) {  ?>
    			<div class="span3">
      				<h3><?php echo ((isset($this->_rootref['L_280'])) ? $this->_rootref['L_280'] : ((isset($MSG['280'])) ? $MSG['280'] : '{ L_280 }')); ?></h3>
      					<table class="table table-condensed">
        					<?php $_end_soon_count = (isset($this->_tpldata['end_soon'])) ? sizeof($this->_tpldata['end_soon']) : 0;if ($_end_soon_count) {for ($_end_soon_i = 0; $_end_soon_i < $_end_soon_count; ++$_end_soon_i){$_end_soon_val = &$this->_tpldata['end_soon'][$_end_soon_i]; ?>
        					<tr class="well">
          						<td style="text-align:center; width:50px;"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_end_soon_val['SEO_TITLE']; ?>-<?php echo $_end_soon_val['ID']; ?>"> <img class="img-polaroid" src="<?php echo $_end_soon_val['IMAGE']; ?>" alt="<?php echo $_end_soon_val['TITLE']; ?>" style="max-height:40px; max-width:40px; width: auto; height: auto;"/></a></td>
          						<td><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_end_soon_val['SEO_TITLE']; ?>-<?php echo $_end_soon_val['ID']; ?>"><?php echo $_end_soon_val['TITLE']; ?></a><br />
            						<span class="muted"><small><?php echo $_end_soon_val['DATE']; ?></small></span>
            					</td>
        					</tr>
        					<?php }} ?>
      					</table>
    			</div>
    		<?php } ?>
  			</div>
		</div>
		<div class="span3">
  			  		<?php if ($this->_rootref['B_NEWS_BOX']) {  ?>
  		<div style="padding: 8px 0;">
    		<ul class="well nav nav-list ">
      			<li class="nav-header"><?php echo ((isset($this->_rootref['L_282'])) ? $this->_rootref['L_282'] : ((isset($MSG['282'])) ? $MSG['282'] : '{ L_282 }')); ?></li>
      			<li class="divider"></li>
      			<?php $_newsbox_count = (isset($this->_tpldata['newsbox'])) ? sizeof($this->_tpldata['newsbox']) : 0;if ($_newsbox_count) {for ($_newsbox_i = 0; $_newsbox_i < $_newsbox_count; ++$_newsbox_i){$_newsbox_val = &$this->_tpldata['newsbox'][$_newsbox_i]; ?>
      			<small><a data-fancybox-type="iframe" class="infoboxs" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>news/<?php echo $_newsbox_val['SEO_TITLE']; ?>-<?php echo $_newsbox_val['ID']; ?>"><?php echo $_newsbox_val['TITLE']; ?></a><br />
      			<span class="muted"><?php echo $_newsbox_val['DATE']; ?></span></small>
      			<hr />
      			<?php }} ?>
      			<div class="clearfix"></div>
    		</ul>
  		</div>
  		<?php } if ($this->_rootref['B_HELPBOX']) {  ?>
  		<div style=" padding: 8px 0;">
    		<ul class="well nav nav-list">
     			<li class="nav-header"><?php echo ((isset($this->_rootref['L_281'])) ? $this->_rootref['L_281'] : ((isset($MSG['281'])) ? $MSG['281'] : '{ L_281 }')); ?></li>
      			<li class="divider"></li>
      			<?php if ($this->_rootref['B_BOARDS']) {  ?>
      			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>boards.php"><?php echo ((isset($this->_rootref['L_5030'])) ? $this->_rootref['L_5030'] : ((isset($MSG['5030'])) ? $MSG['5030'] : '{ L_5030 }')); ?></a></li>
      			<?php } if ($this->_rootref['B_FEES']) {  ?>
      			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>fees.php"><?php echo ((isset($this->_rootref['L_25_0012'])) ? $this->_rootref['L_25_0012'] : ((isset($MSG['25_0012'])) ? $MSG['25_0012'] : '{ L_25_0012 }')); ?></a></li>
      			<?php } ?>
      			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>email_request_support.php"><?php echo ((isset($this->_rootref['L_350_10207'])) ? $this->_rootref['L_350_10207'] : ((isset($MSG['350_10207'])) ? $MSG['350_10207'] : '{ L_350_10207 }')); ?></a></li>
      			<?php $_helpbox_count = (isset($this->_tpldata['helpbox'])) ? sizeof($this->_tpldata['helpbox']) : 0;if ($_helpbox_count) {for ($_helpbox_i = 0; $_helpbox_i < $_helpbox_count; ++$_helpbox_i){$_helpbox_val = &$this->_tpldata['helpbox'][$_helpbox_i]; ?>
      			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>viewhelp.php?cat=<?php echo $_helpbox_val['ID']; ?>" alt="faqs" data-fancybox-type="iframe" class="infoboxs"><?php echo $_helpbox_val['TITLE']; ?></a></li>
      			<?php }} ?>
   		 	</ul>
  		</div>
  	<?php } ?>
  	<br><?php echo (isset($this->_rootref['INDEX_ADSENSE_3'])) ? $this->_rootref['INDEX_ADSENSE_3'] : ''; ?>
</div>