</div>

<div class="clearfix"></div>
<div class="footer well"> 
  <small><span class="muted"> <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>home"><?php echo ((isset($this->_rootref['L_166'])) ? $this->_rootref['L_166'] : ((isset($MSG['166'])) ? $MSG['166'] : '{ L_166 }')); ?></a>
  <?php if ($this->_rootref['B_CAN_SELL']) {  ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>select_category.php?"><?php echo ((isset($this->_rootref['L_028'])) ? $this->_rootref['L_028'] : ((isset($MSG['028'])) ? $MSG['028'] : '{ L_028 }')); ?></a>
  <?php } if ($this->_rootref['B_LOGGED_IN']) {  ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>user_menu.php?"><?php echo ((isset($this->_rootref['L_622'])) ? $this->_rootref['L_622'] : ((isset($MSG['622'])) ? $MSG['622'] : '{ L_622 }')); ?></a> | <a href="<?php echo (isset($this->_rootref['SSLURL'])) ? $this->_rootref['SSLURL'] : ''; ?>logout.php?"><?php echo ((isset($this->_rootref['L_245'])) ? $this->_rootref['L_245'] : ((isset($MSG['245'])) ? $MSG['245'] : '{ L_245 }')); ?></a>
  <?php } else { ?>
  | <a href="<?php echo (isset($this->_rootref['SSLURL'])) ? $this->_rootref['SSLURL'] : ''; ?>new_account"><?php echo ((isset($this->_rootref['L_235'])) ? $this->_rootref['L_235'] : ((isset($MSG['235'])) ? $MSG['235'] : '{ L_235 }')); ?></a> | <a href="<?php echo (isset($this->_rootref['SSLURL'])) ? $this->_rootref['SSLURL'] : ''; ?>user_login.php?"><?php echo ((isset($this->_rootref['L_052'])) ? $this->_rootref['L_052'] : ((isset($MSG['052'])) ? $MSG['052'] : '{ L_052 }')); ?></a>
  <?php } ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>help.php" alt="faqs" data-fancybox-type="iframe" class="infoboxs"><?php echo ((isset($this->_rootref['L_148'])) ? $this->_rootref['L_148'] : ((isset($MSG['148'])) ? $MSG['148'] : '{ L_148 }')); ?></a>
  <?php if ($this->_rootref['B_FEES']) {  ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>fees.php"><?php echo ((isset($this->_rootref['L_25_0012'])) ? $this->_rootref['L_25_0012'] : ((isset($MSG['25_0012'])) ? $MSG['25_0012'] : '{ L_25_0012 }')); ?></a>
  <?php } if ($this->_rootref['B_VIEW_ABOUTUS']) {  ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>contents.php?show=aboutus" ><?php echo ((isset($this->_rootref['L_5085'])) ? $this->_rootref['L_5085'] : ((isset($MSG['5085'])) ? $MSG['5085'] : '{ L_5085 }')); ?></a>
  <?php } if ($this->_rootref['B_VIEW_PRIVPOL']) {  ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>contents.php?show=priv"><?php echo ((isset($this->_rootref['L_401'])) ? $this->_rootref['L_401'] : ((isset($MSG['401'])) ? $MSG['401'] : '{ L_401 }')); ?></a>
  <?php } if ($this->_rootref['B_VIEW_COOKIES']) {  ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>contents.php?show=cookies"> <?php echo ((isset($this->_rootref['L_30_0239'])) ? $this->_rootref['L_30_0239'] : ((isset($MSG['30_0239'])) ? $MSG['30_0239'] : '{ L_30_0239 }')); ?> </a> 
<?php } if ($this->_rootref['B_VIEW_TERMS']) {  ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>contents.php?show=terms"><?php echo ((isset($this->_rootref['L_5086'])) ? $this->_rootref['L_5086'] : ((isset($MSG['5086'])) ? $MSG['5086'] : '{ L_5086 }')); ?></a>
  <?php } ?>
  | <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>email_request_support.php"><?php echo ((isset($this->_rootref['L_350_10207'])) ? $this->_rootref['L_350_10207'] : ((isset($MSG['350_10207'])) ? $MSG['350_10207'] : '{ L_350_10207 }')); ?></a>
  </span></small> <br>
</div>
<div align="center">
<?php echo ((isset($this->_rootref['L_COPY'])) ? $this->_rootref['L_COPY'] : ((isset($MSG['COPY'])) ? $MSG['COPY'] : '{ L_COPY }')); ?><br>
<!--
			We request you retain the full copyright notice below including the link to www.u-auctions.com.
			This not only gives respect to the large amount of time given freely by the developers
			but also helps build interest, traffic and use of u-Auctions. If you (honestly) cannot retain
			the full copyright we ask you at least leave in place the "Powered by u-Auctions" line, with
			"u-Auctions" linked to https://u-auctions.com. If you must remove thte copyright message pelase make
            a donation at https://www.u-auctions.com/forum/donate.php to help pay for future developments
		-->
		<small><span>Powered by <a href="https://www.u-auctions.com/">u-Auctions</a> &copy; 2013 - 2015 <a href="https://www.u-auctions.com/">u-Auctions</a></span></small>
</div>

<?php if ($this->_rootref['B_SUB_ADMIN']) {  ?>
<br />
<div align="center">
<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/login.php" class="btn btn-primary" target="_blank"><?php echo ((isset($this->_rootref['L_3500_1015692'])) ? $this->_rootref['L_3500_1015692'] : ((isset($MSG['3500_1015692'])) ? $MSG['3500_1015692'] : '{ L_3500_1015692 }')); ?></a>
<br />&nbsp;
</div>
<?php } if ($this->_rootref['B_MAIN_ADMIN']) {  ?>
<br />
<div align="center">
<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/login.php" class="btn btn-primary" target="_blank"><?php echo ((isset($this->_rootref['L_3500_1015405'])) ? $this->_rootref['L_3500_1015405'] : ((isset($MSG['3500_1015405'])) ? $MSG['3500_1015405'] : '{ L_3500_1015405 }')); ?></a>
<br />&nbsp;
</div>
<?php } ?>
</div><br />&nbsp;<br />
</body></html>