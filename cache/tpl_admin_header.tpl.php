<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="<?php echo (isset($this->_rootref['LANGUAGE'])) ? $this->_rootref['LANGUAGE'] : ''; ?>" dir="<?php echo (isset($this->_rootref['DOCDIR'])) ? $this->_rootref['DOCDIR'] : ''; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="<?php echo (isset($this->_rootref['LANGUAGE'])) ? $this->_rootref['LANGUAGE'] : ''; ?>" dir="<?php echo (isset($this->_rootref['DOCDIR'])) ? $this->_rootref['DOCDIR'] : ''; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="<?php echo (isset($this->_rootref['LANGUAGE'])) ? $this->_rootref['LANGUAGE'] : ''; ?>" dir="<?php echo (isset($this->_rootref['DOCDIR'])) ? $this->_rootref['DOCDIR'] : ''; ?>"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="<?php echo (isset($this->_rootref['LANGUAGE'])) ? $this->_rootref['LANGUAGE'] : ''; ?>" dir="<?php echo (isset($this->_rootref['DOCDIR'])) ? $this->_rootref['DOCDIR'] : ''; ?>"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo (isset($this->_rootref['CHARSET'])) ? $this->_rootref['CHARSET'] : ''; ?>">
<title>Admin panel</title>

<!-- Viewport Metatag -->
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="icon" type="image/ico" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>favicon.ico"/>
<!-- Plugin Stylesheets first to ease overrides -->
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/custom-plugins/wizard/wizard.css" media="screen">

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/bootstrap/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/fonts/ptsans/stylesheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/mws-style.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/icons/icol16.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/icons/icol32.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/imgareaselect/css/imgareaselect-default.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/colorpicker/colorpicker.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/jgrowl/jquery.jgrowl.css" media="screen">

<!-- jQuery-UI Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/jui/css/jquery.ui.all.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/jui/css/jquery.ui.timepicker.css" media="screen">

<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/mws-theme.css" media="screen">

<script type="text/javascript" src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>loader.php?js=js/jquery.js<?php echo (isset($this->_rootref['EXTRAJS'])) ? $this->_rootref['EXTRAJS'] : ''; ?>"></script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>inc/ckeditor/ckeditor.js"></script>
<!-- JavaScript Plugins -->
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/libs/jquery-1.8.3.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/libs/jquery.mousewheel.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/libs/jquery.placeholder.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/custom-plugins/fileinput.js"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/jui/jquery-ui.custom.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/jui/js/jquery.ui.touch-punch.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/jui/js/timepicker/jquery-ui-timepicker.min.js"></script>

    <!-- Plugin Scripts -->
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/libs/excanvas.min.js"></script>
    <![endif]-->
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/flot/jquery.flot.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/flot/plugins/jquery.flot.pie.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/flot/plugins/jquery.flot.stack.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/flot/plugins/jquery.flot.resize.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/colorpicker/colorpicker-min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/validate/jquery.validate-min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/jgrowl/jquery.jgrowl-min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/custom-plugins/wizard/wizard.min.js"></script>

    <!-- Core Script -->
    <script type="text/javascript" src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/source/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/core/mws.js"></script>

    <!-- Fancybox -->
    <script type="text/javascript">
	$(document).ready(function() {
	$('.fancybox-buttons').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		prevEffect : 'none',
		nextEffect : 'none',
		closeBtn  : false,
		helpers : {
			title : {
				type : 'float'
				},
				buttons	: {}
				},
		afterLoad : function() {
			this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
		}
	});
	$(".converter").fancybox({
		maxWidth	: 600,
		maxHeight	: 700,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	$(".infoboxs").fancybox({
		maxWidth	: 750,
		maxHeight	: 850,
		fitToView	: false,
		width		: '80%',
		height		: '80%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	$(".imaging").fancybox({
		maxWidth	: 1300,
		maxHeight	: 1300,
		fitToView	: false,
		width		: '95%',
		height		: '95%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

});
</script>
</head>
<body>

	<!-- Header -->
	<div id="mws-header" class="clearfix">
    
    	<!-- Logo Container -->
    	<div id="mws-logo-container">
        
        	<!-- Logo Wrapper, images put within this wrapper will always be vertically centered -->
        	<div id="mws-logo-wrap">
            	<img src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/images/mws-logo.png" alt="mws admin">
			</div>
        </div>
        
        <!-- User Tools (notifications, logout, profile, change password) -->
        <div id="mws-user-tools" class="clearfix">
        	
                
                <!-- Notifications -->
        		<div id="mws-user-notif" class="mws-dropdown-menu">
            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-info-sign"></i></a>
                
                <!-- Unread notification count -->
                <span class="mws-dropdown-notif">8</span>
                
                <!-- Notifications dropdown -->
                <div class="mws-dropdown-box">
                	<div class="mws-dropdown-content">
                        <ul class="mws-notifications">
                        	<li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/forum/portal.php">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_1063'])) ? $this->_rootref['L_1063'] : ((isset($MSG['1063'])) ? $MSG['1063'] : '{ L_1063 }')); ?>
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/wiki/doku.php">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_3500_1015515'])) ? $this->_rootref['L_3500_1015515'] : ((isset($MSG['3500_1015515'])) ? $MSG['3500_1015515'] : '{ L_3500_1015515 }')); ?>
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/Themes">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_1069'])) ? $this->_rootref['L_1069'] : ((isset($MSG['1069'])) ? $MSG['1069'] : '{ L_1069 }')); ?>
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/Mods">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_1072'])) ? $this->_rootref['L_1072'] : ((isset($MSG['1072'])) ? $MSG['1072'] : '{ L_1072 }')); ?>
                                    </span>
                                </a>
                            </li>
                            <li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/Languages">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_1073'])) ? $this->_rootref['L_1073'] : ((isset($MSG['1073'])) ? $MSG['1073'] : '{ L_1073 }')); ?>
                                    </span>
                                </a>
                            </li>
                            <li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/forum/viewforum.php?f=22">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_1076'])) ? $this->_rootref['L_1076'] : ((isset($MSG['1076'])) ? $MSG['1076'] : '{ L_1076 }')); ?>
                                    </span>
                                </a>
                            </li>
                            <li class="unread">
                            	<a class="imaging" data-fancybox-type="iframe" href="https://www.u-auctions.com/forum/donate.php">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_1080'])) ? $this->_rootref['L_1080'] : ((isset($MSG['1080'])) ? $MSG['1080'] : '{ L_1080 }')); ?>
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            
            <!-- Notifications -->
        		<div id="mws-user-notif" class="mws-dropdown-menu">
            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-cogs"></i></a>
                
                <!-- Unread notification count -->
                <span class="mws-dropdown-notif">2</span>
                
                <!-- Notifications dropdown -->
                <div class="mws-dropdown-box">
                	<div class="mws-dropdown-content">
                        <ul class="mws-notifications">
                        	<li class="unread">
                            	<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/errorlog.php">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_891'])) ? $this->_rootref['L_891'] : ((isset($MSG['891'])) ? $MSG['891'] : '{ L_891 }')); ?>
                                    </span>
                                </a>
                            </li>
                            <li class="unread">
                            	<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/cronlog.php">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_3500_1015588'])) ? $this->_rootref['L_3500_1015588'] : ((isset($MSG['3500_1015588'])) ? $MSG['3500_1015588'] : '{ L_3500_1015588 }')); ?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <!-- Notifications -->
        		<div id="mws-user-notif" class="mws-dropdown-menu">
            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-network"></i></a>
                
                <!-- Unread notification count -->
                <span class="mws-dropdown-notif">2</span>
                
                <!-- Notifications dropdown -->
                <div class="mws-dropdown-box">
                	<div class="mws-dropdown-content">
                        <ul class="mws-notifications">
                        	<li class="unread">
                            	<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/logout.php">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_245'])) ? $this->_rootref['L_245'] : ((isset($MSG['245'])) ? $MSG['245'] : '{ L_245 }')); ?>
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>" target="_blank">
                                    <span class="message">
                                        <?php echo ((isset($this->_rootref['L_3500_1015516'])) ? $this->_rootref['L_3500_1015516'] : ((isset($MSG['3500_1015516'])) ? $MSG['3500_1015516'] : '{ L_3500_1015516 }')); ?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        	
            <!-- User Information and functions section -->
            <div id="mws-user-info" class="mws-inset">
            
            	<!-- User Photo -->
            	<div id="mws-user-photo">
                	<img src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/images/personal.png" alt="User Photo">
                </div>
                
                <!-- Username and Functions -->
                <div id="mws-user-functions">
                    <div id="mws-username">
                        <?php echo ((isset($this->_rootref['L_200'])) ? $this->_rootref['L_200'] : ((isset($MSG['200'])) ? $MSG['200'] : '{ L_200 }')); ?> <?php echo (isset($this->_rootref['ADMIN_USER'])) ? $this->_rootref['ADMIN_USER'] : ''; ?><br />
                        <?php echo ((isset($this->_rootref['L_559'])) ? $this->_rootref['L_559'] : ((isset($MSG['559'])) ? $MSG['559'] : '{ L_559 }')); ?>: <?php echo (isset($this->_rootref['LAST_LOGIN'])) ? $this->_rootref['LAST_LOGIN'] : ''; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Start Main Wrapper -->
    <div id="mws-wrapper">
    
    	<!-- Necessary markup, do not remove -->
		<div id="mws-sidebar-stitch"></div>
		<div id="mws-sidebar-bg"></div>
        
        <!-- Sidebar Wrapper -->
        <div id="mws-sidebar">
        
            <!-- Hidden Nav Collapse Button -->
            <div id="mws-nav-collapse">
                <span></span>
                <span></span>
                <span></span>
            </div>
            
            <div id="mws-searchbox" class="mws-inset">
            	<ul>
                    	<li>
                    	 || 
                        </li>
                </ul>
            </div>
            
            <!-- Main Navigation -->
            <div id="mws-navigation">
                <ul>
                    <li class="active"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/index.php"><i class="icon-home"></i> <?php echo ((isset($this->_rootref['L_166'])) ? $this->_rootref['L_166'] : ((isset($MSG['166'])) ? $MSG['166'] : '{ L_166 }')); ?></a></li>
                    <li><a href="#"><i class="icon-cogs"></i> <?php echo ((isset($this->_rootref['L_5139'])) ? $this->_rootref['L_5139'] : ((isset($MSG['5139'])) ? $MSG['5139'] : '{ L_5139 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/settings.php"> <?php echo ((isset($this->_rootref['L_526'])) ? $this->_rootref['L_526'] : ((isset($MSG['526'])) ? $MSG['526'] : '{ L_526 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/auctions.php"><?php echo ((isset($this->_rootref['L_5087'])) ? $this->_rootref['L_5087'] : ((isset($MSG['5087'])) ? $MSG['5087'] : '{ L_5087 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/minetype.php"><?php echo ((isset($this->_rootref['L_3500_1015778'])) ? $this->_rootref['L_3500_1015778'] : ((isset($MSG['3500_1015778'])) ? $MSG['3500_1015778'] : '{ L_3500_1015778 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/displaysettings.php"><?php echo ((isset($this->_rootref['L_788'])) ? $this->_rootref['L_788'] : ((isset($MSG['788'])) ? $MSG['788'] : '{ L_788 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/usersettings.php"><?php echo ((isset($this->_rootref['L_894'])) ? $this->_rootref['L_894'] : ((isset($MSG['894'])) ? $MSG['894'] : '{ L_894 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/errorhandling.php"><?php echo ((isset($this->_rootref['L_409'])) ? $this->_rootref['L_409'] : ((isset($MSG['409'])) ? $MSG['409'] : '{ L_409 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/conditions.php"><?php echo ((isset($this->_rootref['L_104100'])) ? $this->_rootref['L_104100'] : ((isset($MSG['104100'])) ? $MSG['104100'] : '{ L_104100 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/countries.php"><?php echo ((isset($this->_rootref['L_081'])) ? $this->_rootref['L_081'] : ((isset($MSG['081'])) ? $MSG['081'] : '{ L_081 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/payments.php"><?php echo ((isset($this->_rootref['L_075'])) ? $this->_rootref['L_075'] : ((isset($MSG['075'])) ? $MSG['075'] : '{ L_075 }')); ?></a></li>
                          	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/durations.php"><?php echo ((isset($this->_rootref['L_069'])) ? $this->_rootref['L_069'] : ((isset($MSG['069'])) ? $MSG['069'] : '{ L_069 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/increments.php"><?php echo ((isset($this->_rootref['L_128'])) ? $this->_rootref['L_128'] : ((isset($MSG['128'])) ? $MSG['128'] : '{ L_128 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/membertypes.php"><?php echo ((isset($this->_rootref['L_25_0169'])) ? $this->_rootref['L_25_0169'] : ((isset($MSG['25_0169'])) ? $MSG['25_0169'] : '{ L_25_0169 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/currency.php"><?php echo ((isset($this->_rootref['L_5004'])) ? $this->_rootref['L_5004'] : ((isset($MSG['5004'])) ? $MSG['5004'] : '{ L_5004 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/time.php"><?php echo ((isset($this->_rootref['L_344'])) ? $this->_rootref['L_344'] : ((isset($MSG['344'])) ? $MSG['344'] : '{ L_344 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/buyitnow.php"><?php echo ((isset($this->_rootref['L_3500_1015728'])) ? $this->_rootref['L_3500_1015728'] : ((isset($MSG['3500_1015728'])) ? $MSG['3500_1015728'] : '{ L_3500_1015728 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/defaultcountry.php"><?php echo ((isset($this->_rootref['L_5322'])) ? $this->_rootref['L_5322'] : ((isset($MSG['5322'])) ? $MSG['5322'] : '{ L_5322 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/counters.php"><?php echo ((isset($this->_rootref['L_2__0057'])) ? $this->_rootref['L_2__0057'] : ((isset($MSG['2__0057'])) ? $MSG['2__0057'] : '{ L_2__0057 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/multilingual.php"><?php echo ((isset($this->_rootref['L_2__0002'])) ? $this->_rootref['L_2__0002'] : ((isset($MSG['2__0002'])) ? $MSG['2__0002'] : '{ L_2__0002 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/metatags.php"><?php echo ((isset($this->_rootref['L_25_0178'])) ? $this->_rootref['L_25_0178'] : ((isset($MSG['25_0178'])) ? $MSG['25_0178'] : '{ L_25_0178 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/contactseller.php"><?php echo ((isset($this->_rootref['L_25_0216'])) ? $this->_rootref['L_25_0216'] : ((isset($MSG['25_0216'])) ? $MSG['25_0216'] : '{ L_25_0216 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/buyerprivacy.php"><?php echo ((isset($this->_rootref['L_236'])) ? $this->_rootref['L_236'] : ((isset($MSG['236'])) ? $MSG['236'] : '{ L_236 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-list"></i> <?php echo ((isset($this->_rootref['L_3500_1015421'])) ? $this->_rootref['L_3500_1015421'] : ((isset($MSG['3500_1015421'])) ? $MSG['3500_1015421'] : '{ L_3500_1015421 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/catsorting.php"><?php echo ((isset($this->_rootref['L_25_0146'])) ? $this->_rootref['L_25_0146'] : ((isset($MSG['25_0146'])) ? $MSG['25_0146'] : '{ L_25_0146 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/categories.php"><?php echo ((isset($this->_rootref['L_078'])) ? $this->_rootref['L_078'] : ((isset($MSG['078'])) ? $MSG['078'] : '{ L_078 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/categoriestrans.php"><?php echo ((isset($this->_rootref['L_132'])) ? $this->_rootref['L_132'] : ((isset($MSG['132'])) ? $MSG['132'] : '{ L_132 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-tags"></i> <?php echo ((isset($this->_rootref['L_25_0012'])) ? $this->_rootref['L_25_0012'] : ((isset($MSG['25_0012'])) ? $MSG['25_0012'] : '{ L_25_0012 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/fees.php"><?php echo ((isset($this->_rootref['L_25_0012'])) ? $this->_rootref['L_25_0012'] : ((isset($MSG['25_0012'])) ? $MSG['25_0012'] : '{ L_25_0012 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/fee_gateways.php"><?php echo ((isset($this->_rootref['L_445'])) ? $this->_rootref['L_445'] : ((isset($MSG['445'])) ? $MSG['445'] : '{ L_445 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/enablefees.php"><?php echo ((isset($this->_rootref['L_395'])) ? $this->_rootref['L_395'] : ((isset($MSG['395'])) ? $MSG['395'] : '{ L_395 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/accounts.php"><?php echo ((isset($this->_rootref['L_854'])) ? $this->_rootref['L_854'] : ((isset($MSG['854'])) ? $MSG['854'] : '{ L_854 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/invoice_settings.php"><?php echo ((isset($this->_rootref['L_1094'])) ? $this->_rootref['L_1094'] : ((isset($MSG['1094'])) ? $MSG['1094'] : '{ L_1094 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/invoice.php"><?php echo ((isset($this->_rootref['L_766'])) ? $this->_rootref['L_766'] : ((isset($MSG['766'])) ? $MSG['766'] : '{ L_766 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/tax.php"><?php echo ((isset($this->_rootref['L_1088'])) ? $this->_rootref['L_1088'] : ((isset($MSG['1088'])) ? $MSG['1088'] : '{ L_1088 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/tax_levels.php"><?php echo ((isset($this->_rootref['L_1083'])) ? $this->_rootref['L_1083'] : ((isset($MSG['1083'])) ? $MSG['1083'] : '{ L_1083 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/theme.php"><i class="icon-business-card"></i> <?php echo ((isset($this->_rootref['L_26_0002'])) ? $this->_rootref['L_26_0002'] : ((isset($MSG['26_0002'])) ? $MSG['26_0002'] : '{ L_26_0002 }')); ?></a>
                    </li>
                    <li><a href="#"><i class="icon-bullhorn"></i> <?php echo ((isset($this->_rootref['L_25_0011'])) ? $this->_rootref['L_25_0011'] : ((isset($MSG['25_0011'])) ? $MSG['25_0011'] : '{ L_25_0011 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/banners.php"><?php echo ((isset($this->_rootref['L_5205'])) ? $this->_rootref['L_5205'] : ((isset($MSG['5205'])) ? $MSG['5205'] : '{ L_5205 }')); ?></a></li>
                    		<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/managebanners.php"><?php echo ((isset($this->_rootref['L__0008'])) ? $this->_rootref['L__0008'] : ((isset($MSG['_0008'])) ? $MSG['_0008'] : '{ L__0008 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-user"></i> <?php echo ((isset($this->_rootref['L_25_0010'])) ? $this->_rootref['L_25_0010'] : ((isset($MSG['25_0010'])) ? $MSG['25_0010'] : '{ L_25_0010 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/admin_support.php"><?php echo ((isset($this->_rootref['L_3500_1015432'])) ? $this->_rootref['L_3500_1015432'] : ((isset($MSG['3500_1015432'])) ? $MSG['3500_1015432'] : '{ L_3500_1015432 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/listusers.php"><?php echo ((isset($this->_rootref['L_045'])) ? $this->_rootref['L_045'] : ((isset($MSG['045'])) ? $MSG['045'] : '{ L_045 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/useractivity.php"><?php echo ((isset($this->_rootref['L_350_10210'])) ? $this->_rootref['L_350_10210'] : ((isset($MSG['350_10210'])) ? $MSG['350_10210'] : '{ L_350_10210 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/usergroups.php"><?php echo ((isset($this->_rootref['L_448'])) ? $this->_rootref['L_448'] : ((isset($MSG['448'])) ? $MSG['448'] : '{ L_448 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/profile.php"><?php echo ((isset($this->_rootref['L_048'])) ? $this->_rootref['L_048'] : ((isset($MSG['048'])) ? $MSG['048'] : '{ L_048 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/activatenewsletter.php"><?php echo ((isset($this->_rootref['L_25_0079'])) ? $this->_rootref['L_25_0079'] : ((isset($MSG['25_0079'])) ? $MSG['25_0079'] : '{ L_25_0079 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/newsletter.php"><?php echo ((isset($this->_rootref['L_607'])) ? $this->_rootref['L_607'] : ((isset($MSG['607'])) ? $MSG['607'] : '{ L_607 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/banips.php"><?php echo ((isset($this->_rootref['L_2_0017'])) ? $this->_rootref['L_2_0017'] : ((isset($MSG['2_0017'])) ? $MSG['2_0017'] : '{ L_2_0017 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/newadminuser.php"><?php echo ((isset($this->_rootref['L_367'])) ? $this->_rootref['L_367'] : ((isset($MSG['367'])) ? $MSG['367'] : '{ L_367 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/adminusers.php"><?php echo ((isset($this->_rootref['L_525'])) ? $this->_rootref['L_525'] : ((isset($MSG['525'])) ? $MSG['525'] : '{ L_525 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/security.php"><i class="icon-key-2"></i> <?php echo ((isset($this->_rootref['L_3500_1015543'])) ? $this->_rootref['L_3500_1015543'] : ((isset($MSG['3500_1015543'])) ? $MSG['3500_1015543'] : '{ L_3500_1015543 }')); ?></a></li>
                    <li><a href="#"><i class="icon-android"></i> <?php echo ((isset($this->_rootref['L_3500_1015418'])) ? $this->_rootref['L_3500_1015418'] : ((isset($MSG['3500_1015418'])) ? $MSG['3500_1015418'] : '{ L_3500_1015418 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/spam.php"><?php echo ((isset($this->_rootref['L_749'])) ? $this->_rootref['L_749'] : ((isset($MSG['749'])) ? $MSG['749'] : '{ L_749 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/email_block.php"><?php echo ((isset($this->_rootref['L_3500_1015416'])) ? $this->_rootref['L_3500_1015416'] : ((isset($MSG['3500_1015416'])) ? $MSG['3500_1015416'] : '{ L_3500_1015416 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-comments"></i> <?php echo ((isset($this->_rootref['L_5030'])) ? $this->_rootref['L_5030'] : ((isset($MSG['5030'])) ? $MSG['5030'] : '{ L_5030 }')); ?></a>
                        <ul class="closed">
							<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/boardsettings.php"><?php echo ((isset($this->_rootref['L_5047'])) ? $this->_rootref['L_5047'] : ((isset($MSG['5047'])) ? $MSG['5047'] : '{ L_5047 }')); ?></a></li>
							<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/newboard.php"><?php echo ((isset($this->_rootref['L_5031'])) ? $this->_rootref['L_5031'] : ((isset($MSG['5031'])) ? $MSG['5031'] : '{ L_5031 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/boards.php"><?php echo ((isset($this->_rootref['L_5032'])) ? $this->_rootref['L_5032'] : ((isset($MSG['5032'])) ? $MSG['5032'] : '{ L_5032 }')); ?></a></li>
						</ul>
					</li>
                    <li><a href="#"><i class="icon-legal"></i> <?php echo ((isset($this->_rootref['L_239'])) ? $this->_rootref['L_239'] : ((isset($MSG['239'])) ? $MSG['239'] : '{ L_239 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/listauctions.php"><?php echo ((isset($this->_rootref['L_067'])) ? $this->_rootref['L_067'] : ((isset($MSG['067'])) ? $MSG['067'] : '{ L_067 }')); ?></a></li>
                    		<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/listclosedauctions.php"><?php echo ((isset($this->_rootref['L_214'])) ? $this->_rootref['L_214'] : ((isset($MSG['214'])) ? $MSG['214'] : '{ L_214 }')); ?></a></li>
                    		<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/listsuspendedauctions.php"><?php echo ((isset($this->_rootref['L_5227'])) ? $this->_rootref['L_5227'] : ((isset($MSG['5227'])) ? $MSG['5227'] : '{ L_5227 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-info-sign"></i> <?php echo ((isset($this->_rootref['L_5236'])) ? $this->_rootref['L_5236'] : ((isset($MSG['5236'])) ? $MSG['5236'] : '{ L_5236 }')); ?></a>
                        <ul class="closed">
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/faqscategories.php"><?php echo ((isset($this->_rootref['L_5230'])) ? $this->_rootref['L_5230'] : ((isset($MSG['5230'])) ? $MSG['5230'] : '{ L_5230 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/newfaq.php"><?php echo ((isset($this->_rootref['L_5231'])) ? $this->_rootref['L_5231'] : ((isset($MSG['5231'])) ? $MSG['5231'] : '{ L_5231 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/faqs.php"><?php echo ((isset($this->_rootref['L_5232'])) ? $this->_rootref['L_5232'] : ((isset($MSG['5232'])) ? $MSG['5232'] : '{ L_5232 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-edit"></i> <?php echo ((isset($this->_rootref['L_25_0018'])) ? $this->_rootref['L_25_0018'] : ((isset($MSG['25_0018'])) ? $MSG['25_0018'] : '{ L_25_0018 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/news.php"><?php echo ((isset($this->_rootref['L_516'])) ? $this->_rootref['L_516'] : ((isset($MSG['516'])) ? $MSG['516'] : '{ L_516 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/aboutus.php"><?php echo ((isset($this->_rootref['L_5074'])) ? $this->_rootref['L_5074'] : ((isset($MSG['5074'])) ? $MSG['5074'] : '{ L_5074 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/terms.php"><?php echo ((isset($this->_rootref['L_5075'])) ? $this->_rootref['L_5075'] : ((isset($MSG['5075'])) ? $MSG['5075'] : '{ L_5075 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/privacypolicy.php"><?php echo ((isset($this->_rootref['L_402'])) ? $this->_rootref['L_402'] : ((isset($MSG['402'])) ? $MSG['402'] : '{ L_402 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/cookiespolicy.php"><?php echo ((isset($this->_rootref['L_30_0233'])) ? $this->_rootref['L_30_0233'] : ((isset($MSG['30_0233'])) ? $MSG['30_0233'] : '{ L_30_0233 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-bars"></i> <?php echo ((isset($this->_rootref['L_25_0023'])) ? $this->_rootref['L_25_0023'] : ((isset($MSG['25_0023'])) ? $MSG['25_0023'] : '{ L_25_0023 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/stats_settings.php"><?php echo ((isset($this->_rootref['L_3500_1015768'])) ? $this->_rootref['L_3500_1015768'] : ((isset($MSG['3500_1015768'])) ? $MSG['3500_1015768'] : '{ L_3500_1015768 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/viewaccessstats.php"><?php echo ((isset($this->_rootref['L_5143'])) ? $this->_rootref['L_5143'] : ((isset($MSG['5143'])) ? $MSG['5143'] : '{ L_5143 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/viewbrowserstats.php"><?php echo ((isset($this->_rootref['L_5165'])) ? $this->_rootref['L_5165'] : ((isset($MSG['5165'])) ? $MSG['5165'] : '{ L_5165 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/viewplatformstats.php"><?php echo ((isset($this->_rootref['L_5318'])) ? $this->_rootref['L_5318'] : ((isset($MSG['5318'])) ? $MSG['5318'] : '{ L_5318 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/viewbotstats.php"><?php echo ((isset($this->_rootref['L_3500_1015742'])) ? $this->_rootref['L_3500_1015742'] : ((isset($MSG['3500_1015742'])) ? $MSG['3500_1015742'] : '{ L_3500_1015742 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-tools"></i> <?php echo ((isset($this->_rootref['L_5436'])) ? $this->_rootref['L_5436'] : ((isset($MSG['5436'])) ? $MSG['5436'] : '{ L_5436 }')); ?></a>
                        <ul class="closed">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/checkversion.php"><?php echo ((isset($this->_rootref['L_25_0169a'])) ? $this->_rootref['L_25_0169a'] : ((isset($MSG['25_0169a'])) ? $MSG['25_0169a'] : '{ L_25_0169a }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/maintainance.php"><?php echo ((isset($this->_rootref['L__0001'])) ? $this->_rootref['L__0001'] : ((isset($MSG['_0001'])) ? $MSG['_0001'] : '{ L__0001 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/wordsfilter.php"><?php echo ((isset($this->_rootref['L_5068'])) ? $this->_rootref['L_5068'] : ((isset($MSG['5068'])) ? $MSG['5068'] : '{ L_5068 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/errorlog.php"><?php echo ((isset($this->_rootref['L_891'])) ? $this->_rootref['L_891'] : ((isset($MSG['891'])) ? $MSG['891'] : '{ L_891 }')); ?></a></li>
                            <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/cronlog.php"><?php echo ((isset($this->_rootref['L_3500_1015588'])) ? $this->_rootref['L_3500_1015588'] : ((isset($MSG['3500_1015588'])) ? $MSG['3500_1015588'] : '{ L_3500_1015588 }')); ?></a></li>
                    		<li><a href="help.php"><?php echo ((isset($this->_rootref['L_148'])) ? $this->_rootref['L_148'] : ((isset($MSG['148'])) ? $MSG['148'] : '{ L_148 }')); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-pushpin"></i> <?php echo ((isset($this->_rootref['L_1061'])) ? $this->_rootref['L_1061'] : ((isset($MSG['1061'])) ? $MSG['1061'] : '{ L_1061 }')); ?></a>
                        <ul class="closed">
                        	<li>
                            	<form name="anotes" action="" method="post">
                                    <textarea rows="15" style="width:100%" name="anotes" class="anotes"><?php echo (isset($this->_rootref['ADMIN_NOTES'])) ? $this->_rootref['ADMIN_NOTES'] : ''; ?></textarea><br />
                                    <input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
                                    <input type="hidden" name="submitdata" value="data">
                                    <input type="submit" class="btn btn-success" name="act" value="<?php echo ((isset($this->_rootref['L_007'])) ? $this->_rootref['L_007'] : ((isset($MSG['007'])) ? $MSG['007'] : '{ L_007 }')); ?>">
                                    <span style="float:right">
                                    <input type="submit" class="btn btn-danger" name="act" value="<?php echo ((isset($this->_rootref['L_008'])) ? $this->_rootref['L_008'] : ((isset($MSG['008'])) ? $MSG['008'] : '{ L_008 }')); ?>"></span><br /><br />
								</form>
                        	</li>
                        </ul>
                    </li>     
                </ul>
            </div>         
        </div>
                <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- error messages -->
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-info-sign"></i> <?php echo ((isset($this->_rootref['L_3500_1015582'])) ? $this->_rootref['L_3500_1015582'] : ((isset($MSG['3500_1015582'])) ? $MSG['3500_1015582'] : '{ L_3500_1015582 }')); ?></span>
                    </div>
                <?php if ($this->_rootref['SUPPORTMESSAGE']) {  ?>
                    <div class="mws-panel-body no-padding">
                        	<div class="mws-form-message error">
                            	<?php echo (isset($this->_rootref['MESSAGES'])) ? $this->_rootref['MESSAGES'] : ''; ?> <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['ADMIN_FOLDER'])) ? $this->_rootref['ADMIN_FOLDER'] : ''; ?>/admin_support.php"><?php echo ((isset($this->_rootref['L_3500_1015439p'])) ? $this->_rootref['L_3500_1015439p'] : ((isset($MSG['3500_1015439p'])) ? $MSG['3500_1015439p'] : '{ L_3500_1015439p }')); ?></a>
                            </div>
                    </div>
                <?php } ?>
                <div class="mws-panel-body no-padding">
                    <div class="mws-form-message info"><?php echo (isset($this->_rootref['CHECK_DONATED'])) ? $this->_rootref['CHECK_DONATED'] : ''; ?></div>
                </div>
                <?php if ($this->_rootref['WARNINGREPORT']) {  ?>
                    <div class="mws-panel-body no-padding">
                        	<div class="mws-form-message error">
                            	<?php echo ((isset($this->_rootref['L_3500_1015581'])) ? $this->_rootref['L_3500_1015581'] : ((isset($MSG['3500_1015581'])) ? $MSG['3500_1015581'] : '{ L_3500_1015581 }')); ?>
                                <ul>
                                	<li><?php echo (isset($this->_rootref['WARNINGMESSAGE'])) ? $this->_rootref['WARNINGMESSAGE'] : ''; ?></li>
                                </ul>
                            </div>
                    </div>
                    <?php } if ($this->_rootref['ERROR'] != ('')) {  ?>
                    <div class="mws-panel-body no-padding">
                        	<div class="mws-form-message info">
                            	<?php echo (isset($this->_rootref['ERROR'])) ? $this->_rootref['ERROR'] : ''; ?>
                            </div>
                    </div>
                    <?php } if ($this->_rootref['THIS_VERSION'] == $this->_rootref['REALVERSION']) {  ?>
                    	<div class="mws-panel-body no-padding">
                     	   	<div class="mws-form-message info">
                     	       	<?php echo ((isset($this->_rootref['L_30_0212'])) ? $this->_rootref['L_30_0212'] : ((isset($MSG['30_0212'])) ? $MSG['30_0212'] : '{ L_30_0212 }')); ?>
                            </div>
                    	</div>
                    <?php } else { ?>
                    	<div class="mws-panel-body no-padding">
                        	<div class="mws-form-message error">
                            	<?php echo ((isset($this->_rootref['L_30_0211'])) ? $this->_rootref['L_30_0211'] : ((isset($MSG['30_0211'])) ? $MSG['30_0211'] : '{ L_30_0211 }')); ?>
                            </div>
                    	</div>
                    <?php } ?>
                </div>