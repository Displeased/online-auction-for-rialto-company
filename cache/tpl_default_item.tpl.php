<link rel="Stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>js/counter/item/jquery.countdown.css">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>themes/<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>/css/elastislide.css">
<link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>themes/<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>/css/base/advanced-slider-base.css" media="screen">

<script type="text/javascript" src="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>themes/<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>/js/jquery.tmpl.min.js"></script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>themes/<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>themes/<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>/js/jquery.elastislide.js"></script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>js/counter/item/jquery.countdown.js"></script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>js/counter/item/jquery.countdown-<?php echo (isset($this->_rootref['LANGUAGE'])) ? $this->_rootref['LANGUAGE'] : ''; ?>.js"></script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>themes/<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>/js/gallery.js"></script>
<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
	<div class="rg-image-wrapper">
		{{if itemsCount > 1}}
			<div class="rg-image-nav">
				<a href="#" class="rg-image-nav-prev">Previous Image</a>
				<a href="#" class="rg-image-nav-next">Next Image</a>
			</div>
		{{/if}}
		<div class="rg-image"></div>
		<div class="rg-loading"></div>
		<div class="rg-caption-wrapper">
			<div class="rg-caption" style="display:none;">
				<p></p>
			</div>
		</div>
	</div>
</script>
<?php if ($this->_rootref['TQTY'] > (1)) {  ?>
<script type="text/javascript">
$(document).ready(function(){
	$("#qty").keydown(function(){
		$("#bidcost").text(($("#qty").val())*($("#bid").val()) + ' <?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?>');
	});
	$("#bid").keydown(function(){
		$("#bidcost").text(($("#qty").val())*($("#bid").val()) + ' <?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?>');
	});
});
</script>

<?php } ?>
<!--adding 4 latest bottom-->
  <div class="breadcrumb"><?php echo ((isset($this->_rootref['L_041'])) ? $this->_rootref['L_041'] : ((isset($MSG['041'])) ? $MSG['041'] : '{ L_041 }')); ?>: <?php echo (isset($this->_rootref['CATSPATH'])) ? $this->_rootref['CATSPATH'] : ''; ?></div>
  <?php if ($this->_rootref['SECCATSPATH'] != ('')) {  ?>
  <div class="breadcrumb"><?php echo ((isset($this->_rootref['L_814'])) ? $this->_rootref['L_814'] : ((isset($MSG['814'])) ? $MSG['814'] : '{ L_814 }')); ?>: <?php echo (isset($this->_rootref['SECCATSPATH'])) ? $this->_rootref['SECCATSPATH'] : ''; ?></div>
  <?php } if ($this->_rootref['B_USERBID']) {  ?>
<div class="alert alert-success"> <?php echo (isset($this->_rootref['YOURBIDMSG'])) ? $this->_rootref['YOURBIDMSG'] : ''; ?> </div>
<?php } if ($this->_rootref['B_CANEDIT']) {  ?>
<div class="form-actions"> <a class="btn btn-primary" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>edit_active_auction.php?id=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>"><?php echo ((isset($this->_rootref['L_30_0069'])) ? $this->_rootref['L_30_0069'] : ((isset($MSG['30_0069'])) ? $MSG['30_0069'] : '{ L_30_0069 }')); ?></a> </div>
<?php } ?>
<div class="row">
<div class="span12">
<div class="row">
<div class="span12">
<ul class="inline pull-right" style="margin-bottom:8px;">
  <?php if ($this->_rootref['B_CANCONTACTSELLER']) {  ?>
  <li><small><a class="btn btn-mini" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>send_email.php?auction_id=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>"><i class="icon-question-sign"></i> <?php echo ((isset($this->_rootref['L_922'])) ? $this->_rootref['L_922'] : ((isset($MSG['922'])) ? $MSG['922'] : '{ L_922 }')); ?></a></small> </li>
  <?php } if ($this->_rootref['B_LOGGED_IN']) {  ?>
  <li><small><a class="btn btn-mini" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>item_watch.php?<?php echo (isset($this->_rootref['WATCH_VAR'])) ? $this->_rootref['WATCH_VAR'] : ''; ?>=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>"><i class="icon-eye-open"></i> <?php echo (isset($this->_rootref['WATCH_STRING'])) ? $this->_rootref['WATCH_STRING'] : ''; ?></a></small></li>
  <?php } else { ?>
  <li><small><a class="btn btn-mini" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>user_login.php?"><i class="icon-eye-open"></i> <?php echo ((isset($this->_rootref['L_5202'])) ? $this->_rootref['L_5202'] : ((isset($MSG['5202'])) ? $MSG['5202'] : '{ L_5202 }')); ?></a></small></li>
  <?php } ?>
  <li><small><a class="btn btn-mini" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>friend.php?id=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>"><i class="icon-user"></i> <?php echo ((isset($this->_rootref['L_106'])) ? $this->_rootref['L_106'] : ((isset($MSG['106'])) ? $MSG['106'] : '{ L_106 }')); ?></a></small></li>
  <li><small><a class="new-window btn btn-mini" href="https://www.facebook.com/sharer.php?u=<?php echo (isset($this->_rootref['FB_URL'])) ? $this->_rootref['FB_URL'] : ''; ?>&amp;t=<?php echo (isset($this->_rootref['FB_TITLE'])) ? $this->_rootref['FB_TITLE'] : ''; echo (isset($this->_rootref['FB_PRICE'])) ? $this->_rootref['FB_PRICE'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_350_10167'])) ? $this->_rootref['L_350_10167'] : ((isset($MSG['350_10167'])) ? $MSG['350_10167'] : '{ L_350_10167 }')); ?>"><i class="icon-user"></i> <?php echo ((isset($this->_rootref['L_350_10167'])) ? $this->_rootref['L_350_10167'] : ((isset($MSG['350_10167'])) ? $MSG['350_10167'] : '{ L_350_10167 }')); ?></a></small></li> 
  <li><small><a class="new-window btn btn-mini" href="https://twitter.com/home?status=<?php echo (isset($this->_rootref['FB_TITLE'])) ? $this->_rootref['FB_TITLE'] : ''; ?>-<?php echo (isset($this->_rootref['FB_URL'])) ? $this->_rootref['FB_URL'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_350_10168'])) ? $this->_rootref['L_350_10168'] : ((isset($MSG['350_10168'])) ? $MSG['350_10168'] : '{ L_350_10168 }')); ?>" rel="nofollow" target="_blank"><i class="icon-user"></i> <?php echo ((isset($this->_rootref['L_350_10168'])) ? $this->_rootref['L_350_10168'] : ((isset($MSG['350_10168'])) ? $MSG['350_10168'] : '{ L_350_10168 }')); ?></a></small></li>
  <li><small><a class="new-window btn btn-mini" href="https://plus.google.com/share?url=<?php echo (isset($this->_rootref['FB_URL'])) ? $this->_rootref['FB_URL'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_350_10169'])) ? $this->_rootref['L_350_10169'] : ((isset($MSG['350_10169'])) ? $MSG['350_10169'] : '{ L_350_10169 }')); ?>" target="_blank"><i class="icon-user"></i> <?php echo ((isset($this->_rootref['L_350_10169'])) ? $this->_rootref['L_350_10169'] : ((isset($MSG['350_10169'])) ? $MSG['350_10169'] : '{ L_350_10169 }')); ?></a></small></li>
</ul>

<div class="clearfix"></div>
<div class="row">
	<div class="span3" style="text-align:center">
	<img class="img-polaroid" src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; echo (isset($this->_rootref['PIC_URL'])) ? $this->_rootref['PIC_URL'] : ''; ?>" border="0" align="center"><br />
</div>
<div class="span5">
  	<h3> <?php echo (isset($this->_rootref['TITLE'])) ? $this->_rootref['TITLE'] : ''; ?> </h3>
  	<?php if ($this->_rootref['SUBTITLE'] != ('')) {  ?>
  	<h5><?php echo (isset($this->_rootref['SUBTITLE'])) ? $this->_rootref['SUBTITLE'] : ''; ?></h5>
  	<?php } ?>

	<ul class="nav nav-tabs">
		<li class="active"><a href="#details" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015484'])) ? $this->_rootref['L_3500_1015484'] : ((isset($MSG['3500_1015484'])) ? $MSG['3500_1015484'] : '{ L_3500_1015484 }')); ?></a></li>
		<li><a href="#shipping" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015485'])) ? $this->_rootref['L_3500_1015485'] : ((isset($MSG['3500_1015485'])) ? $MSG['3500_1015485'] : '{ L_3500_1015485 }')); ?></a></li>
		<li><a href="#seller" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015486'])) ? $this->_rootref['L_3500_1015486'] : ((isset($MSG['3500_1015486'])) ? $MSG['3500_1015486'] : '{ L_3500_1015486 }')); ?></a></li>
		<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo ((isset($this->_rootref['L_3500_1015487'])) ? $this->_rootref['L_3500_1015487'] : ((isset($MSG['3500_1015487'])) ? $MSG['3500_1015487'] : '{ L_3500_1015487 }')); ?> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<div class="span2">
					<div class="row">
						<div class="span2">
							<div class="single-cat-dopdown ">
								<a href="#adi" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015491'])) ? $this->_rootref['L_3500_1015491'] : ((isset($MSG['3500_1015491'])) ? $MSG['3500_1015491'] : '{ L_3500_1015491 }')); ?></a> 
							</div>
							<?php if ($this->_rootref['B_SHOWHISTORY']) {  ?>
							<div class="single-cat-dopdown ">
								<a href="#history" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015490'])) ? $this->_rootref['L_3500_1015490'] : ((isset($MSG['3500_1015490'])) ? $MSG['3500_1015490'] : '{ L_3500_1015490 }')); ?></a> 
							</div>
							<?php } if ($this->_rootref['B_CONDITION']) {  ?>
							<div class="single-cat-dopdown ">
								<a href="#Item_Condition" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015488'])) ? $this->_rootref['L_3500_1015488'] : ((isset($MSG['3500_1015488'])) ? $MSG['3500_1015488'] : '{ L_3500_1015488 }')); ?></a> 
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</ul>
		</li>
	</ul>

<div class="tab-content">
	<div class="tab-pane well tab-pane fade in active" id="details">
		<?php if ($this->_rootref['B_FREE_ITEM']) {  ?>
		<?php echo ((isset($this->_rootref['L_3500_1015745'])) ? $this->_rootref['L_3500_1015745'] : ((isset($MSG['3500_1015745'])) ? $MSG['3500_1015745'] : '{ L_3500_1015745 }')); ?>: <?php if ($this->_rootref['B_HASENDED'] == (false)) {  if ($this->_rootref['B_CANCONTACTSELLER']) {  ?><a style="cursor:pointer" class="btn btn-success" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>buy_now.php?id=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>"><?php echo ((isset($this->_rootref['L_3500_1015747'])) ? $this->_rootref['L_3500_1015747'] : ((isset($MSG['3500_1015747'])) ? $MSG['3500_1015747'] : '{ L_3500_1015747 }')); ?></a><?php } } ?><br><br>
	  	<?php } if ($this->_rootref['B_BUY_NOW']) {  ?>
		<?php echo ((isset($this->_rootref['L_496'])) ? $this->_rootref['L_496'] : ((isset($MSG['496'])) ? $MSG['496'] : '{ L_496 }')); ?>: <?php echo (isset($this->_rootref['BUYNOW'])) ? $this->_rootref['BUYNOW'] : ''; ?> <?php if ($this->_rootref['B_HASENDED'] == (false)) {  if ($this->_rootref['B_CANCONTACTSELLER']) {  ?><a style="cursor:pointer" class="btn btn-success" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>buy_now.php?id=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>"><?php echo ((isset($this->_rootref['L_350_1015402'])) ? $this->_rootref['L_350_1015402'] : ((isset($MSG['350_1015402'])) ? $MSG['350_1015402'] : '{ L_350_1015402 }')); ?></a><?php } } ?><br><br>
	  <?php } if ($this->_rootref['B_NOTBNONLY']) {  if ($this->_rootref['ATYPE'] == (2)) {  ?>
	  	<?php echo ((isset($this->_rootref['L_038'])) ? $this->_rootref['L_038'] : ((isset($MSG['038'])) ? $MSG['038'] : '{ L_038 }')); ?>: <?php echo (isset($this->_rootref['MINBID'])) ? $this->_rootref['MINBID'] : ''; ?><br />
	  <?php } ?>
	  	<div class="well" style="padding:4px; margin-bottom:10px;"> <?php echo ((isset($this->_rootref['L_116'])) ? $this->_rootref['L_116'] : ((isset($MSG['116'])) ? $MSG['116'] : '{ L_116 }')); ?>: <?php echo (isset($this->_rootref['MAXBID'])) ? $this->_rootref['MAXBID'] : ''; ?></div>
	  <?php if ($this->_rootref['B_HASRESERVE']) {  ?>
	  	&nbsp;<?php echo ((isset($this->_rootref['L_514'])) ? $this->_rootref['L_514'] : ((isset($MSG['514'])) ? $MSG['514'] : '{ L_514 }')); ?>
	  <?php } ?>
	  	<small><?php echo ((isset($this->_rootref['L_119'])) ? $this->_rootref['L_119'] : ((isset($MSG['119'])) ? $MSG['119'] : '{ L_119 }')); ?>: <?php echo (isset($this->_rootref['NUMBIDS'])) ? $this->_rootref['NUMBIDS'] : ''; ?> <?php echo (isset($this->_rootref['VIEW_HISTORY2'])) ? $this->_rootref['VIEW_HISTORY2'] : ''; ?></small><br />
	  <?php } if ($this->_rootref['B_HASBUYER']) {  ?>
	  	<?php echo ((isset($this->_rootref['L_117'])) ? $this->_rootref['L_117'] : ((isset($MSG['117'])) ? $MSG['117'] : '{ L_117 }')); ?>:
	  <?php $_high_bidders_count = (isset($this->_tpldata['high_bidders'])) ? sizeof($this->_tpldata['high_bidders']) : 0;if ($_high_bidders_count) {for ($_high_bidders_i = 0; $_high_bidders_i < $_high_bidders_count; ++$_high_bidders_i){$_high_bidders_val = &$this->_tpldata['high_bidders'][$_high_bidders_i]; if ($this->_rootref['B_BIDDERPRIV']) {  ?>
	  	<b><?php echo $_high_bidders_val['BUYER_NAME']; ?></b>
	  <?php } else { ?>
	  	<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>profile.php?user_id=<?php echo $_high_bidders_val['BUYER_ID']; ?>&auction_id=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>"><b><?php echo $_high_bidders_val['BUYER_NAME']; ?></b></a> <b>(<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>feedback.php?id=<?php echo $_high_bidders_val['BUYER_ID']; ?>&faction=show"><?php echo $_high_bidders_val['BUYER_FB']; ?></a>)</b>
	  <?php } ?>
	  	<?php echo $_high_bidders_val['BUYER_FB_ICON']; ?><br />
	  <?php }} } ?>
	  	<?php echo ((isset($this->_rootref['L_023'])) ? $this->_rootref['L_023'] : ((isset($MSG['023'])) ? $MSG['023'] : '{ L_023 }')); ?>: <?php echo (isset($this->_rootref['SHIPPING_COST'])) ? $this->_rootref['SHIPPING_COST'] : ''; ?><br />
	  	<?php echo ((isset($this->_rootref['L_026'])) ? $this->_rootref['L_026'] : ((isset($MSG['026'])) ? $MSG['026'] : '{ L_026 }')); ?>: <?php echo (isset($this->_rootref['PAYMENTS'])) ? $this->_rootref['PAYMENTS'] : ''; ?> <br />
	  	<span class="muted" style="font-weight:normal"> <?php echo ((isset($this->_rootref['L_611'])) ? $this->_rootref['L_611'] : ((isset($MSG['611'])) ? $MSG['611'] : '{ L_611 }')); ?> <?php echo (isset($this->_rootref['AUCTION_VIEWS'])) ? $this->_rootref['AUCTION_VIEWS'] : ''; ?> <?php echo ((isset($this->_rootref['L_612'])) ? $this->_rootref['L_612'] : ((isset($MSG['612'])) ? $MSG['612'] : '{ L_612 }')); ?> </span><br />
	  	<div align="center">
	  		<?php if ($this->_rootref['B_COUNTDOWN']) {  ?>
	  			<?php echo ((isset($this->_rootref['L_354'])) ? $this->_rootref['L_354'] : ((isset($MSG['354'])) ? $MSG['354'] : '{ L_354 }')); ?>
	  		<?php } else { ?>
	  		<span style="color:red"><b><?php echo ((isset($this->_rootref['L_118'])) ? $this->_rootref['L_118'] : ((isset($MSG['118'])) ? $MSG['118'] : '{ L_118 }')); ?>:</b></span><br />
	  			<div class="well"><div id="2show<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>"></div ></div>
	  				<script type="text/javascript">
						$(function () {
						var time  = new Date();
						$('#2show<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>').countdown({until: '<?php echo (isset($this->_rootref['ENDS_IN'])) ? $this->_rootref['ENDS_IN'] : ''; ?>'}); 
						});
					</script>
	  		<?php } ?>
	  	</div>
	</div>
  
  <div class="tab-pane tab-pane fade in" id="shipping">
  	<table class="table table-condensed table-striped table-bordered">
  		<tr>
  			<td width="30%"><?php echo ((isset($this->_rootref['L_023'])) ? $this->_rootref['L_023'] : ((isset($MSG['023'])) ? $MSG['023'] : '{ L_023 }')); ?>:</td>
  			<td><?php echo (isset($this->_rootref['SHIPPING_COST'])) ? $this->_rootref['SHIPPING_COST'] : ''; ?></td>
  		</tr>
  		<?php if ($this->_rootref['B_ADDITIONAL_SHIPPING_COST'] || $this->_rootref['B_BUY_NOW_ONLY']) {  ?>
  		<tr>
  			<td width="30%"><?php echo ((isset($this->_rootref['L_350_1008'])) ? $this->_rootref['L_350_1008'] : ((isset($MSG['350_1008'])) ? $MSG['350_1008'] : '{ L_350_1008 }')); ?>:</td>
  			<td><?php echo (isset($this->_rootref['ADDITIONAL_SHIPPING_COST'])) ? $this->_rootref['ADDITIONAL_SHIPPING_COST'] : ''; ?></td>
  		</tr>
  		<?php } if ($this->_rootref['CITY'] != ('')) {  ?>
  		<tr>
  			<td width="30%"><?php echo ((isset($this->_rootref['L_010'])) ? $this->_rootref['L_010'] : ((isset($MSG['010'])) ? $MSG['010'] : '{ L_010 }')); ?>:</td>
  			<td><?php echo (isset($this->_rootref['CITY'])) ? $this->_rootref['CITY'] : ''; ?></td>
  		</tr>
		<?php } if ($this->_rootref['COUNTRY'] != ('') || $this->_rootref['ZIP'] != ('')) {  ?>
  		<tr>
  			<td width="30%"><?php echo ((isset($this->_rootref['L_014'])) ? $this->_rootref['L_014'] : ((isset($MSG['014'])) ? $MSG['014'] : '{ L_014 }')); ?>:</td>
  			<td><?php echo (isset($this->_rootref['COUNTRY'])) ? $this->_rootref['COUNTRY'] : ''; ?> (<?php echo (isset($this->_rootref['ZIP'])) ? $this->_rootref['ZIP'] : ''; ?>)</td>
  		</tr>
		<?php } if ($this->_rootref['B_SHIPPING_CONDITIONS'] == (y)) {  ?>
		<tr>
  			<td width="30%"><?php echo ((isset($this->_rootref['L_025'])) ? $this->_rootref['L_025'] : ((isset($MSG['025'])) ? $MSG['025'] : '{ L_025 }')); ?>:</td>
  			<td><?php echo (isset($this->_rootref['SHIPPING'])) ? $this->_rootref['SHIPPING'] : ''; ?>, <?php echo (isset($this->_rootref['INTERNATIONAL'])) ? $this->_rootref['INTERNATIONAL'] : ''; ?></td>
  		</tr>
  		<?php } if ($this->_rootref['B_SHIPPING_TERMS'] == (y)) {  if ($this->_rootref['SHIPPINGTERMS'] != ('')) {  ?>
	  		<tr>
	  			<td width="30%"><?php echo ((isset($this->_rootref['L_25_0215'])) ? $this->_rootref['L_25_0215'] : ((isset($MSG['25_0215'])) ? $MSG['25_0215'] : '{ L_25_0215 }')); ?>:</td>
	  			<td><?php echo (isset($this->_rootref['SHIPPINGTERMS'])) ? $this->_rootref['SHIPPINGTERMS'] : ''; ?></td>
	  		</tr>
	  		<?php } } ?>
  		<tr>
  			<td width="30%"><?php echo ((isset($this->_rootref['L_025_C'])) ? $this->_rootref['L_025_C'] : ((isset($MSG['025_C'])) ? $MSG['025_C'] : '{ L_025_C }')); ?>:</td>
  			<td><?php echo (isset($this->_rootref['RETURNS'])) ? $this->_rootref['RETURNS'] : ''; ?></td>
  		</tr>
  	</table>
  </div>
  
  <div class="tab-pane tab-pane fade in" id="adi">
  <table class="table table-condensed table-striped table-bordered">
  <tr>
  	<td><?php echo ((isset($this->_rootref['L_113'])) ? $this->_rootref['L_113'] : ((isset($MSG['113'])) ? $MSG['113'] : '{ L_113 }')); ?>:</td>
  	<td><?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?></td>
  </tr>
  <!-- auction type -->
  <tr>
  	<td><?php echo ((isset($this->_rootref['L_261'])) ? $this->_rootref['L_261'] : ((isset($MSG['261'])) ? $MSG['261'] : '{ L_261 }')); ?>:</td>
  	<td><?php echo (isset($this->_rootref['AUCTION_TYPE'])) ? $this->_rootref['AUCTION_TYPE'] : ''; ?></td>
  </tr>
  <!-- higher bidder -->
  <tr>
  	<td><?php if ($this->_rootref['ATYPE'] == (1)) {  echo ((isset($this->_rootref['L_127'])) ? $this->_rootref['L_127'] : ((isset($MSG['127'])) ? $MSG['127'] : '{ L_127 }')); } else { echo ((isset($this->_rootref['L_038'])) ? $this->_rootref['L_038'] : ((isset($MSG['038'])) ? $MSG['038'] : '{ L_038 }')); } ?>:</td>
  	<td><?php echo (isset($this->_rootref['MINBID'])) ? $this->_rootref['MINBID'] : ''; ?></td>
  </tr>
  <tr>
  	<td><?php echo ((isset($this->_rootref['L_111'])) ? $this->_rootref['L_111'] : ((isset($MSG['111'])) ? $MSG['111'] : '{ L_111 }')); ?>:</td>
  	<td><?php echo (isset($this->_rootref['STARTTIME'])) ? $this->_rootref['STARTTIME'] : ''; ?></td>
  </tr>
  <tr>
  	<td><?php echo ((isset($this->_rootref['L_112'])) ? $this->_rootref['L_112'] : ((isset($MSG['112'])) ? $MSG['112'] : '{ L_112 }')); ?>:</td>
  	<td><?php echo (isset($this->_rootref['ENDTIME'])) ? $this->_rootref['ENDTIME'] : ''; ?></td>
  </tr>
  <?php if ($this->_rootref['QTY'] > (1)) {  ?>
  <tr>
  	<td><?php echo ((isset($this->_rootref['L_901'])) ? $this->_rootref['L_901'] : ((isset($MSG['901'])) ? $MSG['901'] : '{ L_901 }')); ?>:</td>
  	<td><?php echo (isset($this->_rootref['QTY'])) ? $this->_rootref['QTY'] : ''; ?></td>
  </tr>
  <?php } if ($this->_rootref['B_HASENDED']) {  ?>
  <tr>
  	<td><?php echo ((isset($this->_rootref['L_904'])) ? $this->_rootref['L_904'] : ((isset($MSG['904'])) ? $MSG['904'] : '{ L_904 }')); ?></td>
  	<td>&nbsp;</td>
  </tr>
  <?php } ?>
</table>
  </div>
  <?php if ($this->_rootref['B_CONDITION']) {  ?>
<div class="tab-pane tab-pane fade in" id="Item_Condition">
	<table class="table table-condensed table-striped table-bordered">
	<?php if ($this->_rootref['B_ITEM_CONDITION']) {  ?>
		<tr>
			<td width="35%"><?php echo ((isset($this->_rootref['L_103600'])) ? $this->_rootref['L_103600'] : ((isset($MSG['103600'])) ? $MSG['103600'] : '{ L_103600 }')); ?>:</td>
			<td><?php echo (isset($this->_rootref['ITEM_CONDITION'])) ? $this->_rootref['ITEM_CONDITION'] : ''; ?></td>
		</tr>
	<?php } if ($this->_rootref['B_ITEM_MANUFACTURER']) {  ?>
		<tr>
			<td width="35%"><?php echo ((isset($this->_rootref['L_103700'])) ? $this->_rootref['L_103700'] : ((isset($MSG['103700'])) ? $MSG['103700'] : '{ L_103700 }')); ?>:</td>
			<td><?php echo (isset($this->_rootref['ITEM_MANUFACTURER'])) ? $this->_rootref['ITEM_MANUFACTURER'] : ''; ?></td>
		</tr>
	<?php } if ($this->_rootref['B_ITEM_MODEL']) {  ?>
		<tr>
			<td width="35%"><?php echo ((isset($this->_rootref['L_103800'])) ? $this->_rootref['L_103800'] : ((isset($MSG['103800'])) ? $MSG['103800'] : '{ L_103800 }')); ?>:</td>
			<td><?php echo (isset($this->_rootref['ITEM_MODEL'])) ? $this->_rootref['ITEM_MODEL'] : ''; ?></td>
		</tr>
	<?php } if ($this->_rootref['B_ITEM_COLOUR']) {  ?>
		<tr>
			<td width="35%"><?php echo ((isset($this->_rootref['L_103900'])) ? $this->_rootref['L_103900'] : ((isset($MSG['103900'])) ? $MSG['103900'] : '{ L_103900 }')); ?>:</td>
			<td><?php echo (isset($this->_rootref['ITEM_COLOUR'])) ? $this->_rootref['ITEM_COLOUR'] : ''; ?></td>
		</tr>
  	<?php } if ($this->_rootref['B_ITEM_YEAR']) {  ?>
  		<tr>
  			<td width="35%"><?php echo ((isset($this->_rootref['L_104000'])) ? $this->_rootref['L_104000'] : ((isset($MSG['104000'])) ? $MSG['104000'] : '{ L_104000 }')); ?>:</td>
  			<td><?php echo (isset($this->_rootref['ITEM_YEAR'])) ? $this->_rootref['ITEM_YEAR'] : ''; ?></td>
  		</tr>
  	<?php } ?>
  		<tr>
			<td width="35%"><?php echo ((isset($this->_rootref['L_104400'])) ? $this->_rootref['L_104400'] : ((isset($MSG['104400'])) ? $MSG['104400'] : '{ L_104400 }')); ?></td>
			<td><?php echo (isset($this->_rootref['CONDITION_DESCRIPTION'])) ? $this->_rootref['CONDITION_DESCRIPTION'] : ''; ?></td>
		</tr>
	</table>
</div>
<?php } ?>
<div class="tab-pane tab-pane fade well in" id="seller">
	<span><?php if ($this->_rootref['AVATAR'] != ('')) {  ?>
		<img src="<?php echo (isset($this->_rootref['AVATAR'])) ? $this->_rootref['AVATAR'] : ''; ?>" border="0" style="float: left" hspace="5"><?php } else { ?><img src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>/uploaded/avatar/default.gif" border="0" style="float: left" hspace="5"><?php } ?></span>
		<a href='<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>profile.php?user_id=<?php echo (isset($this->_rootref['SELLER_ID'])) ? $this->_rootref['SELLER_ID'] : ''; ?>&auction_id=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>'><b><?php echo (isset($this->_rootref['SELLER_NICK'])) ? $this->_rootref['SELLER_NICK'] : ''; ?></b></a> (<a href='<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>feedback.php?id=<?php echo (isset($this->_rootref['SELLER_ID'])) ? $this->_rootref['SELLER_ID'] : ''; ?>&faction=show'><?php echo (isset($this->_rootref['SELLER_TOTALFB'])) ? $this->_rootref['SELLER_TOTALFB'] : ''; ?></a>)
    	<?php echo (isset($this->_rootref['SELLER_FBICON'])) ? $this->_rootref['SELLER_FBICON'] : ''; ?>
		<ul class="seller-list">
			<li><?php if ($this->_rootref['IS_ONLINE']) {  ?><img src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>images/online.png"><?php echo ((isset($this->_rootref['L_350_10111'])) ? $this->_rootref['L_350_10111'] : ((isset($MSG['350_10111'])) ? $MSG['350_10111'] : '{ L_350_10111 }')); } else { ?><img src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>images/offline.png"><?php echo ((isset($this->_rootref['L_350_10112'])) ? $this->_rootref['L_350_10112'] : ((isset($MSG['350_10112'])) ? $MSG['350_10112'] : '{ L_350_10112 }')); } ?></li>
			<li><b><?php echo ((isset($this->_rootref['L_5506'])) ? $this->_rootref['L_5506'] : ((isset($MSG['5506'])) ? $MSG['5506'] : '{ L_5506 }')); echo (isset($this->_rootref['SELLER_FBPOS'])) ? $this->_rootref['SELLER_FBPOS'] : ''; ?></b></li>
			<li><small><?php echo ((isset($this->_rootref['L_5509'])) ? $this->_rootref['L_5509'] : ((isset($MSG['5509'])) ? $MSG['5509'] : '{ L_5509 }')); echo (isset($this->_rootref['SELLER_NUMFB'])) ? $this->_rootref['SELLER_NUMFB'] : ''; echo ((isset($this->_rootref['L__0151'])) ? $this->_rootref['L__0151'] : ((isset($MSG['_0151'])) ? $MSG['_0151'] : '{ L__0151 }')); ?></small></li>
			<?php if ($this->_rootref['SELLER_FBNEG'] != 0) {  ?>
				<li><small><?php echo (isset($this->_rootref['SELLER_FBNEG'])) ? $this->_rootref['SELLER_FBNEG'] : ''; ?></small></li>
			<?php } ?>
			<li><small><?php echo ((isset($this->_rootref['L_5508'])) ? $this->_rootref['L_5508'] : ((isset($MSG['5508'])) ? $MSG['5508'] : '{ L_5508 }')); echo (isset($this->_rootref['SELLER_REG'])) ? $this->_rootref['SELLER_REG'] : ''; ?></small></li>
			<small><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>active_auctions.php?user_id=<?php echo (isset($this->_rootref['SELLER_ID'])) ? $this->_rootref['SELLER_ID'] : ''; ?>"><?php echo ((isset($this->_rootref['L_213'])) ? $this->_rootref['L_213'] : ((isset($MSG['213'])) ? $MSG['213'] : '{ L_213 }')); ?></a> </small>
			<?php if ($this->_rootref['B_CANCONTACTSELLER']) {  ?><br>
				<?php if ($this->_rootref['B_SETFSM']) {  ?>
				<form name="fav" action="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo (isset($this->_rootref['SEO_TITLE'])) ? $this->_rootref['SEO_TITLE'] : ''; ?>-<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>" method="post" enctype="multipart/form-data">
				  	<input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">       	
					<input type="hidden" name="faveseller" value="yes">
					<input class="btn btn-small btn-primary" type="submit" name="report_auction" value="<?php echo ((isset($this->_rootref['L_FSM1'])) ? $this->_rootref['L_FSM1'] : ((isset($MSG['FSM1'])) ? $MSG['FSM1'] : '{ L_FSM1 }')); ?>" title="<?php echo ((isset($this->_rootref['L_FSM1'])) ? $this->_rootref['L_FSM1'] : ((isset($MSG['FSM1'])) ? $MSG['FSM1'] : '{ L_FSM1 }')); ?>">
				</form>
				<?php } ?><small><font color='red'><?php echo (isset($this->_rootref['FSM'])) ? $this->_rootref['FSM'] : ''; ?></font></small>
			<?php } ?>
		</ul>
</div>

<div class="tab-pane tab-pane fade in" id="history">
    <?php if ($this->_rootref['B_SHOWHISTORY']) {  ?>
      <table class="table table-condensed table-striped table-bordered">
        <tr>
          <th width="33%" align="center"><small><?php echo ((isset($this->_rootref['L_176'])) ? $this->_rootref['L_176'] : ((isset($MSG['176'])) ? $MSG['176'] : '{ L_176 }')); ?></small></th>
          <th width="33%" align="center"><small><?php echo ((isset($this->_rootref['L_130'])) ? $this->_rootref['L_130'] : ((isset($MSG['130'])) ? $MSG['130'] : '{ L_130 }')); ?></small></th>
          <th width="33%" align="center"><small><?php echo ((isset($this->_rootref['L_175'])) ? $this->_rootref['L_175'] : ((isset($MSG['175'])) ? $MSG['175'] : '{ L_175 }')); ?></small></th>
          <?php if ($this->_rootref['ATYPE'] == (2)) {  ?>
          <th width="33%" align="center"><small><?php echo ((isset($this->_rootref['L_284'])) ? $this->_rootref['L_284'] : ((isset($MSG['284'])) ? $MSG['284'] : '{ L_284 }')); ?></small></th>
          <?php } ?>
        </tr>
        <?php $_bidhistory_count = (isset($this->_tpldata['bidhistory'])) ? sizeof($this->_tpldata['bidhistory']) : 0;if ($_bidhistory_count) {for ($_bidhistory_i = 0; $_bidhistory_i < $_bidhistory_count; ++$_bidhistory_i){$_bidhistory_val = &$this->_tpldata['bidhistory'][$_bidhistory_i]; ?>
        <tr valign="top" <?php echo $_bidhistory_val['BGCOLOUR']; ?>>
          <td><?php if ($this->_rootref['B_BIDDERPRIV']) {  ?>
            <small><?php echo $_bidhistory_val['NAME']; ?></small>
            <?php } else { ?>
            <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>profile.php?user_id=<?php echo $_bidhistory_val['ID']; ?>"><small><?php echo $_bidhistory_val['NAME']; ?></small></a>
            <?php } ?>
          </td>
          <td align="center"><small><?php echo $_bidhistory_val['BID']; ?></small> </td>
          <td align="center"><small><?php echo $_bidhistory_val['WHEN']; ?></small> </td>
          <?php if ($this->_rootref['ATYPE'] == (2)) {  ?>
          <td align="center"><small><?php echo $_bidhistory_val['QTY']; ?></small> </td>
          <?php } ?>
        </tr>
        <?php }} ?>
      </table>
    <?php } ?>
    </div>
</div></div>
<?php $this->_tpl_include('bid.tpl'); ?>
<div class="span12">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#description" data-toggle="tab"><?php echo ((isset($this->_rootref['L_018'])) ? $this->_rootref['L_018'] : ((isset($MSG['018'])) ? $MSG['018'] : '{ L_018 }')); ?></a></li>
		<?php if ($this->_rootref['B_HASGALELRY']) {  ?>
		<li><a href="#Photo_Gallery" data-toggle="tab"><?php echo ((isset($this->_rootref['L_663'])) ? $this->_rootref['L_663'] : ((isset($MSG['663'])) ? $MSG['663'] : '{ L_663 }')); ?></a></li>
		<?php if ($this->_rootref['B_HAS_QUESTIONS']) {  ?>
		<li><a href="#question" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015489'])) ? $this->_rootref['L_3500_1015489'] : ((isset($MSG['3500_1015489'])) ? $MSG['3500_1015489'] : '{ L_3500_1015489 }')); ?></a></li>
		<?php } } ?>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade in active" id="description">
			<div class="well"><h3><?php echo ((isset($this->_rootref['L_018'])) ? $this->_rootref['L_018'] : ((isset($MSG['018'])) ? $MSG['018'] : '{ L_018 }')); ?></h3>
			<?php echo (isset($this->_rootref['AUCTION_DESCRIPTION'])) ? $this->_rootref['AUCTION_DESCRIPTION'] : ''; ?></div>
		</div>
		<div class="tab-pane tab-pane fade in" id="question">
			<?php if ($this->_rootref['B_HAS_QUESTIONS']) {  $_questions_count = (isset($this->_tpldata['questions'])) ? sizeof($this->_tpldata['questions']) : 0;if ($_questions_count) {for ($_questions_i = 0; $_questions_i < $_questions_count; ++$_questions_i){$_questions_val = &$this->_tpldata['questions'][$_questions_i]; ?>
		  	<table class="table table-condensed table-striped table-bordered">
		  	<tr>
		  		<td><strong><?php echo ((isset($this->_rootref['L_5239'])) ? $this->_rootref['L_5239'] : ((isset($MSG['5239'])) ? $MSG['5239'] : '{ L_5239 }')); ?></strong></td>
		  	</tr>
		  	<?php $_conv_count = (isset($_questions_val['conv'])) ? sizeof($_questions_val['conv']) : 0;if ($_conv_count) {for ($_conv_i = 0; $_conv_i < $_conv_count; ++$_conv_i){$_conv_val = &$_questions_val['conv'][$_conv_i]; ?>
		  	<tr>
		   		<td><strong><?php echo $_conv_val['BY_WHO']; ?>:</strong> <small><?php echo $_conv_val['MESSAGE']; ?></small></td>
		   	</tr>
		   	<?php }} ?>
		   	</table>
		  	<?php }} } ?>
		</div>

	<?php if ($this->_rootref['B_HASGALELRY']) {  ?>
	<div class="tab-pane fade in" id="Photo_Gallery">
		<div id="rg-gallery" class="rg-gallery well">
			<div class="rg-thumbs">
			<!-- Elastislide Carousel Thumbnail Viewer -->
				<div class="es-carousel-wrapper">
					<div class="es-nav">
						<span class="es-nav-prev"><?php echo ((isset($this->_rootref['L_3500_1015527'])) ? $this->_rootref['L_3500_1015527'] : ((isset($MSG['3500_1015527'])) ? $MSG['3500_1015527'] : '{ L_3500_1015527 }')); ?></span>
						<span class="es-nav-next"><?php echo ((isset($this->_rootref['L_3500_1015528'])) ? $this->_rootref['L_3500_1015528'] : ((isset($MSG['3500_1015528'])) ? $MSG['3500_1015528'] : '{ L_3500_1015528 }')); ?></span>
					</div>
					<div class="es-carousel">
						<ul>
						<?php $_gallery_count = (isset($this->_tpldata['gallery'])) ? sizeof($this->_tpldata['gallery']) : 0;if ($_gallery_count) {for ($_gallery_i = 0; $_gallery_i < $_gallery_count; ++$_gallery_i){$_gallery_val = &$this->_tpldata['gallery'][$_gallery_i]; ?>
							<li><a href="#"><img src="<?php echo $_gallery_val['THUMB_V']; ?>" data-large="<?php echo $_gallery_val['V']; ?>" data-description="" /></a></li>
						<?php }} ?>
						</ul>
					</div>
				</div>
				<!-- End Elastislide Carousel Thumbnail Viewer -->
			</div><!-- rg-thumbs -->
		</div><!-- rg-gallery -->
	</div>
	<?php } ?>	  	
</div></div>