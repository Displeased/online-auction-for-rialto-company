<div class="row">
<?php $this->_tpl_include('u_header.tpl'); ?>
<script type="text/javascript">
$(document).ready(function() {
	
	var relist_fee = <?php echo (isset($this->_rootref['RELIST_FEE_NO'])) ? $this->_rootref['RELIST_FEE_NO'] : ''; ?>;
	
	$("#sellall").click(function() {
		var checked_status = this.checked;
        $("input[id=sellitem]").each(function()
        { 
        	this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
        });	
	});
	
	$("#relistall").click(function() {
		var checked_status = this.checked;
        $("input[class=relistid]").each(function()
        { 
        	this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
        });	
	});

	$("#deleteall").click(function() {
		var checked_status = this.checked;
		$("input[id=deleteitem]").each(function()
        { 
            this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
		});	
	});

	$("#processdel").submit(function() {
		if (confirm('<?php echo ((isset($this->_rootref['L_30_0087'])) ? $this->_rootref['L_30_0087'] : ((isset($MSG['30_0087'])) ? $MSG['30_0087'] : '{ L_30_0087 }')); ?>')){
			return true;
		} else {
			return false;
		}
	});

	$(".relistid").click(function(){
		var n = $(".relistid:checked").length;
		$("#to_pay").text(parseFloat(n * relist_fee));
	});
});
</script>


<div class="span9">
  <legend><?php echo ((isset($this->_rootref['L_204'])) ? $this->_rootref['L_204'] : ((isset($MSG['204'])) ? $MSG['204'] : '{ L_204 }')); ?></legend>
  <form name="closed" method="post" action="" id="processdel">
    <input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
    <?php if ($this->_rootref['B_RELIST_FEE']) {  ?>
    <?php echo ((isset($this->_rootref['L_437'])) ? $this->_rootref['L_437'] : ((isset($MSG['437'])) ? $MSG['437'] : '{ L_437 }')); ?>: <?php echo (isset($this->_rootref['RELIST_FEE'])) ? $this->_rootref['RELIST_FEE'] : ''; ?> - <?php echo ((isset($this->_rootref['L_189'])) ? $this->_rootref['L_189'] : ((isset($MSG['189'])) ? $MSG['189'] : '{ L_189 }')); ?>: <span id="to_pay">0.00</span>
    <?php } ?>
    <small><span class="muted"><?php echo ((isset($this->_rootref['L_5117'])) ? $this->_rootref['L_5117'] : ((isset($MSG['5117'])) ? $MSG['5117'] : '{ L_5117 }')); ?>&nbsp;<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>&nbsp;<?php echo ((isset($this->_rootref['L_5118'])) ? $this->_rootref['L_5118'] : ((isset($MSG['5118'])) ? $MSG['5118'] : '{ L_5118 }')); ?>&nbsp;<?php echo (isset($this->_rootref['PAGES'])) ? $this->_rootref['PAGES'] : ''; ?></span></small>
    <table class="table table-bordered table-condense table-striped">
      <tr>
        <td  width="40%"><small><a href="yourauctions_c.php?ca_ord=title&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo ((isset($this->_rootref['L_624'])) ? $this->_rootref['L_624'] : ((isset($MSG['624'])) ? $MSG['624'] : '{ L_624 }')); ?></a></small>
          <?php if ($this->_rootref['ORDERCOL'] == ('title')) {  ?>
          <a href="yourauctions_c.php?ca_ord=title&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></a>
          <?php } ?>
        </td>
        <td  width="10%"><small><a href="yourauctions_c.php?ca_ord=starts&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo ((isset($this->_rootref['L_625'])) ? $this->_rootref['L_625'] : ((isset($MSG['625'])) ? $MSG['625'] : '{ L_625 }')); ?></a></small>
          <?php if ($this->_rootref['ORDERCOL'] == ('starts')) {  ?>
          <small><a href="yourauctions_c.php?ca_ord=starts&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></a></small>
          <?php } ?>
        </td>
        <td class="hidden-phone"  width="10%"><small><a href="yourauctions_c.php?ca_ord=ends&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo ((isset($this->_rootref['L_626'])) ? $this->_rootref['L_626'] : ((isset($MSG['626'])) ? $MSG['626'] : '{ L_626 }')); ?></a></small>
          <?php if ($this->_rootref['ORDERCOL'] == ('ends')) {  ?>
          <a href="yourauctions_c.php?ca_ord=ends&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></a>
          <?php } ?>
        </td>
        <td  width="10%" align="center"><small><a href="yourauctions_c.php?ca_ord=num_bids&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo ((isset($this->_rootref['L_627'])) ? $this->_rootref['L_627'] : ((isset($MSG['627'])) ? $MSG['627'] : '{ L_627 }')); ?></a></small>
          <?php if ($this->_rootref['ORDERCOL'] == ('num_bids')) {  ?>
          <a href="yourauctions_c.php?ca_ord=num_bids&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></a>
          <?php } ?>
        </td>
        <td class="hidden-phone"  width="11%" align="center"><small><a href="yourauctions_c.php?ca_ord=current_bid&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo ((isset($this->_rootref['L_628'])) ? $this->_rootref['L_628'] : ((isset($MSG['628'])) ? $MSG['628'] : '{ L_628 }')); ?></a></small>
          <?php if ($this->_rootref['ORDERCOL'] == ('current_bid')) {  ?>
          <a href="yourauctions_c.php?ca_ord=current_bid&ca_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></a>
          <?php } ?>
        </td>
        <td  width="10%" align="center"><small><?php echo ((isset($this->_rootref['L_630'])) ? $this->_rootref['L_630'] : ((isset($MSG['630'])) ? $MSG['630'] : '{ L_630 }')); ?></small> </td>
        <td  width="10%" align="center"><small><?php echo ((isset($this->_rootref['L_25_0209'])) ? $this->_rootref['L_25_0209'] : ((isset($MSG['25_0209'])) ? $MSG['25_0209'] : '{ L_25_0209 }')); ?></small> </td>
        <td  width="9%" align="center" bgcolor=><small><?php echo ((isset($this->_rootref['L_008'])) ? $this->_rootref['L_008'] : ((isset($MSG['008'])) ? $MSG['008'] : '{ L_008 }')); ?></small> </td>
      </tr>
      
      <?php $_items_count = (isset($this->_tpldata['items'])) ? sizeof($this->_tpldata['items']) : 0;if ($_items_count) {for ($_items_i = 0; $_items_i < $_items_count; ++$_items_i){$_items_val = &$this->_tpldata['items'][$_items_i]; ?>
      <tr>
        <td width="40%"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_items_val['SEO_TITLE']; ?>-<?php echo $_items_val['ID']; ?>"><?php echo $_items_val['TITLE']; ?></a> </td>
        <td width="10%"><small><?php echo $_items_val['STARTS']; ?></small> </td>
        <td class="hidden-phone" width="10%"><small><?php echo $_items_val['ENDS']; ?></small> </td>
        <td class="hidden-phone" width="10%"  align="center"><small><?php echo $_items_val['BIDS']; ?></small> </td>
        <td width="11%"  align="center"><small><?php echo $_items_val['BID']; ?></small> </td>
        <td width="10%"  align="center"><?php if ($_items_val['B_CANRELIST'] && $this->_rootref['B_AUTORELIST']) {  ?>
          <input type="checkbox" name="relist[]" value="<?php echo $_items_val['ID']; ?>" class="relistid">
          <?php } else { if ($_items_val['B_CANRELIST']) {  ?>
          <a href="sellsimilar.php?id=<?php echo $_items_val['ID']; ?>&relist=1"><?php echo ((isset($this->_rootref['L_2__0051'])) ? $this->_rootref['L_2__0051'] : ((isset($MSG['2__0051'])) ? $MSG['2__0051'] : '{ L_2__0051 }')); ?></a>
          <?php } else { ?>
          <a href="sellsimilar.php?id=<?php echo $_items_val['ID']; ?>"><?php echo ((isset($this->_rootref['L_2__0050'])) ? $this->_rootref['L_2__0050'] : ((isset($MSG['2__0050'])) ? $MSG['2__0050'] : '{ L_2__0050 }')); ?></a>
          <?php } } ?>
        </td>
        <td width="10%"  align="center"><?php if ($_items_val['B_CANSSELL']) {  ?>
          <input type="checkbox" name="sell[]" value="<?php echo $_items_val['ID']; ?>" id="sellitem">
          <?php } ?>
        </td>
        <td width="9%"  align="center"><?php if ($_items_val['B_HASNOBIDS']) {  ?>
          <input type="checkbox" name="delete[]" value="<?php echo $_items_val['ID']; ?>" id="deleteitem">
          <?php } ?>
        </td>
      </tr>
      <?php }} ?>
      
      <tr class="hidden-phone">
        <td  colspan="5" style="text-align:right"><span class="muted"><small><?php echo ((isset($this->_rootref['L_30_0102'])) ? $this->_rootref['L_30_0102'] : ((isset($MSG['30_0102'])) ? $MSG['30_0102'] : '{ L_30_0102 }')); ?></small></span></td>
        <td align="center"><input type="checkbox" id="relistall"></td>
        <td align="center"><input type="checkbox" id="sellall"></td>
        <td align="center"><input type="checkbox" id="deleteall"></td>
      </tr>
      <tr>
        <td colspan="10" style="text-align:center"><input type="hidden" name="action" value="update">
          <input type="submit" name="Submit" value="<?php echo ((isset($this->_rootref['L_631'])) ? $this->_rootref['L_631'] : ((isset($MSG['631'])) ? $MSG['631'] : '{ L_631 }')); ?>" class="btn btn-primary">
        </td>
      </tr>
    </table>
  </form>
  <div class="pagination pagination-centered">
    <ul>
      <li><?php echo (isset($this->_rootref['PREV'])) ? $this->_rootref['PREV'] : ''; ?></li>
      <?php $_pages_count = (isset($this->_tpldata['pages'])) ? sizeof($this->_tpldata['pages']) : 0;if ($_pages_count) {for ($_pages_i = 0; $_pages_i < $_pages_count; ++$_pages_i){$_pages_val = &$this->_tpldata['pages'][$_pages_i]; ?>
      <li><?php echo $_pages_val['PAGE']; ?></li>
      <?php }} ?>
      <li><?php echo (isset($this->_rootref['NEXT'])) ? $this->_rootref['NEXT'] : ''; ?></li>
    </ul>
  </div>
<?php $this->_tpl_include('user_menu_footer.tpl'); ?>