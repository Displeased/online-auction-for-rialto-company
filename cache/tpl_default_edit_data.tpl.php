<div class="row">
<?php $this->_tpl_include('u_header.tpl'); ?>
<div class="span9">
  <?php if ($this->_rootref['B_MENUTITLE']) {  ?>
  <legend><?php echo (isset($this->_rootref['UCP_TITLE'])) ? $this->_rootref['UCP_TITLE'] : ''; ?></legend>
  <?php } if ($this->_rootref['B_ISERROR']) {  ?>
  <div class="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo (isset($this->_rootref['UCP_ERROR'])) ? $this->_rootref['UCP_ERROR'] : ''; ?> </div>
  <?php } ?>
  <div class="well">
    <h2> 
    <?php if ($this->_rootref['AVATAR'] != ('')) {  ?><img src="<?php echo (isset($this->_rootref['AVATAR'])) ? $this->_rootref['AVATAR'] : ''; ?>" border="0" style="float: left"><?php } else { ?><img src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>/uploaded/avatar/default.gif" border="0" style="float: left"><?php } ?>
    <?php echo ((isset($this->_rootref['L_200'])) ? $this->_rootref['L_200'] : ((isset($MSG['200'])) ? $MSG['200'] : '{ L_200 }')); ?> <?php echo (isset($this->_rootref['NAME'])) ? $this->_rootref['NAME'] : ''; ?> <small>( <?php echo (isset($this->_rootref['NICK'])) ? $this->_rootref['NICK'] : ''; ?> )</small>
    <br>
    <form class="hidden-phone" action="avatar_upload.php" method="post" enctype="multipart/form-data" name="form2" id="form2">
  		<small><?php echo ((isset($this->_rootref['L_350_10025'])) ? $this->_rootref['L_350_10025'] : ((isset($MSG['350_10025'])) ? $MSG['350_10025'] : '{ L_350_10025 }')); ?></small><br><br>
  		<div class="well">
  			<input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
        	<input type="file" name="ifile">
            <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo (isset($this->_rootref['MAXUPLOADSIZE'])) ? $this->_rootref['MAXUPLOADSIZE'] : ''; ?>" />
            <input name="Submit"  type="submit" class="btn btn-primary" id="Submit" value="Upload">
		</div>
	</form>
    </h2>
      </div>
  <form name="details" action="" method="post">
    <fieldset>
	    <input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
	    <input type="hidden" name="FB_ids" value="<?php echo (isset($this->_rootref['FBOOK_ID'])) ? $this->_rootref['FBOOK_ID'] : ''; ?>">
	    <div class="row">
	    	<div class="span4 well">
	    		<span class="help-block"><?php echo ((isset($this->_rootref['L_617'])) ? $this->_rootref['L_617'] : ((isset($MSG['617'])) ? $MSG['617'] : '{ L_617 }')); ?></span>
	    		<label><?php echo ((isset($this->_rootref['L_004'])) ? $this->_rootref['L_004'] : ((isset($MSG['004'])) ? $MSG['004'] : '{ L_004 }')); ?></label>
	    		<input type="password" name="TPL_password" size=20 maxlength="20"><?php echo ((isset($this->_rootref['L_050'])) ? $this->_rootref['L_050'] : ((isset($MSG['050'])) ? $MSG['050'] : '{ L_050 }')); ?>
	    		<label><?php echo ((isset($this->_rootref['L_005'])) ? $this->_rootref['L_005'] : ((isset($MSG['005'])) ? $MSG['005'] : '{ L_005 }')); ?></label>
	    		<input type="password" name="TPL_repeat_password" size=20 maxlength=20 />
	    		<label><?php echo ((isset($this->_rootref['L_006'])) ? $this->_rootref['L_006'] : ((isset($MSG['006'])) ? $MSG['006'] : '{ L_006 }')); ?></label>
	    		<input type="text" name="TPL_email" size=50 maxlength=50 value="<?php echo (isset($this->_rootref['EMAIL'])) ? $this->_rootref['EMAIL'] : ''; ?>">
	    	</div>
	    	<?php if ($this->_rootref['B_FBOOK_LOGIN']) {  ?>
	    	<div class="span4 well">
				<span class="help-block alert alert-info"><?php echo ((isset($this->_rootref['L_350_10192'])) ? $this->_rootref['L_350_10192'] : ((isset($MSG['350_10192'])) ? $MSG['350_10192'] : '{ L_350_10192 }')); ?></span><br>
				<?php echo (isset($this->_rootref['FBOOK_EMAIL'])) ? $this->_rootref['FBOOK_EMAIL'] : ''; ?>
	    	</div>
	    	<?php } ?>
	    	</div>
	    	<ul class="nav nav-tabs">
				<li class="active"><a href="#details" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015484'])) ? $this->_rootref['L_3500_1015484'] : ((isset($MSG['3500_1015484'])) ? $MSG['3500_1015484'] : '{ L_3500_1015484 }')); ?></a></li>
				<li><a href="#payment" data-toggle="tab"><?php echo ((isset($this->_rootref['L_719'])) ? $this->_rootref['L_719'] : ((isset($MSG['719'])) ? $MSG['719'] : '{ L_719 }')); ?></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade in active" id="details">
			    	<div class="row" style="margin-left:0;">
			    		<div class="span4">
			        		<label><?php echo ((isset($this->_rootref['L_009'])) ? $this->_rootref['L_009'] : ((isset($MSG['009'])) ? $MSG['009'] : '{ L_009 }')); ?></label>
			        		<input type="text" name="TPL_address" size="40" maxlength=255 value="<?php echo (isset($this->_rootref['ADDRESS'])) ? $this->_rootref['ADDRESS'] : ''; ?>">
			        		<label><?php echo ((isset($this->_rootref['L_010'])) ? $this->_rootref['L_010'] : ((isset($MSG['010'])) ? $MSG['010'] : '{ L_010 }')); ?></label>
			        		<input type="text" name="TPL_city" size="25" maxlength=25 value="<?php echo (isset($this->_rootref['CITY'])) ? $this->_rootref['CITY'] : ''; ?>">
			        		<label><?php echo ((isset($this->_rootref['L_011'])) ? $this->_rootref['L_011'] : ((isset($MSG['011'])) ? $MSG['011'] : '{ L_011 }')); ?></label>
			        		<input type="text" name="TPL_prov" size="10" maxlength=10 value="<?php echo (isset($this->_rootref['PROV'])) ? $this->_rootref['PROV'] : ''; ?>">
			         		<label><?php echo ((isset($this->_rootref['L_252'])) ? $this->_rootref['L_252'] : ((isset($MSG['252'])) ? $MSG['252'] : '{ L_252 }')); ?></label>
			    			<div class="control-group">
			    				<div class="controls register-date">
			    					<?php echo (isset($this->_rootref['DATEFORMAT'])) ? $this->_rootref['DATEFORMAT'] : ''; ?>
			    					<input class="input-small" type="text" name="TPL_year" size="4" maxlength="4" value="<?php echo (isset($this->_rootref['YEAR'])) ? $this->_rootref['YEAR'] : ''; ?>">
			    				</div>
			    			</div>
			      		</div>
			      		<div class="span4">
			        		<label><?php echo ((isset($this->_rootref['L_014'])) ? $this->_rootref['L_014'] : ((isset($MSG['014'])) ? $MSG['014'] : '{ L_014 }')); ?></label>
			        		<select  name="TPL_country"><?php echo (isset($this->_rootref['COUNTRYLIST'])) ? $this->_rootref['COUNTRYLIST'] : ''; ?></select>
			        		<label><?php echo ((isset($this->_rootref['L_012'])) ? $this->_rootref['L_012'] : ((isset($MSG['012'])) ? $MSG['012'] : '{ L_012 }')); ?></label>
			        		<input type="text" name="TPL_zip" size=8 value="<?php echo (isset($this->_rootref['ZIP'])) ? $this->_rootref['ZIP'] : ''; ?>">
			        		<label><?php echo ((isset($this->_rootref['L_013'])) ? $this->_rootref['L_013'] : ((isset($MSG['013'])) ? $MSG['013'] : '{ L_013 }')); ?></label>
			        		<input type="text" name="TPL_phone" size=40 maxlength=40 value="<?php echo (isset($this->_rootref['PHONE'])) ? $this->_rootref['PHONE'] : ''; ?>">
			        		<label><?php echo ((isset($this->_rootref['L_346'])) ? $this->_rootref['L_346'] : ((isset($MSG['346'])) ? $MSG['346'] : '{ L_346 }')); ?></label>
			        		<?php echo (isset($this->_rootref['TIMEZONE'])) ? $this->_rootref['TIMEZONE'] : ''; ?> 
			        	</div>
			    	</div>
			    	<div class="row"  style="margin-left:0;">
			      		<div class="span4">
			      			<label><?php echo ((isset($this->_rootref['L_350_10115'])) ? $this->_rootref['L_350_10115'] : ((isset($MSG['350_10115'])) ? $MSG['350_10115'] : '{ L_350_10115 }')); ?>&nbsp;&nbsp; <?php echo (isset($this->_rootref['IS_ONLINE'])) ? $this->_rootref['IS_ONLINE'] : ''; ?></label>
				  			<label class="switch-light well" onclick="">
			        			<input type="checkbox" name="hide_online" id="hide_online" value="y" <?php echo (isset($this->_rootref['HIDEONLINE'])) ? $this->_rootref['HIDEONLINE'] : ''; ?>>
			        			<span>
									<span>Off</span>
									<span>On</span>
								</span>
								<a class="btn btn-primary"></a>
							</label>
			         		<span class="help-block alert alert-info"><?php echo ((isset($this->_rootref['L_350_10116'])) ? $this->_rootref['L_350_10116'] : ((isset($MSG['350_10116'])) ? $MSG['350_10116'] : '{ L_350_10116 }')); ?></span><br>
			        		<label><?php echo ((isset($this->_rootref['L_352'])) ? $this->_rootref['L_352'] : ((isset($MSG['352'])) ? $MSG['352'] : '{ L_352 }')); ?></label>
			        		<label class="radio">
			        			<input type="radio" name="TPL_emailtype" value="html" <?php echo (isset($this->_rootref['EMAILTYPE1'])) ? $this->_rootref['EMAILTYPE1'] : ''; ?> /><?php echo ((isset($this->_rootref['L_902'])) ? $this->_rootref['L_902'] : ((isset($MSG['902'])) ? $MSG['902'] : '{ L_902 }')); ?> 
			        		</label>
			        		<label class="radio">
			        			<input type="radio" name="TPL_emailtype" value="text" <?php echo (isset($this->_rootref['EMAILTYPE2'])) ? $this->_rootref['EMAILTYPE2'] : ''; ?> /><?php echo ((isset($this->_rootref['L_915'])) ? $this->_rootref['L_915'] : ((isset($MSG['915'])) ? $MSG['915'] : '{ L_915 }')); ?> 
			        		</label>
			      		</div>
			      		<?php if ($this->_rootref['B_NEWLETTER']) {  ?>
			      		<div class="span4">
			        		<label><?php echo ((isset($this->_rootref['L_603'])) ? $this->_rootref['L_603'] : ((isset($MSG['603'])) ? $MSG['603'] : '{ L_603 }')); ?></label>
			        		<label class="radio">
			        			<input type="radio" name="TPL_nletter" value="1" <?php echo (isset($this->_rootref['NLETTER1'])) ? $this->_rootref['NLETTER1'] : ''; ?> /><?php echo ((isset($this->_rootref['L_030'])) ? $this->_rootref['L_030'] : ((isset($MSG['030'])) ? $MSG['030'] : '{ L_030 }')); ?> 
			        		</label>
			        		<label class="radio">
			        			<input type="radio" name="TPL_nletter" value="2" <?php echo (isset($this->_rootref['NLETTER2'])) ? $this->_rootref['NLETTER2'] : ''; ?> /><?php echo ((isset($this->_rootref['L_029'])) ? $this->_rootref['L_029'] : ((isset($MSG['029'])) ? $MSG['029'] : '{ L_029 }')); ?> 
			        		</label>
			        		<span class="help-block alert alert-info"><?php echo ((isset($this->_rootref['L_609'])) ? $this->_rootref['L_609'] : ((isset($MSG['609'])) ? $MSG['609'] : '{ L_609 }')); ?></span> 
			        	</div>
			    	<?php } ?> 
			    	</div>
			    </div>
			    <div class="tab-pane tab-pane fade in" id="payment"> 
			    	<div class="row" style="margin-left:0;">
			      		<div class="span4">
			    			<?php if ($this->_rootref['B_PAYPAL']) {  ?>
			    			<label><?php echo ((isset($this->_rootref['L_720'])) ? $this->_rootref['L_720'] : ((isset($MSG['720'])) ? $MSG['720'] : '{ L_720 }')); ?></label>
			    			<input type="text" name="TPL_pp_email" size=40 value="<?php echo (isset($this->_rootref['PP_EMAIL'])) ? $this->_rootref['PP_EMAIL'] : ''; ?>">
			    			<?php } if ($this->_rootref['B_AUTHNET']) {  ?>
			    			<label><?php echo ((isset($this->_rootref['L_773'])) ? $this->_rootref['L_773'] : ((isset($MSG['773'])) ? $MSG['773'] : '{ L_773 }')); ?></label>
			    			<input type="text" name="TPL_authnet_id" size=40 value="<?php echo (isset($this->_rootref['AN_ID'])) ? $this->_rootref['AN_ID'] : ''; ?>">
			    			<label><?php echo ((isset($this->_rootref['L_774'])) ? $this->_rootref['L_774'] : ((isset($MSG['774'])) ? $MSG['774'] : '{ L_774 }')); ?></label>
			    			<input type="text" name="TPL_authnet_pass" size=40 value="<?php echo (isset($this->_rootref['AN_PASS'])) ? $this->_rootref['AN_PASS'] : ''; ?>">
			    			<?php } if ($this->_rootref['B_WORLDPAY']) {  ?>
			    			<label><?php echo ((isset($this->_rootref['L_824'])) ? $this->_rootref['L_824'] : ((isset($MSG['824'])) ? $MSG['824'] : '{ L_824 }')); ?></label>
			    			<input type="text" name="TPL_worldpay_id" size=40 value="<?php echo (isset($this->_rootref['WP_ID'])) ? $this->_rootref['WP_ID'] : ''; ?>">
			    			<?php } if ($this->_rootref['B_TOOCHECKOUT']) {  ?>
			    			<label><?php echo ((isset($this->_rootref['L_826'])) ? $this->_rootref['L_826'] : ((isset($MSG['826'])) ? $MSG['826'] : '{ L_826 }')); ?></label>
			    			<input type="text" name="TPL_toocheckout_id" size=40 value="<?php echo (isset($this->_rootref['TC_ID'])) ? $this->_rootref['TC_ID'] : ''; ?>">
			   	 			<?php } ?>
			    		</div> 
			    		<div class="span4">
			    			<?php if ($this->_rootref['B_MONEYBOOKERS']) {  ?>
			    			<label><?php echo ((isset($this->_rootref['L_825'])) ? $this->_rootref['L_825'] : ((isset($MSG['825'])) ? $MSG['825'] : '{ L_825 }')); ?></label>
			    			<input type="text" name="TPL_skrill_email" size=40 value="<?php echo (isset($this->_rootref['MB_EMAIL'])) ? $this->_rootref['MB_EMAIL'] : ''; ?>">
			    			<?php } if ($this->_rootref['B_BANK_TRANSFER']) {  ?>
			    			<label><?php echo ((isset($this->_rootref['L_30_0216'])) ? $this->_rootref['L_30_0216'] : ((isset($MSG['30_0216'])) ? $MSG['30_0216'] : '{ L_30_0216 }')); ?></label>
			    			<input type="text" name="TPL_bank_name" size=40 maxlength=40 value="<?php echo (isset($this->_rootref['BANK'])) ? $this->_rootref['BANK'] : ''; ?>">
			    			<?php } if ($this->_rootref['B_BANK_TRANSFER']) {  ?>
			    			<label><?php echo ((isset($this->_rootref['L_30_0217'])) ? $this->_rootref['L_30_0217'] : ((isset($MSG['30_0217'])) ? $MSG['30_0217'] : '{ L_30_0217 }')); ?></label>
			        		<input type="text" name="TPL_bank_account" size=40 maxlength=40 value="<?php echo (isset($this->_rootref['BANK_ACCOUNT'])) ? $this->_rootref['BANK_ACCOUNT'] : ''; ?>">
			    			<?php } if ($this->_rootref['B_BANK_TRANSFER']) {  ?>
			    			<label><?php echo ((isset($this->_rootref['L_30_0218'])) ? $this->_rootref['L_30_0218'] : ((isset($MSG['30_0218'])) ? $MSG['30_0218'] : '{ L_30_0218 }')); ?></label>
			        		<input type="text" name="TPL_bank_routing" size=40 maxlength=40 value="<?php echo (isset($this->_rootref['BANK_ROUTING'])) ? $this->_rootref['BANK_ROUTING'] : ''; ?>">
							<?php } ?>
			    		</div>
			    	</div>
			    </div>
			</div>
   	 		<br />
    		<div class="form-actions">
      			<button  type="submit" name="Input" class="btn btn-primary"><?php echo ((isset($this->_rootref['L_530'])) ? $this->_rootref['L_530'] : ((isset($MSG['530'])) ? $MSG['530'] : '{ L_530 }')); ?></button>
      			<input type="reset" name="Input" class="btn">
      			<input type="hidden" name="action" value="update">
    		</div>
    </fieldset>
</form>
<?php $this->_tpl_include('user_menu_footer.tpl'); ?>