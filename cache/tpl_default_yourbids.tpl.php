<div class="row">
<?php $this->_tpl_include('u_header.tpl'); ?>
<div class="span9">
  <table class="table table-bordered table-center table-striped">
    <tr>
      <th> <?php echo ((isset($this->_rootref['L_168'])) ? $this->_rootref['L_168'] : ((isset($MSG['168'])) ? $MSG['168'] : '{ L_168 }')); ?> </th>
      <th> <?php echo ((isset($this->_rootref['L_461'])) ? $this->_rootref['L_461'] : ((isset($MSG['461'])) ? $MSG['461'] : '{ L_461 }')); ?> </th>
      <th> <?php echo ((isset($this->_rootref['L_171'])) ? $this->_rootref['L_171'] : ((isset($MSG['171'])) ? $MSG['171'] : '{ L_171 }')); ?> </th>
    </tr>
    <?php $_bids_count = (isset($this->_tpldata['bids'])) ? sizeof($this->_tpldata['bids']) : 0;if ($_bids_count) {for ($_bids_i = 0; $_bids_i < $_bids_count; ++$_bids_i){$_bids_val = &$this->_tpldata['bids'][$_bids_i]; ?>
    <tr <?php echo $_bids_val['BGCOLOUR']; ?>>
      <td><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_bids_val['SEO_TITLE']; ?>-<?php echo $_bids_val['ID']; ?>"><?php echo $_bids_val['TITLE']; ?></a> </td>
      <td> <?php echo $_bids_val['BID']; ?>
        <?php if ($_bids_val['QTY'] > (1)) {  ?>
        (x <?php echo $_bids_val['QTY']; ?> <?php echo ((isset($this->_rootref['L_5492'])) ? $this->_rootref['L_5492'] : ((isset($MSG['5492'])) ? $MSG['5492'] : '{ L_5492 }')); ?>)
        <?php } if ($_bids_val['PROXYBID'] != ('')) {  ?>
        <p><small><?php echo $_bids_val['PROXYBID']; ?></small></p>
        <?php } ?>
      </td>
      <td> <?php echo $_bids_val['TIMELEFT']; ?> </td>
    </tr>
    <?php }} if ($this->_rootref['NUM_BIDS'] == 0) {  ?>
    <tr>
      <td><?php echo ((isset($this->_rootref['L_3500_1015547'])) ? $this->_rootref['L_3500_1015547'] : ((isset($MSG['3500_1015547'])) ? $MSG['3500_1015547'] : '{ L_3500_1015547 }')); ?></td>
    </tr>
    <?php } ?>
  </table>
  <span class="alert"><?php echo ((isset($this->_rootref['L_30_0098'])) ? $this->_rootref['L_30_0098'] : ((isset($MSG['30_0098'])) ? $MSG['30_0098'] : '{ L_30_0098 }')); ?></span> </div>
<?php $this->_tpl_include('user_menu_footer.tpl'); ?>