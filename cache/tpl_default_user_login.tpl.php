<div class="span8 offset2 well">
  <legend><?php echo ((isset($this->_rootref['L_181'])) ? $this->_rootref['L_181'] : ((isset($MSG['181'])) ? $MSG['181'] : '{ L_181 }')); ?></legend>
  <form name="user_login" action="<?php echo (isset($this->_rootref['SSLURL'])) ? $this->_rootref['SSLURL'] : ''; ?>user_login.php" method="post" class="form-horizontal" enctype="multipart/form-data">
    <?php if ($this->_rootref['ERROR'] != ('')) {  ?>
    <div class="alert"><?php echo (isset($this->_rootref['ERROR'])) ? $this->_rootref['ERROR'] : ''; ?></div>
    <?php } ?>
    <div class="control-group">
      <label class="control-label" for="inputEmail"><?php echo ((isset($this->_rootref['L_187'])) ? $this->_rootref['L_187'] : ((isset($MSG['187'])) ? $MSG['187'] : '{ L_187 }')); ?></label>
      <div class="controls">
        <input type="text" name="username" size="20" maxlength="20" value="<?php echo (isset($this->_rootref['USER'])) ? $this->_rootref['USER'] : ''; ?>">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="inputEmail"><?php echo ((isset($this->_rootref['L_004'])) ? $this->_rootref['L_004'] : ((isset($MSG['004'])) ? $MSG['004'] : '{ L_004 }')); ?></label>
      <div class="controls">
        <input type="password" name="password" size="20" maxlength="20" value="">
      </div>
    </div>
    <div class="controls">
      <label class="checkbox">
      <input type="checkbox" name="rememberme" value="1">
      &nbsp;<?php echo ((isset($this->_rootref['L_25_0085'])) ? $this->_rootref['L_25_0085'] : ((isset($MSG['25_0085'])) ? $MSG['25_0085'] : '{ L_25_0085 }')); ?> </label>
  	  <label class="checkbox">
      <input type="checkbox" id="hide_online" name="hide_online" value="y">
      <?php echo ((isset($this->_rootref['L_350_10114'])) ? $this->_rootref['L_350_10114'] : ((isset($MSG['350_10114'])) ? $MSG['350_10114'] : '{ L_350_10114 }')); ?></label>

      <a class="btn btn-danger" href="forgotpasswd.php"><?php echo ((isset($this->_rootref['L_215'])) ? $this->_rootref['L_215'] : ((isset($MSG['215'])) ? $MSG['215'] : '{ L_215 }')); ?></a>
    </div>
    <div class="form-actions">
      <button type="submit" class="btn btn-primary"><?php echo ((isset($this->_rootref['L_052'])) ? $this->_rootref['L_052'] : ((isset($MSG['052'])) ? $MSG['052'] : '{ L_052 }')); ?></button>
      <a class="btn btn-info" href="<?php echo (isset($this->_rootref['SSLURL'])) ? $this->_rootref['SSLURL'] : ''; ?>new_account"><?php echo ((isset($this->_rootref['L_235'])) ? $this->_rootref['L_235'] : ((isset($MSG['235'])) ? $MSG['235'] : '{ L_235 }')); ?></a>
      <?php if ($this->_rootref['B_FBOOK_LOGIN']) {  ?>
    	<a class="btn btn-primary" id="facebook" href="#" data-content="<?php echo ((isset($this->_rootref['L_350_10193'])) ? $this->_rootref['L_350_10193'] : ((isset($MSG['350_10193'])) ? $MSG['350_10193'] : '{ L_350_10193 }')); ?>" data-original-title="<?php echo ((isset($this->_rootref['L_350_10204_d'])) ? $this->_rootref['L_350_10204_d'] : ((isset($MSG['350_10204_d'])) ? $MSG['350_10204_d'] : '{ L_350_10204_d }')); ?>" data-trigger="hover" onclick="FBUserLogin();"><?php echo ((isset($this->_rootref['L_350_10204_d'])) ? $this->_rootref['L_350_10204_d'] : ((isset($MSG['350_10204_d'])) ? $MSG['350_10204_d'] : '{ L_350_10204_d }')); ?></a>
      <?php } ?>
      <input type="hidden" name="action" value="login">
    </div>
  </form>