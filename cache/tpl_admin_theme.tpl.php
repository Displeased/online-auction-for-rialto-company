<?php $this->_tpl_include('header.tpl'); if ($this->_rootref['B_EDIT_FILE']) {  ?>
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> <?php echo ((isset($this->_rootref['L_26_0003'])) ? $this->_rootref['L_26_0003'] : ((isset($MSG['26_0003'])) ? $MSG['26_0003'] : '{ L_26_0003 }')); ?></span>
                    </div>
                    <form name="payments" action="" method="post">
                    <input type="hidden" name="filename" value="<?php echo (isset($this->_rootref['FILENAME'])) ? $this->_rootref['FILENAME'] : ''; ?>">
                    <input type="hidden" name="theme" value="<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>">
                    <input type="hidden" name="action" value="<?php if ($this->_rootref['FILENAME'] != ('')) {  ?>edit<?php } else { ?>add<?php } ?>">
                    <input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="<?php echo ((isset($this->_rootref['L_089'])) ? $this->_rootref['L_089'] : ((isset($MSG['089'])) ? $MSG['089'] : '{ L_089 }')); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td><?php echo ((isset($this->_rootref['L_812'])) ? $this->_rootref['L_812'] : ((isset($MSG['812'])) ? $MSG['812'] : '{ L_812 }')); ?></td>
                                    <td>
                                    	<?php if ($this->_rootref['FILENAME'] != ('')) {  ?><b><?php echo (isset($this->_rootref['FILENAME'])) ? $this->_rootref['FILENAME'] : ''; ?></b><?php } else { ?><input type="text" name="new_filename" value="" style="width:600px;"><?php } ?>
                            </tr>
                            <tr>
                                	<td><?php echo ((isset($this->_rootref['L_813'])) ? $this->_rootref['L_813'] : ((isset($MSG['813'])) ? $MSG['813'] : '{ L_813 }')); ?></td>
                                    <td><textarea style="width:600px; height:400px;" name="content"><?php echo (isset($this->_rootref['FILECONTENTS'])) ? $this->_rootref['FILECONTENTS'] : ''; ?></textarea>
</td>
                           </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
           <?php } ?>
           <div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icon-picture"></i> <?php echo ((isset($this->_rootref['L_30_0031a'])) ? $this->_rootref['L_30_0031a'] : ((isset($MSG['30_0031a'])) ? $MSG['30_0031a'] : '{ L_30_0031a }')); ?></span>
                    </div>
                    <form name="logo" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="update_logo">
                    <input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="<?php echo ((isset($this->_rootref['L_3500_1015665'])) ? $this->_rootref['L_3500_1015665'] : ((isset($MSG['3500_1015665'])) ? $MSG['3500_1015665'] : '{ L_3500_1015665 }')); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                                <tr>
                                        <td><img height="120px" src="<?php echo (isset($this->_rootref['IMAGEURL'])) ? $this->_rootref['IMAGEURL'] : ''; ?>"></td>
                               </tr>
                               <tr>
                                    <td><?php echo ((isset($this->_rootref['L_602'])) ? $this->_rootref['L_602'] : ((isset($MSG['602'])) ? $MSG['602'] : '{ L_602 }')); ?></td>
                               </tr>
                               <tr>
                               		<td>
                                    <input type="file" name="logo" size="40">
                                </td>
                               </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
           		<div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icol-arrow-refresh"></i> <?php echo ((isset($this->_rootref['L_3500_1015466'])) ? $this->_rootref['L_3500_1015466'] : ((isset($MSG['3500_1015466'])) ? $MSG['3500_1015466'] : '{ L_3500_1015466 }')); ?></span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
                    <input type="hidden" name="action" value="clear_cache">
                    <div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="<?php echo ((isset($this->_rootref['L_30_0031'])) ? $this->_rootref['L_30_0031'] : ((isset($MSG['30_0031'])) ? $MSG['30_0031'] : '{ L_30_0031 }')); ?>">
                            </div>
                        </div>
                    </div>
                    </form>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                                <tr>
                                    <td><?php echo ((isset($this->_rootref['L_30_0032'])) ? $this->_rootref['L_30_0032'] : ((isset($MSG['30_0032'])) ? $MSG['30_0032'] : '{ L_30_0032 }')); ?></td>
                                </tr>      
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-business-card"></i> <?php echo ((isset($this->_rootref['L_26_0002'])) ? $this->_rootref['L_26_0002'] : ((isset($MSG['26_0002'])) ? $MSG['26_0002'] : '{ L_26_0002 }')); ?></span>
                    </div>
                    <form name="theme" action="" method="post">
                    <input type="hidden" name="action" value="update" id="action">
                    <input type="hidden" name="theme" value="" id="theme">
                    <input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="" class="btn btn-success" value="<?php echo ((isset($this->_rootref['L_26_0000'])) ? $this->_rootref['L_26_0000'] : ((isset($MSG['26_0000'])) ? $MSG['26_0000'] : '{ L_26_0000 }')); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <?php $_themes_count = (isset($this->_tpldata['themes'])) ? sizeof($this->_tpldata['themes']) : 0;if ($_themes_count) {for ($_themes_i = 0; $_themes_i < $_themes_count; ++$_themes_i){$_themes_val = &$this->_tpldata['themes'][$_themes_i]; ?>
                                <tr>
                                     <td>
                                        	<?php if ($_themes_val['B_NOTADMIN']) {  ?>
                                        	<input type="radio" name="dtheme" value="<?php echo $_themes_val['NAME']; ?>" <?php if ($_themes_val['B_CHECKED']) {  ?>checked="checked" <?php } ?>/>
                            				<b><?php echo $_themes_val['NAME']; ?></b>
											<?php } else { ?>
        									<b><?php echo ((isset($this->_rootref['L_841'])) ? $this->_rootref['L_841'] : ((isset($MSG['841'])) ? $MSG['841'] : '{ L_841 }')); ?></b>
											<?php } ?>
                                    </td>
                                    <td>
                                    	<p><a href="theme.php?do=listfiles&theme=<?php echo $_themes_val['NAME']; ?>"><?php echo ((isset($this->_rootref['L_26_0003'])) ? $this->_rootref['L_26_0003'] : ((isset($MSG['26_0003'])) ? $MSG['26_0003'] : '{ L_26_0003 }')); ?></a></p>
                            			<p><a href="theme.php?do=addfile&theme=<?php echo $_themes_val['NAME']; ?>"><?php echo ((isset($this->_rootref['L_26_0004'])) ? $this->_rootref['L_26_0004'] : ((isset($MSG['26_0004'])) ? $MSG['26_0004'] : '{ L_26_0004 }')); ?></a></p>
                                    </td>
                               </tr>
                               <?php if ($_themes_val['B_LISTFILES']) {  ?>
                               <tr>
                               		<td colspan="2">
                                        <select name="file" multiple size="24" style="font-weight:bold; width:350px"
                                     ondblclick="document.getElementById('action').value = ''; document.getElementById('theme').value = '<?php echo $_themes_val['NAME']; ?>'; this.form.submit();">
                    				<?php $_files_count = (isset($_themes_val['files'])) ? sizeof($_themes_val['files']) : 0;if ($_files_count) {for ($_files_i = 0; $_files_i < $_files_count; ++$_files_i){$_files_val = &$_themes_val['files'][$_files_i]; ?>
                                    	<option value="<?php echo $_files_val['FILE']; ?>"><?php echo $_files_val['FILE']; ?></option>
                    				<?php }} ?>
                                    	</select>
                                    </td>
                               </tr>
                               <?php } }} ?>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
           
<?php $this->_tpl_include('footer.tpl'); ?>