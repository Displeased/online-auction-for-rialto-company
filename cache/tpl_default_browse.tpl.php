<ul class="nav nav-tabs">
	<?php if ($this->_rootref['B_FEATURED_ITEMS']) {  ?>
	<li class="active"><a href="#featured-item" data-toggle="tab"><?php echo ((isset($this->_rootref['L_350_10206'])) ? $this->_rootref['L_350_10206'] : ((isset($MSG['350_10206'])) ? $MSG['350_10206'] : '{ L_350_10206 }')); ?></a></li>
	<?php } ?>
	<li <?php if ($this->_rootref['B_FEATURED_ITEMS']) {  } else { ?>class="active"<?php } ?>><a href="#standard-auction" data-toggle="tab"><?php echo ((isset($this->_rootref['L_1021'])) ? $this->_rootref['L_1021'] : ((isset($MSG['1021'])) ? $MSG['1021'] : '{ L_1021 }')); ?></a></li>
</ul>
<div class="tab-content">
<?php if ($this->_rootref['B_FEATURED_ITEMS']) {  ?>
	<div class="tab-pane fade in active" id="featured-item">
		<table class="table table-condensed table-striped table-bordered table-hover">
		  <tr>
		    <th class="tr-image"> <small><?php echo ((isset($this->_rootref['L_741'])) ? $this->_rootref['L_741'] : ((isset($MSG['741'])) ? $MSG['741'] : '{ L_741 }')); ?></small> </th>
		    <th class="tr-title"> <small><?php echo ((isset($this->_rootref['L_017'])) ? $this->_rootref['L_017'] : ((isset($MSG['017'])) ? $MSG['017'] : '{ L_017 }')); ?></small> </th>
		    <th width="15%"> <small><?php echo ((isset($this->_rootref['L_169'])) ? $this->_rootref['L_169'] : ((isset($MSG['169'])) ? $MSG['169'] : '{ L_169 }')); ?></small> </th>
		    <th width="10%" class="hidden-phone tr-bindsno"> <small><?php echo ((isset($this->_rootref['L_319'])) ? $this->_rootref['L_319'] : ((isset($MSG['319'])) ? $MSG['319'] : '{ L_319 }')); ?></small> </th>
		    <th width="8%" class="hidden-phone tr-bindsno"> <small><?php echo ((isset($this->_rootref['L_170'])) ? $this->_rootref['L_170'] : ((isset($MSG['170'])) ? $MSG['170'] : '{ L_170 }')); ?></small> </th>
		    <th width="15%" class="tr-timeleft hidden-phone"> <small><?php echo ((isset($this->_rootref['L_171'])) ? $this->_rootref['L_171'] : ((isset($MSG['171'])) ? $MSG['171'] : '{ L_171 }')); ?></small> </th>
		  </tr>
		  <?php $_featured_items_count = (isset($this->_tpldata['featured_items'])) ? sizeof($this->_tpldata['featured_items']) : 0;if ($_featured_items_count) {for ($_featured_items_i = 0; $_featured_items_i < $_featured_items_count; ++$_featured_items_i){$_featured_items_val = &$this->_tpldata['featured_items'][$_featured_items_i]; ?>
		  <tr class="<?php echo $_featured_items_val['ROWCOLOUR']; ?>"<?php if ($_featured_items_val['B_BOLD']) {  ?>style="font-weight: bold;"<?php } ?>>
		  	<td class="mini-img"><div class="tdb-image"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_featured_items_val['SEO_TITLE']; ?>-<?php echo $_featured_items_val['ID']; ?>"><img src="<?php echo $_featured_items_val['IMAGE']; ?>" style="max-width:<?php echo (isset($this->_rootref['MAXIMAGESIZE'])) ? $this->_rootref['MAXIMAGESIZE'] : ''; ?>px; max-height:<?php echo (isset($this->_rootref['MAXIMAGESIZE'])) ? $this->_rootref['MAXIMAGESIZE'] : ''; ?>px; width: auto; height: auto;"></a></div></td>
		    <td><span class="list-title"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_featured_items_val['SEO_TITLE']; ?>-<?php echo $_featured_items_val['ID']; ?>"><?php echo $_featured_items_val['TITLE']; ?></a></span>
		      <?php if ($this->_rootref['B_SUBTITLE'] && $_featured_items_val['SUBTITLE'] != ('')) {  ?>
		      <br />
		      <span class="truncate-table"> <?php echo $_featured_items_val['SUBTITLE']; ?> </span>
		      <?php } ?>
		    </td>
		    <td> <?php echo $_featured_items_val['BIDFORM']; ?> <em><?php echo $_featured_items_val['BUY_NOW']; ?></em></td>
		    <td class="hidden-phone"><small><?php echo $_featured_items_val['SHIPPING_COST']; ?></small> </td>
		    <td class="hidden-phone"><small><span class="badge badge-success"><?php echo $_featured_items_val['NUMBIDS']; ?></span></small> </td>
		    <td class="hidden-phone"><small><span class="label label-info"><?php echo $_featured_items_val['TIMELEFT']; ?></span></small> </td>
		  </tr>
		  <?php }} ?>
		</table>
	</div>
	<?php } ?>
	<div class="tab-pane fade in <?php if ($this->_rootref['B_FEATURED_ITEMS']) {  } else { ?>active<?php } ?>" id="standard-auction">
		<table  class="table table-condensed table-striped table-bordered table-hover">
		  <tr>
		    <th class="tr-image"> <small><?php echo ((isset($this->_rootref['L_741'])) ? $this->_rootref['L_741'] : ((isset($MSG['741'])) ? $MSG['741'] : '{ L_741 }')); ?></small> </th>
		    <th class="tr-title"> <small><?php echo ((isset($this->_rootref['L_017'])) ? $this->_rootref['L_017'] : ((isset($MSG['017'])) ? $MSG['017'] : '{ L_017 }')); ?></small> </th>
		    <th width="15%"> <small><?php echo ((isset($this->_rootref['L_169'])) ? $this->_rootref['L_169'] : ((isset($MSG['169'])) ? $MSG['169'] : '{ L_169 }')); ?></small> </th>
		    <th width="10%" class="hidden-phone tr-bindsno"> <small><?php echo ((isset($this->_rootref['L_319'])) ? $this->_rootref['L_319'] : ((isset($MSG['319'])) ? $MSG['319'] : '{ L_319 }')); ?></small> </th>
		    <th width="8%" class="hidden-phone tr-bindsno"> <small><?php echo ((isset($this->_rootref['L_170'])) ? $this->_rootref['L_170'] : ((isset($MSG['170'])) ? $MSG['170'] : '{ L_170 }')); ?></small> </th>
		    <th width="15%" class="tr-timeleft  hidden-phone"> <small><?php echo ((isset($this->_rootref['L_171'])) ? $this->_rootref['L_171'] : ((isset($MSG['171'])) ? $MSG['171'] : '{ L_171 }')); ?></small> </th>
		  </tr>
		  <?php $_items_count = (isset($this->_tpldata['items'])) ? sizeof($this->_tpldata['items']) : 0;if ($_items_count) {for ($_items_i = 0; $_items_i < $_items_count; ++$_items_i){$_items_val = &$this->_tpldata['items'][$_items_i]; ?>
		  <tr class="<?php echo $_items_val['ROWCOLOUR']; ?>"  <?php if ($_items_val['B_BOLD']) {  ?>style="font-weight: bold;"<?php } ?>>
		  	<td  class="mini-img"><div class="tdb-image"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_items_val['SEO_TITLE']; ?>-<?php echo $_items_val['ID']; ?>"><img src="<?php echo $_items_val['IMAGE']; ?>" style="max-width:<?php echo (isset($this->_rootref['MAXIMAGESIZE'])) ? $this->_rootref['MAXIMAGESIZE'] : ''; ?>px; max-height:<?php echo (isset($this->_rootref['MAXIMAGESIZE'])) ? $this->_rootref['MAXIMAGESIZE'] : ''; ?>px; width: auto; height: auto;"></a></div></td>
		    <td><span class="list-title"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_items_val['SEO_TITLE']; ?>-<?php echo $_items_val['ID']; ?>"><?php echo $_items_val['TITLE']; ?></a></span>
		      <?php if ($this->_rootref['B_SUBTITLE'] && $_items_val['SUBTITLE'] != ('')) {  ?>
		      <br />
		      <span class="truncate-table"> <?php echo $_items_val['SUBTITLE']; ?> </span>
		      <?php } ?>
		    </td>
		    <td> <?php echo $_items_val['BIDFORM']; ?> <em><?php echo $_items_val['BUY_NOW']; ?></em> </td>
		    <td class="hidden-phone"><small><?php echo $_items_val['SHIPPING_COST']; ?></small> </td>
		    <td  class="hidden-phone"  style="text-align:center;"><small><span class="badge badge-success"><?php echo $_items_val['NUMBIDS']; ?></span></small> </td>
		    <td class="hidden-phone"><small><span class="label label-info"><?php echo $_items_val['TIMELEFT']; ?></span></small> </td>
		  </tr>
		  <?php }} ?>
		</table>
	</div>
</div>

<div class="pagination pagination-centered">
  <ul>
    <li><?php echo (isset($this->_rootref['PREV'])) ? $this->_rootref['PREV'] : ''; ?></li>
    <?php $_pages_count = (isset($this->_tpldata['pages'])) ? sizeof($this->_tpldata['pages']) : 0;if ($_pages_count) {for ($_pages_i = 0; $_pages_i < $_pages_count; ++$_pages_i){$_pages_val = &$this->_tpldata['pages'][$_pages_i]; ?>
    <li><?php echo $_pages_val['PAGE']; ?></li>
    <?php }} ?>
    <li><?php echo (isset($this->_rootref['NEXT'])) ? $this->_rootref['NEXT'] : ''; ?></li>
  </ul>
</div>
</div>