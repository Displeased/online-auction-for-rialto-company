<?php $this->_tpl_include('sell_js.tpl'); ?>
<div class="well">
  <legend> <?php echo (isset($this->_rootref['TITLE'])) ? $this->_rootref['TITLE'] : ''; ?></legend>
  <a name="goto"></a>
  <?php if ($this->_rootref['PAGE'] == 0) {  ?>
  <form name="sell" class="form-horizontal"  action="<?php echo (isset($this->_rootref['ASSLURL'])) ? $this->_rootref['ASSLURL'] : ''; ?>sell.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
    <?php if ($this->_rootref['ERROR'] != ('')) {  ?>
    <div class="alert"><?php echo (isset($this->_rootref['ERROR'])) ? $this->_rootref['ERROR'] : ''; ?></div>
    <?php } ?>
    <div class="control-group">
      <label class="control-label"><span class="muted"><?php echo ((isset($this->_rootref['L_287'])) ? $this->_rootref['L_287'] : ((isset($MSG['287'])) ? $MSG['287'] : '{ L_287 }')); ?></span></label>
      <div class="controls">
        <div style="padding:5px;"> <?php echo (isset($this->_rootref['CAT_LIST1'])) ? $this->_rootref['CAT_LIST1'] : ''; ?>
          <?php if ($this->_rootref['CAT_LIST2'] != ('')) {  ?>
          <br /><br /><?php echo (isset($this->_rootref['CAT_LIST2'])) ? $this->_rootref['CAT_LIST2'] : ''; ?>
          <?php } ?>
          <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>select_category.php?change=yes" class="btn btn-small btn-info"><?php echo ((isset($this->_rootref['L_5113'])) ? $this->_rootref['L_5113'] : ((isset($MSG['5113'])) ? $MSG['5113'] : '{ L_5113 }')); ?></a> </div>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_017'])) ? $this->_rootref['L_017'] : ((isset($MSG['017'])) ? $MSG['017'] : '{ L_017 }')); ?></label>
      <div class="controls">
        <input type="text" name="title" size="40" maxlength="70" value="<?php echo (isset($this->_rootref['AUC_TITLE'])) ? $this->_rootref['AUC_TITLE'] : ''; ?>">
      </div>
    </div>
    <?php if ($this->_rootref['B_SUBTITLE']) {  ?>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_806'])) ? $this->_rootref['L_806'] : ((isset($MSG['806'])) ? $MSG['806'] : '{ L_806 }')); ?></label>
      <div class="controls">
        <input type="text" name="subtitle" id="subtitle" size="40" maxlength="70" value="<?php echo (isset($this->_rootref['AUC_SUBTITLE'])) ? $this->_rootref['AUC_SUBTITLE'] : ''; ?>">
      </div>
    </div>
    <?php } if ($this->_rootref['B_CONDITION']) {  ?>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_103600'])) ? $this->_rootref['L_103600'] : ((isset($MSG['103600'])) ? $MSG['103600'] : '{ L_103600 }')); ?></label>
      <div class="controls">
        <?php echo (isset($this->_rootref['ITEM_CONDITION'])) ? $this->_rootref['ITEM_CONDITION'] : ''; ?> 
		  <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>meanings.php" alt="description meanings" data-fancybox-type="iframe" class="btn converter btn-small btn-info"><?php echo ((isset($this->_rootref['L_104400'])) ? $this->_rootref['L_104400'] : ((isset($MSG['104400'])) ? $MSG['104400'] : '{ L_104400 }')); ?></a>
	  </div>
	</div>
	 <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_103700'])) ? $this->_rootref['L_103700'] : ((isset($MSG['103700'])) ? $MSG['103700'] : '{ L_103700 }')); ?></label>
      <div class="controls">
 		<input type="text" name="item_manufacturer" id="item_manufacturer" size="32" maxlength="32" value="<?php echo (isset($this->_rootref['ITEM_MANUFACTURER'])) ? $this->_rootref['ITEM_MANUFACTURER'] : ''; ?>"> 
 	  </div>
	</div>
	<div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_103800'])) ? $this->_rootref['L_103800'] : ((isset($MSG['103800'])) ? $MSG['103800'] : '{ L_103800 }')); ?></label>
      <div class="controls">
 		<input type="text" name="item_model" id="item_model" size="32" maxlength="32" value="<?php echo (isset($this->_rootref['ITEM_MODEL'])) ? $this->_rootref['ITEM_MODEL'] : ''; ?>"> 
 	  </div>
	</div>
	<div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_103900'])) ? $this->_rootref['L_103900'] : ((isset($MSG['103900'])) ? $MSG['103900'] : '{ L_103900 }')); ?></label>
      <div class="controls">
 		<input type="text" name="item_colour" id="item_colour" size="32" maxlength="32" value="<?php echo (isset($this->_rootref['ITEM_COLOUR'])) ? $this->_rootref['ITEM_COLOUR'] : ''; ?>"> 
 	  </div>
	</div>
	<div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_104000'])) ? $this->_rootref['L_104000'] : ((isset($MSG['104000'])) ? $MSG['104000'] : '{ L_104000 }')); ?></label>
      <div class="controls">
 		<input type="text" name="item_year" id="item_year" size="4" maxlength="4" value="<?php echo (isset($this->_rootref['ITEM_YEAR'])) ? $this->_rootref['ITEM_YEAR'] : ''; ?>"> 
 	  </div>
	</div>
	<?php } ?>

    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_018'])) ? $this->_rootref['L_018'] : ((isset($MSG['018'])) ? $MSG['018'] : '{ L_018 }')); ?></label>
      <div class="controls">
        <div class=""> <?php echo (isset($this->_rootref['AUC_DESCRIPTION'])) ? $this->_rootref['AUC_DESCRIPTION'] : ''; ?>
          <input type="hidden" name="imgtype" value="1">
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <hr  />
    <input type="hidden" name="imgtype" value="1">
	<?php if ($this->_rootref['B_GALLERY']) {  ?>	
	<div align="center">													
	<?php echo ((isset($this->_rootref['L_663'])) ? $this->_rootref['L_663'] : ((isset($MSG['663'])) ? $MSG['663'] : '{ L_663 }')); ?><br>
	<p><?php echo (isset($this->_rootref['MAXPICS'])) ? $this->_rootref['MAXPICS'] : ''; ?></p>
	<a id="pics" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>upldgallery.php" alt="gallery" data-fancybox-type="iframe" class="imaging btn btn-primary" data-content="<?php echo ((isset($this->_rootref['L_1049'])) ? $this->_rootref['L_1049'] : ((isset($MSG['1049'])) ? $MSG['1049'] : '{ L_1049 }')); ?>" rel="popover" data-placement="bottom" data-original-title="<?php echo ((isset($this->_rootref['L_677'])) ? $this->_rootref['L_677'] : ((isset($MSG['677'])) ? $MSG['677'] : '{ L_677 }')); ?>" data-trigger="hover"><?php echo ((isset($this->_rootref['L_677'])) ? $this->_rootref['L_677'] : ((isset($MSG['677'])) ? $MSG['677'] : '{ L_677 }')); ?></a>
    <input type="hidden" name="numimages" value="<?php echo (isset($this->_rootref['NUMIMAGES'])) ? $this->_rootref['NUMIMAGES'] : ''; ?>" id="numimages">
	</div>	
	<?php } ?>
	<hr />
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_257'])) ? $this->_rootref['L_257'] : ((isset($MSG['257'])) ? $MSG['257'] : '{ L_257 }')); ?></label>
      <div class="controls"><?php echo (isset($this->_rootref['ATYPE'])) ? $this->_rootref['ATYPE'] : ''; ?></div>
    </div>
    <?php if ($this->_rootref['B_FREEITEM']) {  ?>
	<div class="control-group">
    	<label class="control-label"><?php echo ((isset($this->_rootref['L_3500_1015744'])) ? $this->_rootref['L_3500_1015744'] : ((isset($MSG['3500_1015744'])) ? $MSG['3500_1015744'] : '{ L_3500_1015744 }')); ?></label>
	    <div class="controls">
	    	<label class="radio ">
        		<input data-content="<?php echo ((isset($this->_rootref['L_3500_1015752'])) ? $this->_rootref['L_3500_1015752'] : ((isset($MSG['3500_1015752'])) ? $MSG['3500_1015752'] : '{ L_3500_1015752 }')); ?>" rel="popover" data-original-title="<?php echo ((isset($this->_rootref['L_3500_1015744'])) ? $this->_rootref['L_3500_1015744'] : ((isset($MSG['3500_1015744'])) ? $MSG['3500_1015744'] : '{ L_3500_1015744 }')); ?>" data-trigger="hover" type="radio" name="sellType" id="free_item_yes" value="free" <?php echo (isset($this->_rootref['FREEITEM_Y'])) ? $this->_rootref['FREEITEM_Y'] : ''; ?>>
        		<?php echo ((isset($this->_rootref['L_3500_1015745'])) ? $this->_rootref['L_3500_1015745'] : ((isset($MSG['3500_1015745'])) ? $MSG['3500_1015745'] : '{ L_3500_1015745 }')); ?>
        	</label>
        	<label class="radio ">
        		<input data-content="<?php echo ((isset($this->_rootref['L_3500_1015753'])) ? $this->_rootref['L_3500_1015753'] : ((isset($MSG['3500_1015753'])) ? $MSG['3500_1015753'] : '{ L_3500_1015753 }')); ?>" rel="popover" data-original-title="<?php echo ((isset($this->_rootref['L_3500_1015744'])) ? $this->_rootref['L_3500_1015744'] : ((isset($MSG['3500_1015744'])) ? $MSG['3500_1015744'] : '{ L_3500_1015744 }')); ?>" data-trigger="hover" type="radio" name="sellType" id="free_item_no" value="sell" <?php echo (isset($this->_rootref['FREEITEM_N'])) ? $this->_rootref['FREEITEM_N'] : ''; ?>>
        		<?php echo ((isset($this->_rootref['L_3500_1015746'])) ? $this->_rootref['L_3500_1015746'] : ((isset($MSG['3500_1015746'])) ? $MSG['3500_1015746'] : '{ L_3500_1015746 }')); ?>
        	</label>
		</div>
    </div>
    <?php } if ($this->_rootref['B_SELL_DI']) {  ?>
    <div class="control-group hide1" <?php echo (isset($this->_rootref['DIGITAL_ITEM'])) ? $this->_rootref['DIGITAL_ITEM'] : ''; ?>>
      <label class="control-label"><?php echo ((isset($this->_rootref['L_350_1010'])) ? $this->_rootref['L_350_1010'] : ((isset($MSG['350_1010'])) ? $MSG['350_1010'] : '{ L_350_1010 }')); ?></label>
      <div class="controls">
      	<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>digital_item.php" data-fancybox-type="iframe" class="infoboxs btn btn-primary"><?php echo ((isset($this->_rootref['L_677_a'])) ? $this->_rootref['L_677_a'] : ((isset($MSG['677_a'])) ? $MSG['677_a'] : '{ L_677_a }')); ?></a>
      </div>
    </div>
	<?php } ?>
    <div class="control-group hide9">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_258'])) ? $this->_rootref['L_258'] : ((isset($MSG['258'])) ? $MSG['258'] : '{ L_258 }')); ?></label>
      <div class="controls">
        <input type="text" name="iquantity" id="iqty" size="5" value="<?php echo (isset($this->_rootref['ITEMQTY'])) ? $this->_rootref['ITEMQTY'] : ''; ?>" <?php echo (isset($this->_rootref['ITEMQTYD'])) ? $this->_rootref['ITEMQTYD'] : ''; ?>>
      </div>
    </div>
    <hr />
    <div class="control-group hide2">
      <label class="control-label" id="minval_text"><?php echo (isset($this->_rootref['MINTEXT'])) ? $this->_rootref['MINTEXT'] : ''; ?></label>
      <div class="controls">
        <input type="text" size="10" name="minimum_bid" id="min_bid" value="<?php echo (isset($this->_rootref['MIN_BID'])) ? $this->_rootref['MIN_BID'] : ''; ?>" <?php echo (isset($this->_rootref['BID_PRICE'])) ? $this->_rootref['BID_PRICE'] : ''; ?>>
        <small><?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?>&nbsp;&nbsp;(<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>converter.php" alt="converter" class="new-window"><?php echo ((isset($this->_rootref['L_5010'])) ? $this->_rootref['L_5010'] : ((isset($MSG['5010'])) ? $MSG['5010'] : '{ L_5010 }')); ?></a>)</small> </div>
    </div>
    <div class="control-group hide3">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_023'])) ? $this->_rootref['L_023'] : ((isset($MSG['023'])) ? $MSG['023'] : '{ L_023 }')); ?></label>
      <div class="controls">
        <input type="text" size="10" name="shipping_cost" id="shipping_cost" value="<?php echo (isset($this->_rootref['SHIPPING_COST'])) ? $this->_rootref['SHIPPING_COST'] : ''; ?>"<?php if ($this->_rootref['SHIPPING1'] == ('')) {  ?>disabled="disabled"<?php } ?>>
        <small><?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?>&nbsp;&nbsp;(<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>converter.php" alt="converter" class="new-window"><?php echo ((isset($this->_rootref['L_5010'])) ? $this->_rootref['L_5010'] : ((isset($MSG['5010'])) ? $MSG['5010'] : '{ L_5010 }')); ?></a>)</small> </div>
    </div>
    <div class="control-group hide4">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_350_1008'])) ? $this->_rootref['L_350_1008'] : ((isset($MSG['350_1008'])) ? $MSG['350_1008'] : '{ L_350_1008 }')); ?></label>
      <div class="controls">
        <input id="popoverData" data-content="<?php echo ((isset($this->_rootref['L_3500_1015739'])) ? $this->_rootref['L_3500_1015739'] : ((isset($MSG['3500_1015739'])) ? $MSG['3500_1015739'] : '{ L_3500_1015739 }')); ?>" rel="popover" data-original-title="<?php echo ((isset($this->_rootref['L_350_1008'])) ? $this->_rootref['L_350_1008'] : ((isset($MSG['350_1008'])) ? $MSG['350_1008'] : '{ L_350_1008 }')); ?>" data-trigger="hover" type="text" size="10" name="additional_shipping_cost" id="additional_shipping_cost" value="<?php echo (isset($this->_rootref['ADDITIONAL_SHIPPING_COST'])) ? $this->_rootref['ADDITIONAL_SHIPPING_COST'] : ''; ?>" <?php if ($this->_rootref['SHIPPING1'] == ('')) {  ?>disabled="disabled"<?php } ?>>
        <small><?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?>&nbsp;&nbsp;(<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>converter.php" alt="converter" class="new-window"><?php echo ((isset($this->_rootref['L_5010'])) ? $this->_rootref['L_5010'] : ((isset($MSG['5010'])) ? $MSG['5010'] : '{ L_5010 }')); ?></a>)</small> </div>
    </div>
    <div class="control-group hide5">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_021'])) ? $this->_rootref['L_021'] : ((isset($MSG['021'])) ? $MSG['021'] : '{ L_021 }')); ?></label>
      <div class="controls">
        <label class="radio inline">
        <input type="radio" name="with_reserve" id="with_reserve_no" value="no" <?php echo (isset($this->_rootref['RESERVE_N'])) ? $this->_rootref['RESERVE_N'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_029'])) ? $this->_rootref['L_029'] : ((isset($MSG['029'])) ? $MSG['029'] : '{ L_029 }')); ?></label>
        <label class="radio inline">
        <input type="radio" name="with_reserve" id="with_reserve_yes" value="yes" <?php echo (isset($this->_rootref['RESERVE_Y'])) ? $this->_rootref['RESERVE_Y'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_030'])) ? $this->_rootref['L_030'] : ((isset($MSG['030'])) ? $MSG['030'] : '{ L_030 }')); ?></label>
        <input type="text" name="reserve_price" id="reserve_price" size="10" value="<?php echo (isset($this->_rootref['RESERVE'])) ? $this->_rootref['RESERVE'] : ''; ?>" <?php echo (isset($this->_rootref['BN_ONLY'])) ? $this->_rootref['BN_ONLY'] : ''; ?>>
        <small><?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?>&nbsp;&nbsp;(<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>converter.php" alt="converter" class="new-window"><?php echo ((isset($this->_rootref['L_5010'])) ? $this->_rootref['L_5010'] : ((isset($MSG['5010'])) ? $MSG['5010'] : '{ L_5010 }')); ?></a>)</small> </div>
    </div>
    <?php if ($this->_rootref['B_BN_ONLY']) {  ?>
    <div class="control-group hide6">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_30_0063'])) ? $this->_rootref['L_30_0063'] : ((isset($MSG['30_0063'])) ? $MSG['30_0063'] : '{ L_30_0063 }')); ?></label>
      <div class="controls">
        <label class="radio inline">
        <input type="radio" name="buy_now_only" value="n" <?php echo (isset($this->_rootref['BN_ONLY_N'])) ? $this->_rootref['BN_ONLY_N'] : ''; ?> id="bn_only_no">
        <?php echo ((isset($this->_rootref['L_029'])) ? $this->_rootref['L_029'] : ((isset($MSG['029'])) ? $MSG['029'] : '{ L_029 }')); ?></label>
        <label class="radio inline">
        <input type="radio" name="buy_now_only" value="y" <?php echo (isset($this->_rootref['BN_ONLY_Y'])) ? $this->_rootref['BN_ONLY_Y'] : ''; ?> id="bn_only_yes">
        <?php echo ((isset($this->_rootref['L_030'])) ? $this->_rootref['L_030'] : ((isset($MSG['030'])) ? $MSG['030'] : '{ L_030 }')); ?></label>
      </div>
    </div>
    <?php } if ($this->_rootref['B_BN']) {  ?>
    <div class="control-group hide7">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_496'])) ? $this->_rootref['L_496'] : ((isset($MSG['496'])) ? $MSG['496'] : '{ L_496 }')); ?></label>
      <div class="controls">
        <label class="radio inline">
        <input type="radio" name="buy_now" id="bn_no" value="no" <?php echo (isset($this->_rootref['BN_N'])) ? $this->_rootref['BN_N'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_029'])) ? $this->_rootref['L_029'] : ((isset($MSG['029'])) ? $MSG['029'] : '{ L_029 }')); ?></label>
        <label class="radio inline">
        <input type="radio" name="buy_now" id="bn_yes" value="yes" <?php echo (isset($this->_rootref['BN_Y'])) ? $this->_rootref['BN_Y'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_030'])) ? $this->_rootref['L_030'] : ((isset($MSG['030'])) ? $MSG['030'] : '{ L_030 }')); ?></label>
        <input type="text" name="buy_now_price" id="bn" size="10" value="<?php echo (isset($this->_rootref['BN_PRICE'])) ? $this->_rootref['BN_PRICE'] : ''; ?>" <?php echo (isset($this->_rootref['BN'])) ? $this->_rootref['BN'] : ''; ?>>
        <small><?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?>&nbsp;&nbsp;(<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>converter.php" alt="converter" class="new-window"><?php echo ((isset($this->_rootref['L_5010'])) ? $this->_rootref['L_5010'] : ((isset($MSG['5010'])) ? $MSG['5010'] : '{ L_5010 }')); ?></a>) </small> </div>
    </div>
    <?php } if ($this->_rootref['B_CUSINC']) {  ?>
    <div class="control-group hide8">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_120'])) ? $this->_rootref['L_120'] : ((isset($MSG['120'])) ? $MSG['120'] : '{ L_120 }')); ?></label>
      <div class="controls">
        <label class="radio ">
        <input type="radio" name="increments" id="inc1" value="1" <?php echo (isset($this->_rootref['INCREMENTS1'])) ? $this->_rootref['INCREMENTS1'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_614'])) ? $this->_rootref['L_614'] : ((isset($MSG['614'])) ? $MSG['614'] : '{ L_614 }')); ?></label>
        <label class="radio ">
        <input type="radio" name="increments" id="inc2" value="2" <?php echo (isset($this->_rootref['INCREMENTS2'])) ? $this->_rootref['INCREMENTS2'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_615'])) ? $this->_rootref['L_615'] : ((isset($MSG['615'])) ? $MSG['615'] : '{ L_615 }')); ?></label>
        <input type="text" name="customincrement" id="custominc" size="10" value="<?php echo (isset($this->_rootref['CUSTOM_INC'])) ? $this->_rootref['CUSTOM_INC'] : ''; ?>" <?php echo (isset($this->_rootref['INCREMENTS3'])) ? $this->_rootref['INCREMENTS3'] : ''; ?>>
        <small><?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?>&nbsp;&nbsp;(<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>converter.php" alt="converter" class="new-window"><?php echo ((isset($this->_rootref['L_5010'])) ? $this->_rootref['L_5010'] : ((isset($MSG['5010'])) ? $MSG['5010'] : '{ L_5010 }')); ?></a>) </small> </div>
    </div>
    <?php } else { ?>
    <input type="hidden" name="increments" id="inc1" value="1">
    <?php } ?>
    <hr />
    <?php if ($this->_rootref['B_EDIT_STARTTIME']) {  ?>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_2__0016'])) ? $this->_rootref['L_2__0016'] : ((isset($MSG['2__0016'])) ? $MSG['2__0016'] : '{ L_2__0016 }')); ?></label>
      <div class="controls">
        <?php if ($this->_rootref['B_EDITING']) {  ?>
        <input type="hidden" name="a_starts" value="<?php echo (isset($this->_rootref['START_TIME'])) ? $this->_rootref['START_TIME'] : ''; ?>">
        <?php } else { ?>
        <label class="radio "><?php echo ((isset($this->_rootref['L_211'])) ? $this->_rootref['L_211'] : ((isset($MSG['211'])) ? $MSG['211'] : '{ L_211 }')); ?>
          <input id="popoverData" data-content="<?php echo ((isset($this->_rootref['L_3500_1015740'])) ? $this->_rootref['L_3500_1015740'] : ((isset($MSG['3500_1015740'])) ? $MSG['3500_1015740'] : '{ L_3500_1015740 }')); ?>" rel="popover" data-original-title="<?php echo ((isset($this->_rootref['L_2__0016'])) ? $this->_rootref['L_2__0016'] : ((isset($MSG['2__0016'])) ? $MSG['2__0016'] : '{ L_2__0016 }')); ?>" data-trigger="hover" type="checkbox" name="start_now" <?php echo (isset($this->_rootref['START_NOW'])) ? $this->_rootref['START_NOW'] : ''; ?>>
        </label>
        <label class="radio "><?php echo ((isset($this->_rootref['L_260'])) ? $this->_rootref['L_260'] : ((isset($MSG['260'])) ? $MSG['260'] : '{ L_260 }')); ?>
        	<div class="input-append date form_datetime">
        		<input data-content="<?php echo ((isset($this->_rootref['L_3500_1015741'])) ? $this->_rootref['L_3500_1015741'] : ((isset($MSG['3500_1015741'])) ? $MSG['3500_1015741'] : '{ L_3500_1015741 }')); ?>" data-original-title="<?php echo ((isset($this->_rootref['L_2__0016'])) ? $this->_rootref['L_2__0016'] : ((isset($MSG['2__0016'])) ? $MSG['2__0016'] : '{ L_2__0016 }')); ?>" data-trigger="hover" type="text" name="a_starts" id="pubdate_input" value="<?php echo (isset($this->_rootref['START_TIME'])) ? $this->_rootref['START_TIME'] : ''; ?>" size="20" maxlength="19">
        		<span class="add-on"><i class="icon-calendar"></i></span>
        	</div>
        	<script type="text/javascript" src="<?php echo (isset($this->_rootref['INCURL'])) ? $this->_rootref['INCURL'] : ''; ?>themes/<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>/js/timepicker.js"></script>
	        <script type="text/javascript">
				$(function()
				{
					$("#pubdate_input").datetimepicker(
					{
						format: '<?php echo (isset($this->_rootref['DATES_FORMAT'])) ? $this->_rootref['DATES_FORMAT'] : ''; ?>',
						autoclose: true,
						showMeridian: true,
						minuteStep: 1,
						todayHighlight: true,
						forceParse: true,
        				pickerPosition: "bottom-left",
        				startDate: '<?php echo (isset($this->_rootref['START_TIME'])) ? $this->_rootref['START_TIME'] : ''; ?>'
					});
				});
			</script>
        </label>
        <?php } ?>
      </div>
    </div>
    <?php } else { ?>
    <input type="hidden" name="start_now" value="1">
    <?php } ?>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_022'])) ? $this->_rootref['L_022'] : ((isset($MSG['022'])) ? $MSG['022'] : '{ L_022 }')); ?></label>
      <div class="controls"> <?php echo (isset($this->_rootref['DURATIONS'])) ? $this->_rootref['DURATIONS'] : ''; ?> </div>
    </div>
    <div class="control-group hide10">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_025'])) ? $this->_rootref['L_025'] : ((isset($MSG['025'])) ? $MSG['025'] : '{ L_025 }')); ?></label>
      <div class="controls">
        <label class="radio ">
        <input type="radio" name="shipping" id="bps" value="1" <?php echo (isset($this->_rootref['SHIPPING1'])) ? $this->_rootref['SHIPPING1'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_031'])) ? $this->_rootref['L_031'] : ((isset($MSG['031'])) ? $MSG['031'] : '{ L_031 }')); ?></label>
        <label class="radio ">
        <input type="radio" name="shipping" id="sps" value="2" <?php echo (isset($this->_rootref['SHIPPING2'])) ? $this->_rootref['SHIPPING2'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_032'])) ? $this->_rootref['L_032'] : ((isset($MSG['032'])) ? $MSG['032'] : '{ L_032 }')); ?></label>
        <label class="checkbox ">
        <input type="checkbox" name="returns" id="returns" value="1" <?php echo (isset($this->_rootref['RETURNS'])) ? $this->_rootref['RETURNS'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_025_E'])) ? $this->_rootref['L_025_E'] : ((isset($MSG['025_E'])) ? $MSG['025_E'] : '{ L_025_E }')); ?></label>
        <label class="checkbox ">
        <input type="checkbox" name="international" value="1" <?php echo (isset($this->_rootref['INTERNATIONAL'])) ? $this->_rootref['INTERNATIONAL'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_033'])) ? $this->_rootref['L_033'] : ((isset($MSG['033'])) ? $MSG['033'] : '{ L_033 }')); ?></label>
      </div>
    </div>
    <div class="control-group hide12">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_25_0215'])) ? $this->_rootref['L_25_0215'] : ((isset($MSG['25_0215'])) ? $MSG['25_0215'] : '{ L_25_0215 }')); ?></label>
      <div class="controls">
        <textarea name="shipping_terms" rows="3" cols="34"><?php echo (isset($this->_rootref['SHIPPING_TERMS'])) ? $this->_rootref['SHIPPING_TERMS'] : ''; ?></textarea>
      </div>
    </div>
    <hr />
    <div class="control-group hide11">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_026'])) ? $this->_rootref['L_026'] : ((isset($MSG['026'])) ? $MSG['026'] : '{ L_026 }')); ?></label>
      <div class="controls"> <?php echo (isset($this->_rootref['PAYMENTS'])) ? $this->_rootref['PAYMENTS'] : ''; ?> </div>
    </div>
    <?php if ($this->_rootref['B_MKFEATURED'] || $this->_rootref['B_MKBOLD'] || $this->_rootref['B_MKHIGHLIGHT']) {  ?>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_268'])) ? $this->_rootref['L_268'] : ((isset($MSG['268'])) ? $MSG['268'] : '{ L_268 }')); ?></label>
      <div class="controls">
        <?php if ($this->_rootref['B_MKFEATURED']) {  ?>
        <label class="checkbox ">
        <input type="checkbox" name="is_featured" id="is_featured" <?php echo (isset($this->_rootref['IS_FEATURED'])) ? $this->_rootref['IS_FEATURED'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_273'])) ? $this->_rootref['L_273'] : ((isset($MSG['273'])) ? $MSG['273'] : '{ L_273 }')); ?></label>
        <?php } if ($this->_rootref['B_MKBOLD']) {  ?>
        <label class="checkbox ">
        <input type="checkbox" name="is_bold" id="is_bold" <?php echo (isset($this->_rootref['IS_BOLD'])) ? $this->_rootref['IS_BOLD'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_274'])) ? $this->_rootref['L_274'] : ((isset($MSG['274'])) ? $MSG['274'] : '{ L_274 }')); ?></label>
        <?php } if ($this->_rootref['B_MKHIGHLIGHT']) {  ?>
        <label class="checkbox ">
        <input type="checkbox" name="is_highlighted" id="is_highlighted" <?php echo (isset($this->_rootref['IS_HIGHLIGHTED'])) ? $this->_rootref['IS_HIGHLIGHTED'] : ''; ?>>
        <?php echo ((isset($this->_rootref['L_292'])) ? $this->_rootref['L_292'] : ((isset($MSG['292'])) ? $MSG['292'] : '{ L_292 }')); ?></label>
        <?php } ?>
      </div>
    </div>
    <?php } if ($this->_rootref['B_AUTORELIST']) {  ?>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L__0161'])) ? $this->_rootref['L__0161'] : ((isset($MSG['_0161'])) ? $MSG['_0161'] : '{ L__0161 }')); ?></label>
      <div class="controls"> <?php echo (isset($this->_rootref['RELIST'])) ? $this->_rootref['RELIST'] : ''; ?> </div>
    </div>
    <?php } if ($this->_rootref['B_CAN_TAX']) {  ?>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_1102'])) ? $this->_rootref['L_1102'] : ((isset($MSG['1102'])) ? $MSG['1102'] : '{ L_1102 }')); ?></label>
      <div class="controls">
        <input type="radio" name="is_taxed" value="y" <?php echo (isset($this->_rootref['TAX_Y'])) ? $this->_rootref['TAX_Y'] : ''; ?>>
        <span><strong><?php echo ((isset($this->_rootref['L_030'])) ? $this->_rootref['L_030'] : ((isset($MSG['030'])) ? $MSG['030'] : '{ L_030 }')); ?></strong></span><br>
        <input type="radio" name="is_taxed" value="n" <?php echo (isset($this->_rootref['TAX_N'])) ? $this->_rootref['TAX_N'] : ''; ?>>
        <span><strong><?php echo ((isset($this->_rootref['L_029'])) ? $this->_rootref['L_029'] : ((isset($MSG['029'])) ? $MSG['029'] : '{ L_029 }')); ?></strong></span> 
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_1103'])) ? $this->_rootref['L_1103'] : ((isset($MSG['1103'])) ? $MSG['1103'] : '{ L_1103 }')); ?></label>
      <div class="controls">
        <input type="radio" name="tax_included" value="y" <?php echo (isset($this->_rootref['TAXINC_Y'])) ? $this->_rootref['TAXINC_Y'] : ''; ?>>
        <span><strong><?php echo ((isset($this->_rootref['L_030'])) ? $this->_rootref['L_030'] : ((isset($MSG['030'])) ? $MSG['030'] : '{ L_030 }')); ?></strong></span><br>
        <input type="radio" name="tax_included" value="n" <?php echo (isset($this->_rootref['TAXINC_N'])) ? $this->_rootref['TAXINC_N'] : ''; ?>>
        <span><strong><?php echo ((isset($this->_rootref['L_029'])) ? $this->_rootref['L_029'] : ((isset($MSG['029'])) ? $MSG['029'] : '{ L_029 }')); ?></strong></span> 
      </div>
    </div>
    <?php } if ($this->_rootref['B_FEES']) {  ?>
    <div class="control-group">
      <label class="control-label"><?php echo ((isset($this->_rootref['L_263'])) ? $this->_rootref['L_263'] : ((isset($MSG['263'])) ? $MSG['263'] : '{ L_263 }')); ?></label>
      <div class="controls">
        <input type="hidden" name="fee_exact" id="fee_exact" value="<?php echo (isset($this->_rootref['FEE_VALUE'])) ? $this->_rootref['FEE_VALUE'] : ''; ?>">
        <span id="to_pay"><strong><?php echo (isset($this->_rootref['FEE_VALUE_F'])) ? $this->_rootref['FEE_VALUE_F'] : ''; ?></strong></span> <?php echo (isset($this->_rootref['CURRENCY'])) ? $this->_rootref['CURRENCY'] : ''; ?> </div>
    </div>
    <?php } ?>
    <div class="form-actions">
      <input type="hidden" value="3" name="action">
      <input type="submit" class="btn btn-primary" name="" value="<?php echo ((isset($this->_rootref['L_5189'])) ? $this->_rootref['L_5189'] : ((isset($MSG['5189'])) ? $MSG['5189'] : '{ L_5189 }')); ?>" title="<?php echo ((isset($this->_rootref['L_5189'])) ? $this->_rootref['L_5189'] : ((isset($MSG['5189'])) ? $MSG['5189'] : '{ L_5189 }')); ?>">&nbsp;&nbsp;&nbsp;
      <input type="reset" class="btn" id="resetbt" value="<?php echo ((isset($this->_rootref['L_5190'])) ? $this->_rootref['L_5190'] : ((isset($MSG['5190'])) ? $MSG['5190'] : '{ L_5190 }')); ?>" title="<?php echo ((isset($this->_rootref['L_5190'])) ? $this->_rootref['L_5190'] : ((isset($MSG['5190'])) ? $MSG['5190'] : '{ L_5190 }')); ?>">
    </div>
  </form>
<?php } else if ($this->_rootref['PAGE'] == (2)) {  ?>
  <form name="preview" action="<?php echo (isset($this->_rootref['ASSLURL'])) ? $this->_rootref['ASSLURL'] : ''; ?>sell.php" method="post">
    <input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
    <table class="table">
      <?php if ($this->_rootref['ERROR'] != ('')) {  ?>
      <tr>
        <td class="alert" colspan="2" align="center"><?php echo (isset($this->_rootref['ERROR'])) ? $this->_rootref['ERROR'] : ''; ?></td>
      </tr>
      <?php } ?>
      <div class="alert alert-info"><?php echo ((isset($this->_rootref['L_046'])) ? $this->_rootref['L_046'] : ((isset($MSG['046'])) ? $MSG['046'] : '{ L_046 }')); ?></div>
      <tr>
        <td width="40%" align="right"  valign="top"><b><?php echo ((isset($this->_rootref['L_017'])) ? $this->_rootref['L_017'] : ((isset($MSG['017'])) ? $MSG['017'] : '{ L_017 }')); ?></b></td>
        <td width="60%" ><?php echo (isset($this->_rootref['TITLE'])) ? $this->_rootref['TITLE'] : ''; ?></td>
      </tr>
      <?php if ($this->_rootref['B_SUBTITLE']) {  ?>
      <tr>
        <td width="40%" align="right"  valign="top"><b><?php echo ((isset($this->_rootref['L_806'])) ? $this->_rootref['L_806'] : ((isset($MSG['806'])) ? $MSG['806'] : '{ L_806 }')); ?></b></td>
        <td width="60%" ><?php echo (isset($this->_rootref['SUBTITLE'])) ? $this->_rootref['SUBTITLE'] : ''; ?></td>
      </tr>
      <?php } ?>
      <tr>
        <td  valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_018'])) ? $this->_rootref['L_018'] : ((isset($MSG['018'])) ? $MSG['018'] : '{ L_018 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['AUC_DESCRIPTION'])) ? $this->_rootref['AUC_DESCRIPTION'] : ''; ?></td>
      </tr>
      <?php if ($this->_rootref['B_CONDITION']) {  ?>
      <tr>
				<td  valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_103600'])) ? $this->_rootref['L_103600'] : ((isset($MSG['103600'])) ? $MSG['103600'] : '{ L_103600 }')); ?></b></td>
				<td><?php echo (isset($this->_rootref['ITEM_CONDITION'])) ? $this->_rootref['ITEM_CONDITION'] : ''; ?></td>
			</tr>
			<tr>
				<td  valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_103700'])) ? $this->_rootref['L_103700'] : ((isset($MSG['103700'])) ? $MSG['103700'] : '{ L_103700 }')); ?></b></td>
				<td><?php echo (isset($this->_rootref['ITEM_MANUFACTURER'])) ? $this->_rootref['ITEM_MANUFACTURER'] : ''; ?></td>
			</tr>
			<tr>
				<td  valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_103800'])) ? $this->_rootref['L_103800'] : ((isset($MSG['103800'])) ? $MSG['103800'] : '{ L_103800 }')); ?></b></td>
				<td><?php echo (isset($this->_rootref['ITEM_MODEL'])) ? $this->_rootref['ITEM_MODEL'] : ''; ?></td>
			</tr>
			<tr>
				<td  valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_103900'])) ? $this->_rootref['L_103900'] : ((isset($MSG['103900'])) ? $MSG['103900'] : '{ L_103900 }')); ?></b></td>
				<td><?php echo (isset($this->_rootref['ITEM_COLOUR'])) ? $this->_rootref['ITEM_COLOUR'] : ''; ?></td>
			</tr>
			<tr>
				<td  valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_104000'])) ? $this->_rootref['L_104000'] : ((isset($MSG['104000'])) ? $MSG['104000'] : '{ L_104000 }')); ?></b></td>
				<td><?php echo (isset($this->_rootref['ITEM_YEAR'])) ? $this->_rootref['ITEM_YEAR'] : ''; ?></td>
			</tr>
			<?php } ?>
      <tr>
        <td  valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_019'])) ? $this->_rootref['L_019'] : ((isset($MSG['019'])) ? $MSG['019'] : '{ L_019 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['PIC_URL'])) ? $this->_rootref['PIC_URL'] : ''; ?></td>
      </tr>
      <?php if ($this->_rootref['B_GALLERY']) {  ?>
      <tr>
        <td width="260" valign="middle" align="right">&nbsp;</td>
        <td> <?php echo ((isset($this->_rootref['L_663'])) ? $this->_rootref['L_663'] : ((isset($MSG['663'])) ? $MSG['663'] : '{ L_663 }')); ?><br>
          <?php $_gallery_count = (isset($this->_tpldata['gallery'])) ? sizeof($this->_tpldata['gallery']) : 0;if ($_gallery_count) {for ($_gallery_i = 0; $_gallery_i < $_gallery_count; ++$_gallery_i){$_gallery_val = &$this->_tpldata['gallery'][$_gallery_i]; ?>
          <a href="<?php echo $_gallery_val['IMAGE']; ?>" data-fancybox-type="image" class="fancybox-buttons" rel="gallery1"><img src="<?php echo $_gallery_val['IMAGE']; ?>" width=40 hspace=5 border=0></a>
          <?php }} ?>
        </td>
      </tr>
      <?php } if ($this->_rootref['ATYPE_PLAIN'] == (3)) {  ?>
	<tr>
	<td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_350_1010'])) ? $this->_rootref['L_350_1010'] : ((isset($MSG['350_1010'])) ? $MSG['350_1010'] : '{ L_350_1010 }')); ?></b></td>
	<td>
		<b><?php echo ((isset($this->_rootref['L_350_10172'])) ? $this->_rootref['L_350_10172'] : ((isset($MSG['350_10172'])) ? $MSG['350_10172'] : '{ L_350_10172 }')); ?></b> <?php echo (isset($this->_rootref['FILE'])) ? $this->_rootref['FILE'] : ''; ?><br>
      	<?php $_d_items_count = (isset($this->_tpldata['d_items'])) ? sizeof($this->_tpldata['d_items']) : 0;if ($_d_items_count) {for ($_d_items_i = 0; $_d_items_i < $_d_items_count; ++$_d_items_i){$_d_items_val = &$this->_tpldata['d_items'][$_d_items_i]; ?>
      	<b><?php echo ((isset($this->_rootref['L_350_10173'])) ? $this->_rootref['L_350_10173'] : ((isset($MSG['350_10173'])) ? $MSG['350_10173'] : '{ L_350_10173 }')); ?></b> <?php echo $_d_items_val['K']; ?>
        <?php }} ?>
    </td>
	</tr>
	<?php } if ($this->_rootref['B_BN_ONLY']) {  ?>
      <tr>
        <td valign="top" align="right"><b><?php echo (isset($this->_rootref['MINTEXT'])) ? $this->_rootref['MINTEXT'] : ''; ?></b></td>
        <td><?php echo (isset($this->_rootref['MIN_BID'])) ? $this->_rootref['MIN_BID'] : ''; ?></td>
      </tr>
      <?php } if ($this->_rootref['ATYPE_PLAIN'] == (1)) {  if ($this->_rootref['B_BN_ONLY']) {  ?>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_021'])) ? $this->_rootref['L_021'] : ((isset($MSG['021'])) ? $MSG['021'] : '{ L_021 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['RESERVE'])) ? $this->_rootref['RESERVE'] : ''; ?></td>
      </tr>
      <?php } if ($this->_rootref['B_BN']) {  ?>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_496'])) ? $this->_rootref['L_496'] : ((isset($MSG['496'])) ? $MSG['496'] : '{ L_496 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['BN_PRICE'])) ? $this->_rootref['BN_PRICE'] : ''; ?></td>
      </tr>
      <?php } } ?>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_023'])) ? $this->_rootref['L_023'] : ((isset($MSG['023'])) ? $MSG['023'] : '{ L_023 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['SHIPPING_COST'])) ? $this->_rootref['SHIPPING_COST'] : ''; ?></td>
      </tr>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_2__0016'])) ? $this->_rootref['L_2__0016'] : ((isset($MSG['2__0016'])) ? $MSG['2__0016'] : '{ L_2__0016 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['STARTDATE'])) ? $this->_rootref['STARTDATE'] : ''; ?></td>
      </tr>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_022'])) ? $this->_rootref['L_022'] : ((isset($MSG['022'])) ? $MSG['022'] : '{ L_022 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['DURATION'])) ? $this->_rootref['DURATION'] : ''; ?></td>
      </tr>
      <?php if ($this->_rootref['B_CUSINC']) {  ?>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_120'])) ? $this->_rootref['L_120'] : ((isset($MSG['120'])) ? $MSG['120'] : '{ L_120 }')); ?></b> </td>
        <td><?php echo (isset($this->_rootref['INCREMENTS'])) ? $this->_rootref['INCREMENTS'] : ''; ?></td>
      </tr>
      <?php } ?>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_261'])) ? $this->_rootref['L_261'] : ((isset($MSG['261'])) ? $MSG['261'] : '{ L_261 }')); ?></b> </td>
        <td><?php echo (isset($this->_rootref['ATYPE'])) ? $this->_rootref['ATYPE'] : ''; ?></td>
      </tr>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_025'])) ? $this->_rootref['L_025'] : ((isset($MSG['025'])) ? $MSG['025'] : '{ L_025 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['SHIPPING'])) ? $this->_rootref['SHIPPING'] : ''; ?><br>
          <?php echo (isset($this->_rootref['INTERNATIONAL'])) ? $this->_rootref['INTERNATIONAL'] : ''; ?></td>
      </tr>
      <tr> 
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_025_C'])) ? $this->_rootref['L_025_C'] : ((isset($MSG['025_C'])) ? $MSG['025_C'] : '{ L_025_C }')); ?></b></td> 
    	<td><?php echo (isset($this->_rootref['RETURNS'])) ? $this->_rootref['RETURNS'] : ''; ?></td> 
	  </tr>
      <tr>
        <td align="right" valign="top"><b><?php echo ((isset($this->_rootref['L_25_0215'])) ? $this->_rootref['L_25_0215'] : ((isset($MSG['25_0215'])) ? $MSG['25_0215'] : '{ L_25_0215 }')); ?></b></td>
        <td><?php echo (isset($this->_rootref['SHIPPING_TERMS'])) ? $this->_rootref['SHIPPING_TERMS'] : ''; ?></td>
      </tr>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_026'])) ? $this->_rootref['L_026'] : ((isset($MSG['026'])) ? $MSG['026'] : '{ L_026 }')); ?></b> </td>
        <td><?php echo (isset($this->_rootref['PAYMENTS_METHODS'])) ? $this->_rootref['PAYMENTS_METHODS'] : ''; ?></td>
      </tr>
      <tr>
        <td  valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_027'])) ? $this->_rootref['L_027'] : ((isset($MSG['027'])) ? $MSG['027'] : '{ L_027 }')); ?></b></td>
        <td> <?php echo (isset($this->_rootref['CAT_LIST1'])) ? $this->_rootref['CAT_LIST1'] : ''; ?>
          <?php if ($this->_rootref['CAT_LIST2'] != ('')) {  ?>
          <br>
          <?php echo (isset($this->_rootref['CAT_LIST2'])) ? $this->_rootref['CAT_LIST2'] : ''; ?>
          <?php } ?>
        </td>
      </tr>
      <?php if ($this->_rootref['B_FEES']) {  ?>
      <tr>
        <td valign="top" align="right"><b><?php echo ((isset($this->_rootref['L_263'])) ? $this->_rootref['L_263'] : ((isset($MSG['263'])) ? $MSG['263'] : '{ L_263 }')); ?></b> </td>
        <td><?php echo (isset($this->_rootref['FEE'])) ? $this->_rootref['FEE'] : ''; ?></td>
      </tr>
      <?php } ?>
      <tr> </tr>
      <?php if ($this->_rootref['B_USERAUTH']) {  ?>
      <tr>
        <td align="right"><?php echo ((isset($this->_rootref['L_003'])) ? $this->_rootref['L_003'] : ((isset($MSG['003'])) ? $MSG['003'] : '{ L_003 }')); ?></td>
        <td><b><?php echo (isset($this->_rootref['YOURUSERNAME'])) ? $this->_rootref['YOURUSERNAME'] : ''; ?></b>
          <input type="hidden" name="nick" value="<?php echo (isset($this->_rootref['YOURUSERNAME'])) ? $this->_rootref['YOURUSERNAME'] : ''; ?>">
      </tr>
      <tr>
        <td align="right"><?php echo ((isset($this->_rootref['L_004'])) ? $this->_rootref['L_004'] : ((isset($MSG['004'])) ? $MSG['004'] : '{ L_004 }')); ?></td>
        <td><input type="password" name="password" size="20" maxlength="20" value=""></td>
      </tr>
      <?php } ?>
    </table>
    <div class="alert alert-info"><?php echo ((isset($this->_rootref['L_046'])) ? $this->_rootref['L_046'] : ((isset($MSG['046'])) ? $MSG['046'] : '{ L_046 }')); ?></div>
    <div class="form-actions">
      <input type="hidden" value="4" name="action">
      <input type="submit" name="" value="<?php echo ((isset($this->_rootref['L_2__0037'])) ? $this->_rootref['L_2__0037'] : ((isset($MSG['2__0037'])) ? $MSG['2__0037'] : '{ L_2__0037 }')); ?>" class="btn btn-primary">
    </div>
  </form>
  <?php } else { ?>
  <div style="text-align:center">
    <p><?php if ($this->_rootref['B_EMAIL']) {  echo ((isset($this->_rootref['L_100'])) ? $this->_rootref['L_100'] : ((isset($MSG['100'])) ? $MSG['100'] : '{ L_100 }')); } else { echo ((isset($this->_rootref['L_100_b'])) ? $this->_rootref['L_100_b'] : ((isset($MSG['100_b'])) ? $MSG['100_b'] : '{ L_100_b }')); } ?>
    <?php echo ((isset($this->_rootref['L_101'])) ? $this->_rootref['L_101'] : ((isset($MSG['101'])) ? $MSG['101'] : '{ L_101 }')); ?>:&nbsp;&nbsp;<a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo (isset($this->_rootref['SEO_TITLE'])) ? $this->_rootref['SEO_TITLE'] : ''; ?>-<?php echo (isset($this->_rootref['AUCTION_ID'])) ? $this->_rootref['AUCTION_ID'] : ''; ?>"><?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo (isset($this->_rootref['SEO_TITLE'])) ? $this->_rootref['SEO_TITLE'] : ''; ?>-<?php echo (isset($this->_rootref['AUCTION_ID'])) ? $this->_rootref['AUCTION_ID'] : ''; ?></a><br>
    <?php echo (isset($this->_rootref['MESSAGE'])) ? $this->_rootref['MESSAGE'] : ''; ?><br><br>
    <a class="btn btn-success" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>edit_active_auction.php?id=<?php echo (isset($this->_rootref['AUCTION_ID'])) ? $this->_rootref['AUCTION_ID'] : ''; ?>"><?php echo ((isset($this->_rootref['L_30_0069'])) ? $this->_rootref['L_30_0069'] : ((isset($MSG['30_0069'])) ? $MSG['30_0069'] : '{ L_30_0069 }')); ?></a> <a class="btn btn-info" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>sellsimilar.php?id=<?php echo (isset($this->_rootref['AUCTION_ID'])) ? $this->_rootref['AUCTION_ID'] : ''; ?>"><?php echo ((isset($this->_rootref['L_2__0050'])) ? $this->_rootref['L_2__0050'] : ((isset($MSG['2__0050'])) ? $MSG['2__0050'] : '{ L_2__0050 }')); ?></a>
    <p> 
  </div>
  <?php } ?>