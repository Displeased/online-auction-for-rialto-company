<script type="text/javascript">
$(document).ready(function() {
	$("#closeall").click(function() {
		var checked_status = this.checked;
        $("input[id=close]").each(function()
        { 
        	this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
        });	
	});
	
	$("#deleteall").click(function() {
		var checked_status = this.checked;
        $("input[id=delete]").each(function()
        { 
        	this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
        });	
	});

	$("#processdel").submit(function() {
		if (confirm('<?php echo ((isset($this->_rootref['L_30_0087'])) ? $this->_rootref['L_30_0087'] : ((isset($MSG['30_0087'])) ? $MSG['30_0087'] : '{ L_30_0087 }')); ?>')){
			return true;
		} 
		else 
		{
			return false;
		}
	});
});
</script>
<div class="row">
<?php $this->_tpl_include('u_header.tpl'); ?>
<div class="span9">
  <legend><?php echo ((isset($this->_rootref['L_203'])) ? $this->_rootref['L_203'] : ((isset($MSG['203'])) ? $MSG['203'] : '{ L_203 }')); ?></legend>
  <form name="auctions" method="post" action="" id="processdel">
    <input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
    <small><span class="muted"><?php echo ((isset($this->_rootref['L_5117'])) ? $this->_rootref['L_5117'] : ((isset($MSG['5117'])) ? $MSG['5117'] : '{ L_5117 }')); ?>&nbsp;<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>&nbsp;<?php echo ((isset($this->_rootref['L_5118'])) ? $this->_rootref['L_5118'] : ((isset($MSG['5118'])) ? $MSG['5118'] : '{ L_5118 }')); ?>&nbsp;<?php echo (isset($this->_rootref['PAGES'])) ? $this->_rootref['PAGES'] : ''; ?></span></small>
    <table class="table table-bordered table-condensed table-striped">
      <tr>
        <td  width="32%"><a href="yourauctions.php?oa_ord=title&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_624'])) ? $this->_rootref['L_624'] : ((isset($MSG['624'])) ? $MSG['624'] : '{ L_624 }')); ?></small></a>
          <?php if ($this->_rootref['ORDERCOL'] == ('title')) {  ?>
          <a href="yourauctions.php?oa_ord=title&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></small></a>
          <?php } ?>
        </td>
        <td class="hidden-phone"  width="11%"><a href="yourauctions.php?oa_ord=starts&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_625'])) ? $this->_rootref['L_625'] : ((isset($MSG['625'])) ? $MSG['625'] : '{ L_625 }')); ?></small></a>
          <?php if ($this->_rootref['ORDERCOL'] == ('starts')) {  ?>
          <a href="yourauctions.php?oa_ord=starts&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></small></a>
          <?php } ?>
        </td>
        <td  width="11%"><a href="yourauctions.php?oa_ord=ends&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_626'])) ? $this->_rootref['L_626'] : ((isset($MSG['626'])) ? $MSG['626'] : '{ L_626 }')); ?></small></a>
          <?php if ($this->_rootref['ORDERCOL'] == ('ends')) {  ?>
          <a href="yourauctions.php?oa_ord=ends&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></small></a>
          <?php } ?>
        </td>
        <td class="hidden-phone"  align="center" nowrap="nowrap"><small><?php echo ((isset($this->_rootref['L__0153'])) ? $this->_rootref['L__0153'] : ((isset($MSG['_0153'])) ? $MSG['_0153'] : '{ L__0153 }')); ?></small> </td>
        <td class="hidden-phone" width="7%" align="center"><a href="yourauctions.php?oa_ord=num_bids&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_627'])) ? $this->_rootref['L_627'] : ((isset($MSG['627'])) ? $MSG['627'] : '{ L_627 }')); ?></small></a>
          <?php if ($this->_rootref['ORDERCOL'] == ('num_bids')) {  ?>
          <a href="yourauctions.php?oa_ord=num_bids&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></small></a>
          <?php } ?>
        </td>
        <td width="10%" align="center"><a href="yourauctions.php?oa_ord=current_bid&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_628'])) ? $this->_rootref['L_628'] : ((isset($MSG['628'])) ? $MSG['628'] : '{ L_628 }')); ?></small></a>
          <?php if ($this->_rootref['ORDERCOL'] == ('current_bid')) {  ?>
          <a href="yourauctions.php?oa_ord=current_bid&oa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><small><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></small></a>
          <?php } ?>
        </td>
        <td  width="6%" align="center"><small><?php echo ((isset($this->_rootref['L_298'])) ? $this->_rootref['L_298'] : ((isset($MSG['298'])) ? $MSG['298'] : '{ L_298 }')); ?></small> </td>
        <td  width="8%" align="center"><small><?php echo ((isset($this->_rootref['L_008'])) ? $this->_rootref['L_008'] : ((isset($MSG['008'])) ? $MSG['008'] : '{ L_008 }')); ?></small> </td>
        <td  width="6%" align="center" bgcolor="#FFFF00"><small><?php echo ((isset($this->_rootref['L_2__0048'])) ? $this->_rootref['L_2__0048'] : ((isset($MSG['2__0048'])) ? $MSG['2__0048'] : '{ L_2__0048 }')); ?></small> </td>
      </tr>
      <?php if ($this->_rootref['B_AREITEMS']) {  $_items_count = (isset($this->_tpldata['items'])) ? sizeof($this->_tpldata['items']) : 0;if ($_items_count) {for ($_items_i = 0; $_items_i < $_items_count; ++$_items_i){$_items_val = &$this->_tpldata['items'][$_items_i]; ?>
      <tr>
        <td width="32%"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_items_val['SEO_TITLE']; ?>-<?php echo $_items_val['ID']; ?>"><?php echo $_items_val['TITLE']; ?></a> <br>
          <small><?php echo ((isset($this->_rootref['L_30_0081'])) ? $this->_rootref['L_30_0081'] : ((isset($MSG['30_0081'])) ? $MSG['30_0081'] : '{ L_30_0081 }')); echo $_items_val['COUNTER']; echo ((isset($this->_rootref['L__0151'])) ? $this->_rootref['L__0151'] : ((isset($MSG['_0151'])) ? $MSG['_0151'] : '{ L__0151 }')); ?></small></td>
        <td class="hidden-phone" width="11%"><small><?php echo $_items_val['STARTS']; ?></small> </td>
        <td width="11%"><small><?php echo $_items_val['ENDS']; ?></small> </td>
        <td class="hidden-phone" width="9%"  align="center"><small>
          <?php if ($_items_val['RELISTED'] == 0) {  ?>
          --
          <?php } else { ?>
          <?php echo $_items_val['RELISTED']; ?>
          <?php } ?>
        </small></td>
        <td class="hidden-phone" width="7%"  align="center"><small><?php echo $_items_val['BIDS']; ?></small> </td>
        <td width="10%"  align="center"><?php if ($_items_val['B_HASNOBIDS']) {  ?>
          -
          <?php } else { ?>
          <?php echo $_items_val['BID']; ?>
          <?php } ?>
          </td>
        <td width="6%"  align="center"><?php if ($_items_val['B_HASNOBIDS']) {  ?>
          <a href="edit_active_auction.php?id=<?php echo $_items_val['ID']; ?>"><img src="images/edititem.gif" width="13" height="17" alt="<?php echo ((isset($this->_rootref['L_512'])) ? $this->_rootref['L_512'] : ((isset($MSG['512'])) ? $MSG['512'] : '{ L_512 }')); ?>" border="0"></a>
          <?php } ?>
        </td>
        <td width="8%"  align="center"><?php if ($_items_val['B_HASNOBIDS']) {  ?>
          <input type="checkbox" name="O_delete[]" value="<?php echo $_items_val['ID']; ?>" id="delete">
          <?php } ?>
        </td>
        <td width="6%"  align="center" ><input type="checkbox" name="closenow[]" value="<?php echo $_items_val['ID']; ?>" id="close">
        </td>
      </tr>
      <?php }} } ?>
      <tr class="hidden-phone">
        <td colspan="7" align="right" style="text-align:right; margin-right:10px"><small><span class="muted"><?php echo ((isset($this->_rootref['L_30_0102'])) ? $this->_rootref['L_30_0102'] : ((isset($MSG['30_0102'])) ? $MSG['30_0102'] : '{ L_30_0102 }')); ?></span></small></td>
        <td align="center"><input type="checkbox" id="deleteall"></td>
        <td align="center"><input type="checkbox" id="closeall"></td>
      </tr>
      <tr>
        <td colspan="9" style="text-align:center"><input type="hidden" name="action" value="delopenauctions">
          <input type="submit" name="Submit" value="<?php echo ((isset($this->_rootref['L_631'])) ? $this->_rootref['L_631'] : ((isset($MSG['631'])) ? $MSG['631'] : '{ L_631 }')); ?>" class="btn btn-primary">
        </td>
      </tr>
    </table>
  </form>
  <div class="pagination pagination-centered">
    <ul>
      <li><?php echo (isset($this->_rootref['PREV'])) ? $this->_rootref['PREV'] : ''; ?></li>
      <?php $_pages_count = (isset($this->_tpldata['pages'])) ? sizeof($this->_tpldata['pages']) : 0;if ($_pages_count) {for ($_pages_i = 0; $_pages_i < $_pages_count; ++$_pages_i){$_pages_val = &$this->_tpldata['pages'][$_pages_i]; ?>
      <li><?php echo $_pages_val['PAGE']; ?></li>
      <?php }} ?>
      <li><?php echo (isset($this->_rootref['NEXT'])) ? $this->_rootref['NEXT'] : ''; ?></li>
    </ul>
  </div>
<?php $this->_tpl_include('user_menu_footer.tpl'); ?>