<!DOCTYPE html>
<html dir="<?php echo (isset($this->_rootref['DOCDIR'])) ? $this->_rootref['DOCDIR'] : ''; ?>" lang="<?php echo (isset($this->_rootref['LANGUAGE'])) ? $this->_rootref['LANGUAGE'] : ''; ?>">
<head>
<?php $this->_tpl_include('js.tpl'); ?>
</head>
<body>

<div id="toppanel">
	<div id="panel">
		<div class="content clearfix">
			<div class="left">
				<?php if ($this->_rootref['B_LOGGED_IN']) {  ?>
					<h1><?php echo ((isset($this->_rootref['L_25_0083'])) ? $this->_rootref['L_25_0083'] : ((isset($MSG['25_0083'])) ? $MSG['25_0083'] : '{ L_25_0083 }')); ?></h1>
					<ul class="nav nav-stacked">
						<?php if ($this->_rootref['B_DIGITAL_ITEM_ON']) {  ?>
						<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>my_downloads.php"><?php echo ((isset($this->_rootref['L_3500_1015430'])) ? $this->_rootref['L_3500_1015430'] : ((isset($MSG['3500_1015430'])) ? $MSG['3500_1015430'] : '{ L_3500_1015430 }')); ?> <?php echo (isset($this->_rootref['MY_DOWNLOADS'])) ? $this->_rootref['MY_DOWNLOADS'] : ''; ?></a></li>
						<?php } ?>
	        			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>auction_watch.php"><?php echo ((isset($this->_rootref['L_471'])) ? $this->_rootref['L_471'] : ((isset($MSG['471'])) ? $MSG['471'] : '{ L_471 }')); ?> <?php echo (isset($this->_rootref['AUC_KEYWORDS'])) ? $this->_rootref['AUC_KEYWORDS'] : ''; ?></a></li>
	        			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>item_watch.php"><?php echo ((isset($this->_rootref['L_472'])) ? $this->_rootref['L_472'] : ((isset($MSG['472'])) ? $MSG['472'] : '{ L_472 }')); ?> <?php echo (isset($this->_rootref['BENDING_SOON'])) ? $this->_rootref['BENDING_SOON'] : ''; ?></a></li>
	        			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>yourbids.php"><?php echo ((isset($this->_rootref['L_620'])) ? $this->_rootref['L_620'] : ((isset($MSG['620'])) ? $MSG['620'] : '{ L_620 }')); ?> <?php echo (isset($this->_rootref['BOUTBID'])) ? $this->_rootref['BOUTBID'] : ''; ?></a></li>
	        			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>buying.php"><?php echo ((isset($this->_rootref['L_454'])) ? $this->_rootref['L_454'] : ((isset($MSG['454'])) ? $MSG['454'] : '{ L_454 }')); ?> <?php echo (isset($this->_rootref['ITEMS_WON'])) ? $this->_rootref['ITEMS_WON'] : ''; ?></a></li>
	        			<li><a href='<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>fsm.php'><?php echo ((isset($this->_rootref['L_FSM7'])) ? $this->_rootref['L_FSM7'] : ((isset($MSG['FSM7'])) ? $MSG['FSM7'] : '{ L_FSM7 }')); ?> <?php echo (isset($this->_rootref['FAVSELLER'])) ? $this->_rootref['FAVSELLER'] : ''; ?></a></li>
        			</ul>
        		<?php } else { ?>
        			<?php echo ((isset($this->_rootref['L_863'])) ? $this->_rootref['L_863'] : ((isset($MSG['863'])) ? $MSG['863'] : '{ L_863 }')); ?>
				<?php } ?>
			</div>
			<div class="left right">
				<?php if ($this->_rootref['B_LOGGED_IN']) {  if ($this->_rootref['B_CAN_SELL']) {  ?>
      					<h1><?php echo ((isset($this->_rootref['L_25_0082'])) ? $this->_rootref['L_25_0082'] : ((isset($MSG['25_0082'])) ? $MSG['25_0082'] : '{ L_25_0082 }')); ?></h1>
      					<ul class="nav nav-stacked">
	      					<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>selleremails.php"><?php echo ((isset($this->_rootref['L_25_0188'])) ? $this->_rootref['L_25_0188'] : ((isset($MSG['25_0188'])) ? $MSG['25_0188'] : '{ L_25_0188 }')); ?></a></li>
	      					<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>yourauctions_p.php"><?php echo ((isset($this->_rootref['L_25_0115'])) ? $this->_rootref['L_25_0115'] : ((isset($MSG['25_0115'])) ? $MSG['25_0115'] : '{ L_25_0115 }')); ?> <?php echo (isset($this->_rootref['PENDING_AUCTIONS'])) ? $this->_rootref['PENDING_AUCTIONS'] : ''; ?></a></li>
	      					<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>yourauctions.php"><?php echo ((isset($this->_rootref['L_203'])) ? $this->_rootref['L_203'] : ((isset($MSG['203'])) ? $MSG['203'] : '{ L_203 }')); ?> <?php echo (isset($this->_rootref['ACTIVE_AUCTIONS'])) ? $this->_rootref['ACTIVE_AUCTIONS'] : ''; ?></a></li>
	        				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>yourauctions_c.php"><?php echo ((isset($this->_rootref['L_204'])) ? $this->_rootref['L_204'] : ((isset($MSG['204'])) ? $MSG['204'] : '{ L_204 }')); ?> <?php echo (isset($this->_rootref['CLOSED_AUCTIONS'])) ? $this->_rootref['CLOSED_AUCTIONS'] : ''; ?></a></li>
	        				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>yourauctions_s.php"><?php echo ((isset($this->_rootref['L_2__0056'])) ? $this->_rootref['L_2__0056'] : ((isset($MSG['2__0056'])) ? $MSG['2__0056'] : '{ L_2__0056 }')); ?> <?php echo (isset($this->_rootref['SUSPENDED_AUCTIONS'])) ? $this->_rootref['SUSPENDED_AUCTIONS'] : ''; ?></a></li>
	        				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>yourauctions_sold.php"><?php echo ((isset($this->_rootref['L_25_0119'])) ? $this->_rootref['L_25_0119'] : ((isset($MSG['25_0119'])) ? $MSG['25_0119'] : '{ L_25_0119 }')); ?> <?php echo (isset($this->_rootref['SOLD_ITEM4'])) ? $this->_rootref['SOLD_ITEM4'] : ''; ?></a></li>
        				</ul>
        			<?php } } else { } ?>
			</div>
			<div class="left ">	
			<?php if ($this->_rootref['B_LOGGED_IN']) {  ?>
      				<h1><?php echo ((isset($this->_rootref['L_205'])) ? $this->_rootref['L_205'] : ((isset($MSG['205'])) ? $MSG['205'] : '{ L_205 }')); ?></h1>
      				<ul class="nav nav-stacked">
	      				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>support"><?php echo ((isset($this->_rootref['L_3500_1015432'])) ? $this->_rootref['L_3500_1015432'] : ((isset($MSG['3500_1015432'])) ? $MSG['3500_1015432'] : '{ L_3500_1015432 }')); ?> <?php echo (isset($this->_rootref['SUPPORT'])) ? $this->_rootref['SUPPORT'] : ''; ?></a></li>
	      				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>user_menu.php"><?php echo ((isset($this->_rootref['L_622'])) ? $this->_rootref['L_622'] : ((isset($MSG['622'])) ? $MSG['622'] : '{ L_622 }')); ?></a></li>
	      				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>edit_data.php?"><?php echo ((isset($this->_rootref['L_621'])) ? $this->_rootref['L_621'] : ((isset($MSG['621'])) ? $MSG['621'] : '{ L_621 }')); ?></a></li>
	      				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>yourfeedback.php"><?php echo ((isset($this->_rootref['L_208'])) ? $this->_rootref['L_208'] : ((isset($MSG['208'])) ? $MSG['208'] : '{ L_208 }')); ?> <?php echo (isset($this->_rootref['FEEDBACK'])) ? $this->_rootref['FEEDBACK'] : ''; ?></a></li>
	      				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>buysellnofeedback.php"><?php echo ((isset($this->_rootref['L_207'])) ? $this->_rootref['L_207'] : ((isset($MSG['207'])) ? $MSG['207'] : '{ L_207 }')); ?> <?php echo (isset($this->_rootref['FBTOLEAVE2'])) ? $this->_rootref['FBTOLEAVE2'] : ''; ?></a></li>
	      				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>mail.php"><?php echo ((isset($this->_rootref['L_623'])) ? $this->_rootref['L_623'] : ((isset($MSG['623'])) ? $MSG['623'] : '{ L_623 }')); ?> <?php echo (isset($this->_rootref['NEW_MESSAGES2'])) ? $this->_rootref['NEW_MESSAGES2'] : ''; ?></a></li>
	      				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>outstanding.php"><?php echo ((isset($this->_rootref['L_422'])) ? $this->_rootref['L_422'] : ((isset($MSG['422'])) ? $MSG['422'] : '{ L_422 }')); ?> <?php echo (isset($this->_rootref['OUTSTANDING'])) ? $this->_rootref['OUTSTANDING'] : ''; ?></a></li>
	      				<?php if ($this->_rootref['B_BANNERMANAGER']) {  ?>
	      				<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>managebanners.php"><?php echo ((isset($this->_rootref['L_350_1012111'])) ? $this->_rootref['L_350_1012111'] : ((isset($MSG['350_1012111'])) ? $MSG['350_1012111'] : '{ L_350_1012111 }')); ?> <?php echo (isset($this->_rootref['BANNER_ACC'])) ? $this->_rootref['BANNER_ACC'] : ''; ?></a></li>
	      				<?php } ?>
      				</ul>
			<?php } else { ?>
				<form class="login-home" name="login" action="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>user_login.php" method="post">
					<h1><?php echo ((isset($this->_rootref['L_181'])) ? $this->_rootref['L_181'] : ((isset($MSG['181'])) ? $MSG['181'] : '{ L_181 }')); ?></h1>
					<label class="grey" for="log"><?php echo ((isset($this->_rootref['L_221'])) ? $this->_rootref['L_221'] : ((isset($MSG['221'])) ? $MSG['221'] : '{ L_221 }')); ?>:</label>
					<input type="text" name="username" maxlength="70" id="username" placeholder="<?php echo ((isset($this->_rootref['L_003'])) ? $this->_rootref['L_003'] : ((isset($MSG['003'])) ? $MSG['003'] : '{ L_003 }')); ?>">
					<label class="grey" for="pwd"><?php echo ((isset($this->_rootref['L_004'])) ? $this->_rootref['L_004'] : ((isset($MSG['004'])) ? $MSG['004'] : '{ L_004 }')); ?>:</label>
					<input type="password" name="password" id="password" placeholder="<?php echo ((isset($this->_rootref['L_004'])) ? $this->_rootref['L_004'] : ((isset($MSG['004'])) ? $MSG['004'] : '{ L_004 }')); ?>">
        	</div>
        	<div class="left ">
        			<label><input type="checkbox" name="rememberme" id="rememberme" value="1"> &nbsp;<?php echo ((isset($this->_rootref['L_25_0085'])) ? $this->_rootref['L_25_0085'] : ((isset($MSG['25_0085'])) ? $MSG['25_0085'] : '{ L_25_0085 }')); ?></label>
        			<label><input type="checkbox" id="hide_online" name="hide_online" value="y"> &nbsp;<?php echo ((isset($this->_rootref['L_350_10114'])) ? $this->_rootref['L_350_10114'] : ((isset($MSG['350_10114'])) ? $MSG['350_10114'] : '{ L_350_10114 }')); ?></label>
					<label><a class="btn btn-danger" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>forgotpasswd.php"><?php echo ((isset($this->_rootref['L_215'])) ? $this->_rootref['L_215'] : ((isset($MSG['215'])) ? $MSG['215'] : '{ L_215 }')); ?></a></label>
        			<div class="clear"></div>
        			<?php if ($this->_rootref['B_FBOOK_LOGIN']) {  ?>
            			<div id="fb-root"></div>
            			<label><a class="btn" onclick="<?php echo (isset($this->_rootref['B_FB_LINK'])) ? $this->_rootref['B_FB_LINK'] : ''; ?>();" id="popoverData" href="#" data-content="<?php echo ((isset($this->_rootref['L_350_10193'])) ? $this->_rootref['L_350_10193'] : ((isset($MSG['350_10193'])) ? $MSG['350_10193'] : '{ L_350_10193 }')); ?>" rel="popover" data-placement="bottom" data-original-title="<?php echo ((isset($this->_rootref['L_350_10204_d'])) ? $this->_rootref['L_350_10204_d'] : ((isset($MSG['350_10204_d'])) ? $MSG['350_10204_d'] : '{ L_350_10204_d }')); ?>" data-trigger="hover"><?php echo ((isset($this->_rootref['L_350_10204_d'])) ? $this->_rootref['L_350_10204_d'] : ((isset($MSG['350_10204_d'])) ? $MSG['350_10204_d'] : '{ L_350_10204_d }')); ?></a></label>
            		<?php } ?>
					<label><input type="submit" name="action" value="<?php echo ((isset($this->_rootref['L_052'])) ? $this->_rootref['L_052'] : ((isset($MSG['052'])) ? $MSG['052'] : '{ L_052 }')); ?>" class="btn btn-primary"></label>
				</form>
			<?php } ?>
			</div>
		</div>
	</div> 	
	<div class="tab hidden-phone">
		<ul class="login">
			<li class="left">&nbsp;</li>
			<li><?php echo ((isset($this->_rootref['L_200'])) ? $this->_rootref['L_200'] : ((isset($MSG['200'])) ? $MSG['200'] : '{ L_200 }')); ?> <?php if ($this->_rootref['B_LOGGED_IN']) {  echo (isset($this->_rootref['YOURUSERNAME'])) ? $this->_rootref['YOURUSERNAME'] : ''; ?><br><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>logout.php"><?php echo ((isset($this->_rootref['L_245'])) ? $this->_rootref['L_245'] : ((isset($MSG['245'])) ? $MSG['245'] : '{ L_245 }')); ?></a><?php } else { echo ((isset($this->_rootref['L_3500_1015406'])) ? $this->_rootref['L_3500_1015406'] : ((isset($MSG['3500_1015406'])) ? $MSG['3500_1015406'] : '{ L_3500_1015406 }')); } ?></li>
			<li class="sep">|</li>
			<li id="toggle">
				<a id="open" class="open" href="#"><?php if ($this->_rootref['B_LOGGED_IN']) {  echo ((isset($this->_rootref['L_25_0081'])) ? $this->_rootref['L_25_0081'] : ((isset($MSG['25_0081'])) ? $MSG['25_0081'] : '{ L_25_0081 }')); } else { echo ((isset($this->_rootref['L_221'])) ? $this->_rootref['L_221'] : ((isset($MSG['221'])) ? $MSG['221'] : '{ L_221 }')); ?></a><?php } ?>
				<a id="close" style="display: none;" class="closed" href="#"><?php echo ((isset($this->_rootref['L_3500_1015407'])) ? $this->_rootref['L_3500_1015407'] : ((isset($MSG['3500_1015407'])) ? $MSG['3500_1015407'] : '{ L_3500_1015407 }')); ?></a>			
			</li>
			<li class="right">&nbsp;</li>
		</ul> 
	</div> 
</div> 
	
<div class="navbar navbar-fixed-top hidden-desktop" >
  <div class="navbar-inner">
    <ul class="nav pull-left" style="margin:0;">
      <li class="active" style="cursor: pointer"><a style="cursor: pointer" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>home?"><i class="icon-home"></i></a></li>
      <li class="active" style="cursor: pointer; margin-left:2px"><a style="cursor: pointer" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>cat/<?php echo ((isset($this->_rootref['L_277_1'])) ? $this->_rootref['L_277_1'] : ((isset($MSG['277_1'])) ? $MSG['277_1'] : '{ L_277_1 }')); ?>-0"><i class="icon-list"></i></a></li>
    </ul>
    <ul class="nav pull-right">
      <?php if ($this->_rootref['B_LOGGED_IN']) {  ?>
      <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>user_menu.php?cptab=summary"><i class="icon-user"></i> <?php echo ((isset($this->_rootref['L_25_0081'])) ? $this->_rootref['L_25_0081'] : ((isset($MSG['25_0081'])) ? $MSG['25_0081'] : '{ L_25_0081 }')); ?></a></li>
      <li><a href="<?php echo (isset($this->_rootref['SSLURL'])) ? $this->_rootref['SSLURL'] : ''; ?>logout.php?"><i class="icon-lock"></i> <?php echo ((isset($this->_rootref['L_245'])) ? $this->_rootref['L_245'] : ((isset($MSG['245'])) ? $MSG['245'] : '{ L_245 }')); ?></a></li>
      <?php } else { ?>
      <li><a href="<?php echo (isset($this->_rootref['SSLURL'])) ? $this->_rootref['SSLURL'] : ''; ?>new_account"><i class="icon-user"></i> <?php echo ((isset($this->_rootref['L_235'])) ? $this->_rootref['L_235'] : ((isset($MSG['235'])) ? $MSG['235'] : '{ L_235 }')); ?></a></li>
      <li class="divider-vertical"></li>
      <li><a href="<?php echo (isset($this->_rootref['SSLURL'])) ? $this->_rootref['SSLURL'] : ''; ?>user_login.php?"><i class="icon-share-alt"></i> <?php echo ((isset($this->_rootref['L_052'])) ? $this->_rootref['L_052'] : ((isset($MSG['052'])) ? $MSG['052'] : '{ L_052 }')); ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>
<div class="container">
<div class="row">
  <div class="span12" style="position:relative">
    <div class="row">
    <br>
      <div class="span4 logo-holder">
      <br><br>
        <h2><?php echo (isset($this->_rootref['LOGO'])) ? $this->_rootref['LOGO'] : ''; ?></h2>
    </div>
      <div class="span8">
        <br><div class="hidden-phone" style="float:right;margin-top:5px; text-align:right"><?php echo (isset($this->_rootref['BANNER'])) ? $this->_rootref['BANNER'] : ''; echo (isset($this->_rootref['HEADER_ADSENSE'])) ? $this->_rootref['HEADER_ADSENSE'] : ''; ?></div>
      </div>
      <?php if ($this->_rootref['B_MULT_LANGS']) {  ?>
      <div class="span12">
		<div class="well well-small flags-icons"><small><span><?php echo ((isset($this->_rootref['L_2__0001'])) ? $this->_rootref['L_2__0001'] : ((isset($MSG['2__0001'])) ? $MSG['2__0001'] : '{ L_2__0001 }')); ?>:</span></small> <?php echo (isset($this->_rootref['FLAGS'])) ? $this->_rootref['FLAGS'] : ''; ?> <div class="hidden-phone" style="float:right" class="muted"><small style="font-size:9px"><?php echo (isset($this->_rootref['HEADERCOUNTER'])) ? $this->_rootref['HEADERCOUNTER'] : ''; ?></small></div></div>
	</div>
	<?php } else { ?>
	<div class="span12">
		<span style="float:right" class="muted"><small style="font-size:9px"><?php echo (isset($this->_rootref['HEADERCOUNTER'])) ? $this->_rootref['HEADERCOUNTER'] : ''; ?></small></span>
	</div>
	<?php } ?>
    </div>
  </div>
</div>
<div class="navbar">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li class="hidden-phone hidden-tablet" style="cursor: pointer"><a style="cursor: pointer" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>home"><i class="icon-home"></i></a></li>
          <li class="divider-vertical"></li>
          <li class="hidden-phone hidden-tablet"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>cat/<?php echo (isset($this->_rootref['BROWSE_SEO'])) ? $this->_rootref['BROWSE_SEO'] : ''; ?>-0"> <?php echo ((isset($this->_rootref['L_104'])) ? $this->_rootref['L_104'] : ((isset($MSG['104'])) ? $MSG['104'] : '{ L_104 }')); ?></a></li>
          <li class="divider-vertical"></li>
          <?php if ($this->_rootref['B_CAN_SELL']) {  ?>
          <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>select_category.php?"><?php echo ((isset($this->_rootref['L_028'])) ? $this->_rootref['L_028'] : ((isset($MSG['028'])) ? $MSG['028'] : '{ L_028 }')); ?></a></li>
          <li class="divider-vertical"></li>
          <?php } if ($this->_rootref['B_BOARDS'] && $this->_rootref['B_HELPBOX'] == (false)) {  ?>
      	  		<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>boards.php"><?php echo ((isset($this->_rootref['L_5030'])) ? $this->_rootref['L_5030'] : ((isset($MSG['5030'])) ? $MSG['5030'] : '{ L_5030 }')); ?></a></li>
      	  		<li class="divider-vertical hidden-phone"></li>
      	  <?php } ?> 
          <li class="dropdown"> 
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo ((isset($this->_rootref['L_276'])) ? $this->_rootref['L_276'] : ((isset($MSG['276'])) ? $MSG['276'] : '{ L_276 }')); ?> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
            	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>cat/<?php echo ((isset($this->_rootref['L_277_1'])) ? $this->_rootref['L_277_1'] : ((isset($MSG['277_1'])) ? $MSG['277_1'] : '{ L_277_1 }')); ?>-0"><?php echo ((isset($this->_rootref['L_277'])) ? $this->_rootref['L_277'] : ((isset($MSG['277'])) ? $MSG['277'] : '{ L_277 }')); ?></a></li>
                <?php $_cat_list_drop_2_count = (isset($this->_tpldata['cat_list_drop_2'])) ? sizeof($this->_tpldata['cat_list_drop_2']) : 0;if ($_cat_list_drop_2_count) {for ($_cat_list_drop_2_i = 0; $_cat_list_drop_2_i < $_cat_list_drop_2_count; ++$_cat_list_drop_2_i){$_cat_list_drop_2_val = &$this->_tpldata['cat_list_drop_2'][$_cat_list_drop_2_i]; ?>
                <li> <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>cat/<?php echo $_cat_list_drop_2_val['SEO_NAME']; ?>-<?php echo $_cat_list_drop_2_val['ID']; ?>"><?php echo $_cat_list_drop_2_val['IMAGE']; echo $_cat_list_drop_2_val['NAME']; ?></a></li>
                <?php }} ?>
            </ul>
          </li>
          <?php if ($this->_rootref['B_FEES'] && $this->_rootref['B_HELPBOX'] == (false)) {  ?>
          <li class="divider-vertical hidden-phone"></li>
      	  <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>fees.php"><?php echo ((isset($this->_rootref['L_25_0012'])) ? $this->_rootref['L_25_0012'] : ((isset($MSG['25_0012'])) ? $MSG['25_0012'] : '{ L_25_0012 }')); ?></a></li>
      	  <?php } if ($this->_rootref['B_HELPBOX']) {  ?>
          <li class="divider-vertical hidden-phone"></li>
          <li class="dropdown"> 
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo ((isset($this->_rootref['L_148'])) ? $this->_rootref['L_148'] : ((isset($MSG['148'])) ? $MSG['148'] : '{ L_148 }')); ?> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
            	<?php if ($this->_rootref['B_BOARDS']) {  ?>
      			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>boards.php"><?php echo ((isset($this->_rootref['L_5030'])) ? $this->_rootref['L_5030'] : ((isset($MSG['5030'])) ? $MSG['5030'] : '{ L_5030 }')); ?></a></li>
      			<?php } if ($this->_rootref['B_FEES']) {  ?>
      			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>fees.php"><?php echo ((isset($this->_rootref['L_25_0012'])) ? $this->_rootref['L_25_0012'] : ((isset($MSG['25_0012'])) ? $MSG['25_0012'] : '{ L_25_0012 }')); ?></a></li>
      			<?php } ?>
      			<li class="dropdown-submenu">
      				<a href="#"><?php echo ((isset($this->_rootref['L_5236'])) ? $this->_rootref['L_5236'] : ((isset($MSG['5236'])) ? $MSG['5236'] : '{ L_5236 }')); ?></a>
      				<ul class="dropdown-menu">
      					<?php $_helpbox_count = (isset($this->_tpldata['helpbox'])) ? sizeof($this->_tpldata['helpbox']) : 0;if ($_helpbox_count) {for ($_helpbox_i = 0; $_helpbox_i < $_helpbox_count; ++$_helpbox_i){$_helpbox_val = &$this->_tpldata['helpbox'][$_helpbox_i]; ?>
      					<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>viewhelp.php?cat=<?php echo $_helpbox_val['ID']; ?>" alt="faqs" data-fancybox-type="iframe" class="infoboxs"><?php echo $_helpbox_val['TITLE']; ?></a></li>
      					<?php }} ?>
      				</ul>
      			</li>
      			<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>email_request_support.php"><?php echo ((isset($this->_rootref['L_350_10207'])) ? $this->_rootref['L_350_10207'] : ((isset($MSG['350_10207'])) ? $MSG['350_10207'] : '{ L_350_10207 }')); ?></a></li>
            </ul>
          </li>
          <?php } ?>
          <li class="divider-vertical hidden-phone"></li>
          <form class="navbar-form pull-left" action="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>search.php" method="get" enctype="multipart/form-data">
            <input type="text" class="search-query" placeholder="<?php echo ((isset($this->_rootref['L_103'])) ? $this->_rootref['L_103'] : ((isset($MSG['103'])) ? $MSG['103'] : '{ L_103 }')); ?>" name="q" size="50" value="<?php echo (isset($this->_rootref['Q'])) ? $this->_rootref['Q'] : ''; ?>">
            <button type="submit" class="btn btn-success" name="sub" value="<?php echo ((isset($this->_rootref['L_399'])) ? $this->_rootref['L_399'] : ((isset($MSG['399'])) ? $MSG['399'] : '{ L_399 }')); ?>" ><?php echo ((isset($this->_rootref['L_199'])) ? $this->_rootref['L_199'] : ((isset($MSG['199'])) ? $MSG['199'] : '{ L_199 }')); ?></button>
            <a class="btn btn-info" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>adsearch.php"><?php echo ((isset($this->_rootref['L_464'])) ? $this->_rootref['L_464'] : ((isset($MSG['464'])) ? $MSG['464'] : '{ L_464 }')); ?></a>
          </form>
        </ul>
      </div>
    </div>
  </div>
</div>