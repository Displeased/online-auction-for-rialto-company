<div class="row">
<?php $this->_tpl_include('u_header.tpl'); ?>
<div class="span9">
<?php if ($this->_rootref['B_MENUTITLE']) {  ?>
<legend><?php echo (isset($this->_rootref['UCP_TITLE'])) ? $this->_rootref['UCP_TITLE'] : ''; ?></legend>
<?php } ?>
<div class="hero-unit">
  <h3><?php echo (isset($this->_rootref['USERNICK'])) ? $this->_rootref['USERNICK'] : ''; ?> <small>(<?php echo (isset($this->_rootref['USERFB'])) ? $this->_rootref['USERFB'] : ''; ?>) <?php echo (isset($this->_rootref['USERFBIMG'])) ? $this->_rootref['USERFBIMG'] : ''; ?></small></h3>
</div>
<table class="table table-bordered table-striped">
  <?php $_fbs_count = (isset($this->_tpldata['fbs'])) ? sizeof($this->_tpldata['fbs']) : 0;if ($_fbs_count) {for ($_fbs_i = 0; $_fbs_i < $_fbs_count; ++$_fbs_i){$_fbs_val = &$this->_tpldata['fbs'][$_fbs_i]; ?>
  <tr>
    <td style="width:20px;"><img src="<?php echo $_fbs_val['IMG']; ?>" align="middle" alt=""> </td>
    <td style="width:50%"><b><?php echo ((isset($this->_rootref['L_504'])) ? $this->_rootref['L_504'] : ((isset($MSG['504'])) ? $MSG['504'] : '{ L_504 }')); ?>: </b><?php echo $_fbs_val['FEEDBACK']; ?> </td>
    <td><a href="<?php echo $_fbs_val['USFLINK']; ?>"><small><?php echo $_fbs_val['USERNAME']; ?> (<?php echo $_fbs_val['USFEED']; ?>)</a> &nbsp;<?php echo $_fbs_val['USICON']; ?><br />
      (<?php echo ((isset($this->_rootref['L_506'])) ? $this->_rootref['L_506'] : ((isset($MSG['506'])) ? $MSG['506'] : '{ L_506 }')); echo $_fbs_val['FBDATE']; ?> <?php echo ((isset($this->_rootref['L_25_0177'])) ? $this->_rootref['L_25_0177'] : ((isset($MSG['25_0177'])) ? $MSG['25_0177'] : '{ L_25_0177 }')); ?> <?php echo $_fbs_val['AUCTIONURL']; ?>)</small> </td>
  </tr>
  <?php }} ?>
</table>
<div class="pagination pagination-centered">
  <ul>
    <?php echo (isset($this->_rootref['PAGENATION'])) ? $this->_rootref['PAGENATION'] : ''; ?>
  </ul>
</div>
<?php $this->_tpl_include('user_menu_footer.tpl'); ?>