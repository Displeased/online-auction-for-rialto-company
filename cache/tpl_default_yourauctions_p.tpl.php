<script type="text/javascript">

$(document).ready(function() {

	$("#startall").click(function() {
		var checked_status = this.checked;
        $("input[id=start]").each(function()
        { 
        	this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
        });	
	});
	
	$("#deleteall").click(function() {
		var checked_status = this.checked;
        $("input[id=delete]").each(function()
        { 
        	this.checked = checked_status;
        	if($(this).is(':checked'))
        	{
        		$(this).attr('checked',true)
        	}
        	else
        	{
            	$(this).attr('checked',false)
            }
        });	
	});

	$("#processdel").submit(function() {
		if (confirm('<?php echo ((isset($this->_rootref['L_30_0087'])) ? $this->_rootref['L_30_0087'] : ((isset($MSG['30_0087'])) ? $MSG['30_0087'] : '{ L_30_0087 }')); ?>')){
			return true;
		} else {
			return false;
		}
	});
});

</script>

<div class="row">
<?php $this->_tpl_include('u_header.tpl'); ?>
<div class="span9">
  <legend><?php echo ((isset($this->_rootref['L_25_0115'])) ? $this->_rootref['L_25_0115'] : ((isset($MSG['25_0115'])) ? $MSG['25_0115'] : '{ L_25_0115 }')); ?></legend>
  <form name="open" method="post" action="" id="processdel">
    <input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
    <small><span class="muted"><?php echo ((isset($this->_rootref['L_5117'])) ? $this->_rootref['L_5117'] : ((isset($MSG['5117'])) ? $MSG['5117'] : '{ L_5117 }')); ?>&nbsp;<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>&nbsp;<?php echo ((isset($this->_rootref['L_5118'])) ? $this->_rootref['L_5118'] : ((isset($MSG['5118'])) ? $MSG['5118'] : '{ L_5118 }')); ?>&nbsp;<?php echo (isset($this->_rootref['PAGES'])) ? $this->_rootref['PAGES'] : ''; ?></span></small>
    <table class="table table-bordered table-condensed table-striped">
      <tr>
        <td ><a href="yourauctions_p.php?pa_ord=title&pa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo ((isset($this->_rootref['L_624'])) ? $this->_rootref['L_624'] : ((isset($MSG['624'])) ? $MSG['624'] : '{ L_624 }')); ?></a>
          <?php if ($this->_rootref['ORDERCOL'] == ('title')) {  ?>
          <a href="yourauctions_p.php?pa_ord=title&pa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></a>
          <?php } ?>
        </td>
        <td  width="15%"><a href="yourauctions_p.php?pa_ord=starts&pa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo ((isset($this->_rootref['L_25_0116'])) ? $this->_rootref['L_25_0116'] : ((isset($MSG['25_0116'])) ? $MSG['25_0116'] : '{ L_25_0116 }')); ?></a>
          <?php if ($this->_rootref['ORDERCOL'] == ('starts')) {  ?>
          <a href="yourauctions_p.php?pa_ord=starts&pa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></a>
          <?php } ?>
        </td>
        <td  width="15%"><a href="yourauctions_p.php?pa_ord=ends&pa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo ((isset($this->_rootref['L_25_0117'])) ? $this->_rootref['L_25_0117'] : ((isset($MSG['25_0117'])) ? $MSG['25_0117'] : '{ L_25_0117 }')); ?></a>
          <?php if ($this->_rootref['ORDERCOL'] == ('ends')) {  ?>
          <a href="yourauctions_p.php?pa_ord=ends&pa_type=<?php echo (isset($this->_rootref['ORDERNEXT'])) ? $this->_rootref['ORDERNEXT'] : ''; ?>"><?php echo (isset($this->_rootref['ORDERTYPEIMG'])) ? $this->_rootref['ORDERTYPEIMG'] : ''; ?></a>
          <?php } ?>
        </td>
        <td  width="10%" align="center"> <?php echo ((isset($this->_rootref['L_298'])) ? $this->_rootref['L_298'] : ((isset($MSG['298'])) ? $MSG['298'] : '{ L_298 }')); ?> </td>
        <td  width="10%" align="center"> <?php echo ((isset($this->_rootref['L_008'])) ? $this->_rootref['L_008'] : ((isset($MSG['008'])) ? $MSG['008'] : '{ L_008 }')); ?> </td>
        <td  width="9%" align="center"> <?php echo ((isset($this->_rootref['L_25_0118'])) ? $this->_rootref['L_25_0118'] : ((isset($MSG['25_0118'])) ? $MSG['25_0118'] : '{ L_25_0118 }')); ?> </td>
      </tr>
      <?php if ($this->_rootref['B_AREITEMS']) {  $_items_count = (isset($this->_tpldata['items'])) ? sizeof($this->_tpldata['items']) : 0;if ($_items_count) {for ($_items_i = 0; $_items_i < $_items_count; ++$_items_i){$_items_val = &$this->_tpldata['items'][$_items_i]; ?>
      <tr>
        <td width="32%"><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo $_items_val['SEO_TITLE']; ?>-<?php echo $_items_val['ID']; ?>"><?php echo $_items_val['TITLE']; ?></a> </td>
        <td width="11%"><small><?php echo $_items_val['STARTS']; ?></small> </td>
        <td width="11%"><small><?php echo $_items_val['ENDS']; ?></small> </td>
        <td width="6%"  align="center"><?php if ($_items_val['B_HASNOBIDS']) {  ?>
          <a href="edit_active_auction.php?id=<?php echo $_items_val['ID']; ?>"><img src="images/edititem.gif" width="13" height="17" alt="<?php echo ((isset($this->_rootref['L_512'])) ? $this->_rootref['L_512'] : ((isset($MSG['512'])) ? $MSG['512'] : '{ L_512 }')); ?>" border="0"></a>
          <?php } ?>
        </td>
        <td width="8%" align="center"><?php if ($_items_val['B_HASNOBIDS']) {  ?>
          <input type="checkbox" name="O_delete[]" value="<?php echo $_items_val['ID']; ?>" id="delete">
          <?php } ?>
        </td>
        <td width="6%" align="center"><input type="checkbox" name="startnow[]" value="<?php echo $_items_val['ID']; ?>" id="start">
        </td>
      </tr>
      <?php }} } ?>
      <tr <?php echo (isset($this->_rootref['BGCOLOUR'])) ? $this->_rootref['BGCOLOUR'] : ''; ?>>
        <td colspan="4" style="text-align:right;"><small><span class="muted"><?php echo ((isset($this->_rootref['L_30_0102'])) ? $this->_rootref['L_30_0102'] : ((isset($MSG['30_0102'])) ? $MSG['30_0102'] : '{ L_30_0102 }')); ?></span></small></td>
        <td align="center"><input type="checkbox" id="deleteall"></td>
        <td align="center"><input type="checkbox" id="startall"></td>
      </tr>
      <tr>
        <td colspan="10" align="center"><input type="hidden" name="action" value="delopenauctions">
          <input type="submit" name="Submit" value="<?php echo ((isset($this->_rootref['L_631'])) ? $this->_rootref['L_631'] : ((isset($MSG['631'])) ? $MSG['631'] : '{ L_631 }')); ?>" class="btn btn-primary">
        </td>
      </tr>
    </table>
  </form>
  <div class="pagination pagination-centered">
    <ul>
      <li><?php echo (isset($this->_rootref['PREV'])) ? $this->_rootref['PREV'] : ''; ?></li>
      <?php $_pages_count = (isset($this->_tpldata['pages'])) ? sizeof($this->_tpldata['pages']) : 0;if ($_pages_count) {for ($_pages_i = 0; $_pages_i < $_pages_count; ++$_pages_i){$_pages_val = &$this->_tpldata['pages'][$_pages_i]; ?>
      <li><?php echo $_pages_val['PAGE']; ?></li>
      <?php }} ?>
      <li><?php echo (isset($this->_rootref['NEXT'])) ? $this->_rootref['NEXT'] : ''; ?></li>
    </ul>
  </div>
<?php $this->_tpl_include('user_menu_footer.tpl'); ?>