<div class="form-actions"> <a class="btn btn-primary" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>products/<?php echo (isset($this->_rootref['SEO'])) ? $this->_rootref['SEO'] : ''; ?>"> <i class="icon-chevron-left icon-white"></i> <?php echo ((isset($this->_rootref['L_138'])) ? $this->_rootref['L_138'] : ((isset($MSG['138'])) ? $MSG['138'] : '{ L_138 }')); ?></a> </div>
<?php if ($this->_rootref['EMAILSENT'] == ('')) {  ?>
<div align="center" class="alert alert-info"> <strong><?php echo ((isset($this->_rootref['L_017'])) ? $this->_rootref['L_017'] : ((isset($MSG['017'])) ? $MSG['017'] : '{ L_017 }')); ?> : <?php echo (isset($this->_rootref['TITLE'])) ? $this->_rootref['TITLE'] : ''; ?></strong><br />
  <?php echo ((isset($this->_rootref['L_146'])) ? $this->_rootref['L_146'] : ((isset($MSG['146'])) ? $MSG['146'] : '{ L_146 }')); ?> <strong><?php echo (isset($this->_rootref['FRIEND_EMAIL'])) ? $this->_rootref['FRIEND_EMAIL'] : ''; ?></strong> <br />
  <br />
</div>
<?php } else { ?>
<div class="row">
  <div class="span8 offset2 well">
    <legend><?php echo ((isset($this->_rootref['L_139'])) ? $this->_rootref['L_139'] : ((isset($MSG['139'])) ? $MSG['139'] : '{ L_139 }')); ?></legend>
    <form class="form-horizontal" name="friend" action="friend.php" method="post" enctype="multipart/form-data">
      <input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
      <?php if ($this->_rootref['ERROR'] != ('')) {  ?>
      <div align="center" class="alert alert-error"><?php echo (isset($this->_rootref['ERROR'])) ? $this->_rootref['ERROR'] : ''; ?></div>
      <?php } ?>
      <strong><?php echo ((isset($this->_rootref['L_017'])) ? $this->_rootref['L_017'] : ((isset($MSG['017'])) ? $MSG['017'] : '{ L_017 }')); ?></strong><br />
      <?php echo (isset($this->_rootref['TITLE'])) ? $this->_rootref['TITLE'] : ''; ?><br />
      <br />
      <div class="control-group">
        <label class="control-label" for="inputEmail"><?php echo ((isset($this->_rootref['L_140'])) ? $this->_rootref['L_140'] : ((isset($MSG['140'])) ? $MSG['140'] : '{ L_140 }')); ?></label>
        <div class="controls">
          <input type="text" name="friend_name" size="25" value="<?php echo (isset($this->_rootref['FRIEND_NAME'])) ? $this->_rootref['FRIEND_NAME'] : ''; ?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="inputEmail"><?php echo ((isset($this->_rootref['L_141'])) ? $this->_rootref['L_141'] : ((isset($MSG['141'])) ? $MSG['141'] : '{ L_141 }')); ?></label>
        <div class="controls">
          <input type="text" name="friend_email" size="25" value="<?php echo (isset($this->_rootref['FRIEND_EMAIL'])) ? $this->_rootref['FRIEND_EMAIL'] : ''; ?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="inputEmail"><?php echo ((isset($this->_rootref['L_002'])) ? $this->_rootref['L_002'] : ((isset($MSG['002'])) ? $MSG['002'] : '{ L_002 }')); ?></label>
        <div class="controls">
          <input type="text" name="sender_name" size="25" value="<?php echo (isset($this->_rootref['YOUR_NAME'])) ? $this->_rootref['YOUR_NAME'] : ''; ?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="inputEmail"><?php echo ((isset($this->_rootref['L_143'])) ? $this->_rootref['L_143'] : ((isset($MSG['143'])) ? $MSG['143'] : '{ L_143 }')); ?></label>
        <div class="controls">
          <input type="text" name="sender_email" size="25" value="<?php echo (isset($this->_rootref['YOUR_EMAIL'])) ? $this->_rootref['YOUR_EMAIL'] : ''; ?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="inputEmail"><?php echo ((isset($this->_rootref['L_144'])) ? $this->_rootref['L_144'] : ((isset($MSG['144'])) ? $MSG['144'] : '{ L_144 }')); ?></label>
        <div class="controls">
          <textarea name="sender_comment" cols="30" rows="6"><?php echo (isset($this->_rootref['COMMENT'])) ? $this->_rootref['COMMENT'] : ''; ?></textarea>
        </div>
      </div>
      <?php echo (isset($this->_rootref['CAPCHA'])) ? $this->_rootref['CAPCHA'] : ''; ?>
      <div class="form-actions" style="margin-top:40px">
        <input type="hidden" name="id" value="<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>">
        <input type="hidden" name="item_title" value="<?php echo (isset($this->_rootref['TITLE'])) ? $this->_rootref['TITLE'] : ''; ?>">
        <input type="hidden" name="action" value="sendmail">
        <input type="submit" name="" value="<?php echo ((isset($this->_rootref['L_5201'])) ? $this->_rootref['L_5201'] : ((isset($MSG['5201'])) ? $MSG['5201'] : '{ L_5201 }')); ?>" class="btn btn-primary">
        <input type="reset" name="" value="<?php echo ((isset($this->_rootref['L_035'])) ? $this->_rootref['L_035'] : ((isset($MSG['035'])) ? $MSG['035'] : '{ L_035 }')); ?>" class="btn">
      </div>
    </form>
  </div>
  <?php } ?>