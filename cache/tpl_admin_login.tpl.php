<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Admin - Login Page</title>

	<!-- Required Stylesheets -->
    <link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/bootstrap/css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/fonts/ptsans/stylesheet.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/fonts/icomoon/style.css" media="screen" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/login.css" media="screen" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/css/mws-theme.css" media="screen" />
        <script type="text/javascript" src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>loader.php?js=js/jquery.js<?php echo (isset($this->_rootref['EXTRAJS'])) ? $this->_rootref['EXTRAJS'] : ''; ?>"></script>

    <!-- JavaScript Plugins -->
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/libs/jquery.placeholder.min.js"></script>
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/custom-plugins/fileinput.js"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/jui/js/jquery-ui-effects.min.js"></script>

    <!-- Plugin Scripts -->
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/plugins/validate/jquery.validate-min.js"></script>

    <!-- Login Script -->
    <script src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>themes/admin/js/core/login.js"></script>
</head>

<body>
    <div id="mws-login-wrapper">
        <div id="mws-login">
            <h1><?php echo ((isset($this->_rootref['L_052'])) ? $this->_rootref['L_052'] : ((isset($MSG['052'])) ? $MSG['052'] : '{ L_052 }')); ?></h1>
            <div class="mws-login-lock"><i class="icon-lock" style="margin-top:9px"></i></div>
            <div id="mws-login-form">
                <form class="mws-form" action="login.php" method="post">
                <?php if ($this->_rootref['PAGE'] == (1)) {  ?>
            	<h4 style="color:red;"><?php echo ((isset($this->_rootref['L_441'])) ? $this->_rootref['L_441'] : ((isset($MSG['441'])) ? $MSG['441'] : '{ L_441 }')); ?></h4>
                <h2 style="color:aqua"><?php echo ((isset($this->_rootref['L_3500_1015459'])) ? $this->_rootref['L_3500_1015459'] : ((isset($MSG['3500_1015459'])) ? $MSG['3500_1015459'] : '{ L_3500_1015459 }')); ?></h2>
            	<?php } if ($this->_rootref['ERROR'] != ('')) {  ?>
					<p style="color:red;"><?php echo (isset($this->_rootref['ERROR'])) ? $this->_rootref['ERROR'] : ''; ?></p>
				<?php } ?>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="username" class="mws-login-username required" placeholder="<?php echo ((isset($this->_rootref['L_003'])) ? $this->_rootref['L_003'] : ((isset($MSG['003'])) ? $MSG['003'] : '{ L_003 }')); ?>">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="password" name="password" class="mws-login-password required" placeholder="<?php echo ((isset($this->_rootref['L_004'])) ? $this->_rootref['L_004'] : ((isset($MSG['004'])) ? $MSG['004'] : '{ L_004 }')); ?>">
                        </div>
                    </div>
                    <?php if ($this->_rootref['PAGE'] == (1)) {  ?>
                    <h2 style="color:aqua"><?php echo ((isset($this->_rootref['L_3500_1015460'])) ? $this->_rootref['L_3500_1015460'] : ((isset($MSG['3500_1015460'])) ? $MSG['3500_1015460'] : '{ L_3500_1015460 }')); ?></h2>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="full_name" class="mws-login-username required" placeholder="<?php echo ((isset($this->_rootref['L_002'])) ? $this->_rootref['L_002'] : ((isset($MSG['002'])) ? $MSG['002'] : '{ L_002 }')); ?>">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="user_name" class="mws-login-username required" placeholder="<?php echo ((isset($this->_rootref['L_003'])) ? $this->_rootref['L_003'] : ((isset($MSG['003'])) ? $MSG['003'] : '{ L_003 }')); ?>">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="password" name="pass_word" class="mws-login-password required" placeholder="<?php echo ((isset($this->_rootref['L_004'])) ? $this->_rootref['L_004'] : ((isset($MSG['004'])) ? $MSG['004'] : '{ L_004 }')); ?>">
                        </div>
                    </div>
                    <p style="color:aqua"><?php echo ((isset($this->_rootref['L_3500_1015461'])) ? $this->_rootref['L_3500_1015461'] : ((isset($MSG['3500_1015461'])) ? $MSG['3500_1015461'] : '{ L_3500_1015461 }')); ?></p>
                    <input type="hidden" name="action" value="insert">
                    <div class="mws-form-row">
                        <input type="submit" value="<?php echo ((isset($this->_rootref['L_5204'])) ? $this->_rootref['L_5204'] : ((isset($MSG['5204'])) ? $MSG['5204'] : '{ L_5204 }')); ?>" class="btn btn-success mws-login-button">
                    </div>
                    <?php } else { ?>
                    <input type="hidden" name="action" value="login">
                    <div class="mws-form-row">
                        <input type="submit" value="<?php echo ((isset($this->_rootref['L_052'])) ? $this->_rootref['L_052'] : ((isset($MSG['052'])) ? $MSG['052'] : '{ L_052 }')); ?>" class="btn btn-success mws-login-button">
                    </div>
                    <?php } ?>
                </form>
                <?php if ($this->_rootref['B_MULT_LANGS']) {  ?>
						<h1><?php echo ((isset($this->_rootref['L_2__0001'])) ? $this->_rootref['L_2__0001'] : ((isset($MSG['2__0001'])) ? $MSG['2__0001'] : '{ L_2__0001 }')); ?></h1>
                    	<br /><p><?php echo (isset($this->_rootref['FLAGS'])) ? $this->_rootref['FLAGS'] : ''; ?> </p>
				<?php } ?>
            </div>
        </div>