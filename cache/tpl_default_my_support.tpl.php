<script type="text/javascript">
$(document).ready(function () {
    $("#checkboxalldelete").click(function () {
        var checked_status = this.checked
        $(".deleteid").each(function () {
            this.checked = checked_status;
        });
    });
});

$(document).ready(function () {
    $("#checkboxallclose").click(function () {
        var checked_status = this.checked
        $(".closeid").each(function () {
            this.checked = checked_status;
        });
    });
});

$(".form1").submit(function () {
    if ($(".to").val() == "") {
        return false;
    }
    if ($(".subject").val() == "") {
        return false;
    }
    if ($(".message").val() == "") {
        return false;
    }
    return true;
});
</script>
<div class="row">
<?php $this->_tpl_include('u_header.tpl'); ?>
<div class="span9">
  	<legend><?php echo ((isset($this->_rootref['L_3500_1015432'])) ? $this->_rootref['L_3500_1015432'] : ((isset($MSG['3500_1015432'])) ? $MSG['3500_1015432'] : '{ L_3500_1015432 }')); ?></legend>
  	<?php if ($this->_rootref['B_ISERROR']) {  ?>
  	<div class="alert">
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    	<?php echo (isset($this->_rootref['ERROR'])) ? $this->_rootref['ERROR'] : ''; ?> 
    </div>
  	<?php } ?>
	<ul class="nav nav-tabs">
		<li class="active"><a href="#submitted" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015439i'])) ? $this->_rootref['L_3500_1015439i'] : ((isset($MSG['3500_1015439i'])) ? $MSG['3500_1015439i'] : '{ L_3500_1015439i }')); ?></a></li>
		<li><a href="#new" data-toggle="tab"><?php echo ((isset($this->_rootref['L_3500_1015439h'])) ? $this->_rootref['L_3500_1015439h'] : ((isset($MSG['3500_1015439h'])) ? $MSG['3500_1015439h'] : '{ L_3500_1015439h }')); ?></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane well tab-pane fade in" id="new">
			<form name="submite_new_ticket" id="form1" method="post" class="form-horizontal" action="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>support" enctype="multipart/form-data">
		    	<input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
		    	<div class="control-group">
		      		<label class="control-label" for="to"><?php echo ((isset($this->_rootref['L_241'])) ? $this->_rootref['L_241'] : ((isset($MSG['241'])) ? $MSG['241'] : '{ L_241 }')); ?>:</label>
		      		<div class="controls" style="margin-top:5px"><?php echo ((isset($this->_rootref['L_3500_1015436'])) ? $this->_rootref['L_3500_1015436'] : ((isset($MSG['3500_1015436'])) ? $MSG['3500_1015436'] : '{ L_3500_1015436 }')); ?></div>
		    	</div>
		    	<div class="control-group">
		      		<label class="control-label" for="subject"><?php echo ((isset($this->_rootref['L_332'])) ? $this->_rootref['L_332'] : ((isset($MSG['332'])) ? $MSG['332'] : '{ L_332 }')); ?>:</label>
		      		<div class="controls">
		        		<input name="subject" maxlength="75" type="text" size="60" value="<?php echo (isset($this->_rootref['SUBJECT'])) ? $this->_rootref['SUBJECT'] : ''; ?>" id="subject" maxlength="50">
		      		</div>
		    	</div>
		    	<div class="control-group">
		      		<label class="control-label" for="message"><?php echo ((isset($this->_rootref['L_333'])) ? $this->_rootref['L_333'] : ((isset($MSG['333'])) ? $MSG['333'] : '{ L_333 }')); ?>:</label>
		      		<div class="controls"><?php echo (isset($this->_rootref['MESSAGE'])) ? $this->_rootref['MESSAGE'] : ''; ?></div>
		    	</div>
		    	<div class="form-actions">
		      		<input name="submit" type="submit" value="<?php echo ((isset($this->_rootref['L_007'])) ? $this->_rootref['L_007'] : ((isset($MSG['007'])) ? $MSG['007'] : '{ L_007 }')); ?>" class="btn btn-primary">
		    	</div>
			</form>
		</div>

		<div class="tab-pane tab-pane fade in active" id="submitted">
			<form action="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>support" method="post" name="deletemessages" enctype="multipart/form-data">
		    	<input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
		    	<table class="table table-bordered table-condensed table-striped">
			      	<thead>
			        	<tr>
			          		<th width="15%"><?php echo ((isset($this->_rootref['L_3500_1015439d'])) ? $this->_rootref['L_3500_1015439d'] : ((isset($MSG['3500_1015439d'])) ? $MSG['3500_1015439d'] : '{ L_3500_1015439d }')); ?></th>
			          		<th width="30%"><?php echo ((isset($this->_rootref['L_332'])) ? $this->_rootref['L_332'] : ((isset($MSG['332'])) ? $MSG['332'] : '{ L_332 }')); ?></th>
			          		<th><?php echo ((isset($this->_rootref['L_3500_1015439e'])) ? $this->_rootref['L_3500_1015439e'] : ((isset($MSG['3500_1015439e'])) ? $MSG['3500_1015439e'] : '{ L_3500_1015439e }')); ?></th>
			          		<th width="15%"><?php echo ((isset($this->_rootref['L_3500_1015439f'])) ? $this->_rootref['L_3500_1015439f'] : ((isset($MSG['3500_1015439f'])) ? $MSG['3500_1015439f'] : '{ L_3500_1015439f }')); ?></th>
			          		<th width="18%"><?php echo ((isset($this->_rootref['L_3500_1015439g'])) ? $this->_rootref['L_3500_1015439g'] : ((isset($MSG['3500_1015439g'])) ? $MSG['3500_1015439g'] : '{ L_3500_1015439g }')); ?></th>
			          		<th><small><span class="muted"><?php echo ((isset($this->_rootref['L_678'])) ? $this->_rootref['L_678'] : ((isset($MSG['678'])) ? $MSG['678'] : '{ L_678 }')); ?></span></small><br>
			            		<input type="checkbox" name="" id="checkboxallclose" value="">
			            	</th>
			          		<th><small><span class="muted"><?php echo ((isset($this->_rootref['L_008'])) ? $this->_rootref['L_008'] : ((isset($MSG['008'])) ? $MSG['008'] : '{ L_008 }')); ?></span></small><br>
			            		<input type="checkbox" name="" id="checkboxalldelete" value="">
			            	</th>
			        	</tr>
			      	</thead>
			      	<tbody>
			        	<?php if ($this->_rootref['MSGCOUNT'] == 0) {  ?>
			        	<tr>
			          		<td colspan="7"><?php echo ((isset($this->_rootref['L_3500_1015439r'])) ? $this->_rootref['L_3500_1015439r'] : ((isset($MSG['3500_1015439r'])) ? $MSG['3500_1015439r'] : '{ L_3500_1015439r }')); ?></td>
			        	</tr>
			        	<?php } else { $_ticket_count = (isset($this->_tpldata['ticket'])) ? sizeof($this->_tpldata['ticket']) : 0;if ($_ticket_count) {for ($_ticket_i = 0; $_ticket_i < $_ticket_count; ++$_ticket_i){$_ticket_val = &$this->_tpldata['ticket'][$_ticket_i]; ?>
			        	<tr>
			          		<td><small><span class="muted"><?php echo $_ticket_val['CREATED']; ?></span></small></td>
			          		<td><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>support-<?php echo $_ticket_val['TICKET_ID']; ?>"><?php echo $_ticket_val['TICKET_TITLE']; ?></a></td>
			          		<td><?php echo $_ticket_val['LAST_UPDATE_USER']; ?></td>
			          		<td><?php if ($_ticket_val['TICKET_STATUS']) {  ?> <span style="color:green"><b><?php echo ((isset($this->_rootref['L_3500_1015439a'])) ? $this->_rootref['L_3500_1015439a'] : ((isset($MSG['3500_1015439a'])) ? $MSG['3500_1015439a'] : '{ L_3500_1015439a }')); ?></b></span><?php } else { ?><span style="color:red"><b><?php echo ((isset($this->_rootref['L_3500_1015439b'])) ? $this->_rootref['L_3500_1015439b'] : ((isset($MSG['3500_1015439b'])) ? $MSG['3500_1015439b'] : '{ L_3500_1015439b }')); ?></b></span><?php } ?></td>
			          		<td><?php echo $_ticket_val['LAST_UPDATED_TIME']; ?></td>
			          		<?php if ($_ticket_val['TICKET_STATUS']) {  ?>
			          		<td style="text-align:center">
			          			<input type="checkbox" name="closeid[]" class="closeid" value="<?php echo $_ticket_val['TICKET_ID']; ?>">
			          		</td>
			          		<?php } else { ?>
			          		<td></td>
			          		<?php } ?>
			          		<td style="text-align:center">
			          			<input type="checkbox" name="deleteid[]" class="deleteid" value="<?php echo $_ticket_val['TICKET_ID']; ?>">
			          		</td>
			        	</tr>
			        	<?php }} } ?>
			      	</tbody>
		    	</table>
		    	<div align="right">
		      		<input class="btn btn-primary" type="submit" name="submit" value="<?php echo ((isset($this->_rootref['L_071'])) ? $this->_rootref['L_071'] : ((isset($MSG['071'])) ? $MSG['071'] : '{ L_071 }')); ?>">
		    	</div>
			</form>
		</div>
	</div>

<?php $this->_tpl_include('user_menu_footer.tpl'); ?>