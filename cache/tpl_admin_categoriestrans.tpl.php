<?php $this->_tpl_include('header.tpl'); ?>
<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> <?php echo (isset($this->_rootref['PAGENAME'])) ? $this->_rootref['PAGENAME'] : ''; ?></span>
                    </div>
                    <form name="errorlog" action="" method="post">
                    <input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="act" class="btn btn-success" value="<?php echo ((isset($this->_rootref['L_089'])) ? $this->_rootref['L_089'] : ((isset($MSG['089'])) ? $MSG['089'] : '{ L_089 }')); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                        	<thead>
                            <?php $_langs_count = (isset($this->_tpldata['langs'])) ? sizeof($this->_tpldata['langs']) : 0;if ($_langs_count) {for ($_langs_i = 0; $_langs_i < $_langs_count; ++$_langs_i){$_langs_val = &$this->_tpldata['langs'][$_langs_i]; ?>
                            	<tr>
                                	<th><?php echo ((isset($this->_rootref['L_161'])) ? $this->_rootref['L_161'] : ((isset($MSG['161'])) ? $MSG['161'] : '{ L_161 }')); ?></th>
                                    <th><a href="categoriestrans.php?lang=<?php echo $_langs_val['LANG']; ?>"><img align="middle" src="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>inc/flags/<?php echo $_langs_val['LANG']; ?>.gif" border="0"></a></th>
                                </tr>
                                <?php }} ?>
                                <tr>
                                    <th><?php echo ((isset($this->_rootref['L_771'])) ? $this->_rootref['L_771'] : ((isset($MSG['771'])) ? $MSG['771'] : '{ L_771 }')); ?></th>
                                    <th><?php echo ((isset($this->_rootref['L_772'])) ? $this->_rootref['L_772'] : ((isset($MSG['772'])) ? $MSG['772'] : '{ L_772 }')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $_cats_count = (isset($this->_tpldata['cats'])) ? sizeof($this->_tpldata['cats']) : 0;if ($_cats_count) {for ($_cats_i = 0; $_cats_i < $_cats_count; ++$_cats_i){$_cats_val = &$this->_tpldata['cats'][$_cats_i]; ?>
                            <tr <?php echo $_cats_val['BG']; ?>>
                            		<td><input type="text" name="categories_o[]" value="<?php echo $_cats_val['CAT_NAME']; ?>" size="45" disabled></td>
                                    <td><input type="text" name="categories[<?php echo $_cats_val['CAT_ID']; ?>]" value="<?php echo $_cats_val['TRAN_CAT']; ?>" size="45"></td>
                            </tr>
                            <?php }} ?>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
<?php $this->_tpl_include('footer.tpl'); ?>