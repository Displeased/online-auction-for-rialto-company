<div>
  <ul class="breadcrumb">
    <li class="active"><?php echo ((isset($this->_rootref['L_287'])) ? $this->_rootref['L_287'] : ((isset($MSG['287'])) ? $MSG['287'] : '{ L_287 }')); ?> : </li>
    <?php echo (isset($this->_rootref['CAT_STRING'])) ? $this->_rootref['CAT_STRING'] : ''; ?>
  </ul>
</div>
<div class="row ">
  <div class="span3 hidden-phone"> <?php echo (isset($this->_rootref['cat_name'])) ? $this->_rootref['cat_name'] : ''; ?>
    <div class="row">
      <div id="cat-list-holder" class="span3" style="position:relative;">
        <ul class="well nav nav-list cat-list">
          <li class="nav-header"><?php echo ((isset($this->_rootref['L_276'])) ? $this->_rootref['L_276'] : ((isset($MSG['276'])) ? $MSG['276'] : '{ L_276 }')); ?></li>
          <li class="divider"></li>
          <li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>cat/<?php echo ((isset($this->_rootref['L_277_1'])) ? $this->_rootref['L_277_1'] : ((isset($MSG['277_1'])) ? $MSG['277_1'] : '{ L_277_1 }')); ?>-0"><i class="icon-tags"></i> <?php echo ((isset($this->_rootref['L_277'])) ? $this->_rootref['L_277'] : ((isset($MSG['277'])) ? $MSG['277'] : '{ L_277 }')); ?></a></li>
          <li class="divider"></li>
          <?php $_cat_list_drop_2_count = (isset($this->_tpldata['cat_list_drop_2'])) ? sizeof($this->_tpldata['cat_list_drop_2']) : 0;if ($_cat_list_drop_2_count) {for ($_cat_list_drop_2_i = 0; $_cat_list_drop_2_i < $_cat_list_drop_2_count; ++$_cat_list_drop_2_i){$_cat_list_drop_2_val = &$this->_tpldata['cat_list_drop_2'][$_cat_list_drop_2_i]; ?>
          <li> <a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>cat/<?php echo $_cat_list_drop_2_val['SEO_NAME']; ?>-<?php echo $_cat_list_drop_2_val['ID']; ?>"><?php echo $_cat_list_drop_2_val['IMAGE']; ?> <?php echo $_cat_list_drop_2_val['NAME']; ?></a></li>
          <?php }} ?>
        </ul><br>
        <?php echo (isset($this->_rootref['BROWSE_ADSENSE_1'])) ? $this->_rootref['BROWSE_ADSENSE_1'] : ''; ?>
      </div>
    </div>
  </div>
  <div class="row">
    <div id="browser-holder" class="span9">
      <div class="">
        <legend><?php echo (isset($this->_rootref['CUR_CAT'])) ? $this->_rootref['CUR_CAT'] : ''; ?></legend>
        <br />
        <button id="sub-cats-btn" 
        <?php if ($this->_rootref['TOP_HTML'] == ('')) {  ?>
        style="display:none;"
        <?php } ?>
        type="button" class="btn btn-small" data-toggle="collapse" data-target="#sub-cats"><?php echo ((isset($this->_rootref['L_31_1'])) ? $this->_rootref['L_31_1'] : ((isset($MSG['31_1'])) ? $MSG['31_1'] : '{ L_31_1 }')); ?>
        </button>
        <?php if ($this->_rootref['NUM_AUCTIONS'] == 0) {  ?>
        <div class="alert alert-info" style="margin-top:10px"><i class="icon-info-sign icon-white"></i> <strong><?php echo ((isset($this->_rootref['L_198'])) ? $this->_rootref['L_198'] : ((isset($MSG['198'])) ? $MSG['198'] : '{ L_198 }')); ?></strong> </div>
        <?php } if ($this->_rootref['TOP_HTML'] != ('')) {  ?>
        <div id="sub-cats" class="collapse" style="margin-top:10px"> <small> <?php echo (isset($this->_rootref['TOP_HTML'])) ? $this->_rootref['TOP_HTML'] : ''; ?> </small> </div>
      </div>
      <?php } else { } ?>
      <div class="row">
        <div class="span9">
          <?php if ($this->_rootref['ID'] > 0 && $this->_rootref['NUM_AUCTIONS'] > 0) {  ?>
          <div class="form-actions">
            <div class="row">
              <div class="span4">
                <form class="form-search" name="catsearch" action="?id=<?php echo (isset($this->_rootref['ID'])) ? $this->_rootref['ID'] : ''; ?>" method="post" >
                  <input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
                  <div class="input-append">
                    <input type="text" placeholder="<?php echo ((isset($this->_rootref['L_287'])) ? $this->_rootref['L_287'] : ((isset($MSG['287'])) ? $MSG['287'] : '{ L_287 }')); ?>: <?php echo (isset($this->_rootref['CUR_CAT'])) ? $this->_rootref['CUR_CAT'] : ''; ?> " class="span2 search-query" name="catkeyword">
                    <button type="submit" class="btn btn-success" name="submit"><?php echo ((isset($this->_rootref['L_103'])) ? $this->_rootref['L_103'] : ((isset($MSG['103'])) ? $MSG['103'] : '{ L_103 }')); ?></button>
                  </div>
                </form>
              </div>
              <div class="span2 offset2"><a class="btn btn-primary" href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>adsearch.php"><?php echo ((isset($this->_rootref['L_464'])) ? $this->_rootref['L_464'] : ((isset($MSG['464'])) ? $MSG['464'] : '{ L_464 }')); ?></a> </div>
            </div>
          </div>
        </div>
        <?php } if ($this->_rootref['NUM_AUCTIONS'] > 0) {  ?>
      </div>
      <?php $this->_tpl_include('browse.tpl'); } else { ?>
    </div>
  </div>
</div>
</div>
<?php } ?>
</div>