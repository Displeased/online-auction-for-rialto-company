<?php $this->_tpl_include('header.tpl'); ?>
<div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icon-business-card"></i> <?php echo ((isset($this->_rootref['L_5024'])) ? $this->_rootref['L_5024'] : ((isset($MSG['5024'])) ? $MSG['5024'] : '{ L_5024 }')); ?></span>
                    </div>
                    <form name="search" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" name="submit" class="btn btn-success" value="<?php echo ((isset($this->_rootref['L_5023'])) ? $this->_rootref['L_5023'] : ((isset($MSG['5023'])) ? $MSG['5023'] : '{ L_5023 }')); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td><?php echo ((isset($this->_rootref['L_5022'])) ? $this->_rootref['L_5022'] : ((isset($MSG['5022'])) ? $MSG['5022'] : '{ L_5022 }')); ?></td>
                            		<td><input type="text" name="keyword" size="25"></td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
                <div class="mws-panel grid_4">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> <?php echo ((isset($this->_rootref['L__0052'])) ? $this->_rootref['L__0052'] : ((isset($MSG['_0052'])) ? $MSG['_0052'] : '{ L__0052 }')); ?></span>
                    </div>
                    <form name="filter" id="filter" action="" method="get">
                    <input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
					<div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                            <input type="submit" class="btn btn-success" value="<?php echo ((isset($this->_rootref['L_5029'])) ? $this->_rootref['L_5029'] : ((isset($MSG['5029'])) ? $MSG['5029'] : '{ L_5029 }')); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <tbody>
                            <tr>
                            		<td><?php echo ((isset($this->_rootref['L_1014'])) ? $this->_rootref['L_1014'] : ((isset($MSG['1014'])) ? $MSG['1014'] : '{ L_1014 }')); ?></td>
                            		<td><select name="usersfilter" id="userfilter">
                                    <option value="all"><?php echo ((isset($this->_rootref['L_5296'])) ? $this->_rootref['L_5296'] : ((isset($MSG['5296'])) ? $MSG['5296'] : '{ L_5296 }')); ?></option>
                                    <option value="active" <?php if ($this->_rootref['USERFILTER'] == ('active')) {  ?>selected<?php } ?>><?php echo ((isset($this->_rootref['L_5291'])) ? $this->_rootref['L_5291'] : ((isset($MSG['5291'])) ? $MSG['5291'] : '{ L_5291 }')); ?></option>
                                    <option value="admin" <?php if ($this->_rootref['USERFILTER'] == ('admin')) {  ?>selected<?php } ?>><?php echo ((isset($this->_rootref['L_5294'])) ? $this->_rootref['L_5294'] : ((isset($MSG['5294'])) ? $MSG['5294'] : '{ L_5294 }')); ?></option>
                                    <option value="fee" <?php if ($this->_rootref['USERFILTER'] == ('fee')) {  ?>selected<?php } ?>><?php echo ((isset($this->_rootref['L_5293'])) ? $this->_rootref['L_5293'] : ((isset($MSG['5293'])) ? $MSG['5293'] : '{ L_5293 }')); ?></option>
                                    <option value="confirmed" <?php if ($this->_rootref['USERFILTER'] == ('confirmed')) {  ?>selected<?php } ?>><?php echo ((isset($this->_rootref['L_5292'])) ? $this->_rootref['L_5292'] : ((isset($MSG['5292'])) ? $MSG['5292'] : '{ L_5292 }')); ?></option>
                                    <option value="admin_approve" <?php if ($this->_rootref['USERFILTER'] == ('admin_approve')) {  ?>selected<?php } ?>><?php echo ((isset($this->_rootref['L_25_0136'])) ? $this->_rootref['L_25_0136'] : ((isset($MSG['25_0136'])) ? $MSG['25_0136'] : '{ L_25_0136 }')); ?></option>
                                </select></td>
                            </tr>
                           </tbody>
                        </table>
                    </div>
                    </form>    	
                </div>
                
                <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-user"></i> <?php echo (isset($this->_rootref['PAGENAME'])) ? $this->_rootref['PAGENAME'] : ''; ?></span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-table">
                            <thead>
                            	<tr>
                                	<th colspan="8">
                                    	<?php echo ((isset($this->_rootref['L_5117'])) ? $this->_rootref['L_5117'] : ((isset($MSG['5117'])) ? $MSG['5117'] : '{ L_5117 }')); ?>&nbsp;<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>&nbsp;<?php echo ((isset($this->_rootref['L_5118'])) ? $this->_rootref['L_5118'] : ((isset($MSG['5118'])) ? $MSG['5118'] : '{ L_5118 }')); ?>&nbsp;<?php echo (isset($this->_rootref['PAGES'])) ? $this->_rootref['PAGES'] : ''; ?>
                                        <br>
                                        <?php echo (isset($this->_rootref['PREV'])) ? $this->_rootref['PREV'] : ''; ?>
            						<?php $_pages_count = (isset($this->_tpldata['pages'])) ? sizeof($this->_tpldata['pages']) : 0;if ($_pages_count) {for ($_pages_i = 0; $_pages_i < $_pages_count; ++$_pages_i){$_pages_val = &$this->_tpldata['pages'][$_pages_i]; ?>
                                        <?php echo $_pages_val['PAGE']; ?>&nbsp;&nbsp;
            						<?php }} ?>
                                        <?php echo (isset($this->_rootref['NEXT'])) ? $this->_rootref['NEXT'] : ''; ?>
                            		</th>
                                </tr>
                                <tr>
                                    <th><?php echo ((isset($this->_rootref['L_293'])) ? $this->_rootref['L_293'] : ((isset($MSG['293'])) ? $MSG['293'] : '{ L_293 }')); ?></th>
                                    <th><?php echo ((isset($this->_rootref['L_294'])) ? $this->_rootref['L_294'] : ((isset($MSG['294'])) ? $MSG['294'] : '{ L_294 }')); ?></th>
                                    <th><?php echo ((isset($this->_rootref['L_295'])) ? $this->_rootref['L_295'] : ((isset($MSG['295'])) ? $MSG['295'] : '{ L_295 }')); ?></th>
                                    <th><?php echo ((isset($this->_rootref['L_296'])) ? $this->_rootref['L_296'] : ((isset($MSG['296'])) ? $MSG['296'] : '{ L_296 }')); ?></th>
                                    <th><?php echo ((isset($this->_rootref['L_25_0079'])) ? $this->_rootref['L_25_0079'] : ((isset($MSG['25_0079'])) ? $MSG['25_0079'] : '{ L_25_0079 }')); ?></th>
                                    <th><?php echo ((isset($this->_rootref['L_763'])) ? $this->_rootref['L_763'] : ((isset($MSG['763'])) ? $MSG['763'] : '{ L_763 }')); ?></th>
                                    <th><?php echo ((isset($this->_rootref['L_560'])) ? $this->_rootref['L_560'] : ((isset($MSG['560'])) ? $MSG['560'] : '{ L_560 }')); ?></th>
                                    <th><?php echo ((isset($this->_rootref['L_297'])) ? $this->_rootref['L_297'] : ((isset($MSG['297'])) ? $MSG['297'] : '{ L_297 }')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $_users_count = (isset($this->_tpldata['users'])) ? sizeof($this->_tpldata['users']) : 0;if ($_users_count) {for ($_users_i = 0; $_users_i < $_users_count; ++$_users_i){$_users_val = &$this->_tpldata['users'][$_users_i]; ?>
                            <tr>
                       		  	<td>
                       		  		<b><?php echo $_users_val['NICK']; ?></b><br>
                       		  		<a href="listauctions.php?uid=<?php echo $_users_val['ID']; ?>&offset=<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_5094'])) ? $this->_rootref['L_5094'] : ((isset($MSG['5094'])) ? $MSG['5094'] : '{ L_5094 }')); ?></small></a><br />
                            		<a href="userfeedback.php?id=<?php echo $_users_val['ID']; ?>&offset=<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_503'])) ? $this->_rootref['L_503'] : ((isset($MSG['503'])) ? $MSG['503'] : '{ L_503 }')); ?></small></a><br />
                            		<a href="viewuserips.php?id=<?php echo $_users_val['ID']; ?>&offset=<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_2_0004'])) ? $this->_rootref['L_2_0004'] : ((isset($MSG['2_0004'])) ? $MSG['2_0004'] : '{ L_2_0004 }')); ?></small></a>
                       		  	</td>
                                <td><?php echo $_users_val['NAME']; ?></td>
                                <td><?php echo $_users_val['COUNTRY']; ?></td>
                                <td><a href="mailto:<?php echo $_users_val['EMAIL']; ?>"><?php echo $_users_val['EMAIL']; ?></a></td>
                                <td><?php echo $_users_val['NEWSLETTER']; ?></td>
                              	<td><?php echo $_users_val['BALANCE']; ?>
                        		<?php if ($_users_val['BALANCE_CLEAN'] < 0) {  ?>
                        		<form name="payreminder" action="" method="post" enctype="multipart/form-data">
                    				<input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
                    				<input type="hidden" name="payreminder_email" value="send">
                    				<input type="hidden" name="user_id" value="<?php echo $_users_val['ID']; ?>">
                    				<div class="mws-button-row">
                    					<input type="submit" value="<?php echo ((isset($this->_rootref['L_764'])) ? $this->_rootref['L_764'] : ((isset($MSG['764'])) ? $MSG['764'] : '{ L_764 }')); ?>" class="btn btn-danger">
                    				</div>
								</form>
                                <?php } ?>
							  	</td>
                                <td>
                                    <?php if ($_users_val['SUSPENDED'] == 0) {  ?>
                                        <b><span style="color:green;"><?php echo ((isset($this->_rootref['L_5291'])) ? $this->_rootref['L_5291'] : ((isset($MSG['5291'])) ? $MSG['5291'] : '{ L_5291 }')); ?></span></b>
                					<?php } else if ($_users_val['SUSPENDED'] == (1)) {  ?>
                                        <b><span style="color:violet;"><?php echo ((isset($this->_rootref['L_5294'])) ? $this->_rootref['L_5294'] : ((isset($MSG['5294'])) ? $MSG['5294'] : '{ L_5294 }')); ?></span></b>
                					<?php } else if ($_users_val['SUSPENDED'] == (7)) {  ?>
                                        <b><span style="color:red;"><?php echo ((isset($this->_rootref['L_5297'])) ? $this->_rootref['L_5297'] : ((isset($MSG['5297'])) ? $MSG['5297'] : '{ L_5297 }')); ?></span></b>
                					<?php } else if ($_users_val['SUSPENDED'] == (8)) {  ?>
                                        <b><span style="color:orange;"><?php echo ((isset($this->_rootref['L_5292'])) ? $this->_rootref['L_5292'] : ((isset($MSG['5292'])) ? $MSG['5292'] : '{ L_5292 }')); ?></span></b><br>
                                        <form name="resend" action="" method="post" enctype="multipart/form-data">
		                    				<input type="hidden" name="admincsrftoken" value="<?php echo (isset($this->_rootref['_ACSRFTOKEN'])) ? $this->_rootref['_ACSRFTOKEN'] : ''; ?>">
		                    				<input type="hidden" name="resend_email" value="send">
		                    				<input type="hidden" name="user_id" value="<?php echo $_users_val['ID']; ?>">
		                    				<div class="mws-button-row">
		                    					<input type="submit" value="<?php echo ((isset($this->_rootref['L_25_0074'])) ? $this->_rootref['L_25_0074'] : ((isset($MSG['25_0074'])) ? $MSG['25_0074'] : '{ L_25_0074 }')); ?>" class="btn btn-danger">
		                    				</div>
										</form>
                					<?php } else if ($_users_val['SUSPENDED'] == (9)) {  ?>
                                        <b><span style="color:red;"><?php echo ((isset($this->_rootref['L_5293'])) ? $this->_rootref['L_5293'] : ((isset($MSG['5293'])) ? $MSG['5293'] : '{ L_5293 }')); ?></span></b>
                					<?php } else if ($_users_val['SUSPENDED'] == (10)) {  ?>
                                        <b><small style="color:orange;"><a href="excludeuser.php?id=<?php echo $_users_val['ID']; ?>&offset=<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>"><?php echo ((isset($this->_rootref['L_25_0136'])) ? $this->_rootref['L_25_0136'] : ((isset($MSG['25_0136'])) ? $MSG['25_0136'] : '{ L_25_0136 }')); ?></a></small></b>
                					<?php } ?>
                				</td>
                                <td>
                                    	<a href="edituser.php?userid=<?php echo $_users_val['ID']; ?>&offset=<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_298'])) ? $this->_rootref['L_298'] : ((isset($MSG['298'])) ? $MSG['298'] : '{ L_298 }')); ?></small></a><br />
                            			<a href="deleteuser.php?id=<?php echo $_users_val['ID']; ?>&offset=<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>"><small><?php echo ((isset($this->_rootref['L_008'])) ? $this->_rootref['L_008'] : ((isset($MSG['008'])) ? $MSG['008'] : '{ L_008 }')); ?></small></a><br />
                            			<a href="excludeuser.php?id=<?php echo $_users_val['ID']; ?>&offset=<?php echo (isset($this->_rootref['PAGE'])) ? $this->_rootref['PAGE'] : ''; ?>"><small>
			    						<?php if ($_users_val['SUSPENDED'] == 0) {  ?>
			                                <?php echo ((isset($this->_rootref['L_305'])) ? $this->_rootref['L_305'] : ((isset($MSG['305'])) ? $MSG['305'] : '{ L_305 }')); ?>
			    						<?php } else if ($_users_val['SUSPENDED'] == (8)) {  ?>
			                                <?php echo ((isset($this->_rootref['L_515'])) ? $this->_rootref['L_515'] : ((isset($MSG['515'])) ? $MSG['515'] : '{ L_515 }')); ?>
			    						<?php } else { ?>
			                                <?php echo ((isset($this->_rootref['L_299'])) ? $this->_rootref['L_299'] : ((isset($MSG['299'])) ? $MSG['299'] : '{ L_299 }')); ?>
			    						<?php } ?>
			                    	</small></a>
                                </td>
                            </tr>
                           <?php }} ?>
                           </tbody>
                        </table>
                    </div>   	
                </div>
<?php $this->_tpl_include('footer.tpl'); ?>