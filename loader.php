<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the u-Auctions website or https://ubidzz.com 
 * Please register at http://u-auctions.com and contact the u-Auctions admin  
 * at http://u-auctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/

ob_start('ob_gzhandler');
header("Content-type: text/javascript");
include 'inc/checks/files.php';
if (isset($_GET['js']))
{
	$js = explode(';', $_GET['js']);
	foreach ($js as $val)
	{
		$ext = substr($val, strrpos($val, '.') + 1);
		if ($ext == 'php')
		{
			if (check_file($val))
			{
				include $val;
			}
		}
		elseif ($ext == 'js' || $ext == 'css')
		{
			if (check_file($val) && is_file($val))
			{
				echo file_get_contents($val);
				echo "\n";
			}
		}
	}
}
ob_end_flush();

function check_file($file)
{
	global $file_allowed;
	$tmp = $file_allowed;
	
	$folders = explode('/', $file);
	foreach ($folders as $val)
	{
		if (isset($tmp[$val]))
		{
			$tmp = $tmp[$val];
		}
		else
		{
			return false;
		}
	}
	return true;
}