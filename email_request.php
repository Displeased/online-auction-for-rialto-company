<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

// Get auction_id from sessions variables
if (isset($_REQUEST['auction_id']))
{
	$auction_id = $_SESSION['CURRENT_ITEM'] = intval($_REQUEST['auction_id']);
}
elseif (isset($_SESSION['CURRENT_ITEM']))
{
	$auction_id = $_SESSION['CURRENT_ITEM'];
}

if(isset($auction_id))
{
	$query = "SELECT title FROM " . $DBPrefix . "auctions WHERE id = :auc_id";
	$params = array();
	$params[] = array(':auc_id', $auction_id, 'int');
	$db->query($query, $params);
	if ($db->numrows() > 0)
	{
		$TPL_item_title = $db->result('title');
	}
}

if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'email_request.php';
	header('location: user_login.php');
	exit;
}

$query = "SELECT id, email, nick FROM " . $DBPrefix . "users WHERE id = :user_id";
$params = array();
$params[] = array(':user_id', intval($_REQUEST['user_id']), 'int');
$db->query($query, $params);
$user_info = $db->result();
$user_id = $user_info['id'];
$email = $user_info['email'];
$username = $user_info['nick'];

$sent = false;
if (isset($_POST['action']) && $_POST['action'] == 'proceed')
{
	if (empty($_POST['TPL_text']))
	{
		$ERR = $ERR_031;
	}
	elseif ($auction_id < 0 || empty($auction_id))
	{
		$ERR = $ERR_622;
	}
	else
	{
		$query = "SELECT title FROM " . $DBPrefix . "auctions WHERE id = :auction_id";
		$params = array();
		$params[] = array(':auction_id', $auction_id, 'int');
		$db->query($query, $params);
		if ($db->numrows() == 0)
		{
			$ERR = $ERR_622;
		}
		else
		{
			$item_title = $db->result('title');
			$item_title = $system->uncleanvars($item_title);
			$from_email = ($system->SETTINGS['users_email'] == 'n') ? $user->user_data['email'] : $system->SETTINGS['adminmail'];
			// Send e-mail message
			$subject = $MSG['335'] . ' ' . $system->SETTINGS['sitename'] . ' ' . $MSG['336'] . ' ' . $item_title;
			$message = $MSG['084'] . ' ' . $MSG['240'] . ': ' . $from_email . "\n\n" . $_POST['TPL_text'];
			
			$send_email->email_request($user_id, $subject, $email, $message, $user->user_data['name'], $from_email);
			
			// send a copy to their mesasge box
			$nowmessage = nl2br($system->cleanvars($message));
			$query = "INSERT INTO " . $DBPrefix . "messages (sentto, sentfrom, sentat, message, subject)
					VALUES (:id, :user_id, :times, :nowmessage, :msg)";
			$params = array();
			$params[] = array(':id', $user_id, 'int');
			$params[] = array(':user_id', $user->user_data['id'], 'int');
			$params[] = array(':times', $system->ctime, 'int');
			$params[] = array(':nowmessage', $nowmessage, 'str');
			$params[] = array(':msg', $system->cleanvars(sprintf($MSG['651'], $item_title)), 'str');
			$db->query($query, $params);
			$sent = true;
		}
	}
}

$template->assign_vars(array(
		'B_SENT' => $sent,
		'ERROR' => (isset($TPL_error_text)) ? $TPL_error_text : '',
		'USERID' => $user_id,
		'B_FB_LINK' => 'IndexFBLogin',
		'USERNAME' => $username,
		'AUCTION_ID' => generate_seo_link($TPL_item_title) . '-' . $auction_id,
		'MSG_TEXT' => (isset($_POST['TPL_text'])) ? $_POST['TPL_text'] : ''
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'email_request.tpl'
		));
$template->display('body');
include 'footer.php';