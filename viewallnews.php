<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

$query = "SELECT id, title FROM " . $DBPrefix . "news WHERE suspended = :zero ORDER BY new_date";
$params = array();
$params[] = array(':zero', 0, 'int');
$db->query($query, $params);

while ($new = $db->result())
{
	$template->assign_block_vars('news', array(
			'TITLE' => stripslashes($new['title']),
			'ID' => $new['id']
			));
}

include 'header.php';
$template->set_filenames(array(
		'body' => 'viewallnews.tpl'
		));
$template->display('body');
include 'footer.php';