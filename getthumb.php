<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/

include 'common.php';

$w = (isset($_GET['w'])) ? intval($_GET['w']) : '';
$_w = $w;
$fromfile = (isset($_GET['fromfile'])) ? $uploaded_path . $security->decrypt($_GET['fromfile']) : '';
$nomanage = false;

//this only loads if there is no image cache file
function imageCache($fromfile, $w = false, $h= false)
{
	global $_w, $upload_path;	

	//finding out what image type and splitting it so we can also use it
	$imgtype = explode('.', $fromfile);
	$imgType = "imagecreatefrom" . $imgtype[1];
	$img = $imgType($fromfile);
	//image size
	$im_x = imagesx($img);
	$im_y = imagesy($img);
	
	if($w && $h)//thumb image
	{
		$image = imagecreatetruecolor($w, $h);
		imagecopyresampled($image, $img, 0, 0, 0, 0, $w, $h, $im_x, $im_y);    
		imagealphablending($image,true); //allows us to apply a 24-bit watermark over $image
		$is_thumb = true;
	}
	else //full size image
	{
		$image = imagecreatetruecolor($im_x, $im_y);
		imagecopyresampled($image, $img, 0, 0, 0, 0, $im_x, $im_y, $im_x, $im_y);    
		imagealphablending($image,true); //allows us to apply a 24-bit watermark over $image
		$is_thumb = false;
	}
	$funcall = "image" . $imgtype[1];
	//build the cache file
	if($is_thumb)//make thumb size image cache file
	{
		$funcall($image, $upload_path . 'cache/' . $_w . '-' . md5($fromfile)); //thumb image cache file
	}
	else//make full size image cache file
	{
		$funcall($image, $upload_path . 'cache/' . md5($fromfile)); //full size image cache file
	}
	//display the image 
	return $funcall($image);
	//destroy all the imagecopyresampled images
	imagedestroy($img);
	imagedestroy($image);
	imagedestroy($funcall);
}

function ErrorPNG($err)
{
	header('Content-type: image/png');
	$im = imagecreate(100, 30);
	$bgc = imagecolorallocate($im, 255, 255, 255);
	$tc = imagecolorallocate($im, 0, 0, 0);
	imagefilledrectangle($im, 0, 0, 100, 30, $bgc);
	imagestring($im, 1, 5, 5, $err, $tc);
	imagepng($im);
}

// control parameters and file existence
if (!isset($fromfile) || $fromfile == '')
{
	ErrorPNG('params empty');
	exit;
}
elseif (!file_exists($fromfile) && !fopen($fromfile, 'r'))
{
	ErrorPNG('img does not exist');
	exit;
}
//checking to see if the image was cached in the server and pull the image from that to save loading time
if (file_exists($upload_path . 'cache/' . $w . '-' . md5($fromfile))) //check to see if the thumb image cache file exists
{
	$img = getimagesize($fromfile);
	header('Content-type: ' . $img['mime']);
	echo file_get_contents($upload_path . 'cache/' . $w . '-' . md5($fromfile));
}
elseif (file_exists($upload_path . 'cache/' . md5($fromfile))) //check to see if the full size image cache file exists
{
	$img = getimagesize($fromfile);
	header('Content-type: ' . $img['mime']);
	echo file_get_contents($upload_path . 'cache/' . md5($fromfile));
}
else //no cache file found so making a new image cache file for the image
{
	if (function_exists('imagetypes'))
	{
		if (!is_dir($upload_path . 'cache')) mkdir($upload_path . 'cache', 0755);
		$img = @getimagesize($fromfile);
		if (is_array($img))
		{
			if($w !='')
			{
				// check image orientation
				if ($img[0] < $img[1] )
				{
					$h = $w;
					$ratio = floatval($img[1] / $h);
					$w = ceil($img[0] / $ratio);
				}
				else
				{
					$ratio = floatval($img[0] / $w);
					$h = ceil($img[1] / $ratio);
				}
			}
		}
		else
		{
			ErrorPNG('not image type');
			exit;
		}
	}
	else
	{
		$nomanage = true;
	}
	if ($nomanage)
	{
		ErrorPNG('image type not supported');
		exit;
	}
	header('Content-type: ' . $img['mime']);
	imageCache($fromfile,$w, $h);
}