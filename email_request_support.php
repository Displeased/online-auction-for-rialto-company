<?php
/*******************************************************************************
 *   copyright				: (C) 2011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   uAuctions website or http://codecanyon.net/ ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the uAuctions website or http://codecanyon.net/ 
 * Please register at http://uAuctions.com and contact the uAuctions admin  
 * at http://uAuctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
 *******************************************************************************/
 
include 'common.php';

if ($user->is_logged_in())
{
	header('location: support');
	exit;
}

// check recaptcha is enabled
$spam_html = '';
if ($system->SETTINGS['spam_register'] == 2)
{
	include $main_path . 'inc/captcha/recaptchalib.php';
	if(!$_POST["g-recaptcha-response"])
	{
		$capcha_text = '<script src="https://www.google.com/recaptcha/api.js"></script> <div class="g-recaptcha" data-sitekey="' . $system->SETTINGS['recaptcha_public'] . '"></div>';
	}
	elseif(isset($_POST["g-recaptcha-response"]))
	{
		$resp = recaptcha_check_answer($system->SETTINGS['recaptcha_private'], $_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);
	}
}
elseif ($system->SETTINGS['spam_register'] == 1)
{
	include $main_path . 'inc/captcha/securimage.php';
	
	$resp = new Securimage();
	$spam_html = $resp->show_html();
}

$site_id = $system->SETTINGS['sitename'];
$page_title = $MSG['350_10207'];
$admin_nick = $MSG['3500_1015733'];
$admin_email = $system->SETTINGS['adminmail'];
$sender_email = $user->user_data['email'] ? $user->user_data['email'] : $system->cleanvars($_POST['sender_email']);


if (isset($_POST['action']) || !empty($_POST['action'])) 
{ 
    $cleaned_question = stripslashes($system->cleanvars($_POST['sender_question'])); 
    $cleaned_subject = stripslashes($system->cleanvars($_POST['subject'])); 
    if ($system->SETTINGS['wordsfilter'] == 'y') 
    { 
        $cleaned_question = stripslashes($system->filter($cleaned_question)); 
        $cleaned_subject = stripslashes($system->filter($cleaned_subject));     
    }
    
	// Check errors
	if (isset($_POST['action']) && (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['sender_email']) || !preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $admin_email))) 
    {
    	$TPL_error_text = $ERR_032;
	}
	elseif (empty($cleaned_question))
	{
		$TPL_error_text = $ERR_031;
	}
	elseif (empty($cleaned_subject)) 
    { 
        $TPL_error_text = $ERR_031; 
    } 
	elseif (isset($_POST['action']) && (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $_POST['sender_email']) || !preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $admin_email) || !preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i', $sender_email))) 
    {
    	$TPL_error_text = $ERR_008;
	}
	elseif ($system->SETTINGS['spam_register'] == 2 && !$resp)
	{
    	$TPL_error_text = $MSG['752'];
	}
	elseif ($system->SETTINGS['spam_register'] == 1 && !$resp->check($_POST['captcha_code']))
	{
		$TPL_error_text = $MSG['752'];
	}

	if (empty($TPL_error_text))
	{
		$mes = $MSG['337'] . ': <i>' . $admin_nick . '</i><br>'.$MSG['350_10157'].'<br>';
		$senders_name = $user->user_data['name'] ? $system->cleanvars($_POST['sender_name']) : $system->cleanvars($_POST['sender_name']);
		$senders_email = $user->user_data['email'] ? $system->cleanvars($_POST['sender_email']) : $system->cleanvars($_POST['sender_email']);
		
		$send_email->email_request_support($senders_name, $cleaned_question, $cleaned_subject, $senders_email, $admin_nick, $user->user_data['id'], $admin_email, $sender_email);
	}
}

$template->assign_vars(array(
		'MESSAGE' => (isset($mes)) ? $mes : '',
		'ERROR' => (isset($TPL_error_text)) ? $TPL_error_text : '',
		'ADMIN_NICK' => $admin_nick,
		'ADMIN_EMAIL' => $system->SETTINGS['adminmail'],
		'SELLER_QUESTION' => (isset($_POST['sender_question'])) ? $system->cleanvars($_POST['sender_question']) : '',
		'PAGE_TITLE' => $page_title,
		'B_FB_LINK' => 'IndexFBLogin',
		'CAPCHA' => ($system->SETTINGS['spam_register'] == 2) ? $capcha_text : $spam_html,
		'SENDER_EMAIL' => isset($_POST['sender_email']) ? $_POST['sender_email'] : '',
		'SENDER_NAME' => isset($_POST['sender_name']) ? $_POST['sender_name'] : '',
		'SUBJECT' => isset($_POST['subject']) ? $system->cleanvars($_POST['subject']) : '',
		'EMAIL' => $user->user_data['email']
		));


include 'header.php';
$template->set_filenames(array(
		'body' => 'email_request_support.tpl'
		));
$template->display('body');
include 'footer.php';