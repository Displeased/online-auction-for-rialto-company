<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $include_path . 'dates.inc.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	header("location: user_login.php");
	exit;
}

// Auction id is present, now update table
if (isset($_GET['add']) && !empty($_GET['add']))
{
	$add_id = intval($_GET['add']);
	// Check if this item is not already added
	$items = trim($user->user_data['item_watch']);
	$match = strstr($items, $add_id);

	if (!$match)
	{
		$item_watch = trim($items . ' ' . $add_id);
		$item_watch_new = trim($item_watch);
		$query = "UPDATE " . $DBPrefix . "users SET item_watch = :new_item_watch WHERE id = :user_id";
		$params = array();
		$params[] = array(':new_item_watch', $system->cleanvars($item_watch_new), 'str');
		$params[] = array(':user_id', $user->user_data['id'], 'int');
		$db->query($query, $params);
		$user->user_data['item_watch'] = $item_watch_new;
	}
}

// Delete item form item watch
if (isset($_GET['delete']) && !empty($_GET['delete']))
{
	$items = trim($user->user_data['item_watch']);
	$auc_id = explode(' ', $items);
	for ($j = 0; $j < count($auc_id); $j++)
	{
		$match = strstr($auc_id[$j], $_GET['delete']);
		if ($match)
		{
			$item_watch = $item_watch;
		}
		else
		{
			$item_watch = $auc_id[$j] . ' ' . $item_watch;
		}
	}
	$item_watch_new = trim($item_watch);
	$query = "UPDATE " . $DBPrefix . "users SET item_watch = '" . $item_watch_new . "' WHERE id = " . $user->user_data['id'];
	$params = array();
	$params[] = array(':item_watch_new', $system->cleanvars($item_watch_new), 'str');
	$params[] = array(':user_id', $user->user_data['id'], 'int');
	$db->query($query, $params);
	$user->user_data['item_watch'] = $item_watch_new;
}

// Show results
$items = trim($user->user_data['item_watch']);

if ($items != '' && $items != null)
{
	$item = preg_split('/ /', $items);
	$total = count($item);
	$itemids = 0;
	for ($j = 0; $j < $total; $j++)
	{
		$itemids .= ',' . $item[$j];
		
	}

	$query = "SELECT * FROM " . $DBPrefix . "auctions WHERE id IN (" . $itemids . ")";
	$db->direct_query($query);
	$k = 0;

	while ($row = $db->result())
	{
		// get the data we need
		$row = build_items($row);

		// time left till the end of this auction 
		$ends = $row['ends'];
		$difference = $ends - $system->ctime;
		if ($difference > 0)
		{
			$ends_string = FormatTimeLeft($difference);
		}
		else
		{
			$ends_string = $MSG['911'];
		}
		$bgcolour = ($k % 2) ? 'bgcolor="#FFFEEE"' : '';

		$template->assign_block_vars('items', array(
			'ID' => $row['id'],
			'ROWCOLOUR' => ($row['highlighted'] == 'y') ? 'bgcolor="#fea100"' : $bgcolour,
			'IMAGE' => $row['pict_url'],
			'TITLE' => $row['title'],
			'SUBTITLE' => $row['subtitle'],
			'BUY_NOW' => ($difference < 0) ? '' : $row['buy_now'],
			'BID' => $row['current_bid'],
			'SHIPPING_COST' => $system->print_money($auction_data['shipping_cost']),
			'SEO_TITLE' => generate_seo_link($row['title']),
			'BIDFORM' => $system->print_money($row['current_bid']),
			'CLOSES' => ArrangeDateNoCorrection($row['ends']),
			'SHIPPING_COST' => $system->print_money($row['shipping_cost']),
			'TIMELEFT' => $ends_string,
			'NUMBIDS' => sprintf($MSG['950'], $row['num_bids']),

			'B_BOLD' => ($row['bold'] == 'y')
		));
		$k++;
	}
}

function build_items($row)
{
	global $system, $uploaded_path;

	// image icon
	if (!empty($row['pict_url']))
	{
		$row['pict_url'] = $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_list'] . '&fromfile=' . $security->encrypt($row['id'] . '/' . $row['pict_url']);
	}
	else
	{
		$row['pict_url'] = get_lang_img('nopicture.gif');
	}

	if ($row['current_bid'] == 0)
	{
		$row['current_bid'] = $row['minimum_bid'];
	}

	if ($row['buy_now'] > 0 && $row['bn_only'] == 'n' && ($row['num_bids'] == 0 || ($row['reserve_price'] > 0 && $row['current_bid'] < $row['reserve_price'])))
	{
		$row['buy_now'] = '<br><a href="' . $system->SETTINGS['siteurl'] . 'buy_now.php?id=' . $row['id'] . '"><img src="' . get_lang_img('buy_it_now.gif') . '" border=0 class="buynow"></a><small>' . $system->print_money($row['buy_now']) . '</small>';
	}
	elseif ($row['buy_now'] > 0 && $row['bn_only'] == 'y')
	{
		$row['current_bid'] = $row['buy_now'];
		$row['buy_now'] = '<br><a href="' . $system->SETTINGS['siteurl'] . 'buy_now.php?id=' . $row['id'] . '"><img src="' . get_lang_img('buy_it_now.gif') . '" border=0 class="buynow"></a> <small>' . $system->print_money($row['buy_now']) . '</small> <img src="' . get_lang_img('bn_only.png') . '" border="0" class="buynow">';
	}
	else
	{
		$row['buy_now'] = '';
	}

	return $row;
}

include 'header.php';
$TMP_usmenutitle = $MSG['472'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'item_watch.tpl'
		));
$template->display('body');
include 'footer.php';
