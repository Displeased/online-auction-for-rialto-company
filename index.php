<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';
include $main_path . 'language/' . $language . '/categories.inc.php';
if(!empty($_SESSION['SELL_title']))
{
	include $include_path . 'functions_sell.php';
	unsetsessions();
}

// Run cron according to SETTINGS as non-batch
if ($system->SETTINGS['cron'] == 2)
{
	include_once 'cron.php';
}

$fb_return_page = 'home';
$_SESSION['REDIRECT_AFTER_LOGIN'] = 'home';
$NOW = $system->ctime;

// prepare categories list for templates/template
// Prepare categories sorting
if ($system->SETTINGS['catsorting'] == 'alpha')
{
	$catsorting = 'ORDER BY cat_name ASC';
}
else
{
	$catsorting = 'ORDER BY sub_counter DESC';
}

$query = "SELECT cat_id FROM " . $DBPrefix . "categories WHERE parent_id = :parent_id";
$params = array();
$params[] = array(':parent_id', -1, 'int');
$db->query($query, $params);
$cate_ids = $db->result('cat_id');

$query = "SELECT * FROM " . $DBPrefix . "categories WHERE parent_id = :cateids " . $catsorting . " LIMIT :catsshows";
$params = array();	
$params[] = array(':cateids', $cate_ids, 'int');  
$params[] = array(':catsshows', $system->SETTINGS['catstoshow'], 'int');
$db->query($query, $params);
while ($row = $db->result())
{
	$sub_counter = $row['sub_counter'];
	$cat_counter = $row['counter'];
	
	if ($sub_counter > 0 && $system->SETTINGS['cat_counters'] == 'y')
	{
		$count_string = ' (' . $sub_counter . ')';	
	}
	else
	{
		$count_string = '';
	}

	$template->assign_block_vars('cat_list', array(
			'CATAUCNUM' => ($row['sub_counter'] != 0) ? '(' . $row['sub_counter'] . ')' : '',
			'ID' => $row['cat_id'],
			'SEO_NAME' => generate_seo_link($category_names[$row['cat_id']]),
			'IMAGE' => (!empty($row['cat_image'])) ? '<img src="' . $row['cat_image'] . '" border=0>' : '',
			'COLOUR' => (empty($row['cat_colour'])) ? '#FFFFFF' : $row['cat_colour'],
			'NAME' => $category_names[$row['cat_id']] . ' ' . $count_string
			));
}

// get featured items
$query = "SELECT id, title, current_bid, pict_url, ends, num_bids, minimum_bid, bn_only, buy_now, sell_type
        FROM " . $DBPrefix . "auctions 
        WHERE closed = 0 AND suspended = 0 AND starts <= :now AND featured = :yes ORDER BY RAND() DESC LIMIT :limit";
$params = array();	  
$params[] = array(':now', $NOW, 'int');
$params[] = array(':yes', 'y', 'str');
$params[] = array(':limit', $system->SETTINGS['featureditemsnumber'], 'int');
$db->query($query, $params);
$i = 0;
while($row = $db->result())
{
	$ends = $row['ends'];
	$difference = $ends - $system->ctime;
	if ($difference > 0)
	{
		$ends_string = FormatTimeLeft($difference);
	}
	else
	{
		$ends_string = $MSG['911'];
	}
	if($row['sell_type'] == 'free')
	{
		$high_bid = $MSG['3500_1015745'];
	}else{
		$high_bid = ($row['num_bids'] == 0) ? $row['minimum_bid'] : $row['current_bid'];
		$high_bid = ($row['bn_only'] == 'y') ? $system->print_money($row['buy_now']) : $system->print_money($high_bid);
	}
	$template->assign_block_vars('featured', array(
			'ENDS' => $ends_string,
			'ID' => $row['id'],
			'SEO_TITLE' => generate_seo_link($row['title']),
			'BID' => $high_bid,
			'IMAGE' => (!empty($row['pict_url'])) ? $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_list'] . '&fromfile=' . $security->encrypt($row['id'] . '/' . $row['pict_url']) : 'images/email_alerts/default_item_img.jpg',
			'TITLE' => $row['title']
			));
	$i++;
}
$featured_items = ($i > 0) ? true : false;

// get last created auctions
$query = "SELECT id, title,pict_url, starts from " . $DBPrefix . "auctions 
WHERE closed = 0 AND suspended = 0 AND starts <= :now ORDER BY starts DESC LIMIT :limit";
$params = array();	  
$params[] = array(':now', $NOW, 'int');
$params[] = array(':limit', $system->SETTINGS['lastitemsnumber'], 'int');
$db->query($query, $params);

$i = 0;
while ($row = $db->result())
{
	$date = $row['starts'] + $system->tdiff;
	$template->assign_block_vars('auc_last', array(
			'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
			'DATE' => ArrangeDateNoCorrection($date),
			'ID' => $row['id'],
			'SEO_TITLE' => generate_seo_link($row['title']),
			'IMAGE' => (!empty($row['pict_url'])) ? $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_list'] . '&fromfile=' . $security->encrypt($row['id'] . '/' . $row['pict_url']) : 'images/email_alerts/default_item_img.jpg',
			'TITLE' => $row['title']
			));
	$i++;
}
$auc_last = ($i > 0) ? true : false;

// get ending soon auctions
$query = "SELECT ends, id, title, pict_url FROM " . $DBPrefix . "auctions
WHERE closed = 0 AND suspended = 0 AND starts <= :now ORDER BY ends LIMIT :limit";
$params = array();	  
$params[] = array(':now', $NOW, 'int');
$params[] = array(':limit', $system->SETTINGS['endingsoonnumber'], 'int');
$db->query($query, $params);
$i = 0;
while ($row = $db->result())
{
	$difference = $row['ends'] - $system->ctime;
	if ($difference > 0)
	{
		$ends_string = FormatTimeLeft($difference);
	}
	else
	{
		$ends_string = $MSG['911'];
	}
	$template->assign_block_vars('end_soon', array(
			'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
			'DATE' => $ends_string,
			'ID' => $row['id'],
			'SEO_TITLE' => generate_seo_link($row['title']),
			'IMAGE' => (!empty($row['pict_url'])) ? $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_list'] . '&fromfile=' . $security->encrypt($row['id'] . '/' . $row['pict_url']) : 'images/email_alerts/default_item_img.jpg',
			'TITLE' => $row['title']
			));
	$i++;
}
$end_soon = ($i > 0) ? true : false;

// get hot items
$query = "SELECT a.id, a.title, a.current_bid, a.pict_url, a.ends, a.num_bids, a.minimum_bid, a.bn_only, a.buy_now, a.sell_type 
        FROM " . $DBPrefix . "auctions a 
        LEFT JOIN " . $DBPrefix . "auccounter c ON (a.id = c.auction_id) 
        WHERE closed = 0 AND suspended = 0 AND starts <= :now ORDER BY c.counter DESC LIMIT :limit";
$params = array();	  
$params[] = array(':now', $NOW, 'int');
$params[] = array(':limit', $system->SETTINGS['hotitemsnumber'], 'int');
$db->query($query, $params);
$i = 0;
while ($row = $db->result())
{
	$i++;
	$ends = $row['ends'];
    $difference = $ends - $system->ctime;
    if ($difference > 0)
	{
        $ends_string = FormatTimeLeft($difference); 
    }
	else
	{
        $ends_string = $MSG['911'];
    }
    if($row['sell_type'] == 'free')
	{
		$high_bid = $MSG['3500_1015745'];
	}else{
		$high_bid = ($row['num_bids'] == 0) ? $row['minimum_bid'] : $row['current_bid'];
		$high_bid = ($row['bn_only'] == 'y') ? $system->print_money($row['buy_now']) : $system->print_money($high_bid);
	}
    $template->assign_block_vars('hotitems', array(
            'ENDS' => $ends_string,
            'ID' => $row['id'],
            'SEO_TITLE' => generate_seo_link($row['title']),
            'BID' => $high_bid,
            'IMAGE' => (!empty($row['pict_url'])) ? $system->SETTINGS['siteurl'] . 'getthumb.php?w=' . $system->SETTINGS['thumb_list'] . '&fromfile=' . $security->encrypt($row['id'] . '/' . $row['pict_url']) : 'images/email_alerts/default_item_img.jpg',
            'TITLE' => $row['title']
            ));
}
$hot_items = ($i > 0) ? true : false;

// Build news list
$query = "SELECT n.title As t, n.new_date, t.* FROM " . $DBPrefix . "news n
LEFT JOIN " . $DBPrefix . "news_translated t ON (t.id = n.id)
WHERE t.lang = :languages AND n.suspended = :zero ORDER BY new_date DESC, id DESC LIMIT :limit";
$params = array();	
$params[] = array(':languages', $language, 'str');
$params[] = array(':zero', 0, 'int');  
$params[] = array(':limit', $system->SETTINGS['newstoshow'], 'int');  
$db->query($query, $params);
$i = 0;
while ($new = $db->result())
{
	$template->assign_block_vars('newsbox', array(
		'ID' => $new['id'],
		'DATE' => FormatDate($new['new_date']),
		'SEO_TITLE' => generate_seo_link($new['title']),
		'TITLE' => (!empty($new['title'])) ? $new['title'] : $new['t']
		));
	$i++;
}
$newsbox = ($i > 0) ? true : false;


$template->assign_vars(array(
		'B_FB_LINK' => 'IndexFBLogin',
		'B_FEATU_ITEMS' => $featured_items,
		'B_AUC_LAST' => $auc_last,
		'B_HOT_ITEMS' => $hot_items,
		'B_AUC_ENDSOON' => $end_soon,
		'B_LOGIN_BOX' => ($system->SETTINGS['loginbox'] == 1),
		'B_NEWS_BOX' => ($newsbox && $system->SETTINGS['newsbox'] == 1)
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'home.tpl'
		));
$template->display('body');
include 'footer.php';
unset($_SESSION['loginerror']);