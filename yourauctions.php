<?php
/*******************************************************************************
 *   copyright				: (C) 20011 - 2014 u-Auctions
 *   site					: http://www.u-auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of u-Auctions script.
 *   You are not allowed to resell/sell this script is  copyrighted to u-auctions.com.
 *   If you have been sold this script from a 3rd party and not from the 
 *   http://u-auctions.com website or https://ubidzz.com ask for a refund.
 *******************************************************************************/
/*******************************************************************************
 * If you bought this script from the https://u-Auctions.com website or https://ubidzz.com 
 * Please register at http://u-auctions.com/forum and contact the u-Auctions admin  
 * at http://u-auctions.com/forum with your order number and full name so we can change 
* your group to premium so you can view the paid area on the forums.
 *******************************************************************************/

include 'common.php';

// If user is not logged in redirect to login page
if (!$user->is_logged_in())
{
	$_SESSION['REDIRECT_AFTER_LOGIN'] = 'yourauctions.php';
	header('location: user_login.php');
	exit;
}

$NOW = $system->ctime;
$NOWB = gmdate('Ymd');
// DELETE OR CLOSE OPEN AUCTIONS
if (isset($_POST['action']) && $_POST['action'] == 'delopenauctions')
{
	if (is_array($_POST['O_delete']) && count($_POST['O_delete']) > 0)
	{
		$removed = 0;
		foreach ($_POST['O_delete'] as $k => $v)
		{
			$v = intval($v);
			// Pictures Gallery
			if ($dir = @opendir($upload_path . $v))
			{
				while ($file = readdir($dir))
				{
					if ($file != '.' && $file != '..')
					{
						@unlink($upload_path . $v . '/' . $file);
					}
				}
				closedir($dir);
				@rmdir($upload_path . $v);
			}

			// Delete auction views
			$query = "DELETE FROM " . $DBPrefix . "auccounter WHERE auction_id = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $v, 'int');
			$db->query($query, $params);

			// Auction
			$query = "DELETE FROM " . $DBPrefix . "auctions WHERE id = :auc_id";
			$params = array();
			$params[] = array(':auc_id', $v, 'int');
			$db->query($query, $params);
			$removed++;
		}

		$query = "UPDATE " . $DBPrefix . "counters SET auctions = (auctions - :removed)";
		$params = array();
		$params[] = array(':removed', $removed, 'int');
		$db->query($query, $params);
	}

	if (is_array($_POST['closenow']))
	{
		foreach ($_POST['closenow'] as $k => $v)
		{
			// Update end time to the current time
			$query = "UPDATE " . $DBPrefix . "auctions SET ends = :time, relist = relisted WHERE id = :auc_id";
			$params = array();
			$params[] = array(':time', $NOW, 'int');
			$params[] = array(':auc_id', $v, 'int');
			$db->query($query, $params);
		}
		include 'cron.php';
	}
}
// Retrieve active auctions from the database
$query = "SELECT id FROM " . $DBPrefix . "auctions WHERE user = :user_id AND closed = :closed AND starts <= :now_time AND suspended = :suspended";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$params[] = array(':closed', 0, 'int');
$params[] = array(':now_time', $NOW, 'int');
$params[] = array(':suspended', 0, 'int');
$db->query($query, $params);
$TOTALAUCTIONS = $db->numrows('id');

if (!isset($_GET['PAGE']) || $_GET['PAGE'] <= 1 || $_GET['PAGE'] == '')
{
	$OFFSET = 0;
	$PAGE = 1;
}
else
{
	$PAGE = intval($_GET['PAGE']);
	$OFFSET = ($PAGE - 1) * $system->SETTINGS['perpage'];
}
$PAGES = ($TOTALAUCTIONS == 0) ? 1 : ceil($TOTALAUCTIONS / $system->SETTINGS['perpage']);
// Handle columns sorting variables
if (!isset($_SESSION['oa_ord']) && empty($_GET['oa_ord']))
{
	$_SESSION['oa_ord'] = 'title';
	$_SESSION['oa_type'] = 'asc';
}
elseif (!empty($_GET['oa_ord']))
{
	$_SESSION['oa_ord'] = $_GET['oa_ord'];
	$_SESSION['oa_type'] = $_GET['oa_type'];
}
elseif (isset($_SESSION['oa_ord']) && empty($_GET['oa_ord']))
{
	$_SESSION['oa_nexttype'] = $_SESSION['oa_type'];
}
if (!isset($_SESSION['oa_nexttype']) || $_SESSION['oa_nexttype'] == 'desc')
{
	$_SESSION['oa_nexttype'] = 'asc';
}
else
{
	$_SESSION['oa_nexttype'] = 'desc';
}
if (!isset($_SESSION['oa_type']) || $_SESSION['oa_type'] == 'desc') {
	$_SESSION['oa_type_img'] = '<img src="images/arrow_up.gif" align="center" hspace="2" border="0" />';
}
else
{
	$_SESSION['oa_type_img'] = '<img src="images/arrow_down.gif" align="center" hspace="2" border="0" />';
}

$query = "SELECT id, title, starts, ends, current_bid, num_bids, relist, relisted FROM " . $DBPrefix . "auctions
	WHERE user = :user_id AND closed = 0 AND starts <= :now_time AND suspended = 0
	ORDER BY " . $_SESSION['oa_ord'] . " " . $_SESSION['oa_type'] . " LIMIT :offset, :perpage";
$params = array();
$params[] = array(':user_id', $user->user_data['id'], 'int');
$params[] = array(':now_time', $NOW, 'int');
$params[] = array(':offset', $OFFSET, 'int');
$params[] = array(':perpage', $system->SETTINGS['perpage'], 'int');
$db->query($query, $params);

$i = 0;
while ($item = $db->fetch())
{
	if ($item['num_bids'] > 0)
	{
		$query = "SELECT bid FROM " . $DBPrefix . "bids WHERE auction = :auc_id ORDER BY bid DESC, id DESC LIMIT 1";
		$params = array();
		$params[] = array(':auc_id', $item['id'], 'int');
		$db->query($query, $params);
		if ($db->numrows() > 0)
		{
			$high_bid = $db->result('bid');
		}
	}
	// Retrieve counter
	$query = "SELECT counter FROM " . $DBPrefix . "auccounter WHERE auction_id = :auc_id";
	$params = array();
	$params[] = array(':auc_id', $item['id'], 'int');
	$db->query($query, $params);
	if ($db->numrows() > 0)
	{
		$viewcounter = $db->result('counter');
	}
	else
	{
		$viewcounter = 0;
	}

	$template->assign_block_vars('items', array(
			'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
			'ID' => $item['id'],
			'TITLE' => $item['title'],
			'STARTS' => FormatDate($item['starts']),
			'ENDS' => FormatDate($item['ends']),
			'SEO_TITLE' => generate_seo_link($item['title']),
			'BID' => $system->print_money($item['current_bid']),
			'BIDS' => $item['num_bids'],
			'RELIST' => $item['relist'],
			'RELISTED' => $item['relisted'],
			'COUNTER' => $viewcounter,

			'B_HASNOBIDS' => ($item['current_bid'] == 0)
			));
	$i++;
}
// get pagenation
$PREV = intval($PAGE - 1);
$NEXT = intval($PAGE + 1);
if ($PAGES > 1)
{
	$LOW = $PAGE - 5;
	if ($LOW <= 0) $LOW = 1;
	$COUNTER = $LOW;
	while ($COUNTER <= $PAGES && $COUNTER < ($PAGE + 6))
	{
		$template->assign_block_vars('pages', array(
				'PAGE' => ($PAGE == $COUNTER) ? '<b>' . $COUNTER . '</b>' : '<a href="' . $system->SETTINGS['siteurl'] . 'yourauctions.php?PAGE=' . $COUNTER . '"><u>' . $COUNTER . '</u></a>'
				));
		$COUNTER++;
	}
}

$template->assign_vars(array(
		'BGCOLOUR' => (!($i % 2)) ? '' : 'class="alt-row"',
		'ORDERCOL' => $_SESSION['oa_ord'],
		'ORDERNEXT' => $_SESSION['oa_nexttype'],
		'ORDERTYPEIMG' => $_SESSION['oa_type_img'],

		'PREV' => ($PAGES > 1 && $PAGE > 1) ? '<a href="' . $system->SETTINGS['siteurl'] . 'yourauctions.php?PAGE=' . $PREV . '"><u>' . $MSG['5119'] . '</u></a>&nbsp;&nbsp;' : '',
		'NEXT' => ($PAGE < $PAGES) ? '<a href="' . $system->SETTINGS['siteurl'] . 'yourauctions.php?PAGE=' . $NEXT . '"><u>' . $MSG['5120'] . '</u></a>' : '',
		'PAGE' => $PAGE,
		'PAGES' => $PAGES,

		'B_AREITEMS' => ($i > 0)
		));

include 'header.php';
$TMP_usmenutitle = $MSG['619'];
include $include_path . 'user_cp.php';
$template->set_filenames(array(
		'body' => 'yourauctions.tpl'
		));
$template->display('body');
include 'footer.php';